//
//  SearchUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class SearchUseCase: UseCaseImpl {
    static var shared: SearchUseCase = SearchUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func searchGuilds(keyword: String) -> Promise<GuildEntities> {
        return Promise<GuildEntities> { resolve, reject, _ in
            guard keyword.count > 0 else { resolve(GuildEntities()); return }
            store.dispatch(SearchDispatcher.SET_GUILD_KEYWORD(payload: keyword))
            self.searchRepository?.searchGuilds(
                keyword: keyword,
                limit: DataConfig.fetch_data_limit(.S),
                offset: 0
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(SearchDispatcher.SET_GUILD(keyword: keyword, payload: result))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func searchMoreGuilds(keyword: String) -> Promise<GuildEntities> {
        return Promise<GuildEntities> { resolve, reject, _ in
            guard keyword.count > 0, let repositories = store.state.search.searchGuilds[keyword], repositories.count > 0 else { resolve(GuildEntities()); return }
            store.dispatch(SearchDispatcher.SET_GUILD_KEYWORD(payload: keyword))
            self.searchRepository?.searchGuilds(
                keyword: keyword,
                limit: DataConfig.fetch_data_limit(.S),
                offset: repositories.count
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(SearchDispatcher.ADD_GUILD(keyword: keyword, payload: result))
                    guard let guilds = store.state.search.searchGuilds[keyword] else { resolve(GuildEntities()); return }
                    resolve(guilds)
                }
            }).catch({
                reject($0)
            })
        }
    }
    
}
