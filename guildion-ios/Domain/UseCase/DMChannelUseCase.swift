//
//  DMChannelUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class DMChannelUseCase: UseCaseImpl {
    static var shared: DMChannelUseCase = DMChannelUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func get(uid: String) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> { resolve, reject, _ in
            self.dmChannelRepository?.get(
                uid: uid
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getRoles(uid: String) -> Promise<DMChannelRoleEntities> {
        return Promise<DMChannelRoleEntities> { resolve, reject, _ in
            self.dmChannelRepository?.getRoles(
                uid: uid
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func start(guild: GuildEntity, target: MemberEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> { resolve, reject, _ in
            self.dmChannelRepository?.start(
                guild: guild,
                target: target
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> { resolve, reject, _ in
            channel.Members = channel.Members?.toNewMemory()
            self.dmChannelRepository?.create(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> { resolve, reject, _ in
            channel.Members = channel.Members?.toNewMemory()
            self.dmChannelRepository?.update(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(entry: DMChannelEntryEntity) -> Promise<DMChannelEntryEntity> {
        return Promise<DMChannelEntryEntity> { resolve, reject, _ in
            self.dmChannelRepository?.update(
                entry: entry
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(channel: DMChannelEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.dmChannelRepository?.delete(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentMembers(channel: DMChannelEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.dmChannelRepository?.getCurrentDMChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM)
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementMembers(channel: DMChannelEntity, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.dmChannelRepository?.getIncrementDMChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementMembers(channel: DMChannelEntity, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.dmChannelRepository?.getDecrementDMChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getInvitableMembers(channel: DMChannelEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.dmChannelRepository?.getDMChannelInvitableMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.M)
            ).then(in: .main , {
                results in
                resolve(results)
                DispatchQueue.main.async {
                    store.dispatch(MemberDispatcher.SET_CHANNEL_MEMBERS(channel_uid: channel.uid, payload: results))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func invite(channel: DMChannelEntity, members: MemberEntities) -> Promise<DMChannelEntryEntities> {
        return Promise<DMChannelEntryEntities> { resolve, reject, _ in
            self.dmChannelRepository?.invite(
                channel: channel,
                targets: members
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func kick(channel: DMChannelEntity, members: MemberEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.dmChannelRepository?.kick(
                channel: channel,
                targets: members
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func leave(channel: DMChannelEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.dmChannelRepository?.leave(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(channel: DMChannelEntity, role: RoleEntity) -> Promise<DMChannelRoleEntity> {
        return Promise<DMChannelRoleEntity> { resolve, reject, _ in
            self.dmChannelRepository?.create(
                channel: channel,
                role: role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(channel_role: DMChannelRoleEntity) -> Promise<DMChannelRoleEntity> {
        return Promise<DMChannelRoleEntity> { resolve, reject, _ in
            self.dmChannelRepository?.update(
                channel_role: channel_role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(channel: DMChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.dmChannelRepository?.delete(
                channel: channel,
                role: role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func reads(channel: DMChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> { resolve, reject, _ in
            self.dmChannelRepository?.reads(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
}
