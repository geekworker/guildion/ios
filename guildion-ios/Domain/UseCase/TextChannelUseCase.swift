//
//  TextChannelUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class TextChannelUseCase: UseCaseImpl {
    static var shared: TextChannelUseCase = TextChannelUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func get(uid: String) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> { resolve, reject, _ in
            self.textChannelRepository?.get(
                uid: uid
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getRoles(uid: String) -> Promise<TextChannelRoleEntities> {
        return Promise<TextChannelRoleEntities> { resolve, reject, _ in
            self.textChannelRepository?.getRoles(
                uid: uid
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> { resolve, reject, _ in
            self.textChannelRepository?.create(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> { resolve, reject, _ in
            self.textChannelRepository?.update(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(channel: TextChannelEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.textChannelRepository?.delete(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentMembers(channel: TextChannelEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.textChannelRepository?.getCurrentTextChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM)
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementMembers(channel: TextChannelEntity, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.textChannelRepository?.getIncrementTextChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementMembers(channel: TextChannelEntity, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.textChannelRepository?.getDecrementTextChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getInvitableMembers(channel: TextChannelEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.textChannelRepository?.getTextChannelInvitableMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.M)
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func invite(channel: TextChannelEntity, members: MemberEntities) -> Promise<TextChannelEntryEntities> {
        return Promise<TextChannelEntryEntities> { resolve, reject, _ in
            self.textChannelRepository?.invite(
                channel: channel,
                targets: members
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func kick(channel: TextChannelEntity, members: MemberEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.textChannelRepository?.kick(
                channel: channel,
                targets: members
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func leave(channel: TextChannelEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.textChannelRepository?.leave(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(channel: TextChannelEntity, role: RoleEntity) -> Promise<TextChannelRoleEntity> {
        return Promise<TextChannelRoleEntity> { resolve, reject, _ in
            self.textChannelRepository?.create(
                channel: channel,
                role: role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(channel_role: TextChannelRoleEntity) -> Promise<TextChannelRoleEntity> {
        return Promise<TextChannelRoleEntity> { resolve, reject, _ in
            self.textChannelRepository?.update(
                channel_role: channel_role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(entry: TextChannelEntryEntity) -> Promise<TextChannelEntryEntity> {
        return Promise<TextChannelEntryEntity> { resolve, reject, _ in
            self.textChannelRepository?.update(
                entry: entry
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(channel: TextChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.textChannelRepository?.delete(
                channel: channel,
                role: role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func reads(channel: TextChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> { resolve, reject, _ in
            self.textChannelRepository?.reads(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func updateIndexes(channels: TextChannelEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.textChannelRepository?.sectionIndexesUpdate(
                channels: channels
            ).then(in: .main , { results in
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
}
