//
//  MessageUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class MessageUseCase: UseCaseImpl {
    static var shared: MessageUseCase = MessageUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func create(message: MessageEntity) -> Promise<MessageEntity> {
        return Promise<MessageEntity> { resolve, reject, _ in
            self.messageRepository?.create(
                message: message,
                onNextProgress: { progress in
                    DispatchQueue.main.async {
                        store.dispatch(AppDispatcher.SET_PROGRESS(payload: progress))
                    }
                }
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    let entities = MessageEntities()
                    entities.items.append(result)
                    store.dispatch(MessageDispatcher.CREATED(channel_uid: message.Messageable?.messageable_uid ?? "", payload: result))
                    store.dispatch(MessageDispatcher.REMOVE_CHANNEL_CHACHE_MESSAGES(channel_uid: message.Messageable?.messageable_uid ?? ""))
                    resolve(result)
                    if message.Files?.storage_count ?? 0 > 0 {
                        BaseViewControllerBuilder.build_AuthUseCase().syncCurrentUserForce().then({ _ in })
                    }
                }
            }).catch { error in
                DispatchQueue.main.async {
                    store.dispatch(MessageDispatcher.REMOVE_CHANNEL_CHACHE_MESSAGES(channel_uid: message.Messageable?.messageable_uid ?? ""))
                }
                reject(error)
            }
        }
    }
    
    func create(reaction: ReactionEntity) -> Promise<ReactionEntity> {
        return Promise<ReactionEntity> { resolve, reject, _ in
            self.messageRepository?.create(
                reaction: reaction
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func update(message: MessageEntity) -> Promise<MessageEntity> {
        return Promise<MessageEntity> { resolve, reject, _ in
            self.messageRepository?.update(
                message: message,
                onNextProgress: { progress in
                    DispatchQueue.main.async {
                        store.dispatch(AppDispatcher.SET_PROGRESS(payload: progress))
                    }
                }
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    let entities = MessageEntities()
                    entities.items.append(message)
                    store.dispatch(MessageDispatcher.SET_CACHES(payload: entities))
                    store.dispatch(MessageDispatcher.REMOVE_CHANNEL_CHACHE_MESSAGES(channel_uid: message.Messageable?.messageable_uid ?? ""))
                    resolve(result)
                    if message.Files?.storage_count ?? 0 > 0 {
                        BaseViewControllerBuilder.build_AuthUseCase().syncCurrentUserForce().then({ _ in })
                    }
                }
            }).catch { error in
                DispatchQueue.main.async {
                    store.dispatch(MessageDispatcher.REMOVE_CHANNEL_CHACHE_MESSAGES(channel_uid: message.Messageable?.messageable_uid ?? ""))
                }
                reject(error)
            }
        }
    }
    
    func delete(message: MessageEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.messageRepository?.delete(
                message: message
            ).then(in: .main , { result in
                DispatchQueue.main.async {
                    let entities = MessageEntities()
                    entities.items.append(message)
                    store.dispatch(MessageDispatcher.SET_DELETES(payload: entities))
                    resolve(result)
                    if message.Files?.storage_count ?? 0 > 0 {
                        BaseViewControllerBuilder.build_AuthUseCase().syncCurrentUserForce().then({ _ in })
                    }
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func getCurrentMessages(current_channel: MessageableEntity) -> Promise<MessageEntities> {
        return Promise<MessageEntities> { resolve, reject, _ in
            guard let channel_uid = current_channel.messageable_uid else { resolve(MessageEntities()); return }
            
            if let messages = store.state.message.channelMessages[channel_uid], messages.filter({ $0.id != nil }).count != 0 {
                self.getIncrementMessages(current_channel: current_channel).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            
            self.messageRepository?.getCurrentMessages(
                channel: current_channel,
                limit: DataConfig.fetch_data_limit(.FM) + 5,
                is_static: false
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(MessageDispatcher.SET_CHANNEL_MESSAGES(channel_uid: channel_uid, payload: result))
                    resolve(result)
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func getIncrementMessages(current_channel: MessageableEntity) -> Promise<MessageEntities> {
        return Promise<MessageEntities> { resolve, reject, _ in
            guard let channel_uid = current_channel.messageable_uid else { resolve(MessageEntities()); return }
            guard let messages = store.state.message.channelMessages[channel_uid], messages.count != 0, let last_id = messages.items.filter({ $0.id != nil }).last?.id else {
                self.getCurrentMessages(current_channel: current_channel).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            self.messageRepository?.getIncrementMessages(
                channel: current_channel,
                limit: DataConfig.fetch_data_limit(.FM) + 5,
                last_id: last_id,
                is_static: false
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(MessageDispatcher.ADD_CHANNEL_MESSAGES(channel_uid: channel_uid, payload: result))
                    guard let new_messages = store.state.message.channelMessages[channel_uid] else { return }
                    resolve(new_messages)
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func getDecrementMessages(current_channel: MessageableEntity) -> Promise<MessageEntities> {
        return Promise<MessageEntities> { resolve, reject, _ in
            guard let channel_uid = current_channel.messageable_uid else { resolve(MessageEntities()); return }
            guard let messages = store.state.message.channelMessages[channel_uid], messages.count != 0, let first_id = messages.items.filter({ $0.id != nil }).first?.id else {
                self.getCurrentMessages(current_channel: current_channel).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            self.messageRepository?.getDecrementMessages(
                channel: current_channel,
                limit: DataConfig.fetch_data_limit(.FM) + 5,
                first_id: first_id,
                is_static: false
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(MessageDispatcher.INSERT_CHANNEL_MESSAGES(channel_uid: channel_uid, payload: result))
                    guard let new_messages = store.state.message.channelMessages[channel_uid] else { return }
                    resolve(new_messages)
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func reads(messages: MessageEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard messages.count > 0 else { return }
            self.messageRepository?.reads(
                messages: messages
            ).then(in: .main , {
                result in
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func unreads(messages: MessageEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard messages.count > 0 else { return }
            self.messageRepository?.unreads(
                messages: messages
            ).then(in: .main , {
                result in
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
}
