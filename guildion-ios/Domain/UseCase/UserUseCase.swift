//
//  UserUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class UserUseCase: UseCaseImpl {
    static var shared: UserUseCase = UserUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func registerDevise() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.userRepository?.registerDevise(
            ).then(in: .main , {
                _ in
                resolve(())
            }).catch({
                reject($0)
            })
        }
    }
    
    func updateDevise() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.userRepository?.updateDevise(
            ).then(in: .main , {
                _ in
                resolve(())
            }).catch({
                reject($0)
            })
        }
    }
    
    func getMembers() -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.userRepository?.getMembers(
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentMembers() -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.userRepository?.getCurrentMembers(
                offset: 0,
                limit: DataConfig.fetch_data_limit(.XL)
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(user: UserEntity) -> Promise<UserEntity> {
        return Promise<UserEntity> { resolve, reject, _ in
            self.userRepository?.updateUser(
                user: user
            ).then(in: .main , {
                result in
                let authUseCase = BaseViewControllerBuilder.build_AuthUseCase()
                authUseCase.syncCurrentUserForce().then({ _ in })
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.userRepository?.deleteUser(
            ).then(in: .main , {
                result in
                let authUseCase = BaseViewControllerBuilder.build_AuthUseCase()
                authUseCase.logout().then({ _ in })
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func active() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.userRepository?.active(
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func inActive() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.userRepository?.inActive(
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func logoutDevise() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.userRepository?.logoutDevise(
            ).then(in: .main , { _ in
                resolve(())
            }).catch({
                reject($0)
            })
        }
    }
    
    func block(target: UserEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard target.id != 0 else { return }
            self.userRepository?.block(
                target: target
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
        
    func unblock(target: UserEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard target.id != 0 else { return }
            self.userRepository?.unblock(
                target: target
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func mute(target: UserEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard target.id != 0 else { return }
            self.userRepository?.mute(
                target: target
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func unmute(target: UserEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard target.id != 0 else { return }
            self.userRepository?.unmute(
                target: target
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func report(target: UserEntity, detail: String) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard target.id != 0, detail.count > 0 else { return }
            self.userRepository?.report(
                target: target,
                detail: detail
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getUserRelationStats() -> Promise<UserEntity> {
        return Promise<UserEntity> { resolve, reject, _ in
            self.userRepository?.getRelationStats(
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(UserDispatcher.SET_GUILD_BLOCKS(payload: result.GuildBlocks ?? GuildEntities()))
                    store.dispatch(UserDispatcher.SET_USER_BLOCKS(payload: result.Blocks ?? UserEntities()))
                    store.dispatch(UserDispatcher.SET_USER_BLOCKEDS(payload: result.Blockers ?? UserEntities()))
                    store.dispatch(UserDispatcher.SET_USER_MUTES(payload: result.Mutes ?? UserEntities()))
                    store.dispatch(UserDispatcher.SET_USER_MUTEDS(payload: result.Mutes ?? UserEntities()))
                }
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
}
