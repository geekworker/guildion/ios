//
//  SessionUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class SessionUseCase: UseCaseImpl {
    static var shared: SessionUseCase = SessionUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func register(nickname: String, email: String, password: String, enable_mail_notification: Bool) -> Promise<IdentityEntity> {
        return Promise<IdentityEntity> { resolve, reject, _ in
            self.sessionRepository?.register(
                nickname: nickname,
                email: email,
                password: password,
                locale: NSLocale.current.languageCode ?? "ja",
                country_code: (Locale.current as NSLocale).object(forKey: .countryCode) as? String ?? "JP",
                timezone: TimeZone.current.identifier,
                enable_mail_notification: enable_mail_notification
            ).then(in: .main , { result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(SessionDispatcher.REGISTERED())
                    if let user = result.User {
                        store.dispatch(AuthDispatcher.SET_CURRENT_USER(payload: user))
                    }
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func confirmEmail(code: String) -> Promise<IdentityEntity> {
        return Promise<IdentityEntity> { resolve, reject, _ in
            self.sessionRepository?.findOneIdentityRequest(
            ).then(in: .main , { [unowned self] result in
                let sessionModel = store.state.session.repository
                sessionModel?.identity = result
                DispatchQueue.main.async {
                    store.dispatch(SessionDispatcher.SET_SESSION(payload: sessionModel))
                }
                self.sessionRepository?.confirmEmail(
                    email: result.email,
                    code: code
                ).then(in: .main , { result in
                    resolve(result.identity)
                    DispatchQueue.main.async {
                        store.dispatch(SessionDispatcher.CONFIRMED())
                        if let user = result.identity.User {
                            store.dispatch(AuthDispatcher.SET_CURRENT_USER(payload: user))
                        }
                    }
                }).catch { error in
                    reject(error)
                    store.dispatch(AppDispatcher.SET_ERRORS(payload: [UUID().uuidString: error]))
                }
            })
        }
    }
    
    func resendConfirmEmail() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.sessionRepository?.findOneIdentityRequest(
            ).then(in: .main , { [unowned self] result in
                let sessionModel = store.state.session.repository
                sessionModel?.identity = result
                DispatchQueue.main.async {
                    store.dispatch(SessionDispatcher.SET_SESSION(payload: sessionModel))
                }
                self.sessionRepository?.rersendConfirmEmail(
                    email: result.email
                ).then(in: .main , { result in
                    resolve(result)
                    DispatchQueue.main.async {
                        store.dispatch(SessionDispatcher.CONFIRMED())
                    }
                }).catch({
                    reject($0)
                })
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func findOneIdentity() -> Promise<IdentityEntity> {
        return Promise<IdentityEntity> { resolve, reject, _ in
            self.sessionRepository?.findOneIdentityRequest(
            ).then(in: .main , { result in
                let sessionModel = store.state.session.repository
                sessionModel?.identity = result
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(
                        SessionDispatcher.SET_SESSION(
                            payload: sessionModel
                        )
                    )
                }
            }).catch { error in
                reject(error)
            }
        }
    }
}
