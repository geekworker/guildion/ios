//
//  FileUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class FileUseCase: UseCaseImpl {
    static var shared: FileUseCase = FileUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func get(id: Int) -> Promise<FileEntity> {
        return Promise<FileEntity> { resolve, reject, _ in
            guard id != 0 else { return }
            self.fileRepository?.get(
                id: id
            ).then(in: .main , {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentUserFiles() -> Promise<FileEntities> {
        return Promise<FileEntities> { resolve, reject, _ in
            guard CurrentUser.getCurrentUserEntity() != nil else { resolve(FileEntities()); return }
            self.fileRepository?.getCurrentUserFiles(
                limit: 10
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.SET_USER_FILES(payload: result))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func creates(files: FileEntities, in current_guild: GuildEntity) -> Promise<FileEntities> {
        return Promise<FileEntities> { resolve, reject, _ in
            
            if files.items.filter({ $0.format.content_type.is_movie }).count > 0 {
                let entities = FileEntities()
                entities.items = files.items.filter({ $0.format.content_type.is_movie })
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.ADD_GUILD_CHACHE_FILES(guild_uid: current_guild.uid, payload: entities))
                }
            }
            
            var promises: [Promise<FileEntities>] = []
            for file in files.items {
                let entities = FileEntities()
                file.setProgress(0)
                entities.items = [file]
                promises.append(
                    self.fileRepository!.creates(
                        files: entities,
                        onNextProgress: { progress in
                            DispatchQueue.main.async {
                                guard file.format.uploadable else { return }
                                store.dispatch(AppDispatcher.SET_PROGRESS(payload: progress))
                            }
                        },
                        didUpload: { results in
                            DispatchQueue.main.async {
                                store.dispatch(FileDispatcher.REMOVE_GUILD_CHACHE_FILES(guild_uid: current_guild.uid, payload: results))
                            }
                        }
                    )
                )
            }
            all(promises, concurrency: 1).then({
                let results = FileEntities()
                results.items = $0.reduce([]) { $0 + $1.items }
                resolve(results)
                if files.storage_count > 0 {
                    BaseViewControllerBuilder.build_AuthUseCase().syncCurrentUserForce().then({ _ in })
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func update(file: FileEntity) -> Promise<FileEntity> {
        return Promise<FileEntity> { resolve, reject, _ in
            self.fileRepository?.update(
                file: file
            ).then(in: .main , {
                results in
                let entities = FileEntities()
                entities.items = [results]
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.SET_CACHES(payload: entities))
                }
                resolve(results)
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func delete(file: FileEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.fileRepository?.delete(
                file: file
            ).then(in: .main , { _ in
                let entities = FileEntities()
                entities.items = [file]
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.SET_DELETES(payload: entities))
                }
                resolve(())
                if file.format.content_type.is_movie || file.format.content_type.is_image {
                    BaseViewControllerBuilder.build_AuthUseCase().syncCurrentUserForce().then({ _ in })
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func deletes(files: FileEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.fileRepository?.deletes(
                files: files
            ).then(in: .main , {
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.SET_DELETES(payload: files))
                }
                resolve($0)
                if files.storage_count > 0 {
                    BaseViewControllerBuilder.build_AuthUseCase().syncCurrentUserForce().then({ _ in })
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func createReferences(references: FileReferenceEntities) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> { resolve, reject, _ in
            guard let first = references.items.first, let folder = first.Folder else { return }
            store.dispatch(FolderDispatcher.ADD_REFERENCES(folder_uid: folder.uid, payload: references))
            store.dispatch(FileReferenceDispatcher.ADD_GALLERY_FILE_REFERENCES(folder_uid: folder.uid, payload: references))
            self.fileRepository?.createReferences(
                references: references
            ).then(in: .main , {
                results in
                resolve(results)
                DispatchQueue.main.async {
                    store.dispatch(FolderDispatcher.REFERENCES_CREATED(folder_uid: folder.uid, payload: results))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func deleteReferences(references: FileReferenceEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.fileRepository?.deleteReferences(
                references: references
            ).then(in: .main , {
                DispatchQueue.main.async {
                    store.dispatch(FileReferenceDispatcher.SET_DELETES(payload: references))
                }
                resolve($0)
            }).catch { error in
                reject(error)
            }
        }
    }
}
