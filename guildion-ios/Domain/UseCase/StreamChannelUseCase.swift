//
//  StreamChannelUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class StreamChannelUseCase: UseCaseImpl {
    static var shared: StreamChannelUseCase = StreamChannelUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func get(uid: String) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> { resolve, reject, _ in
            self.streamChannelRepository?.getStreamChannel(
                uid: uid
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getRoles(uid: String) -> Promise<StreamChannelRoleEntities> {
        return Promise<StreamChannelRoleEntities> { resolve, reject, _ in
            self.streamChannelRepository?.getRoles(
                uid: uid
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getParticipants(uid: String) -> Promise<ParticipantEntities> {
        return Promise<ParticipantEntities> { resolve, reject, _ in
            self.streamChannelRepository?.getParticipants(
                uid: uid
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getStreamChannel(uid: String) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> { resolve, reject, _ in
            guard uid != "" else { return }
            self.streamChannelRepository?.getStreamChannel(
                uid: uid
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(AppDispatcher.SET_CURRENT_CHANNEL(payload: result))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func start(guild: GuildEntity, target: MemberEntity?, stream: StreamEntity?) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> { resolve, reject, _ in
            self.streamChannelRepository?.start(
                guild: guild,
                target: target,
                stream: stream,
                voice_only: false,
                movie_only: false
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func startVoice(guild: GuildEntity, target: MemberEntity?, stream: StreamEntity?) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> { resolve, reject, _ in
            self.streamChannelRepository?.start(
                guild: guild,
                target: target,
                stream: stream,
                voice_only: true,
                movie_only: false
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func startMovie(guild: GuildEntity, target: MemberEntity?, stream: StreamEntity?) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> { resolve, reject, _ in
            self.streamChannelRepository?.start(
                guild: guild,
                target: target,
                stream: stream,
                voice_only: false,
                movie_only: true
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> { resolve, reject, _ in
            channel.Members = channel.Members?.toNewMemory()
            self.streamChannelRepository?.create(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> { resolve, reject, _ in
            channel.Members = channel.Members?.toNewMemory()
            self.streamChannelRepository?.update(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(channel: StreamChannelEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.streamChannelRepository?.delete(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentMembers(channel: StreamChannelEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.streamChannelRepository?.getCurrentStreamChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM)
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementMembers(channel: StreamChannelEntity, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.streamChannelRepository?.getIncrementStreamChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementMembers(channel: StreamChannelEntity, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.streamChannelRepository?.getDecrementStreamChannelMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getInvitableMembers(channel: StreamChannelEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.streamChannelRepository?.getStreamChannelInvitableMembers(
                channel: channel.toNewMemoryChannel(),
                offset: 0,
                limit: DataConfig.fetch_data_limit(.M)
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func invite(channel: StreamChannelEntity, members: MemberEntities) -> Promise<StreamChannelEntryEntities> {
        return Promise<StreamChannelEntryEntities> { resolve, reject, _ in
            self.streamChannelRepository?.invite(
                channel: channel,
                targets: members
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func kick(channel: StreamChannelEntity, members: MemberEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.streamChannelRepository?.kick(
                channel: channel,
                targets: members
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func leave(channel: StreamChannelEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.streamChannelRepository?.leave(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(channel: StreamChannelEntity, role: RoleEntity) -> Promise<StreamChannelRoleEntity> {
        return Promise<StreamChannelRoleEntity> { resolve, reject, _ in
            self.streamChannelRepository?.create(
                channel: channel,
                role: role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(channel_role: StreamChannelRoleEntity) -> Promise<StreamChannelRoleEntity> {
        return Promise<StreamChannelRoleEntity> { resolve, reject, _ in
            self.streamChannelRepository?.update(
                channel_role: channel_role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(entry: StreamChannelEntryEntity) -> Promise<StreamChannelEntryEntity> {
        return Promise<StreamChannelEntryEntity> { resolve, reject, _ in
            self.streamChannelRepository?.update(
                entry: entry
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(channel: StreamChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.streamChannelRepository?.delete(
                channel: channel,
                role: role
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func reads(channel: StreamChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> { resolve, reject, _ in
            self.streamChannelRepository?.reads(
                channel: channel
            ).then(in: .main, {
                resolve($0)
            }).catch({
                reject($0)
            })
        }
    }
    
    func updateIndexes(channels: StreamChannelEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.streamChannelRepository?.sectionIndexesUpdate(
                channels: channels
            ).then(in: .main , { results in
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func update(channel: StreamChannelEntity, is_loop: Bool) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.streamChannelRepository?.update(
                channel: channel,
                is_loop: is_loop
            ).then(in: .main , { results in
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
}
