//
//  RoleUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class RoleUseCase: UseCaseImpl {
    static var shared: RoleUseCase = RoleUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func create(role: RoleEntity) -> Promise<RoleEntity> {
        return Promise<RoleEntity> { resolve, reject, _ in
            self.roleRepository?.create(
                role: role
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(role: RoleEntity) -> Promise<RoleEntity> {
        return Promise<RoleEntity> { resolve, reject, _ in
            self.roleRepository?.update(
                role: role
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(role: RoleEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.roleRepository?.delete(
                role: role
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
}
