//
//  FolderUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class FolderUseCase: UseCaseImpl {
    static var shared: FolderUseCase = FolderUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.fileRepository = fileRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func create(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> { resolve, reject, _ in
            self.folderRepository?.create(
                folder: folder
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func update(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> { resolve, reject, _ in
            self.folderRepository?.update(
                folder: folder
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func delete(folder: FolderEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.folderRepository?.delete(
                folder: folder
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func getFolder(id: Int) -> Promise<FolderEntity> {
        return Promise<FolderEntity> { resolve, reject, _ in
            guard id != 0 else { resolve(FolderEntity()); return }
            self.folderRepository?.getFolder(
                id: id
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(FolderDispatcher.SET_SHOW_GALLERY(folder_uid: result.uid, payload: result))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func getCurrentFolders(current_guild: GuildEntity, is_playlist: Bool) -> Promise<FolderEntities> {
        return Promise<FolderEntities> { resolve, reject, _ in
            guard let guild_id = current_guild.id else { resolve(FolderEntities()); return }
            
            if let folders = store.state.folder.guildFolders[current_guild.uid], folders.count != 0 {
//                store.dispatch(self.getIncrementGuildFiles(afterFetched: { afterFetched($0) }))
            }
            
            self.folderRepository?.getFolders(
                guild_id: guild_id,
                limit: DataConfig.fetch_data_limit(.M),
                offset: 0,
                is_playlist: is_playlist
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(FolderDispatcher.SET_GUILD_GALLERIES(guild_uid: current_guild.uid, payload: result))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func getCurrentGuildFiles(current_guild: GuildEntity, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> { resolve, reject, _ in
            guard let guild_id = current_guild.id else { resolve(FileEntities()); return }
            
            if let files = store.state.file.guildFiles[current_guild.uid], files.count != 0 {
                self.getIncrementGuildFiles(current_guild: current_guild, is_playlist: is_playlist).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            
            self.folderRepository?.getCurrentGuildFiles(
                guild_id: guild_id,
                limit: DataConfig.fetch_data_limit(.FM) + 1,
                is_playlist: is_playlist
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.SET_GUILD_FILES(guild_uid: current_guild.uid, payload: result))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func getIncrementGuildFiles(current_guild: GuildEntity, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> { resolve, reject, _ in
            guard let guild_id = current_guild.id else { resolve(FileEntities()); return }
            guard let files = store.state.file.guildFiles[current_guild.uid], files.count != 0 else {
                self.getCurrentGuildFiles(current_guild: current_guild, is_playlist: is_playlist).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            guard let last_id = files.items.filter({ $0.id != nil }).last?.id else { resolve(FileEntities()); return }
            self.folderRepository?.getIncrementGuildFiles(
                guild_id: guild_id,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id,
                is_playlist: is_playlist
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.ADD_GUILD_FILES(guild_uid: current_guild.uid, payload: result))
                    guard let new_files = store.state.file.guildFiles[current_guild.uid] else { resolve(FileEntities()); return }
                    resolve(new_files)
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementGuildFiles(current_guild: GuildEntity, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> { resolve, reject, _ in
            guard let guild_id = current_guild.id else { resolve(FileEntities()); return }
            guard let files = store.state.file.guildFiles[current_guild.uid], files.count != 0 else {
                self.getCurrentGuildFiles(current_guild: current_guild, is_playlist: is_playlist).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            guard let first_id = files.items.filter({ $0.id != nil }).first?.id else { resolve(FileEntities()); return }
            self.folderRepository?.getDecrementGuildFiles(
                guild_id: guild_id,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id,
                is_playlist: is_playlist
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(FileDispatcher.INSERT_GUILD_FILES(guild_uid: current_guild.uid, payload: result))
                    guard let new_files = store.state.file.guildFiles[current_guild.uid] else { resolve(FileEntities()); return }
                    resolve(new_files)
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementGuildFile(file: FileEntity, is_playlist: Bool, in current_guild: GuildEntity) -> Promise<FileEntity?> {
        return Promise<FileEntity?> { resolve, reject, _ in
            guard let guild_id = current_guild.id, let file_id = file.id else { resolve(nil); return }
            self.folderRepository?.getIncrementGuildFiles(
                guild_id: guild_id,
                limit: 1,
                last_id: file_id,
                is_playlist: is_playlist
            ).then(in: .main , {
                result in
                resolve(result.first)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementGuildFile(file: FileEntity, is_playlist: Bool, in current_guild: GuildEntity) -> Promise<FileEntity?> {
        return Promise<FileEntity?> { resolve, reject, _ in
            guard let guild_id = current_guild.id, let file_id = file.id else { resolve(nil); return }
            self.folderRepository?.getDecrementGuildFiles(
                guild_id: guild_id,
                limit: 1,
                first_id: file_id,
                is_playlist: is_playlist
            ).then(in: .main , {
                result in
                resolve(result.first)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentFolderReferences(folder: FolderEntity) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> { resolve, reject, _ in
            guard let folder_id = folder.id else { resolve(FileReferenceEntities()); return }
//            if let references = store.state.folder.showFolders[folder.uid]?.FileReferences, references.count > 0 {
//                store.dispatch(self.getIncrementFolderReferences(folder: folder, afterFetched: afterFetched))
//                return
//            }
            self.folderRepository?.getCurrentFolderReference(
                folder_id: folder_id,
                limit: DataConfig.fetch_data_limit(.FM) - 5
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(FolderDispatcher.SET_REFERENCES(folder_uid: folder.uid, payload: result))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementFolderReferences(folder: FolderEntity) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> { resolve, reject, _ in
            guard let folder_id = folder.id else { resolve(FileReferenceEntities()); return }
            guard let references = store.state.folder.showFolders[folder.uid]?.FileReferences, let last_id = references.filter({ $0.id != nil }).last?.id else {
                self.getCurrentFolderReferences(folder: folder).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            self.folderRepository?.getIncrementFolderReference(
                folder_id: folder_id,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(FolderDispatcher.ADD_REFERENCES(folder_uid: folder.uid, payload: result))
                    guard let newEntities = store.state.folder.showFolders[folder.uid]?.FileReferences else { resolve(FileReferenceEntities()); return }
                    resolve(newEntities)
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementFolderReferences(folder: FolderEntity) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> { resolve, reject, _ in
            guard let folder_id = folder.id else { return }
            guard let references = store.state.folder.showFolders[folder.uid]?.FileReferences, let first_id = references.filter({ $0.id != nil }).first?.id else {
                self.getCurrentFolderReferences(folder: folder).then({ resolve($0) }).catch({ reject($0) })
                return
            }
            self.folderRepository?.getDecrementFolderReference(
                folder_id: folder_id,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id
            ).then(in: .main , {
                result in
                DispatchQueue.main.async {
                    store.dispatch(FolderDispatcher.INSERT_REFERENCES(folder_uid: folder.uid, payload: result))
                    guard let newEntities = store.state.folder.showFolders[folder.uid]?.FileReferences else { resolve(FileReferenceEntities()); return }
                    resolve(newEntities)
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementReference(reference: FileReferenceEntity) -> Promise<FileReferenceEntity?> {
        return Promise<FileReferenceEntity?> { resolve, reject, _ in
            guard let reference_id = reference.id, let folder_id = reference.FolderId else { resolve(nil); return }
            self.folderRepository?.getIncrementFolderReference(
                folder_id: folder_id,
                limit: 1,
                last_id: reference_id
            ).then(in: .main , {
                result in
                resolve(result.first)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementReference(reference: FileReferenceEntity) -> Promise<FileReferenceEntity?> {
        return Promise<FileReferenceEntity?> { resolve, reject, _ in
            guard let reference_id = reference.id, let folder_id = reference.FolderId else { resolve(nil); return }
            self.folderRepository?.getDecrementFolderReference(
                folder_id: folder_id,
                limit: 1,
                first_id: reference_id
            ).then(in: .main , {
                result in
                resolve(result.first)
            }).catch({
                reject($0)
            })
        }
    }

    func updateReferenceIndexes(references: FileReferenceEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.folderRepository?.updateIndexes(
                references: references
            ).then(in: .main , { results in
                DispatchQueue.main.async {
                    store.dispatch(FileReferenceDispatcher.SET_CACHES(payload: results))
                }
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func updateIndexes(folders: FolderEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.folderRepository?.updateIndexes(
                folders: folders
            ).then(in: .main , { results in
                // dispatch(FolderDispatcher.SET_CACHES(payload: results))
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
}
