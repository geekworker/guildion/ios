//
//  UseCaseImpl.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import UIKit

protocol UseCaseImpl: class {
    var authRepository: AuthRepository? { get set }
    var categoryRepository: CategoryRepository? { get set }
    var folderRepository: FolderRepository? { get set }
    var dmChannelRepository: DMChannelRepository? { get set }
    var fileRepository: FileRepository? { get set }
    var guildRepository: GuildRepository? { get set }
    var searchRepository: SearchRepository? { get set }
    var sessionRepository: SessionRepository? { get set }
    var streamChannelRepository: StreamChannelRepository? { get set }
    var streamRepository: StreamRepository? { get set }
    var textChannelRepository: TextChannelRepository? { get set }
    var tagRepository: TagRepository? { get set }
    var notificationRepository: NotificationRepository? { get set }
    var messageRepository: MessageRepository? { get set }
    var memberRepository: MemberRepository? { get set }
    var userRepository: UserRepository? { get set }
    var roleRepository: RoleRepository? { get set }
}

