//
//  AuthUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import UIKit
import ReSwift
import Hydra

class AuthUseCase: UseCaseImpl, CurrentUserProtocol {
    static var shared: AuthUseCase = AuthUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func login(email: String, password: String) -> Promise<UserEntity> {
        return Promise<UserEntity> { resolve, reject, _ in
            self.authRepository?.login(
                email: email,
                password: password
            ).then(in: .main , { result in
                DispatchQueue.main.async {
                    store.dispatch(AuthDispatcher.SET_CURRENT_USER(payload: result))
                    store.dispatch(AuthDispatcher.LOGINED())
                    store.dispatch(AuthDispatcher.SYNC_CURRENT_USER_END())
                }
                UserUseCase.shared.getUserRelationStats().then({ _ in })
                UserUseCase.shared.updateDevise().then({ _ in })
                resolve(result)
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func logout() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            WebSocketHandler.shared.closeWebSocket()
            self.userRepository!.logoutDevise().then({ _ in
                self.userRepository?.inActive(
                ).then(in: .main , {
                    CurrentUser.logout()
                    ClientDevise.logout()
                    CurrentAudio.logout()
                    DispatchQueue.main.async {
                        store.dispatch(AuthDispatcher.LOGOUTED())
                        store.dispatch(SessionDispatcher.NOT_REGISTERED())
                        store.dispatch(SessionDispatcher.NOT_CONFIRMED())
                        store = Store<State>(reducer: rootReducer, state: nil, middleware: [sagaMiddleware])
                    }
                    resolve($0)
                }).catch({
                    if $0 is AppError {
                        CurrentUser.logout()
                        ClientDevise.logout()
                        CurrentAudio.logout()
                        DispatchQueue.main.async {
                            store.dispatch(AuthDispatcher.LOGOUTED())
                            store.dispatch(SessionDispatcher.NOT_REGISTERED())
                            store.dispatch(SessionDispatcher.NOT_CONFIRMED())
                            store = Store<State>(reducer: rootReducer, state: nil, middleware: [sagaMiddleware])
                            resolve(())
                        }
                    } else {
                        reject($0)
                    }
                })
            }).catch({
                if $0 is AppError {
                    CurrentUser.logout()
                    ClientDevise.logout()
                    CurrentAudio.logout()
                    DispatchQueue.main.async {
                        store.dispatch(AuthDispatcher.LOGOUTED())
                        store.dispatch(SessionDispatcher.NOT_REGISTERED())
                        store.dispatch(SessionDispatcher.NOT_CONFIRMED())
                        store = Store<State>(reducer: rootReducer, state: nil, middleware: [sagaMiddleware])
                        resolve(())
                    }
                } else {
                    reject($0)
                }
            })
        }
    }
    
    func syncCurrentUser() -> Promise<UserEntity?> {
        return Promise<UserEntity?> { resolve, reject, _ in
            if let synced = store.state.auth.synced, synced {
                return
            } else {
                guard CurrentUser.getCurrentUserEntity() != nil else {
                    self.logout().then({})
                    resolve(nil)
                    return
                }
                self.authRepository?.syncUser().then(in: .main , {
                    guard let userEntity = $0 else {
                        self.logout().then({})
                        resolve($0)
                        return
                    }
                    self.setCurrentUser(entity: userEntity)
                    UserUseCase.shared.getUserRelationStats().then({ _ in })
                    UserUseCase.shared.updateDevise().then({ _ in })
                    DispatchQueue.main.async {
                        AppConfig.session_out = false
                        store.dispatch(AuthDispatcher.SET_CURRENT_USER(payload: userEntity))
                        store.dispatch(AuthDispatcher.LOGINED())
                        store.dispatch(AuthDispatcher.SYNC_CURRENT_USER_END())
                    }
                    resolve($0)
                })
                .catch {
                    reject($0)
                }
            }
        }
    }
    
    func syncCurrentUserForce() -> Promise<UserEntity?> {
        return Promise<UserEntity?> { resolve, reject, _ in
            guard CurrentUser.getCurrentUserEntity() != nil else {
                self.logout().then({})
                resolve(nil)
                return
            }
            self.authRepository?.syncUser(
            ).then(in: .main , {
                guard let userEntity = $0 else {
                    self.logout().then({})
                    resolve($0)
                    return
                }
                self.setCurrentUser(entity: userEntity)
                self.setCurrentUser(entity: userEntity)
                UserUseCase.shared.getUserRelationStats().then({ _ in })
                UserUseCase.shared.updateDevise().then({ _ in })
                DispatchQueue.main.async {
                    AppConfig.session_out = false
                    store.dispatch(AuthDispatcher.SET_CURRENT_USER(payload: userEntity))
                    store.dispatch(AuthDispatcher.LOGINED())
                    store.dispatch(AuthDispatcher.SYNC_CURRENT_USER_END())
                }
                resolve($0)
            })
            .catch {
                reject($0)
            }
        }
    }
}

