//
//  StreamUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class StreamUseCase: UseCaseImpl {
    static var shared: StreamUseCase = StreamUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func startLive(stream: StreamEntity, loggable: Bool, in current_channel: StreamChannelEntity) -> Promise<StreamEntity> {
        return Promise<StreamEntity> { resolve, reject, _ in
            stream.Channel = current_channel
            stream.ChannelId = current_channel.id
            self.streamRepository?.startLive(
                stream: stream,
                loggable: loggable
            ).then(in: .main , {
                result in
                resolve(result.stream)
                DispatchQueue.main.async {
                    store.dispatch(StreamDispatcher.START_LIVE(payload: result.stream))
                    store.dispatch(StreamDispatcher.JOINED(payload: result.participant))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func endLive() -> Promise<StreamEntity> {
        return Promise<StreamEntity> { resolve, reject, _ in
            guard let stream = store.state.stream.liveStream else {
                reject(AppError.unknown)
                return
            }
            self.streamRepository?.endLive(
                stream: stream
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(StreamDispatcher.END_LIVE())
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func switchLive(stream: StreamEntity, from current_stream: StreamEntity, in current_channel: StreamChannelEntity) -> Promise<StreamEntity> {
        return Promise<StreamEntity> { resolve, reject, _ in
            stream.Channel = current_channel
            stream.ChannelId = current_channel.id
            self.streamRepository?.switchLive(
                stream: stream,
                current_stream: current_stream
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(StreamDispatcher.SET_LIVE(payload: result))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func join(channel: StreamChannelEntity) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> { resolve, reject, _ in
            self.streamRepository?.join(
                channel: channel
            ).then(in: .main , {
                result in
                resolve((result.stream, result.participant))
                DispatchQueue.main.async {
                    store.dispatch(StreamDispatcher.START_LIVE(payload: result.stream))
                    store.dispatch(StreamDispatcher.JOINED(payload: result.participant))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func leave(channel: StreamChannelEntity, duration: Float) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> { resolve, reject, _ in
            self.streamRepository?.leave(
                channel: channel,
                duration: duration
            ).then(in: .main , {
                result in
                resolve((result.stream, result.participant))
                DispatchQueue.main.async {
                    store.dispatch(StreamDispatcher.END_LIVE())
                    store.dispatch(StreamDispatcher.LEFT())
                }
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func updateParticipantState(participant: ParticipantEntity) -> Promise<ParticipantEntity> {
        return Promise<ParticipantEntity> { resolve, reject, _ in
            self.streamRepository?.updateParticipantState(
                participant: participant
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(StreamDispatcher.JOINED(payload: result))
                }
            }).catch { error in
                reject(error)
            }
        }
    }
}
