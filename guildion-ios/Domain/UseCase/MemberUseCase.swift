//
//  MemberUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class MemberUseCase: UseCaseImpl {
    static var shared: MemberUseCase = MemberUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func get(id: Int) -> Promise<MemberEntity> {
        return Promise<MemberEntity> { resolve, reject, _ in
            self.memberRepository?.get(
                id: id
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getRoles(member: MemberEntity) -> Promise<MemberRoleEntities> {
        return Promise<MemberRoleEntities> { resolve, reject, _ in
            self.memberRepository?.getRoles(
                member: member
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentRoleMembers(role: RoleEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.memberRepository?.getCurrentRoleMembers(
                role: role,
                limit: DataConfig.fetch_data_limit(.FM)
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementRoleMembers(role: RoleEntity, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.memberRepository?.getIncrementRoleMembers(
                role: role,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementRoleMembers(role: RoleEntity, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.memberRepository?.getDecrementRoleMembers(
                role: role,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id
            ).then(in: .main , {
                results in
                resolve(results)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> { resolve, reject, _ in
            self.memberRepository?.update(
                member: member
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func updates() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.memberRepository?.updates(
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func updateNotification(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> { resolve, reject, _ in
            self.memberRepository?.updateNotification(
                member: member
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(member_role: MemberRoleEntity) -> Promise<MemberRoleEntity> {
        return Promise<MemberRoleEntity> { resolve, reject, _ in
            self.memberRepository?.create(
                member_role: member_role
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(member_role: MemberRoleEntity) -> Promise<MemberRoleEntity> {
        return Promise<MemberRoleEntity> { resolve, reject, _ in
            self.memberRepository?.update(
                member_role: member_role
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(member_role: MemberRoleEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.memberRepository?.delete(
                member_role: member_role
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func updateIndexes(members: MemberEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.memberRepository?.updateIndexes(
                members: members
            ).then(in: .main , { results in
                // dispatch(MemberDispatcher.SET_CACHES(payload: results))
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
}
