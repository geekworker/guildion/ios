//
//  GuildUseCase.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift
import Hydra

class GuildUseCase: UseCaseImpl {
    static var shared: GuildUseCase = GuildUseCase(
        authRepository: AuthRepository.shared,
        categoryRepository: CategoryRepository.shared,
        folderRepository: FolderRepository.shared,
        dmChannelRepository: DMChannelRepository.shared,
        fileRepository: FileRepository.shared,
        guildRepository: GuildRepository.shared,
        searchRepository: SearchRepository.shared,
        sessionRepository: SessionRepository.shared,
        streamChannelRepository: StreamChannelRepository.shared,
        streamRepository: StreamRepository.shared,
        textChannelRepository: TextChannelRepository.shared,
        tagRepository: TagRepository.shared,
        notificationRepository: NotificationRepository.shared,
        messageRepository: MessageRepository.shared,
        memberRepository: MemberRepository.shared,
        userRepository: UserRepository.shared,
        roleRepository: RoleRepository.shared
    )
    var authRepository: AuthRepository?
    var categoryRepository: CategoryRepository?
    var folderRepository: FolderRepository?
    var dmChannelRepository: DMChannelRepository?
    var fileRepository: FileRepository?
    var guildRepository: GuildRepository?
    var searchRepository: SearchRepository?
    var sessionRepository: SessionRepository?
    var streamChannelRepository: StreamChannelRepository?
    var streamRepository: StreamRepository?
    var textChannelRepository: TextChannelRepository?
    var tagRepository: TagRepository?
    var notificationRepository: NotificationRepository?
    var messageRepository: MessageRepository?
    var memberRepository: MemberRepository?
    var userRepository: UserRepository?
    var roleRepository: RoleRepository?

    required init(
        authRepository: AuthRepository,
        categoryRepository: CategoryRepository,
        folderRepository: FolderRepository,
        dmChannelRepository: DMChannelRepository,
        fileRepository: FileRepository,
        guildRepository: GuildRepository,
        searchRepository: SearchRepository,
        sessionRepository: SessionRepository,
        streamChannelRepository: StreamChannelRepository,
        streamRepository: StreamRepository,
        textChannelRepository: TextChannelRepository,
        tagRepository: TagRepository,
        notificationRepository: NotificationRepository,
        messageRepository: MessageRepository,
        memberRepository: MemberRepository,
        userRepository: UserRepository,
        roleRepository: RoleRepository
    ) {
        self.authRepository = authRepository
        self.categoryRepository = categoryRepository
        self.folderRepository = folderRepository
        self.dmChannelRepository = dmChannelRepository
        self.fileRepository = fileRepository
        self.guildRepository = guildRepository
        self.searchRepository = searchRepository
        self.sessionRepository = sessionRepository
        self.streamChannelRepository = streamChannelRepository
        self.streamRepository = streamRepository
        self.textChannelRepository = textChannelRepository
        self.tagRepository = tagRepository
        self.notificationRepository = notificationRepository
        self.messageRepository = messageRepository
        self.memberRepository = memberRepository
        self.userRepository = userRepository
        self.roleRepository = roleRepository
    }
    
    func getUserGuilds() -> Promise<GuildEntities> {
        return Promise<GuildEntities> { resolve, reject, _ in
            self.guildRepository?.getUserGuilds(
                limit: DataConfig.fetch_data_limit(.XL),
                offset: 0
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(GuildDispatcher.SET_USER_GUILDS(payload: result))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getMoreUserGuilds() -> Promise<GuildEntities> {
        return Promise<GuildEntities> { resolve, reject, _ in
            guard let offset = store.state.guild.userGuilds?.items.count else { resolve(GuildEntities()); return }
            self.guildRepository?.getUserGuilds(
                limit: DataConfig.fetch_data_limit(.XL),
                offset: offset
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(GuildDispatcher.ADD_USER_GUILDS(payload: result))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getGuildMembers(guild: GuildEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.guildRepository?.getGuildMembers(
                guild: guild
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentGuildMembers(guild: GuildEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.guildRepository?.getCurrentGuildMembers(
                guild: guild,
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM)
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementGuildMembers(guild: GuildEntity, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.guildRepository?.getIncrementGuildMembers(
                guild: guild,
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementGuildMembers(guild: GuildEntity, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> { resolve, reject, _ in
            self.guildRepository?.getDecrementGuildMembers(
                guild: guild,
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getCurrentMemberRequests(guild: GuildEntity) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> { resolve, reject, _ in
            self.guildRepository?.getCurrentMemberRequests(
                guild: guild,
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM)
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getIncrementMemberRequests(guild: GuildEntity, last_id: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> { resolve, reject, _ in
            self.guildRepository?.getIncrementMemberRequests(
                guild: guild,
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                last_id: last_id
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getDecrementMemberRequests(guild: GuildEntity, first_id: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> { resolve, reject, _ in
            self.guildRepository?.getDecrementMemberRequests(
                guild: guild,
                offset: 0,
                limit: DataConfig.fetch_data_limit(.FM),
                first_id: first_id
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getGuildChannels(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> { resolve, reject, _ in
            self.guildRepository?.getGuildChannels(
                uid: guild.uid
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getGuild(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> { resolve, reject, _ in
            guard uid != "" else { resolve(GuildEntity()); return }
            self.guildRepository?.getGuild(
                uid: uid
            ).then(in: .main , {
                result in
                resolve(result)
                DispatchQueue.main.async {
                    store.dispatch(GuildDispatcher.SET_SHOW_GUILD(payload: result))
                }
            }).catch({
                reject($0)
            })
        }
    }
    
    func getGuildWelcome(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> { resolve, reject, _ in
            guard uid != "" else { resolve(GuildEntity()); return }
            self.guildRepository?.getGuildWelcome(
                uid: uid
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getGuildChannel(guild_uid: String, channel_uid: String) -> Promise<(guild: GuildEntity, channel: MessageableEntity, member: MemberEntity)> {
        return Promise<(guild: GuildEntity, channel: MessageableEntity, member: MemberEntity)> { resolve, reject, _ in
            self.guildRepository?.getGuildChannel(
                guild_uid: guild_uid,
                channel_uid: channel_uid
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getGuildChannels(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> { resolve, reject, _ in
            guard uid != "" else { return }
            self.guildRepository?.getGuildChannels(
                uid: uid
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func getGuildRoles(uid: String) -> Promise<RoleEntities> {
        return Promise<RoleEntities> { resolve, reject, _ in
            self.guildRepository?.getGuildRoles(
                uid: uid
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> { resolve, reject, _ in
            self.guildRepository?.create(
                guild: guild
            ).then(in: .main , {
                result in
                resolve(result)
                self.getUserGuilds().then({ _ in })
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> { resolve, reject, _ in
            self.guildRepository?.update(
                guild: guild
            ).then(in: .main , {
                result in
                resolve(result)
                self.getGuild(uid: result.uid).then({ _ in })
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(guild: GuildEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.delete(
                guild: guild
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func create(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> { resolve, reject, _ in
            self.guildRepository?.create(
                channel: channel
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func update(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> { resolve, reject, _ in
            self.guildRepository?.update(
                channel: channel
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func delete(channel: SectionChannelEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.delete(
                channel: channel
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func join(guild: GuildEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> { resolve, reject, _ in
            self.guildRepository?.join(
                guild: guild
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func leave(guild: GuildEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.leave(
                guild: guild
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func kick(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.kick(
                guild: guild,
                target: target
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func block(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.block(
                guild: guild,
                target: target
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func report(guild: GuildEntity, description: String) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.report(
                guild: guild,
                description: description
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func createMemberRequest(guild: GuildEntity) -> Promise<MemberRequestEntity> {
        return Promise<MemberRequestEntity> { resolve, reject, _ in
            self.guildRepository?.createMemberRequest(
                guild: guild
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func updateMemberRequest(guild: GuildEntity, target: UserEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> { resolve, reject, _ in
            self.guildRepository?.updateMemberRequest(
                guild: guild,
                target: target
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func deleteMemberRequest(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.deleteMemberRequest(
                guild: guild,
                target: target
            ).then(in: .main , {
                result in
                resolve(result)
            }).catch({
                reject($0)
            })
        }
    }
    
    func updateIndexes(channels: SectionChannelEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.sectionIndexesUpdate(
                channels: channels
            ).then(in: .main , { results in
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
    
    func updateIndexes(guilds: GuildEntities) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            self.guildRepository?.updateIndexes(
                guilds: guilds
            ).then(in: .main , { results in
                resolve(())
            }).catch { error in
                reject(error)
            }
        }
    }
}
