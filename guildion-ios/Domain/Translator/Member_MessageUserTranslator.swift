//
//  User_MessageUserTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Parchment
import MessageKit

final class Member_MessageUsersTranslator: Translator {
    typealias Input  = MemberEntities
    typealias Output = [MessageUser]
    
    static func translate(_ entity: Input) -> Output {
        return entity.items.map { Member_MessageUserTranslator.translate($0) }
    }
}

final class Member_MessageUserTranslator: Translator {
    typealias Input  = MemberEntity
    typealias Output = MessageUser
    
    static func translate(_ entity: Input) -> Output {
        return MessageUser(senderId: "\(entity.User?.id ?? 0)", displayName: entity.nickname)
    }
}
