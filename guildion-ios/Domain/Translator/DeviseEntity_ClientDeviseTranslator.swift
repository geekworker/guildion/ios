//
//  DeviseEntity_ClientDeviseTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/22.
//

import Foundation
import UIKit
import Nuke

final class DeviseEntity_ClientDeviseTranslator: Translator {
    typealias Input  = DeviseEntity
    typealias Output = ClientDevise
    
    static func translate(_ input: Input) -> Output {
        let entity = ClientDevise.getClientDevise()
        entity.uid = input.uid
        entity.notification_id = input.notification_id
        entity.permission = input.permission
        return entity
    }
}

