//
//  Guild_ImagePagingItemTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import UIKit
import Parchment

final class Guild_ImagePagingItemsTranslator: Translator {
    typealias Input  = GuildEntities
    typealias Output = ImagePagingModels
    
    static func translate(_ entity: Input) -> Output {
        let repositories: [ImagePagingModel] = entity.items.map {
            Guild_ImagePagingItemTranslator.translate($0)
        }
        let models = ImagePagingModels(items: repositories)
        return models
    }
}

final class Guild_ImagePagingItemTranslator: Translator {
    typealias Input  = GuildEntity
    typealias Output = ImagePagingModel
    
    static func translate(_ entity: Input) -> Output {
        return ImagePagingModel(
            index: entity.index,
            title: entity.name,
            headerImageURL: URL(string: entity.picture_small),
            imageable_id: entity.id ?? 0,
            imageable_type: String(describing: type(of: entity))
        )
    }
}
