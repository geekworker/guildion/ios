//
//  Message_CustomMessageTypeTransfer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/19.
//

import Foundation
import UIKit
import MessageKit

final class Message_CustomMessageTypeTransfer: Translator {
    typealias Input  = MessageEntity
    typealias Output = [CustomMessageTypes]
    
    static func translate(_ entity: Input) -> Output {
        var types: [CustomMessageTypes] = []
        guard !entity.is_log, entity.Channelog?.id == nil else { return entity.Reactions?.count ?? 0 > 0 ? [.channelog, .reaction] : [.channelog] }
        if entity.text.count > 0 {
            types.append(.text)
        }
        if entity.text.extractURL().count > 0 {
            types.append(.ogp)
        }
        if entity.Files?.count ?? 0 > 0 {
            types.append(.media)
        }
        if entity.Reactions?.count ?? 0 > 0 {
            types.append(.reaction)
        }
        return types
    }
}
