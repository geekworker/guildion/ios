//
//  FileEntity_ImageViewerModelTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/07.
//

import Foundation
import UIKit
import Nuke

final class FileEntity_ImageViewerModelTranslator: Translator {
    typealias Input  = FileEntity
    typealias Output = ImageViewerModel?
    
    static func translate(_ entity: Input) -> Output {
        guard entity.format.content_type.is_image, let url = URL(string: entity.url) else { return nil }
        return .image(fetchImageBlock: { handler in
            let imageView = UIImageView()
            Nuke.loadImage(with: url, into: imageView, completion: { _ in
                handler(imageView.image ?? UIImage())
            })
        })
    }
}
