//
//  User_MessageUserTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Parchment
import MessageKit

final class User_MessageUsersTranslator: Translator {
    typealias Input  = UserEntities
    typealias Output = [MessageUser]
    
    static func translate(_ entity: Input) -> Output {
        return entity.items.map { User_MessageUserTranslator.translate($0) }
    }
}

final class User_MessageUserTranslator: Translator {
    typealias Input  = UserEntity
    typealias Output = MessageUser
    
    static func translate(_ entity: Input) -> Output {
        return MessageUser(senderId: "\(entity.id ?? 0)", displayName: entity.nickname)
    }
}

