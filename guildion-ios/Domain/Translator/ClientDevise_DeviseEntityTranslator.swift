//
//  ClientDevise_DeviseEntityTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/22.
//

import Foundation
import UIKit
import Nuke
import CoreLocation
final class ClientDevise_DeviseEntityTranslator: Translator {
    typealias Input  = ClientDevise
    typealias Output = DeviseEntity
    
    static func translate(_ input: Input) -> Output {
        let entity = DeviseEntity()
        entity.uid = input.uid
        entity.udid = input.udid
        entity.os = input.os
        entity.model = input.model
        entity.country_code = input.country_code ?? "JP"
        entity.app_version = input.app_version
        entity.locale = input.locale.rawValue
        entity.notification_id = input.notification_id
        entity.permission = input.permission
        return entity
    }
}
