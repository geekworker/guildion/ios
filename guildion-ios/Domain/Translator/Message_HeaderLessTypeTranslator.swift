//
//  Message_HeaderLessTypeTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/21.
//

import Foundation
import UIKit
import MessageKit

final class Message_HeaderLessTypeTranslator: Translator {
    typealias Input  = (current: MessageEntity, before: MessageEntity)
    typealias Output = Bool
    
    static func translate(_ inputs: Input) -> Output {
        guard
            inputs.current.SenderId == inputs.before.SenderId,
            inputs.current.created_at.timeIntervalSince(inputs.before.created_at) < ChannelConfig.headerless_max_interval_sc
        else { return false }
        return true
    }
}
