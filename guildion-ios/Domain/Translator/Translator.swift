//
//  Translator.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import Foundation

protocol Translator: class {
    associatedtype Input
    associatedtype Output
    
    static func translate(_ entity: Input) -> Output
}
