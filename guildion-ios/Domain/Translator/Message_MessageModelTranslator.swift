//
//  Message_MessageModelTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Parchment
import MessageKit

final class Message_MessageModelsTranslator: Translator {
    typealias Input  = MessageEntities
    typealias Output = [MessageModel]
    
    static func translate(_ entity: Input) -> Output {
        return entity.items.map { Message_MessageModelTranslator.translate($0) }
    }
}

final class Message_MessageModelTranslator: Translator {
    typealias Input  = MessageEntity
    typealias Output = MessageModel
    
    static func translate(_ entity: Input) -> Output {
        // MARK: should switch entity.Attachments => media message model
        return MessageModel(custom: entity, user: Member_MessageUserTranslator.translate(entity.Sender ?? MemberEntity()), messageId: entity.uid, date: entity.created_at)
    }
}


