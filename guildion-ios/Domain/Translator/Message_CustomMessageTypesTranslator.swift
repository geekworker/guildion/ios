//
//  Message_CustomMessageTypesTranslator.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/12.
//

import Foundation
import UIKit
import MessageKit

final class Message_CustomMessageTypesTranslator: Translator {
    typealias Input  = (current: MessageEntity, messagesLayout: MessageKit.MessagesCollectionViewFlowLayout, indexPath: IndexPath)
    typealias Output = [CustomMessageTypes]
    
    static func translate(_ inputs: Input) -> Output {
        let messagesLayout = inputs.messagesLayout
        let dataSource = messagesLayout.messagesDataSource
        var types = Message_CustomMessageTypeTransfer.translate(inputs.current)
        
        var before: MessageEntity? = nil
        
        if inputs.indexPath.section > 0 {
            let _before = dataSource.messageForItem(at: IndexPath(row: 0, section: inputs.indexPath.section - 1), in: messagesLayout.messagesCollectionView)
            if case .custom(let before_data) = _before.kind, let __before = before_data as? MessageEntity {
                before = __before
            }
        }
        
        if inputs.indexPath.section > 0 {
            if !types.contains(.channelog),
               let unwrapped = before,
               Message_HeaderLessTypeTranslator.translate((current: inputs.current, before: unwrapped)),
               !TimeSectionView.isVisible(current_date: inputs.current.created_at, before_date: unwrapped.created_at) {
                types.append(.headLess)
            } else if let unwrapped = before, TimeSectionView.isVisible(current_date: inputs.current.created_at, before_date: unwrapped.created_at) {
                types.append(.timeSection)
            }
        } else if inputs.indexPath.section == 0 {
            types.append(.timeSection)
        }
        
        return types
    }
}
