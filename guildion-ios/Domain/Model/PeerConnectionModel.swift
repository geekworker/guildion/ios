//
//  PeerConnectionModel.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import UIKit
import AVFoundation
import WebRTC
import Starscream
import SwiftyJSON
import VideoToolbox

class PeerConnectionModel: ModelImpl {
    public var id: String = ""
    public var peerConnection: RTCPeerConnection? = nil
    public var syncPlayManager: SyncPlayManagerModel = SyncPlayManagerModel()
    public var user: UserEntity?
    public var member: MemberEntity?
    public var videoTrack: RTCVideoTrack?
    public var audioTrack: RTCAudioTrack?
    public var meterLevel: Float = 0
    
    var syncingTarget: Bool = false
    var seekingTarget: Bool = false
    var fetchingTarget: Bool = false
    
    init(
        id: String = UUID().uuidString,
        peerConnection: RTCPeerConnection?,
        user: UserEntity
    ) {
        self.id = id
        self.peerConnection = peerConnection
        self.user = user
    }
}

extension PeerConnectionModel: Equatable {
    static func == (lhs: PeerConnectionModel, rhs: PeerConnectionModel) -> Bool{
        let lid = lhs.id
        let rid = rhs.id
        return lid == rid
    }
}

class PeerConnectionModels: ModelsImpl {
    public var items: [PeerConnectionModel] = []
    public var syncings: [PeerConnectionModel] {
        self.items.filter({
            $0.peerConnection?.validICEConnectionState ?? false
        })
    }
    public var syncingTarget: SyncPlayManagerModel? {
        get {
            return items.filter({ $0.syncingTarget }).first?.syncPlayManager
        }
        set {
            if let unwrapped =  items.filter({ $0.syncingTarget }).first {
                unwrapped.syncingTarget = false
            }
            items.filter({ $0.syncPlayManager == newValue }).first?.syncingTarget = true
        }
    }
    public var seekingTarget: SyncPlayManagerModel? {
        get {
            return items.filter({ $0.seekingTarget }).first?.syncPlayManager
        }
        set {
            if let unwrapped =  items.filter({ $0.seekingTarget }).first {
                unwrapped.seekingTarget = false
            }
            items.filter({ $0.syncPlayManager == newValue }).first?.seekingTarget = true
        }
    }
    public var fetchingTarget: SyncPlayManagerModel? {
        get {
            return items.filter({ $0.fetchingTarget }).first?.syncPlayManager
        }
        set {
            if let unwrapped =  items.filter({ $0.fetchingTarget }).first {
                unwrapped.fetchingTarget = false
            }
            items.filter({ $0.syncPlayManager == newValue }).first?.fetchingTarget = true
        }
    }
    
    init() {
    }
    
    public func getConnection(id: String) -> PeerConnectionModel? {
        for (_, value) in items.enumerated() {
            if value.id == id {
                return value
            }
        }
        return nil
    }
    
    public func getConnection(user: UserEntity) -> PeerConnectionModel? {
        for (_, value) in items.enumerated() {
            if value.user == user {
                return value
            }
        }
        return nil
    }
    
    public func getConnection(member: MemberEntity) -> PeerConnectionModel? {
        for (_, value) in items.enumerated() {
            if value.member == member {
                return value
            }
        }
        return nil
    }
    
    public func getConnectionFromPeerConnection(peerConnection: RTCPeerConnection) -> PeerConnectionModel? {
        for (_, value) in items.enumerated() {
            if value.peerConnection == peerConnection {
                return value
            }
        }
        return nil
    }
    
    public func addConnection(peerConnectionModel: PeerConnectionModel) {
        guard self.getConnection(id: peerConnectionModel.id) == nil else { return }
        self.items.append(peerConnectionModel)
    }
    
    public func getConnectionIndex(id: String) -> Int? {
        for (index, value) in items.enumerated() {
            if value.id == id {
                return index
            }
        }
        return nil
    }
    
    public func removeConnection(id: String) {
        guard let index = getConnectionIndex(id: id) else { return }
        self.items.remove(at: index)
    }
    
    public func stopAllConnections() {
        _ = self.items.enumerated().compactMap {
            stopConnection(id: $0.element.id)
        }
    }
    
    public func stopConnection(id: String) {
        guard let model = getConnection(id: id) else { return }
        if let peerConnection = model.peerConnection {
            peerConnection.close()
            model.peerConnection = nil
        }
        removeConnection(id: model.id)
    }
    
    public func stopConnectionFromPeerConnection(peerConnection: RTCPeerConnection) {
        for (_, value) in items.enumerated() {
            if value.peerConnection == peerConnection {
                stopConnection(id: value.id)
            }
        }
    }
    
    public func resetStatus() {
        for (_, value) in items.enumerated() {
            value.syncPlayManager.reset()
        }
    }
    
    public func checkSyncReady() -> Bool {
        guard self.items.count > 0 else { return true }
        return self.syncings
            .filter {
                $0.syncPlayManager.status == .readyToPlay || $0.syncPlayManager.status == .readyToPause
            }.count >= self.syncings.count
    }
    
    public func checkShouldFetch() -> Bool {
        guard self.items.count > 0 else { return false }
        return self.syncings
            .filter {
                $0.syncPlayManager.duration > 0.1
            }.count == self.syncings.count
    }
    
    public func shouldTransferHost() -> Bool {
        guard let current_user = CurrentUser.getCurrentUserEntity() else { return false }
        guard self.items.count > 0 else { return true }
        return self.syncings
            .sorted {
                $0.user?.id ?? 0 < $1.user?.id ?? 0
            }.first?.user?.id ?? 0 >= current_user.id ?? 0
    }
}

extension RTCPeerConnection {
    var validICEConnectionState: Bool {
        return self.iceConnectionState == .connected
    }
}
