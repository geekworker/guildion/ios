//
//  StreamableModel.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import UIKit

class StreamableModel: ModelImpl {
    public var files: FileEntities?
    public var file: FileEntity?
    public var reference: FileReferenceEntity?
}

