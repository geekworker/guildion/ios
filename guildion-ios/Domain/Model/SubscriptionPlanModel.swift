//
//  SubscriptionPlanModel.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit

class SubscriptionPlanModel: ModelImpl {
    public var plan: PlanConfig.SubscriptionPlan = .volumeMini
    public var span: PlanConfig.SubscriptionSpan = .month1
    public var function: PlanConfig.PlanFunction = .count
    var priceString: String {
        return self.span.priceString(plan: self.plan)
    }
    var discountString: String {
        return self.span.discountString(plan: self.plan)
    }
    var monthPriceString: String {
        return self.span.monthPriceString(plan: self.plan)
    }
}
