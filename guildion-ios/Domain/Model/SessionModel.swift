//
//  SessionModel.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit

class SessionModel: ModelImpl {
    public var identity: IdentityEntity? = IdentityEntity()
    public var nickname: String?
    public var code: String?
    public var user: UserEntity? = UserEntity()
    
    func initializeUser() {
        if self.user == nil {
            user = UserEntity()
        }
        user?.picture_small = DataConfig.Image.default_profile_image_url
    }
}
