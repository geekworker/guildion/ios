//
//  Accordionable.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/18.
//

import Foundation

protocol Accordionable: class {
    var collapsed: Bool { get set }
}
