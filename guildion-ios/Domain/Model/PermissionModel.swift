//
//  PermissionModel.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit

class PermissionModel: ModelImpl {
    lazy var Guild: GuildPermission = {
        let Guild = GuildPermission()
        return Guild
    }()
    lazy var DMChannel: DMChannelPermission = {
        let DMChannel = DMChannelPermission()
        return DMChannel
    }()
    lazy var TextChannel: TextChannelPermission = {
        let TextChannel = TextChannelPermission()
        return TextChannel
    }()
    lazy var StreamChannel: StreamChannelPermission = {
        let StreamChannel = StreamChannelPermission()
        return StreamChannel
    }()
    
    init() {
        DMChannel.guildDelegate = Guild
        TextChannel.guildDelegate = Guild
        StreamChannel.guildDelegate = Guild
    }
}
