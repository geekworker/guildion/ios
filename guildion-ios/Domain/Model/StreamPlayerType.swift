//
//  StreamPlayerType.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/13.
//

import Foundation

public enum StreamPlayerType: Int {
    case avplayer
    case youtube
    case voice
    case other
}
