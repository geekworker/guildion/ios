//
//  StreamScreenType.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/13.
//

import Foundation
import UIKit

public enum StreamScreenType: Int {
    case normal
    case mini
    case full
}
