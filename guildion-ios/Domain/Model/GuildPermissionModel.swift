//
//  GuildPermissionModel.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/14.
//

import Foundation

class GuildPermission: ModelImpl, TextChannelPermissionGuildDelegate, StreamChannelPermissionGuildDelegate, DMChannelPermissionGuildDelegate {
    var current_guild: GuildEntity?
    var current_member_roles: MemberRoleEntities = MemberRoleEntities()
    public var prioritest_role: RoleEntity {
        let entities = RoleEntities()
        entities.items = current_member_roles.compactMap({ $0.Role })
        return entities.prioritest_role ?? RoleEntity()
    }
    
    init() {}
    
    init(
        current_guild: GuildEntity,
        current_member_roles: MemberRoleEntities
    ) {
        self.current_guild = current_guild
        self.current_member_roles = current_member_roles
    }
    
    public var admin: Bool {
        self.prioritest_role.admin || current_guild?.OwnerId == CurrentUser.getCurrentUserEntity()?.id
    }
    public var guild_managable: Bool {
        self.prioritest_role.guild_managable || admin
    }
    public var roles_managable: Bool {
        self.prioritest_role.roles_managable || admin
    }
    public var channels_managable: Bool {
        self.prioritest_role.channels_managable || admin
    }
    public var channels_invitable: Bool {
        self.prioritest_role.channels_invitable || admin
    }
    public var kickable: Bool {
        self.prioritest_role.kickable || admin
    }
    public var banable: Bool {
        self.prioritest_role.banable || admin
    }
    public var member_acceptable: Bool {
        self.prioritest_role.member_acceptable || admin
    }
    public var member_managable: Bool {
        self.prioritest_role.member_managable || admin
    }
    public var members_managable: Bool {
        self.prioritest_role.members_managable || admin
    }
    public var channels_viewable: Bool {
        self.prioritest_role.channels_viewable || admin
    }
    public var message_sendable: Bool {
        self.prioritest_role.message_sendable || admin
    }
    public var messages_managable: Bool {
        self.prioritest_role.messages_managable || admin
    }
    public var message_embeddable: Bool {
        self.prioritest_role.message_embeddable || admin
    }
    public var message_attachable: Bool {
        self.prioritest_role.message_attachable || admin
    }
    public var messages_readable: Bool {
        self.prioritest_role.messages_readable || admin
    }
    public var message_mentionable: Bool {
        self.prioritest_role.message_mentionable || admin
    }
    public var message_reactionable: Bool {
        self.prioritest_role.message_reactionable || admin
    }
    public var stream_connectable: Bool {
        self.prioritest_role.stream_connectable || admin
    }
    public var stream_speekable: Bool {
        self.prioritest_role.stream_speekable || admin
    }
    public var stream_livestreamable: Bool {
        self.prioritest_role.stream_livestreamable || admin
    }
    public var movie_selectable: Bool {
        self.prioritest_role.movie_selectable || admin
    }
    public var stream_mutable: Bool {
        self.prioritest_role.stream_mutable || admin
    }
    public var stream_deafenable: Bool {
        self.prioritest_role.stream_deafenable || admin
    }
    public var stream_movable: Bool {
        self.prioritest_role.stream_movable || admin
    }
    public var folders_viewable: Bool {
        self.prioritest_role.folders_viewable || admin
    }
    public var folder_creatable: Bool {
        self.prioritest_role.folder_creatable || admin
    }
    public var folders_managable: Bool {
        self.prioritest_role.folders_managable || admin
    }
    public var files_viewable: Bool {
        self.prioritest_role.files_viewable || admin
    }
    public var file_creatable: Bool {
        self.prioritest_role.file_creatable || admin
    }
    public var files_managable: Bool {
        self.prioritest_role.files_managable || admin
    }
}
