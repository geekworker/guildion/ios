//
//  SearchModel.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/11.
//

import Foundation
import UIKit

class SearchModel: ModelImpl {
    var name: String = ""
    var category: String = ""
}

class SearchModels: ModelsImpl {
    var keyword: String = ""
    var items: [SearchModel] = []
}
