//
//  SyncPlayManagerModel.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/09.
//

import Foundation
import SwiftyJSON

public enum SyncPlayManagerType: String {
    case client = "client"
}

public enum SyncPlayManagerReadyStatus: String {
    case readyToPlay = "readyToPlay"
    case readyToPause = "readyToPause"
    case unknown = "unknown"
}

public enum SyncPlayManagerStatus: String {
    case playing = "playing"
    case paused = "paused"
    case end = "end"
    case readyToPlay = "readyToPlay"
    case readyToPause = "readyToPause"
    case failed = "failed"
    case buffering = "buffering"
    case unknown = "unknown"
    
    var ready_status: SyncPlayManagerReadyStatus {
        switch self {
        case .playing, .readyToPlay: return .readyToPlay
        case .paused, .readyToPause: return .readyToPause
        case .unknown: return .unknown
        default: return .unknown
        }
    }
}

class SyncPlayManagerModel: ModelImpl {
    var uid: String = UUID().uuidString
    var duration: Float = 0
    var loop: Bool = false
    var completed: Bool = false
    var host: Bool = false
    var status: SyncPlayManagerStatus = .unknown
    var ready_status: SyncPlayManagerReadyStatus = .unknown
    var type: SyncPlayManagerType = .client
    var stream: StreamEntity = StreamEntity()
    
    func reset() {
        self.status = .unknown
        self.host = false
        self.completed = false
        self.duration = 0
    }
}

extension SyncPlayManagerModel: Equatable {
    static func == (lhs: SyncPlayManagerModel, rhs: SyncPlayManagerModel) -> Bool{
        let lid = lhs.uid
        let rid = rhs.uid
        return lid == rid
    }
}

extension SyncPlayManagerModel: WebsocketPayloadable {
    typealias Element = SyncPlayManagerModel
    
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "uid": self.uid,
            "duration": self.duration,
            "loop": self.loop,
            "completed": self.completed,
            "host": self.host,
            "status": self.status.rawValue,
            "ready_status": self.ready_status.rawValue,
            "type": self.type.rawValue,
            "stream": self.stream.toWebsocketPayload(),
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> SyncPlayManagerModel.Element? {
        let model = SyncPlayManagerModel()
        model.uid = json["duration"].stringValue
        model.duration = json["duration"].floatValue
        model.loop = json["loop"].boolValue
        model.completed = json["completed"].boolValue
        model.host = json["host"].boolValue
        model.status = SyncPlayManagerStatus.init(rawValue: json["status"].stringValue) ?? .unknown
        model.ready_status = SyncPlayManagerReadyStatus.init(rawValue: json["ready_status"].stringValue) ?? .unknown
        model.type = SyncPlayManagerType.init(rawValue: json["type"].stringValue) ?? .client
        model.stream = StreamEntity.jsonable(json["stream"]) ?? StreamEntity()
        return model
    }
}
