//
//  SearchDomainType.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/24.
//

import Foundation
import UIKit

public enum SearchDomainType: Int, CaseIterable {
    case google
    case youtube
    
    var image: UIImage {
        switch self {
        case .google: return R.image.gLogo()!.withRenderingMode(.alwaysOriginal)
        case .youtube: return R.image.yt_icon_rgb()!.withRenderingMode(.alwaysOriginal)
        }
    }
    
    var urlString: String {
        switch self {
        case .google: return "https://google.com"
        case .youtube: return "https://youtube.com"
        }
    }
    
    var url: URL {
        return URL(string: self.urlString)!
    }
    
    func search(q: String) -> URL {
        var urlString: String = ""
        //let converted = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        switch self {
        case .google: urlString = "\(self.urlString)/search?q=\(q)"
        case .youtube: urlString = "\(self.urlString)/results?search_query=\(q)"
        }
        let encodeUrlString: String = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? urlString
        return URL(string: encodeUrlString)!
    }
    
    
    init(url: URL) {
        self = SearchDomainType.allCases.filter({ url.host?.contains($0.url.host ?? "") ?? false }).first ?? .google
    }
}
