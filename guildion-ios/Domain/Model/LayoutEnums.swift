//
//  HeaderFooterLayout.swift
//  ImageViewer
//
//  Created by Kristian Angyal on 08/03/2016.
//  Copyright © 2016 MailOnline. All rights reserved.
//

import UIKit

public typealias ImageViewerMarginLeft = CGFloat
public typealias ImageViewerMarginRight = CGFloat
public typealias ImageViewerMarginTop = CGFloat
public typealias ImageViewerMarginBottom = CGFloat

/// Represents possible layouts for the close button
public enum ImageViewerButtonLayout {

    case pinLeft(ImageViewerMarginTop, ImageViewerMarginLeft)
    case pinRight(ImageViewerMarginTop, ImageViewerMarginRight)
}

/// Represents various possible layouts for the header
public enum ImageViewerHeaderLayout {

    case pinLeft(ImageViewerMarginTop, ImageViewerMarginLeft)
    case pinRight(ImageViewerMarginTop, ImageViewerMarginRight)
    case pinBoth(ImageViewerMarginTop, ImageViewerMarginLeft, ImageViewerMarginRight)
    case center(ImageViewerMarginTop)
}

/// Represents various possible layouts for the footer
public enum ImageViewerFooterLayout {

    case pinLeft(ImageViewerMarginBottom, ImageViewerMarginLeft)
    case pinRight(ImageViewerMarginBottom, ImageViewerMarginRight)
    case pinBoth(ImageViewerMarginBottom, ImageViewerMarginLeft, ImageViewerMarginRight)
    case center(ImageViewerMarginBottom)
}
