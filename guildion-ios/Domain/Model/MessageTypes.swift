//
//  MessageTypes.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import MessageKit

enum MessageTypes: String, CaseIterable {
    case Text
    case AttributedText
    case Photo
    case Video
    case Audio
    case Emoji
    case Location
    case Url
    case Phone
    case Custom
    case ShareContact
}
