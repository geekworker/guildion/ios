//
//  ImagePagingItem.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import UIKit
import Parchment

struct ImagePagingModel: PagingItem, Hashable, Comparable {
    
    var index: Int
    var title: String
    var headerImageURL: URL?
    var imageable_id: Int
    var imageable_type: String
    
    static func < (lhs: ImagePagingModel, rhs: ImagePagingModel) -> Bool {
        return lhs.index < rhs.index
    }
    
    static func > (lhs: ImagePagingModel, rhs: ImagePagingModel) -> Bool {
        return lhs.index < rhs.index
    }
    
    static func == (lhs: ImagePagingModel, rhs: ImagePagingModel) -> Bool {
        return lhs.imageable_id == rhs.imageable_id && lhs.imageable_type == rhs.imageable_type
    }
}

struct ImagePagingModels {
    var items: [ImagePagingModel]
}
