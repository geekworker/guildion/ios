//
//  StreamChannelPermissionModel.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/14.
//

import Foundation

protocol StreamChannelPermissionGuildDelegate: class {
    var current_member_roles: MemberRoleEntities { get set }
    var admin: Bool { get }
    var guild_managable: Bool { get }
    var roles_managable: Bool { get }
    var channels_managable: Bool { get }
    var channels_invitable: Bool { get }
    var kickable: Bool { get }
    var banable: Bool { get }
    var member_acceptable: Bool { get }
    var member_managable: Bool { get }
    var members_managable: Bool { get }
    var channels_viewable: Bool { get }
    var message_sendable: Bool { get }
    var messages_managable: Bool { get }
    var message_embeddable: Bool { get }
    var message_attachable: Bool { get }
    var messages_readable: Bool { get }
    var message_mentionable: Bool { get }
    var message_reactionable: Bool { get }
    var stream_connectable: Bool { get }
    var stream_speekable: Bool { get }
    var stream_livestreamable: Bool { get }
    var movie_selectable: Bool { get }
    var stream_mutable: Bool { get }
    var stream_deafenable: Bool { get }
    var stream_movable: Bool { get }
    var folders_viewable: Bool { get }
    var folder_creatable: Bool { get }
    var folders_managable: Bool { get }
    var files_viewable: Bool { get }
    var file_creatable: Bool { get }
    var files_managable: Bool { get }
}

class StreamChannelPermission: ModelImpl {
    weak var guildDelegate: StreamChannelPermissionGuildDelegate?
    var current_stream_channel_roles: StreamChannelRoleEntities = StreamChannelRoleEntities()
    public var roles: [StreamChannelRoleEntity] {
        guard let prioritest = current_stream_channel_roles.filter({ $0.Role != nil && guildDelegate?.current_member_roles.toRoles().contains($0.Role!) ?? false }).max(by: { $0.Role!.priority < $1.Role!.priority }) else { return [] }
        return [prioritest]
    }
    
    init() {}
    
    init(current_stream_channel_roles: StreamChannelRoleEntities) {
        self.current_stream_channel_roles = current_stream_channel_roles
    }
    
    public var roles_managable: Bool {
        guard roles.count > 0 else { return guildDelegate?.roles_managable ?? false }
        return roles.filter({ $0.roles_managable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var channel_managable: Bool {
        guard roles.count > 0 else { return guildDelegate?.channels_managable ?? false }
        return roles.filter({ $0.channel_managable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var channel_members_managable: Bool {
        guard roles.count > 0 else { return guildDelegate?.channels_managable ?? false }
        return roles.filter({ $0.channel_members_managable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var kickable: Bool {
        guard roles.count > 0 else { return guildDelegate?.kickable ?? false }
        return roles.filter({ $0.kickable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var invitable: Bool {
        guard roles.count > 0 else { return guildDelegate?.channels_invitable ?? false }
        return roles.filter({ $0.invitable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var channel_viewable: Bool {
        guard roles.count > 0 else { return guildDelegate?.channels_viewable ?? false }
        return roles.filter({ $0.channel_viewable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var message_sendable: Bool {
        guard roles.count > 0 else { return guildDelegate?.message_sendable ?? false }
        return roles.filter({ $0.message_sendable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var messages_managable: Bool {
        guard roles.count > 0 else { return guildDelegate?.messages_managable ?? false }
        return roles.filter({ $0.messages_managable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var message_embeddable: Bool {
        guard roles.count > 0 else { return guildDelegate?.message_embeddable ?? false }
        return roles.filter({ $0.message_embeddable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var message_attachable: Bool {
        guard roles.count > 0 else { return guildDelegate?.message_attachable ?? false }
        return roles.filter({ $0.message_attachable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var messages_readable: Bool {
        guard roles.count > 0 else { return guildDelegate?.messages_readable ?? false }
        return roles.filter({ $0.messages_readable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var message_mentionable: Bool {
        guard roles.count > 0 else { return guildDelegate?.message_mentionable ?? false }
        return roles.filter({ $0.message_mentionable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var message_reactionable: Bool {
        guard roles.count > 0 else { return guildDelegate?.message_reactionable ?? false }
        return roles.filter({ $0.message_reactionable ?? false }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var stream_connectable: Bool {
        guard roles.count > 0 else { return guildDelegate?.stream_connectable ?? false }
        return roles.filter({ $0.stream_connectable }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var stream_speekable: Bool {
        guard roles.count > 0 else { return guildDelegate?.stream_speekable ?? false }
        return roles.filter({ $0.stream_speekable }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var stream_livestreamable: Bool {
        guard roles.count > 0 else { return guildDelegate?.stream_livestreamable ?? false }
        return roles.filter({ $0.stream_livestreamable }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var movie_selectable: Bool {
        guard roles.count > 0 else { return guildDelegate?.movie_selectable ?? false }
        return roles.filter({ $0.movie_selectable }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var stream_mutable: Bool {
        guard roles.count > 0 else { return guildDelegate?.stream_mutable ?? false }
        return roles.filter({ $0.stream_mutable }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var stream_deafenable: Bool {
        guard roles.count > 0 else { return guildDelegate?.stream_deafenable ?? false }
        return roles.filter({ $0.stream_deafenable }).count > 0 || (guildDelegate?.admin ?? false)
    }
    public var stream_movable: Bool {
        guard roles.count > 0 else { return guildDelegate?.stream_movable ?? false }
        return roles.filter({ $0.stream_movable }).count > 0 || (guildDelegate?.admin ?? false)
    }
}
