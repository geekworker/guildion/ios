//
//  OGPModel.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/28.
//

import Foundation
import UIKit
import Hydra
import ObjectMapper

class OGPModel: ModelImpl, Mappable {
    public var url: URL = URL(string: Constant.APP_URL)!
    public var host: String = ""
    public var title: String = ""
    public var description: String = ""
    public var image: String = ""
    
    init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
    }
    
//    func asyncInit(url: URL) -> [Promise<Void>] {
//        self.url = url
//        return [self.fetchOGP()]
//    }
//    
//    func fetchOGP() -> Promise<Void> {
//        return Promise<Void> { resolve, reject, _ in
//            OpenGraph.fetch(url: self.url, completion: { result in
//                self.host = self.url.host ?? ""
//                switch result {
//                case .success(let og):
//                    self.title = og[.title] ?? ""
//                    self.image = og[.image] ?? ""
//                    self.description = og[.description] ?? ""
//                    resolve(())
//                case .failure(_): resolve(())
//                }
//            })
//        }
//    }
}

class OGPModels: ModelsImpl {
    var items: [OGPModel] = []
    
//    func asyncInit(urls: [URL]) -> [Promise<Void>] {
//        guard urls.count > 0 else { return [] }
//        self.items = []
//        return urls.compactMap({
//            let entity = Element()
//            self.items.append(entity)
//            return entity.asyncInit(url: $0)
//        }).reduce([] as [Promise<Void>], +)
//    }
//    
//    func asyncInit(text: String) -> [Promise<Void>] {
//        let urls = text.extractURL()
//        guard urls.count > 0 else { return [] }
//        self.items = []
//        return urls.compactMap({
//            let entity = Element()
//            self.items.append(entity)
//            return entity.asyncInit(url: $0)
//        }).reduce([] as [Promise<Void>], +)
//    }
}
