//
//  CustomMessageTypes.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/19.
//

import Foundation
import UIKit
import MessageKit

enum CustomMessageTypes: String, CaseIterable, Equatable {
    private static func ==(lhs: String, rhs: CustomMessageTypes) -> Bool {
        return lhs == rhs.rawValue
    }
    case text
    case headLess
    case timeSection
    case media
    case audio
    case ogp
    case reaction
    case channelog
    case failed
}

