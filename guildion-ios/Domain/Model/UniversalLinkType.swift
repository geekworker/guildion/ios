//
//  UniversalLinkType.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/15.
//

import Foundation
import UIKit

enum UniversalLinkType {
    case guildWelcome(uid: String)
    case channel(guild_uid: String, channel_uid: String)
    case other
    
    init(url: URL) {
        guard let components = URLComponents(string: url.absoluteString), let host = components.host, let scheme = components.scheme, scheme == Constant.IOS_URL_SCHEME || host == Constant.APP_DOMAIN else {
            self = .other
            return
        }
        if components.path.hasPrefix("/guild/welcome") || components.path.hasPrefix("guild/welcome") {
            self = .guildWelcome(
                uid: components.path
                    .replacingOccurrences(of: "/guild/welcome/", with: "")
                    .replacingOccurrences(of: "guild/welcome/", with: "")
            )
        } else if components.path.hasPrefix("/guild") || components.path.hasPrefix("guild") {
            let splits = components.path.split(separator: "/")
            guard splits.count == 3 else { self = .other; return }
            self = .channel(guild_uid: String(splits[1]), channel_uid: String(splits[2]))
        } else {
            self = .other
        }
    }
}
