//
//  FileReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct FileDispatcher {
    struct SET_MEMBER_FILES: Action {
        var member_uid: String
        var payload: FileEntities
    }
    struct ADD_MEMBER_FILES: Action {
        var member_uid: String
        var payload: FileEntities
    }
    struct INSERT_MEMBER_FILES: Action {
        var member_uid: String
        var payload: FileEntities
    }
    struct RESET_MEMBER_FILES: Action {
        var payload: String
    }
    struct RESET_ALL_MEMBER_FILES: Action {
        var payload: String
    }
    
    struct SET_GUILD_FILES: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    struct ADD_GUILD_FILES: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    struct INSERT_GUILD_FILES: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    struct RESET_GUILD_FILES: Action {
        var payload: String
    }
    struct RESET_ALL_GUILD_FILES: Action {
    }
    
    struct SET_GUILD_CHACHE_FILES: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    struct ADD_GUILD_CHACHE_FILES: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    struct INSERT_GUILD_CHACHE_FILES: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    struct REMOVE_GUILD_CHACHE_FILES: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    struct RESET_GUILD_CHACHE_FILES: Action {
        var payload: String
    }
    
    struct SET_USER_FILES: Action {
        var payload: FileEntities
    }
    struct ADD_USER_FILES: Action {
        var payload: FileEntities
    }
    struct INSERT_USER_FILES: Action {
        var payload: FileEntities
    }
    struct RESET_USER_FILES: Action {
        var payload: String
    }
    
    struct CREATEDS: Action {
        var guild_uid: String
        var payload: FileEntities
    }
    
    struct SET_CACHES: Action {
        var payload: FileEntities
    }
    struct SET_DELETES: Action {
        var payload: FileEntities
    }
    struct RESET_CACHES: Action {
        var payload: FileEntities
    }
    struct RESET_DELETES: Action {
        var payload: FileEntities
    }
    public static func getCache(id: Int, state: FileState) -> FileEntity? {
        return state.caches?.filter({ $0.id == id }).first
    };

    public static func getDelete(id: Int, state: FileState) -> FileEntity? {
        return state.deletes?.filter({ $0.id == id }).first
    };

    public static func bind(content: FileEntity, state: FileState) -> FileEntity? {
        if getDelete(id: content.id ?? 0, state: state) != nil { return nil }
        return getCache(id: content.id ?? 0, state: state) != nil ? getCache(id: content.id ?? 0, state: state) : content
    };
    
    public static func binds(contents: FileEntities, state: FileState) -> FileEntities {
        contents.items = contents.compactMap({ bind(content: $0, state: state) })
        return contents
    };
}

func fileReducer(action: Action, state: FileState?) -> FileState {
    var state = state ?? FileState()
    
    switch action {
    case let action as FileDispatcher.SET_MEMBER_FILES:
        state.memberFiles[action.member_uid] = action.payload
        break
    
    case let action as FileDispatcher.ADD_MEMBER_FILES:
        guard action.payload.items.count != 0 else { return state }
        state.memberFiles[action.member_uid]?.items += action.payload.items
        state.memberFiles[action.member_uid]?.unified()
        break
    
    case let action as FileDispatcher.INSERT_MEMBER_FILES:
        guard action.payload.count != 0, let before = state.memberFiles[action.member_uid]?.items else { return state }
        state.memberFiles[action.member_uid]?.items = action.payload.items
        state.memberFiles[action.member_uid]?.items += before
        state.memberFiles[action.member_uid]?.unified()
        break
        
    case let action as FileDispatcher.RESET_MEMBER_FILES:
        state.memberFiles[action.payload] = FileEntities()
        break
        
    case _ as FileDispatcher.RESET_ALL_MEMBER_FILES:
        state.memberFiles = [:]
        break
        
    case let action as FileDispatcher.SET_GUILD_FILES:
        state.guildFiles[action.guild_uid] = action.payload
        break
    
    case let action as FileDispatcher.ADD_GUILD_FILES:
        guard action.payload.count != 0 else { return state }
        state.guildFiles[action.guild_uid]?.items += action.payload.items
        state.guildFiles[action.guild_uid]?.items = state.guildFiles[action.guild_uid]?.items.filter({ $0.id != nil && $0.id != 0 }) ?? []
        break
    
    case let action as FileDispatcher.INSERT_GUILD_FILES:
        guard action.payload.count != 0, let before = state.guildFiles[action.guild_uid]?.items else { return state }
        state.guildFiles[action.guild_uid]?.items = action.payload.items
        state.guildFiles[action.guild_uid]?.items += before
        state.guildFiles[action.guild_uid]?.items = state.guildFiles[action.guild_uid]?.items.filter({ $0.id != nil && $0.id != 0 }) ?? []
        break
        
    case let action as FileDispatcher.RESET_GUILD_FILES:
        state.guildFiles[action.payload] = FileEntities()
        break
        
    case _ as FileDispatcher.RESET_ALL_GUILD_FILES:
        state.guildFiles = [:]
        break
        
    case let action as FileDispatcher.SET_GUILD_CHACHE_FILES:
        state.guildCacheFiles[action.guild_uid] = action.payload
        break
    
    case let action as FileDispatcher.ADD_GUILD_CHACHE_FILES:
        guard action.payload.count != 0 else { return state }
        if state.guildCacheFiles[action.guild_uid] == nil {
            state.guildCacheFiles[action.guild_uid] = FileEntities()
        }
        state.guildCacheFiles[action.guild_uid]?.items += action.payload.items
        break
    
    case let action as FileDispatcher.INSERT_GUILD_CHACHE_FILES:
        guard action.payload.count != 0 else { return state }
        guard let before = state.guildCacheFiles[action.guild_uid]?.items else {
            state.guildCacheFiles[action.guild_uid] = action.payload
            return state
        }
        state.guildCacheFiles[action.guild_uid]?.items = action.payload.items
        state.guildCacheFiles[action.guild_uid]?.items += before
        break
        
    case let action as FileDispatcher.REMOVE_GUILD_CHACHE_FILES:
        guard action.payload.count != 0, let before = state.guildCacheFiles[action.guild_uid]?.items else { return state }
        state.guildCacheFiles[action.guild_uid]?.items = before.filter({ !action.payload.contains($0) })
        break
        
    case let action as FileDispatcher.RESET_GUILD_CHACHE_FILES:
        state.guildCacheFiles[action.payload] = FileEntities()
        break
        
    case let action as FileDispatcher.SET_USER_FILES:
        state.userFiles = action.payload
        break
    
    case let action as FileDispatcher.ADD_USER_FILES:
        guard action.payload.count != 0 else { return state }
        state.userFiles?.items += action.payload.items
        break
    
    case let action as FileDispatcher.INSERT_USER_FILES:
        guard action.payload.count != 0 else { return state }
        action.payload.items += state.userFiles?.items ?? []
        state.userFiles?.items = action.payload.items
        break
        
    case _ as FileDispatcher.RESET_USER_FILES:
        state.userFiles = FileEntities()
        break
        
    case let action as FileDispatcher.SET_CACHES:
        guard action.payload.items.count != 0 else { return state }
        state.caches = action.payload
        break
    
    case let action as FileDispatcher.SET_DELETES:
        guard action.payload.items.count != 0 else { return state }
        state.deletes = action.payload
        break
        
    case _ as FileDispatcher.RESET_CACHES:
        state.caches = nil
        break
        
    case _ as FileDispatcher.RESET_DELETES:
        state.deletes = nil
        break
        
    case let action as FileDispatcher.CREATEDS:
        guard action.payload.items.count > 0 else { return state }
        if let files = state.guildFiles[action.guild_uid] {
            for file in files.items.enumerated() {
                if let newFile = action.payload.items.filter({ $0 == file.element }).first {
                    files.items[file.offset].id = newFile.id
                }
            }
            files.unified()
        }
        
        break
        
    default:
        break
    }
    return state
}

struct FileState: StateType {
    var memberFiles: [String: FileEntities] = [:]
    var guildFiles: [String: FileEntities] = [:]
    var guildCacheFiles: [String: FileEntities] = [:]
    var userFiles: FileEntities? = FileEntities()
    var caches: FileEntities? = FileEntities()
    var deletes: FileEntities? = FileEntities()
}

var fileStore = Store(
    reducer: fileReducer,
    state: FileState()
)
