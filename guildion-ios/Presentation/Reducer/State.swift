//
//  State.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import ReSwift

struct State: StateType {
    var app: AppState
    var auth: AuthState
    var category: CategoryState
    var folder: FolderState
    var dmChannel: DMChannelState
    var file: FileState
    var fileReference: FileReferenceState
    var guild: GuildState
    var search: SearchState
    var session: SessionState
    var streamChannel: StreamChannelState
    var stream: StreamState
    var textChannel: TextChannelState
    var tag: TagState
    var notification: NotificationState
    var message: MessageState
    var member: MemberState
    var user: UserState
    var role: RoleState
}


