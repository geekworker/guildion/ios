//
//  AppReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import ReSwift

struct AppDispatcher {
    
    struct SET_ERRORS: Action {
        var payload: [String :Error]
    }
    struct SET_SUCCESSES: Action {
        var payload: [String :String]
    }
    struct SET_WARNINGS: Action {
        var payload: [String :String]
    }
    
    struct RESET_ERRORS: Action {
    }
    struct RESET_SUCCESSES: Action {
    }
    struct RESET_WARNINGS: Action {
    }
    
    struct REMOVE_ERROR: Action {
        var payload: String
    }
    struct REMOVE_SUCCESS: Action {
        var payload: String
    }
    struct REMOVE_WARNING: Action {
        var payload: String
    }
    
    struct SET_CURRENT_GUILD: Action {
        var payload: GuildEntity
    }
    struct RESET_CURRENT_GUILD: Action {
    }
    
    struct SET_CURRENT_CHANNEL: Action {
        var payload: MessageableEntity
    }
    struct RESET_CURRENT_CHANNEL: Action {
    }
    
    struct SET_PROGRESS: Action {
        var payload: CGFloat
    }
    struct FINISH_PROGRESS: Action {
    }
    struct RESET_PROGRESS: Action {
    }
}

func appReducer(action: Action, state: AppState?) -> AppState {
    var state = state ?? AppState()
    
    switch action {
        
    case let action as AppDispatcher.SET_ERRORS:
        state.errors?.merge(contentsOf: action.payload)
        break
        
    case let action as AppDispatcher.SET_SUCCESSES:
        state.successes?.merge(contentsOf: action.payload)
        break
    
    case let action as AppDispatcher.SET_WARNINGS:
        state.warnings?.merge(contentsOf: action.payload)
        break
        
    case _ as AppDispatcher.RESET_ERRORS:
        state.errors = [:]
        break
        
    case _ as AppDispatcher.RESET_SUCCESSES:
        state.successes = [:]
        break
        
    case _ as AppDispatcher.RESET_WARNINGS:
        state.warnings = [:]
        break
        
    case let action as AppDispatcher.REMOVE_ERROR:
        state.errors?.removeValue(forKey: action.payload)
        break
        
    case let action as AppDispatcher.REMOVE_SUCCESS:
        state.successes?.removeValue(forKey: action.payload)
        break
        
    case let action as AppDispatcher.REMOVE_WARNING:
        state.warnings?.removeValue(forKey: action.payload)
        break
        
    case let action as AppDispatcher.SET_CURRENT_GUILD:
        state.current_guild = action.payload
        CurrentGuild.setCurrentGuild(entity: action.payload)
        break
        
    case _ as AppDispatcher.RESET_CURRENT_GUILD:
        state.current_guild = nil
        break
        
    case let action as AppDispatcher.SET_CURRENT_CHANNEL:
        state.current_channel = action.payload
        CurrentChannel.setCurrentChannel(entity: action.payload)
        break
        
    case _ as AppDispatcher.RESET_CURRENT_CHANNEL:
        state.current_channel = nil
        break
    
    case let action as AppDispatcher.SET_PROGRESS:
        state.progressing = true
        state.progress = action.payload
        break
        
    case _ as AppDispatcher.RESET_PROGRESS:
        state.progressing = false
        state.progress = 0
        break
        
    case _ as AppDispatcher.FINISH_PROGRESS:
        state.progressing = false
        state.progress = 1
        break
    
    default:
        break
    }
    return state
}

struct AppState: StateType {
    var errors: [String: Error]? = [:]
    var successes: [String: String]? = [:]
    var warnings: [String: String]? = [:]
    var current_guild: GuildEntity?
    var current_channel: MessageableEntity?
    var progressing: Bool = false
    var progress: CGFloat = 0.0
}

var appStore = Store(
    reducer: appReducer,
    state: AppState()
)
