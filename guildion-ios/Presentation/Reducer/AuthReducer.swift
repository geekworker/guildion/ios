//
//  AuthReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct AuthDispatcher {
    struct SET_IDENTITY: Action {
        var payload: IdentityEntity?
    }
    struct SET_CURRENT_USER: Action {
        var payload: UserEntity?
    }
    struct LOGINED: Action {
    }
    struct LOGOUTED: Action {
    }
    struct SYNC_CURRENT_USER_END: Action {
    }
}

func authReducer(action: Action, state: AuthState?) -> AuthState {
    var state = state ?? AuthState()
    
    switch action {
    case let action as AuthDispatcher.SET_IDENTITY:
        state.identity = action.payload
        break
    case let action as AuthDispatcher.SET_CURRENT_USER:
        state.current_user = action.payload
        break
    case _ as AuthDispatcher.LOGINED:
        state.logined = true
        break
    case _ as AuthDispatcher.LOGOUTED:
        state.current_user = nil
        state.logined = false
        break
    case _ as AuthDispatcher.SYNC_CURRENT_USER_END:
        state.synced = true
        break
    default:
        break
    }
    return state
}

struct AuthState: StateType {
    var identity: IdentityEntity?
    var logined: Bool? = CurrentUser.getCurrentUserEntity() != nil
    var current_user: UserEntity? = CurrentUser.getCurrentUserEntity()
    var synced: Bool?
}

var authStore = Store(
    reducer: authReducer,
    state: AuthState()
)

