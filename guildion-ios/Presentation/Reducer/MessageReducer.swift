//
//  MessageReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct MessageDispatcher {
    struct SET_CHANNEL_MESSAGES: Action {
        var channel_uid: String
        var payload: MessageEntities
    }
    struct ADD_CHANNEL_MESSAGES: Action {
        var channel_uid: String
        var payload: MessageEntities
    }
    struct INSERT_CHANNEL_MESSAGES: Action {
        var channel_uid: String
        var payload: MessageEntities
    }
    struct RESET_CHANNEL_MESSAGES: Action {
        var payload: String
    }
    struct RESET_ALL_CHANNEL_MESSAGES: Action {
        var payload: String
    }
    struct CREATED: Action {
        var channel_uid: String
        var payload: MessageEntity
    }
    
    struct SET_CHANNEL_CHACHE_MESSAGES: Action {
        var channel_uid: String
        var payload: MessageEntities
    }
    struct ADD_CHANNEL_CHACHE_MESSAGES: Action {
        var channel_uid: String
        var payload: MessageEntities
    }
    struct INSERT_CHANNEL_CHACHE_MESSAGES: Action {
        var channel_uid: String
        var payload: MessageEntities
    }
    struct REMOVE_CHANNEL_CHACHE_MESSAGES: Action {
        var channel_uid: String
    }
    struct RESET_CHANNEL_CHACHE_MESSAGES: Action {
        var payload: String
    }
    
    struct SET_CACHES: Action {
        var payload: MessageEntities
    }
    struct SET_DELETES: Action {
        var payload: MessageEntities
    }
    struct RESET_CACHES: Action {
        var payload: MessageEntities
    }
    struct RESET_DELETES: Action {
        var payload: MessageEntities
    }
    public static func getCache(id: Int, state: MessageState) -> MessageEntity? {
        return state.caches?.filter({ $0.id == id }).first
    };

    public static func getDelete(id: Int, state: MessageState) -> MessageEntity? {
        return state.deletes?.filter({ $0.id == id }).first
    };

    public static func bind(content: MessageEntity, state: MessageState) -> MessageEntity? {
        if getDelete(id: content.id ?? 0, state: state) != nil { return nil }
        return getCache(id: content.id ?? 0, state: state) != nil ? getCache(id: content.id ?? 0, state: state) : content
    };
    
    public static func binds(contents: MessageEntities, state: MessageState) -> MessageEntities {
        contents.items = contents.compactMap({ bind(content: $0, state: state) })
        return contents
    };
}

func messageReducer(action: Action, state: MessageState?) -> MessageState {
    var state = state ?? MessageState()
    
    switch action {
    case let action as MessageDispatcher.SET_CHANNEL_MESSAGES:
        state.channelMessages[action.channel_uid] = action.payload
        break
    
    case let action as MessageDispatcher.ADD_CHANNEL_MESSAGES:
        guard action.payload.items.count != 0 else { return state }
        state.channelMessages[action.channel_uid]?.items += action.payload.items
        state.channelMessages[action.channel_uid]?.items = state.channelMessages[action.channel_uid]?.items.filter({ $0.id != nil && $0.id != 0 }) ?? []
        break
    
    case let action as MessageDispatcher.INSERT_CHANNEL_MESSAGES:
        if state.channelMessages[action.channel_uid] == nil {
            state.channelMessages[action.channel_uid] = MessageEntities()
        }
        guard action.payload.items.count != 0, let before = state.channelMessages[action.channel_uid]?.items else { return state }
        state.channelMessages[action.channel_uid]?.items = action.payload.items + before
        state.channelMessages[action.channel_uid]?.items = state.channelMessages[action.channel_uid]?.items.filter({ $0.id != nil && $0.id != 0 }) ?? []
        break
        
    case let action as MessageDispatcher.RESET_CHANNEL_MESSAGES:
        state.channelMessages[action.payload] = MessageEntities()
        break
        
    case _ as MessageDispatcher.RESET_ALL_CHANNEL_MESSAGES:
        state.channelMessages = [:]
        break
        
    case let action as MessageDispatcher.SET_CHANNEL_CHACHE_MESSAGES:
        if state.channelCacheMessages[action.channel_uid] == nil {
            state.channelCacheMessages[action.channel_uid] = MessageEntities()
        }
        state.channelCacheMessages[action.channel_uid] = action.payload
        if state.channelMessages[action.channel_uid] == nil {
            state.channelMessages[action.channel_uid] = MessageEntities()
        }
        if let repositories = state.channelMessages[action.channel_uid], let caches = state.channelCacheMessages[action.channel_uid] {
            repositories.items += caches.compactMap {
                return !repositories.contains($0) ? $0 : nil
            }
        }
        break
    
    case let action as MessageDispatcher.ADD_CHANNEL_CHACHE_MESSAGES:
        guard action.payload.count != 0 else { return state }
        if state.channelCacheMessages[action.channel_uid] == nil {
            state.channelCacheMessages[action.channel_uid] = MessageEntities()
        }
        state.channelCacheMessages[action.channel_uid]?.items += action.payload.items
        if state.channelMessages[action.channel_uid] == nil {
            state.channelMessages[action.channel_uid] = MessageEntities()
        }
        if let repositories = state.channelMessages[action.channel_uid], let caches = state.channelCacheMessages[action.channel_uid] {
            repositories.items += caches.compactMap {
                return !repositories.contains($0) ? $0 : nil
            }
        }
        break
    
    case let action as MessageDispatcher.INSERT_CHANNEL_CHACHE_MESSAGES:
        guard action.payload.count != 0 else { return state }
        if state.channelCacheMessages[action.channel_uid] == nil {
            state.channelCacheMessages[action.channel_uid] = MessageEntities()
        }
        guard let before = state.channelCacheMessages[action.channel_uid]?.items else {
            state.channelCacheMessages[action.channel_uid] = action.payload
            return state
        }
        state.channelCacheMessages[action.channel_uid]?.items = action.payload.items
        state.channelCacheMessages[action.channel_uid]?.items += before
        if state.channelMessages[action.channel_uid] == nil {
            state.channelMessages[action.channel_uid] = MessageEntities()
        }
        if let repositories = state.channelMessages[action.channel_uid], let caches = state.channelCacheMessages[action.channel_uid] {
            repositories.items += caches.compactMap {
                return !repositories.contains($0) ? $0 : nil
            }
        }
        break
        
    case let action as MessageDispatcher.REMOVE_CHANNEL_CHACHE_MESSAGES:
        if state.channelCacheMessages[action.channel_uid] == nil {
            state.channelCacheMessages[action.channel_uid] = MessageEntities()
        }
        guard let before = state.channelCacheMessages[action.channel_uid]?.items else { return state }
        state.channelCacheMessages[action.channel_uid]?.items = before.filter({ $0.Files?.filter({ $0.progress ?? 0 < 1 && $0.provider == .other }).count ?? 0 > 0 })
        if state.channelMessages[action.channel_uid] == nil {
            state.channelMessages[action.channel_uid] = MessageEntities()
        }
        if let repositories = state.channelMessages[action.channel_uid], let caches = state.channelCacheMessages[action.channel_uid] {
            repositories.items += caches.compactMap {
                return !repositories.contains($0) ? $0 : nil
            }
        }
        break
    
    case _ as MessageDispatcher.CREATED:
        break
        
    case let action as MessageDispatcher.SET_CACHES:
        guard action.payload.items.count != 0 else { return state }
        state.caches = action.payload
        break
    
    case let action as MessageDispatcher.SET_DELETES:
        guard action.payload.items.count != 0 else { return state }
        state.deletes = action.payload
        break
        
    case _ as MessageDispatcher.RESET_CACHES:
        state.caches = nil
        break
        
    case _ as MessageDispatcher.RESET_DELETES:
        state.deletes = nil
        break
        
    default:
        break
    }
    return state
}

struct MessageState: StateType {
    var channelMessages: [String: MessageEntities] = [:]
    var channelCacheMessages: [String: MessageEntities] = [:]
    var caches: MessageEntities? = MessageEntities()
    var deletes: MessageEntities? = MessageEntities()
}

var messageStore = Store(
    reducer: messageReducer,
    state: MessageState()
)
