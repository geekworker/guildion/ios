//
//  FolderReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct FolderDispatcher {
    struct SET_GUILD_GALLERIES: Action {
        var guild_uid: String
        var payload: FolderEntities
    }
    struct ADD_GUILD_GALLERIES: Action {
        var guild_uid: String
        var payload: FolderEntities
    }
    struct INSERT_GUILD_GALLERIES: Action {
        var guild_uid: String
        var payload: FolderEntities
    }
    struct RESET_GUILD_GALLERIES: Action {
        var payload: String
    }
    struct RESET_ALL_GUILD_GALLERIES: Action {
        var payload: String
    }
    
    struct SET_SHOW_GALLERY: Action {
        var folder_uid: String
        var payload: FolderEntity
    }
    struct ADD_SHOW_GALLERY: Action {
        var folder_uid: String
        var payload: FolderEntity
    }
    struct INSERT_SHOW_GALLERY: Action {
        var folder_uid: String
        var payload: FolderEntity
    }
    struct RESET_SHOW_GALLERY: Action {
        var payload: String
    }
    struct RESET_ALL_SHOW_GALLERY: Action {
        var payload: String
    }
    
    
    struct SET_REFERENCES: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
    struct ADD_REFERENCES: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
    struct INSERT_REFERENCES: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
    
    struct REFERENCES_CREATED: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
}

func folderReducer(action: Action, state: FolderState?) -> FolderState {
    var state = state ?? FolderState()
    
    switch action {
    case let action as FolderDispatcher.SET_GUILD_GALLERIES:
        state.guildFolders[action.guild_uid] = action.payload
        break
    
    case let action as FolderDispatcher.ADD_GUILD_GALLERIES:
        guard action.payload.items.count != 0 else { return state }
        state.guildFolders[action.guild_uid]?.items += action.payload.items
        state.guildFolders[action.guild_uid]?.unified()
        break
    
    case let action as FolderDispatcher.INSERT_GUILD_GALLERIES:
        guard action.payload.items.count != 0, let before = state.guildFolders[action.guild_uid]?.items else { return state }
        state.guildFolders[action.guild_uid]?.items = action.payload.items
        state.guildFolders[action.guild_uid]?.items += before
        state.guildFolders[action.guild_uid]?.unified()
        break
        
    case let action as FolderDispatcher.RESET_GUILD_GALLERIES:
        state.guildFolders[action.payload] = FolderEntities()
        break
        
    case _ as FolderDispatcher.RESET_ALL_GUILD_GALLERIES:
        state.guildFolders = [:]
        break
        
    case let action as FolderDispatcher.SET_SHOW_GALLERY:
        state.showFolders[action.folder_uid] = action.payload
        break
        
    case let action as FolderDispatcher.RESET_SHOW_GALLERY:
        state.showFolders[action.payload] = FolderEntity()
        break
        
    case _ as FolderDispatcher.RESET_ALL_SHOW_GALLERY:
        state.showFolders = [:]
        break
        
    case let action as FolderDispatcher.SET_REFERENCES:
        for (_, element) in state.guildFolders {
            for folder in element.items.filter({ $0.uid == action.folder_uid }) {
                folder.FileReferences?.items = action.payload.items
                folder.FileReferences?.unified()
            }
        }
        for (_, element) in state.showFolders {
            if element.uid == action.folder_uid {
                element.FileReferences?.items = action.payload.items
                element.FileReferences?.unified()
            }
        }
        break
        
    case let action as FolderDispatcher.ADD_REFERENCES:
        for (_, element) in state.guildFolders {
            for folder in element.items.filter({ $0.uid == action.folder_uid }) {
                folder.FileReferences?.items += action.payload.items
                folder.FileReferences?.unified()
            }
        }
        for (_, element) in state.showFolders {
            if element.uid == action.folder_uid {
                element.FileReferences?.items += action.payload.items
                element.FileReferences?.unified()
            }
        }
        break
        
    case let action as FolderDispatcher.INSERT_REFERENCES:
        for (_, element) in state.guildFolders {
            for folder in element.items.filter({ $0.uid == action.folder_uid }) {
                folder.FileReferences?.items = action.payload.items + (folder.FileReferences?.items ?? [])
                folder.FileReferences?.unified()
            }
        }
        for (_, element) in state.showFolders {
            if element.uid == action.folder_uid, let before = element.FileReferences?.items {
                element.FileReferences?.items = action.payload.items + before
                element.FileReferences?.unified()
            }
        }
        break
        
    case let action as FolderDispatcher.REFERENCES_CREATED:
        for (_, element) in state.guildFolders {
            for folder in element.items.filter({ $0.uid == action.folder_uid }) {
                for reference in folder.FileReferences?.items.filter({ $0.id == nil }) ?? [] {
                    reference.id = action.payload.items.filter({ $0 == reference }).first?.id
                }
                folder.FileReferences?.unified()
            }
        }
        for (_, element) in state.showFolders {
            if element.uid == action.folder_uid {
                for reference in element.FileReferences?.items.filter({ $0.id == nil }) ?? [] {
                    reference.id = action.payload.items.filter({ $0 == reference }).first?.id
                }
                element.FileReferences?.unified()
            }
        }
        break
        
    default:
        break
    }
    return state
}

struct FolderState: StateType {
    var guildFolders: [String: FolderEntities] = [:]
    var showFolders: [String: FolderEntity] = [:]
    var current_reference: FileReferenceEntity?
}

var folderStore = Store(
    reducer: folderReducer,
    state: FolderState()
)
