//
//  RootReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import Foundation
import UIKit
import ReSwift

func rootReducer(action: Action, state: State?) -> State {
    return State(
        app: appReducer(action: action, state: state?.app),
        auth: authReducer(action: action, state: state?.auth),
        category: categoryReducer(action: action, state: state?.category),
        folder: folderReducer(action: action, state: state?.folder),
        dmChannel: dmChannelReducer(action: action, state: state?.dmChannel),
        file: fileReducer(action: action, state: state?.file),
        fileReference: fileReferenceReducer(action: action, state: state?.fileReference),
        guild: guildReducer(action: action, state: state?.guild),
        search: searchReducer(action: action, state: state?.search),
        session: sessionReducer(action: action, state: state?.session),
        streamChannel: streamChannelReducer(action: action, state: state?.streamChannel),
        stream: streamReducer(action: action, state: state?.stream),
        textChannel: textChannelReducer(action: action, state: state?.textChannel),
        tag: tagReducer(action: action, state: state?.tag),
        notification: notificationReducer(action: action, state: state?.notification),
        message: messageReducer(action: action, state: state?.message),
        member: memberReducer(action: action, state: state?.member),
        user: userReducer(action: action, state: state?.user),
        role: roleReducer(action: action, state: state?.role)
    )
}

//func combineReducers<S>(_ reducers: Reducer<S>...) /*-> Reducer<S> */{
//    return { action, state in
//        for reducer in reducers {
//            return reducer(action, state)
//        }
//    }
//}
