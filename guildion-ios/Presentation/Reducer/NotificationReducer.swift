//
//  NotificationReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct NotificationDispatcher {
}

func notificationReducer(action: Action, state: NotificationState?) -> NotificationState {
    var state = state ?? NotificationState()
    
    switch action {
    default:
        break
    }
    return state
}

struct NotificationState: StateType {
}

var notificationStore = Store(
    reducer: notificationReducer,
    state: NotificationState()
)
