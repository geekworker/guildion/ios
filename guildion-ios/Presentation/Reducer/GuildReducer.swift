//
//  GuildReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct GuildDispatcher {
    struct SET_USER_GUILDS: Action {
        var payload: GuildEntities
    }
    struct ADD_USER_GUILDS: Action {
        var payload: GuildEntities
    }
    struct RESET_USER_GUILDS: Action {
    }
    
    struct SET_SHOW_GUILD: Action {
        var payload: GuildEntity
    }
    struct RESET_SHOW_GUILD: Action {
    }
    
    static func getCurrentMember(state: GuildState) -> MemberEntity? {
        guard let unwrapped = state.current_show_guild, let members = unwrapped.Members else { return nil }
        var current_member: MemberEntity?
        for member in members.items {
            if member.GuildId == unwrapped.id && member.UserId == CurrentUser.getCurrentUserEntity()?.id ?? 0 {
                current_member = member
                current_member?.Guild = unwrapped.toNewMemory()
            }
        }
        return current_member
    }
    
    static func getCurrentMemberFromGuild(guild: GuildEntity) -> MemberEntity? {
        guard let members = guild.Members else { return nil }
        var current_member: MemberEntity?
        for member in members.items {
            if member.GuildId == guild.id && member.UserId == CurrentUser.getCurrentUserEntity()?.id ?? 0 {
                current_member = member
                current_member?.Guild = guild.toNewMemory()
            }
        }
        return current_member
    }
}

func guildReducer(action: Action, state: GuildState?) -> GuildState {
    var state = state ?? GuildState()
    
    switch action {
    case let action as GuildDispatcher.SET_USER_GUILDS:
        state.userGuilds = action.payload
        break
    
    case let action as GuildDispatcher.ADD_USER_GUILDS:
        guard action.payload.items.count != 0 else { return state }
        state.userGuilds?.items += action.payload.items
        break
    case _ as GuildDispatcher.RESET_USER_GUILDS:
        state.userGuilds = nil
        break
        
        
    case let action as GuildDispatcher.SET_SHOW_GUILD:
        state.showGuilds[action.payload.uid] = action.payload
        state.current_show_guild = action.payload
        break
    
    case _ as GuildDispatcher.RESET_SHOW_GUILD:
        state.showGuilds = [:]
        break
        
    default:
        break
    }
    return state
}

struct GuildState: StateType {
    var userGuilds: GuildEntities? = GuildEntities()
    var showGuilds: [String: GuildEntity] = [:]
    var current_show_guild: GuildEntity?
}

var guildStore = Store(
    reducer: guildReducer,
    state: GuildState()
)

