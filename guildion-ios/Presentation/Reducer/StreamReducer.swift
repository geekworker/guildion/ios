//
//  StreamReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct StreamDispatcher {
    struct SET_LIVE: Action {
        var payload: StreamEntity
    }
    struct START_LIVE: Action {
        var payload: StreamEntity
    }
    struct END_LIVE: Action {
    }
    struct START_REDUCE: Action {
        var payload: StreamEntity
    }
    struct END_REDUCE: Action {
    }
    struct SET_CACHES: Action {
        var payload: StreamEntities
    }
    struct SET_DELETES: Action {
        var payload: StreamEntities
    }
    struct RESET_CACHES: Action {
        var payload: StreamEntities
    }
    struct RESET_DELETES: Action {
        var payload: StreamEntities
    }
    
    struct JOINED: Action {
        var payload: ParticipantEntity
    }
    struct LEFT: Action {
    }
    
    public static func getCache(id: Int, state: StreamState) -> StreamEntity? {
        return state.caches?.items.filter({ $0.id == id }).first
    };

    public static func getDelete(id: Int, state: StreamState) -> StreamEntity? {
        return state.deletes?.items.filter({ $0.id == id }).first
    };

    public static func bind(content: StreamEntity, state: StreamState) -> StreamEntity? {
        if getDelete(id: content.id ?? 0, state: state) != nil { return nil }
        return getCache(id: content.id ?? 0, state: state) != nil ? getCache(id: content.id ?? 0, state: state) : content
    };
    
    public static func getLive(state: StreamState) -> StreamEntity? {
        guard let model = state.liveStream, let id = model.id, let stream = StreamDispatcher.getCache(id: id, state: state) else {
            return state.liveStream
        }
        return stream
    };
}

func streamReducer(action: Action, state: StreamState?) -> StreamState {
    var state = state ?? StreamState()
    
    switch action {
    case let action as StreamDispatcher.SET_CACHES:
        guard action.payload.items.count != 0 else { return state }
        state.caches = action.payload
        break
    
    case let action as StreamDispatcher.SET_DELETES:
        guard action.payload.items.count != 0 else { return state }
        state.deletes = action.payload
        break
        
    case let action as StreamDispatcher.SET_LIVE:
        state.liveStream = action.payload
        break
        
    case let action as StreamDispatcher.START_LIVE:
        guard action.payload.is_live else { return state }
        state.liveStream = action.payload
        break
        
    case _ as StreamDispatcher.END_LIVE:
        state.liveStream = nil
        break
    
    case let action as StreamDispatcher.START_REDUCE:
        state.streamReduced = action.payload
        break
        
    case _ as StreamDispatcher.END_REDUCE:
        state.streamReduced = nil
        break
        
    case _ as StreamDispatcher.RESET_CACHES:
        state.caches = nil
        break
        
    case _ as StreamDispatcher.RESET_DELETES:
        state.deletes = nil
        break
        
    case let action as StreamDispatcher.JOINED:
        state.current_participant = action.payload
        break
    
    case _ as StreamDispatcher.LEFT:
        state.current_participant = nil
        break
        
    default:
        break
    }
    return state
}

struct StreamState: StateType {
    var liveStream: StreamEntity? = nil
    var caches: StreamEntities?
    var deletes: StreamEntities?
    var streamReduced: StreamEntity? = nil
    var current_participant: ParticipantEntity?
}

var streamStore = Store(
    reducer: streamReducer,
    state: StreamState()
)
