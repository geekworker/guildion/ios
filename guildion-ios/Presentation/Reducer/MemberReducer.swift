//
//  MemberReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct MemberDispatcher {
    struct SET_CHANNEL_MEMBERS: Action {
        var channel_uid: String
        var payload: MemberEntities
    }
    struct ADD_CHANNEL_MEMBERS: Action {
        var channel_uid: String
        var payload: MemberEntities
    }
    struct INSERT_CHANNEL_MEMBERS: Action {
        var channel_uid: String
        var payload: MemberEntities
    }
    struct RESET_CHANNEL_MEMBERS: Action {
        var payload: String
    }
    struct RESET_ALL_CHANNEL_MEMBERS: Action {
        var payload: String
    }
    
    struct SET_CACHES: Action {
        var payload: MemberEntities
    }
    struct SET_DELETES: Action {
        var payload: MemberEntities
    }
    struct RESET_CACHES: Action {
        var payload: MemberEntities
    }
    struct RESET_DELETES: Action {
        var payload: MemberEntities
    }
    public static func getCache(id: Int, state: MemberState) -> MemberEntity? {
        return state.caches?.items.filter({ $0.id == id }).first
    };

    public static func getDelete(id: Int, state: MemberState) -> MemberEntity? {
        return state.deletes?.items.filter({ $0.id == id }).first
    };

    public static func bind(content: MemberEntity, state: MemberState) -> MemberEntity? {
        if getDelete(id: content.id ?? 0, state: state) != nil { return nil }
        return getCache(id: content.id ?? 0, state: state) != nil ? getCache(id: content.id ?? 0, state: state) : content
    };
    
    public static func binds(contents: MemberEntities, state: MemberState) -> MemberEntities {
        contents.items = contents.compactMap({ bind(content: $0, state: state) })
        return contents
    };
}

func memberReducer(action: Action, state: MemberState?) -> MemberState {
    var state = state ?? MemberState()
    
    switch action {
    case let action as MemberDispatcher.SET_CHANNEL_MEMBERS:
        state.channelMembers[action.channel_uid] = action.payload
        break
    
    case let action as MemberDispatcher.ADD_CHANNEL_MEMBERS:
        guard action.payload.items.count != 0 else { return state }
        state.channelMembers[action.channel_uid]?.items += action.payload.items
        state.channelMembers[action.channel_uid]?.unified()
        break
    
    case let action as MemberDispatcher.INSERT_CHANNEL_MEMBERS:
        guard action.payload.items.count != 0, let before = state.channelMembers[action.channel_uid]?.items else { return state }
        state.channelMembers[action.channel_uid]?.items = action.payload.items
        state.channelMembers[action.channel_uid]?.items += before
        state.channelMembers[action.channel_uid]?.unified()
        break
        
    case let action as MemberDispatcher.RESET_CHANNEL_MEMBERS:
        state.channelMembers[action.payload] = MemberEntities()
        break
        
    case _ as MemberDispatcher.RESET_ALL_CHANNEL_MEMBERS:
        state.channelMembers = [:]
        break
        
    case let action as MemberDispatcher.SET_CACHES:
        guard action.payload.items.count != 0 else { return state }
        state.caches = action.payload
        break
    
    case let action as MemberDispatcher.SET_DELETES:
        guard action.payload.items.count != 0 else { return state }
        state.deletes = action.payload
        break
        
    case _ as MemberDispatcher.RESET_CACHES:
        state.caches = nil
        break
        
    case _ as MemberDispatcher.RESET_DELETES:
        state.deletes = nil
        break
        
    default:
        break
    }
    return state
}

struct MemberState: StateType {
    var channelMembers: [String: MemberEntities] = [:]
    var caches: MemberEntities? = MemberEntities()
    var deletes: MemberEntities? = MemberEntities()
}

var memberStore = Store(
    reducer: memberReducer,
    state: MemberState()
)
