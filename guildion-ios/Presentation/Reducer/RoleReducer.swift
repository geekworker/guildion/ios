//
//  RoleReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import ReSwift

struct RoleDispatcher {
}

func roleReducer(action: Action, state: RoleState?) -> RoleState {
    var state = state ?? RoleState()
    
    switch action {
    default:
        break
    }
    return state
}

struct RoleState: StateType {
}

var roleStore = Store(
    reducer: roleReducer,
    state: RoleState()
)
