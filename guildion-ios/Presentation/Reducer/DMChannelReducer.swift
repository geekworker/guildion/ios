//
//  DMChannelReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct DMChannelDispatcher {
}

func dmChannelReducer(action: Action, state: DMChannelState?) -> DMChannelState {
    var state = state ?? DMChannelState()
    
    switch action {
    default:
        break
    }
    return state
}

struct DMChannelState: StateType {
}

var dmChannelStore = Store(
    reducer: dmChannelReducer,
    state: DMChannelState()
)
