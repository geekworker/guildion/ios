//
//  CategoryReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct CategoryDispatcher {
    struct SET_CATEGORIES: Action {
        var payload: CategoryEntities
    }
    struct RESET_CATEGORIES: Action {
    }
}

func categoryReducer(action: Action, state: CategoryState?) -> CategoryState {
    var state = state ?? CategoryState()
    
    switch action {
    case let action as CategoryDispatcher.SET_CATEGORIES:
        state.categories = action.payload
        break
    case _ as CategoryDispatcher.RESET_CATEGORIES:
        state.categories = CategoryEntities()
        break
    default:
        break
    }
    return state
}

struct CategoryState: StateType {
    var categories: CategoryEntities = CategoryEntities()
}

var categoryStore = Store(
    reducer: categoryReducer,
    state: CategoryState()
)
