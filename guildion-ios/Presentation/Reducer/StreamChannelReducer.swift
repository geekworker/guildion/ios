//
//  StreamChannelReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct StreamChannelDispatcher {
}

func streamChannelReducer(action: Action, state: StreamChannelState?) -> StreamChannelState {
    var state = state ?? StreamChannelState()
    
    switch action {
    default:
        break
    }
    return state
}

struct StreamChannelState: StateType {
}

var streamChannelStore = Store(
    reducer: streamChannelReducer,
    state: StreamChannelState()
)
