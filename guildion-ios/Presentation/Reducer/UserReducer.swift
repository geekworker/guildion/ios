//
//  UserReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct UserDispatcher {
    struct SET_GUILD_BLOCKS: Action {
        var payload: GuildEntities
    }
    struct SET_USER_BLOCKS: Action {
        var payload: UserEntities
    }
    struct SET_USER_BLOCKEDS: Action {
        var payload: UserEntities
    }
    struct SET_USER_MUTES: Action {
        var payload: UserEntities
    }
    struct SET_USER_MUTEDS: Action {
        var payload: UserEntities
    }
    struct SET_USER_REPORTS: Action {
        var payload: UserEntities
    }
    struct SET_USER_REPORTEDS: Action {
        var payload: UserEntities
    }
    static func checkBlocked(target: UserEntity, state: UserState) -> Bool {
        return state.blockeds.filter({ $0.id == target.id }).count > 0
    }
    static func checkBlocked(guild: GuildEntity, state: UserState) -> Bool {
        return state.guildBlocks.filter({ $0.id == guild.id }).count > 0
    }
}

func userReducer(action: Action, state: UserState?) -> UserState {
    var state = state ?? UserState()
    
    switch action {
    case let action as UserDispatcher.SET_GUILD_BLOCKS:
        state.guildBlocks = action.payload
        break
    case let action as UserDispatcher.SET_USER_BLOCKS:
        state.blocks = action.payload
        break
    case let action as UserDispatcher.SET_USER_BLOCKEDS:
        state.blockeds = action.payload
        break
    case let action as UserDispatcher.SET_USER_MUTES:
        state.mutes = action.payload
        break
    case let action as UserDispatcher.SET_USER_MUTEDS:
        state.muteds = action.payload
        break
    case let action as UserDispatcher.SET_USER_REPORTS:
        state.reports = action.payload
        break
    case let action as UserDispatcher.SET_USER_REPORTEDS:
        state.reporteds = action.payload
        break
    default:
        break
    }
    return state
}

struct UserState: StateType {
    var guildBlocks: GuildEntities = GuildEntities()
    var blocks: UserEntities = UserEntities()
    var blockeds: UserEntities = UserEntities()
    var mutes: UserEntities = UserEntities()
    var muteds: UserEntities = UserEntities()
    var reports: UserEntities = UserEntities()
    var reporteds: UserEntities = UserEntities()
}

var userStore = Store(
    reducer: userReducer,
    state: UserState()
)
