//
//  FileReferenceReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/23.
//

import Foundation
import UIKit
import ReSwift

struct FileReferenceDispatcher {
    struct SET_GALLERY_FILE_REFERENCES: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
    struct ADD_GALLERY_FILE_REFERENCES: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
    struct INSERT_GALLERY_FILE_REFERENCES: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
    struct RESET_GALLERY_FILE_REFERENCES: Action {
        var payload: String
    }
    struct RESET_ALL_GALLERY_FILE_REFERENCES: Action {
        var payload: String
    }
    
    struct CREATED_GALLERY_FILE_REFERENCES: Action {
        var folder_uid: String
        var payload: FileReferenceEntities
    }
    
    struct SET_CACHES: Action {
        var payload: FileReferenceEntities
    }
    struct SET_DELETES: Action {
        var payload: FileReferenceEntities
    }
    struct RESET_CACHES: Action {
        var payload: FileReferenceEntities
    }
    struct RESET_DELETES: Action {
        var payload: FileReferenceEntities
    }
    public static func getCache(id: Int, state: FileReferenceState) -> FileReferenceEntity? {
        return state.caches?.items.filter({ $0.id == id }).first
    };

    public static func getDelete(id: Int, state: FileReferenceState) -> FileReferenceEntity? {
        return state.deletes?.items.filter({ $0.id == id }).first
    };

    public static func bind(content: FileReferenceEntity, state: FileReferenceState) -> FileReferenceEntity? {
        if getDelete(id: content.id ?? 0, state: state) != nil { return nil }
        return getCache(id: content.id ?? 0, state: state) != nil ? getCache(id: content.id ?? 0, state: state) : content
    };
    
    public static func binds(contents: FileReferenceEntities, state: FileReferenceState) -> FileReferenceEntities {
        contents.items = contents.compactMap({ bind(content: $0, state: state) })
        return contents
    };
}

func fileReferenceReducer(action: Action, state: FileReferenceState?) -> FileReferenceState {
    var state = state ?? FileReferenceState()
    
    switch action {
    case let action as FileReferenceDispatcher.SET_GALLERY_FILE_REFERENCES:
        state.folderReferences[action.folder_uid] = action.payload
        break
    
    case let action as FileReferenceDispatcher.ADD_GALLERY_FILE_REFERENCES:
        guard action.payload.items.count != 0 else { return state }
        state.folderReferences[action.folder_uid]?.items += action.payload.items
        state.folderReferences[action.folder_uid]?.unified()
        break
    
    case let action as FileReferenceDispatcher.INSERT_GALLERY_FILE_REFERENCES:
        guard action.payload.items.count != 0, let before = state.folderReferences[action.folder_uid]?.items else { return state }
        state.folderReferences[action.folder_uid]?.items = action.payload.items
        state.folderReferences[action.folder_uid]?.items += before
        state.folderReferences[action.folder_uid]?.unified()
        break
        
    case let action as FileReferenceDispatcher.RESET_GALLERY_FILE_REFERENCES:
        state.folderReferences[action.payload] = FileReferenceEntities()
        break
        
    case _ as FileReferenceDispatcher.RESET_ALL_GALLERY_FILE_REFERENCES:
        state.folderReferences = [:]
        break
        
    case let action as FileReferenceDispatcher.CREATED_GALLERY_FILE_REFERENCES:
        guard action.payload.items.count != 0, let before = state.folderReferences[action.folder_uid]?.items else { return state }
        for reference in before.filter({ $0.id == nil }) {
            reference.id = action.payload.items.filter({ $0 == reference }).first?.id
        }
        break
        
    case let action as FileReferenceDispatcher.SET_CACHES:
        guard action.payload.items.count != 0 else { return state }
        state.caches = action.payload
        break
    
    case let action as FileReferenceDispatcher.SET_DELETES:
        guard action.payload.items.count != 0 else { return state }
        state.deletes = action.payload
        break
        
    case _ as FileReferenceDispatcher.RESET_CACHES:
        state.caches = nil
        break
        
    case _ as FileReferenceDispatcher.RESET_DELETES:
        state.deletes = nil
        break
        
    default:
        break
    }
    return state
}

struct FileReferenceState: StateType {
    var folderReferences: [String: FileReferenceEntities] = [:]
    var caches: FileReferenceEntities? = FileReferenceEntities()
    var deletes: FileReferenceEntities? = FileReferenceEntities()
}

var fileReferenceStore = Store(
    reducer: fileReferenceReducer,
    state: FileReferenceState()
)
