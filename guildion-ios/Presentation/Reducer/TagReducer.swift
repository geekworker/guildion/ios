//
//  TagReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct TagDispatcher {
}

func tagReducer(action: Action, state: TagState?) -> TagState {
    var state = state ?? TagState()
    
    switch action {
    default:
        break
    }
    return state
}

struct TagState: StateType {
}

var tagStore = Store(
    reducer: tagReducer,
    state: TagState()
)
