//
//  SearchReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct SearchDispatcher {
    struct SET_GUILD: Action {
        var keyword: String
        var payload: GuildEntities
    }
    struct ADD_GUILD: Action {
        var keyword: String
        var payload: GuildEntities
    }
    struct INSERT_GUILD: Action {
        var keyword: String
        var payload: GuildEntities
    }
    struct RESET_GUILD: Action {
        var payload: String
    }
    struct RESET_ALL_GUILD: Action {
        var payload: String
    }
    
    struct SET_GUILD_KEYWORD: Action {
        var payload: String
    }
    struct RESET_GUILD_KEYWORD: Action {
    }
}

func searchReducer(action: Action, state: SearchState?) -> SearchState {
    var state = state ?? SearchState()
    
    switch action {
    
    case let action as SearchDispatcher.SET_GUILD:
        state.searchGuilds[action.keyword] = action.payload
        break
    
    case let action as SearchDispatcher.ADD_GUILD:
        guard action.payload.items.count != 0 else { return state }
        state.searchGuilds[action.keyword]?.items += action.payload.items
        state.searchGuilds[action.keyword]?.unified()
        break
    
    case let action as SearchDispatcher.INSERT_GUILD:
        guard action.payload.items.count != 0, let before = state.searchGuilds[action.keyword]?.items else { return state }
        state.searchGuilds[action.keyword]?.items = action.payload.items
        state.searchGuilds[action.keyword]?.items += before
        state.searchGuilds[action.keyword]?.unified()
        break
        
    case let action as SearchDispatcher.RESET_GUILD:
        state.searchGuilds[action.payload] = GuildEntities()
        break
        
    case _ as SearchDispatcher.RESET_ALL_GUILD:
        state.searchGuilds = [:]
        break
        
    case let action as SearchDispatcher.SET_GUILD_KEYWORD:
        state.guild_keyword = action.payload
        break
    
    case _ as SearchDispatcher.RESET_GUILD_KEYWORD:
        state.guild_keyword = nil
        break
        
    default:
        break
    }
    return state
}

struct SearchState: StateType {
    var guild_keyword: String?
    var searchGuilds: [String: GuildEntities] = [:]
}

var searchStore = Store(
    reducer: searchReducer,
    state: SearchState()
)
