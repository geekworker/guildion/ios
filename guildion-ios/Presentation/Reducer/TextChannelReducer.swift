//
//  TextChannelReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct TextChannelDispatcher {
}

func textChannelReducer(action: Action, state: TextChannelState?) -> TextChannelState {
    var state = state ?? TextChannelState()
    
    switch action {
    default:
        break
    }
    return state
}

struct TextChannelState: StateType {
}

var textChannelStore = Store(
    reducer: textChannelReducer,
    state: TextChannelState()
)
