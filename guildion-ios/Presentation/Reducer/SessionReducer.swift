//
//  SessionReducer.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import ReSwift

struct SessionDispatcher {
    struct SET_SESSION: Action {
        var payload: SessionModel?
    }
    struct REGISTERED: Action {
    }
    struct NOT_REGISTERED: Action {
    }
    struct CONFIRMED: Action {
    }
    struct NOT_CONFIRMED: Action {
    }
}

func sessionReducer(action: Action, state: SessionState?) -> SessionState {
    var state = state ?? SessionState()
    
    switch action {
    case let action as SessionDispatcher.SET_SESSION:
        state.repository = action.payload
        break
    case _ as SessionDispatcher.REGISTERED:
        state.registered = true
        break
    case _ as SessionDispatcher.NOT_REGISTERED:
        state.registered = false
        break
    case _ as SessionDispatcher.CONFIRMED:
        state.confirmed = true
        break
    case _ as SessionDispatcher.NOT_CONFIRMED:
        state.confirmed = false
        break
    default:
        break
    }
    return state
}

struct SessionState: StateType {
    var repository: SessionModel? = SessionModel()
    var registered: Bool? = CurrentUser.getCurrentUserEntity() != nil
    var confirmed: Bool? = CurrentUser.getCurrentUserEntity()?.verified
}

var sessionStore = Store(
    reducer: sessionReducer,
    state: SessionState()
)
