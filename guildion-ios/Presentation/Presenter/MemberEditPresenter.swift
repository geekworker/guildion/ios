//
//  MemberEditPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol MemberEditPresenterOutput: class {
    var blocked: Bool { get set }
    var muted: Bool { get set }
    func sync(didFetchRepository repository: MemberEntity)
    func memberEditPresenter(didUpdate repository: MemberEntity, in memberEditPresenter: MemberEditPresenter)
    func memberEditPresenter(shouldReload memberEditPresenter: MemberEditPresenter)
    func memberEditPresenter(shouldBack memberEditPresenter: MemberEditPresenter)
    func memberEditPresenter(onError error: Error, in memberEditPresenter: MemberEditPresenter)
}

@objcMembers
class MemberEditPresenter: PresenterImpl {
    typealias WireframeType = MemberEditWireframe
    typealias RepositoriesType = MemberEntity
    typealias ViewControllerType = MemberEditPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func toMemberRolesEdit() {
        self.wireframe?.toMemberRolesEdit()
    }
    
    func fetch(repostiroy: MemberEntity) {
        guard let id = repostiroy.id else { return }
        self.repositories = repostiroy
        memberUseCase!.get(
            id: id
        ).then({
            self.repositories = repostiroy
            self.viewController?.sync(didFetchRepository: $0)
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func update(repository: MemberEntity) {
        memberUseCase!.update(
            member: repository
        ).then({
            self.viewController?.memberEditPresenter(didUpdate: $0, in: self)
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func kick(guild: GuildEntity, repository: MemberEntity) {
        guard let user = repository.User else { return }
        guildUseCase!.kick(
            guild: guild, target: user
        ).then({
            GuildConnector.shared.fetchRequest().then({})
            self.viewController?.memberEditPresenter(didUpdate: repository, in: self)
            self.viewController?.memberEditPresenter(shouldBack: self)
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func block(guild: GuildEntity, repository: MemberEntity) {
        guard let user = repository.User else { return }
        guildUseCase!.block(
            guild: guild, target: user
        ).then({
            self.viewController?.memberEditPresenter(didUpdate: repository, in: self)
            self.viewController?.memberEditPresenter(shouldBack: self)
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func block(target: UserEntity) {
        self.userUseCase!.block(
            target: target
        ).then({
            self.userUseCase!.getUserRelationStats().then({ user in
                self.viewController?.blocked = user.Blocks?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.muted = user.Mutes?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.memberEditPresenter(shouldReload: self)
            }).catch({
                self.viewController?.memberEditPresenter(onError: $0, in: self)
            })
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func unblock(target: UserEntity) {
        self.userUseCase!.unblock(
            target: target
        ).then({
            self.userUseCase!.getUserRelationStats().then({ user in
                self.viewController?.blocked = user.Blocks?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.muted = user.Mutes?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.memberEditPresenter(shouldReload: self)
            }).catch({
                self.viewController?.memberEditPresenter(onError: $0, in: self)
            })
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func mute(target: UserEntity) {
        self.userUseCase!.mute(
            target: target
        ).then({
            self.userUseCase!.getUserRelationStats().then({ user in
                self.viewController?.blocked = user.Blocks?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.muted = user.Mutes?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.memberEditPresenter(shouldReload: self)
            }).catch({
                self.viewController?.memberEditPresenter(onError: $0, in: self)
            })
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func unmute(target: UserEntity) {
        self.userUseCase!.unmute(
            target: target
        ).then({
            self.userUseCase!.getUserRelationStats().then({ user in
                self.viewController?.blocked = user.Blocks?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.muted = user.Mutes?.filter({ $0.id == self.repositories?.UserId }).count ?? 0 > 0
                self.viewController?.memberEditPresenter(shouldReload: self)
            }).catch({
                self.viewController?.memberEditPresenter(onError: $0, in: self)
            })
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
    
    func report(target: UserEntity, detail: String) {
        self.userUseCase!.report(
            target: target,
            detail: detail
        ).then({
            self.userUseCase!.getUserRelationStats().then({ _ in
            }).catch({
                self.viewController?.memberEditPresenter(onError: $0, in: self)
            })
        }).catch({
            self.viewController?.memberEditPresenter(onError: $0, in: self)
        })
    }
}

extension MemberEditPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        self.viewController?.blocked = state.user.blockeds.filter({ $0.id == repositories?.UserId }).count > 0
        self.viewController?.muted = state.user.muteds.filter({ $0.id == repositories?.UserId }).count > 0
    }
}
