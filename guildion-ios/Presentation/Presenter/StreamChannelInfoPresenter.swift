//
//  StreamChannelInfoPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/14.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol StreamChannelInfoPresenterOutput: class {
    var all_files: Bool { get set }
    var current_guild: GuildEntity { get set }
    func sync(didFetchRepository repository: StreamChannelEntity)
    func sync(didFetchFolder repository: FolderEntity)
    func streamChannelInfoPresenter(didFetch repositories: FileReferenceEntities, in streamChannelInfoPresenter: StreamChannelInfoPresenter)
    func streamChannelInfoPresenter(didIncrement repositories: FileReferenceEntities, in streamChannelInfoPresenter: StreamChannelInfoPresenter)
    func streamChannelInfoPresenter(didDecrement repositories: FileReferenceEntities, in streamChannelInfoPresenter: StreamChannelInfoPresenter)
}

@objcMembers
class StreamChannelInfoPresenter: PresenterImpl {
    typealias WireframeType = StreamChannelInfoWireframe
    typealias RepositoriesType = FolderEntity
    typealias ViewControllerType = StreamChannelInfoPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var folder: FolderEntity?
    var categories: CategoryEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(folder: FolderEntity) {
        self.repositories = folder
        if let all_files = viewController?.all_files, all_files {
            folderUseCase!.getCurrentGuildFiles(
                current_guild: self.viewController!.current_guild,
                is_playlist: true
            ).then({
                self.repositories = FolderEntity()
                self.repositories?.name = R.string.localizable.allFiles()
                self.repositories?.temporary = true
                let entities = FileReferenceEntities()
                entities.items = $0.enumerated().compactMap({
                    let entity = FileReferenceEntity()
                    entity.id = $0.offset
                    entity.File = $0.element
                    entity.FileId = $0.element.id
                    return entity
                }).reversed()
                self.viewController?.sync(didFetchFolder: self.repositories!)
                self.viewController?.streamChannelInfoPresenter(didFetch: entities, in: self)
            })
        } else {
            folderUseCase!.getFolder(
                id: folder.id ?? 0
            ).then({
                self.folder = $0
                store.dispatch(FolderDispatcher.SET_SHOW_GALLERY(folder_uid: $0.uid, payload: $0))
                self.viewController?.sync(didFetchFolder: $0)
            })
            folderUseCase!.getCurrentFolderReferences(folder: folder).then({
                self.viewController?.streamChannelInfoPresenter(didFetch: $0, in: self)
            })
        }
    }
    
    func increment() {
        if self.viewController?.all_files ?? false {
            folderUseCase!.getDecrementGuildFiles(current_guild: self.viewController!.current_guild, is_playlist: true).then({
                let entities = FileReferenceEntities()
                entities.items = $0.enumerated().compactMap({
                    let entity = FileReferenceEntity()
                    entity.id = $0.offset
                    entity.File = $0.element
                    entity.FileId = $0.element.id
                    return entity
                }).reversed()
                self.viewController?.streamChannelInfoPresenter(didIncrement: entities, in: self)
            })
        } else {
            folderUseCase!.getIncrementFolderReferences(folder: self.repositories!).then({
                self.viewController?.streamChannelInfoPresenter(didIncrement: $0, in: self)
            })
        }
    }
    
    func decrement() {
        if self.viewController?.all_files ?? false {
            folderUseCase!.getIncrementGuildFiles(current_guild: self.viewController!.current_guild, is_playlist: true).then({
                let entities = FileReferenceEntities()
                entities.items = $0.enumerated().compactMap({
                    let entity = FileReferenceEntity()
                    entity.id = $0.offset
                    entity.File = $0.element
                    entity.FileId = $0.element.id
                    return entity
                }).reversed()
                self.viewController?.streamChannelInfoPresenter(didDecrement: entities, in: self)
            })
        } else {
            folderUseCase!.getDecrementFolderReferences(folder: self.repositories!).then({
                self.viewController?.streamChannelInfoPresenter(didDecrement: $0, in: self)
            })
        }
    }
}

extension StreamChannelInfoPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}

