//
//  DMChannelPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol DMChannelPresenterOutput: class {
    var permission: PermissionModel { get set }
    var current_member: MemberEntity { get set }
    var current_guild: GuildEntity { get set }
    var repositories: MessageEntities { get set }
    var repository: DMChannelEntity { get set }
    func sync(didFetchRepository repository: DMChannelEntity)
    func sync(didFetchRepositories repositories: MessageEntities)
    func sync(didFetchRepositories repositories: MemberEntities)
    func syncCurrentMember(didFetchRepository repository: MemberEntity)
    func dmChannelPresenter(didIncrements repositories: MessageEntities)
    func dmChannelPresenter(didDecrements repositories: MessageEntities)
    func dmChannelPresenter(didCreated repository: MessageEntity)
    func dmChannelPresenter(didCreated repository: ReactionEntity)
    func dmChannelPresenter(willTyping repository: MemberEntity)
    func dmChannelPresenter(didTyping repository: MemberEntity)
    func dmChannelPresenter(onError error: Error)
    func dmChannelPresenter(didCache repositories: MessageEntities, shouldScroll: Bool, in dmChannelPresenter: DMChannelPresenter)
    func dmChannelPresenter(didRetry count: Int, in dmChannelPresenter: DMChannelPresenter)
    func dmChannelPresenter(shouldReload dmChannelPresenter: DMChannelPresenter)
}

@objcMembers
class DMChannelPresenter: PresenterImpl {
    typealias WireframeType = DMChannelWireframe
    typealias RepositoriesType = DMChannelEntity
    typealias ViewControllerType = DMChannelPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var current_member: MemberEntity?
    var categories: CategoryEntities?
    var caches: MessageEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        ChannelConnector.shared.inject(self)
        self.sync(self.repositories!)
        self.fetchMessages()
        self.dmChannelUseCase!.get(
            uid: self.repositories!.uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.dmChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.viewController?.permission.DMChannel.current_dm_channel_roles = $0.Roles ?? DMChannelRoleEntities()
            ChannelConnector.shared.current_channel = $0
            self.viewController?.sync(didFetchRepository: $0)
        }).catch({
            self.viewController?.dmChannelPresenter(onError: $0)
        })
    }
    
    func reload() {
        guard let uid = self.repositories?.uid else { return }
        self.dmChannelUseCase!.get(
            uid: uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.dmChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.viewController?.permission.DMChannel.current_dm_channel_roles = $0.Roles ?? DMChannelRoleEntities()
            self.viewController?.sync(didFetchRepository: $0)
        }).catch({
            self.viewController?.dmChannelPresenter(onError: $0)
        })
    }
    
    func join() {
        ChannelConnector.shared.didJoin(current_channel: self.repositories!).then({ self.read() })
    }
    
    func unbind() {
        store.unsubscribe(self)
        ChannelConnector.shared.endEditInput().then({
            ChannelConnector.shared.didLeave().then({})
        })
    }
    
    func toChannelMenu() {
        self.wireframe?.toChannelMenu()
    }
    
    func toMemberMenu(repository: MemberEntity) {
        self.wireframe?.toMemberMenu(repository: repository)
    }
    
    func toMessageMenu(repository: MessageEntity) {
        self.wireframe?.toMessageMenu(repository: repository)
    }
    
    func fetchMembers() {
        self.guildUseCase?.getGuildMembers(
            guild: self.viewController!.current_guild.toNewMemory()
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.dmChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func fetchMessages() {
        self.messageUseCase!.getCurrentMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.dmChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.nilCheckRepositories()
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func increments() {
        guard self.viewController?.repositories.count ?? 0 > 0 else { self.fetchMessages(); return }
        messageUseCase!.getIncrementMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.dmChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            guard self.repositories != nil else { return }
            self.repositories?.Messages = $0
            self.read()
            self.viewController?.dmChannelPresenter(didIncrements: $0)
        })
    }
    
    func decrements() {
        guard self.viewController?.repositories.count ?? 0 > 0 else { self.fetchMessages(); return }
        messageUseCase!.getDecrementMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.dmChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            guard self.repositories != nil else { return }
            self.repositories?.Messages = $0
            self.viewController?.dmChannelPresenter(didDecrements: $0)
        })
    }
    
    func willTyping() {
        ChannelConnector.shared.didEditInput().then({})
    }
    
    func didTyping() {
        ChannelConnector.shared.endEditInput().then({})
    }
    
    func sync(_ repository: RepositoriesType) {
        self.repositories = repository
        store.dispatch(AppDispatcher.SET_CURRENT_CHANNEL(payload: repository))
    }
    
    func create(_ repository: ReactionEntity) {
        messageUseCase!.create(
            reaction: repository
        ).then({
            ChannelConnector.shared.didReaction(reaction: $0).then({})
        })
    }
    
    func create(_ repository: MessageEntity) {
        messageUseCase!.create(
            message: repository
        ).then({
            self.viewController?.dmChannelPresenter(didCreated: $0)
            $0.Sender = self.current_member
            ChannelConnector.shared.didCreateMessage(message: $0).then({})
            self.increments()
        }).catch({
            self.viewController?.dmChannelPresenter(onError: $0)
        })
    }
    
    func update(_ repository: MessageEntity) {
        messageUseCase!.update(
            message: repository
        ).then({
            ChannelConnector.shared.didUpdateMessage(message: $0).then({})
            self.viewController?.dmChannelPresenter(didCreated: $0)
        }).catch({
            self.viewController?.dmChannelPresenter(onError: $0)
        })
        
    }
    
    func read() {
        guard let channel = ChannelConnector.shared.current_channel as? DMChannelEntity else { return }
        dmChannelUseCase!.reads(
            channel: channel.toNewMemoryChannel()
        ).then({ _ in
        }).catch({
            self.viewController?.dmChannelPresenter(onError: $0)
        })
    }
    
    func nilCheckRepositories() {
        if repositories == nil {
            repositories = DMChannelEntity()
        }
    }
    
    func toWebBrowser(_ url: URL, repositories: FileEntities? = nil) {
        self.wireframe?.toWebBrowser(url, repositories: repositories)
    }
    
    func toImageViewer(repository: FileEntity) {
        self.wireframe?.toImageViewer(repository: repository)
    }
}

extension DMChannelPresenter: ChannelConnectorDelegate {
    func channelConnector(event: WebsocketChannelEvent) {
        switch event {
        case .fetchRequest: self.reload()
        case .didCreateMessage(let member, let message):
            guard let current_channel = ChannelConnector.shared.current_channel, let uid = current_channel.messageable_uid else { return }
            let entities = MessageEntities()
            entities.append(message)
            message.Sender = member
            self.read()
            store.dispatch(MessageDispatcher.ADD_CHANNEL_MESSAGES(channel_uid: uid, payload: entities))
            self.viewController?.dmChannelPresenter(shouldReload: self)
        case .didUpdateMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_CACHES(payload: entities))
                self.viewController?.dmChannelPresenter(shouldReload: self)
            }
        case .didDeleteMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_DELETES(payload: entities))
                self.viewController?.dmChannelPresenter(shouldReload: self)
            }
        case .didEditInput(let member):
            self.viewController?.dmChannelPresenter(willTyping: member)
        case .endEditInput(let member):
            self.viewController?.dmChannelPresenter(didTyping: member)
        case .didReaction(_, let reaction):
            self.viewController?.dmChannelPresenter(didCreated: reaction)
        default: break
        }
    }
    
    func channelConnector(onError: Error) {
    }
}

extension DMChannelPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let current_channel = self.repositories, let caches = state.message.channelCacheMessages[current_channel.uid] {
            self.caches = caches
            self.viewController?.dmChannelPresenter(didCache: caches, shouldScroll: false, in: self)
        }
    }
}

