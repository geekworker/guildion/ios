//
//  GuildPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol GuildPresenterOutput: class {
    var viewAppeared: Bool { get set }
    var current_member: MemberEntity { get set }
    var permission: PermissionModel { get set }
    func sync(didFetchRepository repository: GuildEntity)
    func guildPresenter(didRetry count: Int, in guildPresenter: GuildPresenter)
    func guildPresenter(didJoin repository: GuildEntity, in guildPresenter: GuildPresenter)
    func guildPresenter(shouldUpdatePermission repository: PermissionModel, in guildPresenter: GuildPresenter)
}

@objcMembers
class GuildPresenter: NSObject, PresenterImpl {
    typealias WireframeType = GuildWireframe
    typealias RepositoriesType = GuildEntity
    typealias ViewControllerType = GuildPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        GuildConnector.shared.inject(self)
        ChannelConnector.shared.inject(self)
        StreamChannelConnector.shared.inject(self)
        FolderConnector.shared.inject(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
        GuildConnector.shared.reject(self)
        ChannelConnector.shared.reject(self)
        StreamChannelConnector.shared.reject(self)
        FolderConnector.shared.reject(self)
    }
    
    func leave() {
        GuildConnector.shared.didLeave().then({})
    }
    
    func joinChildRoom() {
        GuildConnector.shared.didJoinChildRoom().then({})
    }
    
    func fetch(uid: String, current_update: Bool = true) {
        self.guildUseCase!.getGuild(
            uid: uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.guildPresenter(didRetry: count, in: self)
            return true
        }.then({ repository in
            self.repositories = repository
            if current_update {
                self.setCurrentGuild(repository)
            }
            self.viewController?.permission.Guild.current_guild = repository
            if let current_member = GuildDispatcher.getCurrentMemberFromGuild(guild: repository) {
                self.viewController?.current_member = current_member
                self.viewController?.permission.Guild.current_member_roles = current_member.MemberRoles ?? MemberRoleEntities()
                self.viewController?.guildPresenter(shouldUpdatePermission: self.viewController!.permission, in: self)
                GuildConnector.shared.didJoin(current_guild: repository, current_member: current_member).then({ _ in
                    self.viewController?.sync(didFetchRepository: repository)
                })
            }
            // MARK: Show alert notfication when login and fetch guild
            DispatchQueue.main.async {
                appDelegate.configureOneSignal()
            }
        })
    }
    
    fileprivate var lastReload: Date = Date()
    fileprivate var lastReloadCount: Int = 0
    func reload() {
        guard let uid = self.repositories?.uid, viewController!.viewAppeared, Date().timeIntervalSince(lastReload) > GuildConfig.reload_min_interval_sc else {
            lastReloadCount += 1
            if lastReloadCount == 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + (GuildConfig.reload_min_interval_sc * 2), execute: {
                    self.reload()
                })
            }
            return
        }
        lastReload = Date()
        self.lastReloadCount = 0
        self.guildUseCase!.getGuild(
            uid: uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.guildPresenter(didRetry: count, in: self)
            return true
        }.then({ repository in
            self.repositories = repository
            self.viewController?.permission.Guild.current_guild = repository
            if let current_member = GuildDispatcher.getCurrentMemberFromGuild(guild: repository) {
                self.viewController?.current_member = current_member
                self.viewController?.permission.Guild.current_member_roles = current_member.MemberRoles ?? MemberRoleEntities()
                self.viewController?.guildPresenter(shouldUpdatePermission: self.viewController!.permission, in: self)
                GuildConnector.shared.current_guild = repository
                GuildConnector.shared.current_member = current_member
                self.viewController?.sync(didFetchRepository: repository)
            }
        })
    }
    
    func afterFetch(_ repository: GuildEntity, current_update: Bool = true) {
        
    }
    
    func setCurrentGuild(_ repository: GuildEntity) {
        store.dispatch(AppDispatcher.SET_CURRENT_GUILD(payload: repository))
    }
    
    func toChannelNew(section: SectionChannelEntity) {
        self.wireframe?.toChannelNew(section: section)
    }
    
    func toGuildMenu() {
        self.wireframe?.toGuildMenu()
    }
    
    func toMemberMenu(repository: MemberEntity) {
        self.wireframe?.toMemberMenu(repository: repository)
    }
    
    func toGuildOpenSetting() {
        self.wireframe?.toGuildOpenSetting()
    }
    
    func toMemberRequests() {
        self.wireframe?.toMemberRequest()
    }
    
    func toGuildShare() {
        self.wireframe?.toGuildShare()
    }
}

extension GuildPresenter: GuildConnectorDelegate {
    func guildConnector(event: WebsocketGuildEvent) {
        switch event {
        case .fetchRequest:
            self.reload()
        default: break
        }
    }
    
    func guildConnector(onError: Error) {
    }
}

extension GuildPresenter: ChannelConnectorDelegate {
    func channelConnector(event: WebsocketChannelEvent) {
        switch event {
        case .fetchRequest, .didCreateMessage:
            self.reload()
        case .didUpdateMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_CACHES(payload: entities))
                self.reload()
            }
        case .didDeleteMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_DELETES(payload: entities))
                self.reload()
            }
        default: break
        }
    }
    
    func channelConnector(onError: Error) {
    }
    
}

extension GuildPresenter: StreamChannelConnectorDelegate {
    func streamChannelConnector(event: WebsocketStreamChannelEvent) {
        switch event {
        case .didParticipantJoin, .didParticipantLeave, .syncStream:
            self.reload()
        default: break
        }
    }
    
    func streamChannelConnector(onError: Error) {
    }
    
}

extension GuildPresenter: FolderConnectorDelegate {
    func folderConnector(event: WebsocketFolderEvent) {
        switch event {
        case .didCreateFile, .didDeleteFile, .didCreateFolder, .didUpdateFolder, .didDeleteFolder:
            self.reload()
        default: break
        }
    }
    
    func folderConnector(onError: Error) {
    }
    
}

extension GuildPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
