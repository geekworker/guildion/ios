//
//  StreamChannelPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Hydra
import ReSwift
import AVFoundation
import AudioToolbox
import SwiftyJSON
import RxSwift

protocol StreamChannelPresenterOutput: class {
    var peerConnections: PeerConnectionModels { get set }
    var repository: StreamChannelEntity { get set }
    var repositories: MessageEntities { get set }
    var permission: PermissionModel { get set }
    var current_member: MemberEntity { get set }
    var current_guild: GuildEntity { get set }
    var syncPlayManager: SyncPlayManagerModel { get set }
    var participants: ParticipantEntities { get set }
    func sync(didFetchRepository repository: StreamChannelEntity)
    func sync(didFetchRepositories repositories: MessageEntities)
    func sync(didFetchRepositories repositories: ParticipantEntities)
    func sync(didFetchRepositories repositories: MemberEntities)
    func sync(didFetchFolder repository: FolderEntity)
    func syncCurrentMember(didFetchRepository repository: MemberEntity)
    func syncCurrentMember(didFetchIncrementRepository repository: StreamableEntity?)
    func syncCurrentMember(didFetchDecrementRepository repository: StreamableEntity?)
    func streamChannelPresenter(shouldStart repository: StreamEntity)
    func streamChannelPresenter(shouldReload streamChannelPresenter: StreamChannelPresenter)
    func streamChannelPresenter(onError error: Error)
    func streamChannelPresenter(didSwitchLoop is_loop: Bool)
    func streamChannelPresenter(willSyncStream syncPlayManager: SyncPlayManagerModel)
    func streamChannelPresenter(didSyncStream syncPlayManager: SyncPlayManagerModel)
    func streamChannelPresenter(didSeekStream syncPlayManager: SyncPlayManagerModel)
    func streamChannelPresenter(fetchRequest syncPlayManager: SyncPlayManagerModel, from: PeerConnectionModel)
    func streamChannelPresenter(fetchResponse syncPlayManager: SyncPlayManagerModel, from: PeerConnectionModel)
    func streamChannelPresenter(shouldTransferHost syncPlayManager: SyncPlayManagerModel, from: PeerConnectionModel)
    func streamChannelPresenterStopPlayer()
    func streamChannelPresenterPlayPlayer()
    func streamChannelPresenterSeekPlayer(syncPlayManager: SyncPlayManagerModel)
    
    func streamChannelPresenter(didIncrements repositories: MessageEntities)
    func streamChannelPresenter(didDecrements repositories: MessageEntities)
    func streamChannelPresenter(didCreated repository: MessageEntity)
    func streamChannelPresenter(didCreated repository: ReactionEntity)
    func streamChannelPresenter(willTyping repository: MemberEntity)
    func streamChannelPresenter(didTyping repository: MemberEntity)
    func streamChannelPresenter(didJoin repository: StreamChannelEntity)
    func streamChannelPresenter(didCheckFirst streamChannelPresenter: StreamChannelPresenter)
    func streamChannelPresenter(didCache repositories: MessageEntities, shouldScroll: Bool, in streamChannelPresenter: StreamChannelPresenter)
    func streamChannelPresenter(didRetry count: Int, in streamChannelPresenter: StreamChannelPresenter)
    
    func streamChannelPresenter(willEnterBackground is_background: Bool)
    func streamChannelPresenter(willExitBackground is_background: Bool)
}

@objcMembers
class StreamChannelPresenter: NSObject, PresenterImpl {
    typealias WireframeType = StreamChannelWireframe
    typealias RepositoriesType = StreamChannelEntity
    typealias ViewControllerType = StreamChannelPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var current_member: MemberEntity?
    var categories: CategoryEntities?
    var folder: FolderEntity?
    var caches: MessageEntities?
    var joined: Bool = false
    var firstSyncingStream: Bool = false
    var syncingStream: Bool = false
    var seekingStream: Bool = false
    fileprivate var fetchTimer: Timer?
    fileprivate var disposeBag: DisposeBag = DisposeBag()
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        configureObserver()
    }
    
    func fetch(repository: StreamChannelEntity) {
        WebSocketHandler.shared.inject(self)
        StreamChannelConnector.shared.inject(self)
        ChannelConnector.shared.inject(self)
        self.syncingStream = true
        self.repositories = repository
        store.dispatch(AppDispatcher.SET_CURRENT_CHANNEL(payload: repository))
        
        self.streamChannelUseCase!.get(
            uid: self.repositories!.uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({ result in
            DispatchQueue.main.async {
                self.repositories = result
                self.viewController?.permission.StreamChannel.current_stream_channel_roles = result.Roles ?? StreamChannelRoleEntities()
                ChannelConnector.shared.current_channel = result
                StreamChannelConnector.shared.current_channel = result
                self.read()
                store.dispatch(AppDispatcher.SET_CURRENT_CHANNEL(payload: result))
                self.viewController?.sync(didFetchRepository: result)
            }
        }).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func fetchMessages() {
        self.messageUseCase!.getCurrentMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.nilCheckRepositories()
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func reload() {
        guard let uid = self.repositories?.uid else { return }
        self.streamChannelUseCase!.get(
            uid: uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({ result in
            DispatchQueue.main.async {
                self.repositories = result
                self.viewController?.permission.StreamChannel.current_stream_channel_roles = result.Roles ?? StreamChannelRoleEntities()
                self.viewController?.sync(didFetchRepository: result)
            }
        }).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func unbind() {
        store.unsubscribe(self)
        WebSocketHandler.shared.reject(self)
        StreamChannelConnector.shared.reject(self)
        ChannelConnector.shared.reject(self)
        fetchTimer?.invalidate()
        fetchTimer = nil
    }
    
    func join_sound() {
        let notificationPath = Bundle.main.url(forResource: R.file.bubblingUpMp3.name, withExtension: R.file.bubblingUpMp3.pathExtension)
        var soundID: SystemSoundID = 0
        AudioServicesCreateSystemSoundID(notificationPath! as CFURL, &soundID)
        AudioServicesPlaySystemSoundWithCompletion(soundID) {}
    }
    
    func leave_sound() {
        let notificationPath = Bundle.main.url(forResource: R.file.caseClosedMp3.name, withExtension: R.file.bubblingUpMp3.pathExtension)
        var soundID: SystemSoundID = 1
        AudioServicesCreateSystemSoundID(notificationPath! as CFURL, &soundID)
        AudioServicesPlaySystemSoundWithCompletion(soundID) {}
    }
    
    func join() {
        guard !joined else { StreamChannelConnector.shared.reJoin(current_channel: self.repositories!).then({}); return }
        joined = true
        firstSyncingStream = true
        StreamChannelConnector.shared.didJoin(
            current_channel: self.repositories!
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.viewController?.streamChannelPresenter(didJoin: self.repositories!)
        })
        streamUseCase!.join(
            channel: self.repositories!
        ).then({ (stream, participant) in
            if self.repositories?.Streams == nil {
                self.repositories?.Streams = StreamEntities()
                self.repositories?.Streams?.append(stream)
            }
            guard let streams = self.repositories?.Streams else { return }
            for enumrate in streams.items.enumerated() {
                if enumrate.element.is_live || enumrate.element == stream {
                    streams.items[enumrate.offset] = stream
                }
            }
            self.viewController?.streamChannelPresenter(shouldStart: stream)
            StreamChannelConnector.shared.didParticipantJoin().then({})
            self.fetchParticipants()
        }).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func checkBecomableHostMessage() {
        StreamChannelConnector.shared.checkBecomableHostMessage(
            current_channel: self.viewController!.repository,
            current_guild: self.viewController!.current_guild
        ).then({})
    }
    
    func webscoketDidDisconnect() {
        self.joined = false
    }
    
    func leave() {
        self.transferHost(onSuccess: {
            ChannelConnector.shared.endEditInput().then({
                StreamChannelConnector.shared.didParticipantLeave().then({
                    StreamChannelConnector.shared.didLeave().then({})
                })
            })
        })
        streamUseCase!.leave(
            channel: self.repositories!,
            duration: self.viewController!.syncPlayManager.duration
        ).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func forceLeave() {
        self.transferHost(onSuccess: {
            ChannelConnector.shared.endEditInput().then({
                StreamChannelConnector.shared.didParticipantLeave().then({
                    StreamChannelConnector.shared.didLeave().then({})
                })
            })
        })
        streamUseCase!.leave(
            channel: self.repositories!,
            duration: self.viewController!.syncPlayManager.duration
        ).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func fetch(uid: String) {
        self.streamChannelUseCase!.getStreamChannel(
            uid: uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.repositories = $0
            self.viewController?.sync(didFetchRepository: $0)
        })
    }
    
    func fetchMembers() {
        self.guildUseCase?.getGuildMembers(
            guild: self.viewController!.current_guild.toNewMemory()
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func fetchParticipants() {
        guard let uid = self.repositories?.uid else { return }
        self.streamChannelUseCase?.getParticipants(
            uid: uid
        ).retry(.max) { count, error in
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background
        }.then({
            self.viewController?.participants = $0
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func fetch_folder(id: Int) {
        folderUseCase!.getFolder(id: id).then({
            self.folder = $0
            store.dispatch(FolderDispatcher.SET_SHOW_GALLERY(folder_uid: $0.uid, payload: $0))
            self.viewController?.sync(didFetchFolder: $0)
        })
    }
    
    func start(_ repository: StreamEntity, loggable: Bool = true, onNext: @escaping((_ stream: StreamEntity) -> ()) = {_ in }, onError: @escaping((_ error: Error) -> ()) = {_ in }) {
        self.repositories?.Streams?.append(repository)
        store.dispatch(StreamDispatcher.SET_LIVE(payload: repository))
        self.viewController?.syncPlayManager.duration = 0
        self.streamUseCase!.startLive(stream: repository, loggable: loggable, in: self.viewController!.repository).then({ repository in
            if self.repositories?.Streams == nil {
                self.repositories?.Streams = StreamEntities()
                self.repositories?.Streams?.append(repository)
            }
            guard let streams = self.repositories?.Streams else { return }
            for enumrate in streams.items.enumerated() {
                if enumrate.element.is_live || enumrate.element == repository {
                    streams.items[enumrate.offset] = repository
                }
            }
            self.syncingStream = true
            self.seekingStream = false
            self.viewController?.streamChannelPresenter(shouldStart: repository)
            self.viewController?.syncPlayManager.reset()
            self.viewController?.syncPlayManager.stream = repository
            self.viewController?.syncPlayManager.ready_status = .readyToPlay
            self.viewController?.syncPlayManager.host = true
            onNext(repository)
            StreamChannelConnector.shared.syncStream(syncPlayManager: self.viewController!.syncPlayManager).then({})
        }).catch({
            onError($0)
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }

    
    func increments() {
        guard self.viewController?.repositories.count ?? 0 > 0 else { self.fetchMessages(); return }
        self.messageUseCase!.getIncrementMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            guard self.repositories != nil else { return }
            self.read()
            self.viewController?.streamChannelPresenter(didIncrements: $0)
        })
    }
    
    func decrements() {
        guard self.viewController?.repositories.count ?? 0 > 0 else { self.fetchMessages(); return }
        self.messageUseCase!.getDecrementMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.streamChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            guard self.repositories != nil else { return }
            self.viewController?.streamChannelPresenter(didDecrements: $0)
        })
    }
    
    func increment(_ streamable: StreamableEntity) {
        switch streamable.streamable_type {
        case .File:
            guard let file = streamable as? FileEntity else { return }
            folderUseCase!.getDecrementGuildFile(file: file, is_playlist: true, in: self.viewController!.current_guild).then({
                self.viewController?.syncCurrentMember(didFetchIncrementRepository: $0)
            })
        case .FileReference:
            guard let reference = streamable as? FileReferenceEntity else { return }
            folderUseCase!.getIncrementReference(reference: reference).then({
                self.viewController?.syncCurrentMember(didFetchIncrementRepository: $0)
            })
        default: break
        }
    }
    
    func decrement(_ streamable: StreamableEntity) {
        switch streamable.streamable_type {
        case .File:
            guard let file = streamable as? FileEntity else { return }
            self.folderUseCase!.getIncrementGuildFile(file: file, is_playlist: true, in: self.viewController!.current_guild).then({
                self.viewController?.syncCurrentMember(didFetchDecrementRepository: $0)
            })
        case .FileReference:
            guard let reference = streamable as? FileReferenceEntity else { return }
            self.folderUseCase!.getDecrementReference(reference: reference).then({
                self.viewController?.syncCurrentMember(didFetchDecrementRepository: $0)
            })
        default: break
        }
    }
    
    func willTyping() {
        ChannelConnector.shared.didEditInput().then({})
    }
    
    func didTyping() {
        ChannelConnector.shared.endEditInput().then({})
    }
    
    func create(_ repository: ReactionEntity) {
        messageUseCase!.create(reaction: repository).then({
            ChannelConnector.shared.didReaction(reaction: $0).then({})
        })
    }
    
    func create(_ repository: MessageEntity) {
        messageUseCase!.create(
            message: repository
        ).then({
            self.viewController?.streamChannelPresenter(didCreated: $0)
            $0.Sender = self.current_member
            ChannelConnector.shared.didCreateMessage(message: $0).then({})
            self.increments()
        }).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func update(_ repository: MessageEntity) {
        messageUseCase!.update(
            message: repository
        ).then({
            ChannelConnector.shared.didUpdateMessage(message: $0).then({})
            self.viewController?.streamChannelPresenter(didCreated: $0)
        }).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func read() {
        guard let channel = StreamChannelConnector.shared.current_channel else { return }
        self.streamChannelUseCase!.reads(
            channel: channel.toNewMemoryChannel()
        ).then({_ in
        }).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func switchLoop(is_loop: Bool) {
        guard let manager = self.viewController?.syncPlayManager else { return }
        manager.loop = is_loop
        streamChannelUseCase!.update(
            channel: self.viewController!.repository,
            is_loop: is_loop
        ).then({
            _ in
            StreamChannelConnector.shared.changePlayerLoop(syncPlayManager: manager)
        }).catch({
            self.viewController?.streamChannelPresenter(onError: $0)
        })
    }
    
    func nilCheckRepositories() {
        if repositories == nil {
            repositories = StreamChannelEntity()
        }
    }
    
    func toStreamableSelects() {
        self.wireframe?.toStreamableSelects()
    }
    
    func toMemberMenu(repository: MemberEntity) {
        self.wireframe?.toMemberMenu(repository: repository)
    }
    
    func toMessageMenu(repository: MessageEntity) {
        self.wireframe?.toMessageMenu(repository: repository)
    }
    
    func toWebBrowser(_ url: URL, repositories: FileEntities? = nil) {
        self.wireframe?.toWebBrowser(url, repositories: repositories)
    }
    
    func toImageViewer(repository: FileEntity) {
        self.wireframe?.toImageViewer(repository: repository)
    }
    
    func toChannelMenu() {
        self.wireframe?.toChannelMenu()
    }
    
    func toStreamChannelInfo() {
        self.wireframe?.toStreamChannelInfo()
    }
    
    func toParticipants() {
        self.wireframe?.toParticipants()
    }
}
extension StreamChannelPresenter: StreamChannelConnectorDelegate {
    func checkReadyStream() -> Bool {
        return (
            self.viewController!.syncPlayManager.status == .readyToPlay ||
            self.viewController!.syncPlayManager.status == .readyToPause
        ) && (self.viewController?.syncPlayManager.host ?? false)
    }

    func handleSyncStream() {
        guard syncingStream, checkReadyStream() else { return }
        self.viewController?.syncPlayManager.completed = true
        self.viewController?.streamChannelPresenter(didSyncStream: self.viewController!.syncPlayManager)
        syncingStream = false
        firstSyncingStream = false
    }
    
    func handleSyncSeekStream() {
        guard seekingStream, checkReadyStream() else { return }
        self.viewController?.syncPlayManager.completed = true
        self.viewController?.streamChannelPresenter(didSeekStream: self.viewController!.syncPlayManager)
        seekingStream = false
    }
    
    func syncStream(stream: StreamEntity) {
        self.viewController?.syncPlayManager.stream = stream
        StreamChannelConnector.shared.syncStream(
            syncPlayManager: self.viewController!.syncPlayManager
        ).then({})
    }
    
    func pause() {
        self.viewController?.syncPlayManager.status = .paused
        self.viewController?.syncPlayManager.ready_status = .readyToPause
        StreamChannelConnector.shared.changePlayerStatus(
            syncPlayManager: self.viewController!.syncPlayManager
        ).then({})
        StreamChannelConnector.shared.didStopPlayer().then({})
    }
    
    func play() {
        self.viewController?.syncPlayManager.status = .playing
        self.viewController?.syncPlayManager.ready_status = .readyToPlay
        StreamChannelConnector.shared.changePlayerStatus(
            syncPlayManager: self.viewController!.syncPlayManager
        ).then({})
        StreamChannelConnector.shared.didPlayPlayer().then({})
    }
    
    func seek(at: Float) {
        seekingStream = true
        syncingStream = false
        firstSyncingStream = false
        self.viewController?.peerConnections.resetStatus()
        self.viewController?.syncPlayManager.duration = at
        self.viewController?.syncPlayManager.host = true
        StreamChannelConnector.shared.didSeekPlayer(syncPlayManager: self.viewController!.syncPlayManager).then({})
    }
    
    func fetchRequest() {
        guard !viewController!.syncPlayManager.completed && !viewController!.syncPlayManager.host else { return }
        StreamChannelConnector.shared.fetchRequest(syncPlayManager: self.viewController!.syncPlayManager).then({})
        fetchTimer?.invalidate()
        fetchTimer = nil
        fetchTimer = Timer.scheduledTimer(withTimeInterval: LiveConfig.fetch_request_timer_duration, repeats: true) { [weak self] (timer) in
            if !(self?.viewController?.syncPlayManager.completed ?? false) {
                guard let self = self else { return }
                StreamChannelConnector.shared.fetchRequest(syncPlayManager: self.viewController!.syncPlayManager).then({})
            } else {
                timer.invalidate()
            }
        }
    }
    
    func changePlayerStatus(_ newValue: SyncPlayManagerStatus) {
        if self.viewController!.syncPlayManager.status != newValue {
            self.viewController!.syncPlayManager.status = newValue
            StreamChannelConnector.shared.changePlayerStatus(syncPlayManager: self.viewController!.syncPlayManager).then({})
        }
    }
    
    func fetchResponse(syncPlayManager: SyncPlayManagerModel) {
        guard let model = viewController?.peerConnections.filter({ $0.syncPlayManager == syncPlayManager }).first else { return }
        StreamChannelConnector.shared.fetchResponse(syncPlayManager: self.viewController!.syncPlayManager, receiver_id: model.id).then({})
    }
    
    func transferHost(onSuccess: (() -> ())? = nil) {
        guard let vc = viewController, vc.syncPlayManager.host, let nextHost = vc.peerConnections.syncings.first else {
            viewController?.syncPlayManager.host = viewController?.peerConnections.syncings.count ?? 0 == 0
            viewController?.syncPlayManager.completed = viewController?.peerConnections.syncings.count ?? 0 == 0
            onSuccess?()
            return
        }
        StreamChannelConnector.shared.transferHost(
            syncPlayManager: vc.syncPlayManager,
            receiver_id: nextHost.id
        ).then({
            vc.syncPlayManager.completed = false
            vc.syncPlayManager.host = false
            onSuccess?()
        })
    }
    
    func streamChannelConnector(event: WebsocketStreamChannelEvent) {
        switch event {
            
        case .changePlayerStatus(let id, let syncPlayManager, let member):
            guard let model = self.viewController?.peerConnections.getConnection(id: id), model.user?.id == member.UserId else { break }
            model.syncPlayManager = syncPlayManager
            self.handleSyncStream()
            self.handleSyncSeekStream()
            
        case .changePlayerLoop(let id, let syncPlayManager, let member):
            guard let model = self.viewController?.peerConnections.getConnection(id: id), model.user?.id == member.UserId else { break }
            model.syncPlayManager = syncPlayManager
            self.viewController?.streamChannelPresenter(didSwitchLoop: syncPlayManager.loop)

        case .syncStream(let id, let syncPlayManager, let member):
            guard let model = self.viewController?.peerConnections.getConnection(id: id), model.user?.id == member.UserId else { break }
            self.viewController?.peerConnections.syncingTarget = syncPlayManager
            model.syncPlayManager = syncPlayManager
            self.viewController?.peerConnections.resetStatus()
            self.viewController?.syncPlayManager.reset()
            self.viewController?.syncPlayManager.ready_status = syncPlayManager.ready_status
            self.repositories?.Streams?.append(syncPlayManager.stream)
            self.viewController?.streamChannelPresenter(willSyncStream: syncPlayManager)
            self.handleSyncStream()
            
        case .fetchRequest(let id, let receiver_id, let syncPlayManager, let member):
            guard let model = self.viewController?.peerConnections.getConnection(id: id), model.user?.id == member.UserId else { break }
            if let unwrapped = receiver_id, unwrapped != WebSocketHandler.shared.id { break }
            model.syncPlayManager = syncPlayManager
            self.viewController!.streamChannelPresenter(fetchRequest: syncPlayManager, from: model)

        case .fetchResponse(let id, let receiver_id, let syncPlayManager, let member):
            guard let model = self.viewController?.peerConnections.getConnection(id: id), model.user?.id == member.UserId, receiver_id == WebSocketHandler.shared.id, syncPlayManager.completed, !(self.viewController?.syncPlayManager.completed ?? true) else { break }
            if firstSyncingStream, !syncPlayManager.host { return }
            self.firstSyncingStream = false
            model.syncPlayManager = syncPlayManager
            self.viewController?.syncPlayManager.ready_status = syncPlayManager.ready_status
            self.viewController?.peerConnections.fetchingTarget = syncPlayManager
            self.viewController?.syncPlayManager.duration = syncPlayManager.duration
            self.viewController?.syncPlayManager.completed = true
            self.viewController?.streamChannelPresenter(fetchResponse: syncPlayManager, from: model)
        
        case .didStopPlayer(_): self.viewController?.streamChannelPresenterStopPlayer()
            
        case .didPlayPlayer(_): self.viewController?.streamChannelPresenterPlayPlayer()
            
        case .didSeekPlayer(_, let syncPlayManager):
            self.viewController?.peerConnections.seekingTarget = syncPlayManager
            self.viewController?.syncPlayManager.reset()
            self.viewController?.syncPlayManager.completed = false
            self.viewController?.syncPlayManager.ready_status = syncPlayManager.ready_status
            self.viewController?.syncPlayManager.duration = syncPlayManager.duration
            self.viewController?.streamChannelPresenterSeekPlayer(syncPlayManager: syncPlayManager)
            
        case .transferHost(let id, let receiver_id, let syncPlayManager, let member):
            guard let model = self.viewController?.peerConnections.getConnection(id: id), model.user?.id == member.UserId, receiver_id == WebSocketHandler.shared.id, syncPlayManager.host else { break }
            self.viewController?.syncPlayManager.completed = true
            self.viewController?.syncPlayManager.host = true
            self.firstSyncingStream = false
            self.handleSyncStream()
            self.handleSyncSeekStream()
            self.viewController?.streamChannelPresenter(shouldTransferHost: syncPlayManager, from: model)
            
        default: break
        }
    }
    
    func streamChannelConnector(onError: Error) {
    }
}

extension StreamChannelPresenter: ChannelConnectorDelegate {
    func channelConnector(event: WebsocketChannelEvent) {
        switch event {
        case .fetchRequest: self.reload()
        case .didCreateMessage(let member, let message):
            DispatchQueue.main.async {
                guard let current_channel = ChannelConnector.shared.current_channel, let uid = current_channel.messageable_uid else { return }
                let entities = MessageEntities()
                entities.append(message)
                message.Sender = member
                self.read()
                store.dispatch(MessageDispatcher.ADD_CHANNEL_MESSAGES(channel_uid: uid, payload: entities))
                self.viewController?.streamChannelPresenter(shouldReload: self)
            }
        case .didUpdateMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_CACHES(payload: entities))
                self.viewController?.streamChannelPresenter(shouldReload: self)
            }
        case .didDeleteMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_DELETES(payload: entities))
                self.viewController?.streamChannelPresenter(shouldReload: self)
            }
        case .didEditInput(let member):
            self.viewController?.streamChannelPresenter(willTyping: member)
        case .endEditInput(let member):
            self.viewController?.streamChannelPresenter(didTyping: member)
        case .didReaction(_, let reaction):
            self.viewController?.streamChannelPresenter(didCreated: reaction)
        default: break
        }
    }
    
    func channelConnector(onError: Error) {
    }
}

extension StreamChannelPresenter: WebSocketHandlerDelegate {
    func webSocketHandler(event: WebsocketEvent) {
        switch event {
        case .check_first(let roomname):
            guard roomname == viewController!.current_guild.uid + WebSocketHandler.separator + viewController!.repository.messageable_uid! else { break }
            self.viewController?.streamChannelPresenter(didCheckFirst: self)
            self.handleSyncStream()
        default: break
        }
    }
}

extension StreamChannelPresenter {
    func configureObserver() {
        AppConfig.isBackgroundEvent
            .subscribe(
                onNext: { [weak self] result in
                    DispatchQueue.main.async {
                        result ?
                            self?.viewController?.streamChannelPresenter(willEnterBackground: result) :
                            self?.viewController?.streamChannelPresenter(willExitBackground: result)
                    }
                }
            ).disposed(
                by: disposeBag
            )
    }
}

extension StreamChannelPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let current_channel = self.repositories, let caches = state.message.channelCacheMessages[current_channel.uid] {
            self.caches = caches
            self.viewController?.streamChannelPresenter(didCache: caches, shouldScroll: false, in: self)
        }
    }
}
