//
//  GuildMenuPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol GuildMenuPresenterOutput: class {
    func sync(didFetchRepository repository: GuildEntity)
    func guildMenuPresenter(didLeave repository: GuildEntity, in guildMenuPresenter: GuildMenuPresenter)
    func guildMenuPresenter(onError error: Error, in guildMenuPresenter: GuildMenuPresenter)
}

@objcMembers
class GuildMenuPresenter: PresenterImpl {
    typealias WireframeType = GuildMenuWireframe
    typealias RepositoriesType = GuildEntity
    typealias ViewControllerType = GuildMenuPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func toChannelNew() {
        self.wireframe?.toChannelNew(section: SectionChannelEntity())
    }
    
    func toSectionNew() {
        self.wireframe?.toSectionNew()
    }
    
    func toGuildSetting() {
        self.wireframe?.toGuildSetting()
    }
    
    func toGuildNotificationSetting() {
        self.wireframe?.toGuildNotificationSetting()
    }
    
    func toGuildShare() {
        self.wireframe?.toGuildShare()
    }
    
    func leave(_ repository: GuildEntity) {
        guildUseCase!.leave(
            guild: repository.toNewMemory()
        ).then({ _ in
            GuildConnector.shared.fetchRequest().then({})
            self.viewController?.guildMenuPresenter(didLeave: repository, in: self)
        }).catch({
            self.viewController?.guildMenuPresenter(onError: $0, in: self)
        })
    }
}

extension GuildMenuPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
