//
//  GuildSearchPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol GuildSearchPresenterOutput: class {
    var keyword: String? { get set }
    var new_keyword: String? { get set }
    func sync(didFetchRepositories repositories: GuildEntities)
}

@objcMembers
class GuildSearchPresenter: PresenterImpl {
    typealias WireframeType = GuildSearchWireframe
    typealias RepositoriesType = GuildEntities
    typealias ViewControllerType = GuildSearchPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        if let unwrapped = viewController?.keyword {
            searchUseCase!.searchGuilds(
                keyword: unwrapped
            ).then({
                self.repositories = $0
                self.viewController?.sync(didFetchRepositories: $0)
            })
        }
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func toGuildSearch(keyword: String) {
        guard let unwrapped = viewController?.new_keyword, unwrapped.count > 0 else { return }
        store.dispatch(SearchDispatcher.SET_GUILD_KEYWORD(payload: unwrapped))
        self.wireframe?.toGuildSearch()
    }
    
    func toGuildQRCodeReader() {
        self.wireframe?.toGuildQRCodeReader()
    }
    
    func toGuildInfo(repository: GuildEntity) {
        self.wireframe?.toGuildInfo(repository)
    }
    
    func searchMore() {
        guard let unwrapped = viewController?.keyword else { return }
        searchUseCase!.searchMoreGuilds(keyword: unwrapped).then({
            self.repositories = $0
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func afterFetched(_ repositories: GuildEntities) {
        self.repositories = repositories
        viewController?.sync(didFetchRepositories: repositories)
    }
}

extension GuildSearchPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
