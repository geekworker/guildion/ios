//
//  FileMenuPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/10.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol FileMenuPresenterOutput: class {
    func sync(didFetchRepository repository: FileEntity)
    func fileMenuPresenter(willToStreamChannel repository: StreamChannelEntity, in fileMenuPresenter: FileMenuPresenter)
    func fileMenuPresenter(didDelete fileMenuPresenter: FileMenuPresenter)
    func fileMenuPresenter(onError error: Error, in fileMenuPresenter: FileMenuPresenter)
    var current_guild: GuildEntity { get set }
    var current_member: MemberEntity { get set }
}

@objcMembers
class FileMenuPresenter: PresenterImpl {
    typealias WireframeType = FileMenuWireframe
    typealias RepositoriesType = FileEntity
    typealias ViewControllerType = FileMenuPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(_ repository: FileEntity) {
        self.repositories = repository
        fileUseCase!.get(
            id: repository.id ?? 0
        ).then({
            self.viewController?.sync(didFetchRepository: $0)
        })
    }
    
    func copy_url(_ repository: FileEntity) {
        guard let current_guild = self.viewController?.current_guild else { return }
        UIPasteboard.general.string = repository.provider == .youtube ? repository.url : "\(Constant.APP_URL)/guild/\(current_guild.uid)/file/\(repository.uid)"
    }
    
    func startPrivateStreamChannel(repository: StreamableEntity) {
        guard let guild = self.viewController?.current_guild, let current_member = self.viewController?.current_member else { return }
        let stream = StreamEntity()
        stream.Streamable = repository
        stream.StreamableId = repository.streamable_id
        stream.streamable_type = repository.streamable_type ?? .File
        streamChannelUseCase!.start(
            guild: guild.toNewMemory(),
            target: current_member.toNewMemory(),
            stream: stream
        ).then({
            self.viewController?.fileMenuPresenter(willToStreamChannel: $0, in: self)
        }).catch({
            self.viewController?.fileMenuPresenter(onError: $0, in: self)
        })
    }
    
    func startStreamChannel(repository: StreamableEntity) {
        guard let guild = self.viewController?.current_guild else { return }
        let stream = StreamEntity()
        stream.Streamable = repository
        stream.StreamableId = repository.streamable_id
        stream.streamable_type = repository.streamable_type ?? .File
        streamChannelUseCase!.start(
            guild: guild.toNewMemory(),
            target: nil,
            stream: stream
        ).then({
            self.viewController?.fileMenuPresenter(willToStreamChannel: $0, in: self)
        }).catch({
            self.viewController?.fileMenuPresenter(onError: $0, in: self)
        })
    }
    
    func deleteFiles(repositories: FileEntities) {
        fileUseCase!.deletes(
            files: repositories
        ).then({
            self.viewController?.fileMenuPresenter(didDelete: self)
        }).catch({
            self.viewController?.fileMenuPresenter(onError: $0, in: self)
        })
        FolderConnector.shared.didDeleteFile(file: repositories.first ?? FileEntity()).then({})
    }
    
    func deleteFileReferences(repositories: FileReferenceEntities) {
        fileUseCase?.deleteReferences(
            references: repositories
        ).then({
            self.viewController?.fileMenuPresenter(didDelete: self)
        }).catch({
            self.viewController?.fileMenuPresenter(onError: $0, in: self)
        })
    }
    
    func delete(_ repository: MessageEntity) {
        messageUseCase!.delete(
            message: repository
        ).then({
            ChannelConnector.shared.didDeleteMessage(message: repository).then({})
            self.viewController?.fileMenuPresenter(didDelete: self)
        }).catch({
            self.viewController?.fileMenuPresenter(onError: $0, in: self)
        })
    }
    
    func toStreamChannel(repository: StreamChannelEntity) {
        self.wireframe?.checkLiveAndToStreamChannel(repository: repository)
    }
    
    func toFileEdit() {
        self.wireframe?.toFileEdit()
    }
}

extension FileMenuPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}


