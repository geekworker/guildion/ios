//
//  ChannelRoleEditPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol ChannelRoleEditPresenterOutput: class {
    func sync(didFetchRepository repository: ChannelRoleableProtocol)
    func channelRoleEditPresenter(didCreated repository: ChannelRoleableProtocol, in channelRoleEditPresenter: ChannelRoleEditPresenter)
    func channelRoleEditPresenter(didUpdated repository: ChannelRoleableProtocol, in channelRoleEditPresenter: ChannelRoleEditPresenter)
    func channelRoleEditPresenter(didDeleted channelRoleEditPresenter: ChannelRoleEditPresenter)
    func channelRoleEditPresenter(onError error: Error, in channelRoleEditPresenter: ChannelRoleEditPresenter)
}

@objcMembers
class ChannelRoleEditPresenter: PresenterImpl {
    typealias WireframeType = ChannelRoleEditWireframe
    typealias RepositoriesType = ChannelRoleableProtocol
    typealias ViewControllerType = ChannelRoleEditPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var roles: RoleEntities = RoleEntities()
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func create(repository: RoleEntity, channel: ChannelableProtocol) {
        switch channel.channelable_type.superChannel {
        case .TextChannel:
            textChannelUseCase!.create(
                channel: channel as! TextChannelEntity,
                role: repository
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didCreated: $0, in: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.create(
                channel: channel as! StreamChannelEntity,
                role: repository
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didCreated: $0, in: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            dmChannelUseCase!.create(
                channel: channel as! DMChannelEntity,
                role: repository
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didCreated: $0, in: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func update(repository: ChannelRoleableProtocol) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            textChannelUseCase!.update(
                channel_role: repository as! TextChannelRoleEntity
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didUpdated: $0, in: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.update(
                channel_role: repository as! StreamChannelRoleEntity
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didUpdated: $0, in: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            dmChannelUseCase!.update(
                channel_role: repository as! DMChannelRoleEntity
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didUpdated: $0, in: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func delete(repository: RoleEntity, channel: ChannelableProtocol) {
        switch channel.channelable_type.superChannel {
        case .TextChannel:
            textChannelUseCase!.delete(
                channel: channel as! TextChannelEntity,
                role: repository
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didDeleted: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.delete(
                channel: channel as! StreamChannelEntity,
                role: repository
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didDeleted: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            dmChannelUseCase!.delete(
                channel: channel as! DMChannelEntity,
                role: repository
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelRoleEditPresenter(didDeleted: self)
            }).catch({
                self.viewController?.channelRoleEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
}

extension ChannelRoleEditPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
