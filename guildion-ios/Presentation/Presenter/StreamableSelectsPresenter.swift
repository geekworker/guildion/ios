//
//  StreamableSelectsPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol StreamableSelectsPresenterOutput: class {
    var current_guild: GuildEntity { get set }
    var current_member: MemberEntity { get set }
    func sync(didFetchRepositories repositories: StreamableModel)
    func sync(didFetchFolders repositories: FolderEntities)
    func syncCurrentMember(didFetchRepository repository: MemberEntity)
    func streamableSelectsPresenter(shouldStart repository: StreamEntity)
    func streamableSelectsPresenter(didUpload repositories: FileEntities)
    func streamableSelectsPresenter(onError error: Error)
}

@objcMembers
class StreamableSelectsPresenter: PresenterImpl {
    typealias WireframeType = StreamableSelectsWireframe
    typealias RepositoriesType = StreamableModel
    typealias ViewControllerType = StreamableSelectsPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var folders: FolderEntities?
    var current_member: MemberEntity?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func fetch() {
        folderUseCase!.getCurrentFolders(
            current_guild: self.viewController!.current_guild,
            is_playlist: true
        ).then({
            self.folders = $0
            self.viewController?.sync(didFetchFolders: $0)
        })
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func toFileNewFromURL() {
        self.wireframe?.toFileNewFromURL()
    }
    
    func toFileNewFromBrowser() {
        self.wireframe?.toFileNewFromBrowser()
    }
    
    func start(_ repository: StreamableModel) {
        guard let vc = LiveConfig.playingStreamChannelViewController else { return }
        self.repositories = repository
        let entity = StreamEntity()
        if let files = repository.files {
            entity.Files = files
        } else if let file = repository.file {
            entity.Streamable = file
            entity.streamable_type = .File
            entity.StreamableId = file.id
        } else if let reference = repository.reference {
            entity.Streamable = reference
            entity.streamable_type = .FileReference
            entity.StreamableId = reference.id
        }
        store.dispatch(StreamDispatcher.SET_LIVE(payload: entity))
        vc.presenter?.start(entity, onNext: {
            self.viewController?.streamableSelectsPresenter(shouldStart: $0)
        }, onError: {
            self.viewController?.streamableSelectsPresenter(onError: $0)
        })
    }
    
    func uploads(_ repositories: FileEntities) {
        guard repositories.count > 0 else { return }
        fileUseCase!.creates(
            files: repositories,
            in: self.viewController!.current_guild
        ).then({
            FolderConnector.shared.didCreateFile(file: $0.first ?? FileEntity()).then({})
            if let guild = self.viewController?.current_guild {
                store.dispatch(FileDispatcher.RESET_GUILD_CHACHE_FILES(payload: guild.uid))
            }
            self.viewController?.streamableSelectsPresenter(didUpload: $0)
        }).catch({
            self.viewController?.streamableSelectsPresenter(onError: $0)
        })
    }
    
    func toSelectablePlaylist(repository: FolderEntity) {
        self.wireframe?.toSelectablePlaylist(repository: repository)
    }
    
    func toSelectableAllFiles() {
        self.wireframe?.toSelectableAllFiles()
    }
    
    func toWebBrowser() {
        self.wireframe?.toWebBrowser()
    }
}

extension StreamableSelectsPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}


