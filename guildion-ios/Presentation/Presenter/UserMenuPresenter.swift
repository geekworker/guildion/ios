//
//  UserMenuPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/25.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol UserMenuPresenterOutput: class {
    var current_guild: GuildEntity { get set }
    var current_member: MemberEntity { get set }
    func sync(didFetchRepository repository: UserEntity)
    func userMenuPresenter(didBlock repository: UserEntity, in memberMenuPresenter: UserMenuPresenter)
    func userMenuPresenter(onError error: Error, in memberMenuPresenter: UserMenuPresenter)
}

@objcMembers
class UserMenuPresenter: PresenterImpl {
    typealias WireframeType = UserMenuWireframe
    typealias RepositoriesType = UserEntity
    typealias ViewControllerType = UserMenuPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(_ repository: UserEntity) {
    }
    
    func block(repository: UserEntity, from guild: GuildEntity) {
        guildUseCase?.block(
            guild: guild,
            target: repository
        ).then({ _ in
            self.viewController?.userMenuPresenter(didBlock: repository, in: self)
        })
    }
    
    func unblock(repository: UserEntity, from guild: GuildEntity) {
    }
}

extension UserMenuPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
