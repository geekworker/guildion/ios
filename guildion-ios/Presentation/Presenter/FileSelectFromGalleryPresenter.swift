//
//  FileSelectFromFolderPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/20.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol FileSelectFromFolderPresenterOutput: class {
    func fileSelectFromFolderPresenter(didFetch repositories: FileEntities, in fileSelectFromFolderPresenter: FileSelectFromFolderPresenter)
    func fileSelectFromFolderPresenter(didIncrement repositories: FileEntities, in fileSelectFromFolderPresenter: FileSelectFromFolderPresenter)
    func fileSelectFromFolderPresenter(didDecrement repositories: FileEntities, in fileSelectFromFolderPresenter: FileSelectFromFolderPresenter)
    var current_guild: GuildEntity { get set }
}

@objcMembers
class FileSelectFromFolderPresenter: PresenterImpl {
    typealias WireframeType = FileSelectFromFolderWireframe
    typealias RepositoriesType = FileEntities
    typealias ViewControllerType = FileSelectFromFolderPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var categories: CategoryEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch() {
        folderUseCase!.getCurrentGuildFiles(
            current_guild: self.viewController!.current_guild,
            is_playlist: true
        ).then({
            self.repositories = $0
            self.viewController?.fileSelectFromFolderPresenter(didFetch: $0, in: self)
        })
    }
    
    func increments() {
        folderUseCase!.getIncrementGuildFiles(
            current_guild: self.viewController!.current_guild,
            is_playlist: true
        ).then({
            self.repositories = $0
            self.viewController?.fileSelectFromFolderPresenter(didIncrement: $0, in: self)
        })
    }
    
    func decrements() {
        folderUseCase!.getDecrementGuildFiles(
            current_guild: self.viewController!.current_guild,
            is_playlist: true
        ).then({
            self.repositories = $0
            self.viewController?.fileSelectFromFolderPresenter(didDecrement: $0, in: self)
        })
    }
}

extension FileSelectFromFolderPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}

