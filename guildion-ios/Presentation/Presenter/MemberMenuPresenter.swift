//
//  MemberMenuPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol MemberMenuPresenterOutput: class {
    var current_guild: GuildEntity { get set }
    var current_member: MemberEntity { get set }
    func sync(didFetchRepository repository: MemberEntity)
    func memberMenuPresenter(willToStreamChannel repository: StreamChannelEntity, in memberMenuPresenter: MemberMenuPresenter)
    func memberMenuPresenter(onError error: Error, in memberMenuPresenter: MemberMenuPresenter)
}

@objcMembers
class MemberMenuPresenter: PresenterImpl {
    typealias WireframeType = MemberMenuWireframe
    typealias RepositoriesType = MemberEntity
    typealias ViewControllerType = MemberMenuPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(_ repository: MemberEntity) {
        guard let id = repository.id else { return }
        self.memberUseCase?.get(
            id: id
        ).then({
            self.viewController?.sync(didFetchRepository: $0)
        })
    }
    
    func copy_url(_ repository: MemberEntity) {
        guard let current_guild = viewController?.current_guild else { return }
        UIPasteboard.general.string = "\(Constant.APP_URL)/guild/\(current_guild.uid)/member/\(repository.uid)"
    }
    
    func toMemberEdit() {
        self.wireframe?.toMemberEdit()
    }
    
    func toDMChannel(repository: MemberEntity) {
        guard let guild = viewController?.current_guild else { return }
        dmChannelUseCase!.start(guild: guild.toNewMemory(), target: repository.toNewMemory()).then({
            self.wireframe?.toDMChannel(repository: $0, current_guild: self.viewController!.current_guild, current_member: self.viewController!.current_member)
        }).catch({
            self.viewController?.memberMenuPresenter(onError: $0, in: self)
        })
    }
    
    func startStreamChannel(repository: MemberEntity) {
        guard let guild = viewController?.current_guild else { return }
        streamChannelUseCase!.start(
            guild: guild.toNewMemory(), target: repository.toNewMemory(), stream: nil
        ).then({
            self.viewController?.memberMenuPresenter(willToStreamChannel: $0, in: self)
        }).catch({
            self.viewController?.memberMenuPresenter(onError: $0, in: self)
        })
    }
    
    func startPlayChannel(repository: MemberEntity) {
        guard let guild = viewController?.current_guild else { return }
        streamChannelUseCase!.startMovie(
            guild: guild.toNewMemory(), target: repository.toNewMemory(), stream: nil
        ).then({
            self.viewController?.memberMenuPresenter(willToStreamChannel: $0, in: self)
        }).catch({
            self.viewController?.memberMenuPresenter(onError: $0, in: self)
        })
    }
    
    func startVoiceChannel(repository: MemberEntity) {
        guard let guild = viewController?.current_guild else { return }
        streamChannelUseCase!.startVoice(
            guild: guild.toNewMemory(), target: repository.toNewMemory(), stream: nil
        ).then({
            self.viewController?.memberMenuPresenter(willToStreamChannel: $0, in: self)
        }).catch({
            self.viewController?.memberMenuPresenter(onError: $0, in: self)
        })
    }
    
    func toStreamChannel(repository: StreamChannelEntity) {
        self.wireframe?.checkLiveAndToStreamChannel(repository: repository, current_guild: self.viewController!.current_guild, current_member: self.viewController!.current_member)
    }
}

extension MemberMenuPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
