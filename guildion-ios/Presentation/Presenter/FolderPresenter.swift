//
//  FolderPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol FolderPresenterOutput: class {
    func sync(didFetchRepository repository: FolderEntity)
    func folderPresenter(didCreate repositories: FileReferenceEntities, in folderPresenter: FolderPresenter)
    func folderPresenter(didDelete folderPresenter: FolderPresenter)
    func folderPresenter(onError error: Error, in folderPresenter: FolderPresenter)
}

@objcMembers
class FolderPresenter: PresenterImpl {
    typealias WireframeType = FolderWireframe
    typealias RepositoriesType = FolderEntity
    typealias ViewControllerType = FolderPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
        FolderConnector.shared.didLeave().then({})
    }
    
    func fetch(id: Int) {
        folderUseCase!.getFolder(
            id: id
        ).then({
            self.repositories = $0
            FolderConnector.shared.didJoin(current_folder: $0).then({})
            store.dispatch(FolderDispatcher.SET_SHOW_GALLERY(folder_uid: $0.uid, payload: $0))
            self.viewController?.sync(didFetchRepository: $0)
        })
    }
    
    func toFileSelect() {
        self.wireframe?.toFileSelect()
    }
    
    func createFileReferences(repositories: FileEntities, to folder: FolderEntity) {
        let references = FileReferenceEntities()
        references.items = repositories.compactMap({
            let reference = FileReferenceEntity()
            reference.File = $0
            reference.FileId = $0.id
            reference.Folder = folder
            reference.FolderId = folder.id
            return reference
        })
        fileUseCase!.createReferences(
            references: references
        ).then({
            self.viewController?.folderPresenter(didCreate: $0, in: self)
        }).catch({
            self.viewController?.folderPresenter(onError: $0, in: self)
        })
    }
    
    func deleteFileReferences(repositories: FileReferenceEntities) {
        fileUseCase!.deleteReferences(
            references: repositories
        ).then({
            self.viewController?.folderPresenter(didDelete: self)
        }).catch({
            self.viewController?.folderPresenter(onError: $0, in: self)
        })
    }
}

extension FolderPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
