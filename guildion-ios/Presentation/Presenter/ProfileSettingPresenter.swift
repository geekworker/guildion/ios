//
//  ProfileSettingPresenter.swift
//  nowy-ios
//
//  Created by Apple on 2020/07/10.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import ReSwift

protocol ProfileSettingPresenterOutput: class {
    func sync(didFetchRepository repository: SessionModel)
    func profileSettingPresenter(onError error: Error)
    func profileSettingPresenter(didUpdate repository: UserEntity)
}

@objcMembers
class ProfileSettingPresenter: PresenterImpl {
    typealias WireframeType = ProfileSettingWireframe
    typealias RepositoriesType = SessionModel
    typealias ViewControllerType = ProfileSettingPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        repositories?.user = CurrentUser.getCurrentUserEntity()
        self.viewController?.sync(didFetchRepository: repositories!)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func submit(_ repository: RepositoriesType) {
        guard let user = repository.user, let newUser = CurrentUser.getCurrentUserEntity()?.pureUser() else { return }
        newUser.picture_small = user.picture_small
        self.userUseCase!.update(user: newUser).then({
            self.viewController?.profileSettingPresenter(didUpdate: $0)
        }).catch({
            self.viewController?.profileSettingPresenter(onError: $0)
        })
    }
    
    func onError(_ error: Error) {
        self.viewController?.profileSettingPresenter(onError: error)
    }
}

extension ProfileSettingPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}

