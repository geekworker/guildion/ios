//
//  MessageMenuPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol MessageMenuPresenterOutput: class {
    func sync(didFetchRepository repository: MessageEntity)
    func messageMenuPresenter(didDelete repository: MessageEntity, messageMenuPresenter: MessageMenuPresenter)
    func messageMenuPresenter(didUpdate repository: MessageEntity, messageMenuPresenter: MessageMenuPresenter)
    func messageMenuPresenter(onError error: Error, messageMenuPresenter: MessageMenuPresenter)
    var current_guild: GuildEntity { get set }
    var current_channel: MessageableEntity { get set }
    var current_member: MemberEntity { get set }
}

@objcMembers
class MessageMenuPresenter: PresenterImpl {
    typealias WireframeType = MessageMenuWireframe
    typealias RepositoriesType = MessageEntity
    typealias ViewControllerType = MessageMenuPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func edit(_ repository: MessageEntity) {
    }
    
    func delete(_ repository: MessageEntity) {
        messageUseCase!.delete(message: repository).then({
            ChannelConnector.shared.didDeleteMessage(message: repository).then({})
            self.viewController?.messageMenuPresenter(didDelete: repository, messageMenuPresenter: self)
        }).catch({
            self.viewController?.messageMenuPresenter(onError: $0, messageMenuPresenter: self)
        })
    }
    
    func mention(_ repository: MessageEntity) {
        // guard let sender = repository.Sender else { return }
    }
    
    func copy(_ repository: MessageEntity) {
        UIPasteboard.general.string = repository.text
    }
    
    func copy_url(_ repository: MessageEntity) {
        guard let current_guild = viewController?.current_guild, let current_channel = viewController?.current_channel, let channel_uid = current_channel.messageable_uid else { return }
        UIPasteboard.general.string = "\(Constant.APP_URL)/guild/\(current_guild.uid)/\(channel_uid)/\(repository.uid)"
    }
}

extension MessageMenuPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}

