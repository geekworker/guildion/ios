//
//  GuildSettingPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol GuildSettingPresenterOutput: class {
    func sync(didFetchRepository repository: GuildEntity)
    func guildSettingPresenter(afterDeleted repository: GuildEntity, in guildSettingPresenter: GuildSettingPresenter)
    func guildSettingPresenter(afterUpdated repository: GuildEntity, in guildSettingPresenter: GuildSettingPresenter)
    func guildSettingPresenter(didJoin repository: GuildEntity, in guildSettingPresenter: GuildSettingPresenter)
    func guildSettingPresenter(didSuccess guildSettingPresenter: GuildSettingPresenter)
    func guildSettingPresenter(onError error: Error, in guildSettingPresenter: GuildSettingPresenter)
}

@objcMembers
class GuildSettingPresenter: PresenterImpl {
    typealias WireframeType = GuildSettingWireframe
    typealias RepositoriesType = GuildEntity
    typealias ViewControllerType = GuildSettingPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(_ repository: GuildEntity) {
        self.guildUseCase!.getGuild(
            uid: repository.uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            return true
        }.then({ repository in
            self.repositories = repository
            GuildConnector.shared.current_permission.Guild.current_guild = repository
            if let current_member = GuildDispatcher.getCurrentMemberFromGuild(guild: repository) {
                GuildConnector.shared.current_member = current_member
                GuildConnector.shared.current_permission.Guild.current_member_roles = current_member.MemberRoles ?? MemberRoleEntities()
                GuildConnector.shared.didJoin(current_guild: repository, current_member: current_member).then({ _ in
                    self.viewController?.sync(didFetchRepository: repository)
                })
            }
        })
    }
    
    func update(repository: GuildEntity) {
        guildUseCase!.update(
            guild: repository
        ).then({
            GuildConnector.shared.fetchRequest().then({})
            self.viewController?.guildSettingPresenter(afterUpdated: $0, in: self)
        }).catch({
            self.viewController?.guildSettingPresenter(onError: $0, in: self)
        })
    }
    
    func join(_ repository: GuildEntity) {
        self.repositories = repository
        GuildConnector.shared.didJoin(
            current_guild: self.repositories!,
            current_member: self.repositories?.Members?.filter({ $0.UserId == CurrentUser.getCurrentUserEntity()?.id }).first ?? MemberEntity()
        ).then({
            self.viewController?.guildSettingPresenter(didJoin: repository, in: self)
        })
    }
    
    func delete(repository: GuildEntity) {
        guildUseCase!.delete(guild: repository).then({
            GuildConnector.shared.fetchRequest().then({})
            self.viewController?.guildSettingPresenter(afterDeleted: repository, in: self)
        }).catch({
            self.viewController?.guildSettingPresenter(onError: $0, in: self)
        })
    }
    
    func report(guild: GuildEntity, description: String) {
        self.guildUseCase!.report(guild: guild, description: description).then({
            self.viewController?.guildSettingPresenter(didSuccess: self)
        }).catch({
            self.viewController?.guildSettingPresenter(onError: $0, in: self)
        })
    }
    
    func toGuildShare() {
        self.wireframe?.toGuildShare()
    }
    
    func toPlaylistsEdit() {
        self.wireframe?.toPlaylistsEdit()
    }
    
    func toSectionChannelsEdit() {
        self.wireframe?.toSectionChannelsEdit()
    }
}

extension GuildSettingPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let uid = self.repositories?.uid, let guild = state.guild.showGuilds[uid] {
            self.viewController?.sync(didFetchRepository: guild)
        }
    }
}
