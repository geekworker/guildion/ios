//
//  FoldersPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/23.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol FoldersPresenterOutput: class {
    func foldersPresenter(didCache repositories: FileEntities, in foldersPresenter: FoldersPresenter)
    func foldersPresenter(didIncrement repositories: FileEntities, in foldersPresenter: FoldersPresenter)
    func foldersPresenter(didDecrement repositories: FileEntities, in foldersPresenter: FoldersPresenter)
    func foldersPresenter(didCreate repositories: FileReferenceEntities, in foldersPresenter: FoldersPresenter)
    func foldersPresenter(didCreate repository: FolderEntity, in foldersPresenter: FoldersPresenter)
    func foldersPresenter(didDelete foldersPresenter: FoldersPresenter)
    func foldersPresenter(onError error: Error, in foldersPresenter: FoldersPresenter)
    func foldersPresenter(didRetry count: Int, in foldersPresenter: FoldersPresenter)
    func sync(didFetchRepositories repositories: FileEntities)
    func sync(didFetchFolders repositories: FolderEntities)
    func syncCurrentMember(didFetchRepository repository: MemberEntity)
    var is_playlist: Bool { get set }
    var current_guild: GuildEntity { get set }
    var current_member: MemberEntity { get set }
}

@objcMembers
class FoldersPresenter: PresenterImpl {
    typealias WireframeType = FoldersWireframe
    typealias RepositoriesType = FileEntities
    typealias ViewControllerType = FoldersPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var folders: FolderEntities?
    var caches: FileEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func join() {
        GuildConnector.shared.didJoin(current_guild: self.viewController!.current_guild, current_member: self.viewController!.current_member).then({ _ in })
    }
    
    func fetchFolders() {
        folderUseCase!.getCurrentFolders(
            current_guild: viewController!.current_guild,
            is_playlist: viewController?.is_playlist ?? false
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.foldersPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.folders = $0
            self.viewController?.sync(didFetchFolders: $0)
        })
    }
    
    func fetch() {
        folderUseCase!.getCurrentGuildFiles(
            current_guild: viewController!.current_guild,
            is_playlist: viewController?.is_playlist ?? false
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.foldersPresenter(didRetry: count, in: self)
            return true
        }.then({
            guard $0.count > 0 else { return }
            self.repositories = $0
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func increments() {
        folderUseCase!.getIncrementGuildFiles(
            current_guild: viewController!.current_guild,
            is_playlist: self.viewController?.is_playlist ?? false
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.foldersPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.repositories = $0
            self.viewController?.foldersPresenter(didIncrement: $0, in: self)
        })
    }
    
    func decrements() {
        folderUseCase!.getDecrementGuildFiles(
            current_guild: self.viewController!.current_guild,
            is_playlist: self.viewController?.is_playlist ?? false
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.foldersPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.repositories = $0
            self.viewController?.foldersPresenter(didDecrement: $0, in: self)
        })
    }
    
    func toNewFile() {
        self.wireframe?.toFileNew()
    }
    
    func toFolder() {
        self.wireframe?.toFolder()
    }
    
    func toPlaylist() {
        self.wireframe?.toPlaylist()
    }
    
    func toFoldersEdit() {
        self.wireframe?.toFoldersEdit()
    }
    
    func toFile(_ repository: FileEntity) {
        self.wireframe?.toFile(repository)
    }
    
    func createFileReferences(repositories: FileEntities, to folder: FolderEntity) {
        let references = FileReferenceEntities()
        references.items = repositories.compactMap({
            let reference = FileReferenceEntity()
            reference.File = $0
            reference.FileId = $0.id
            reference.Folder = folder
            reference.FolderId = folder.id
            return reference
        })
        fileUseCase?.createReferences(
            references: references
        ).then({ results in
            self.folderUseCase!.getCurrentFolders(
                current_guild: self.viewController!.current_guild,
                is_playlist: self.viewController?.is_playlist ?? false
            ).then({
                self.folders = $0
                self.viewController?.sync(didFetchFolders: $0)
            })
            self.viewController?.foldersPresenter(didCreate: results, in: self)
        }).catch({
            self.viewController?.foldersPresenter(onError: $0, in: self)
        })
    }
    
    func afterCreatedReferences(_ results: FileReferenceEntities) {
    }
    
    func deleteFiles(repositories: FileEntities) {
        fileUseCase!.deletes(
            files: repositories
        ).then({
            GuildConnector.shared.fetchRequest().then({})
            self.viewController?.foldersPresenter(didDelete: self)
        }).catch({
            self.viewController?.foldersPresenter(onError: $0, in: self)
        })
        FolderConnector.shared.didDeleteFile(file: repositories.first ?? FileEntity()).then({})
    }
    
    func create(_ repository: FolderEntity) {
        folderUseCase!.create(
            folder: repository
        ).then({
            GuildConnector.shared.fetchRequest().then({})
            FolderConnector.shared.didCreateFolder(folder: $0).then({})
            self.viewController?.foldersPresenter(didCreate: $0, in: self)
        }).catch({
            self.viewController?.foldersPresenter(onError: $0, in: self)
        })
    }
}

extension FoldersPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let current_guild = state.app.current_guild, current_guild == self.viewController?.current_guild, let caches = state.file.guildCacheFiles[current_guild.uid] {
            self.caches = caches
            self.caches?.items = caches.filter({ $0.progress ?? 0 < 1 })
            self.viewController?.foldersPresenter(didCache: caches, in: self)
        }
    }
}
