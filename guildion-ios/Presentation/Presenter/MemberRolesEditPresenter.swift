//
//  MemberRolesEditPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol MemberRolesEditPresenterOutput: class {
    var current_guild: GuildEntity { get set }
    func sync(didFetchRepositories repositories: MemberRoleEntities)
    func sync(didFetchRepositories repositories: RoleEntities)
    func memberRolesEditPresenter(didCreate repository: MemberRoleEntity, in memberRolesEditPresenter: MemberRolesEditPresenter)
    func memberRolesEditPresenter(didUpdate repository: MemberRoleEntity, in memberRolesEditPresenter: MemberRolesEditPresenter)
    func memberRolesEditPresenter(didDelete repository: MemberRoleEntity, in memberRolesEditPresenter: MemberRolesEditPresenter)
    func memberRolesEditPresenter(onError error: Error, in memberRolesEditPresenter: MemberRolesEditPresenter)
}

@objcMembers
class MemberRolesEditPresenter: PresenterImpl {
    typealias WireframeType = MemberRolesEditWireframe
    typealias RepositoriesType = MemberRoleEntities
    typealias ViewControllerType = MemberRolesEditPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var repository: MemberEntity = MemberEntity()
    var roles: RoleEntities = RoleEntities()
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(repository: MemberEntity) {
        self.repository = repository
        memberUseCase!.getRoles(member: repository).then({
            self.viewController?.sync(didFetchRepositories: $0)
        }).catch({
            self.viewController?.memberRolesEditPresenter(onError: $0, in: self)
        })
        
        guildUseCase!.getGuildRoles(uid: viewController!.current_guild.uid).then({
            self.viewController?.sync(didFetchRepositories: $0)
        }).catch({
            self.viewController?.memberRolesEditPresenter(onError: $0, in: self)
        })
    }
    
    func create(repository: RoleEntity, member: MemberEntity) {
        let entity = MemberRoleEntity()
        entity.Member = member
        entity.MemberId = member.id
        entity.Role = repository
        entity.RoleId = repository.id
        memberUseCase!.create(member_role: entity).then({
            GuildConnector.shared.fetchRequest().then({})
            self.viewController?.memberRolesEditPresenter(didCreate: $0, in: self)
        }).catch({
            self.viewController?.memberRolesEditPresenter(onError: $0, in: self)
        })
    }
    
    func update(repository: MemberRoleEntity) {
    }
    
    func delete(repository: MemberRoleEntity) {
    }
    
    func toMemberRoleEdit() {
        self.wireframe?.toMemberRoleEdit()
    }
}

extension MemberRolesEditPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
