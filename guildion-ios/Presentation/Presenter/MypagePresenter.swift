//
//  MypagePresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol MypagePresenterOutput: class {
    func sync(didFetchRepository repository: UserEntity)
    func mypagePresenter(didLogout mypagePresenter: MypagePresenter)
    func mypagePresenter(onError error: Error, in mypagePresenter: MypagePresenter)
}

@objcMembers
class MypagePresenter: PresenterImpl {
    typealias WireframeType = MypageWireframe
    typealias RepositoriesType = UserEntity
    typealias ViewControllerType = MypagePresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var categories: CategoryEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        authUseCase!.syncCurrentUserForce().then({
            guard let unwrapped = $0 else { self.logout(); return }
            self.viewController?.sync(didFetchRepository: unwrapped)
        })
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func logout() {
        authUseCase!.logout(
        ).then({
            self.viewController?.mypagePresenter(didLogout: self)
        }).catch({
            self.viewController?.mypagePresenter(onError: $0, in: self)
        })
    }
    
    func toPlans() {
        self.wireframe?.toPlans()
    }
    
    func toUserFiles() {
        self.wireframe?.toUserFiles()
    }
}

extension MypagePresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let current_user = state.auth.current_user {
            viewController?.sync(didFetchRepository: current_user)
        }
    }
}
