//
//  ChannelRolesEditPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol ChannelRolesEditPresenterOutput: class {
    func sync(didFetchRepositories repositories: [ChannelRoleableProtocol])
    func sync(didFetchRepositories repositories: RoleEntities)
    func channelRolesEditPresenter(didCreated repository: ChannelRoleableProtocol, in channelRolesEditPresenter: ChannelRolesEditPresenter)
    func channelRolesEditPresenter(onError error: Error, in channelRolesEditPresenter: ChannelRolesEditPresenter)
}

@objcMembers
class ChannelRolesEditPresenter: PresenterImpl {
    typealias WireframeType = ChannelRolesEditWireframe
    typealias RepositoriesType = [ChannelRoleableProtocol]
    typealias ViewControllerType = ChannelRolesEditPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var roles: RoleEntities = RoleEntities()
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func toChannelEdit() {
        self.wireframe?.toChannelRoleEdit()
    }
    
    func fetch(repository: ChannelableProtocol) {
        guildUseCase!.getGuildRoles(
            uid: GuildConnector.shared.current_guild?.uid ?? ""
        ).then({
            self.viewController?.sync(didFetchRepositories: $0)
        }).catch({
            self.viewController?.channelRolesEditPresenter(onError: $0, in: self)
        })
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            textChannelUseCase!.getRoles(
                uid: repository.uid
            ).then({
                self.viewController?.sync(didFetchRepositories: $0.items)
            }).catch({
                self.viewController?.channelRolesEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            dmChannelUseCase!.getRoles(
                uid: repository.uid
            ).then({
                self.viewController?.sync(didFetchRepositories: $0.items)
            }).catch({
                self.viewController?.channelRolesEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.getRoles(
                uid: repository.uid
            ).then({
                self.viewController?.sync(didFetchRepositories: $0.items)
            }).catch({
                self.viewController?.channelRolesEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func create(repository: RoleEntity, channel: ChannelableProtocol) {
        switch channel.channelable_type.superChannel {
        case .TextChannel:
            textChannelUseCase!.create(
                channel: (channel as! TextChannelEntity).toNewMemoryChannel(),
                role: repository
            ).then({
                GuildConnector.shared.fetchRequest().then({})
                self.viewController?.channelRolesEditPresenter(didCreated: $0, in: self)
            }).catch({
                self.viewController?.channelRolesEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.create(
                channel: (channel as! StreamChannelEntity).toNewMemoryChannel(),
                role: repository
            ).then({
                GuildConnector.shared.fetchRequest().then({})
                self.viewController?.channelRolesEditPresenter(didCreated: $0, in: self)
            }).catch({
                self.viewController?.channelRolesEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            dmChannelUseCase!.create(
                channel: (channel as! DMChannelEntity).toNewMemoryChannel(),
                role: repository
            ).then({
                GuildConnector.shared.fetchRequest().then({})
                self.viewController?.channelRolesEditPresenter(didCreated: $0, in: self)
            }).catch({
                self.viewController?.channelRolesEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
}

extension ChannelRolesEditPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
