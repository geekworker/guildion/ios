//
//  ChannelSettingPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol ChannelSettingPresenterOutput: class {
    func sync(didFetchRepository repository: ChannelableProtocol)
    func channelSettingPresenter(didUpdate repository: ChannelableProtocol, in channelSettingPresenter: ChannelSettingPresenter)
    func channelSettingPresenter(onError error: Error, in channelSettingPresenter: ChannelSettingPresenter)
}

@objcMembers
class ChannelSettingPresenter: PresenterImpl {
    typealias WireframeType = ChannelSettingWireframe
    typealias RepositoriesType = ChannelableProtocol
    typealias ViewControllerType = ChannelSettingPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(repository: ChannelableProtocol) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let channel = repository as? TextChannelEntity else { return }
            textChannelUseCase!.get(
                uid: channel.uid
            ).then({ repository in
                ChannelConnector.shared.didJoin(current_channel: repository).then({
                    self.viewController?.sync(didFetchRepository: repository)
                })
            })
        case .StreamChannel:
            guard let channel = repository as? StreamChannelEntity else { return }
            streamChannelUseCase!.get(
                uid: channel.uid
            ).then({ repository in
                ChannelConnector.shared.didJoin(current_channel: repository).then({
                    self.viewController?.sync(didFetchRepository: repository)
                })
            })
        case .DMChannel:
            guard let channel = repository as? DMChannelEntity else { return }
            dmChannelUseCase!.get(
                uid: channel.uid
            ).then({ repository in
                ChannelConnector.shared.didJoin(current_channel: repository).then({
                    self.viewController?.sync(didFetchRepository: repository)
                })
            })
        default: break
        }
    }
    
    func update(repository: ChannelableProtocol) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let channel = repository as? TextChannelEntity else { return }
            textChannelUseCase!.update(
                channel: channel.toNewMemoryChannel()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelSettingPresenter(didUpdate: $0, in: self)
            })
        case .StreamChannel:
            guard let channel = repository as? StreamChannelEntity else { return }
            streamChannelUseCase!.update(
                channel: channel.toNewMemoryChannel()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelSettingPresenter(didUpdate: $0, in: self)
            })
        case .DMChannel:
            guard let channel = repository as? DMChannelEntity else { return }
            dmChannelUseCase!.update(
                channel: channel.toNewMemoryChannel()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelSettingPresenter(didUpdate: $0, in: self)
            })
        default: break
        }
    }
    
    func delete(repository: ChannelableProtocol) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let channel = repository as? TextChannelEntity else { return }
            textChannelUseCase!.delete(
                channel: channel.toNewMemoryChannel()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelSettingPresenter(didUpdate: channel, in: self)
            }).catch({
                self.viewController?.channelSettingPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            guard let channel = repository as? StreamChannelEntity else { return }
            streamChannelUseCase!.delete(
                channel: channel.toNewMemoryChannel()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelSettingPresenter(didUpdate: channel, in: self)
            }).catch({
                self.viewController?.channelSettingPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            guard let channel = repository as? DMChannelEntity else { return }
            dmChannelUseCase!.delete(
                channel: channel.toNewMemoryChannel()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelSettingPresenter(didUpdate: channel, in: self)
            }).catch({
                self.viewController?.channelSettingPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
}

extension ChannelSettingPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
