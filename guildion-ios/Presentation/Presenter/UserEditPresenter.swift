//
//  UserEditPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol UserEditPresenterOutput: class {
    func sync(didFetchRepository repository: UserEntity)
    func userEditPresenter(afterUpdated repository: UserEntity, in userEditPresenter: UserEditPresenter)
    func userEditPresenter(onError error: Error, in userEditPresenter: UserEditPresenter)
}

@objcMembers
class UserEditPresenter: PresenterImpl {
    typealias WireframeType = UserEditWireframe
    typealias RepositoriesType = UserEntity
    typealias ViewControllerType = UserEditPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func update(repository: UserEntity) {
        userUseCase!.update(user: repository).then({
            self.viewController?.userEditPresenter(afterUpdated: $0, in: self)
        }).catch({
            self.viewController?.userEditPresenter(onError: $0, in: self)
        })
    }
    
    func updates(repository: UserEntity?) {
        if let repository = repository {
            userUseCase!.update(user: repository).then({ user in
                self.memberUseCase?.updates(
                ).then({ _ in
                    self.viewController?.userEditPresenter(afterUpdated: user, in: self)
                })
            }).catch({
                self.viewController?.userEditPresenter(onError: $0, in: self)
            })
        } else {
            memberUseCase!.updates().then({
                self.viewController?.userEditPresenter(afterUpdated: UserEntity(), in: self)
            }).catch({
                self.viewController?.userEditPresenter(onError: $0, in: self)
            })
        }
    }
}

extension UserEditPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
