//
//  MailConfirmPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol MailConfirmPresenterOutput: class {
    func sync(didFetchRepository repository: SessionModel)
    func confirmed()
    func mailConfirmPresenter(onError error: Error)
    func mailConfirmPresenter(didResend mailConfirmPresenter: MailConfirmPresenter)
}

@objcMembers
class MailConfirmPresenter: PresenterImpl {
    typealias WireframeType = MailConfirmWireframe
    typealias RepositoriesType = SessionModel
    typealias ViewControllerType = MailConfirmPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func confirm(repository: SessionModel) {
        store.dispatch(SessionDispatcher.SET_SESSION(payload: repository))
        sessionUseCase!.confirmEmail(code: repository.code ?? "").then({
            self.repositories?.identity = $0
            self.viewController?.confirmed()
        }).catch({
            self.viewController?.mailConfirmPresenter(onError: $0)
        })
    }
    
    func resend(repository: SessionModel) {
        sessionUseCase!.resendConfirmEmail().then({
            self.viewController?.mailConfirmPresenter(didResend: self)
        }).catch({
            self.viewController?.mailConfirmPresenter(onError: $0)
        })
    }
    
    func toGuildNew() {
        self.wireframe?.toGuildNew()
    }
    
    func toProfileSetting() {
        self.wireframe?.toProfileSetting()
    }
    
    func onError(_ error: Error) {
        self.viewController?.mailConfirmPresenter(onError: error)
    }
}

extension MailConfirmPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let repository = state.session.repository {
            self.repositories = repository
            self.viewController?.sync(didFetchRepository: repository)
        }
    }
}
