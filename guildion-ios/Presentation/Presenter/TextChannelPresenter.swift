//
//  TextChannelPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol TextChannelPresenterOutput: class {
    func textChannelPresenter(didCache repositories: MessageEntities, shouldScroll: Bool, in textChannelPresenter: TextChannelPresenter)
    func textChannelPresenter(shouldUpdatePermission repository: PermissionModel, in textChannelPresenter: TextChannelPresenter)
    func textChannelPresenter(didRetry count: Int, in textChannelPresenter: TextChannelPresenter)
    func textChannelPresenter(shouldReload textChannelPresenter: TextChannelPresenter)
    func sync(didFetchRepository repository: TextChannelEntity)
    func sync(didFetchRepositories repositories: MessageEntities)
    func sync(didFetchRepositories repositories: MemberEntities)
    func textChannelPresenter(didJoin repository: TextChannelEntity)
    func syncCurrentMember(didFetchRepository repository: MemberEntity)
    func textChannelPresenter(didIncrements repositories: MessageEntities)
    func textChannelPresenter(didDecrements repositories: MessageEntities)
    func textChannelPresenter(didCreated repository: MessageEntity)
    func textChannelPresenter(didCreated repository: ReactionEntity)
    func textChannelPresenter(willTyping repository: MemberEntity)
    func textChannelPresenter(didTyping repository: MemberEntity)
    func textChannelPresenter(onError error: Error)
    var repository: TextChannelEntity { get set }
    var repositories: MessageEntities { get set }
    var current_member: MemberEntity { get set }
    var current_guild: GuildEntity { get set }
    var permission: PermissionModel { get set }
}

@objcMembers
class TextChannelPresenter: NSObject, PresenterImpl {
    typealias WireframeType = TextChannelWireframe
    typealias RepositoriesType = TextChannelEntity
    typealias ViewControllerType = TextChannelPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType? = TextChannelEntity()
    var caches: MessageEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        ChannelConnector.shared.inject(self)
        self.sync(self.repositories!)
        self.fetchMessages()
        self.textChannelUseCase!.get(
            uid: self.repositories!.uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.textChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.repositories = $0
            self.viewController?.permission.TextChannel.current_text_channel_roles = $0.Roles ?? TextChannelRoleEntities()
            self.viewController?.textChannelPresenter(shouldUpdatePermission: self.viewController!.permission, in: self)
            self.viewController?.sync(didFetchRepository: $0)
            ChannelConnector.shared.current_channel = $0
        }).catch({
            self.viewController?.textChannelPresenter(onError: $0)
        })
    }
    
    func join() {
        ChannelConnector.shared.didJoin(current_channel: self.repositories!, current_guild: self.viewController?.current_guild, current_member: self.viewController?.current_member).then({
            self.viewController?.textChannelPresenter(didJoin: self.repositories!)
            self.read()
        })
    }
    
    func unbind() {
        store.unsubscribe(self)
        ChannelConnector.shared.endEditInput().then({
            ChannelConnector.shared.didLeave().then({})
        })
        ChannelConnector.shared.reject(self)
    }
    
    func read() {
        guard let channel = ChannelConnector.shared.current_channel as? TextChannelEntity else { return }
        textChannelUseCase!.reads(
            channel: channel.toNewMemoryChannel()
        ).then({ _ in
        }).catch({
            self.viewController?.textChannelPresenter(onError: $0)
        })
    }
    
    func reload() {
        guard let uid = self.repositories?.uid else { return }
        self.textChannelUseCase!.get(
            uid: uid
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.textChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.repositories = $0
            self.viewController?.permission.TextChannel.current_text_channel_roles = $0.Roles ?? TextChannelRoleEntities()
            self.viewController?.textChannelPresenter(shouldUpdatePermission: self.viewController!.permission, in: self)
            self.viewController?.sync(didFetchRepository: $0)
        }).catch({
            self.viewController?.textChannelPresenter(onError: $0)
        })
    }
    
    func toChannelMenu() {
        self.wireframe?.toChannelMenu()
    }
    
    func toMemberMenu(repository: MemberEntity) {
        self.wireframe?.toMemberMenu(repository: repository)
    }
    
    func toMessageMenu(repository: MessageEntity) {
        self.wireframe?.toMessageMenu(repository: repository)
    }
    
    func fetchMembers() {
        self.guildUseCase?.getGuildMembers(
            guild: self.viewController!.current_guild.toNewMemory()
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.textChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func fetchMessages() {
        self.messageUseCase!.getCurrentMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.textChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            self.nilCheckRepositories()
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func increments() {
        guard self.viewController?.repositories.count ?? 0 > 0 else { self.fetchMessages(); return }
        messageUseCase!.getIncrementMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.textChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            guard self.repositories != nil else { return }
            self.read()
            self.viewController?.textChannelPresenter(didIncrements: $0)
        })
    }
    
    func decrements() {
        guard self.viewController?.repositories.count ?? 0 > 0 else { self.fetchMessages(); return }
        messageUseCase!.getDecrementMessages(
            current_channel: self.viewController!.repository
        ).retry(.max) { count, error in
            guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
            self.viewController?.textChannelPresenter(didRetry: count, in: self)
            return true
        }.then({
            guard self.repositories != nil else { return }
            self.viewController?.textChannelPresenter(didDecrements: $0)
        })
    }
    
    func willTyping() {
        ChannelConnector.shared.didEditInput().then({})
    }
    
    func didTyping() {
        ChannelConnector.shared.endEditInput().then({})
    }
    
    func sync(_ repository: RepositoriesType) {
        self.repositories = repository
        store.dispatch(AppDispatcher.SET_CURRENT_CHANNEL(payload: repository))
    }
    
    func create(_ repository: ReactionEntity) {
        messageUseCase!.create(
            reaction: repository
        ).then({
            ChannelConnector.shared.didReaction(reaction: $0).then({})
        })
    }
    
    func create(_ repository: MessageEntity) {
        messageUseCase!.create(
            message: repository
        ).then({
            self.viewController?.textChannelPresenter(didCreated: $0)
            repository.Sender = self.viewController?.current_member
            ChannelConnector.shared.didCreateMessage(message: $0).then({})
            self.increments()
        })
    }
    
    func update(_ repository: MessageEntity) {
        messageUseCase!.update(message: repository).then({
            ChannelConnector.shared.didUpdateMessage(message: $0).then({})
            self.viewController?.textChannelPresenter(didCreated: $0)
        })
    }
    
    func nilCheckRepositories() {
        if repositories == nil {
            repositories = TextChannelEntity()
        }
    }
    
    func toWebBrowser(_ url: URL, repositories: FileEntities? = nil) {
        self.wireframe?.toWebBrowser(url, repositories: repositories)
    }
    
    func toImageViewer(repository: FileEntity) {
        self.wireframe?.toImageViewer(repository: repository)
    }
}

extension TextChannelPresenter: ChannelConnectorDelegate {
    func channelConnector(event: WebsocketChannelEvent) {
        switch event {
        case .fetchRequest: self.reload()
        case .didCreateMessage(let member, let message):
            guard let current_channel = ChannelConnector.shared.current_channel, let uid = current_channel.messageable_uid else { return }
            let entities = MessageEntities()
            entities.append(message)
            message.Sender = member
            self.read()
            store.dispatch(MessageDispatcher.ADD_CHANNEL_MESSAGES(channel_uid: uid, payload: entities))
            self.viewController?.textChannelPresenter(shouldReload: self)
        case .didUpdateMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_CACHES(payload: entities))
                self.viewController?.textChannelPresenter(shouldReload: self)
            }
        case .didDeleteMessage(let member, let message):
            DispatchQueue.main.async {
                let entities = MessageEntities()
                entities.items = [message]
                message.Sender = member
                store.dispatch(MessageDispatcher.SET_DELETES(payload: entities))
                self.viewController?.textChannelPresenter(shouldReload: self)
            }
        case .didEditInput(let member):
            self.viewController?.textChannelPresenter(willTyping: member)
        case .endEditInput(let member):
            self.viewController?.textChannelPresenter(didTyping: member)
        case .didReaction(_, let reaction):
            self.viewController?.textChannelPresenter(didCreated: reaction)
        default: break
        }
    }
    
    func channelConnector(onError: Error) {
    }
}

extension TextChannelPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let current_channel = self.repositories, let caches = state.message.channelCacheMessages[current_channel.uid] {
            self.caches = caches
            self.viewController?.textChannelPresenter(didCache: caches, shouldScroll: false, in: self)
        }
    }
}
