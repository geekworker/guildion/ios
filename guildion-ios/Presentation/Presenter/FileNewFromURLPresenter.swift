//
//  FileNewFromURLPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol FileNewFromURLPresenterOutput: class {
    func sync(didFetchRepositories repositories: FileEntities)
    func syncCurrentMember(didFetchRepository repository: MemberEntity)
    func fileNewFromURLPresenter(shouldCreate repositories: FileEntities)
    func fileNewFromURLPresenter(didCreated repositories: FileEntities)
    func fileNewFromURLPresenter(onError error: Error)
    var current_member: MemberEntity { get set }
    var current_guild: GuildEntity { get set }
}

@objcMembers
class FileNewFromURLPresenter: PresenterImpl {
    typealias WireframeType = FileNewFromURLWireframe
    typealias RepositoriesType = FileEntities
    typealias ViewControllerType = FileNewFromURLPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var folders: FolderEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func creates(_ repositories: FileEntities) {
        guard repositories.count > 0 else { return }
        fileUseCase!.creates(
            files: repositories,
            in: self.viewController!.current_guild
        ).then({
            self.repositories = $0
            if let guild = self.viewController?.current_guild {
                store.dispatch(FileDispatcher.RESET_GUILD_CHACHE_FILES(payload: guild.uid))
            }
            self.viewController?.fileNewFromURLPresenter(didCreated: $0)
        }).catch({
            self.viewController?.fileNewFromURLPresenter(onError: $0)
        })
        self.viewController?.fileNewFromURLPresenter(shouldCreate: repositories)
    }
    
    func afterCreated(_ repositories: FileEntities) {
        
    }
}

extension FileNewFromURLPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
