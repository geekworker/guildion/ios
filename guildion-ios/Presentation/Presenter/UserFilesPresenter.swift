//
//  UserFilesPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol UserFilesPresenterOutput: class {
    func sync(didFetchRepositories repositories: FileEntities)
    func userFilesPresenter(shouldReload repository: UserEntity)
    func userFilesPresenter(didDelete repository: FileEntity, in userFilesPresenter: UserFilesPresenter)
    func userFilesPresenter(onError error: Error, in userFilesPresenter: UserFilesPresenter)
}

@objcMembers
class UserFilesPresenter: PresenterImpl {
    typealias WireframeType = UserFilesWireframe
    typealias RepositoriesType = FileEntities
    typealias ViewControllerType = UserFilesPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var repository: UserEntity = CurrentUser.getCurrentUserEntity() ?? UserEntity()
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch() {
        guard let current_user = CurrentUser.getCurrentUserEntity() else { return }
        self.repository = current_user
        fileUseCase!.getCurrentUserFiles().then({
            self.repositories = $0
            self.viewController?.sync(didFetchRepositories: $0)
        })
    }
    
    func delete(repository: FileEntity) {
        fileUseCase!.delete(file: repository).then({
            self.viewController?.userFilesPresenter(didDelete: repository, in: self)
            self.updateCurrentUser()
        }).catch({
            self.viewController?.userFilesPresenter(onError: $0, in: self)
        })
    }
    
    func updateCurrentUser() {
        authUseCase!.syncCurrentUserForce().then({ _ in })
    }
    
    func toPlans() {
        self.wireframe?.toPlans()
    }
    
    func toFileMenu(repository: FileEntity) {
        self.wireframe?.toFileMenu(repository: repository)
    }
}

extension UserFilesPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
        if let current_user = state.auth.current_user, self.repository.size_byte != current_user.size_byte {
            self.repository = current_user
            self.viewController?.userFilesPresenter(shouldReload: current_user)
        }
    }
}
