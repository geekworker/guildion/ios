//
//  ChannelMembersEditPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/02.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol ChannelMembersEditPresenterOutput: class {
    func sync(didFetchRepositories repositories: MemberEntities)
    func channelMembersEditPresenter(didInvite repositories: MemberEntities, in channelMembersEditPresenter: ChannelMembersEditPresenter)
    func channelMembersEditPresenter(didKick repositories: MemberEntities, in channelMembersEditPresenter: ChannelMembersEditPresenter)
    func channelMembersEditPresenter(onError error: Error, in channelMembersEditPresenter: ChannelMembersEditPresenter)
}

@objcMembers
class ChannelMembersEditPresenter: PresenterImpl {
    typealias WireframeType = ChannelMembersEditWireframe
    typealias RepositoriesType = MemberEntities
    typealias ViewControllerType = ChannelMembersEditPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(repository: ChannelableProtocol) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let channel = repository as? TextChannelEntity else { return }
            textChannelUseCase?.getCurrentMembers(
                channel: channel
            ).then({
                self.viewController?.sync(didFetchRepositories: $0)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            guard let channel = repository as? StreamChannelEntity else { return }
            streamChannelUseCase?.getCurrentMembers(
                channel: channel
            ).then({
                self.viewController?.sync(didFetchRepositories: $0)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            guard let channel = repository as? DMChannelEntity else { return }
            dmChannelUseCase?.getCurrentMembers(
                channel: channel
            ).then({
                self.viewController?.sync(didFetchRepositories: $0)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func invite(repository: ChannelableProtocol, repositories: MemberEntities) {
        switch repository.channelable_type.superChannel {
        case .DMChannel:
            dmChannelUseCase?.invite(
                channel: (repository as! DMChannelEntity).toNewMemoryChannel(),
                members: repositories.toNewMemory()
            ).then({ _ in
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMembersEditPresenter(didInvite: repositories, in: self)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        case .TextChannel:
            textChannelUseCase?.invite(
                channel: (repository as! TextChannelEntity).toNewMemoryChannel(),
                members: repositories.toNewMemory()
            ).then({ _ in
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMembersEditPresenter(didInvite: repositories, in: self)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase?.invite(
                channel: (repository as! StreamChannelEntity).toNewMemoryChannel(),
                members: repositories.toNewMemory()
            ).then({ _ in
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMembersEditPresenter(didInvite: repositories, in: self)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func kick(repository: ChannelableProtocol, targets: MemberEntities) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let channel = repository as? TextChannelEntity else { return }
            textChannelUseCase?.kick(
                channel: channel,
                members: targets.toNewMemory()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMembersEditPresenter(didKick: targets, in: self)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            guard let channel = repository as? StreamChannelEntity else { return }
            streamChannelUseCase?.kick(
                channel: channel,
                members: targets.toNewMemory()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMembersEditPresenter(didKick: targets, in: self)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            guard let channel = repository as? DMChannelEntity else { return }
            dmChannelUseCase?.kick(
                channel: channel,
                members: targets.toNewMemory()
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMembersEditPresenter(didKick: targets, in: self)
            }).catch({
                self.viewController?.channelMembersEditPresenter(onError: $0, in: self)
            })
        default: break
        }
        
    }
    
    func toMemberEdit() {
        self.wireframe?.toMemberEdit()
    }
    
    func toMemberSelect() {
        self.wireframe?.toMemberSelect()
    }
}

extension ChannelMembersEditPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
