//
//  PresenterImpl.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa

protocol PresenterImpl: class {
    associatedtype WireframeType
    associatedtype ViewControllerType
    associatedtype RepositoriesType
    
    var wireframe: WireframeType? { get set }
    var repositories: RepositoriesType? { get set }
    var viewController: ViewControllerType? { get set }
    
    var authUseCase: AuthUseCase? { get set }
    var categoryUseCase: CategoryUseCase? { get set }
    var folderUseCase: FolderUseCase? { get set }
    var dmChannelUseCase: DMChannelUseCase? { get set }
    var fileUseCase: FileUseCase? { get set }
    var guildUseCase: GuildUseCase? { get set }
    var searchUseCase: SearchUseCase? { get set }
    var sessionUseCase: SessionUseCase? { get set }
    var streamChannelUseCase: StreamChannelUseCase? { get set }
    var streamUseCase: StreamUseCase? { get set }
    var textChannelUseCase: TextChannelUseCase? { get set }
    var tagUseCase: TagUseCase? { get set }
    var notificationUseCase: NotificationUseCase? { get set }
    var messageUseCase: MessageUseCase? { get set }
    var memberUseCase: MemberUseCase? { get set }
    var userUseCase: UserUseCase? { get set }
    var roleUseCase: RoleUseCase? { get set }

}
