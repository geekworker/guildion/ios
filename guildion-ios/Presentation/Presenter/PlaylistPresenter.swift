//
//  PlaylistPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/25.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol PlaylistPresenterOutput: class {
    var all_files: Bool { get set }
    var selectable: Bool { get set }
    var current_guild: GuildEntity { get set }
    var current_member: MemberEntity { get set }
    func sync(didFetchRepository repository: FolderEntity)
    func playlistPresenter(didFetch repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter)
    func playlistPresenter(didIncrement repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter)
    func playlistPresenter(didDecrement repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter)
    func playlistPresenter(didCreate repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter)
    func playlistPresenter(willToStreamChannel repository: StreamChannelEntity, in playlistPresenter: PlaylistPresenter)
    func playlistPresenter(didDelete playlistPresenter: PlaylistPresenter)
    func playlistPresenter(onError error: Error, in playlistPresenter: PlaylistPresenter)
    func playlistPresenter(didRetry count: Int, in playlistPresenter: PlaylistPresenter)
}

@objcMembers
class PlaylistPresenter: PresenterImpl {
    typealias WireframeType = PlaylistWireframe
    typealias RepositoriesType = FolderEntity
    typealias ViewControllerType = PlaylistPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
        if !(self.viewController?.selectable ?? false) && !(self.viewController?.all_files ?? false) {
            FolderConnector.shared.didLeave().then({})
        }
    }
    
    func fetch(folder: FolderEntity) {
        if let all_files = viewController?.all_files, all_files {
            folderUseCase!.getCurrentGuildFiles(
                current_guild: self.viewController!.current_guild,
                is_playlist: true
            ).retry(.max) { count, error in
                guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
                self.viewController?.playlistPresenter(didRetry: count, in: self)
                return true
            }.then({
                self.repositories = FolderEntity()
                self.repositories?.name = R.string.localizable.allFiles()
                self.repositories?.temporary = true
                let entities = FileReferenceEntities()
                entities.items = $0.enumerated().compactMap({
                    let entity = FileReferenceEntity()
                    entity.id = $0.offset
                    entity.File = $0.element
                    entity.FileId = $0.element.id
                    return entity
                }).reversed()
                self.viewController?.sync(didFetchRepository: self.repositories!)
                self.viewController?.playlistPresenter(didFetch: entities, in: self)
            })
        } else {
            folderUseCase!.getFolder(
                id: folder.id ?? 0
            ).retry(.max) { count, error in
                guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
                self.viewController?.playlistPresenter(didRetry: count, in: self)
                return true
            }.then({
                self.repositories = $0
                if !(self.viewController?.selectable ?? false) && !(self.viewController?.all_files ?? false) {
                    FolderConnector.shared.didJoin(current_folder: $0).then({})
                }
                store.dispatch(FolderDispatcher.SET_SHOW_GALLERY(folder_uid: $0.uid, payload: $0))
                self.viewController?.sync(didFetchRepository: $0)
            })
            folderUseCase!.getCurrentFolderReferences(folder: folder).then({
                self.viewController?.playlistPresenter(didFetch: $0, in: self)
            })
        }
    }
    
    func increment() {
        if self.viewController?.all_files ?? false {
            folderUseCase!.getDecrementGuildFiles(
                current_guild: self.viewController!.current_guild, is_playlist: true
            ).retry(.max) { count, error in
                guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
                self.viewController?.playlistPresenter(didRetry: count, in: self)
                return true
            }.then({
                let entities = FileReferenceEntities()
                entities.items = $0.enumerated().compactMap({
                    let entity = FileReferenceEntity()
                    entity.id = $0.offset
                    entity.File = $0.element
                    entity.FileId = $0.element.id
                    return entity
                }).reversed()
                self.viewController?.playlistPresenter(didIncrement: entities, in: self)
            })
        } else if let repositories = self.repositories {
            folderUseCase!.getIncrementFolderReferences(
                folder: repositories
            ).retry(.max) { count, error in
                guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
                self.viewController?.playlistPresenter(didRetry: count, in: self)
                return true
            }.then({
                self.viewController?.playlistPresenter(didIncrement: $0, in: self)
            })
        }
    }
    
    func decrement() {
        if self.viewController?.all_files ?? false {
            folderUseCase!.getIncrementGuildFiles(
                current_guild: self.viewController!.current_guild, is_playlist: true
            ).retry(.max) { count, error in
                guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
                self.viewController?.playlistPresenter(didRetry: count, in: self)
                return true
            }.then({
                let entities = FileReferenceEntities()
                entities.items = $0.enumerated().compactMap({
                    let entity = FileReferenceEntity()
                    entity.id = $0.offset
                    entity.File = $0.element
                    entity.FileId = $0.element.id
                    return entity
                }).reversed()
                self.viewController?.playlistPresenter(didDecrement: entities, in: self)
            })
        } else if let repositories = self.repositories {
            folderUseCase!.getDecrementFolderReferences(
                folder: repositories
            ).retry(.max) { count, error in
                guard CurrentUser.getCurrentUserEntity() != nil && error.by_internet_reason && !AppConfig.is_background else { return false }
                self.viewController?.playlistPresenter(didRetry: count, in: self)
                return true
            }.then({
                self.viewController?.playlistPresenter(didDecrement: $0, in: self)
            })
        }
    }
    
    func startPrivateStreamChannel(repository: StreamableEntity) {
        guard let guild = viewController?.current_guild, let current_member = viewController?.current_member else { return }
        let stream = StreamEntity()
        stream.Streamable = repository
        stream.StreamableId = repository.streamable_id
        stream.streamable_type = repository.streamable_type ?? .File
        streamChannelUseCase!.start(
            guild: guild.toNewMemory(),
            target: current_member.toNewMemory(),
            stream: stream
        ).then({
            self.viewController?.playlistPresenter(willToStreamChannel: $0, in: self)
        }).catch({
            self.viewController?.playlistPresenter(onError: $0, in: self)
        })
    }
    
    func startStreamChannel(repository: StreamableEntity) {
        guard let guild = viewController?.current_guild else { return }
        let stream = StreamEntity()
        stream.Streamable = repository
        stream.StreamableId = repository.streamable_id
        stream.streamable_type = repository.streamable_type ?? .File
        streamChannelUseCase!.start(
            guild: guild.toNewMemory(), target: nil, stream: stream
        ).then({
            self.viewController?.playlistPresenter(willToStreamChannel: $0, in: self)
        }).catch({
            self.viewController?.playlistPresenter(onError: $0, in: self)
        })
    }
    
    func toFileSelect() {
        self.wireframe?.toFileSelect()
    }
    
    func toFileMenu(_ repository: FileEntity, reference: FileReferenceEntity? = nil) {
        self.wireframe?.toFileMenu(repository: repository, reference: reference)
    }
    
    func toStreamChannel(repository: StreamChannelEntity) {
        self.wireframe?.checkLiveAndToStreamChannel(repository: repository, current_guild: self.viewController!.current_guild, current_member: self.viewController!.current_member)
    }
    
    func toEdit() {
        self.wireframe?.toEdit()
    }
    
    func createFileReferences(repositories: FileEntities, to folder: FolderEntity) {
        let references = FileReferenceEntities()
        references.items = repositories.compactMap({
            let reference = FileReferenceEntity()
            reference.File = $0
            reference.FileId = $0.id
            reference.Folder = folder
            reference.FolderId = folder.id
            return reference
        })
        fileUseCase!.createReferences(
            references: references
        ).then({
            self.viewController?.playlistPresenter(didCreate: $0, in: self)
        }).catch({
            self.viewController?.playlistPresenter(onError: $0, in: self)
        })
    }
    
    func deleteFileReferences(repositories: FileReferenceEntities) {
        fileUseCase!.deleteReferences(
            references: repositories
        ).then({
            self.viewController?.playlistPresenter(didDelete: self)
        }).catch({
            self.viewController?.playlistPresenter(onError: $0, in: self)
        })
    }
    
    func afterDeletedReferences() {
        self.viewController?.playlistPresenter(didDelete: self)
    }
    
    func updateIndexes(repositories: FileReferenceEntities) {
        folderUseCase!.updateReferenceIndexes(references: repositories).then({})
    }
}

extension PlaylistPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}

