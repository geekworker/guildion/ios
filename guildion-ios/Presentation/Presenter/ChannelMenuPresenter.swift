//
//  ChannelMenuPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/28.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol ChannelMenuPresenterOutput: class {
    func sync(didFetchRepository repository: ChannelableProtocol)
    func sync(didFetchRepository repositories: MemberEntities)
    func sync(didDecrementRepository repositories: MemberEntities)
    func sync(didFetchRepository repositories: ParticipantEntities)
    func channelMenuPresenter(didLeave repository: ChannelableProtocol, in channelMenuPresenter: ChannelMenuPresenter)
    func channelMenuPresenter(didInvite repository: ChannelableProtocol, members: MemberEntities, in channelMenuPresenter: ChannelMenuPresenter)
    func channelMenuPresenter(onError error: Error, in channelMenuPresenter: ChannelMenuPresenter)
}

@objcMembers
class ChannelMenuPresenter: PresenterImpl {
    typealias WireframeType = ChannelMenuWireframe
    typealias RepositoriesType = ChannelableProtocol
    typealias ViewControllerType = ChannelMenuPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var folders: FolderEntities?
    var members: MemberEntities = MemberEntities()
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
        guard let current_channel = ChannelConnector.shared.current_channel else { return }
        self.fetchMembers(current_channel)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetchMembers(_ repository: ChannelableProtocol) {
        switch repository.channelable_type.superChannel {
        case .DMChannel:
            dmChannelUseCase!.getCurrentMembers(
                channel: repository as! DMChannelEntity
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.viewController?.sync(didFetchRepository: $0)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .TextChannel:
            textChannelUseCase!.getCurrentMembers(
                channel: repository as! TextChannelEntity
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.viewController?.sync(didFetchRepository: $0)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.getCurrentMembers(
                channel: repository as! StreamChannelEntity
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.viewController?.sync(didFetchRepository: $0)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func fetchMembers(_ repository: MessageableEntity) {
        switch repository.messageable_type {
        case .DMChannel:
            dmChannelUseCase!.getCurrentMembers(
                channel: repository as! DMChannelEntity
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.members = $0
                self.viewController?.sync(didFetchRepository: $0)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .TextChannel:
            textChannelUseCase!.getCurrentMembers(
                channel: repository as! TextChannelEntity
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.members = $0
                self.viewController?.sync(didFetchRepository: $0)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.getCurrentMembers(
                channel: repository as! StreamChannelEntity
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.members = $0
                self.viewController?.sync(didFetchRepository: $0)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func decrementMembers(_ repository: ChannelableProtocol, first_id: Int) {
        switch repository.channelable_type.superChannel {
        case .DMChannel:
            dmChannelUseCase!.getDecrementMembers(
                channel: repository as! DMChannelEntity,
                first_id: first_id
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.members.items += $0.items
                self.viewController?.sync(didDecrementRepository: self.members)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .TextChannel:
            textChannelUseCase!.getDecrementMembers(
                channel: repository as! TextChannelEntity,
                first_id: first_id
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.members.items += $0.items
                self.viewController?.sync(didDecrementRepository: self.members)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.getDecrementMembers(
                channel: repository as! StreamChannelEntity,
                first_id: first_id
            ).retry(.max) { count, error in
                return CurrentUser.getCurrentUser() != nil && error.by_internet_reason && !AppConfig.is_background
            }.then({
                self.members.items += $0.items
                self.viewController?.sync(didDecrementRepository: self.members)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func invite(repository: ChannelableProtocol, repositories: MemberEntities) {
        switch repository.channelable_type.superChannel {
        case .DMChannel:
            dmChannelUseCase!.invite(
                channel: (repository as! DMChannelEntity).toNewMemoryChannel(),
                members: repositories.toNewMemory()
            ).then({ _ in
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMenuPresenter(didInvite: repository, members: repositories, in: self)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .TextChannel:
            textChannelUseCase!.invite(
                channel: (repository as! TextChannelEntity).toNewMemoryChannel(),
                members: repositories.toNewMemory()
            ).then({ _ in
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMenuPresenter(didInvite: repository, members: repositories, in: self)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase!.invite(
                channel: (repository as! StreamChannelEntity).toNewMemoryChannel(),
                members: repositories.toNewMemory()
            ).then({ _ in
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMenuPresenter(didInvite: repository, members: repositories, in: self)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func leave(repository: ChannelableProtocol) {
        switch repository.channelable_type.superChannel {
        case .DMChannel:
            dmChannelUseCase?.leave(
                channel: repository as! DMChannelEntity
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMenuPresenter(didLeave: repository, in: self)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .TextChannel:
            textChannelUseCase?.leave(
                channel: repository as! TextChannelEntity
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMenuPresenter(didLeave: repository, in: self)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            streamChannelUseCase?.leave(
                channel: repository as! StreamChannelEntity
            ).then({
                ChannelConnector.shared.fetchRequest().then({})
                self.viewController?.channelMenuPresenter(didLeave: repository, in: self)
            }).catch({
                self.viewController?.channelMenuPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func toChannelSetting() {
        self.wireframe?.toChannelSetting()
    }
    
    func toChannelNotificationEdit() {
        self.wireframe?.toChannelNotificationEdit()
    }
    
    func toMemberSelect() {
        self.wireframe?.toMemberSelect()
    }
    
    func toMemberMenu(repository: MemberEntity) {
        self.wireframe?.toMemberMenu(repository: repository)
    }
    
    func toMembersEdit(repository: ChannelableProtocol) {
        self.wireframe?.toMembersEdit(repository: repository)
    }
}

extension ChannelMenuPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}


