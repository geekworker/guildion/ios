//
//  FileNewPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol FileNewPresenterOutput: class {
    var didCreateCallback: ((_ repositories: FileEntities) -> ())? { get set }
    func sync(didFetchRepositories repositories: FileEntities)
    func syncCurrentMember(didFetchRepository repository: MemberEntity)
    func fileNewPresenter(shouldCreate repositories: FileEntities)
    func fileNewPresenter(didCreated repositories: FileEntities)
    func fileNewPresenter(onError error: Error)
    var current_guild: GuildEntity { get set }
    var current_member: MemberEntity { get set }
}

@objcMembers
class FileNewPresenter: PresenterImpl {
    typealias WireframeType = FileNewWireframe
    typealias RepositoriesType = FileEntities
    typealias ViewControllerType = FileNewPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    var folders: FolderEntities?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func creates(_ repositories: FileEntities, viewController: FileNewViewController) {
        guard repositories.count > 0 else { return }
        self.viewController?.fileNewPresenter(shouldCreate: repositories)
        fileUseCase!.creates(
            files: repositories,
            in: self.viewController!.current_guild
        ).then({
            self.repositories = $0
            self.viewController?.didCreateCallback?(repositories)
            FolderConnector.shared.didCreateFile(file: $0.first ?? FileEntity()).then({})
            if let guild = self.viewController?.current_guild {
                store.dispatch(FileDispatcher.RESET_GUILD_CHACHE_FILES(payload: guild.uid))
            }
            self.viewController?.fileNewPresenter(didCreated: $0)
        }).catch({
            self.viewController?.fileNewPresenter(onError: $0)
        })
    }
    
    func toFileNewFromURL() {
        self.wireframe?.toFileNewFromURL()
    }
    
    func toFileNewFromBrowser() {
        self.wireframe?.toFileNewFromBrowser()
    }
    
    func toWebKit() {
        self.wireframe?.toWebKit()
    }
}

extension FileNewPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}

