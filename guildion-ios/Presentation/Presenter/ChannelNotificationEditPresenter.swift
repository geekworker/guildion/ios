//
//  ChannelNotificationEditPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol ChannelNotificationEditPresenterOutput: class {
    func sync(didFetchRepository repository: ChannelEntryableProtocol)
    func channelNotificationEditPresenter(didUpdate repository: ChannelEntryableProtocol, in channelNotificationEditPresenter: ChannelNotificationEditPresenter)
    func channelNotificationEditPresenter(onError error: Error, in channelNotificationEditPresenter: ChannelNotificationEditPresenter)
}

@objcMembers
class ChannelNotificationEditPresenter: PresenterImpl {
    typealias WireframeType = ChannelNotificationEditWireframe
    typealias RepositoriesType = ChannelEntryableProtocol
    typealias ViewControllerType = ChannelNotificationEditPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func fetch(repository: ChannelableProtocol) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let channel = repository as? TextChannelEntity else { return }
            textChannelUseCase!.get(
                uid: channel.uid
            ).then({
                guard let entry = $0.Members?.filter({ $0.UserId == CurrentUser.getCurrentUserEntity()?.id }).first?.TextChannelEntry else { return }
                self.viewController?.sync(didFetchRepository: entry)
            }).catch({
                self.viewController?.channelNotificationEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            guard let channel = repository as? StreamChannelEntity else { return }
            streamChannelUseCase!.get(
                uid: channel.uid
            ).then({
                guard let entry = $0.Members?.filter({ $0.UserId == CurrentUser.getCurrentUserEntity()?.id }).first?.StreamChannelEntry else { return }
                self.viewController?.sync(didFetchRepository: entry)
            }).catch({
                self.viewController?.channelNotificationEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            guard let channel = repository as? DMChannelEntity else { return }
            dmChannelUseCase!.get(
                uid: channel.uid
            ).then({
                guard let entry = $0.Members?.filter({ $0.UserId == CurrentUser.getCurrentUserEntity()?.id }).first?.DMChannelEntry else { return }
                self.viewController?.sync(didFetchRepository: entry)
            }).catch({
                self.viewController?.channelNotificationEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
    
    func update(_ repository: ChannelEntryableProtocol) {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let entry = repository as? TextChannelEntryEntity else { break }
            textChannelUseCase!.update(
                entry: entry
            ).then({
                self.viewController?.channelNotificationEditPresenter(didUpdate: $0, in: self)
            }).catch({
                self.viewController?.channelNotificationEditPresenter(onError: $0, in: self)
            })
        case .StreamChannel:
            guard let entry = repository as? StreamChannelEntryEntity else { break }
            streamChannelUseCase!.update(
                entry: entry
            ).then({
                self.viewController?.channelNotificationEditPresenter(didUpdate: $0, in: self)
            }).catch({
                self.viewController?.channelNotificationEditPresenter(onError: $0, in: self)
            })
        case .DMChannel:
            guard let entry = repository as? DMChannelEntryEntity else { break }
            dmChannelUseCase!.update(
                entry: entry
            ).then({
                self.viewController?.channelNotificationEditPresenter(didUpdate: $0, in: self)
            }).catch({
                self.viewController?.channelNotificationEditPresenter(onError: $0, in: self)
            })
        default: break
        }
    }
}

extension ChannelNotificationEditPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
