//
//  GuildInfoPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Hydra
import ReSwift

protocol GuildInfoPresenterOutput: class {
    func sync(didFetchRepository repository: GuildEntity)
    func guildInfoPresenter(afterJoined repository: MemberEntity, in guildInfoPresenter: GuildInfoPresenter)
    func guildInfoPresenter(afterCreated repository: MemberRequestEntity, in guildInfoPresenter: GuildInfoPresenter)
    func guildInfoPresenter(onError error: Error, in guildInfoPresenter: GuildInfoPresenter)
}

@objcMembers
class GuildInfoPresenter: PresenterImpl {
    typealias WireframeType = GuildInfoWireframe
    typealias RepositoriesType = GuildEntity
    typealias ViewControllerType = GuildInfoPresenterOutput
    weak var viewController: ViewControllerType?
    var wireframe: WireframeType?
    var repositories: RepositoriesType?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        wireframe: WireframeType,
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase,
        roleUseCase: RoleUseCase
    ) {
        self.wireframe = wireframe
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
        self.roleUseCase = roleUseCase
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func join(_ repository: GuildEntity) {
        if !repository.is_public {
            self.sendMemberRequest(repository)
            return
        }
        guildUseCase!.join(
            guild: repository
        ).then({
            self.viewController?.guildInfoPresenter(afterJoined: $0, in: self)
        }).catch({
            self.viewController?.guildInfoPresenter(onError: $0, in: self)
        })
    }
    
    func sendMemberRequest(_ repository: GuildEntity) {
        guildUseCase!.createMemberRequest(
            guild: repository
        ).then({
            self.viewController?.guildInfoPresenter(afterCreated: $0, in: self)
        }).catch({
            self.viewController?.guildInfoPresenter(onError: $0, in: self)
        })
    }
    
    func fetch(uid: String) {
        guildUseCase!.getGuildWelcome(
            uid: uid
        ).then({
            self.viewController?.sync(didFetchRepository: $0)
        }).catch({ _ in
            self.viewController?.sync(didFetchRepository: GuildEntity())
            // self.viewController?.guildInfoPresenter(onError: $0, in: self)
        })
    }
    
    func onError(_ error: Error) {
    }
}

extension GuildInfoPresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    
    func newState(state: StoreSubscriberStateType) {
    }
}
