//
//  MenuCollectionView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class MenuCollectionView: UICollectionView {
    var repositories: [MenuCollectionViewCell.MenuType] = []
    
    func calcHeight() -> CGFloat {
        let row_count = Int(self.frame.width / (MenuCollectionViewCell.cellSize.width + 4))
        if row_count == 0 {
            return MenuCollectionViewCell.cellSize.height + 16
        }
        return CGFloat(repositories.count % row_count == 0 ? repositories.count / row_count : repositories.count / row_count + 1) * MenuCollectionViewCell.cellSize.height + 16
    }
    
    func configure() {
        let flowLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.minimumLineSpacing = 4
        flowLayout.minimumInteritemSpacing = 4
        flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 12, right: 0)
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = MenuCollectionViewCell.cellSize
        self.register([
            UINib(resource: R.nib.menuCollectionViewCell): R.nib.menuCollectionViewCell.name,
        ])
        self.isScrollEnabled = false
        self.isPagingEnabled = false
        self.backgroundColor = Theme.themify(key: .transparent)
        self.backgroundView?.backgroundColor = Theme.themify(key: .transparent)
    }
    
    func setRepositories(_ repositories: [MenuCollectionViewCell.MenuType]) {
        self.repositories = repositories
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func collectionView(numberOfItemsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func collectionView(cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: R.nib.menuCollectionViewCell.name, for: indexPath) as? MenuCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
            return UICollectionViewCell()
        }
        cell.setRepository(repository)
        return cell
    }
    
    func collectionView(didSelectItemAt indexPath: IndexPath) -> MenuCollectionViewCell.MenuType {
        self.deselectItem(at: indexPath, animated: true)
        guard let repository = repositories[safe: indexPath.row] else {
            return .none
        }
        return repository
    }
}
