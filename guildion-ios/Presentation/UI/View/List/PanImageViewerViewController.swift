//
//  PanImageViewerViewController.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/07.
//

import Foundation
import UIKit

class PanImageViewerViewController: ImageViewerViewController, PanContainerNavigationViewChildDelegate {
    weak var parentContainerView: PanContainerNavigationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = .black
        configureImageViewer()
        setupNavigationBar()
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.dismissContainer(animated: true, completion: {
            completion?()
        })
    }
    
    fileprivate func configureImageViewer() {
        self.closeButton?.isHidden = true
        self.deleteButton?.isHidden = true
        self.seeAllCloseButton?.isHidden = true
        self.thumbnailsButton?.isHidden = true
        let stringTemplate = "%d / %d"
        self.title = String(format: stringTemplate, arguments: [currentIndex + 1, count])
        self.landedPageAtIndexCompletion = { index in
            self.title = String(format: stringTemplate, arguments: [self.currentIndex + 1, self.count])
        }
    }
    
    fileprivate func setupNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barTintColor = Theme.themify(key: .backgroundThick)
        navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        if navigationController?.viewControllers.count == 1 {
            let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
            navigationItem.leftBarButtonItems = [exit]
        }
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
