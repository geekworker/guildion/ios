//
//  ReactionCollectionView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/09.
//

import Foundation
import UIKit

protocol ReactionCollectionViewDelegate: class {
//    func reactionCollectionView(onTapReaction emoji: String, in reactionCollectionView: ReactionCollectionView)
    func reactionCollectionView(onTapAdd reactionCollectionView: ReactionCollectionView)
    func reactionCollectionView(didSelected emoji: String, in reactionCollectionView: ReactionCollectionView)
}

class ReactionCollectionView: UICollectionView {
    public var repository: MessageEntity = MessageEntity()
    public weak var reactionDelegate: ReactionCollectionViewDelegate?
    
    static func configure(frame: CGRect) -> ReactionCollectionView {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 4
        flowLayout.minimumInteritemSpacing = 4
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = ReactionCollectionViewCell.getSize()
        let collectionView = ReactionCollectionView.init(frame: frame, collectionViewLayout: flowLayout)
        collectionView.register(UINib(resource: R.nib.reactionCollectionViewCell), forCellWithReuseIdentifier: R.nib.reactionCollectionViewCell.name)
        collectionView.register(UINib(resource: R.nib.reactionAddCollectionViewCell), forCellWithReuseIdentifier: R.nib.reactionAddCollectionViewCell.name)
        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = false
        collectionView.backgroundColor = Theme.themify(key: .transparent)
        collectionView.backgroundView?.backgroundColor = Theme.themify(key: .transparent)
        return collectionView
    }
    
    static func getSize(width: CGFloat, reactions: ReactionEntities) -> CGSize {
        guard reactions.toNestedArray().count > 0 else { return CGSize(width: width, height: 0) }
        let first_row_count = Int((width - CGFloat(ReactionAddCollectionViewCell.getSize().width + 4)) / CGFloat(4 + ReactionCollectionViewCell.getSize().width))
        let row_count: Int = Int(width / CGFloat(4 + ReactionCollectionViewCell.getSize().width))
        if reactions.toNestedArray().count > first_row_count {
            let remind_count: Int = Int((reactions.toNestedArray().count - first_row_count) / row_count) + 1
            return CGSize(
                width: width,
                height: (CGFloat(4 + ReactionCollectionViewCell.getSize().height) * CGFloat(remind_count + 1))
            )
        } else {
            return CGSize(
                width: width,
                height: ReactionCollectionViewCell.getSize().height
            )
        }
    }
    
    func setRepositories(_ repository: MessageEntity) {
        self.repository = repository
    }
    
    func getSize() -> CGSize {
        return ReactionCollectionView.getSize(width: self.frame.width, reactions: self.repository.Reactions ?? ReactionEntities())
    }
}

extension ReactionCollectionView /**: UICollectionViewDelegate, UICollectionViewDataSource **/ {
    func collectionView(numberOfItemsInSection section: Int) -> Int {
        return (repository.Reactions?.toNestedArray().count ?? 0) + 1
    }
    
    func collectionView(cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return indexPath.row == 0 ? self.collectionView(cellForAddItemAt: indexPath) : self.collectionView(cellForReactionItemAt: indexPath)
    }
    
    private func collectionView(cellForAddItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: R.nib.reactionAddCollectionViewCell.name, for: indexPath) as? ReactionAddCollectionViewCell else {
            fatalError("The dequeued cell is not an instance of CollectionCell.")
        }
        cell.delegate = self
        return cell
    }
    
    private func collectionView(cellForReactionItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: R.nib.reactionCollectionViewCell.name, for: indexPath) as? ReactionCollectionViewCell else {
            fatalError("The dequeued cell is not an instance of CollectionCell.")
        }
        if let repositories = self.repository.Reactions?.toNestedArray()[indexPath.row - 1] {
            cell.setRepositories(repositories: repositories)
        }
        cell.delegate = self
        return cell
    }
}

extension ReactionCollectionView /**: UICollectionViewDelegateFlowLayout  **/ {
    func collectionView(layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return ReactionAddCollectionViewCell.getSize()
        }
         return ReactionCollectionViewCell.getSize()
    }

    func collectionView(layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 4, right: 4)
    }
}

extension ReactionCollectionView: ReactionCollectionViewCellDelegate {
    func reactionCollectionViewCell(onTap reactionCollectionViewCell: ReactionCollectionViewCell) {
        guard let emoji = reactionCollectionViewCell.emoji else { return }
        let entity = ReactionEntity()
        entity.data = emoji
        self.repository.Reactions?.append(entity)
        self.reactionDelegate?.reactionCollectionView(didSelected: emoji, in: self)
    }
    
}

extension ReactionCollectionView: ReactionAddCollectionViewCellDelegate {
    func reactionAddCollectionViewCell(onTap reactionAddCollectionViewCell: ReactionAddCollectionViewCell) {
        self.reactionDelegate?.reactionCollectionView(onTapAdd: self)
    }
    
    func reactionAddCollectionViewCell(didSelect emoji: String) {
        let entity = ReactionEntity()
        entity.data = emoji
        self.repository.Reactions?.append(entity)
        self.reactionDelegate?.reactionCollectionView(didSelected: emoji, in: self)
        self.reloadData()
    }
}
