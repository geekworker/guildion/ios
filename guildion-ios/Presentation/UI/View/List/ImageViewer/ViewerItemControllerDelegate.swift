//
//  ViewerItemControllerDelegate.swift
//  ImageViewer
//
//  Created by Kristian Angyal on 18/07/2016.
//  Copyright © 2016 MailOnline. All rights reserved.
//

import Foundation


import UIKit

public protocol ViewerItemControllerDelegate: class {

    ///Represents a generic transitioning progress from 0 to 1 (or reversed) where 0 is no progress and 1 is fully finished transitioning. It's up to the implementing controller to make decisions about how this value is being calculated, based on the nature of transition.
    func viewerItemController(_ controller: ViewerItemController, didSwipeToDismissWithDistanceToEdge distance: CGFloat)

    func viewerItemControllerDidFinishSwipeToDismissSuccessfully()

    func viewerItemControllerDidSingleTap(_ controller: ViewerItemController)
    func viewerItemControllerDidLongPress(_ controller: ViewerItemController, in item: ViewerItemView)

    func viewerItemControllerWillAppear(_ controller: ViewerItemController)
    func viewerItemControllerWillDisappear(_ controller: ViewerItemController)
    func viewerItemControllerDidAppear(_ controller: ViewerItemController)
}
