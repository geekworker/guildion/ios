//
//  ImageViewerItemViewController.swift
//  ImageViewer
//
//  Created by Kristian Angyal on 15/07/2016.
//  Copyright © 2016 MailOnline. All rights reserved.
//

import UIKit

extension UIImageView: ViewerItemView {}

class ImageViewerItemViewController: ViewerItemBaseController<UIImageView> {
}
