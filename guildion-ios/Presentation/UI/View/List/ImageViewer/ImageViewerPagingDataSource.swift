//
//  ImageViewerPagingDataSource.swift
//  ImageViewer
//
//  Created by Kristian Angyal on 15/07/2016.
//  Copyright © 2016 MailOnline. All rights reserved.
//

import UIKit

final class ImageViewerPagingDataSource: NSObject, UIPageViewControllerDataSource {

    weak var viewerItemControllerDelegate: ViewerItemControllerDelegate?
    fileprivate weak var itemsDataSource:          ImageViewerItemsDataSource?
    fileprivate weak var displacedViewsDataSource: ImageViewerDisplacedViewsDataSource?

    fileprivate let configuration: ImageViewerConfiguration
    fileprivate var pagingMode = ImageViewerPagingMode.standard
    fileprivate var itemCount: Int { return itemsDataSource?.itemCount() ?? 0 }
    fileprivate unowned var scrubber: VideoScrubber

    init(itemsDataSource: ImageViewerItemsDataSource, displacedViewsDataSource: ImageViewerDisplacedViewsDataSource?, scrubber: VideoScrubber, configuration: ImageViewerConfiguration) {

        self.itemsDataSource = itemsDataSource
        self.displacedViewsDataSource = displacedViewsDataSource
        self.scrubber = scrubber
        self.configuration = configuration

        if itemsDataSource.itemCount() > 1 { // Potential carousel mode present in configuration only makes sense for more than 1 item

            for item in configuration {

                switch item {

                case .pagingMode(let mode): pagingMode = mode
                default: break
                }
            }
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let currentController = viewController as? ViewerItemController else { return nil }
        let previousIndex = (currentController.index == 0) ? itemCount - 1 : currentController.index - 1

        switch pagingMode {

        case .standard:
            return (currentController.index > 0) ? self.createViewerItemController(previousIndex) : nil

        case .carousel:
            return self.createViewerItemController(previousIndex)
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let currentController = viewController as? ViewerItemController  else { return nil }
        let nextIndex = (currentController.index == itemCount - 1) ? 0 : currentController.index + 1

        switch pagingMode {

        case .standard:
            return (currentController.index < itemCount - 1) ? self.createViewerItemController(nextIndex) : nil

        case .carousel:
            return self.createViewerItemController(nextIndex)
        }
    }

    func createViewerItemController(_ itemIndex: Int, isInitial: Bool = false) -> UIViewController {

        guard let itemsDataSource = itemsDataSource else { return UIViewController() }

        let item = itemsDataSource.provideImageViewerItem(itemIndex)

        switch item {

        case .image(let fetchImageBlock):

            let imageController = ImageViewerItemViewController(index: itemIndex, itemCount: itemsDataSource.itemCount(), fetchImageBlock: fetchImageBlock, configuration: configuration, isInitialController: isInitial)
            imageController.delegate = viewerItemControllerDelegate
            imageController.displacedViewsDataSource = displacedViewsDataSource

            return imageController

        case .video(let fetchImageBlock, let videoURL):

            let videoController = VideoViewerItemViewController(index: itemIndex, itemCount: itemsDataSource.itemCount(), fetchImageBlock: fetchImageBlock, videoURL: videoURL, scrubber: scrubber, configuration: configuration, isInitialController: isInitial)

            videoController.delegate = viewerItemControllerDelegate
            videoController.displacedViewsDataSource = displacedViewsDataSource

            return videoController

        case .custom(let fetchImageBlock, let itemViewControllerBlock):

            guard let viewerItemController = itemViewControllerBlock(itemIndex, itemsDataSource.itemCount(), fetchImageBlock, configuration, isInitial) as? ViewerItemController, let vc = viewerItemController as? UIViewController else { return UIViewController() }

            viewerItemController.delegate = viewerItemControllerDelegate
            viewerItemController.displacedViewsDataSource = displacedViewsDataSource

            return vc
        }
    }
}
