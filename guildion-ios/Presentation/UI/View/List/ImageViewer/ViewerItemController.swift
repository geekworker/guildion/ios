//
//  ViewerItemViewController.swift
//  ImageViewer
//
//  Created by Kristian Angyal on 01/07/2016.
//  Copyright © 2016 MailOnline. All rights reserved.
//

import UIKit

typealias Duration = TimeInterval

public protocol ViewerItemController: class {

    var index: Int { get }
    var isInitialController: Bool { get set }
    var delegate:                 ViewerItemControllerDelegate? { get set }
    var displacedViewsDataSource: ImageViewerDisplacedViewsDataSource? { get set }

    func fetchImage()

    func presentItem(alongsideAnimation: () -> Void, completion: @escaping () -> Void)
    func dismissItem(alongsideAnimation: () -> Void, completion: @escaping () -> Void)

    func closeDecorationViews(_ duration: TimeInterval)
}
