//
//  ImagePagingViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import Parchment
import UIKit

class ImagePagingViewController: PagingViewController {
    override func loadView() {
        view = ImagePagingView(
            options: options,
            collectionView: collectionView,
            pageView: pageViewController.view
        )
    }
}
