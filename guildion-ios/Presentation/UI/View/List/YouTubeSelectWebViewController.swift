//
//  YouTubeSelectWebViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import WebKit
import URLEmbeddedView
import Hydra
import PKHUD
import YoutubePlayer_in_WKWebView

protocol YouTubeSelectWebViewControllerDelegate: class {
    var current_member: MemberEntity { get set }
    func youTubeSelectWebViewController(didSelects repositories: FileEntities, in youTubeSelectWebViewController: YouTubeSelectWebViewController)
}

class YouTubeSelectWebViewController: UIViewController {
    @IBOutlet weak var mediaPickCollectionViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            mediaPickCollectionViewHeightConstraint.constant = MediaMode.off.height
        }
    }
    @IBOutlet weak var mediaPickCollectionViewContainer: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: IconButton! {
        didSet {
            backButton.buttonImage = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            backButton.imageColor = Theme.themify(key: .cancel)
            backButton.hoverColor = .white
            backButton.hoverAlpha = 0.3
            backButton.layer.cornerRadius = backButton.frame.width / 2
            backButton.clipsToBounds = true
            backButton.delegate = self
            backButton.transform = backButton.transform.scaledBy(x: -1, y: 1)
        }
    }
    @IBOutlet weak var nextButton: IconButton! {
        didSet {
            nextButton.buttonImage = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            nextButton.imageColor = Theme.themify(key: .cancel)
            nextButton.hoverColor = .white
            nextButton.hoverAlpha = 0.3
            nextButton.layer.cornerRadius = nextButton.frame.width / 2
            nextButton.clipsToBounds = true
            nextButton.delegate = self
        }
    }
    @IBOutlet weak var submitButton: SimpleButton! {
        didSet {
            submitButton.normalColor = Theme.themify(key: .main)
            submitButton.canceledColor = Theme.themify(key: .cancel)
            submitButton.canceled = true
            submitButton.delegate = self
            submitButton.text = R.string.localizable.send()
        }
    }
    @IBOutlet weak var webContainerView: UIView!
    @IBOutlet weak var webView: WKWebView!
    public var mediaPickCollectionViewController: MediaPickCollectionViewController = MediaPickCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
    var repositories: FileEntities = FileEntities()
    weak var delegate: YouTubeSelectWebViewControllerDelegate?
    
    enum MediaMode: Int {
        case off = 0
        case on = 120
        var height: CGFloat {
            return CGFloat(self.rawValue)
        }
    }
    var mediaMode: MediaMode = .off
    private var configured: Bool = false
    fileprivate var players: [String: WKYTPlayerView] = [:]
    fileprivate var pl_resolve: ((_ value: WKYTPlayerView) -> ())?
    fileprivate var pl_reject: ((_ error: Error) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureWebView()
        configureMediaPickCollectionViewController()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func updateLayout() {
        self.mediaMode = repositories.count > 0 ? .on : .off
        self.submitButton.canceled = mediaMode == .off
        self.submitButton.isHidden = mediaMode == .off
        self.backButton.imageColor = webView.canGoBack ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .cancel)
        self.nextButton.imageColor = webView.canGoForward ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .cancel)
        self.mediaPickCollectionViewHeightConstraint.constant = self.mediaMode.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func setRepositories(_ repositories: FileEntities) {
        self.repositories = repositories
        self.mediaPickCollectionViewController.setRepositories(repositories)
        self.mediaMode = repositories.count > 0 ? .on : .off
        self.submitButton.canceled = repositories.count == 0
        guard isViewLoaded else { return }
        updateLayout()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url), let url = self.webView.url {
            self.backButton.imageColor = webView.canGoBack ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .cancel)
            self.nextButton.imageColor = webView.canGoForward ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .cancel)
            self.handleNewURL(url: String(describing: url))
        }

        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            if (self.webView.estimatedProgress == 1) {}
        }
    }
    
    func loadURLString(_ urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        webView.load(URLRequest(url: url))
    }
    
    fileprivate var pre:String = ""
    
    func handleNewURL(url: String) {
        let provider = ProviderEntity.init(url: url)
        
        if let youtubeID = provider.youtubeID, pre != youtubeID {
            pre = youtubeID
            loadURLString(url + "&autoplay=0&playsinline=0")
            return
        }
        guard repositories.filter({ $0.Provider?.youtubeID == provider.youtubeID }).count == 0 else { return }
        if provider.type == .youtube {
            HUD.show(.progress)
            self.loadYouTube(provider: provider, callback: { [unowned self] in
                self.fetchDuration(player: $0, provider: provider, callback: { time in
                    provider.duration = time * 1000
                    self.fetchYouTubeData(provider: provider)
                }, onError: { _ in
                    self.fetchYouTubeData(provider: provider)
                })
            })
        }
    }
    
    func fetchYouTubeData(provider: ProviderEntity) {
        guard provider.type == .youtube else { return }
        provider.fetchYouTubeData().then(in: .main, { [unowned self] result in
            let file = provider.convertFileEntity()
            file.duration = Double(provider.duration ?? .zero)
            file.Member = self.delegate?.current_member.toNewMemory()
            file.MemberId = self.delegate?.current_member.id
            file.provider = .youtube
            file.format = .youtube
            self.repositories.append(file)
            DispatchQueue.main.async { [unowned self] in
                self.setRepositories(self.repositories)
            }
            HUD.hide()
            HUD.flash(.success)
        }).catch({ error in
            HUD.hide()
            HUD.flash(.error)
        }).cancelled({
            HUD.hide()
            HUD.flash(.error)
        })
    }
}

extension YouTubeSelectWebViewController: WKYTPlayerViewDelegate {
    fileprivate func loadYouTube(provider: ProviderEntity, callback: @escaping ((_ value: WKYTPlayerView) -> ())) {
        guard provider.type == .youtube, let youtubeID = provider.youtubeID else { return }
        let player = WKYTPlayerView()
        player.delegate = self
        self.players[provider.url] = player
        self.pl_resolve = callback
        player.load(withVideoId: youtubeID)
    }
    
    fileprivate func fetchDuration(player: WKYTPlayerView, provider: ProviderEntity, callback: @escaping ((_ value: TimeInterval) -> ()), onError: @escaping ((_ error: Error) -> ())) {
        player.getDuration({ time, error in
            if let unwrapped_error = error {
                onError(unwrapped_error)
            } else {
                callback(time)
            }
        })
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        self.pl_resolve?(playerView)
    }
}

extension YouTubeSelectWebViewController: MediaPickCollectionViewControllerDelegate {
    func configureMediaPickCollectionViewController() {
        mediaPickCollectionViewController.willMove(toParent: self)
        self.addChild(mediaPickCollectionViewController)
        mediaPickCollectionViewController.collectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
        mediaPickCollectionViewController.mediaDelegate = self
        mediaPickCollectionViewController.setRepositories(repositories)
        mediaPickCollectionViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: MediaMode.on.height)
        mediaPickCollectionViewController.setItemSize(CGSize(width: MediaMode.on.height - 10, height: MediaMode.on.height - 10))
        mediaPickCollectionViewContainer.addSubview(mediaPickCollectionViewController.view)
        mediaPickCollectionViewController.didMove(toParent: self)
        self.mediaPickCollectionViewHeightConstraint.constant = self.mediaMode.height
    }
    
    func mediaPickCollectionViewController(sync repositories: FileEntities) {
        self.setRepositories(repositories)
    }
}

extension YouTubeSelectWebViewController {
    fileprivate func configureWebView() {
        webView.configuration.allowsInlineMediaPlayback = true
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        configured = true
    }
    
    public func startExploreYouTube() {
        if !configured { configureWebView() }
        let yURL = URL(string:"https://www.youtube.com?playsinline=1")
        let yRequest = URLRequest(url: yURL!)
        HUD.show(.progress)
        webView.load(yRequest)
    }
}

extension YouTubeSelectWebViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUD.hide()
        let stopVideoScript = "var videos = document.getElementsByTagName('video'); for( var i = 0; i < videos.length; i++ ){videos.item(i).pause()}"
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.webView.evaluateJavaScript(stopVideoScript, completionHandler:nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.webView.evaluateJavaScript(stopVideoScript, completionHandler:nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.webView.evaluateJavaScript(stopVideoScript, completionHandler:nil)
                })
            })
        })
        self.webView.evaluateJavaScript(stopVideoScript, completionHandler:nil)
    }
}

extension YouTubeSelectWebViewController: WKNavigationDelegate {
}

extension YouTubeSelectWebViewController: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        switch iconButton {
        case nextButton: webView.goForward()
        case backButton: webView.goBack()
        default: break
        }
    }
}

extension YouTubeSelectWebViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.delegate?.youTubeSelectWebViewController(didSelects: repositories, in: self)
    }
}
