//
//  MessageLabel.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/22.
//

import Foundation
import TTTAttributedLabel
import UIKit

class MessageLabelManager {
    public weak var label: TTTAttributedLabel!
    public var repository: MessageEntity = MessageEntity()
    public var repositories: MemberEntities = MemberEntities()
    
    static func configure(label: TTTAttributedLabel) {
        label.lineHeightMultiple = 1.4
        label.lineSpacing = 0
        label.textColor = Theme.themify(key: .string)
        label.font = Font.FT(size: .FM, family: .L)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
        label.tintColor = Theme.themify(key: .main)
        configureLinkAttributes(label: label)
    }

    static func configureLinkAttributes(label: TTTAttributedLabel) {
        label.linkAttributes = [
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.foregroundColor: Theme.themify(key: .link),
            NSAttributedString.Key.backgroundColor: Theme.themify(key: .transparent),
        ]
        label.activeLinkAttributes = [
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.foregroundColor: Theme.themify(key: .link),
            NSAttributedString.Key.backgroundColor: Theme.themify(key: .transparent),
        ]
    }
    
    func setLabel(_ label: TTTAttributedLabel) {
        self.label = label
        MessageLabelManager.configure(label: label)
        MessageLabelManager.configureLinkAttributes(label: label)
    }
    
    func setRepository(_ repository: MessageEntity) {
        self.repository = repository
        self.configureLabelTextDidChange()
    }
    
    func setRepositories(_ repositories: MemberEntities) {
        self.repositories = repositories
        self.highlightMentionsInLabel()
    }

    func configureLabelTextDidChange() {
        self.highlightEveryoneMentionInLabel()
        self.highlightHereMentionInLabel()
        self.highlightDurationsInLabel()
        self.highlightMentionsInLabel()
    }
    
    func highlightMentionsInLabel() {
        for repository in repositories.items {
            highlightMentionInLabel(repository)
        }
    }
    
    func highlightMentionInLabel(_ repository: MemberEntity) {
        guard let text: String = label.text as? String else { return }
        let mentionExpression = try? NSRegularExpression(pattern: "@\(repository.nickname)", options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches {
            label.addLink(with: match, attributes: [
                NSAttributedString.Key.foregroundColor: Theme.themify(key: .mainLight) as Any,
                NSAttributedString.Key.backgroundColor: Theme.themify(key: .main).darker(by: 20)?.withAlphaComponent(0.15) as Any,
            ])
        }
    }
    
    static let everyone_mention_pattern = "@everyone"
    func highlightEveryoneMentionInLabel() {
        guard let text: String = label.text as? String else { return }
        let mentionExpression = try? NSRegularExpression(pattern: MessageLabelManager.everyone_mention_pattern, options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches {
            label.addLink(with: match, attributes: [
                NSAttributedString.Key.foregroundColor: Theme.themify(key: .warning).lighter(by: 5) as Any,
                NSAttributedString.Key.backgroundColor: Theme.themify(key: .warning).darker(by: 20)?.withAlphaComponent(0.15) as Any,
            ])
        }
    }
    
    static let here_mention_pattern = "@here"
    func highlightHereMentionInLabel() {
        guard let text: String = label.text as? String else { return }
        let mentionExpression = try? NSRegularExpression(pattern: MessageLabelManager.here_mention_pattern, options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches {
            label.addLink(with: match, attributes: [
                NSAttributedString.Key.foregroundColor: Theme.themify(key: .warning).lighter(by: 5) as Any,
                NSAttributedString.Key.backgroundColor: Theme.themify(key: .warning).darker(by: 20)?.withAlphaComponent(0.15) as Any,
            ])
        }
    }
    
    static let duration_pattern = "([0-9]{1,2}:){1,2}[0-9][0-9]"
    func highlightDurationsInLabel() {
        guard let text: String = label.text as? String else { return }
        let mentionExpression = try? NSRegularExpression(pattern: MessageLabelManager.duration_pattern, options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches {
            label.addLink(with: match, attributes: [
                NSAttributedString.Key.foregroundColor: Theme.themify(key: .mainLight) as Any,
            ])
        }
    }
}
