//
//  ChannelMessagesCollectionView.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/03.
//

import Foundation
import MessageKit
import UIKit

class ChannelMessagesCollectionView: MessagesCollectionView {
    override func scrollToBottom(animated: Bool) {
        self._scrollToBottom(animated: animated)
    }
}
