//
//  ChannelMessageSizeCalculator.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import MessageKit
import UIKit
import TTTAttributedLabel

class ChannelMessageSizeCalculator: MessageSizeCalculator {
    
    override init(layout: MessagesCollectionViewFlowLayout? = nil) {
        super.init()
        self.layout = layout
    }
    
    override func sizeForItem(at indexPath: IndexPath) -> CGSize {
        let dataSource = messagesLayout.messagesDataSource
        let message = dataSource.messageForItem(at: indexPath, in: messagesLayout.messagesCollectionView)
        let itemHeight = cellContentHeight(for: message, at: indexPath)
        return CGSize(width: messagesLayout.itemWidth, height: itemHeight)
    }
    
    override func cellContentHeight(for message: MessageType, at indexPath: IndexPath) -> CGFloat {
        guard case .custom(let data) = message.kind, let repository = data as? MessageEntity else { return 0 }
        //let dataSource = messagesLayout.messagesDataSource
        
        let types = Message_CustomMessageTypesTranslator.translate((current: repository, messagesLayout: messagesLayout, indexPath: indexPath))
        
        guard !types.contains(.channelog) else { return self.chanenlogContentHeight(for: repository, types: types, at: indexPath) + (types.contains(.reaction) ? self.reactionViewSize(for: repository, at: indexPath).height + ChannelMessageSizeCalculator.streamChannelogBottomConstant : 0) }
        
        var height: CGFloat = 0
        let nameLabelHeight = nameLabelSize(for: repository, at: indexPath).height
        if types.contains(.text) {
            let messageLabelHeight = messageLabelSize(for: repository, at: indexPath).height
            height +=
                ChannelMessageSizeCalculator.nameLabelTopConstant +
                ChannelMessageSizeCalculator.nameLabelBottomConstant +
                messageLabelHeight +
                ChannelMessageSizeCalculator.messageLabelBottomConstant
        } else {
            height += 20
        }
        
        if !types.contains(.headLess) {
            height += nameLabelHeight
            height = max(height, self.minHeaderHaveHeight(for: repository, at: indexPath))
            // FIXME
            height += 14
        } else {
            height += ChannelMessageSizeCalculator.nameLabelHeaderLessBottomConstant
        }
        
        if types.contains(.timeSection) {
            height += timeSectionViewSize(for: repository, at: indexPath).height
        }
        
        if types.contains(.ogp) {
            height += ogpViewSize(for: repository, at: indexPath).height + ChannelMessageSizeCalculator.ogpViewBottomConstant
        }
        
        if types.contains(.media) {
            height += attachmentsViewSize(for: repository, at: indexPath).height
        }
        
        if types.contains(.reaction) {
            height += reactionViewSize(for: repository, at: indexPath).height + ChannelMessageSizeCalculator.reactionViewBottomConstant
        }
        
        return height
    }
    
    static let timeSectionViewTopConstant: CGFloat = 8
    static let timeSectionViewHeightConstant: CGFloat = 24
    static let avatarImageTopConstant: CGFloat = 8
    static let avatarImageLeftConstant: CGFloat = 12
    static let messageLabelRightConstant: CGFloat = 20
    static let messageLabelBottomConstant: CGFloat = 8
    static let nameLabelHeight: CGFloat = 16
    static let dateLabelHeight: CGFloat = nameLabelHeight - 4
    static let nameLabelTopConstant: CGFloat = avatarImageTopConstant
    static let nameLabelBottomConstant: CGFloat = 0
    static let nameLabelHeaderLessBottomConstant: CGFloat = -7
    static let avatarImageWidth: CGFloat = 36
    static let avatarImageHeight: CGFloat = 36
    static let avatarImageBottomMinConstant: CGFloat = 8
    static let contentLeftConstant: CGFloat = 56
    static let attachmentMarginConstant: CGFloat = 4
    static let attachmentRawPixelRatio: CGFloat = 0.2
    static let ogpViewHeight: CGFloat = 80
    static let ogpMarginConstant: CGFloat = 4
    static let ogpViewBottomConstant: CGFloat = 12
    static let streamChannelogHeight: CGFloat = 80
    static let streamChannelogBottomConstant: CGFloat = 4
    static let reactionViewBottomConstant: CGFloat = 12
    
    fileprivate func chanenlogContentHeight(for repository: MessageEntity, types: [CustomMessageTypes], at indexPath: IndexPath) -> CGFloat {
        guard types.contains(.channelog), let channelog = repository.Channelog, channelog.id != nil && channelog.id != 0 else { return 0 }
        var height = ChannelMessageSizeCalculator.streamChannelogHeight + ChannelMessageSizeCalculator.streamChannelogBottomConstant
        if types.contains(.timeSection) {
            height += timeSectionViewSize(for: repository, at: indexPath).height
            height += 14
        }
        return height
    }
    
    func messageLabelSize(for repository: MessageEntity, at indexPath: IndexPath) -> CGSize {
        let contentLeftConstant = ChannelMessageSizeCalculator.contentLeftConstant
        let messageLabelRightConstant = ChannelMessageSizeCalculator.messageLabelRightConstant
        let labelWidth = messagesLayout.itemWidth - contentLeftConstant - messageLabelRightConstant
        guard repository.text.count > 0 else { return CGSize(width: labelWidth, height: 0) }
        let sizeCalculatorView = ChannelMessageTextHeightSizeCalculator(frame: CGRect(x: 0, y: 0, width: labelWidth, height: .greatestFiniteMagnitude))
        let label = sizeCalculatorView.messageLabel
        label?.text = repository.text
        sizeCalculatorView.layoutIfNeeded()
        return CGSize(width: sizeCalculatorView.messageLabel.frame.size.width, height: sizeCalculatorView.messageLabel.frame.size.height + 14)
    }
    
    func avatarSize(for repository: MessageEntity, at indexPath: IndexPath) -> CGSize {
        return CGSize(width: ChannelMessageSizeCalculator.avatarImageWidth, height: ChannelMessageSizeCalculator.avatarImageHeight)
    }
    
    func timeSectionViewSize(for repository: MessageEntity, at indexPath: IndexPath) -> CGSize {
        return CGSize(width: messagesLayout.itemWidth, height: ChannelMessageSizeCalculator.timeSectionViewHeightConstant)
    }
    
    func nameLabelSize(for repository: MessageEntity, at indexPath: IndexPath) -> CGSize {
        let label = UILabel()
        ChannelMessageCollectionViewCell.configureNameLabel(label)
        let rect: CGSize = label.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: ChannelMessageSizeCalculator.nameLabelHeight))
        return rect
    }
    
    func ogpViewSize(for repository: MessageEntity, at indexPath: IndexPath) -> CGSize {
        let urls = repository.text.extractURLExcludeYouTube()
        guard urls.count > 0 else { return CGSize(width: 0, height: 0) }
        var height: CGFloat = ChannelMessageSizeCalculator.ogpMarginConstant
        for url in urls {
            height += ogpViewSize(for: url, of: repository, at: indexPath).height + ChannelMessageSizeCalculator.ogpMarginConstant
        }
        return CGSize(width: messageLabelSize(for: repository, at: indexPath).width, height: height)
    }
    
    func ogpViewSize(for repository: URL, of message: MessageEntity, at indexPath: IndexPath) -> CGSize {
        let maxWidth = messagesLayout.itemWidth - ChannelMessageSizeCalculator.contentLeftConstant - ChannelMessageSizeCalculator.messageLabelRightConstant,
            height = ChannelMessageSizeCalculator.ogpViewHeight
        return CGSize(width: maxWidth, height: height)
    }
    
    func reactionViewSize(for repository: MessageEntity, at indexPath: IndexPath) -> CGSize {
        let contentLeftConstant = ChannelMessageSizeCalculator.contentLeftConstant,
            messageLabelRightConstant = ChannelMessageSizeCalculator.messageLabelRightConstant,
            labelWidth = messagesLayout.itemWidth - contentLeftConstant - messageLabelRightConstant
        return ReactionCollectionView.getSize(width: labelWidth, reactions: repository.Reactions ?? ReactionEntities())
    }
    
    func minHeaderHaveHeight(for repository: MessageEntity, at indexPath: IndexPath) -> CGFloat {
        return self.avatarSize(for: repository, at: indexPath).height + ChannelMessageSizeCalculator.avatarImageTopConstant + ChannelMessageSizeCalculator.avatarImageBottomMinConstant
    }
    
    func attachmentsViewSize(for repository: MessageEntity, at indexPath: IndexPath) -> CGSize {
        guard repository.Files?.count ?? 0 > 0 else { return CGSize(width: 0, height: 0) }
        var height: CGFloat = ChannelMessageSizeCalculator.attachmentMarginConstant
        for file in repository.Files?.items ?? [] {
            height += attachmentViewSize(for: file, of: repository, at: indexPath).height + ChannelMessageSizeCalculator.attachmentMarginConstant
        }
        return CGSize(width: messageLabelSize(for: repository, at: indexPath).width, height: height)
    }
    
    func attachmentViewSize(for repository: FileEntity, of message: MessageEntity, at indexPath: IndexPath) -> CGSize {
        guard repository.format != .youtube else { return self.attachmentViewSizeForYouTube(for: repository, of: message, at: indexPath) }
        let maxWidth = messagesLayout.itemWidth - ChannelMessageSizeCalculator.contentLeftConstant - ChannelMessageSizeCalculator.messageLabelRightConstant,
            heightAspect = repository.pixel_height / repository.pixel_width,
            width = min(maxWidth, CGFloat(repository.pixel_width) * ChannelMessageSizeCalculator.attachmentRawPixelRatio),
            height = min(maxWidth, width * CGFloat(heightAspect))
        return CGSize(width: width, height: height)
    }
    
    func attachmentViewSizeForYouTube(for repository: FileEntity, of message: MessageEntity, at indexPath: IndexPath) -> CGSize {
        let maxWidth = messagesLayout.itemWidth - ChannelMessageSizeCalculator.contentLeftConstant - ChannelMessageSizeCalculator.messageLabelRightConstant,
            heightAspect = repository.pixel_height / repository.pixel_width,
            width = min(maxWidth, CGFloat(repository.pixel_width * 2) * ChannelMessageSizeCalculator.attachmentRawPixelRatio),
            height = min(maxWidth, width * CGFloat(heightAspect))
        return CGSize(width: width, height: height)
    }
}
