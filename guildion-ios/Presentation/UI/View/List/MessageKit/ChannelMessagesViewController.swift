//
//  ChannelMessagesViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//


import UIKit
import MessageKit
import InputBarAccessoryView
import Nuke
import SPPermissions
import Photos
import Hydra
import PKHUD
import YoutubePlayer_in_WKWebView
import URLEmbeddedView
import EmojiPicker
import RxSwift
import RxCocoa
import M13ProgressSuite
import ReSwift
import SwiftyJSON
import SideMenu

class ChannelMessagesViewController: MessagesViewController, PanContainerPresenter {
    internal var _messagesCollectionView: MessagesCollectionView = ChannelMessagesCollectionView()
    override var messagesCollectionView: MessagesCollectionView {
        get {
            return self._messagesCollectionView
        }
        set {
            self._messagesCollectionView = newValue
        }
    }
    var containerTransitionContext: PanContainerViewTransitionContext = PanContainerViewTransitionContext()
    public var current_duration: Float = 0
    public var formattedDuration: String {
        let duration = TimeInterval(self.current_duration)
        return duration.hour > 0 ? String(format:"%0d:%02d:%02d", duration.hour, duration.minute, duration.second) : String(format:"%0d:%02d", duration.minute, duration.second)
    }
    private var scrollContentOffset: CGPoint = .zero
    private var skipScrollContentOffset: CGPoint = .zero
    private var progressing = false
    private var fetchingYouTubeOGP = false
    private var progress: CGFloat = 0
    private lazy var sideMenuSettings: SideMenuSettings = {
        var settings = SideMenuSettings()
        settings.completeGestureDuration = 0.2
        settings.dismissDuration = 0.2
        settings.statusBarEndAlpha = 0
        
        let appScreenRect = UIApplication.shared.keyWindow?.bounds ?? UIWindow().bounds
        settings.menuWidth = round(appScreenRect.width * 0.95)
        
        return settings
    }()
    public let preheater = ImagePreheater()
    private var picker: EmojiPickerViewController!
    public var disposeBag: DisposeBag = DisposeBag()
    private var popOverTempView = UIView()
    private var popoverMessage: MessageEntity?
    public var permission: PermissionModel = PermissionModel()
    public var channels: Channelables = Channelables()
    public var channel: MessageableEntity = MessageableEntity()
    public var _repositories: MessageEntities = MessageEntities()
    public var repositories: MessageEntities {
        get {
            return _repositories
        }
        set {
            _repositories = MessageDispatcher.binds(contents: newValue, state: store.state.message)
            _repositories.items = _repositories.items.unique
        }
    }
    public var caches: MessageEntities = MessageEntities() {
        didSet {
            DispatchQueue.main.async {
                self.caches.count > 0 ? self.messageInputBar.sendButton.startAnimating() : self.messageInputBar.sendButton.stopAnimating()
            }
        }
    }
    public var newRepository: MessageEntity = MessageEntity()
    public var current_member: MemberEntity = MemberEntity()
    public var current_members: MemberEntities = MemberEntities() {
        didSet {
            self.messageInputTextViewManager.setRepositories(current_members)
        }
    }
    public var current_guild: GuildEntity = GuildEntity()
    public lazy var current_sender: SenderType = {
        return MessageUser.getCurrentSender()
    }()
    public lazy var assetsPickerHandler: AssetsPickerHandler = { [unowned self] in
        let handler = AssetsPickerHandler()
        handler.delegate = self
        return handler
    }()
    lazy var messageInputTextViewManager: MessageInputTextViewManager = {
        return MessageInputTextViewManager()
    }()
    public var mediaPickCollectionViewController: MediaPickCollectionViewController = MediaPickCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
    public lazy var mediaPickInputItem: InputBarButtonItem = {
        return InputBarButtonItem()
            .configure {
                $0.spacing = .none
                $0.backgroundColor = Theme.themify(key: .stringContrast)
                $0.setSize(CGSize(width: view.frame.width, height: 120), animated: true)
            }
            .onTouchUpInside { _ in }
    }()
    public lazy var mediaPickInputButton: InputBarButtonItem = { [unowned self] in
        return InputBarButtonItem()
            .configure {
                $0.spacing = .none
                $0.image = R.image.insertPicture()!.withRenderingMode(.alwaysTemplate)
                $0.setSize(CGSize(width: 24, height: 34), animated: false)
                $0.tintColor = Theme.themify(key: .placeholder)
            }.onSelected { [unowned self] item in
                UIView.animate(withDuration: 0.3, animations: {
                    item.tintColor = Theme.themify(key: .placeholder).lighter(by: 10)
                })
                guard self.configurePhotoPermission() else { return }
                self.assetsPickerHandler.openImagePicker()
            }.onDeselected { _ in
//                $0.tintColor = Theme.themify(key: .cancel)
            }.onTouchUpInside { _ in
        }
    }()
    public lazy var youtubeInputButton: InputBarButtonItem = { [unowned self] in
        return InputBarButtonItem()
            .configure {
                $0.spacing = .none
                $0.imageView?.contentMode = .scaleAspectFit
                $0.image = R.image.yt_icon_rgb()!.withRenderingMode(.alwaysOriginal)
                $0.setSize(CGSize(width: 24, height: 34), animated: false)
            }.onSelected { [unowned self] item in
                self.toWebBrowser(URL(string: "https://youtube.com")!)
            }.onDeselected { _ in
            }.onTouchUpInside { _ in
        }
    }()
    public lazy var browserInputButton: InputBarButtonItem = { [unowned self] in
        return InputBarButtonItem()
            .configure {
                $0.spacing = .none
                $0.imageView?.contentMode = .scaleAspectFit
                $0.image = R.image.compass()!.withRenderingMode(.alwaysOriginal)
                $0.setSize(CGSize(width: 24, height: 34), animated: false)
            }.onSelected { [unowned self] item in
                self.toWebBrowser(URL(string: "https://google.com")!)
            }.onDeselected { _ in
            }.onTouchUpInside { _ in
        }
    }()
    lazy var pendingLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Theme.themify(key: .background)
        label.textColor = Theme.themify(key: .stringSecondary)
        label.text = R.string.localizable.channelNotAllowSendMessage()
        label.font = Font.FT(size: .S, family: .L)
        label.minimumScaleFactor = 0.5
        label.textAlignment = .left
        return label
    }()
    lazy var autocompleteManager: AutocompleteManager = { [unowned self] in
       let manager = AutocompleteManager(for: self.messageInputBar.inputTextView)
       manager.delegate = self
       manager.dataSource = self
       return manager
    }()
    lazy var durationCompleteView: DurationCompleteView = {
        let view = DurationCompleteView()
        view.backgroundColor = Theme.themify(key: .background)
        view.delegate = self
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        return view
    }()
    fileprivate var youtubeURLs: [String] = []
    fileprivate var players: [String: WKYTPlayerView] = [:]
    fileprivate var pl_resolve: ((_ value: WKYTPlayerView) -> ())?
    fileprivate var pl_reject: ((_ error: Error) -> ())?
    internal var inputAccessoryViewHeight: CGFloat = 0
    
    override func viewDidLoad() {
        configureMessageCollectionView()
        configureMessageInputBar()
        configureAutoCompleteManager()
        configureMediaPickCollectionViewController()
        // MARK: super.viewDidLoad() should call last
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureSideMenu()
        self.messageInputBar.isHidden = false
        configureProgressBar()
        store.subscribe(self)
        WebSocketHandler.shared.inject(self)
        self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            if self.caches.count == 0 && !self.fetchingYouTubeOGP && self.messageInputBar.sendButton.isAnimating { self.messageInputBar.sendButton.stopAnimating() }
            self.websocket_health_check().then({ self.viewWebSocketHealthChecked($0) })
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented
            if self.inputAccessoryViewHeight == 0 {
                self.inputAccessoryViewHeight = self.inputAccessoryView?.frame.height ?? 0
            }
            self.updateMessageInput()
        }
    }
    
    func viewWebSocketHealthChecked(_ health: Bool) {
    }
    
    func viewWebSocketDidDisconnect() {}
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        store.unsubscribe(self)
        WebSocketHandler.shared.reject(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sideMenuNavigationController = segue.destination as? SideMenuNavigationController {
            sideMenuNavigationController.sideMenuDelegate = self
            sideMenuNavigationController.settings = sideMenuSettings
        }
    }
    
    func willDismissPanContainer(_ viewToPresent: PanContainerAnimatable) {
        self.inputAccessoryView?.isHidden = false
        self.additionalBottomInset = 0
        self.updateBottomInset()
        self.becomeFirstResponder()
    }
    
    func didDismissPanContainer(_ viewToPresent: PanContainerAnimatable) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.inputAccessoryView?.isHidden = false
        self.additionalBottomInset = 0
        self.updateBottomInset()
        self.becomeFirstResponder()
    }
    
    func dismissShouldAppear(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard self.tabBarController as? BaseTabBarController == nil else { return }
        self.dismiss(animated: flag, completion: {
            UIViewController.topMostController().viewWillAppear(false)
            UIViewController.topMostController().viewDidAppear(false)
            completion?()
        })
    }
    
    override var shouldAutorotate: Bool {
        if let vc = LiveConfig.playingStreamChannelViewController, !vc.miniScreenTransitionContext.isMiniScreenViewPresented {
            return vc.shouldAutorotate
        }
        return UIDevice.current.orientation == .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if let vc = LiveConfig.playingStreamChannelViewController, !vc.miniScreenTransitionContext.isMiniScreenViewPresented {
            return vc.supportedInterfaceOrientations
        }
        return .portrait
    }
    
    // MARK: should override
    @objc func increment() {}
    @objc func decrement() {}
    @objc func willTyping() {}
    @objc func didTyping() {}
    public func didRecieve(_ event: WebRTCEvent) {}
    public func seek(at duration: Float) {}
    public func verticalScrollDirection(_ direction: ScrollDirection) {}
    public func horizontalScrollDirection(_ direction: ScrollDirection) {}
    
    func toMemberMenu(repository: MemberEntity) {}
    func toMessageMenu(repository: MessageEntity) {}
    func toWebBrowser(_ url: URL, repositories: FileEntities? = nil) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    func toImageViewer(repository: FileEntity) {}
    
    func sendMessage(_ newRepository: MessageEntity) {
        guard let channel_uid = self.channel.messageable_uid else { return }
        let entities = MessageEntities()
        entities.items = [newRepository]
        store.dispatch(MessageDispatcher.ADD_CHANNEL_CHACHE_MESSAGES(channel_uid: channel_uid, payload: entities))
    }
    
    func updateBottomInset() {
        self.messagesCollectionView.contentInset.bottom = inputAccessoryViewHeight + messageInputBar.topStackView.frame.height
        self.messagesCollectionView.scrollIndicatorInsets.bottom = 0
    }
    
    func sendReaction(_ newRepository: ReactionEntity) {}
    
    func didCreated(_ repository: MessageEntity) {}
    
    func didPlayTime(_ duration: Float) {
        self.current_duration = duration
        self.durationCompleteView.setRepository(duration)
        self.setDurationcompleteView(active: duration >= 1)
    }
    
    func didIncrements(_ repositories: MessageEntities) {
        if repositories.count != 0, repositories.last?.id != loadingIncrementStatus {
            loadingIncrementStatus = nil
            DispatchQueue.main.async {
                self.repositories = repositories
                self.messagesCollectionView.reloadDataAndKeepOffset()
                if self.repositories.count == 1 {
                    self.messagesCollectionView._scrollToBottom(animated: true)
                }
            }
        }
    }
    
    func didDecrements(_ repositories: MessageEntities) {
        if repositories.count != 0, repositories.first?.id != loadingDecrementStatus {
            loadingDecrementStatus = nil
            DispatchQueue.main.async {
                self.repositories = repositories
                self.messagesCollectionView.reloadDataAndKeepOffset()
            }
        }
    }
    
    fileprivate var lastCachedCount: Int = 0
    func didCaches(repositories: MessageEntities, shouldScroll: Bool = false) {
        guard repositories.count > 0 else {
            self.caches = repositories
            self.lastCachedCount = self.caches.count
            return
        }
        DispatchQueue.main.async { [unowned self] in
            self.caches = repositories
            guard self.lastCachedCount != self.caches.count else { return }
            self.lastCachedCount = self.caches.count
            self.messagesCollectionView.reloadDataAndKeepOffset()
            if repositories.last?.SenderId == self.current_member.id && self.channel.messageable_type == .StreamChannel {
                self.updateBottomInset()
                self.messagesCollectionView._scrollToBottom(animated: true)
            }
        }
    }
    
    func configureMessageCollectionView() {
        let layout = ChannelMessagesFlowLayout()
        layout.sectionInset = UIEdgeInsets.zero
        layout.collectionView?.backgroundColor = Theme.themify(key: .background)
        messagesCollectionView = MessagesCollectionView(frame: .zero, collectionViewLayout: layout)
        messagesCollectionView.register(UINib(resource: R.nib.channelMessageCollectionViewCell), forCellWithReuseIdentifier: R.nib.channelMessageCollectionViewCell.name)
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.prefetchDataSource = self
        scrollsToLastItemOnKeyboardBeginsEditing = true
        maintainPositionOnKeyboardFrameChanged = true
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(singleTapGesture(_:)))
        singleTap.cancelsTouchesInView = false
        messagesCollectionView.addGestureRecognizer(singleTap)
        self.messagesCollectionView.addSubview(popOverTempView)
        self.popOverTempView.isHidden = true
        self.popOverTempView.backgroundColor = .clear
    }
    
    @objc func singleTapGesture(_ sender: Any) {
    }
    
    private func makeBarButton(named: String) -> InputBarButtonItem {
        return InputBarButtonItem()
            .configure {
                $0.spacing = .fixed(10)
                $0.image = UIImage(named: named)?.withRenderingMode(.alwaysTemplate)
                $0.setSize(CGSize(width: 22, height: 22), animated: false)
                $0.tintColor = Theme.themify(key: .placeholder)
                $0.contentEdgeInsets = UIEdgeInsets(top: 6, left: 0, bottom: 0, right: 0)
            }.onSelected {
                $0.tintColor = Theme.themify(key: .main)
            }.onDeselected {
                $0.tintColor = Theme.themify(key: .cancel)
            }.onTouchUpInside { _ in
        }
    }
    
    func configureAutoCompleteManager() {
        autocompleteManager = AutocompleteManager(for: self.messageInputBar.inputTextView)
        autocompleteManager.defaultTextAttributes = [.font: Font.FT(size: .M, family: .L), .foregroundColor: Theme.themify(key: .string)]
        autocompleteManager.delegate = self
        autocompleteManager.dataSource = self
        autocompleteManager.tableView.register([
            UINib(resource: R.nib.autocompleteMemberTableViewCell): R.nib.autocompleteMemberTableViewCell.name,
            UINib(resource: R.nib.autocompleteChannelTableViewCell): R.nib.autocompleteChannelTableViewCell.name,
            UINib(resource: R.nib.autocompleteDurationTableViewCell): R.nib.autocompleteDurationTableViewCell.name,
        ])
        autocompleteManager.register(prefix: "@", with: [.font: Font.FT(size: .FM, family: .L), .foregroundColor: Theme.themify(key: .main), .backgroundColor: Theme.themify(key: .main).withAlphaComponent(0.3)])
        autocompleteManager.maxSpaceCountDuringCompletion = 1
        configureTextViewObserve()
        messageInputBar.inputPlugins = [autocompleteManager]
    }
    
    func configureMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.inputTextView.isImagePasteEnabled = false
        messageInputBar.backgroundView.backgroundColor = Theme.themify(key: .background)
        messageInputBar.contentView.backgroundColor = Theme.themify(key: .background)
        messageInputBar.inputTextView.textColor = Theme.themify(key: .string)
        messageInputTextViewManager.setTextView(messageInputBar.inputTextView)
        
        messageInputBar.setMiddleContentView(messageInputBar.inputTextView, animated: false)
        messageInputBar.setLeftStackViewWidthConstant(to: 90, animated: false)
        let leftItems = [
            mediaPickInputButton,
            .fixedSpace(8),
            youtubeInputButton,
            .fixedSpace(8),
            browserInputButton,
            .flexibleSpace
        ]
        messageInputBar.setStackViewItems(leftItems, forStack: .left, animated: false)
        
        messageInputBar.setRightStackViewWidthConstant(to: 72, animated: false)
        messageInputBar.setStackViewItems([InputBarButtonItem.fixedSpace(36), messageInputBar.sendButton], forStack: .right, animated: false)
        messageInputBar.sendButton.imageView?.backgroundColor = Theme.themify(key: .cancel)
        messageInputBar.sendButton.contentEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        messageInputBar.sendButton.setSize(CGSize(width: 36, height: 36), animated: false)
        messageInputBar.sendButton.image = R.image.ic_send()
        messageInputBar.sendButton.activityViewColor = Theme.themify(key: .backgroundContrast)
        messageInputBar.sendButton.title = nil
        messageInputBar.sendButton.imageView?.layer.cornerRadius = 16
        messageInputBar.middleContentViewPadding.right = -38
        messageInputBar.sendButton.setTitleColor(
            Theme.themify(key: .main),
            for: .normal
        )
        messageInputBar.sendButton.setTitleColor(
            Theme.themify(key: .main).darker(by: 10),
            for: .highlighted
        )
        messageInputBar.sendButton
            .onSelected { item in
                UIView.animate(withDuration: 0.3, animations: {
                    item.imageView?.backgroundColor = item.isEnabled ? Theme.themify(key: .main).lighter(by: 10) : Theme.themify(key: .cancel).lighter(by: 10)
                })
            }
            .onEnabled { item in
                UIView.animate(withDuration: 0.3, animations: {
                    item.imageView?.backgroundColor = Theme.themify(key: .main)
                })
            }.onDisabled { item in
                UIView.animate(withDuration: 0.3, animations: {
                    item.imageView?.backgroundColor = Theme.themify(key: .cancel)
                })
            }
        messageInputBar.shouldManageSendButtonEnabledState = false
        messageInputBar.sendButton.activityViewColor = Theme.themify(key: .backgroundContrast)
        messageInputBar.separatorLine.backgroundColor = Theme.themify(key: .border)
        self.becomeFirstResponder()
    }
    
    func configureNotSendableMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.setStackViewItems([], forStack: .top, animated: false)
        messageInputBar.setStackViewItems([], forStack: .left, animated: false)
        messageInputBar.setRightStackViewWidthConstant(to: 0, animated: false)
        messageInputBar.setLeftStackViewWidthConstant(to: 0, animated: false)
        messageInputBar.setMiddleContentView(pendingLabel, animated: false)
    }
    
    func configureNotAttachmentableInputBar() {
        messageInputBar.setStackViewItems([], forStack: .left, animated: false)
    }
    
    func evalPermission() {
        switch self.channel.messageable_type {
        case .DMChannel: configureMessageInputBar()
        case .StreamChannel:
            guard !(channel as! StreamChannelEntity).is_dm else { configureMessageInputBar(); return }
            if (permission.StreamChannel.message_sendable && !(channel as! StreamChannelEntity).read_only) || permission.StreamChannel.channel_managable {
                configureMessageInputBar()
                if !permission.StreamChannel.message_attachable {
                    configureNotAttachmentableInputBar()
                }
            } else {
                configureNotSendableMessageInputBar()
            }
        case .TextChannel:
            if (permission.TextChannel.message_sendable && !(channel as! TextChannelEntity).read_only) || permission.TextChannel.channel_managable {
                configureMessageInputBar()
                if !permission.TextChannel.message_attachable {
                    configureNotAttachmentableInputBar()
                }
            } else {
                configureNotSendableMessageInputBar()
            }
        default: break
        }
    }
    
    func updateMessageInput() {
        self.messageInputBar.inputTextView.text = newRepository.text
        if newRepository.Files == nil {
            newRepository.Files = FileEntities()
        }
        mediaPickCollectionViewController.setRepositories(newRepository.Files!)
        self.updateSendButtonState()
    }
    
    // MARK: UICollectionViewDelegate, UICollectionViewDataSorce
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let messagesDataSource = messagesCollectionView.messagesDataSource else {
            fatalError("Ouch. nil data source for messages")
        }
        
        let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView)
        if case .custom = message.kind {
            let cell = messagesCollectionView.dequeueReusableCell(ChannelMessageCollectionViewCell.self, for: indexPath)
            cell.setRepositories(current_members)
            cell.configure(with: message, at: indexPath, and: self.messagesCollectionView)
            cell.delegate = self
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    private var loadingDecrementStatus: Int?
    private var loadingIncrementStatus: Int?
    private var lastWillDisplay: Int?
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if repositories.count > indexPath.section, let repository = repositories[safe: indexPath.section], let last_id = repositories.last?.id, repository.id == last_id, last_id != loadingIncrementStatus {
            loadingIncrementStatus = last_id
            self.increment()
        }
        lastWillDisplay = indexPath.section
    }
    
    fileprivate lazy var typingIndicatorView: TypingIndicatorView = {
        let typingIndicatorView = TypingIndicatorView()
        typingIndicatorView.backgroundColor = Theme.themify(key: .backgroundThick)
        typingIndicatorView.layer.borderColor = Theme.themify(key: .backgroundThick).cgColor
        typingIndicatorView.layer.borderWidth = 0.5
        return typingIndicatorView
    }()
    func addTypingMember(repository: MemberEntity) {
        self.typingIndicatorView.addRepository(repository)
        self.updateTypingIndicator()
    }
    func removeTypingMember(repository: MemberEntity) {
        self.typingIndicatorView.removeRepository(repository)
        self.updateTypingIndicator()
    }
    
    fileprivate func updateTypingIndicator() {
        let topStackView = messageInputBar.topStackView
        if typingIndicatorView.repositories.count > 0 && !topStackView.arrangedSubviews.contains(typingIndicatorView) {
            topStackView.insertArrangedSubview(typingIndicatorView, at: topStackView.arrangedSubviews.count)
            topStackView.layoutIfNeeded()
        } else if typingIndicatorView.repositories.count == 0 && topStackView.arrangedSubviews.contains(typingIndicatorView) {
            topStackView.removeArrangedSubview(typingIndicatorView)
            topStackView.layoutIfNeeded()
        }
        messageInputBar.invalidateIntrinsicContentSize()
    }
    
    func isLastSectionVisible() -> Bool {
        guard repositories.count != 0 else { return false }
        let lastIndexPath = IndexPath(item: 0, section: repositories.count - 1)
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }

    func isPreviousMessageSameSender(at indexPath: IndexPath) -> Bool {
        guard indexPath.section - 1 >= 0, repositories.count > indexPath.section else { return false }
        return repositories[safe: indexPath.section]?.Sender == repositories[safe: indexPath.section - 1]?.Sender
    }

    func isNextMessageSameSender(at indexPath: IndexPath) -> Bool {
        guard indexPath.section + 1 < repositories.count, repositories.count > indexPath.section else { return false }
        return repositories[safe: indexPath.section]?.Sender == repositories[safe: indexPath.section + 1]?.Sender
    }
}

extension ChannelMessagesViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.skipScrollContentOffset = self.scrollContentOffset
        self.scrollContentOffset = scrollView.contentOffset
        self.verticalScrollDirection(self.scrollViewDidChangeVerticalScrollDirection())
        self.horizontalScrollDirection(self.scrollViewDidChangeHorizontalScrollDirection())
        if scrollView.contentOffset.y < 0, loadingDecrementStatus == nil, let repository = repositories[safe: 0], let first_id = repositories.first?.id, repository.id == first_id, first_id != loadingDecrementStatus {
            loadingDecrementStatus = first_id
            self.decrement()
        }
    }
    
    func scrollViewDidChangeVerticalScrollDirection() -> ScrollDirection {
        return skipScrollContentOffset.y < scrollContentOffset.y ? ScrollDirection.Up : skipScrollContentOffset.y > scrollContentOffset.y ? .Down : .None
    }
    
    func scrollViewDidChangeHorizontalScrollDirection() -> ScrollDirection {
        return skipScrollContentOffset.x < scrollContentOffset.x ? ScrollDirection.Left : skipScrollContentOffset.x > scrollContentOffset.x ? .Right : .None
    }
}

extension ChannelMessagesViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row], !repository.is_log, let sender = repository.Sender, let url = URL(string: sender.picture_small) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension ChannelMessagesViewController: MessagesDataSource {
    func currentSender() -> SenderType {
        return self.current_sender
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        guard repositories.count > indexPath.section, let entity = repositories[safe: indexPath.section] else { return Message_MessageModelTranslator.translate(MessageEntity()) }
        return Message_MessageModelTranslator.translate(entity)
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        switch channel.messageable_type {
        case .TextChannel: return permission.TextChannel.messages_readable ? repositories.count : 0
        case .StreamChannel: return permission.StreamChannel.messages_readable ? repositories.count : 0
        default: return repositories.count
        }
        
    }
}

extension ChannelMessagesViewController: MessagesLayoutDelegate {}

extension ChannelMessagesViewController: MessagesDisplayDelegate {
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return Theme.themify(key: .background)
    }
}

extension ChannelMessagesViewController: AutocompleteManagerDelegate {
    func autocompleteManager(_ manager: AutocompleteManager, shouldBecomeVisible: Bool) {
        setAutocompleteManager(active: shouldBecomeVisible)
    }
    
    fileprivate func setAutocompleteManager(active: Bool) {
        let topStackView = messageInputBar.topStackView
        autocompleteManager.tableView.backgroundColor = Theme.themify(key: .background)
        if active && !topStackView.arrangedSubviews.contains(autocompleteManager.tableView) {
            topStackView.insertArrangedSubview(autocompleteManager.tableView, at: topStackView.arrangedSubviews.count)
            topStackView.layoutIfNeeded()
        } else if !active && topStackView.arrangedSubviews.contains(autocompleteManager.tableView) {
            topStackView.removeArrangedSubview(autocompleteManager.tableView)
            topStackView.layoutIfNeeded()
        }
        messageInputBar.invalidateIntrinsicContentSize()
    }
}

extension ChannelMessagesViewController: AutocompleteManagerDataSource {
    func autocompleteManager(_ manager: AutocompleteManager, autocompleteSourceFor prefix: String) -> [AutocompleteCompletion] {
        switch prefix {
        case "@": return [
            AutocompleteCompletion(text: "everyone"),
            AutocompleteCompletion(text: "here")
        ] + self.current_members.compactMap(
            { AutocompleteCompletion(text: $0.nickname == "" ? $0.User?.nickname ?? "" : $0.nickname, context: ["id": $0.uid]) }
        )
        case "#": return channels.items.compactMap({
            switch $0.channelable_type {
            case .TextChannel, .AnnouncementChannel: return AutocompleteCompletion(text: ($0 as! TextChannelEntity).name, context: ["id": ($0 as! TextChannelEntity).uid])
            case .StreamChannel, .MusicChannel, .VoiceChannel: return AutocompleteCompletion(text: ($0 as! StreamChannelEntity).name, context: ["id": ($0 as! StreamChannelEntity).uid])
            default: return nil
            }
        })
        default:
            switch self.channel.messageable_type {
            case .StreamChannel: return [AutocompleteCompletion(text: self.formattedDuration, context: ["current_duration": self.current_duration])]
            default: return []
            }
        }
    }
    
    func autocompleteManager(_ manager: AutocompleteManager, tableView: UITableView, cellForRowAt indexPath: IndexPath, for session: AutocompleteSession) -> UITableViewCell {
        switch session.prefix {
        case "@": return autocompleteManager(manager, tableView: tableView, cellForMentionRowAt: indexPath, for: session)
        case "#": return autocompleteManager(manager, tableView: tableView, cellForHashTagRowAt: indexPath, for: session)
        default: return autocompleteManager(manager, tableView: tableView, cellForCurrentDurationRowAt: indexPath, for: session)
        }
    }
    
    fileprivate func autocompleteManager(_ manager: AutocompleteManager, tableView: UITableView, cellForMentionRowAt indexPath: IndexPath, for session: AutocompleteSession) -> UITableViewCell {
        if let text = session.completion?.text, text == "everyone" || text == "here" {
            return autocompleteManager(manager, tableView: tableView, cellForSystemMentionRowAt: indexPath, for: session)
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.autocompleteMemberTableViewCell.name, for: indexPath) as? AutocompleteMemberTableViewCell, let id = session.completion?.context?["id"] as? String, let repository = self.current_members.filter({ return $0.uid == id }).first else {
            return UITableViewCell()
        }
        cell.setRepository(repository)
        return cell
    }
    
    fileprivate func autocompleteManager(_ manager: AutocompleteManager, tableView: UITableView, cellForSystemMentionRowAt indexPath: IndexPath, for session: AutocompleteSession) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AutocompleteCell.reuseIdentifier, for: indexPath) as? AutocompleteCell, let text = session.completion?.text, text == "everyone" || text == "here" else {
            return UITableViewCell()
        }
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .background).lighter(by: 10)
        cell.selectedBackgroundView = bgColorView
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.attributedText = manager.attributedText(matching: session, fontSize: 15)
        cell.textLabel?.textColor = Theme.themify(key: .warning)
        return cell
    }
    
    func autocompleteManager(_ manager: AutocompleteManager, tableView: UITableView, cellForHashTagRowAt indexPath: IndexPath, for session: AutocompleteSession) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.autocompleteChannelTableViewCell.name, for: indexPath) as? AutocompleteChannelTableViewCell, let id = session.completion?.context?["id"] as? String, let channel = channels.items.filter({ return $0.uid == id }).first else {
            return UITableViewCell()
        }
        cell.setRepository(channel)
        return cell
    }
    
    func autocompleteManager(_ manager: AutocompleteManager, tableView: UITableView, cellForCurrentDurationRowAt indexPath: IndexPath, for session: AutocompleteSession) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.autocompleteDurationTableViewCell.name, for: indexPath) as? AutocompleteDurationTableViewCell else {
            return UITableViewCell()
        }
        cell.setRepository(self.current_duration)
        return cell
    }
}

extension ChannelMessagesViewController: DurationCompleteViewDelegate {
    fileprivate func setDurationcompleteView(active: Bool) {
        let topStackView = messageInputBar.topStackView
        if active && !topStackView.arrangedSubviews.contains(durationCompleteView) {
            topStackView.insertArrangedSubview(durationCompleteView, at: topStackView.arrangedSubviews.count)
            topStackView.layoutIfNeeded()
            messageInputBar.invalidateIntrinsicContentSize()
            self.updateBottomInset()
        } else if !active && topStackView.arrangedSubviews.contains(durationCompleteView) {
            topStackView.removeArrangedSubview(durationCompleteView)
            topStackView.layoutIfNeeded()
            messageInputBar.invalidateIntrinsicContentSize()
            self.updateBottomInset()
        }
    }
    
    func durationCompleteView(onTap duration: Float, in durationCompleteView: DurationCompleteView) {
        let insertText = self.messageInputBar.inputTextView.text.count == 0 ? formattedDuration + " " : " " + formattedDuration + " "
        self.newRepository.text = insertText
        self.messageInputTextViewManager.insertDurationText(newText: insertText)
        self.updateSendButtonState()
    }
}

extension ChannelMessagesViewController: InputBarAccessoryViewDelegate {
    func updateSendButtonState() {
        self.messageInputBar.sendButton.isEnabled = self.newRepository.Files?.count ?? 0 > 0 || self.newRepository.text.count > 0
        self.messageInputBar.sendButton.imageView?.backgroundColor = self.messageInputBar.sendButton.isEnabled ? Theme.themify(key: .main) : Theme.themify(key: .cancel)
    }
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        self.newRepository.text = messageInputBar.inputTextView.text
        self.send()
    }
    
    fileprivate func send() {
        if let files = newRepository.Files, files.storage_count > 0, !files.available_storage {
            self.messageInputBar.isHidden = true
            BaseWireframe.toPlansWithAlert()
            return
        }
        
        self.newRepository.Sender = current_member
        self.newRepository.SenderId = current_member.id
        self.newRepository.MessageableId = self.channel.messageable_id
        self.newRepository.messageable_type = self.channel.messageable_type!
        self.newRepository.Messageable = channel
        self.newRepository.created_at = Date()
        
        messageInputBar.inputTextView.text = String()
        messageInputBar.invalidatePlugins()
        DispatchQueue.main.async {
            self.sendMessage(self.newRepository)
            self.messageInputBar.resignFirstResponder()
            self.autocompleteManager.textView?.resignFirstResponder()
            self.newRepository = MessageEntity()
            self.mediaPickCollectionViewController.setRepositories(FileEntities())
            self.setDurationcompleteView(active: false)
            self.updateSendButtonState()
            UIView.animate(withDuration: 0, animations: { [unowned self] in
                self.messageInputBar.setStackViewItems(self.messageInputBar.topStackViewItems.filter({
                    guard let unwrapped = $0 as? InputBarButtonItem else { return true }
                    return unwrapped != mediaPickInputItem
                }), forStack: .top, animated: true)
            }, completion: { [unowned self] _ in
                self.updateTypingIndicator()
            })
        }
    }
}

extension ChannelMessagesViewController: UITextViewDelegate {
    func configureTextViewObserve() {
        autocompleteManager.textView?.rx.didChange.subscribe(onNext: { [weak self] _ in
            guard let textView = self?.autocompleteManager.textView else { return }
            self?.textViewDidChange(textView)
        }).disposed(by: disposeBag)

        autocompleteManager.textView?.rx.didBeginEditing.subscribe(onNext: { [weak self] _ in
            self?.willTyping()
        }).disposed(by: disposeBag)

        autocompleteManager.textView?.rx.didEndEditing.subscribe(onNext: { [weak self] _ in
            self?.didTyping()
        }).disposed(by: disposeBag)

        autocompleteManager.textView?.rx.text.orEmpty
            .scan("") { [weak self] (previous, new) -> String in
                guard let self = self, let (_, insert_string) = diff(previous, new), let (changed_range, _) = diff(new, previous), insert_string.count > 0 else { return new }
                self.messageInputTextViewManager.configureTextViewDidChange(
                    textView: self.autocompleteManager.textView!,
                    to: new,
                    in: changed_range,
                    as: insert_string
                )
                self.updateSendButtonState()
                return new
            }
            .bind(to: autocompleteManager.textView!.rx.text)
            .disposed(by: disposeBag)
    }
    
    @objc func textViewDidChange(_ textView: UITextView) {
        self.newRepository.text = textView.text
        self.youtubeURLs = self.newRepository.Files?.compactMap({ $0.Provider?.youtubeID }) ?? []
        let youtubeURLs = textView.text.extractURL().filter({
            return ProviderEntity.getYouTubeID(urlString: "\($0)") != nil && !self.youtubeURLs.contains(ProviderEntity.getYouTubeID(urlString: "\($0)")!)
        })
        for youtubeURL in youtubeURLs {
            let provider = ProviderEntity.init(url: "\(youtubeURL)")
            self.youtubeURLs.append(provider.youtubeID!)
            if provider.type == .youtube {
                messageInputBar.sendButton.startAnimating()
                self.fetchingYouTubeOGP = true
                self.loadYouTube(provider: provider, callback: { [unowned self] in
                    self.fetchDuration(player: $0, provider: provider, callback: { [unowned self] time in
                        provider.duration = time * 1000
                        self.fetchYouTubeData(provider: provider)
                    }, onError: { [unowned self] _ in
                        self.fetchYouTubeData(provider: provider)
                    })
                })
            }
        }
        self.updateSendButtonState()
    }
    
    func fetchYouTubeData(provider: ProviderEntity) {
        guard provider.type == .youtube else { return }
        provider.fetchYouTubeData().then(in: .main, { [unowned self] result in
            let file = provider.convertFileEntity()
            file.url = provider.url
            file.duration = Double(provider.duration ?? .zero)
            file.Member = self.current_member
            file.MemberId = self.current_member.id
            file.provider = .youtube
            file.format = .youtube
            switch self.channel.messageable_type {
            case .TextChannel: file.is_private = (self.channel as! TextChannelEntity).is_private
            case .StreamChannel: file.is_private = (self.channel as! StreamChannelEntity).is_private
            case .DMChannel: file.is_private = (self.channel as! DMChannelEntity).is_private
            default: break
            }
            if self.newRepository.Files == nil {
                self.newRepository.Files = FileEntities()
            }
            self.newRepository.Files?.append(file)
            self.mediaPickCollectionViewController.setRepositories(self.newRepository.Files!)
            self.messageInputBar.setStackViewItems([self.mediaPickInputItem], forStack: .top, animated: true)
            self.messageInputBar.sendButton.stopAnimating()
            self.updateSendButtonState()
            self.fetchingYouTubeOGP = false
        }).catch({ [unowned self] error in
            self.messageInputBar.sendButton.stopAnimating()
            self.updateSendButtonState()
            self.fetchingYouTubeOGP = false
            HUD.flash(.error)
        }).cancelled({ [unowned self] in
            self.messageInputBar.sendButton.stopAnimating()
            self.updateSendButtonState()
            self.fetchingYouTubeOGP = false
            HUD.flash(.error)
        })
    }
}

extension ChannelMessagesViewController: WKYTPlayerViewDelegate {
    fileprivate func loadYouTube(provider: ProviderEntity, callback: @escaping ((_ value: WKYTPlayerView) -> ())) {
        guard provider.type == .youtube, let youtubeID = provider.youtubeID else { return }
        let player = WKYTPlayerView()
        player.delegate = self
        self.players[provider.url] = player
        self.pl_resolve = callback
        player.load(withVideoId: youtubeID)
    }
    
    fileprivate func fetchDuration(player: WKYTPlayerView, provider: ProviderEntity, callback: @escaping ((_ value: TimeInterval) -> ()), onError: @escaping ((_ error: Error) -> ())) {
        player.getDuration({ time, error in
            if let unwrapped_error = error {
                onError(unwrapped_error)
            } else {
                callback(time)
            }
        })
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        self.pl_resolve?(playerView)
    }
}

extension ChannelMessagesViewController: EmojiPickerViewControllerDelegate {
    public func showReactionPicker(at repository: MessageEntity) {
        picker = EmojiPicker.viewController
        guard let section = repositories.indexOf(repository) else { return }
        let cell = messagesCollectionView.dequeueReusableCell(ChannelMessageCollectionViewCell.self, for: IndexPath(row: 0, section: section))
        popOverTempView.frame = cell.frame
        popOverTempView.isHidden = false
        let popOverY = popOverTempView.frame.maxY - messagesCollectionView.contentOffset.y
        picker!.sourceRect = CGRect(x: popOverTempView.bounds.width / 2, y: popOverY, width: 0, height: 0)
        picker!.delegate = self
        picker!.dismissAfterSelected = true
        picker!.permittedArrowDirections = popOverY > 300 ? .down : .up
        self.popoverMessage = repository
        picker.modalPresentationStyle = .overCurrentContext
        self.present(picker!, animated: true, completion: nil)
    }
    
    func emojiPickerViewController(_ controller: EmojiPickerViewController, didSelect emoji: String) {
        let entity = ReactionEntity()
        entity.data = emoji
        entity.Member = self.current_member
        entity.MemberId = self.current_member.id
        entity.Message = self.popoverMessage
        entity.MessageId = self.popoverMessage?.id
        self.popoverMessage?.Reactions?.append(entity)
        self.popOverTempView.isHidden = true
        self.popoverMessage = nil
        DispatchQueue.main.async {
            self.messagesCollectionView.reloadDataAndKeepOffset()
        }
        self.sendReaction(entity)
    }
}


extension ChannelMessagesViewController: SPPermissionsDelegate, SPPermissionsDataSource {
    func configurePhotoPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self)
        return false
    }
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        switch permission {
        case .camera:
            cell.permissionTitleLabel.text = R.string.localizable.permissionCamera()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionCameraText()
        case .microphone:
            cell.permissionTitleLabel.text = R.string.localizable.permissionVoice()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionVoiceText()
        case .photoLibrary:
            cell.permissionTitleLabel.text = R.string.localizable.permissionPhoto()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionPhotoText()
        default: break
        }
        return cell
    }
    
    func didHide(permissions ids: [Int]) {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard permissions.isEmpty else { return }
        self.assetsPickerHandler.openImagePicker()
    }
}

extension ChannelMessagesViewController: AssetsPickerHandlerDelegate {
    var viewController: UIViewController {
        get {
            self
        }
    }
    
    func updateAssets(_ assets: FileEntities) {
        if newRepository.Files == nil {
            newRepository.Files = FileEntities()
        }
        for asset in assets.items {
            switch self.channel.messageable_type {
            case .TextChannel: asset.is_private = (self.channel as! TextChannelEntity).is_private
            case .StreamChannel: asset.is_private = (self.channel as! StreamChannelEntity).is_private
            case .DMChannel: asset.is_private = (self.channel as! DMChannelEntity).is_private
            default: break
            }
        }
        newRepository.Files?.items += assets.items
        mediaPickCollectionViewController.setRepositories(newRepository.Files!)
        messageInputBar.setStackViewItems((newRepository.Files?.count ?? 0) > 0 ? [mediaPickInputItem] : [], forStack: .top, animated: true)
        self.updateSendButtonState()
    }
    
    func cancelAssets() {
    }
}

extension ChannelMessagesViewController {
    func configureProgressBar() {
        self.navigationController?.setBackgroundColor(Theme.themify(key: .backgroundThick))
        self.navigationController?.setPrimaryColor(Theme.themify(key: .main))
        self.navigationController?.setSecondaryColor(Theme.themify(key: .borderSecondary))
    }
    
    func updateProgress() {
        DispatchQueue.main.async { [unowned self] in
            if self.progressing {
                if !(self.navigationController?.isShowingProgressBar() ?? false) {
                    self.navigationController?.showProgress()
                }
                self.navigationController?.setProgress(self.progress, animated: true)
                if self.progress >= 1 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [unowned self] in
                        self.navigationController?.finishProgress()
                        store.dispatch(AppDispatcher.FINISH_PROGRESS())
                    }
                }
            } else {
                self.navigationController?.cancelProgress()
            }
        }
    }
}

extension ChannelMessagesViewController: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    func newState(state: StoreSubscriberStateType) {
        if self.progress != state.app.progress || self.progressing != state.app.progressing {
            self.progress = state.app.progress
            self.progressing = state.app.progressing
            updateProgress()
        }
    }
}

extension ChannelMessagesViewController: MediaPickCollectionViewControllerDelegate {
    func configureMediaPickCollectionViewController() {
        mediaPickCollectionViewController.willMove(toParent: messageInputBar.viewController)
        messageInputBar.viewController?.addChild(mediaPickCollectionViewController)
        mediaPickCollectionViewController.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 500);
        mediaPickCollectionViewController.mediaDelegate = self
        if newRepository.Files == nil {
            newRepository.Files = FileEntities()
        }
        mediaPickCollectionViewController.setRepositories(newRepository.Files!)
        mediaPickCollectionViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: mediaPickInputItem.frame.height)
        mediaPickCollectionViewController.setItemSize(CGSize(width: 110, height: 110))
        mediaPickInputItem.configure {
            $0.addSubview(mediaPickCollectionViewController.view)
        }
        mediaPickCollectionViewController.didMove(toParent: messageInputBar.viewController)
    }
    
    func mediaPickCollectionViewController(sync repositories: FileEntities) {
        self.newRepository.Files = repositories
        if newRepository.Files?.count == 0 {
            messageInputBar.setStackViewItems([], forStack: .top, animated: true)
        }
        self.updateSendButtonState()
    }
}

extension ChannelMessagesViewController: WebSocketHandlerDelegate {
    public func websocket_health_check() -> Promise<Bool> {
        return Promise<Bool> { resolve, reject, _ in
            guard !WebSocketHandler.shared.connected else { resolve(true); return }
            if !WebSocketHandler.shared.retrying {
                WebSocketHandler.shared.startWebSocketWithAttempt(
                ).then({
                    resolve(true)
                }).catch(reject)
            } else {
                WebSocketHandler.shared.connect_promise?.then({
                    resolve(true)
                }).catch(reject)
            }
        }
    }
    
    func webSocketHandler(didRetry count: Int, error: Error) {
    }
    
    func webSocketHandlerDidDisconnect() {
        DispatchQueue.global(qos: .background).async {
            self.websocket_health_check().then({ self.viewWebSocketHealthChecked($0) })
        }
    }
    
    func webSocketHandler(event: WebsocketEvent, type: String, id: String, payload: JSON, client_user: UserEntity, client_room: String, sdp: String, ice: JSON) {
        let event = WebRTCEvent.init(type: type, id: id, payload: payload, client_user: client_user, client_room: client_room, sdp: sdp, ice: ice)
        self.didRecieve(event)
    }
}

extension ChannelMessagesViewController: ChannelMessageCollectionViewCellDelegate {
    func onTapURL(_ url: URL) {
        self.toWebBrowser(url)
    }
    
    func channelMessageCollectionViewCell(onLongPress repository: MessageEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        if let channelog = repository.Channelog, channelog.id != 0 && channelog.id != nil, let vc = R.storyboard.folders.fileMenuViewController() {
            vc.setChannelog(repository: repository, current_guild: current_guild, current_member: current_member)
            vc.delegate = self
            self.presentPanModal(vc)
            return
        }
        self.toMessageMenu(repository: repository)
    }
    
    func channelMessageCollectionViewCell(onTapAvatar repository: MemberEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        self.toMemberMenu(repository: repository)
    }
    
    func channelMessageCollectionViewCell(onTapURL url: URL, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        self.onTapURL(url)
    }
    
    func channelMessageCollectionViewCell(onTapDuration duration: Float, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        self.seek(at: duration)
    }
    
    func channelMessageCollectionViewCell(onTapAttachment repository: FileEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        if let vc = R.storyboard.folders.fileMenuViewController() {
            vc.initRepository(repository: repository, current_guild: current_guild, current_member: current_member, permission: permission)
            vc.delegate = self
            self.presentPanModal(vc)
        }
    }
    
    func channelMessageCollectionViewCell(onTapChannelog repository: MessageEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        if let vc = R.storyboard.folders.fileMenuViewController() {
            vc.setChannelog(repository: repository, current_guild: current_guild, current_member: current_member)
            vc.delegate = self
            self.presentPanModal(vc)
        }
    }
    
    func channelMessageCollectionViewCell(didTapAdd channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        self.showReactionPicker(at: channelMessageCollectionViewCell.repository)
    }
    
    func channelMessageCollectionViewCell(didSelect emoji: String, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
        let entity = ReactionEntity()
        entity.data = emoji
        entity.Member = self.current_member
        entity.MemberId = self.current_member.id
        entity.Message = channelMessageCollectionViewCell.repository
        entity.MessageId = channelMessageCollectionViewCell.repository.id
        self.sendReaction(entity)
    }
    
    func channelMessageCollectionViewCell(onProgressEnd repository: FileEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell) {
    }
}

extension ChannelMessagesViewController: WebKitViewControllerDelegate {
    func webKitViewController(didSelects repositories: FileEntities, in webKitViewController: WebKitViewController) {
        guard repositories.count > 0 else { return }
        repositories.items = (newRepository.Files?.items ?? []) + repositories.items
        self.newRepository.Files = repositories
        self.mediaPickCollectionViewController.setRepositories(newRepository.Files!)
        self.messageInputBar.setStackViewItems([self.mediaPickInputItem], forStack: .top, animated: true)
        self.updateSendButtonState()
    }
    
    func webKitViewController(didSelect url: URL, in webKitViewController: WebKitViewController) {
        let insertText = self.newRepository.text.count == 0 ? url.absoluteString : "\n" + url.absoluteString
        self.newRepository.text += insertText
        messageInputTextViewManager.insertURLText(newText: insertText)
        self.textViewDidChange(messageInputBar.inputTextView)
        self.updateSendButtonState()
    }
    
    func webKitViewController(shouldSkip duration: Float, in webKitViewController: WebKitViewController) {
        self.seek(at: duration)
    }
}

extension ChannelMessagesViewController: FileMenuViewControllerDelegate {
    func toWebBrowser(url: URL) {
        self.toWebBrowser(url)
    }
}

extension ChannelMessagesViewController: SideMenuNavigationControllerDelegate {
    fileprivate func configureSideMenu() {
        SideMenuManager.default.rightMenuNavigationController = R.storyboard.channelMenu.channelMenuViewController()
        // SideMenuManager.default.addPanGestureToPresent(toView: self.messagesCollectionView)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
        SideMenuManager.default.rightMenuNavigationController?.settings = sideMenuSettings
        SideMenuManager.default.rightMenuNavigationController?.sideMenuDelegate = self
        if let vc = SideMenuManager.default.rightMenuNavigationController?.viewControllers.first as? ChannelMenuViewController, let repository = channel as? ChannelableProtocol {
            vc.repository = repository
            if !(repository is StreamChannelEntity) {
                SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
            }
        }
    }
}
