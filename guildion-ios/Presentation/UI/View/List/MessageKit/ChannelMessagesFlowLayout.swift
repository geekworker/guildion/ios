//
//  ChannelMessagesFlowLayout.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import MessageKit
import UIKit

class ChannelMessagesFlowLayout: MessagesCollectionViewFlowLayout {
    
    lazy var channelMessageSizeCalculator = ChannelMessageSizeCalculator(layout: self)
    
    override func cellSizeCalculatorForItem(at indexPath: IndexPath) -> CellSizeCalculator {
        if isSectionReservedForTypingIndicator(indexPath.section) {
            return typingIndicatorSizeCalculator
        }
        let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView)
        switch message.kind {
        case .custom: return channelMessageSizeCalculator
        default: return super.cellSizeCalculatorForItem(at: indexPath)
        }
    }
    
    override func messageSizeCalculators() -> [MessageSizeCalculator] {
        var superCalculators = super.messageSizeCalculators()
        superCalculators.append(channelMessageSizeCalculator)
        return superCalculators
    }
}


