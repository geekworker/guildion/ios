//
//  MessageInputTextViewManager.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/29.
//

import Foundation
import TTTAttributedLabel
import UIKit

class MessageInputTextViewManager {
    public var repository: MessageEntity = MessageEntity()
    public var repositories: MemberEntities = MemberEntities()
    public weak var textView: UITextView!

    static func configure(textView: UITextView) {
    }
    
    func setTextView(_ textView: UITextView) {
        self.textView = textView
        MessageInputTextViewManager.configure(textView: textView)
    }
    
    func setRepository(_ repository: MessageEntity) {
        self.repository = repository
    }
    
    func setRepositories(_ repositories: MemberEntities) {
        self.repositories = repositories
    }
    
    static let check_diff_pattern = "\\n"
    func configureTextViewDidChange(textView: UITextView, to text: String, in range: Range<Int>, as newText: String) {
        let mentionExpression = try? NSRegularExpression(pattern: MessageInputTextViewManager.check_diff_pattern, options: [])
        let matches = mentionExpression!.matches(in: textView.text, options: [], range: NSMakeRange(0, text.count))
        let check_start_index = self.getStartIndexShouldHighlight(
            changed_start_index: range.startIndex,
            max_count: text.count,
            diff_matches: matches
        )
        let check_end_index = self.getEndIndexShouldHighlight(
            changed_end_index: range.endIndex,
            max_count: text.count,
            diff_matches: matches
        )
        let should_check_range = NSRange(location: check_start_index, length: check_end_index - check_start_index)
        let current_cursor_position = self.getCurrentCursorPosition()
        self.highlightURLs(in: should_check_range)
        self.highlightEveryoneMention(in: should_check_range)
        self.highlightHereMention(in: should_check_range)
        self.highlightDurations(in: should_check_range)
        self.highlightMentions(in: should_check_range)
        if let unwrapped = current_cursor_position {
            self.textView.selectedRange = unwrapped
        }
    }
    
    func getStartIndexShouldHighlight(changed_start_index: Int, max_count: Int, diff_matches: [NSTextCheckingResult]) -> Int {
        let before_matches = diff_matches.filter({ $0.range.location < changed_start_index })
        guard before_matches.count > 0, let last_match = before_matches.last else { return 0 }
        return last_match.range.location
    }
    
    func getEndIndexShouldHighlight(changed_end_index: Int, max_count: Int, diff_matches: [NSTextCheckingResult]) -> Int {
        let after_matches = diff_matches.filter({ $0.range.location > changed_end_index - 1 })
        guard after_matches.count > 0, let first_match = after_matches.first else { return max_count }
        return first_match.range.location
    }
    
    func getCurrentCursorPosition() -> NSRange? {
        guard let selectedRange = textView.selectedTextRange else { return nil }
        let cursorPosition = textView.offset(from: textView.beginningOfDocument, to: selectedRange.start)
        return NSRange(location: cursorPosition, length: 0)
    }
    
    func highlightMentions(in range: NSRange) {
        for repository in repositories.items {
            highlightMention(repository, in: range)
        }
    }
    
    func highlightMention(_ repository: MemberEntity, in range: NSRange) {
        guard let text = textView.text else { return }
        let mentionExpression = try? NSRegularExpression(pattern: "@\(repository.nickname)", options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches.filter({ $0.range.location >= range.location && $0.range.location + $0.range.length <= range.location + range.length }) {
            let match_range = match.range
            let str = text.substring(with: match_range)
            let newAttr = self.getMentionAttribute(text: str)
            let newAttributedText = textView.attributedText.replacingCharacters(in: NSRange(location: match_range.location, length: match_range.length), with: newAttr)
            textView.attributedText = NSAttributedString()
            textView.attributedText = newAttributedText
        }
    }
    
    func getMentionAttribute(text: String) -> NSAttributedString {
        let newAttr = NSAttributedString(string: text, attributes: [
            NSAttributedString.Key.font: textView.font as Any,
            NSAttributedString.Key.foregroundColor: Theme.themify(key: .mainLight) as Any,
            NSAttributedString.Key.backgroundColor: Theme.themify(key: .main).darker(by: 20)?.withAlphaComponent(0.15) as Any,
        ])
        return newAttr
    }
    
    static let everyone_mention_pattern = "@everyone"
    func highlightEveryoneMention(in range: NSRange) {
        guard let text = textView.text else { return }
        let mentionExpression = try? NSRegularExpression(pattern: MessageInputTextViewManager.everyone_mention_pattern, options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches.filter({ $0.range.location >= range.location && $0.range.location + $0.range.length <= range.location + range.length }) {
            let match_range = match.range
            let str = text.substring(with: match_range)
            let newAttr = self.getEveryoneMentionAttribute(text: str)
            let newAttributedText = textView.attributedText.replacingCharacters(in: NSRange(location: match_range.location, length: match_range.length), with: newAttr)
            textView.attributedText = NSAttributedString()
            textView.attributedText = newAttributedText
        }
    }
    
    func getEveryoneMentionAttribute(text: String) -> NSAttributedString {
        let newAttr = NSAttributedString(string: text, attributes: [
            NSAttributedString.Key.font: textView.font as Any,
            NSAttributedString.Key.foregroundColor: Theme.themify(key: .warning).lighter(by: 5) as Any,
            NSAttributedString.Key.backgroundColor: Theme.themify(key: .warning).darker(by: 20)?.withAlphaComponent(0.15) as Any,
        ])
        return newAttr
    }
    
    static let here_mention_pattern = "@here"
    func highlightHereMention(in range: NSRange) {
        guard let text = textView.text else { return }
        let mentionExpression = try? NSRegularExpression(pattern: MessageInputTextViewManager.here_mention_pattern, options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches.filter({ $0.range.location >= range.location && $0.range.location + $0.range.length <= range.location + range.length }) {
            let match_range = match.range
            let str = text.substring(with: match_range)
            let newAttr = self.getHereAttribute(text: str)
            let newAttributedText = textView.attributedText.replacingCharacters(in: NSRange(location: match_range.location, length: match_range.length), with: newAttr)
            textView.attributedText = NSAttributedString()
            textView.attributedText = newAttributedText
        }
    }
    
    func getHereAttribute(text: String) -> NSAttributedString {
        let newAttr = NSAttributedString(string: text, attributes: [
            NSAttributedString.Key.font: textView.font as Any,
            NSAttributedString.Key.foregroundColor: Theme.themify(key: .warning).lighter(by: 5) as Any,
            NSAttributedString.Key.backgroundColor: Theme.themify(key: .warning).darker(by: 20)?.withAlphaComponent(0.15) as Any,
        ])
        return newAttr
    }
    
    static let duration_pattern = "([0-9]{1,2}:){1,2}[0-9][0-9]"
    func highlightDurations(in range: NSRange) {
        guard let text = textView.text else { return }
        let mentionExpression = try? NSRegularExpression(pattern: MessageInputTextViewManager.duration_pattern, options: [])
        let matches = mentionExpression!.matches(in: text, options: [], range: NSMakeRange(0, text.count))
        for match in matches.filter({ $0.range.location >= range.location && $0.range.location + $0.range.length <= range.location + range.length }) {
            let match_range = match.range
            let str = text.substring(with: match_range)
            let newAttr = self.getDurationAttribute(text: str)
            let newAttributedText = textView.attributedText.replacingCharacters(in: match_range, with: newAttr)
            textView.attributedText = NSAttributedString()
            textView.attributedText = newAttributedText
        }
    }
    
    func insertDurationText(newText: String) {
        let highlightedRange = NSRange(location: textView.text.count, length: 0)
        let newAttr = self.getDurationAttribute(text: newText)
        let newAttributedText = textView.attributedText.replacingCharacters(in: highlightedRange, with: newAttr)
        textView.attributedText = NSAttributedString()
        textView.attributedText = newAttributedText
    }
    
    func getDurationAttribute(text: String) -> NSAttributedString {
        let newAttr = NSAttributedString(string: text, attributes: [
            NSAttributedString.Key.font: textView.font as Any,
            NSAttributedString.Key.foregroundColor: Theme.themify(key: .mainLight) as Any,
        ])
        return newAttr
    }
    
    func highlightURLs(in range: NSRange) {
        guard let text = textView.text else { return }
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: text, range: range)
        for match in matches {
            let match_range = match.range
            let str = text.substring(with: match_range)
            let newAttr = self.getURLSAttribute(text: str)
            let newAttributedText = textView.attributedText.replacingCharacters(in: match_range, with: newAttr)
            textView.attributedText = NSAttributedString()
            textView.attributedText = newAttributedText
        }
    }
    
    func insertURLText(newText: String) {
        let highlightedRange = NSRange(location: textView.text.count, length: 0)
        let newAttr = self.getURLSAttribute(text: newText)
        let newAttributedText = textView.attributedText.replacingCharacters(in: highlightedRange, with: newAttr)
        textView.attributedText = NSAttributedString()
        textView.attributedText = newAttributedText
    }
    
    func getURLSAttribute(text: String) -> NSAttributedString {
        let newAttr = NSAttributedString(string: text, attributes: [
            NSAttributedString.Key.font: textView.font as Any,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.foregroundColor: Theme.themify(key: .link),
            NSAttributedString.Key.backgroundColor: Theme.themify(key: .transparent),
        ])
        return newAttr
    }
}
