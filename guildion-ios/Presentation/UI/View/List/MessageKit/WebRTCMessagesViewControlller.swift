//
//  WebRTCMessagesViewControlller.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/06.
//

import Foundation
import UIKit
import WebRTC
import Starscream
import SwiftyJSON
import AVFoundation

class WebRTCMessagesViewControlller: ChannelMessagesViewController {
    public var peerConnections: PeerConnectionModels = PeerConnectionModels()
    public var peerConnectionFactory: RTCPeerConnectionFactory! = nil
    public var audioSource: RTCAudioSource?
    public var localAudioTrack: RTCAudioTrack?
    
    public var muted: Bool = true
    public var isRTCStarted: Bool = false
    internal var isRTCConfigured: Bool = false
    public var voiceConnectable: Bool {
        return (self.channel as? StreamChannelEntity)?.channelable_type != .MusicChannel && (self.channel as? StreamChannelEntity)?.channelable_type != .PlayChannel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.configureWebRTC()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    func mute() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        self.didChange(meterLevel: 0)
        self.sendMeterLevelChangeRequest(meterLevel: 0)
        self.muted = true
        self.localAudioTrack?.isEnabled = false
    }
    
    func unmute() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        self.muted = false
        self.localAudioTrack?.isEnabled = true
    }
    
    func configureWebRTC() {
        guard !isRTCConfigured else { return }
        AudioEngineHandler.shared.inject(self)
        RTCAudioSession.sharedInstance().add(self)
        if voiceConnectable {
            self.initializeInput()
            AudioEngineHandler.shared.record()
        } else {
            self.initializePlayerInput()
        }
        peerConnectionFactory = RTCPeerConnectionFactory()
        self.configureWebRTCSource(peerConnectionFactory: peerConnectionFactory!)
        self.isRTCConfigured = true
    }
    
    func configureWebRTCSource(peerConnectionFactory: RTCPeerConnectionFactory) {
        guard voiceConnectable else { return }
        let audioSourceConstraints = RTCMediaConstraints(
            mandatoryConstraints: nil, optionalConstraints: nil)
        audioSource = peerConnectionFactory.audioSource(with: audioSourceConstraints)
    }
    
    func configureWebRTCAudioSession() {
        guard !RTCAudioSession.sharedInstance().categoryOptions.contains(.mixWithOthers) || RTCAudioSession.sharedInstance().category != AVAudioSession.Category.playAndRecord.rawValue else { return }
        guard voiceConnectable else { return }
        do {
            RTCAudioSession.sharedInstance().lockForConfiguration()
            try RTCAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord.rawValue, with: [.defaultToSpeaker, .mixWithOthers, .allowBluetooth])
            try RTCAudioSession.sharedInstance().setActive(true)
            RTCAudioSession.sharedInstance().unlockForConfiguration()
            self.didChangeAudioSession()
        } catch {}
    }
    
    func configurePlayAudioSession() {
        do {
            RTCAudioSession.sharedInstance().lockForConfiguration()
            try RTCAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback.rawValue, with: [.defaultToSpeaker, .mixWithOthers])
            try RTCAudioSession.sharedInstance().setActive(true)
            RTCAudioSession.sharedInstance().unlockForConfiguration()
        } catch {}
        self.didChangeAudioSession()
    }
    
    deinit {
        if peerConnections.items.count > 0 {
            closeWebRTC()
        }
        audioSource = nil
        peerConnectionFactory = nil
    }
    
    func closeWebRTC() {
        if peerConnections.items.count > 0 {
            let json = SDMessageDispatcher.close(id: WebSocketHandler.shared.id).json
            if let roomname = StreamChannelConnector.shared.roomname {
                WebSocketHandler.shared.sendMessage(json: json, roomname: roomname).then({})
            }
            peerConnections.stopAllConnections()
            Log.LOG("sending close message")
        }
        AudioEngineHandler.shared.stopRecording()
        localAudioTrack = nil
        peerConnections = PeerConnectionModels()
        self.isRTCStarted = false
        self.isRTCConfigured = false
        RTCAudioSession.sharedInstance().remove(self)
        WebSocketHandler.shared.reject(self)
        AudioEngineHandler.shared.reject(self)
    }
    
    override func didRecieve(_ event: WebRTCEvent) {
        super.didRecieve(event)
        switch event {
        case .talkRequest(let id, let user): makeOffer(id: id, user: user)
        case .offer(let id, let receiver_id, let offer, let user):
            guard WebSocketHandler.shared.id == receiver_id else { break }
            self.setOffer(offer, id: id, user: user)
        case .answer(let answer, let id, let receiver_id, _):
            guard WebSocketHandler.shared.id == receiver_id else { break }
            self.setAnswer(answer, id: id)
        case .candidate(let candidate, let id, let receiver_id, _):
             guard WebSocketHandler.shared.id == receiver_id else { break }
            self.addIceCandidate(candidate, id: id)
        case .meterLevelChange(let id, let meter_level, _): self.meterLevelChange(id: id, meterLevel: meter_level)
        case .close(let id, _): self.closePeerConnection(id: id)
        default: break
        }
    }
    
    // MARK: override methods
    func didChangeAudioSession() {}
    func didTalkRequest() {}
    func didRemovePeerConnection(_ peerConnectionModel: PeerConnectionModel) {}
    func didAddPeerConnection(_ peerConnectionModel: PeerConnectionModel) {}
    func didChange(meterLevel: Float) {}
    func didChange(_ peerConnectionModel: PeerConnectionModel, meterLevel: Float) {}
}

extension WebRTCMessagesViewControlller: RTCPeerConnectionDelegate {
    func prepareNewConnection(id: String, user: UserEntity) -> RTCPeerConnection {
        let configuration = RTCConfiguration()
        configuration.iceServers = [
            RTCIceServer.init(
                urlStrings: ["stun:stun.l.google.com:19302"])]
        
        let peerConnectionConstraints = RTCMediaConstraints(
            mandatoryConstraints: nil, optionalConstraints: nil)
        
        let peerConnection = peerConnectionFactory!.peerConnection(
            with: configuration, constraints: peerConnectionConstraints, delegate: self)
        
        if voiceConnectable {
            self.localAudioTrack = peerConnectionFactory.audioTrack(with: audioSource!, trackId: "ARDAMSa0")
            let audioSender = peerConnection.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: "ARDAMS")
            audioSender.track = localAudioTrack
            
            self.localAudioTrack?.isEnabled = !self.muted
        }
        
        let model = PeerConnectionModel(id: id, peerConnection: peerConnection, user: user)
        
        self.peerConnections.addConnection(peerConnectionModel: model)
        self.didAddPeerConnection(model)
        
        return peerConnection
    }
    
    func talkRequest() {
        Log.LOG("------ sending talkRequest message ------")
        let json = SDMessageDispatcher.talkRequest(id: WebSocketHandler.shared.id).json
        if let roomname = StreamChannelConnector.shared.roomname {
            WebSocketHandler.shared.sendMessage(json: json, roomname: roomname).then(in: .main, {
                self.didTalkRequest()
            })
        }
    }
    
    func meterLevelChange(id: String, meterLevel: Float) {
        guard let model = peerConnections.getConnection(id: id) else { return }
        self.didChange(model, meterLevel: meterLevel)
    }
    
    func sendMeterLevelChangeRequest(meterLevel: Float) {
        let json = SDMessageDispatcher.meterLevelChange(id: WebSocketHandler.shared.id, meterLevel: meterLevel).json
        if let roomname = StreamChannelConnector.shared.roomname {
            WebSocketHandler.shared._sendMessage(json: json, roomname: roomname)
        }
    }
    
    func closePeerConnection(id: String) {
        DispatchQueue.main.async {
            guard let model = self.peerConnections.getConnection(id: id) else { return }
            self.peerConnections.stopConnection(id: id)
            self.didRemovePeerConnection(model)
        }
    }
    
    func sendSDP(_ desc: RTCSessionDescription, id: String) {
        let json = SDMessageDispatcher.sdp(id: WebSocketHandler.shared.id, receiver_id: id, desc: desc).json
        Log.LOG("------ sending SDP ----->")
        if let roomname = StreamChannelConnector.shared.roomname {
            WebSocketHandler.shared.sendMessage(json: json, roomname: roomname).then({})
        }
    }
    
    func makeOffer(id: String, user: UserEntity) {
        guard self.peerConnections.getConnection(id: id) == nil else { return }
        
        let peerConnection = self.prepareNewConnection(id: id, user: user)
        
        let constraints = RTCMediaConstraints(
            mandatoryConstraints: [
                "OfferToReceiveAudio": "true",
                "OfferToReceiveVideo": "false"
            ],
            optionalConstraints: nil
        )
        let offerCompletion = { (offer: RTCSessionDescription?, error: Error?) in
            if error != nil { return }
            Log.LOG("createOffer() succsess")
            let setLocalDescCompletion = {(error: Error?) in
                if error != nil { return }
                Log.LOG("setLocalDescription() succsess")
                self.sendSDP(offer!, id: id)
            }
            peerConnection.setLocalDescription(offer!, completionHandler: setLocalDescCompletion)
        }
        peerConnection.offer(for: constraints, completionHandler: offerCompletion)
    }
    
    func makeAnswer(id: String) {
        Log.LOG("sending Answer. Creating remote session description...")
        guard let model = peerConnections.getConnection(id: id), let peerConnection = model.peerConnection else {
            Log.LOG("peerConnection NOT exist!")
            return
        }
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let answerCompletion = { (answer: RTCSessionDescription?, error: Error?) in
            if error != nil { return }
            Log.LOG("createAnswer() succsess")
            let setLocalDescCompletion = {(error: Error?) in
                if error != nil { return }
                Log.LOG("setLocalDescription() succsess")
                self.sendSDP(answer!, id: id)
            }
            peerConnection.setLocalDescription(answer!, completionHandler: setLocalDescCompletion)
        }
        peerConnection.answer(for: constraints, completionHandler: answerCompletion)
    }
    
    func setOffer(_ offer: RTCSessionDescription, id: String, user: UserEntity) {
        guard peerConnections.getConnection(id: id) == nil else { return }
        let peerConnection = self.prepareNewConnection(id: id, user: user)
        peerConnection.setRemoteDescription(offer, completionHandler: {(error: Error?) in
            if error == nil {
                Log.LOG("setRemoteDescription(offer) succsess")
                self.makeAnswer(id: id)
            } else {
                Log.LOG("setRemoteDescription(offer) ERROR: " + error.debugDescription)
            }
        })
    }
    
    func setAnswer(_ answer: RTCSessionDescription, id: String) {
        guard let model = peerConnections.getConnection(id: id), let peerConnection = model.peerConnection else {
            Log.LOG("peerConnection NOT exist!")
            return
        }
        peerConnection.setRemoteDescription(answer, completionHandler: {
            (error: Error?) in
            if error == nil {
                Log.LOG("setRemoteDescription(answer) succsess")
            } else {
                Log.LOG("setRemoteDescription(answer) ERROR: " + error.debugDescription)
            }
        })
    }
    
    func addIceCandidate(_ candidate: RTCIceCandidate, id: String) {
        guard let model = peerConnections.getConnection(id: id), let peerConnection = model.peerConnection else {
            Log.LOG("peerConnection NOT exist!")
            return
        }
        peerConnection.add(candidate)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        Log.LOG("-- peer.onaddstream()")
        DispatchQueue.main.async(execute: { () -> Void in
            if let model = self.peerConnections.getConnectionFromPeerConnection(peerConnection: peerConnection), self.voiceConnectable {
                model.audioTrack = stream.audioTracks.first
                self.configureWebRTCAudioSession()
            }
        })
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
    }
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        guard let model = peerConnections.getConnectionFromPeerConnection(peerConnection: peerConnection) else { return }
        var state = ""
        switch (newState) {
        case RTCIceConnectionState.checking:
            state = "checking"
        case RTCIceConnectionState.completed:
            state = "completed"
        case RTCIceConnectionState.connected:
            state = "connected"
        case RTCIceConnectionState.closed:
            state = "closed"
            self.closePeerConnection(id: model.id)
        case RTCIceConnectionState.failed:
            state = "failed"
            self.closePeerConnection(id: model.id)
        case RTCIceConnectionState.disconnected:
            state = "disconnected"
            self.closePeerConnection(id: model.id)
        default:
            break
        }
        Log.LOG("ICE connection Status has changed to \(state)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        if candidate.sdpMid != nil, let model = peerConnections.getConnectionFromPeerConnection(peerConnection: peerConnection) {
            sendIceCandidate(peerConnection, candidate, id: model.id)
        } else {
            Log.LOG("empty ice event")
        }
    }
    
    func sendIceCandidate(_ peerConnection: RTCPeerConnection, _ candidate: RTCIceCandidate, id: String) {
        Log.LOG("---sending ICE candidate ---")
        guard peerConnections.getConnection(id: id) != nil else {
            Log.LOG("PeerConnection is not exist")
            return
        }
        let json = SDMessageDispatcher.sendIceCandidate(id: WebSocketHandler.shared.id, receiver_id: id, candidate: candidate).json
        if let roomname = StreamChannelConnector.shared.roomname {
            WebSocketHandler.shared.sendMessage(json: json, roomname: roomname).then(in: .main, {})
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
    }
}

extension WebRTCMessagesViewControlller: RTCAudioSessionDelegate {
    func audioSessionDidBeginInterruption(_ session: RTCAudioSession) {
        
    }
    
    func audioSessionDidEndInterruption(_ session: RTCAudioSession, shouldResumeSession: Bool) {
    }
    
    func audioSession(_ session: RTCAudioSession, didChangeCanPlayOrRecord canPlayOrRecord: Bool) {
    }
}

extension WebRTCMessagesViewControlller: RTCAudioSessionActivationDelegate {
    func audioSessionDidActivate(_ session: AVAudioSession) {
    }
    
    func audioSessionDidDeactivate(_ session: AVAudioSession) {
    }
}

extension WebRTCMessagesViewControlller: AudioActionSheetProtocol {}

extension WebRTCMessagesViewControlller: AudioEngineHandlerDelegate {
    func audioEngineHandler(didInput meterLevel: Float, in audioEngineHandler: AudioEngineHandler) {
        guard !self.muted else { return }
        self.sendMeterLevelChangeRequest(meterLevel: meterLevel)
        self.didChange(meterLevel: meterLevel)
    }
}
