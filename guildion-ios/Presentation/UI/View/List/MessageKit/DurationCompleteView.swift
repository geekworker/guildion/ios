//
//  DurationCompleteView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/29.
//

import Foundation
import UIKit
import MessageKit
import InputBarAccessoryView
import TTTAttributedLabel

protocol DurationCompleteViewDelegate: class {
    func durationCompleteView(onTap duration: Float, in durationCompleteView: DurationCompleteView)
}

class DurationCompleteView: UIView {
    @IBOutlet weak var separatorView: UIView! {
        didSet {
            separatorView.backgroundColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            overView.layer.cornerRadius = 5
            overView.clipsToBounds = true
            overView.isHidden = true
            overView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var durationLabel: TTTAttributedLabel! {
        didSet {
            durationLabel.text = ""
            durationLabel.textColor = Theme.themify(key: .stringSecondary)
            durationLabel.font = Font.FT(size: .FM, family: .L)
            durationLabel.isUserInteractionEnabled = true
            messageLabelManager.label = durationLabel
        }
    }
    public var repository: Float = 0
    public var formattedDuration: String {
        let duration = TimeInterval(self.repository)
        return duration.hour > 0 ? String(format:"%0d:%02d:%02d", duration.hour, duration.minute, duration.second) : String(format:"%0d:%02d", duration.minute, duration.second)
    }
    lazy var messageLabelManager: MessageLabelManager = {
        return MessageLabelManager()
    }()
    weak var delegate: DurationCompleteViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.durationCompleteView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = .none
        let longpress = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.hover(_:))
        )
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
        longpress.minimumPressDuration = 0.01
        longpress.cancelsTouchesInView = false
        longpress.delegate = self
        view.addGestureRecognizer(longpress)
    }
    
    
    public func setRepository(_ repository: Float) {
        self.repository = repository
        self.durationLabel.text = R.string.localizable.channelCurrentDuration(formattedDuration)
        messageLabelManager.highlightDurationsInLabel()
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.durationCompleteView(onTap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension DurationCompleteView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
