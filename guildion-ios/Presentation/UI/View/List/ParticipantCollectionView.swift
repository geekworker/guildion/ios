//
//  ParticipantCollectionView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/20.
//

import Foundation
import UIKit

class ParticipantCollectionView: UICollectionView {
    var repositories: ParticipantEntities = ParticipantEntities()
    var rowCount: Int = 1
    
    func calcHeight() -> CGFloat {
        let row_count = Int(self.frame.width / (MenuCollectionViewCell.cellSize.width + 4))
        if row_count == 0 {
            return MenuCollectionViewCell.cellSize.height + 16
        }
        return CGFloat(repositories.count % row_count == 0 ? repositories.count / row_count : repositories.count / row_count + 1) * MenuCollectionViewCell.cellSize.height + 16
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_guild = GuildConnector.shared.current_guild ?? GuildEntity()
            vc.current_member = GuildConnector.shared.current_member ?? MemberEntity()
            vc.permission = GuildConnector.shared.current_permission
            UIViewController.topMostController().presentPanModal(vc)
        }
    }
    
    func configure(row: Int, size: CGSize) {
        guard row > 0 else { return }
        self.rowCount = row
        let flowLayout = UICollectionViewFlowLayout()
        self.collectionViewLayout = flowLayout
        flowLayout.minimumLineSpacing = 4
        flowLayout.minimumInteritemSpacing = 4
        flowLayout.sectionInset = .zero
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = ParticipantCollectionViewCell.cellSize(height: size.height / CGFloat(row) - 8)
        self.register([
            UINib(resource: R.nib.participantCollectionViewCell): R.nib.participantCollectionViewCell.name,
        ])
        self.backgroundColor = Theme.themify(key: .transparent)
        self.backgroundView?.backgroundColor = Theme.themify(key: .transparent)
    }
    
    func setRepositories(_ repositories: ParticipantEntities) {
        self.repositories = repositories
        self.reloadData()
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func collectionView(numberOfItemsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func collectionView(cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: R.nib.participantCollectionViewCell.name, for: indexPath) as? ParticipantCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
            return UICollectionViewCell()
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
}

extension ParticipantCollectionView: ParticipantCollectionViewCellDelegate {
    func participantCollectionViewCell(onTap repository: ParticipantEntity, in participantCollectionViewCell: ParticipantCollectionViewCell) {
        guard let member = repository.Member else { return }
        self.toMemberMenu(repository: member)
    }
}
