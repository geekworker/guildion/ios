//
//  WebKitViewController.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/21.
//

import Foundation
import UIKit
import WebKit
import URLEmbeddedView
import Hydra
import PKHUD
import YoutubePlayer_in_WKWebView
import RxSwift
import RxCocoa
import PanModal

protocol WebKitViewControllerDelegate: class {
    var current_member: MemberEntity { get set }
    func webKitViewController(didSelects repositories: FileEntities, in webKitViewController: WebKitViewController)
    func webKitViewController(didSelect url: URL, in webKitViewController: WebKitViewController)
    func webKitViewController(shouldSkip duration: Float, in webKitViewController: WebKitViewController)
}

class WebKitViewController: UIViewController, PanContainerNavigationViewChildDelegate {
    @IBOutlet weak var webKitContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaPickCollectionViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            mediaPickCollectionViewHeightConstraint.constant = MediaMode.off.height
        }
    }
    @IBOutlet weak var mediaPickCollectionViewContainer: UIView! {
        didSet {
            mediaPickCollectionViewContainer.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var webViewContainer: UIView!
    public lazy var mediaPickCollectionViewController: MediaPickCollectionViewController = {
        let vc = MediaPickCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
        vc.view.backgroundColor = Theme.themify(key: .backgroundLight)
        return vc
    }()
    public var panContainerViewNavigationController: PanContainerViewNavigationController? {
        return self.navigationController as? PanContainerViewNavigationController
    }
    public var panModalableNavigationController: PanModalableNavigationController? {
        return self.navigationController as? PanModalableNavigationController
    }
    weak var parentContainerView: PanContainerNavigationView!
    public lazy var webView: WKWebView = {
        let configuration = WKWebViewConfiguration()
        configuration.mediaTypesRequiringUserActionForPlayback = []
        configuration.allowsInlineMediaPlayback = true
        let webView = WKWebView(frame: .zero, configuration: configuration)
        return webView
    }()
    fileprivate var panMoveX: CGFloat = 0
    fileprivate var disposeBag = DisposeBag()
    fileprivate lazy var progressView: UIProgressView = {
        let progressView = UIProgressView(progressViewStyle: .default)
        progressView.trackTintColor = .clear
        progressView.tintColor = Theme.themify(key: .main)
        progressView.alpha = 0
        return progressView
    }()
    fileprivate lazy var backButton: UIBarButtonItem = {
        let backIcon = R.image.backIcon()!.withRenderingMode(.alwaysTemplate)
        let backButton = UIBarButtonItem(image: backIcon, style: .plain, target: self, action: #selector(WebKitViewController.backButtonTapped(_:)))
        return backButton
    }()
    fileprivate lazy var forwardButton: UIBarButtonItem = {
        let forwardIcon = R.image.forwardIcon()!.withRenderingMode(.alwaysTemplate)
        let forwardButton = UIBarButtonItem(image: forwardIcon, style: .plain, target: self, action: #selector(WebKitViewController.forwardButtonTapped(_:)))
        return forwardButton
    }()
    fileprivate lazy var googleButton: UIBarButtonItem = {
        let icon = R.image.gLogo()!.withRenderingMode(.alwaysOriginal).resizeSizeFit(height: 22).withRenderingMode(.alwaysOriginal)
        let button = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(WebKitViewController.googleButtonTapped(_:)))
        return button
    }()
    fileprivate lazy var youtubeButton: UIBarButtonItem = {
        let icon = R.image.yt_icon_rgb()!.withRenderingMode(.alwaysOriginal).resizeSizeFit(height: 22).withRenderingMode(.alwaysOriginal)
        let button = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(WebKitViewController.youtubeButtonTapped(_:)))
        return button
    }()
    fileprivate lazy var sendButton: UIBarButtonItem = {
        let icon = R.image.ic_send()!.resizeSizeFit(height: 50)
        let button = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(WebKitViewController.sendButtonTapped(_:)))
        return button
    }()
    fileprivate lazy var reloadButton: UIBarButtonItem = {
        let icon = R.image.reload()!.resizeSizeFit(height: 22)
        let button = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(WebKitViewController.reloadButtonTapped(_:)))
        return button
    }()
    fileprivate lazy var playButton: UIBarButtonItem = {
        let icon = R.image.playing()!.resizeSizeFit(height: 22)
        let button = UIBarButtonItem(image: icon, style: .plain, target: self, action: #selector(WebKitViewController.playButtonTapped(_:)))
        return button
    }()
    fileprivate lazy var itemFixedSeparator: UIBarButtonItem = {
        let itemFixedSeparator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        itemFixedSeparator.width = self.toolbarItemSpace
        return itemFixedSeparator
    }()
    fileprivate lazy var itemFlexibleSeparator: UIBarButtonItem = {
        let itemFixedSeparator = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        return itemFixedSeparator
    }()
    var toolbarItemSpace = WebKitViewController.defaultToolbarItemSpace {
        didSet {
            itemFixedSeparator.width = toolbarItemSpace
        }
    }
    var default_url: URL = SearchDomainType.google.url
    var current_url: URL = SearchDomainType.google.url {
        didSet {
            self.panContainerViewNavigationController?.current_url = self.current_url
            self.panModalableNavigationController?.current_url = self.current_url
            self.domainType = SearchDomainType(url: self.current_url)
        }
    }
    var repositories: FileEntities = FileEntities()
    weak var delegate: WebKitViewControllerDelegate?
    enum MediaMode: Int {
        case off = 0
        case on = 120
        var height: CGFloat {
            return CGFloat(self.rawValue)
        }
    }
    static let callbackScheme = "ytguildion"
    static let defaultToolbarItemSpace: CGFloat = 50
    static let URLKeyPath = "URL"
    static let estimatedProgressKeyPath = "estimatedProgress"
    static var estimatedProgressContext = 0
    public var defaultMedia: Bool = false
    public var mediaMode: MediaMode = .off
    public var domainType: SearchDomainType = .google {
        didSet {
            self.panContainerViewNavigationController?.domainType = self.domainType
            self.panModalableNavigationController?.domainType = self.domainType
        }
    }
    private var configured: Bool = false
    private var reloadedID: String = ""
    fileprivate var players: [String: WKYTPlayerView] = [:]
    fileprivate var pl_resolve: ((_ value: WKYTPlayerView) -> ())?
    fileprivate var pl_reject: ((_ error: Error) -> ())?
    fileprivate var shouldBrowserHistoryPush: Bool = false
    public var isContainer: Bool {
        return self.parentContainerView != nil
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureWebView()
        configureProgressView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureMediaPickCollectionViewController()
        self.loadURL(self.default_url)
        self.setupNavigationBar()
        self.setupToolBar()
        self.updateToolBarState()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        progressView.removeFromSuperview()
    }
    
    func dismissShouldAppear(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.dismiss(animated: flag, completion: {
            UIViewController.topMostController().viewWillAppear(false)
            UIViewController.topMostController().viewDidAppear(false)
            completion?()
        })
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.addSubview(progressView)
        navigationController?.navigationBar.barTintColor = Theme.themify(key: .background)
        navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
    }
    
    func setupToolBar() {
        navigationController?.toolbar.autoresizingMask = [.flexibleHeight]
        navigationController?.setToolbarHidden(false, animated: false)
        navigationController?.toolbar.barTintColor = Theme.themify(key: .backgroundLight)
        navigationController?.toolbar.tintColor = Theme.themify(key: .main)
        updateToolBarState()
    }
    
    fileprivate func updateToolBarState() {
        backButton.isEnabled = webView.canGoBack
        forwardButton.isEnabled = webView.canGoForward
        playButton.isEnabled = self.repositories.count > 0

        var barButtonItems = [UIBarButtonItem]()
        if repositories.count > 0 || defaultMedia {
            barButtonItems = [backButton, itemFixedSeparator, youtubeButton, itemFlexibleSeparator, playButton, itemFlexibleSeparator, googleButton, itemFixedSeparator, forwardButton]
        } else {
            barButtonItems = [backButton, itemFixedSeparator, youtubeButton, itemFlexibleSeparator, sendButton, itemFlexibleSeparator, googleButton, itemFixedSeparator, forwardButton]
        }
        
        if delegate == nil {
            barButtonItems = [backButton, itemFixedSeparator, youtubeButton, itemFlexibleSeparator, reloadButton, itemFlexibleSeparator, googleButton, itemFixedSeparator, forwardButton]
        }
        
        setToolbarItems(barButtonItems, animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url), let url = self.webView.url {
            self.current_url = url
            self.webView(self.webView, currentURL: self.current_url)
        }

        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            progressView.alpha = 1
            let animated = webView.estimatedProgress > Double(progressView.progress)
            progressView.setProgress(Float(webView.estimatedProgress), animated: animated)

            if webView.estimatedProgress >= 1 {
                if shouldBrowserHistoryPush, self.webView.url != nil {
                    self.shouldBrowserHistoryPush = false
                    self.webView.reload()
                }
                UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: {
                    self.progressView.alpha = 0
                    }, completion: { (finished) in
                        self.progressView.progress = 0
                })
            }
        }
    }
    
    func updateLayout() {
        self.mediaMode = repositories.count > 0 ? .on : .off
        self.mediaPickCollectionViewHeightConstraint.constant = self.mediaMode.height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func setRepositories(_ repositories: FileEntities) {
        self.repositories = repositories
        self.mediaPickCollectionViewController.setRepositories(repositories)
        self.mediaMode = repositories.count > 0 ? .on : .off
        guard isViewLoaded else { return }
        updateLayout()
        updateToolBarState()
    }
    
    func setURL(_ url: URL) {
        self.default_url = url
        self.setRepositories(FileEntities())
    }
    
    func loadRequest(_ request: URLRequest) {
        webView.load(request)
    }

    func loadURL(_ url: URL) {
        self.current_url = url
        webView.load(URLRequest(url: url))
    }

    func loadURLString(_ urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        self.current_url = url
        webView.load(URLRequest(url: url))
    }

    func loadHTMLString(_ htmlString: String, baseURL: URL?) {
        webView.loadHTMLString(htmlString, baseURL: baseURL)
    }
}

extension WebKitViewController {
    func goBack() {
        guard webView.canGoBack else { return }
        self.webView.goBack()
        updateToolBarState()
        shouldBrowserHistoryPush = true
    }
    
    func goForward() {
        guard webView.canGoForward else { return }
        self.webView.goForward()
        updateToolBarState()
        shouldBrowserHistoryPush = true
    }
    
    @objc func refreshButtonTapped(_ sender: UIBarButtonItem) {
        webView.stopLoading()
        webView.reload()
    }

    @objc func stopButtonTapped(_ sender: UIBarButtonItem) {
        webView.stopLoading()
    }

    @objc func backButtonTapped(_ sender: UIBarButtonItem) {
        self.goBack()
    }

    @objc func forwardButtonTapped(_ sender: UIBarButtonItem) {
        self.goForward()
        updateToolBarState()
    }
    
    @objc func googleButtonTapped(_ sender: UIBarButtonItem) {
        self.loadURLString(SearchDomainType.google.urlString)
        updateToolBarState()
    }
    
    @objc func youtubeButtonTapped(_ sender: UIBarButtonItem) {
        self.loadURLString(SearchDomainType.youtube.urlString)
        updateToolBarState()
    }
    
    @objc func sendButtonTapped(_ sender: UIBarButtonItem) {
        updateToolBarState()
        self.delegate?.webKitViewController(didSelect: self.current_url, in: self)
        isContainer ? self.dismissContainer(animated: true, completion: nil) : self.dismissShouldAppear(animated: true)
    }
    
    @objc func reloadButtonTapped(_ sender: UIBarButtonItem) {
        self.webView.reload()
        updateToolBarState()
    }
    
    @objc func playButtonTapped(_ sender: UIBarButtonItem) {
        updateToolBarState()
        self.delegate?.webKitViewController(didSelects: self.repositories, in: self)
        isContainer ? self.dismissContainer(animated: true, completion: nil) : self.dismissShouldAppear(animated: true)
    }
}

extension WebKitViewController: WKWebViewInjectable {
    fileprivate func configureWebView() {
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.autoresizesSubviews = true
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.isMultipleTouchEnabled = true
        webView.scrollView.alwaysBounceVertical = true
        webViewContainer.addSubview(webView)
        webViewContainer.constrainToEdges(webView)
        webView.addObserver(self, forKeyPath: WebKitViewController.URLKeyPath, options: .new, context: nil)
        webView.addObserver(self, forKeyPath: WebKitViewController.estimatedProgressKeyPath, options: .new, context: nil)
        let swipeLeftRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(recognizer:)))
        let swipeRightRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(recognizer:)))
        swipeLeftRecognizer.direction = .left
        swipeRightRecognizer.direction = .right
        webView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.panWebView(_:))))
        guard self.delegate != nil else { return }
        self.injectScripts()
    }
    
    fileprivate func injectScripts() {
        guard let path = Bundle.main.path(forResource: R.file.ytguildionJs.name, ofType: R.file.ytguildionJs.pathExtension), let encoded = self.encodeUTF8(path: path) else { return }
        self.injectJS(encoded)
    }
    
    private func encodeUTF8(path: String) -> String? {
        do {
            return try String(contentsOfFile: path, encoding: .utf8)
        } catch {
            return nil
        }
    }
    
    @objc private func handleSwipe(recognizer: UISwipeGestureRecognizer) {
        if (recognizer.direction == .left) {
            self.goForward()
        }

        if (recognizer.direction == .right) {
            self.goBack()
        }
    }
    
    @objc func panWebView(_ sender: UIScreenEdgePanGestureRecognizer) {
        let move: CGPoint = sender.translation(in: view)
        panMoveX += move.x
        if sender.state == .ended {
            if abs(panMoveX) < -view.frame.size.width / 3 {
            } else {
                if panMoveX > 0 {
                    self.goBack()
                }
                if panMoveX < 0 {
                    self.goForward()
                }
            }
            panMoveX = 0
        }
        sender.setTranslation(.zero, in: view)
    }
    
    fileprivate func configureProgressView() {
        let yPosition: CGFloat = {
            guard let navigationBar = self.navigationController?.navigationBar else {
                return 0
            }
            return navigationBar.frame.height - self.progressView.frame.height
        }()
        progressView.frame = CGRect(x: 0, y: yPosition, width: view.frame.width, height: progressView.frame.width)
        progressView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
    }
}

extension WebKitViewController: WKNavigationDelegate {
    // MARK: - WKNavigationDelegate
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        updateToolBarState()
    }

    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        updateToolBarState()
    }

    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        updateToolBarState()
    }

    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        updateToolBarState()
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else { decisionHandler(.allow); return }
        if url.scheme == WebKitViewController.callbackScheme {
            self.webView(webView, callbackURL: url, decidePolicyFor: navigationAction, decisionHandler: decisionHandler)
            return
        }
        if externalAppRequiredToOpen(url) {
            self.webView(webView, externalURL: url, decidePolicyFor: navigationAction, decisionHandler: decisionHandler)
            return
        } else if navigationAction.targetFrame == nil {
            self.webView(webView, blankURL: url, decidePolicyFor: navigationAction, decisionHandler: decisionHandler)
            return
        }
        decisionHandler(.allow)
    }
    
    private func webView(_ webView: WKWebView, callbackURL url: URL, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let action = url.host else { return }
        switch action {
        case "shouldDurationSkip": self.webView(webView, shouldSkip: url.queryDictionary, decidePolicyFor: navigationAction, decisionHandler: decisionHandler)
        case "onYTPlayerClick": self.webView(webView, playerClick: url.queryDictionary, decidePolicyFor: navigationAction, decisionHandler: decisionHandler)
        default: break
        }
        decisionHandler(.cancel)
    }
    
    private func webView(_ webView: WKWebView, playerClick query: [String: String]?, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        self.appendYouTubeFile(url: self.current_url.absoluteString)
    }
    
    private func webView(_ webView: WKWebView, shouldSkip query: [String: String]?, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let unwrapped = query, let at = NumberFormatter().number(from: unwrapped["at"] ?? "0") else { return }
        self.delegate?.webKitViewController(shouldSkip: Float(truncating: at), in: self)
        self.webView.reload()
    }
    
    private func webView(_ webView: WKWebView, blankURL url: URL, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        loadURL(url)
        decisionHandler(.cancel)
    }
    
    private func webView(_ webView: WKWebView, externalURL url: URL, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard UIApplication.shared.canOpenURL(url) else { decisionHandler(.cancel); return }
        self.openExternalApp(with: url)
        decisionHandler(.cancel)
    }
    
    fileprivate func externalAppRequiredToOpen(_ url: URL) -> Bool {
        let validSchemes: Set<String> = ["http", "https"]
        if let urlScheme = url.scheme {
            return !validSchemes.contains(urlScheme)
        } else {
            return false
        }
    }
    
    fileprivate func openExternalApp(with url: URL) {
        let externalAppPermissionAlert = UIAlertController(
            title: R.string.localizable.openExternalAppAlertTitle(),
            message: R.string.localizable.openExternalAppAlertMessage(),
            preferredStyle: .alert
        )
        let cancelAction = UIAlertAction(
            title: R.string.localizable.cancel(),
            style: .cancel,
            handler: { _ in }
        )
        let openAction = UIAlertAction(
            title: R.string.localizable.open(),
            style: .default
        ) { _ in
            UIApplication.shared.open(url)
        }
        externalAppPermissionAlert.addAction(cancelAction)
        externalAppPermissionAlert.addAction(openAction)
        present(externalAppPermissionAlert, animated: true, completion: nil)
    }
}

extension WebKitViewController: WKUIDelegate {
    // MARK: - WKUIDelegate
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if let mainFrame = navigationAction.targetFrame?.isMainFrame, mainFrame == false {
            webView.load(navigationAction.request)
        }
        return nil
    }
    
    fileprivate func webView(_ webView: WKWebView, currentURL: URL) {
        self.updateToolBarState()
        let provider = ProviderEntity(url: currentURL.absoluteString)
        switch provider.type {
        case .youtube: self.webView(webView, youtubeURL: currentURL, provider: provider)
        case .other: self.webView(webView, defaultURL: currentURL, provider: provider)
        }
    }
    
    fileprivate func webView(_ webView: WKWebView, youtubeURL: URL, provider: ProviderEntity) {
        self.webKitContainerTopConstraint.constant = -48
        if let youtubeID = provider.youtubeID, reloadedID != youtubeID {
            reloadedID = youtubeID
            webView.reload()
        }
    }
    
    fileprivate func webView(_ webView: WKWebView, defaultURL: URL, provider: ProviderEntity) {
        self.webKitContainerTopConstraint.constant = 0
    }
}

extension WebKitViewController: PanContainerViewNavigationControllerSearchDelegate {
    func panContainerViewNavigationControllerShouldClose() {
        self.isContainer ? self.dismissContainer(animated: true, completion: nil) : self.dismissShouldAppear(animated: true)
    }
    
    func panContainerViewNavigationControllerShouldReload() {
        self.webView.reload()
    }
    
    func panContainerViewNavigationController(_ panContainerViewNavigationController: PanContainerViewNavigationController, searched keyword: String?) {
        guard let unwrappd = keyword, unwrappd.count > 0 else { return }
        if let url = URL(string: unwrappd), UIApplication.shared.canOpenURL(url) {
            self.loadURL(url)
        } else {
            self.loadURL(self.domainType.search(q: unwrappd))
        }
    }
}

extension WebKitViewController: PanModalableNavigationControllerSearchDelegate {
    func panModalableNavigationControllerShouldClose() {
        self.dismissShouldAppear(animated: true)
    }
    
    func panModalableNavigationControllerShouldReload() {
        self.webView.reload()
    }
    
    func panModalableNavigationController(_ panModalableNavigationController: PanModalableNavigationController, searched keyword: String?) {
        guard let unwrappd = keyword, unwrappd.count > 0 else { return }
        self.loadURL(self.domainType.search(q: unwrappd))
    }
}

extension WebKitViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return webView.scrollView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(UIScreen.main.bounds.height / 2)
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    var allowsExtendedPanScrolling: Bool { return true }
}

extension WebKitViewController: WKYTPlayerViewDelegate {
    func appendYouTubeFile(url: String) {
        let provider = ProviderEntity.init(url: url)
        guard repositories.filter({ $0.Provider?.youtubeID == provider.youtubeID }).count == 0 else { return }
        if provider.type == .youtube {
            HUD.show(.progress)
            self.loadYouTube(provider: provider, callback: { [unowned self] in
                self.fetchDuration(player: $0, provider: provider, callback: { time in
                    provider.duration = time * 1000
                    self.fetchYouTubeData(provider: provider)
                }, onError: { _ in
                    self.fetchYouTubeData(provider: provider)
                })
            })
        }
    }
    
    func fetchYouTubeData(provider: ProviderEntity) {
        guard provider.type == .youtube else { return }
        provider.fetchYouTubeData().then(in: .main, { [unowned self] result in
            let file = provider.convertFileEntity()
            file.duration = Double(provider.duration ?? .zero)
            file.Member = self.delegate?.current_member.toNewMemory()
            file.MemberId = self.delegate?.current_member.id
            file.provider = .youtube
            file.format = .youtube
            self.repositories.append(file)
            DispatchQueue.main.async { [unowned self] in
                self.setRepositories(self.repositories)
            }
            HUD.hide()
            HUD.flash(.success)
        }).catch({ error in
            HUD.hide()
            HUD.flash(.error)
        }).cancelled({
            HUD.hide()
            HUD.flash(.error)
        })
    }
    
    fileprivate func loadYouTube(provider: ProviderEntity, callback: @escaping ((_ value: WKYTPlayerView) -> ())) {
        guard provider.type == .youtube, let youtubeID = provider.youtubeID else { return }
        let player = WKYTPlayerView()
        player.delegate = self
        self.players[provider.url] = player
        self.pl_resolve = callback
        player.load(withVideoId: youtubeID)
    }
    
    fileprivate func fetchDuration(player: WKYTPlayerView, provider: ProviderEntity, callback: @escaping ((_ value: TimeInterval) -> ()), onError: @escaping ((_ error: Error) -> ())) {
        player.getDuration({ time, error in
            if let unwrapped_error = error {
                onError(unwrapped_error)
            } else {
                callback(time)
            }
        })
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        self.pl_resolve?(playerView)
    }
}

extension WebKitViewController: MediaPickCollectionViewControllerDelegate {
    func configureMediaPickCollectionViewController() {
        mediaPickCollectionViewController.willMove(toParent: self)
        self.addChild(mediaPickCollectionViewController)
        mediaPickCollectionViewController.collectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
        mediaPickCollectionViewController.mediaDelegate = self
        mediaPickCollectionViewController.setRepositories(repositories)
        mediaPickCollectionViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: MediaMode.on.height)
        mediaPickCollectionViewController.setItemSize(CGSize(width: MediaMode.on.height - 10, height: MediaMode.on.height - 10))
        mediaPickCollectionViewContainer.addSubview(mediaPickCollectionViewController.view)
        mediaPickCollectionViewController.didMove(toParent: self)
        self.mediaPickCollectionViewHeightConstraint.constant = self.mediaMode.height
    }
    
    func mediaPickCollectionViewController(sync repositories: FileEntities) {
        self.setRepositories(repositories)
    }
}
