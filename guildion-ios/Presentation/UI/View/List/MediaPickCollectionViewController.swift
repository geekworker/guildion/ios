//
//  MediaPickCollectionViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/19.
//

import Foundation
import UIKit

protocol MediaPickCollectionViewControllerDelegate: class {
    func mediaPickCollectionViewController(sync repositories: FileEntities)
}

class MediaPickCollectionViewController: UICollectionViewController {
    var repositories: FileEntities = FileEntities()
    public weak var mediaDelegate: MediaPickCollectionViewControllerDelegate?
    fileprivate var itemSize: CGSize = CGSize(width: 0, height: 0)
    fileprivate var isAppeared = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isAppeared = true
    }
    
    fileprivate func configure() {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.minimumLineSpacing = 4
        flowLayout.minimumInteritemSpacing = 4
        flowLayout.sectionInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = itemSize
        collectionView.register([
            UINib(resource: R.nib.mediaPickCollectionViewCell): R.nib.mediaPickCollectionViewCell.name,
        ])
        self.collectionView.isScrollEnabled = true
        self.collectionView.isPagingEnabled = false
        self.view.backgroundColor = Theme.themify(key: .transparent)
        self.collectionView.backgroundColor = Theme.themify(key: .transparent)
    }
    
    public func setItemSize(_ size: CGSize) {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        self.itemSize = size
        flowLayout.itemSize = size
    }
    
    func setRepositories(_ repositories: FileEntities) {
        self.repositories = repositories
        guard isViewLoaded, repositories.count > 0 else { return }
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: repositories.count - 1, section: 0), at: .left, animated: true)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return repositories.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.mediaPickCollectionViewCell.name, for: indexPath) as? MediaPickCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
            fatalError("cell is not exists")
        }
        cell.delegate = self
        cell.layer.cornerRadius = 5
        cell.clipsToBounds = true
        cell.setRepository(repository)
        return cell
    }
}

extension MediaPickCollectionViewController: MediaPickCollectionViewCellDelegate {
    func mediaPickCollectionViewCell(tap mediaPickCollectionViewCell: MediaPickCollectionViewCell) {
        guard mediaPickCollectionViewCell.imageView.image != nil else { return }
    }
    
    func mediaPickCollectionViewCell(close mediaPickCollectionViewCell: MediaPickCollectionViewCell) {
        if let repository = repositories.filter({ $0 == mediaPickCollectionViewCell.repository }).first {
            repositories.remove(repository)
            mediaDelegate?.mediaPickCollectionViewController(sync: repositories)
            self.collectionView.reloadData()
        }
    }
}
