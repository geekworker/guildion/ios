//
//  MediaCollectionViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit
import Nuke

class MediaCollectionView: UICollectionView {
    var repositories: FileEntities = FileEntities()
    var preheater = ImagePreheater()
    
    func configure() {
        let flowLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        let height = self.frame.height
        flowLayout.minimumLineSpacing = height / 10
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: height / 10, bottom: 0, right: height / 10)
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: height, height: height)
        self.register([
            UINib(resource: R.nib.mediaCollectionViewCell): R.nib.mediaCollectionViewCell.name,
        ])
        self.isScrollEnabled = false
        self.isPagingEnabled = false
        self.backgroundColor = Theme.themify(key: .transparent)
    }
    
    static func configure(frame: CGRect) -> MediaCollectionView {
        let flowLayout = UICollectionViewFlowLayout()
        let height = frame.height
        flowLayout.minimumLineSpacing = height / 10
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: height / 10, bottom: 0, right: height / 10)
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: height, height: height)
        let mediaCollectionView = MediaCollectionView.init(frame: frame, collectionViewLayout: flowLayout)
        mediaCollectionView.register([
            UINib(resource: R.nib.mediaCollectionViewCell): R.nib.mediaCollectionViewCell.name,
        ])
        mediaCollectionView.isScrollEnabled = false
        mediaCollectionView.isPagingEnabled = false
        mediaCollectionView.backgroundColor = Theme.themify(key: .transparent)
        mediaCollectionView.backgroundView?.backgroundColor = Theme.themify(key: .transparent)
        return mediaCollectionView
    }
    
    func setRepositories(_ repositories: FileEntities) {
        self.repositories = repositories
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: R.nib.mediaCollectionViewCell.name, for: indexPath) as? MediaCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            guard let repository = repositories[safe: $0.row] else { return }
            
            if repository.format.content_type.is_movie, let url = URL(string: repository.thumbnail) {
                requests.append(ImageRequest(url: url))
            }
            
            if repository.format == .youtube, let url = URL(string: repository.thumbnail) {
                requests.append(ImageRequest(url: url))
            }
            
            if repository.format.content_type.is_image, let url = URL(string: repository.url) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}
