//
//  MenuCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    enum MenuType: Int {
        case addChannel
        case ban
        case copy
        case copyLink
        case deleteMessage
        case editMessage
        case editMembers
        case folders
        case guilds
        case invite
        case leave
        case kick
        case mention
        case message
        case notes
        case notification
        case playlists
        case reactions
        case save
        case preview
        case search
        case addSection
        case setting
        case watchParty
        case movieChannel
        case voiceChannel
        case play
        case share
        case youtubeLink
        case none
        
        var image: UIImage? {
            switch self {
            case .addChannel: return R.image.addHash_tag()?.withRenderingMode(.alwaysTemplate)
            case .ban: return R.image.ban()?.withRenderingMode(.alwaysTemplate)
            case .copy: return R.image.clipboard()?.withRenderingMode(.alwaysTemplate)
            case .copyLink: return R.image.link()?.withRenderingMode(.alwaysTemplate)
            case .deleteMessage: return R.image.trash()?.withRenderingMode(.alwaysTemplate)
            case .editMessage: return R.image.pencil()?.withRenderingMode(.alwaysTemplate)
            case .editMembers: return R.image.personEdit()?.withRenderingMode(.alwaysTemplate)
            case .folders: return R.image.insertPicture()?.withRenderingMode(.alwaysTemplate)
            case .guilds: return R.image.logoIcon()?.withRenderingMode(.alwaysTemplate)
            case .kick: return R.image.close()?.withRenderingMode(.alwaysTemplate)
            case .invite: return R.image.addPerson()?.withRenderingMode(.alwaysTemplate)
            case .leave: return R.image.leave()?.withRenderingMode(.alwaysTemplate)
            case .mention: return R.image.ic_at()?.withRenderingMode(.alwaysTemplate)
            case .message: return R.image.mail()?.withRenderingMode(.alwaysTemplate)
            case .notes: return R.image.note()?.withRenderingMode(.alwaysTemplate)
            case .notification: return R.image.notification()?.withRenderingMode(.alwaysTemplate)
            case .playlists: return R.image.movieFolder()?.withRenderingMode(.alwaysTemplate)
            case .reactions: return R.image.loveFace()
            case .save: return R.image.download()?.withRenderingMode(.alwaysTemplate)
            case .preview: return R.image.fullscreen()?.withRenderingMode(.alwaysTemplate)
            case .search: return R.image.search()?.withRenderingMode(.alwaysTemplate)
            case .addSection: return R.image.addTag()?.withRenderingMode(.alwaysTemplate)
            case .setting: return R.image.gear()?.withRenderingMode(.alwaysTemplate)
            case .watchParty: return R.image.watchParty()?.withRenderingMode(.alwaysTemplate)
            case .movieChannel: return R.image.miniPlaying()?.withRenderingMode(.alwaysTemplate)
            case .voiceChannel: return R.image.speaker()?.withRenderingMode(.alwaysTemplate)
            case .play: return R.image.playing()?.withRenderingMode(.alwaysTemplate)
            case .share: return R.image.share()?.withRenderingMode(.alwaysTemplate)
            case .youtubeLink: return R.image.yt_icon_rgb()?.withRenderingMode(.alwaysOriginal)
            case .none: return nil
            }
        }
        
        var string: String {
            switch self {
            case .addChannel: return R.string.localizable.menuChannel()
            case .ban: return R.string.localizable.guildBlock()
            case .copy: return R.string.localizable.menuCopy()
            case .copyLink: return R.string.localizable.menuCopyLink()
            case .deleteMessage: return R.string.localizable.menuDelete()
            case .editMessage: return R.string.localizable.menuEdit()
            case .editMembers: return R.string.localizable.menuMembersEdit()
            case .folders: return R.string.localizable.menuFolder()
            case .guilds: return R.string.localizable.menuGuilds()
            case .kick: return R.string.localizable.menuKick()
            case .invite: return R.string.localizable.invite()
            case .leave: return R.string.localizable.menuLeave()
            case .mention: return R.string.localizable.menuMention()
            case .message: return R.string.localizable.menuMessage()
            case .notes: return R.string.localizable.menuNote()
            case .notification: return R.string.localizable.menuNotification()
            case .playlists: return R.string.localizable.menuPlaylist()
            case .reactions: return R.string.localizable.menuReaction()
            case .save: return R.string.localizable.save()
            case .preview: return R.string.localizable.menuPreview()
            case .search: return R.string.localizable.menuSearch()
            case .addSection: return R.string.localizable.menuSection()
            case .setting: return R.string.localizable.menuSetting()
            case .watchParty: return R.string.localizable.menuStream()
            case .movieChannel: return R.string.localizable.menuMovie()
            case .voiceChannel: return R.string.localizable.menuVoice()
            case .share: return R.string.localizable.share()
            case .play: return R.string.localizable.play()
            case .youtubeLink: return R.string.localizable.open()
            case .none: return ""
            }
        }
        
        var color: UIColor {
            switch self {
            case .leave, .kick, .ban: return Theme.themify(key: .danger).darker(by: 10)!
            case .watchParty, .play: return Theme.themify(key: .main)
            default: return Theme.themify(key: .stringSecondary)
            }
        }
    }
    
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.isHidden = true
            overView.backgroundColor = .white
            overView.alpha = 0.3
        }
    }
    @IBOutlet weak var menuImageView: UIImageView! {
        didSet {
            menuImageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var menuLabel: UILabel! {
        didSet {
            menuLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    
    static let cellSize: CGSize = CGSize(width: 70, height: 70)
    public var repository: MenuType = .none
    
    override func awakeFromNib() {
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .background).lighter(by: 10)
        bgColorView.layer.cornerRadius = 5
        bgColorView.clipsToBounds = true
        self.selectedBackgroundView = bgColorView
    }
    
    public func setRepository(_ repository: MenuType) {
        guard repository != .none else { self.contentView.isHidden = true; return }
        self.contentView.isHidden = false
        self.repository = repository
        self.menuImageView.image = repository.image
        self.menuImageView.tintColor = repository.color
        self.menuLabel.text = repository.string
        self.menuLabel.textColor = repository.color
    }
}
