//
//  FolderCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/23.
//

import Foundation
import UIKit
import Nuke

protocol FolderCollectionViewCellDelegate: class {
    func folderCollectionViewCell(onTap repository: FolderEntity, in folderCollectionViewCell: FolderCollectionViewCell)
}

class FolderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .background)
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            baseView.addGestureRecognizer(longpress)
        }
    }
    @IBOutlet weak var folderView: UIView! {
        didSet {
            folderView.layer.cornerRadius = 5
            folderView.layer.borderWidth = 1
            folderView.layer.borderColor = Theme.themify(key: .border).cgColor
            folderView.clipsToBounds = true
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.isUserInteractionEnabled = false
            overView.isHidden = true
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var folderImage: UIImageView! {
        didSet {
            folderImage.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var folderFooterView: UIView! {
        didSet {
            folderFooterView.backgroundColor = Theme.themify(key: .background).withAlphaComponent(0.6)
        }
    }
    @IBOutlet weak var listImage: UIImageView! {
        didSet {
            listImage.image = R.image.list()?.withRenderingMode(.alwaysTemplate)
            listImage.tintColor = .white
        }
    }
    @IBOutlet weak var countLabel: UILabel! {
        didSet {
            countLabel.textColor = .white
            countLabel.font = Font.FT(size: .S, family: .L)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.font = Font.FT(size: .S, family: .L)
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var gradationView: GradationView! {
        didSet {
            let angle = 180 * CGFloat.pi / 180.0;
            gradationView.transform = CGAffineTransform(rotationAngle: angle)
            gradationView.setGradation()
        }
    }
    public var repository: FolderEntity = FolderEntity()
    public weak var delegate: FolderCollectionViewCellDelegate?
    
    public static let inset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    public static func getSize(height: CGFloat) -> CGSize {
        return CGSize(width: height / 2 * 3, height: height)
    }
    
    public func setRepository(_ repository: FolderEntity) {
        self.folderImage.image = nil
        if let file = repository.FileReferences?.last?.File, let url = URL(string: file.thumbnail) {
            Nuke.loadImage(with: url, into: self.folderImage)
        }
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                if self.repository != repository || self.repository.name != repository.name || self.repository.file_count != repository.file_count {
                    self.nameLabel.text = repository.name
                    self.countLabel.text = "\(repository.file_count)"
                }
                self.repository = repository
            }
        }
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.folderCollectionViewCell(onTap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension FolderCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
