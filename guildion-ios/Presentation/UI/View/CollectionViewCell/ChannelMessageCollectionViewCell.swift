//
//  ChannelMessageCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import MessageKit
import Nuke
import TTTAttributedLabel
import LocalizedTimeAgo
import URLEmbeddedView
import SkeletonView

protocol ChannelMessageCollectionViewCellDelegate: class {
    func channelMessageCollectionViewCell(onTapAvatar repository: MemberEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(onTapURL  url: URL, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(onTapDuration duration: Float, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(onLongPress repository: MessageEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(didSelect emoji: String, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(didTapAdd channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(onTapAttachment repository: FileEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(onTapChannelog repository: MessageEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
    func channelMessageCollectionViewCell(onProgressEnd repository: FileEntity, in channelMessageCollectionViewCell: ChannelMessageCollectionViewCell)
}

class ChannelMessageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            setHoverGestureRecognizer(baseView)
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.isUserInteractionEnabled = false
            overView.alpha = 0.3
            overView.layer.cornerRadius = 5
            overView.clipsToBounds = true
        }
    }
    @IBOutlet weak var timeSectionView: TimeSectionView!
    @IBOutlet weak var timeSectionViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            timeSectionViewHeightConstraint.constant = 0
        }
    }
    @IBOutlet weak var timeSectionViewTopConstraint: NSLayoutConstraint! {
        didSet {
            timeSectionViewTopConstraint.constant = 0
        }
    }
    @IBOutlet weak var avatarImageTopConstraint: NSLayoutConstraint! {
        didSet {
            avatarImageTopConstraint.constant = ChannelMessageSizeCalculator.avatarImageTopConstant
        }
    }
    @IBOutlet weak var avatarImageLeftConstraint: NSLayoutConstraint! {
        didSet {
            avatarImageLeftConstraint.constant = ChannelMessageSizeCalculator.avatarImageLeftConstant
        }
    }
    @IBOutlet weak var avatarImageView: UIImageView! {
        didSet {
            avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
            avatarImageView.clipsToBounds = true
            avatarImageView.isUserInteractionEnabled = true
            setAvatarTapGestureRecognizer(avatarImageView)
            avatarImageView.isHidden = true
        }
    }
    @IBOutlet weak var nameLabelBottomConstraint: NSLayoutConstraint! {
        didSet {
            nameLabelBottomConstraint.constant = ChannelMessageSizeCalculator.nameLabelBottomConstant
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            ChannelMessageCollectionViewCell.configureNameLabel(nameLabel)
            setHoverGestureRecognizer(nameLabel)
            setAvatarTapGestureRecognizer(nameLabel)
            nameLabel.isHidden = true
        }
    }
    @IBOutlet weak var nameLabelHeightConstraint: NSLayoutConstraint! {
        didSet {
            nameLabelHeightConstraint.constant = ChannelMessageSizeCalculator.nameLabelHeight
        }
    }
    @IBOutlet weak var dateLabelHeightConstraint: NSLayoutConstraint! {
        didSet {
            dateLabelHeightConstraint.constant = ChannelMessageSizeCalculator.dateLabelHeight
        }
    }
    
    @IBOutlet weak var contentLeftConstraint: NSLayoutConstraint! {
        didSet {
            contentLeftConstraint.constant = ChannelMessageSizeCalculator.contentLeftConstant
        }
    }
    
    @IBOutlet weak var messageLabelBottomConstraint: NSLayoutConstraint! {
        didSet {
            messageLabelBottomConstraint.constant = ChannelMessageSizeCalculator.messageLabelBottomConstant
        }
    }
    @IBOutlet weak var messageLabelRightConstraint: NSLayoutConstraint! {
        didSet {
            messageLabelRightConstraint.constant = ChannelMessageSizeCalculator.messageLabelRightConstant
        }
    }
    lazy var messageLabelManager: MessageLabelManager = {
        return MessageLabelManager()
    }()
    @IBOutlet weak var messageLabel: TTTAttributedLabel! {
        didSet {
            messageLabel.text = ""
            messageLabelManager.setLabel(messageLabel)
            messageLabel.adjustsFontSizeToFitWidth = true
            messageLabel.minimumScaleFactor = 0.5
            setHoverGestureRecognizer(messageLabel)
            messageLabel.delegate = self
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.text = ""
            dateLabel.textColor = Theme.themify(key: .stringSecondary)
            setHoverGestureRecognizer(dateLabel)
        }
    }
    @IBOutlet weak var attachmentsHeightConstraint: NSLayoutConstraint! {
        didSet {
            attachmentsHeightConstraint.constant = 0
        }
    }
    @IBOutlet weak var attachmentsView: UIView! {
        didSet {
            setHoverGestureRecognizer(attachmentsView)
        }
    }
    @IBOutlet weak var attachmentsBottomConstraint: NSLayoutConstraint! {
        didSet {
            attachmentsBottomConstraint.constant = 0
        }
    }
    @IBOutlet weak var ogpHeightConstraint: NSLayoutConstraint! {
        didSet {
            ogpHeightConstraint.constant = 0
        }
    }
    @IBOutlet weak var ogpContainerView: UIView! {
        didSet {
            ogpContainerView.backgroundColor = .none
            setHoverGestureRecognizer(ogpContainerView)
        }
    }
    @IBOutlet weak var ogpBottomConstraint: NSLayoutConstraint! {
        didSet {
            ogpBottomConstraint.constant = 0
        }
    }
    
    @IBOutlet weak var reactionHeightConstraint: NSLayoutConstraint! {
        didSet {
            reactionHeightConstraint.constant = 0
        }
    }
    @IBOutlet weak var reactionView: UIView! {
        didSet {
            reactionView.backgroundColor = .none
            setHoverGestureRecognizer(reactionView)
        }
    }
    @IBOutlet weak var reactionBottomConstraint: NSLayoutConstraint! {
        didSet {
            reactionBottomConstraint.constant = 0
        }
    }
    @IBOutlet weak var streamChannelogViewBottomConstraint: NSLayoutConstraint! {
        didSet {
            streamChannelogViewBottomConstraint.constant = 0
        }
    }
    @IBOutlet weak var streamChannelogViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            streamChannelogViewHeightConstraint.constant = 0
        }
    }
    @IBOutlet weak var streamChennlogView: StreamChannelogView! {
        didSet {
            streamChennlogView.isHidden = true
            streamChennlogView.delegate = self
            setHoverGestureRecognizer(streamChennlogView)
        }
    }
    public var attachmentViews: [AttachmentView] = []
    public var ogpViews: [OGPView] = []
    public lazy var reactionCollectionView: ReactionCollectionView = {
        let reactionCollectionView = ReactionCollectionView.configure(frame: CGRect(x: 0, y: 0, width: reactionView.frame.width, height: 24))
        reactionCollectionView.delegate = self
        reactionCollectionView.dataSource = self
        reactionCollectionView.reactionDelegate = self
        reactionView.addSubview(reactionCollectionView)
        reactionView.constrainToEdges(reactionCollectionView)
        reactionCollectionView.isHidden = true
        return reactionCollectionView
    }()
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    public var repository: MessageEntity = MessageEntity()
    public var repositories: MemberEntities = MemberEntities()
    public var message: MessageModel {
        return Message_MessageModelTranslator.translate(self.repository)
    }
    public weak var delegate: ChannelMessageCollectionViewCellDelegate?
    
    static func configureMessageLabel(_ label: UILabel) {
        label.textColor = Theme.themify(key: .string)
        label.font = Font.FT(size: .FM, family: .L)
        label.numberOfLines = 0
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 7
        paragraphStyle.lineHeightMultiple = 0
        paragraphStyle.lineBreakMode = .byWordWrapping

        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: label.text ?? "")
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSMakeRange(0, attributedString.length)
        )
        attributedString.addAttribute(
            NSAttributedString.Key.baselineOffset,
            value: 0,
            range: NSMakeRange(0, attributedString.length)
        )
        label.attributedText = attributedString
        label.isUserInteractionEnabled = true
    }
    
    static func configureNameLabel(_ label: UILabel) {
        label.textColor = Theme.themify(key: .string)
        label.font = Font.FT(size: .FM, family: .B)
        label.frame = CGRect(x: label.frame.minX, y: label.frame.minY, width: label.frame.width, height: 16)
        label.numberOfLines = 1
        label.isUserInteractionEnabled = true
    }
    
    func initializeCell() {
        self.updateToHeaderLess()
        self.initializeTimeSectionView()
        self.initializeAttachmentView()
        self.initializeReactionView()
        self.initializeMessageLabel()
        self.initializeOGPView()
        self.initializeChannelogView()
        //self.messageLabelHeightConstraint.isActive = false
    }
    
    func configureAttachmentsView(_ repository: MessageEntity) {
        attachmentsView.backgroundColor = .none
        for file in repository.Files?.items ?? [] {
            generateAttachmentView(file)
        }
    }
    
    func configureOGPViews(_ repository: MessageEntity) {
        ogpContainerView.backgroundColor = .none
        for url in repository.text.extractURLExcludeYouTube() {
            generateOGPView(url: url)
        }
    }
    
    func configureReactionView(_ repository: MessageEntity) {
        guard let reactions = repository.Reactions, reactions.count > 0 else { return }
        self.reactionView.isHidden = false
        self.reactionCollectionView.isHidden = false
        self.reactionBottomConstraint.constant = ChannelMessageSizeCalculator.reactionViewBottomConstant
        self.reactionCollectionView.setRepositories(repository)
        self.reactionCollectionView.reloadData()
        self.reactionHeightConstraint.constant = self.reactionCollectionView.getSize().height
        self.reactionView.layoutIfNeeded()
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .background)
        self.overView.isSkeletonable = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.initializeCell()
    }
    
    func setRepositories(_ repositories: MemberEntities) {
        self.repositories = repositories
        self.messageLabelManager.setRepositories(repositories)
    }
    
    func configure(with message: MessageType, at indexPath: IndexPath, and messagesCollectionView: MessagesCollectionView) {
        switch message.kind {
        case .custom(let data):
            guard let repository = data as? MessageEntity else { return }
            if self.repository != repository {
                self.initializeCell()
            }
            let messagesLayout = messagesCollectionView.messagesCollectionViewFlowLayout
            let types = Message_CustomMessageTypesTranslator.translate((current: repository, messagesLayout: messagesLayout, indexPath: indexPath))
            self.avatarImageTopConstraint.constant = ChannelMessageSizeCalculator.avatarImageTopConstant
            self.configureSenderMessage(repository, types: types, compare: self.repository)
            self.configureTextMessage(repository, types: types, compare: self.repository)
            self.configureHeaderLessMessage(repository, types: types, compare: self.repository)
            self.configureOGPMessage(repository, types: types, compare: self.repository)
            self.configureAttachmentMessage(repository, types: types, compare: self.repository)
            self.configurReactionMessage(repository, types: types, compare: self.repository)
            self.configureChannelogMessage(repository, types: types, compare: self.repository)
            self.configureTimeSectionMessage(repository, types: types, compare: self.repository)
            self.repository = repository
        default: break
        }
    }
    
    fileprivate func configureTextMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard types.contains(.text) else { /*initializeMessageLabel();*/ return }
        self.messageLabel.isHidden = false
        self.messageLabelBottomConstraint.constant = ChannelMessageSizeCalculator.messageLabelBottomConstant
        self.messageLabel.text = repository.text
        messageLabelManager.setRepository(repository)
    }
    
    fileprivate func initializeMessageLabel() {
        // self.messageLabelHeightConstraint.isActive = repository.text.count == 0;
        self.messageLabelBottomConstraint.constant = 0
        self.messageLabel.text = ""
        self.messageLabel.isHidden = true
    }
    
    fileprivate func configureSenderMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard let sender = repository.Sender, !types.contains(.headLess) else { /*updateToHeaderLess();*/ return }
        self.avatarImageView.isHidden = false
        self.nameLabel.isHidden = false
        self.dateLabel.isHidden = false
        
        if /*sender.picture_small != compare.Sender?.picture_small,*/ let url = URL(string: sender.picture_small) {
            Nuke.loadImage(with: url, into: self.avatarImageView)
        }
        
        self.nameLabelHeightConstraint.constant = ChannelMessageSizeCalculator.nameLabelHeight
        self.nameLabelBottomConstraint.constant = ChannelMessageSizeCalculator.nameLabelBottomConstant
        self.dateLabelHeightConstraint.constant = ChannelMessageSizeCalculator.dateLabelHeight
        self.nameLabel.text = sender.nickname
        self.dateLabel.text = "\(repository.created_at.timeAgo())・\(repository.created_at.getFormattedTime())"
    }
    
    fileprivate func configureHeaderLessMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard types.contains(.headLess) else { return }
        self.updateToHeaderLess()
    }
    
    fileprivate func configurReactionMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard types.contains(.reaction), let count = repository.Reactions?.count, count > 0 else { /*initializeReactionView();*/ return }
        //guard count != compare.Reactions?.count else { return }
        self.configureReactionView(repository)
    }
    
    fileprivate func initializeReactionView() {
        self.reactionView.isHidden = true
        self.reactionHeightConstraint.constant = 0
        self.reactionBottomConstraint.constant = 0
    }
    
    fileprivate func configureAttachmentMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard types.contains(.media), repository.Files?.count ?? 0 > 0 else { /*initializeAttachmentView();*/ return }
        if !types.contains(.text) {
            self.attachmentsBottomConstraint.constant = 20
        }
        self.configureAttachmentsView(repository)
    }
    
    fileprivate func initializeAttachmentView() {
        _ = attachmentsView.subviews.map({ $0.removeAllConstraints() })
        attachmentsView.removeAllSubviews()
        self.attachmentViews = []
        self.attachmentsHeightConstraint.constant = 0
        self.attachmentsBottomConstraint.constant = 0
    }
    
    fileprivate func configureOGPMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard types.contains(.ogp) else { /*initializeOGPView();*/ return }
        self.configureOGPViews(repository)
    }
    
    fileprivate func initializeOGPView() {
        _ = ogpContainerView.subviews.map({ $0.removeAllConstraints() })
        ogpContainerView.removeAllSubviews()
        self.ogpViews = []
        self.ogpHeightConstraint.constant = 0
        self.ogpBottomConstraint.constant = 0
    }
    
    fileprivate func configureChannelogMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard  types.contains(.channelog), let channelog = repository.Channelog, channelog.id != nil else { /*initializeChannelogView();*/ return }
        self.updateToHeaderLess()
        self.setChannelogLayout(channelog, message: repository)
    }
    
    fileprivate func initializeChannelogView() {
        self.streamChennlogView.isHidden = true
        self.streamChannelogViewHeightConstraint.constant = 0
        self.streamChannelogViewBottomConstraint.constant = 0
        self.reactionBottomConstraint.constant = 0
    }
    
    fileprivate func configureTimeSectionMessage(_ repository: MessageEntity, types: [CustomMessageTypes], compare: MessageEntity = MessageEntity()) {
        guard types.contains(.timeSection) else { /*initializeTimeSectionView();*/ return }
        self.timeSectionViewHeightConstraint.constant = ChannelMessageSizeCalculator.timeSectionViewHeightConstant
        self.timeSectionViewTopConstraint.constant = ChannelMessageSizeCalculator.timeSectionViewTopConstant
        self.timeSectionView.setRepository(repository.created_at)
        self.timeSectionView.isHidden = false
    }
    
    fileprivate func initializeTimeSectionView() {
        self.timeSectionViewTopConstraint.constant = 0
        self.timeSectionViewHeightConstraint.constant = 0
        self.timeSectionView.isHidden = true
    }
    
    fileprivate func setChannelogLayout(_ repository: ChannelogEntity, message: MessageEntity) {
        self.streamChennlogView.setRepository(repository)
        self.streamChennlogView.isHidden = false
        self.streamChennlogView.backgroundColor = Theme.themify(key: .background)
        self.avatarImageView.isHidden = true
        self.nameLabel.isHidden = true
        self.dateLabel.isHidden = true
        self.streamChannelogViewHeightConstraint.constant = ChannelMessageSizeCalculator.streamChannelogHeight
        self.reactionBottomConstraint.constant = ChannelMessageSizeCalculator.streamChannelogBottomConstant
        self.streamChannelogViewBottomConstraint.constant = message.Reactions?.count ?? 0 > 0 ? ChannelMessageSizeCalculator.streamChannelogBottomConstant : 0
    }
    
    fileprivate func generateAttachmentView(_ repository: FileEntity) {
        let attachmentView = AttachmentView(),
        last_attachmentView = self.attachmentViews.last,
        viewInset: CGFloat = ChannelMessageSizeCalculator.attachmentMarginConstant,
        maxWidth = UIScreen.main.bounds.width - ChannelMessageSizeCalculator.contentLeftConstant - ChannelMessageSizeCalculator.messageLabelRightConstant - (viewInset * 2)
        attachmentView.delegate = self
        attachmentView.layer.cornerRadius = 5
        attachmentView.clipsToBounds = true
        attachmentView.setRepository(repository)
        setHoverGestureRecognizer(attachmentView)
        self.attachmentViews.append(attachmentView)
        self.attachmentsView.addSubview(attachmentView)
        attachmentView.translatesAutoresizingMaskIntoConstraints = false
        attachmentView.leadingAnchor.constraint(equalTo: attachmentsView.leadingAnchor, constant: viewInset).isActive = true
        if let unwrapped = last_attachmentView {
            attachmentView.topAnchor.constraint(equalTo: unwrapped.bottomAnchor, constant: viewInset).isActive = true
        } else {
            attachmentView.topAnchor.constraint(equalTo: attachmentsView.topAnchor, constant: viewInset).isActive = true
        }
        attachmentView.heightAnchor.constraint(equalToConstant: attachmentView.getSize(maxWidth: maxWidth).height).isActive = true
        let widthConstraint = attachmentView.widthAnchor.constraint(equalToConstant: attachmentView.getSize(maxWidth: maxWidth).width)
        widthConstraint.priority = UILayoutPriority(rawValue: 999)
        widthConstraint.isActive = true
        self.updateAttachmentHeight()
    }
    
    fileprivate func calculateAttachmentHeight() -> CGFloat {
        let count = self.attachmentViews.count
        guard count > 0 else { return CGFloat(0) }
        var height: CGFloat = ChannelMessageSizeCalculator.attachmentMarginConstant
        let viewInset: CGFloat = ChannelMessageSizeCalculator.attachmentMarginConstant,
        maxWidth = UIScreen.main.bounds.width - ChannelMessageSizeCalculator.contentLeftConstant - ChannelMessageSizeCalculator.messageLabelRightConstant - (viewInset * 2)
        for attachmentView in attachmentViews {
            height += attachmentView.getSize(maxWidth: maxWidth).height + ChannelMessageSizeCalculator.attachmentMarginConstant
        }
        return height
    }
    
    fileprivate func updateAttachmentHeight() {
        self.attachmentsHeightConstraint.constant = calculateAttachmentHeight()
        self.attachmentsView.layoutIfNeeded()
        self.layoutIfNeeded()
    }
    
    fileprivate func generateOGPView(url: URL) {
        let ogpView = OGPView(),
        last_ogpView = self.ogpViews.last,
        viewInset: CGFloat = ChannelMessageSizeCalculator.ogpMarginConstant,
        maxWidth = UIScreen.main.bounds.width - ChannelMessageSizeCalculator.contentLeftConstant - ChannelMessageSizeCalculator.messageLabelRightConstant - (viewInset * 2)
        ogpView.delegate = self
        ogpView.layer.cornerRadius = 5
        ogpView.clipsToBounds = true
        ogpView.setRepository(url)
        setHoverGestureRecognizer(ogpView)
        self.ogpViews.append(ogpView)
        self.ogpContainerView.addSubview(ogpView)
        ogpView.translatesAutoresizingMaskIntoConstraints = false
        ogpView.leadingAnchor.constraint(equalTo: ogpContainerView.leadingAnchor, constant: viewInset).isActive = true
        if let unwrapped = last_ogpView {
            ogpView.topAnchor.constraint(equalTo: unwrapped.bottomAnchor, constant: viewInset).isActive = true
        } else {
            ogpView.topAnchor.constraint(equalTo: ogpContainerView.topAnchor, constant: viewInset).isActive = true
        }
        ogpView.heightAnchor.constraint(equalToConstant: ogpView.calcHeight()).isActive = true
        let widthConstraint = ogpView.widthAnchor.constraint(equalToConstant: maxWidth)
        widthConstraint.priority = UILayoutPriority(rawValue: 999)
        widthConstraint.isActive = true
        self.updateOGPHeight()
    }
    
    fileprivate func calculateOGPHeight() -> CGFloat {
        let count = self.ogpViews.count
        guard count > 0 else { return CGFloat(0) }
        var height: CGFloat = ChannelMessageSizeCalculator.ogpMarginConstant
        let viewInset: CGFloat = ChannelMessageSizeCalculator.ogpMarginConstant
        for ogpView in ogpViews {
            height += ogpView.calcHeight() + viewInset
        }
        return height
    }
    
    fileprivate func updateOGPHeight() {
        self.ogpHeightConstraint.constant = calculateOGPHeight()
        self.ogpContainerView.layoutIfNeeded()
        self.layoutIfNeeded()
    }
    
    fileprivate func updateToHeaderLess() {
        self.avatarImageView.isHidden = true
        self.nameLabel.isHidden = true
        self.dateLabel.isHidden = true
        self.nameLabelHeightConstraint.constant = 0
        self.dateLabelHeightConstraint.constant = 0
        self.nameLabelBottomConstraint.constant = ChannelMessageSizeCalculator.nameLabelHeaderLessBottomConstant
    }
    
    fileprivate func setHoverGestureRecognizer(_ view: UIView) {
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(hover(_:)))
        longpress.minimumPressDuration = 0.3
        longpress.delegate = self
        longpress.cancelsTouchesInView = false
        view.addGestureRecognizer(longpress)
    }
    
    fileprivate func setAvatarTapGestureRecognizer(_ view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTapAvatar(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    fileprivate var gestureStartTime: TimeInterval!
    fileprivate var gestureTimer: Timer!
    fileprivate var gestureDuration: TimeInterval!
    static var minimumLongPressActionTime: TimeInterval = TimeInterval(0.1)
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            gestureStartTime = Date.timeIntervalSinceReferenceDate
            gestureTimer = Timer.scheduledTimer(withTimeInterval: ChannelMessageCollectionViewCell.minimumLongPressActionTime, repeats: false, block: { (timer) in
                self.delegate?.channelMessageCollectionViewCell(onLongPress: self.repository, in: self)
            })
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = Theme.themify(key: .background).darker(by: 10)
            })
        case .ended:
            gestureDuration = Date.timeIntervalSinceReferenceDate - gestureStartTime
            gestureTimer?.invalidate()
            gestureTimer = nil
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .none
            })
        default:
            self.overView.backgroundColor = .none
        }
    }
    
    @objc fileprivate func onTapAvatar(_ sender: Any) {
        guard let sender = self.repository.Sender else { return }
        self.delegate?.channelMessageCollectionViewCell(onTapAvatar: sender, in: self)
    }
}

extension ChannelMessageCollectionViewCell: AttachmentViewDelegate {
    func attachmentView(tap repository: FileEntity, in attachmentView: AttachmentView) {
        self.delegate?.channelMessageCollectionViewCell(onTapAttachment: repository, in: self)
    }
    
    func attachmentView(onProgressEnd repository: FileEntity, in attachmentView: AttachmentView) {
        self.delegate?.channelMessageCollectionViewCell(onProgressEnd: repository, in: self)
    }
}

extension ChannelMessageCollectionViewCell: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        self.delegate?.channelMessageCollectionViewCell(onTapURL: url, in: self)
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith result: NSTextCheckingResult!) {
        guard let regExp = result.regularExpression, let text = label.text as? String else { return }
        switch regExp.pattern {
        case MessageLabelManager.duration_pattern:
            self.attributedLabel(label, didSelectDurationWith: text.substring(with: result.range))
            return
        case MessageLabelManager.everyone_mention_pattern: return
        case MessageLabelManager.here_mention_pattern: return
        default: break
        }
        let prefix = text.substring(with: result.range).first
        let result_text = String(text.substring(with: result.range).dropFirst())
        switch prefix {
        case "#": self.attributedLabel(label, didSelectHashTagWith: result_text)
        case "@": self.attributedLabel(label, didSelectMentionWith: result_text)
        default: break
        }
    }
    
    fileprivate func attributedLabel(_ label: TTTAttributedLabel!, didSelectDurationWith result: String) {
        guard result.contains(":") else { return }
        let splits = result.split(separator: ":")
        let duration = splits.enumerated().reduce(Float(0)) {
            let offset = splits.count - 1 - $1.offset
            return offset == 0 ? $0 + (Float($1.element) ?? 0) : $0 + ((Float($1.element) ?? 0) * pow(60, Float(offset)))
        }
        self.delegate?.channelMessageCollectionViewCell(onTapDuration: duration, in: self)
    }
    
    fileprivate func attributedLabel(_ label: TTTAttributedLabel!, didSelectMentionWith result: String) {
        guard let member = self.repositories.filter({ $0.nickname == result }).first else { return }
        self.delegate?.channelMessageCollectionViewCell(onTapAvatar: member, in: self)
    }
    
    fileprivate func attributedLabel(_ label: TTTAttributedLabel!, didSelectHashTagWith result: String) {
    }
}

extension ChannelMessageCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reactionCollectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return reactionCollectionView.collectionView(cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return reactionCollectionView.collectionView(layout: collectionViewLayout, insetForSectionAt: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return reactionCollectionView.collectionView(layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
}

extension ChannelMessageCollectionViewCell: ReactionCollectionViewDelegate {
    func reactionCollectionView(didSelected emoji: String, in reactionCollectionView: ReactionCollectionView) {
        self.delegate?.channelMessageCollectionViewCell(didSelect: emoji, in: self)
    }
    
    func reactionCollectionView(onTapAdd reactionCollectionView: ReactionCollectionView) {
        self.delegate?.channelMessageCollectionViewCell(didTapAdd: self)
    }
}

extension ChannelMessageCollectionViewCell: StreamChannelogViewDelegate {
    func streamChannelogView(onTap repository: ChannelogEntity, in streamChannelogView: StreamChannelogView) {
        self.delegate?.channelMessageCollectionViewCell(onTapChannelog: self.repository, in: self)
    }
}

extension ChannelMessageCollectionViewCell: OGPViewDelegate {
    func ogpView(onClickURL url: URL, in ogpView: OGPView) {
        self.delegate?.channelMessageCollectionViewCell(onTapURL: url, in: self)
    }
}

extension ChannelMessageCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
