//
//  ParticipantNavigationCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/20.
//

import Foundation
import UIKit
import Nuke

protocol ParticipantNavigationCollectionViewCellDelegate: class {
    func participantNavigationCollectionViewCell(onTap repository: ParticipantEntity, in participantNavigationCollectionViewCell: ParticipantNavigationCollectionViewCell)
}

class ParticipantNavigationCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .transparent)
        }
    }
    @IBOutlet weak var audioView: UIView! {
        didSet {
            audioView.backgroundColor = Theme.themify(key: .main)
            audioView.isHidden = true
        }
    }
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.contentMode = .scaleAspectFill
            profileImageView.image = UIImage()
        }
    }
    static let cellSize: CGSize = CGSize(width: 38, height: 38)
    public var repository: ParticipantEntity = ParticipantEntity()
    public var isSpeaking: Bool = false
    weak var delegate: ParticipantNavigationCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        self.baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        updateLayout()
    }
    
    public func setRepository(_ repository: ParticipantEntity) {
        self.repository = repository
        repository.delegate = self
        if let picture_small = repository.Member?.picture_small, let url = URL(string: picture_small) {
            Nuke.loadImage(with: url, into: profileImageView)
        }
        updateLayout()
    }
    
    func updateLayout() {
        self.baseView.layer.cornerRadius = self.baseView.frame.width / 2
        self.baseView.clipsToBounds = true
        self.audioView.layer.cornerRadius = self.audioView.frame.width / 2
        self.audioView.clipsToBounds = true
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.width / 2
        self.profileImageView.clipsToBounds = true
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.participantNavigationCollectionViewCell(onTap: repository, in: self)
    }
}

extension ParticipantNavigationCollectionViewCell: ParticipantEntityDelegate {
    func participantEntity(didChange meterLevel: Float, in fileEntity: ParticipantEntity) {
        self.audioView.isHidden = meterLevel < LiveConfig.min_meter_level
    }
}
