//
//  MediaPickCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/19.
//

import Foundation
import UIKit
import Nuke

protocol MediaPickCollectionViewCellDelegate: class {
    func mediaPickCollectionViewCell(tap mediaPickCollectionViewCell: MediaPickCollectionViewCell)
    func mediaPickCollectionViewCell(close mediaPickCollectionViewCell: MediaPickCollectionViewCell)
}

class MediaPickCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var closeButton: IconButton! {
        didSet {
            closeButton.buttonImage = R.image.close()!.withRenderingMode(.alwaysTemplate)
            closeButton.imageColor = Theme.themify(key: .string)
            closeButton.backgroundColor = Theme.themify(key: .background).withAlphaComponent(0.7)
            closeButton.buttonInset = 5
            closeButton.delegate = self
            closeButton.layer.cornerRadius = closeButton.bounds.width / 2
            closeButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(self.tap(_:))
                )
            )
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.delegate = self
            longpress.cancelsTouchesInView = false
            overView.addGestureRecognizer(longpress)
            overView.alpha = 0.1
        }
    }
    @IBOutlet weak var gradationView: GradationView! {
        didSet {
            let angle = 180 * CGFloat.pi / 180.0;
            gradationView.transform = CGAffineTransform(rotationAngle: angle)
            gradationView.setGradation()
        }
    }
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var formatImage: UIImageView!
    public var repository: FileEntity = FileEntity()
    public weak var delegate: MediaPickCollectionViewCellDelegate?
    
    public func setRepository(_ repository: FileEntity) {
        self.repository = repository
        
        if repository.format.content_type.is_movie {
            if let image = repository.url_image {
                imageView.image = image
            } else {
                imageView.image = UIImage(url: repository.url)
            }
            self.formatImage.image = R.image.videoPlay()!.withRenderingMode(.alwaysTemplate)
            self.formatImage.tintColor = .white
            self.durationLabel.text = repository.formattedDuration
            self.nameLabel.text = repository.name
            self.footerView.isHidden = false
            self.gradationView.isHidden = false
        }
        
        if repository.format == .youtube {
            if let url = URL(string: repository.thumbnail) {
                Nuke.loadImage(with: url, into: imageView)
            }
            self.formatImage.image = R.image.yt_icon_rgb()?.withRenderingMode(.alwaysOriginal)
            self.durationLabel.text = repository.formattedDuration
            self.nameLabel.text = repository.name
            self.footerView.isHidden = false
            self.gradationView.isHidden = false
        }
        
        if repository.format.content_type.is_image {
            if let image = repository.url_image {
                imageView.image = image
            } else {
                imageView.image = UIImage(url: repository.url)
            }
            self.formatImage.image = R.image.insertPicture()!.withRenderingMode(.alwaysTemplate)
            self.formatImage.tintColor = .white
            self.footerView.isHidden = true
            self.gradationView.isHidden = true
        }
    }
    
    override func awakeFromNib() {
        self.contentView.clipsToBounds = true
    }
    
    @objc fileprivate func tap(_ sender: UITapGestureRecognizer) {
        self.delegate?.mediaPickCollectionViewCell(tap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .white
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .none
            })
        default:
            self.overView.backgroundColor = .none
        }
    }
}

extension MediaPickCollectionViewCell: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        self.delegate?.mediaPickCollectionViewCell(close: self)
    }
}

extension MediaPickCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
