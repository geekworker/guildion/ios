//
//  GuildCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/11.
//

import Foundation
import UIKit
import Nuke

protocol GuildCollectionViewCellDelegate: class {
    func guildCollectionViewCell(onTap repository: GuildEntity, in guildCollectionViewCell: GuildCollectionViewCell)
}

class GuildCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            overView.isHidden = true
            overView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(self.onTap(_:))
                )
            )
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.delegate = self
            longpress.cancelsTouchesInView = false
            baseView.addGestureRecognizer(longpress)
            baseView.backgroundColor = Theme.themify(key: .backgroundThick)
        }
    }
    @IBOutlet weak var guildImageView: UIImageView! {
        didSet {
            guildImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var onlineBadgeView: UIView! {
        didSet {
            onlineBadgeView.layer.cornerRadius = onlineBadgeView.frame.width / 2
            onlineBadgeView.clipsToBounds = true
            onlineBadgeView.backgroundColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var onlineCountLabel: UILabel! {
        didSet {
            onlineCountLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var memberBadgeView: UIView! {
        didSet {
            memberBadgeView.layer.cornerRadius = memberBadgeView.frame.width / 2
            memberBadgeView.clipsToBounds = true
            memberBadgeView.backgroundColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var memberCountLabel: UILabel! {
        didSet {
            memberCountLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    public var repository: GuildEntity = GuildEntity()
    public weak var delegate: GuildCollectionViewCellDelegate?
    
    static let margin: CGFloat = 8
    static func getSize(collectionViewFrame: CGRect) -> CGSize {
        let width = (collectionViewFrame.width - (margin * 3)) / 2
        return CGSize(width: width, height: width / 4 * 5)
    }
    
    override func awakeFromNib() {
        self.contentView.layer.cornerRadius = 5
        self.contentView.clipsToBounds = true
    }
    
    func setRepository(_ repository: GuildEntity) {
        self.repository = repository
        self.nameLabel.text = repository.name
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: guildImageView)
        } else {
            guildImageView.image = nil
        }
        self.descriptionLabel.text = self.repository.description.count == 0 ? R.string.localizable.descriptionNone() : self.repository.description
        self.onlineCountLabel.text = "\(repository.active_count)"
        self.memberCountLabel.text = "\(repository.member_count)"
        
        self.hero.isEnabled = true
        self.hero.id = "view/\(repository.uid)"
        self.guildImageView.hero.id = "guildImageView/\(repository.uid)"
        self.nameLabel.hero.id = "nameLabel/\(repository.uid)"
        self.descriptionLabel.hero.id = "descriptionLabel/\(repository.uid)"
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.guildCollectionViewCell(onTap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension GuildCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
