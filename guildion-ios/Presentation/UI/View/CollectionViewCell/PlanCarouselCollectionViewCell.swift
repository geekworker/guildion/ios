//
//  PlanCarouselCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit
import FSPagerView

class PlanCarouselCollectionViewCell: FSPagerViewCell {
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.tintColor = .white
            iconImageView.layer.shadowOpacity = 0
            iconImageView.layer.shadowColor = .none
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = .white
            titleLabel.layer.shadowOpacity = 0
            titleLabel.layer.shadowColor = .none
        }
    }
    
    enum MenuType: Int {
        case storage
        case participant
        case reaction
        case none
        
        var image: UIImage {
            switch self {
            case .storage: return R.image.moreStorage()!.withRenderingMode(.alwaysTemplate)
            case .participant: return R.image.moreParticipant()!.withRenderingMode(.alwaysTemplate)
            case .reaction: return R.image.moreReaction()!.withRenderingMode(.alwaysTemplate)
            case .none: return UIImage()
            }
        }
        
        var string: String {
            switch self {
            case .storage: return R.string.localizable.planStorageTitle()
            case .participant: return R.string.localizable.planParticipantTitle()
            case .reaction: return R.string.localizable.planReactionTitle()
            case .none: return ""
            }
        }
        
        var color: UIColor {
            switch self {
            case .storage: return UIColor(hex: "E27E7E")
            case .participant: return UIColor(hex: "75CCE8")
            case .reaction: return UIColor(hex: "80BEAF")
            default: return UIColor(hex: "E27E7E")
            }
        }
    }
    
    var repository: MenuType = .none
    
    override func awakeFromNib() {
        self.contentView.layer.shadowRadius = 0
        self.contentView.layer.shadowColor = UIColor.clear.cgColor
    }
    
    func setRepository(_ repository: MenuType) {
        iconImageView.layer.shadowOpacity = 0
        iconImageView.layer.shadowColor = .none
        self.repository = repository
        self.titleLabel.text = repository.string
        self.iconImageView.image = repository.image
        self.backgroundColor = repository.color
    }
}
