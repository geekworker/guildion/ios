//
//  AddCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/10.
//

import Foundation
import UIKit

protocol AddCollectionViewCellDelegate: class {
    func addCollectionViewCell(onTap addCollectionViewCell: AddCollectionViewCell)
}

class AddCollectionViewCell: UICollectionViewCell {
    static func getSize() -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            baseView.addGestureRecognizer(longpress)
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            overView.isHidden = true
            overView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.image = R.image.plus()?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    public weak var delegate: AddCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        contentView.layer.cornerRadius = contentView.frame.height / 2
        contentView.clipsToBounds = true
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.addCollectionViewCell(onTap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension AddCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
