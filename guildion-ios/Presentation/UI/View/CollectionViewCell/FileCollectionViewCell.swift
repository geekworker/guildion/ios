//
//  FileCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/23.
//

import Foundation
import UIKit
import Nuke
import M13ProgressSuite

protocol FileCollectionViewCellDelegate: class {
    func fileCollectionViewCell(onTap repository: FileEntity, in fileCollectionViewCell: FileCollectionViewCell)
    func fileCollectionViewCell(onProgressEnd repository: FileEntity, in fileCollectionViewCell: FileCollectionViewCell)
}

class FileCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lockedImageView: UIImageView! {
        didSet {
            lockedImageView.image = R.image.locked()?.withRenderingMode(.alwaysTemplate)
            lockedImageView.tintColor = Theme.themify(key: .warning)
            lockedImageView.isHidden = true
        }
    }
    @IBOutlet weak var progressViewPie: M13ProgressViewPie! {
        didSet {
            progressViewPie.isUserInteractionEnabled = false
            progressViewPie.isHidden = true
            progressViewPie.primaryColor = Theme.themify(key: .main)
            progressViewPie.secondaryColor = Theme.themify(key: .main)
            progressViewPie.setProgress(0, animated: false)
        }
    }
    @IBOutlet weak var progressView: UIView! {
        didSet {
            progressView.isUserInteractionEnabled = false
            progressView.isHidden = true
            progressView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }
    }
    @IBOutlet weak var hoverView: UIView! {
        didSet {
            hoverView.isHidden = true
            hoverView.isUserInteractionEnabled = false
            hoverView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var checkImageView: UIImageView! {
        didSet {
            checkImageView.isHidden = true
            checkImageView.isUserInteractionEnabled = false
            checkImageView.layer.cornerRadius = checkImageView.bounds.width / 2
            checkImageView.layer.borderWidth = 0.5
            checkImageView.layer.borderColor = Theme.themify(key: .border).cgColor
            checkImageView.clipsToBounds = true
            checkImageView.image = UIImage()
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.alpha = 0.3
            overView.backgroundColor = Theme.themify(key: .transparent)
            overView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.isUserInteractionEnabled = true
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            baseView.addGestureRecognizer(longpress)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.isHidden = true
        }
    }
    @IBOutlet weak var fileImageView: UIImageView! {
        didSet {
            fileImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var gradationView: GradationView! {
        didSet {
            let angle = 180 * CGFloat.pi / 180.0;
            gradationView.transform = CGAffineTransform(rotationAngle: angle)
            gradationView.setGradation()
        }
    }
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var formatImage: UIImageView!
    @IBOutlet weak var ownerImage: UIImageView! {
        didSet {
            ownerImage.layer.cornerRadius = ownerImage.frame.width / 2
            ownerImage.layer.borderWidth = 0.5
            ownerImage.layer.borderColor = Theme.themify(key: .border).cgColor
            ownerImage.clipsToBounds = true
            ownerImage.contentMode = .scaleAspectFill
        }
    }
    
    var isInEditingMode: Bool = false {
        didSet {
            checkImageView.isHidden = !isInEditingMode || repository.is_private
        }
    }

    override var isSelected: Bool {
        didSet {
            if isInEditingMode {
                checkImageView.image = isSelected ? R.image.success()!.withRenderingMode(.alwaysOriginal) : UIImage()
                overView.backgroundColor = isSelected ? .white : .clear
            } else {
                checkImageView.image = UIImage()
                overView.backgroundColor = .clear
            }
        }
    }
    public var repository: FileEntity = FileEntity()
    public weak var delegate: FileCollectionViewCellDelegate?
    
    public static let inset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    public static func getSize() -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: width / 3 - inset.left - inset.right, height: width / 3 - inset.top - inset.bottom)
    }
    
    public static func getMovieSize() -> CGSize {
        let fullWidth = UIScreen.main.bounds.width
        let width = fullWidth / 3
        let height = width / 3 * 2
        return CGSize(width: width - inset.left - inset.right, height: height - inset.top - inset.bottom)
    }
    
    public func setRepository(_ repository: FileEntity) {
        self.isInEditingMode = false
        self.overView.backgroundColor = .clear
        
        guard self.repository !== repository else { return }
        
        if self.repository.thumbnail != repository.thumbnail, let url = URL(string: repository.thumbnail) {
            Nuke.loadImage(with: url, into: self.fileImageView)
        }
        
        repository.delegate = self
        self.progressView.isHidden = repository.progress == nil || repository.progress ?? 0 >= 1
        self.progressViewPie.isHidden = repository.progress == nil || repository.progress ?? 0 >= 1
        if self.repository.progress != repository.progress {
            self.progressViewPie.setProgress(CGFloat(repository.progress ?? 0), animated: false)
        }
        
        self.lockedImageView.isHidden = !repository.is_private
        
        if self.repository.Member?.picture_small != repository.Member?.picture_small, let picture_small = repository.Member?.picture_small, let url = URL(string: picture_small) {
            Nuke.loadImage(with: url, into: self.ownerImage)
        }
        
        self.formatImage.image = repository.format.iconImage
        
        if repository != self.repository && repository.format.content_type.is_movie {
            self.formatImage.tintColor = .white
            self.durationLabel.text = repository.formattedDuration
            self.durationLabel.isHidden = false
        }
        
        if repository != self.repository && repository.format == .youtube {
            self.durationLabel.text = repository.formattedDuration
            self.durationLabel.isHidden = false
        }
        
        if repository != self.repository && repository.format.content_type.is_image {
            self.formatImage.tintColor = .white
            self.durationLabel.isHidden = true
        }
        
        self.repository = repository
    }
    
    func setEditing(_ editing: Bool, animated: Bool) {
        if !editing || repository.is_private {
            self.isSelected = false
        }
        self.isInEditingMode = editing && !repository.is_private
        baseView.isUserInteractionEnabled = !editing
    }

    @objc func onTap(_ sender: Any) {
        self.delegate?.fileCollectionViewCell(onTap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            hoverView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            hoverView.fadeOut(type: .Normal, completed: nil)
        default:
            hoverView.isHidden = true
        }
    }
}

extension FileCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension FileCollectionViewCell: FileEntityDelegate {
    func fileEntity(didChange progress: Float, in fileEntity: FileEntity) {
        if progress > 0, progress < 1 {
            self.progressView.isHidden = false
            self.progressViewPie.isHidden = false
            self.progressViewPie.setProgress(CGFloat(progress), animated: true)
        } else if progress >= 1 {
            self.progressView.fadeOut(type: .Normal, completed: nil)
            self.progressViewPie.fadeOut(type: .Normal, completed: nil)
            self.delegate?.fileCollectionViewCell(onProgressEnd: fileEntity, in: self)
        }
    }
}
