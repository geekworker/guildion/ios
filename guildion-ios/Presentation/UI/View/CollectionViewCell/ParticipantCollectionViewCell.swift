//
//  ParticipantCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/19.
//

import Foundation
import UIKit
import Nuke

protocol ParticipantCollectionViewCellDelegate: class {
    func participantCollectionViewCell(onTap repository: ParticipantEntity, in participantCollectionViewCell: ParticipantCollectionViewCell)
}

class ParticipantCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            overView.layer.cornerRadius = 5
            overView.clipsToBounds = true
            overView.isHidden = true
            overView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var audioView: UIView! {
        didSet {
            audioView.layer.cornerRadius = 48
            audioView.clipsToBounds = true
            audioView.backgroundColor = Theme.themify(key: .main)
            audioView.isHidden = true
        }
    }
    @IBOutlet weak var profileView: UIView! {
        didSet {
            profileView.layer.cornerRadius = 48
            profileView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.font = Font.FT(size: .M, family: .L)
            nameLabel.textColor = Theme.themify(key: .string)
            nameLabel.text = ""
        }
    }
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.layer.cornerRadius = 42
            profileImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var baseView: UIView!
    public var repository: ParticipantEntity = ParticipantEntity()
    weak var delegate: ParticipantCollectionViewCellDelegate?
    static func cellSize(height: CGFloat) -> CGSize {
        return CGSize(width: 130, height: 130)
    }
    
    override func awakeFromNib() {
        let longpress = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.hover(_:))
        )
        self.baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
        longpress.minimumPressDuration = 0.01
        longpress.cancelsTouchesInView = false
        longpress.delegate = self
        baseView.addGestureRecognizer(longpress)
        self.baseView.backgroundColor = Theme.themify(key: .transparent)
        self.backgroundColor = Theme.themify(key: .transparent)
        self.contentView.backgroundColor = Theme.themify(key: .transparent)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.nameLabel.text = ""
        self.profileImageView.image = UIImage()
    }
    
    func setRepository(_ repository: ParticipantEntity) {
        self.repository = repository
        repository.delegate = self
        self.nameLabel.text = repository.Member?.nickname
        if let picture_small = repository.Member?.picture_small, let url = URL(string: picture_small) {
            Nuke.loadImage(with: url, into: profileImageView)
        }
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.participantCollectionViewCell(onTap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension ParticipantCollectionViewCell: ParticipantEntityDelegate {
    func participantEntity(didChange meterLevel: Float, in fileEntity: ParticipantEntity) {
        self.audioView.isHidden = meterLevel < LiveConfig.min_meter_level
    }
}

extension ParticipantCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
