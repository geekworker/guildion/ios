//
//  ReactionAddCollectionViewCell.swift
//  nowy-ios
//
//  Created by Apple on 2020/07/07.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import EmojiPicker

protocol ReactionAddCollectionViewCellDelegate: class {
    func reactionAddCollectionViewCell(onTap reactionAddCollectionViewCell: ReactionAddCollectionViewCell)
}

class ReactionAddCollectionViewCell: UICollectionViewCell {
    static func getSize() -> CGSize {
        return CGSize(width: 30, height: 30)
    }
    
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.image = R.image.plus()?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    public weak var delegate: ReactionAddCollectionViewCellDelegate!
    private var picker: EmojiPickerViewController?
    
    override func awakeFromNib() {
        baseView.layer.cornerRadius = baseView.frame.height / 2
        baseView.clipsToBounds = true
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate.reactionAddCollectionViewCell(onTap: self)
    }
}
