//
//  ReactionCollectionViewCell.swift
//  nowy-ios
//
//  Created by Apple on 2020/07/07.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol ReactionCollectionViewCellDelegate: class {
    func reactionCollectionViewCell(onTap reactionCollectionViewCell: ReactionCollectionViewCell)
}

class ReactionCollectionViewCell: UICollectionViewCell {
    static func getSize() -> CGSize {
        return CGSize(width: 50, height: 30)
    }
    
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
        }
    }
    @IBOutlet weak var reactionLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel! {
        didSet {
            countLabel.textColor = Theme.themify(key: .string)
        }
    }
    public weak var delegate: ReactionCollectionViewCellDelegate!
    public var repositories: ReactionEntities = ReactionEntities()
    public var emoji: String? {
        get {
            repositories.items.first?.data
        }
    }
    
    override func awakeFromNib() {
        baseView.layer.cornerRadius = 2
        baseView.clipsToBounds = true
    }
    
    public func setRepositories(repositories: ReactionEntities) {
        reactionLabel.text = ""
        countLabel.text = ""
        guard repositories.items.count > 0 else { return }
        let beforeCount = self.repositories.items.count
        self.repositories = repositories
        
        if beforeCount + 1 == repositories.items.count {
            let animation: CATransition = CATransition()
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            animation.type = CATransitionType.push
            animation.subtype = CATransitionSubtype.fromTop
            self.countLabel.text = "\(repositories.items.count)"
            animation.duration = 0.25
            self.countLabel.layer.add(animation, forKey: kCATransition)
        } else {
            self.countLabel.text = "\(repositories.items.count)"
        }
        
        self.reactionLabel.text = repositories.items.first!.data
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate.reactionCollectionViewCell(onTap: self)
        let entity = ReactionEntity()
        entity.data = self.reactionLabel.text!
        self.repositories.items.append(entity)
        
        let animation: CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromTop
        self.countLabel.text = "\(repositories.items.count)"
        animation.duration = 0.25
        self.countLabel.layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
}
