//
//  FolderHeaderViewCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/25.
//

import Foundation
import UIKit

class FolderHeaderViewCollectionViewCell: UICollectionViewCell {
    lazy var folderHeaderView: FolderHeaderView = {
        return FolderHeaderView()
    }()
    static let height = FolderHeaderView.height
    static func getSize() -> CGSize { return CGSize(width: UIScreen.main.bounds.width, height: height) }
    public weak var delegate: FolderHeaderViewDelegate? {
        get { folderHeaderView.delegate }
        set { folderHeaderView.delegate = newValue }
    }
    public var repository: FolderEntity {
        get { folderHeaderView.repository }
        set { folderHeaderView.repository = newValue }
    }
    public func setRepository(_ repository: FolderEntity) {
        self.folderHeaderView.setRepository(repository)
    }
    override func awakeFromNib() {
        self.contentView.addSubview(folderHeaderView)
        self.contentView.constrainToEdges(folderHeaderView)
    }
}
