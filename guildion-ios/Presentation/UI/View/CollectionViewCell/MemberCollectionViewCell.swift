//
//  MemberCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit
import Nuke

protocol MemberCollectionViewCellDelegate: class {
    func memberCollectionViewCell(onTap repository: MemberEntity, in memberCollectionViewCell: MemberCollectionViewCell)
}

class MemberCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            overView.layer.cornerRadius = 5
            overView.clipsToBounds = true
            overView.isHidden = true
            overView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var activeView: ActiveView! {
        didSet {
            activeView.layer.borderWidth = 1
            activeView.layer.borderColor = Theme.themify(key: .border).cgColor
        }
    }
    @IBOutlet weak var memberImageView: UIImageView! {
        didSet {
            memberImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var descLabel: UILabel! {
        didSet {
            descLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    
    public weak var delegate: MemberCollectionViewCellDelegate?
    public var repository: MemberEntity = MemberEntity()
    public var role: RoleEntity = RoleEntity()
    
    static func getSize(_ frame: CGRect) -> CGSize {
        return CGSize(width: frame.width / 3, height: (frame.width / 3 - 40) + 86)
    }
    
    override func awakeFromNib() {
        let longpress = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.hover(_:))
        )
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
        longpress.minimumPressDuration = 0.01
        longpress.cancelsTouchesInView = false
        longpress.delegate = self
        contentView.addGestureRecognizer(longpress)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        memberImageView.layer.cornerRadius = memberImageView.frame.width / 2
        memberImageView.clipsToBounds = true
        activeView.layer.cornerRadius = activeView.frame.width / 2
        activeView.clipsToBounds = true
    }
    
    func setRepository(_ repository: MemberEntity, role: RoleEntity) {
        self.repository = repository
        self.role = role
        self.nameLabel.text = repository.nickname == "" ? repository.User?.nickname ?? "" : repository.nickname
        self.descLabel.text = repository.MemberRoles?.filter({ $0.RoleId == role.id }).first?.position ?? ""
        if let url = URL(string: repository.picture_small == "" ? repository.User?.picture_small ?? "" : repository.picture_small) {
            Nuke.loadImage(with: url, into: memberImageView)
        }
        activeView.setRepository(repository: repository.User ?? UserEntity())
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.memberCollectionViewCell(onTap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension MemberCollectionViewCell: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
