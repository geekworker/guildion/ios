//
//  ImagePagingCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import UIKit
import Parchment
import Nuke

class ImagePagingCell: PagingCell {
    fileprivate lazy var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
          
    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.semibold)
        label.textColor = UIColor.white
        label.backgroundColor = UIColor(white: 0, alpha: 0.3)
        label.numberOfLines = 0
        return label
    }()
  
    fileprivate lazy var paragraphStyle: NSParagraphStyle = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.hyphenationFactor = 1
        paragraphStyle.alignment = .center
        return paragraphStyle
    }()
    
    static let menuInsets = UIEdgeInsets(top: 12, left: 18, bottom: 12, right: 18)
    static let menuItemSize = CGSize(width: 80, height: 80)
    static var menuHeight: CGFloat {
        return menuItemSize.height + menuInsets.top + menuInsets.bottom
    }
    static let maxChange: CGFloat = menuItemSize.height / 2
    static var minHeight: CGFloat {
        return menuItemSize.height - maxChange + menuInsets.top + menuInsets.bottom
    }
    static func calculateMenuHeight(for scrollView: UIScrollView) -> CGFloat {
        guard scrollView.contentOffset.y > -menuHeight else { return menuHeight }
        let offset = min(maxChange, scrollView.contentOffset.y + menuHeight) / maxChange
        let height = menuHeight - (offset * maxChange)
        return height
    }
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.layer.cornerRadius = 6
        contentView.clipsToBounds = true
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
        contentView.constrainToEdges(imageView)
        contentView.constrainToEdges(titleLabel)
        contentView.layer.borderWidth = 2
        self.contentView.layer.borderColor = isSelected ? Theme.themify(key: .borderSecondary).cgColor : Theme.themify(key: .transparent).cgColor
    }
  
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  
    override func setPagingItem(_ pagingItem: PagingItem, selected: Bool, options: PagingOptions) {
        let item = pagingItem as! ImagePagingModel
        if let url = item.headerImageURL {
            Nuke.loadImage(with: url, into: imageView)
        }
        titleLabel.attributedText = NSAttributedString(
            string: item.title,
            attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle]
        )
        titleLabel.alpha = selected ? 0 : 1
        imageView.transform = selected ? CGAffineTransform(scaleX: 1 + 0.5, y: 1 + 0.5) : CGAffineTransform.identity
        self.contentView.layer.borderColor = selected ? Theme.themify(key: .borderSecondary).cgColor : Theme.themify(key: .transparent).cgColor
    }
      
    open override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        if let attributes = layoutAttributes as? PagingCellLayoutAttributes {
            let scale = 1 + (0.5 * attributes.progress)
            imageView.transform = CGAffineTransform(scaleX: scale, y: scale)
            titleLabel.alpha = 1 - (1 * attributes.progress)
        }
        guard isSelected else {
            self.contentView.layer.borderColor = Theme.themify(key: .transparent).cgColor
            return
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.layer.borderColor = Theme.themify(key: .borderSecondary).cgColor
        })
    }
  
}
