//
//  MediaCollectionViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit
import Nuke

class MediaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .scaleAspectFill
        }
    }
    public var repository: FileEntity = FileEntity()
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 2
        self.layer.shadowColor = Theme.themify(key: .shadow).cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
    }
    
    func setRepository(_ repository: FileEntity) {
        self.repository = repository
        
        if repository.format.content_type.is_movie {
            if let image = repository.url_image {
                imageView.image = image
            } else if let url = URL(string: repository.thumbnail) {
                Nuke.loadImage(with: url, into: imageView)
            }
        }
        
        if repository.format == .youtube {
            if let url = URL(string: repository.thumbnail) {
                Nuke.loadImage(with: url, into: imageView)
            }
        }
        
        if repository.format.content_type.is_image {
            if let image = repository.url_image {
                imageView.image = image
            } else {
                imageView.image = UIImage(url: repository.url)
            }
        }
    }
}
