//
//  StreamableInfoTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/03.
//

import Foundation
import UIKit
import Nuke

class StreamableInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sizeCountLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var topBorderView: UIView! {
        didSet {
            topBorderView.backgroundColor = Theme.themify(key: .borderSecondary)
        }
    }
    @IBOutlet weak var bottomBorderView: UIView! {
        didSet {
            bottomBorderView.backgroundColor = Theme.themify(key: .backgroundThick).darker(by: 5)
        }
    }
    public var repository: StreamableEntity = StreamableEntity()
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    func setRepository(_ repository: StreamableEntity) {
        self.repository = repository
        if let file = repository as? FileEntity {
            self.nameLabel.text = file.name
            self.sizeCountLabel.text = file.size
            self.dateLabel.text = formatter.string(from: file.updated_at)
        } else if let reference = repository as? FileReferenceEntity, let file = reference.File {
            self.nameLabel.text = file.name
            self.sizeCountLabel.text = file.size
            self.dateLabel.text = formatter.string(from: file.updated_at)
        }
    }
}
