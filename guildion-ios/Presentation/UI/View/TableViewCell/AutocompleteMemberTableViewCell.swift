//
//  AutocompleteMemberTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/29.
//

import Foundation
import UIKit
import Nuke

class AutocompleteMemberTableViewCell: UITableViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.layer.cornerRadius = profileImageView.frame.width / 2
            profileImageView.clipsToBounds = true
        }
    }
    
    public var repository: MemberEntity = MemberEntity()
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .background)
        self.backgroundColor = Theme.themify(key: .background)
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .background).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
    
    public func setRepository(_ repository: MemberEntity) {
        self.repository = repository
        self.nameLabel.text = repository.nickname
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: profileImageView)
        }
    }
}
