//
//  PlaylistPlayTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/03.
//

import Foundation
import UIKit

protocol PlaylistPlayTableViewCellDelegate: class {
    func playlistPlayTableViewCell(onTapAdd playlistPlayTableViewCell: PlaylistPlayTableViewCell)
    func playlistPlayTableViewCell(onTapPlay playlistPlayTableViewCell: PlaylistPlayTableViewCell)
}

class PlaylistPlayTableViewCell: UITableViewCell {
    @IBOutlet weak var addButton: SimpleButton! {
        didSet {
            addButton.normalColor = Theme.themify(key: .cancel)
            addButton.canceled = false
            addButton.textColor = Theme.themify(key: .string)
            addButton.text = R.string.localizable.add()
            addButton.iconImageView.image = R.image.plus()?.withRenderingMode(.alwaysTemplate)
            addButton.isIconShow = true
            addButton.delegate = self
        }
    }
    @IBOutlet weak var playButton: SimpleButton! {
        didSet {
            playButton.normalColor = Theme.themify(key: .main)
            playButton.canceled = false
            playButton.textColor = Theme.themify(key: .string)
            playButton.text = R.string.localizable.play()
            playButton.iconImageView.image = R.image.play()?.withRenderingMode(.alwaysTemplate)
            playButton.isIconShow = true
            playButton.delegate = self
        }
    }
    weak var delegate: PlaylistPlayTableViewCellDelegate?
    static let cellHeight: CGFloat = 68
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
        self.backgroundColor = Theme.themify(key: .backgroundThick)
    }
}

extension PlaylistPlayTableViewCell: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        switch simpleButton {
        case addButton: self.delegate?.playlistPlayTableViewCell(onTapAdd: self)
        case playButton: self.delegate?.playlistPlayTableViewCell(onTapPlay: self)
        default: break
        }
    }
}
