//
//  StreamChannelTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/10.
//

import Foundation
import UIKit
import BadgeSwift

protocol StreamChannelTableViewCellDelegate: class {
}

class StreamChannelTableViewCell: UITableViewCell {
    @IBOutlet weak var counterView: StreamChannelCounterView!
    @IBOutlet weak var counterViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var badgeView: BadgeSwift! {
        didSet {
            badgeView.badgeColor = Theme.themify(key: .badge)
            badgeView.textColor = .white
            badgeView.isHidden = true
        }
    }
    @IBInspectable var badgeText: String? {
        didSet {
            badgeView.text = badgeText
        }
    }
    @IBOutlet weak var leadImage: UIImageView! {
        didSet {
            leadImage.image = R.image.watchParty()!.withRenderingMode(.alwaysTemplate)
            leadImage.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    public var notificated: Bool = false {
        didSet {
            badgeView.isHidden = !notificated
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + StreamChannelTableViewCell.sideMargin,
                y: newFrame.minY + StreamChannelTableViewCell.rowMargin,
                width: newFrame.width - (StreamChannelTableViewCell.sideMargin * 2),
                height: newFrame.height - (StreamChannelTableViewCell.rowMargin * 2)
            )
        }
    }
    
    public weak var delegate: StreamChannelTableViewCellDelegate?
    
    public var repository: StreamChannelEntity = StreamChannelEntity()
    
    public func setRepository(_ repository: StreamChannelEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                self.nameLabel.text = repository.name
                self.leadImage.image = repository.channelable_type.image
                self.notificated = repository.unread_count > 0
                self.badgeText = "\(repository.unread_count)"
                self.counterViewRightConstraint.constant = self.notificated ? 10 : -16
                self.counterView.setRepository(repository)
            }
        }
    }
    
    func updateUnRead(_ repository: StreamChannelEntity) {
        repository.Messages?.getUnReadCount().then(in: .main, {
            self.notificated = $0 > 0
            self.badgeText = "\($0)"
        })
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
}

