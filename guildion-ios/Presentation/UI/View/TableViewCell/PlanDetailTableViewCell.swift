//
//  PlanDetailTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit

class PlanDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var planTitleLabel: UILabel!
    @IBOutlet weak var planDetailLabel: UILabel!
    var repository: SubscriptionPlanModel = SubscriptionPlanModel()
    var planFunction: PlanConfig.PlanFunction = .count
    
    func setRepository(_ repository: SubscriptionPlanModel, at: PlanConfig.PlanFunction) {
        self.repository = repository
        planTitleLabel.text = at.string
        planDetailLabel.text = repository.plan.functionString(function: at)
    }
}
