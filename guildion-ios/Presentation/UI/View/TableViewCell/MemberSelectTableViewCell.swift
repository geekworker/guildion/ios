//
//  MemberSelectTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/13.
//

import Foundation
import UIKit
import Nuke

class MemberSelectTableViewCell: UITableViewCell {
    @IBOutlet weak var memberImageView: UIImageView! {
        didSet {
            memberImageView.layer.cornerRadius = memberImageView.frame.width / 2
            memberImageView.backgroundColor = Theme.themify(key: .backgroundThick)
            memberImageView.clipsToBounds = true
            memberImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var checkImageView: UIImageView! {
        didSet {
            checkImageView.image = UIImage()
            checkImageView.layer.cornerRadius = checkImageView.frame.width / 2
            checkImageView.layer.borderWidth = 1
            checkImageView.layer.borderColor = Theme.themify(key: .border).cgColor
            checkImageView.backgroundColor = Theme.themify(key: .backgroundThick)
            checkImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
            nameLabel.font = Font.FT(size: .FM, family: .B)
        }
    }
    @IBOutlet weak var activeView: ActiveView! {
        didSet {
            activeView.baseView.layer.cornerRadius = activeView.frame.width / 2
            activeView.baseView.layer.borderWidth = 0.5
            activeView.baseView.layer.borderColor = Theme.themify(key: .border).cgColor
            activeView.baseView.clipsToBounds = true
        }
    }
    var isInEditingMode: Bool = false {
        didSet {
//             checkImageView.isHidden = !isInEditingMode
        }
    }

    override var isSelected: Bool {
        didSet {
//            if isInEditingMode {
//                checkImageView.image = isSelected ? R.image.success()!.withRenderingMode(.alwaysOriginal) : UIImage()
//            } else {
//                checkImageView.image = UIImage()
//            }
        }
    }
    public var repository: MemberEntity = MemberEntity()
    
    override func awakeFromNib() {
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .transparent)
        self.selectedBackgroundView = bgColorView
    }
    
    public func setRepository(_ repository: MemberEntity) {
        self.isInEditingMode = false
        self.checkImageView.image = UIImage()
        self.repository = repository
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: memberImageView)
        }
        
        self.nameLabel.text = repository.nickname == "" ? repository.User?.nickname ?? "" : repository.nickname
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        if !editing {
            self.isSelected = false
        }
        self.isInEditingMode = editing
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // checkImageView.image = selected ? R.image.success()!.withRenderingMode(.alwaysOriginal) : UIImage()
//        if isInEditingMode {
//            checkImageView.image = selected ? R.image.success()!.withRenderingMode(.alwaysOriginal) : UIImage()
//        } else {
//            checkImageView.image = UIImage()
//        }
    }
}
