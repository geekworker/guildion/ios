//
//  TextChannelTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/10.
//

import Foundation
import UIKit
import BadgeSwift

protocol TextChannelTableViewCellDelegate: class {
}

class TextChannelTableViewCell: UITableViewCell {
    @IBOutlet weak var badgeView: BadgeSwift! {
        didSet {
            badgeView.badgeColor = Theme.themify(key: .badge)
            badgeView.textColor = .white
            badgeView.isHidden = true
        }
    }
    @IBInspectable var badgeText: String? {
        didSet {
            badgeView.text = badgeText
        }
    }
    @IBOutlet weak var bigLeadImageView: UIImageView! {
        didSet {
            bigLeadImageView.isHidden = true
            bigLeadImageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var leadImage: UIImageView! {
        didSet {
            leadImage.image = R.image.hash_tag()!.withRenderingMode(.alwaysTemplate)
            leadImage.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    public var notificated: Bool = false {
        didSet {
            badgeView.isHidden = !notificated
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + TextChannelTableViewCell.sideMargin,
                y: newFrame.minY + TextChannelTableViewCell.rowMargin,
                width: newFrame.width - (TextChannelTableViewCell.sideMargin * 2),
                height: newFrame.height - (TextChannelTableViewCell.rowMargin * 2)
            )
        }
    }
    
    public weak var delegate: TextChannelTableViewCellDelegate?
    
    public var repository: TextChannelEntity = TextChannelEntity()
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
    
    public func setRepository(_ repository: TextChannelEntity) {
        self.leadImage.image = repository.channelable_type.image
        self.bigLeadImageView.image = repository.channelable_type.image
        self.leadImage.isHidden = repository.channelable_type != .TextChannel
        self.bigLeadImageView.isHidden = repository.channelable_type != .AnnouncementChannel
        self.repository = repository
        self.nameLabel.text = repository.name
        self.notificated = repository.unread_count > 0
        self.badgeText = "\(repository.unread_count)"
    }
    
    fileprivate func updateUnRead(_ repository: TextChannelEntity) {
        repository.Messages?.getUnReadCount().then(in: .main, {
            self.notificated = $0 > 0
            self.badgeText = "\($0)"
        })
    }
}
