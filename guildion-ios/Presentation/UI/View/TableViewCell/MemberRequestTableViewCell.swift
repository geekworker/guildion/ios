//
//  MemberRequestTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Nuke
import FRHyperLabel
import BadgeSwift

protocol MemberRequestTableViewCellDelegate: class {
    func memberRequestTableViewCell(onTap memberRequestTableViewCell: MemberRequestTableViewCell)
    func memberRequestTableViewCell(toProfile memberRequestTableViewCell: MemberRequestTableViewCell)
    func memberRequestTableViewCell(willAccept repository: MemberRequestEntity, memberRequestTableViewCell: MemberRequestTableViewCell)
    func memberRequestTableViewCell(willDeny repository: MemberRequestEntity, memberRequestTableViewCell: MemberRequestTableViewCell)
}

class MemberRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImage: UIImageView! {
        didSet {
            self.profileImage.layer.cornerRadius = self.profileImage.bounds.width / 2
            self.profileImage.clipsToBounds = true
            self.profileImage.contentMode = .scaleAspectFill
            self.profileImage.isUserInteractionEnabled = true
            self.profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapProfile(_:))))
        }
    }
    @IBOutlet weak var label: FRHyperLabel! {
        didSet {
            label.textColor = Theme.themify(key: .string)
            label.font = Font.FT(size: .FM, family: .L)
        }
    }
    @IBOutlet weak var acceptButton: SimpleButton! {
        didSet {
            acceptButton.canceled = false
            acceptButton.normalColor = Theme.themify(key: .main)
            acceptButton.text = R.string.localizable.guildMemberRequestAccept()
            acceptButton.delegate = self
        }
    }
    @IBOutlet weak var denyButton: SimpleButton! {
        didSet {
            denyButton.canceled = false
            denyButton.normalColor = Theme.themify(key: .cancel)
            denyButton.text = R.string.localizable.guildMemberRequestDeny()
            denyButton.delegate = self
        }
    }
    @IBOutlet weak var badgeView: UIView! {
        didSet {
            badgeView.isHidden = true
        }
    }
    public var repository: MemberRequestEntity = MemberRequestEntity()
    public weak var delegate: MemberRequestTableViewCellDelegate!
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .background)
    }
    
    public func setRepository(_ repository: MemberRequestEntity) {
        self.repository = repository
        self.isHidden = repository.is_accepted
        guard let user = repository.Voter, !repository.is_accepted else { return }
        label.text = R.string.localizable.guildMemberRequest(user.nickname)
        label.setLinkForSubstring(user.nickname, withLinkHandler: { label, string in
            self.delegate.memberRequestTableViewCell(toProfile: self)
        })
        acceptButton.text = R.string.localizable.guildMemberRequestAccept()
        denyButton.text = R.string.localizable.guildMemberRequestDeny()
        if let url = URL(string: user.picture_small) {
            Nuke.loadImage(with: url, into: self.profileImage)
        }
    }
    
    public func updateLayout() {
        self.isHidden = repository.is_accepted
        guard let user = repository.Voter, !repository.is_accepted else { return }
        label.text = R.string.localizable.guildMemberRequest(user.nickname)
        label.setLinkForSubstring(user.nickname, withLinkHandler: { label, string in
            self.delegate.memberRequestTableViewCell(toProfile: self)
        })
        acceptButton.text = R.string.localizable.guildMemberRequestAccept()
        denyButton.text = R.string.localizable.guildMemberRequestDeny()
        if let url = URL(string: user.picture_small) {
            Nuke.loadImage(with: url, into: self.profileImage)
        }
    }
    
    @objc func tapProfile(_ sender: Any) {
        self.delegate.memberRequestTableViewCell(toProfile: self)
    }
}

extension MemberRequestTableViewCell: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        switch simpleButton {
        case acceptButton: self.delegate.memberRequestTableViewCell(willAccept: repository, memberRequestTableViewCell: self)
        case denyButton: self.delegate.memberRequestTableViewCell(willDeny: repository, memberRequestTableViewCell: self)
        default: break
        }
    }
}
