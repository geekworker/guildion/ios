//
//  StreamTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit

protocol StreamTableViewCellDelegate: class {
}

class StreamTableViewCell: UITableViewCell {
    @IBOutlet weak var leadImage: UIImageView! {
        didSet {
            leadImage.image = R.image.play()!.withRenderingMode(.alwaysTemplate)
            leadImage.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + StreamChannelTableViewCell.sideMargin,
                y: newFrame.minY + StreamChannelTableViewCell.rowMargin,
                width: newFrame.width - (StreamChannelTableViewCell.sideMargin * 2),
                height: newFrame.height - (StreamChannelTableViewCell.rowMargin * 2)
            )
        }
    }
    public var repository: StreamEntity = StreamEntity()
    public weak var delegate: StreamTableViewCellDelegate?
    
    public func setRepository(_ repository: StreamEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                switch repository.mode {
                case .fileYouTube:
                    self.leadImage.image = repository.File?.format.iconImage
                    self.nameLabel.text = repository.File!.name
                case .fileMovie:
                    self.leadImage.image = repository.File?.format.iconImage
                    self.nameLabel.text = repository.File!.name
                    self.leadImage.tintColor = Theme.themify(key: .mainLight)
                case .fileReferenceYouTube:
                    self.leadImage.image = repository.FileReference?.File?.format.iconImage
                    self.nameLabel.text = repository.FileReference!.File!.name
                case .fileReferenceMovie:
                    self.leadImage.image = repository.FileReference?.File?.format.iconImage
                    self.nameLabel.text = repository.FileReference!.File!.name
                    self.leadImage.tintColor = Theme.themify(key: .mainLight)
                default: break
                }
            }
        }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
}
