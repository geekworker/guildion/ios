//
//  FolderTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit
import Nuke

protocol FolderTableViewCellDelegate: class {
}

class FolderTableViewCell: UITableViewCell {
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.isHidden = true
            overView.isUserInteractionEnabled = false
            overView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.backgroundColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var collectionContainerView: UIView! {
        didSet {
            collectionContainerView.backgroundColor = Theme.themify(key: .transparent)
        }
    }
    @IBOutlet weak var chevronImageView: UIImageView! {
        didSet {
            chevronImageView.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            chevronImageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var collectionContainerViewHeightConstraint: NSLayoutConstraint!
    public var mediaCollectionView: MediaCollectionView!
    static let height: CGFloat = 50
    public var repository: FolderEntity = FolderEntity()
    public weak var delegate: FolderTableViewCellDelegate?
    public var all_files: Bool = false
    
    override func awakeFromNib() {
        self.configureCollectionView()
        let longpress = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.hover(_:))
        )
        longpress.minimumPressDuration = 0.01
        longpress.cancelsTouchesInView = false
        longpress.delegate = self
        contentView.addGestureRecognizer(longpress)
    }
    
    public func setRepository(_ repository: FolderEntity) {
        self.repository = repository
        self.nameLabel.text = all_files ? R.string.localizable.allFiles() : repository.name
        guard let refrerences = repository.FileReferences else { return }
        let fileEntities = FileEntities()
        fileEntities.items = refrerences.compactMap({ $0.File }).reversed()
        self.mediaCollectionView.setRepositories(fileEntities)
        self.mediaCollectionView.isScrollEnabled = false
    }
    
    @objc func onTap(_ sender: Any) {
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension FolderTableViewCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension FolderTableViewCell: UICollectionViewDelegate {
    public func configureCollectionView() {
        self.mediaCollectionView = MediaCollectionView.configure(frame: CGRect(x: 0, y: 0, width: self.collectionContainerView.frame.width, height: collectionContainerView.frame.height))
        self.collectionContainerView.addSubview(self.mediaCollectionView)
        self.collectionContainerView.constrainToEdges(self.mediaCollectionView)
        self.mediaCollectionView.configure()
        self.mediaCollectionView.semanticContentAttribute = .forceRightToLeft
        self.mediaCollectionView.delegate = self
        self.mediaCollectionView.dataSource = self
        self.mediaCollectionView.prefetchDataSource = self
    }
}

extension FolderTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaCollectionView.collectionView(collectionView, numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.mediaCollectionView.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
}

extension FolderTableViewCell: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        self.mediaCollectionView.collectionView(collectionView, prefetchItemsAt: indexPaths)
    }
}
