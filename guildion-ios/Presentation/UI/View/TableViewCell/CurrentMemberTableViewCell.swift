//
//  CurrentMemberTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/04.
//

import Foundation
import UIKit
import Nuke

protocol CurrentMemberTableViewCellDelegate: class {
    func currentMemberTableViewCell(onTap repository: MemberEntity, currentMemberTableViewCell: CurrentMemberTableViewCell)
}

extension CurrentMemberTableViewCellDelegate {
    func currentMemberTableViewCell(onTap repository: MemberEntity, currentMemberTableViewCell: CurrentMemberTableViewCell) {}
}

class CurrentMemberTableViewCell: UITableViewCell {
    @IBOutlet weak var leadImage: UIImageView! {
        didSet {
            leadImage.layer.cornerRadius = leadImage.frame.width / 2
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
            nameLabel.text = ""
        }
    }
    @IBOutlet weak var badgeImage: UIView! {
        didSet {
            badgeImage.layer.cornerRadius = badgeImage.frame.width / 2
            badgeImage.clipsToBounds = true
            badgeImage.backgroundColor = Theme.themify(key: .backgroundContrast)
            badgeImage.isHidden = !notificated
        }
    }
    
    
    public var notificated: Bool = false {
        didSet {
            badgeImage.isHidden = !notificated
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + CurrentMemberTableViewCell.sideMargin,
                y: newFrame.minY + CurrentMemberTableViewCell.rowMargin,
                width: newFrame.width - (CurrentMemberTableViewCell.sideMargin * 2),
                height: newFrame.height - (CurrentMemberTableViewCell.rowMargin * 2)
            )
        }
    }
    
    public var repository: MemberEntity = MemberEntity()
    public weak var delegate: MembersTableViewCellDelegate?
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
    
    func setRepository(_ repository: MemberEntity) {
        DispatchQueue.main.async { [unowned self] in
            self.repository = repository
            self.nameLabel.text = repository.nickname
            if let url = URL(string: repository.picture_small) {
                Nuke.loadImage(with: url, into: leadImage)
            }
        }
    }
}
