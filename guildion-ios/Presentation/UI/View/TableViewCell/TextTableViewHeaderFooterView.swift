//
//  TextTableViewHeaderFooterView.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit

class TextTableViewHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = Font.FT(size: .FM, family: .L)
        }
    }
    static var height: CGFloat = 32
    
    override func awakeFromNib() {
    }
}
