//
//  NavigationTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import BadgeSwift

protocol NavigationTableViewCellDelegate: class {
    func navigationTableViewCell(onTap navigationTableViewCell: NavigationTableViewCell)
}

class NavigationTableViewCell: UITableViewCell {
    @IBOutlet weak var badgeView: BadgeSwift! {
        didSet {
            badgeView.badgeColor = Theme.themify(key: .badge)
            badgeView.textColor = .white
            badgeView.isHidden = true
        }
    }
    @IBInspectable var badgeText: String? {
        didSet {
            badgeView.text = badgeText
            badgeView.isHidden = badgeText == nil || badgeText == ""
            titleLabelRightConstraint.constant = badgeText == nil || badgeText == "" ? 12 : 30
        }
    }
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .border)
            baseView.layer.cornerRadius = 5
            baseView.clipsToBounds = true
            baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            baseView.addGestureRecognizer(longpress)
        }
    }
    @IBOutlet weak var titleLabelRightConstraint: NSLayoutConstraint! {
        didSet {
            titleLabelRightConstraint.constant = 12 // 30
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.isUserInteractionEnabled = false
            overView.isHidden = true
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
            overView.alpha = 0.3
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var titleLabelLeftConstraint: NSLayoutConstraint! {
        didSet {
            titleLabelLeftConstraint.constant = 0 // 12
        }
    }
    @IBOutlet weak var chevronImageView: UIImageView! {
        didSet {
            chevronImageView.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            chevronImageView.tintColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var logoImageView: UIImageView! {
        didSet {
            logoImageView.isHidden = true
        }
    }
    @IBInspectable var logoImage: UIImage? {
        didSet {
            logoImageView.image = logoImage
            logoImageView.isHidden = logoImage == nil
            logoImageViewWidthConstraint.constant = logoImage == nil ? 0 : 20
            titleLabelLeftConstraint.constant = logoImage == nil ? 0 : 12
        }
    }
    @IBOutlet weak var logoImageViewWidthConstraint: NSLayoutConstraint! {
        didSet {
            logoImageViewWidthConstraint.constant = 0 // 20
        }
    }
    @IBOutlet weak var topInset: NSLayoutConstraint!
    @IBOutlet weak var bottomInset: NSLayoutConstraint!
    @IBOutlet weak var leftInset: NSLayoutConstraint!
    @IBOutlet weak var rightInset: NSLayoutConstraint!
    public weak var delegate: NavigationTableViewCellDelegate?
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.navigationTableViewCell(onTap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension NavigationTableViewCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
