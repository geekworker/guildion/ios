//
//  ParticipantTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/11.
//

import Foundation
import UIKit
import Nuke

protocol ParticipantTableViewCellDelegate: class {
}

class ParticipantTableViewCell: UITableViewCell {
    @IBOutlet weak var leadImage: UIImageView! {
        didSet {
            leadImage.tintColor = Theme.themify(key: .stringSecondary)
            leadImage.layer.cornerRadius = leadImage.frame.width / 2
            leadImage.clipsToBounds = true
            leadImage.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + StreamChannelTableViewCell.sideMargin,
                y: newFrame.minY + StreamChannelTableViewCell.rowMargin,
                width: newFrame.width - (StreamChannelTableViewCell.sideMargin * 2),
                height: newFrame.height - (StreamChannelTableViewCell.rowMargin * 2)
            )
        }
    }
    public var repository: ParticipantEntity = ParticipantEntity()
    public weak var delegate: ParticipantTableViewCellDelegate?
    
    public func setRepository(_ repository: ParticipantEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                guard self.repository.id != repository.id else { return }
                self.repository = repository
                self.nameLabel.textColor = Theme.themify(key: .stringSecondary)
                self.leadImage.layer.borderColor = Theme.themify(key: .transparent).cgColor
                if let member = repository.Member, let user = member.User {
                    self.nameLabel.text = member.nickname != "" ? member.nickname : user.nickname
                    
                    if user == CurrentUser.getCurrentUserEntity() {
                        self.nameLabel.textColor = Theme.themify(key: .main)
                        self.leadImage.layer.borderColor = Theme.themify(key: .main).cgColor
                    }
                    
                    if let url = URL(string: member.picture_small) {
                        Nuke.loadImage(with: url, into: self.leadImage)
                    } else if let url = URL(string: user.picture_small) {
                        Nuke.loadImage(with: url, into: self.leadImage)
                    }
                }
            }
        }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
}

