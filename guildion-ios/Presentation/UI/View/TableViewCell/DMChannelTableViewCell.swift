//
//  DMChannelTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/10.
//

import Foundation
import UIKit
import BadgeSwift

protocol DMChannelTableViewCellDelegate: class {
}

class DMChannelTableViewCell: UITableViewCell {
    @IBOutlet weak var badgeView: BadgeSwift! {
        didSet {
            badgeView.badgeColor = Theme.themify(key: .badge)
            badgeView.textColor = .white
            badgeView.isHidden = true
        }
    }
    @IBInspectable var badgeText: String? {
        didSet {
            badgeView.text = badgeText
        }
    }
    @IBOutlet weak var leadView: UIView! {
        didSet {
            leadView.layer.cornerRadius = 2
            leadView.clipsToBounds = true
            leadView.backgroundColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var countLabel: UILabel! {
        didSet {
            countLabel.textColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    public var notificated: Bool = false {
        didSet {
            badgeView.isHidden = !notificated
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + DMChannelTableViewCell.sideMargin,
                y: newFrame.minY + DMChannelTableViewCell.rowMargin,
                width: newFrame.width - (DMChannelTableViewCell.sideMargin * 2),
                height: newFrame.height - (DMChannelTableViewCell.rowMargin * 2)
            )
        }
    }
    
    public weak var delegate: DMChannelTableViewCellDelegate?
    public var repository: DMChannelEntity = DMChannelEntity()
    
    public func setRepository(_ repository: DMChannelEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                self.nameLabel.text = repository.name
                self.countLabel.text = "\(repository.member_count)"
                self.notificated = repository.unread_count > 0
                self.badgeText = "\(repository.unread_count)"
            }
        }
    }
    
    func updateUnRead(_ repository: DMChannelEntity) {
        repository.Messages?.getUnReadCount().then(in: .main, {
            self.notificated = $0 > 0
            self.badgeText = "\($0)"
        })
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
}

