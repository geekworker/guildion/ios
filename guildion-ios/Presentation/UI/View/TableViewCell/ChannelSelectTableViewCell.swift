//
//  ChannelSelectTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class ChannelSelectTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var channelImageView: UIImageView! {
        didSet {
            channelImageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var streamChannelImageView: UIImageView! {
        didSet {
            streamChannelImageView.tintColor = Theme.themify(key: .stringSecondary)
            streamChannelImageView.isHidden = true
        }
    }
    @IBOutlet weak var checkmarkImageView: UIImageView! {
        didSet {
            checkmarkImageView.isHidden = true
            checkmarkImageView.image = R.image.checkmark()!.withRenderingMode(.alwaysTemplate)
            checkmarkImageView.tintColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.backgroundColor = Theme.themify(key: .border)
        }
    }
    @IBInspectable var checked: Bool = false {
        didSet {
            checkmarkImageView.isHidden = !checked
        }
    }
    static let cellHeight: CGFloat = 44
    
    override func awakeFromNib() {
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .background).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
}
