//
//  FoldersTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/23.
//

import Foundation
import UIKit

protocol FoldersTableViewCellDelegate: class {
}

class FoldersTableViewCell: UITableViewCell {
    @IBOutlet weak var leadImage: UIImageView! {
        didSet {
            leadImage.image = R.image.insertPicture()!.withRenderingMode(.alwaysTemplate)
            leadImage.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
            nameLabel.text = R.string.localizable.folders()
        }
    }
    @IBOutlet weak var badgeImage: UIView! {
        didSet {
            badgeImage.layer.cornerRadius = badgeImage.frame.width / 2
            badgeImage.clipsToBounds = true
            badgeImage.backgroundColor = Theme.themify(key: .backgroundContrast)
            badgeImage.isHidden = !notificated
        }
    }
    
    public var notificated: Bool = false {
        didSet {
            badgeImage.isHidden = !notificated
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + FoldersTableViewCell.sideMargin,
                y: newFrame.minY + FoldersTableViewCell.rowMargin,
                width: newFrame.width - (FoldersTableViewCell.sideMargin * 2),
                height: newFrame.height - (FoldersTableViewCell.rowMargin * 2)
            )
        }
    }
    
    public weak var delegate: FoldersTableViewCellDelegate?
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
}
