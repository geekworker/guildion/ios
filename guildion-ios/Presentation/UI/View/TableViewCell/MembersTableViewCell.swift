//
//  MembersTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit

protocol MembersTableViewCellDelegate: class {
}

class MembersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leadImage: UIImageView! {
        didSet {
            leadImage.image = R.image.members()!.withRenderingMode(.alwaysTemplate)
            leadImage.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
            nameLabel.text = ""
        }
    }
    @IBOutlet weak var badgeImage: UIView! {
        didSet {
            badgeImage.layer.cornerRadius = badgeImage.frame.width / 2
            badgeImage.clipsToBounds = true
            badgeImage.backgroundColor = Theme.themify(key: .backgroundContrast)
            badgeImage.isHidden = !notificated
        }
    }
    
    
    public var notificated: Bool = false {
        didSet {
            badgeImage.isHidden = !notificated
            nameLabel.textColor = notificated ? Theme.themify(key: .backgroundContrast) : Theme.themify(key: .stringSecondary)
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 32 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + MembersTableViewCell.sideMargin,
                y: newFrame.minY + MembersTableViewCell.rowMargin,
                width: newFrame.width - (MembersTableViewCell.sideMargin * 2),
                height: newFrame.height - (MembersTableViewCell.rowMargin * 2)
            )
        }
    }
    
    public var repository: GuildEntity = GuildEntity()
    public weak var delegate: MembersTableViewCellDelegate?
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
    
    func setRepository(_ repository: GuildEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                self.nameLabel.text = "\(repository.active_count) \(R.string.localizable.online()), \(repository.member_count) \(R.string.localizable.members())"
            }
        }
    }
}

