//
//  AutocompleteChannelTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/29.
//

import Foundation
import UIKit

class AutocompleteChannelTableViewCell: UITableViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.tintColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    public var repository: ChannelableProtocol = TextChannelEntity()
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
    
    public func setRepository(_ repository: ChannelableProtocol) {
        self.repository = repository
        self.nameLabel.text = repository.name
        self.iconImageView.image = repository.channelable_type.image
    }
}
