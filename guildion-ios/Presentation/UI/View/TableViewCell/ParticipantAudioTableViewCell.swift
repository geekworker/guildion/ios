//
//  ParticipantAudioTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/21.
//

import Foundation
import UIKit
import Nuke

protocol ParticipantAudioTableViewCellDelegate: class {
    func participantAudioTableViewCell(onChangeVolume value: Float, in participantAudioTableViewCell: ParticipantAudioTableViewCell)
}

class ParticipantAudioTableViewCell: UITableViewCell {
    @IBOutlet weak var memberImageView: UIImageView! {
        didSet {
            memberImageView.contentMode = .scaleAspectFill
            memberImageView.layer.cornerRadius = memberImageView.frame.width / 2
            memberImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var volumeLabel: UILabel! {
        didSet {
            volumeLabel.text = R.string.localizable.streamSoundVolume()
            volumeLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var volumeSlider: UISlider! {
        didSet {
            volumeSlider.maximumValue = 1
            volumeSlider.tintColor = Theme.themify(key: .main)
            volumeSlider.value = 0
            volumeSlider.isContinuous = true
        }
    }
    
    var repository: ParticipantEntity = ParticipantEntity()
    weak var delegate: ParticipantAudioTableViewCellDelegate?
    
    func setRepository(_ repository: ParticipantEntity, volume: Float) {
        self.repository = repository
        self.nameLabel.text = repository.Member?.nickname
        self.volumeSlider.value = volume
        if let picture = repository.Member?.picture_small, let url = URL(string: picture) {
            Nuke.loadImage(with: url, into: memberImageView)
        } else {
            memberImageView.image = nil
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        self.delegate?.participantAudioTableViewCell(onChangeVolume: sender.value, in: self)
    }
}
