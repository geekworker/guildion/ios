//
//  StreamAudioPortTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/21.
//

import Foundation
import UIKit
import AVFoundation
import AVKit

class StreamAudioPortTableViewCell: UITableViewCell {
    @IBOutlet weak var audioImageView: UIImageView! {
        didSet {
            audioImageView.contentMode = .scaleAspectFit
            audioImageView.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var settingLabel: UILabel! {
        didSet {
            self.settingLabel.text = R.string.localizable.setting()
            settingLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    lazy var avRoutePickerView: AVRoutePickerView = {
        let pickerView = AVRoutePickerView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        self.routePickerView.addSubview(pickerView)
        return pickerView
    }()
    @IBOutlet weak var routePickerView: UIView! {
        didSet {
            self.routePickerView.addSubview(AVRoutePickerView(frame: self.routePickerView.frame))
        }
    }
    @IBOutlet weak var chevronImageView: UIImageView! {
        didSet {
            chevronImageView.image = R.image.chevron()?.withRenderingMode(.alwaysTemplate)
            chevronImageView.tintColor = Theme.themify(key: .stringSecondary)
            chevronImageView.contentMode = .scaleAspectFit
        }
    }
    static let cellHeight: CGFloat = 60
    
    func updateLayout() {
        guard let current_port = AVAudioSession.sharedInstance().currentRoute.inputs.first else { return }
        self.nameLabel.text = current_port.portName
        self.audioImageView.image = audioImage(port: current_port.portType)
    }
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onRouteChangeNotification(_:)), name: AVAudioSession.routeChangeNotification, object: nil)
        print(avRoutePickerView)
        updateLayout()
    }
    
    @objc func onRouteChangeNotification(_ notification: NSNotification) {
        guard let current_port = AVAudioSession.sharedInstance().currentRoute.inputs.first else { return }
        DispatchQueue.main.async {
            self.nameLabel.text = current_port.portName
            self.audioImageView.image = self.audioImage(port: current_port.portType)
        }
    }
}

extension StreamAudioPortTableViewCell: AudioActionSheetProtocol {
}
