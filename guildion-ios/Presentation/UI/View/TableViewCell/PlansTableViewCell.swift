//
//  PlansTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit
import DropDown
import BadgeSwift

protocol PlansTableViewCellDelegate: class {
    func plansTableViewCell(toBilling repository: SubscriptionPlanModel, in plansTableViewCell: PlansTableViewCell)
    func plansTableViewCell(sync repository: SubscriptionPlanModel, in plansTableViewCell: PlansTableViewCell)
    func plansTableViewCell(shouldDismiss plansTableViewCell: PlansTableViewCell)
}

class PlansTableViewCell: UITableViewCell {
    @IBOutlet weak var planSelectLabel: UILabel! {
        didSet {
            planSelectLabel.text = R.string.localizable.planSelect()
        }
    }
    @IBOutlet weak var planSelectButton: BorderButton! {
        didSet {
            planSelectButton.setTitle(R.string.localizable.planMini(), for: .normal)
            planSelectButton.setTitle(R.string.localizable.planMini(), for: .highlighted)
            planSelectButton.addTarget(self, action: #selector(choosePlan(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var planItem1mo: PlanItem! {
        didSet {
            planItem1mo.delegate = self
        }
    }
    @IBOutlet weak var planItem3mo: PlanItem! {
        didSet {
            planItem3mo.delegate = self
        }
    }
    @IBOutlet weak var planItem12mo: PlanItem! {
        didSet {
            planItem12mo.delegate = self
        }
    }
    @IBOutlet weak var planDetailLabel: UILabel! {
        didSet {
            planDetailLabel.text = R.string.localizable.planTrialAfter()
        }
    }
    @IBOutlet weak var simpleButton: SimpleButton! {
        didSet {
            simpleButton.text = R.string.localizable.planBuy()
            simpleButton.textColor = .white
            simpleButton.normalColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var dismissLabel: UILabel! {
        didSet {
            dismissLabel.text = R.string.localizable.later()
            dismissLabel.textColor = Theme.themify(key: .string)
            dismissLabel.isUserInteractionEnabled = true
            dismissLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapDismissLabel(_:))))
        }
    }
    public var repository: SubscriptionPlanModel = SubscriptionPlanModel()
    public weak var delegate: PlansTableViewCellDelegate?
    public var choosePlanDropDown = DropDown()
    
    override func awakeFromNib() {
        configureDropDown()
        updateLayout()
    }
    
    @objc func onTapDismissLabel(_ sender: Any) {
        self.delegate?.plansTableViewCell(shouldDismiss: self)
    }
    
    @objc func choosePlan(_ sender: AnyObject) {
        choosePlanDropDown.show()
    }
    
    func setRepository(_ repository: SubscriptionPlanModel) {
        self.repository = repository
        self.updateLayout()
    }
    
    func updateLayout() {
        self.planItem1mo.setRepository(repository, at: .month1)
        self.planItem3mo.setRepository(repository, at: .month3)
        self.planItem12mo.setRepository(repository, at: .year)
    }
    
    func configureDropDown() {
        choosePlanDropDown.anchorView = planSelectButton
        choosePlanDropDown.bottomOffset = CGPoint(x: 0, y: planSelectButton.bounds.height)
        choosePlanDropDown.dataSource = PlanConfig.SubscriptionPlan.allCases.compactMap({ $0.string })
        choosePlanDropDown.selectionAction = { [unowned self] (index, item) in
            self.planSelectButton.setTitle(item, for: .normal)
            self.repository.plan = PlanConfig.SubscriptionPlan.allCases[index]
            self.delegate?.plansTableViewCell(sync: repository, in: self)
            self.updateLayout()
        }
    }
}

extension PlansTableViewCell: PlanItemDelegate {
    func planItem(onTap planItem: PlanItem) {
        self.repository.span = planItem.span
        self.delegate?.plansTableViewCell(sync: repository, in: self)
        self.updateLayout()
    }
}

extension PlansTableViewCell: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.delegate?.plansTableViewCell(toBilling: repository, in: self)
    }
}
