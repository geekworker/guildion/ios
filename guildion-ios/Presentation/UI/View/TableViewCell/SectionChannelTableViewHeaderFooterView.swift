//
//  SectionChannelTableViewHeaderFooterView.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit

protocol SectionChannelTableViewHeaderFooterViewDelegate: class {
    func sectionChannelTableViewHeaderFooterView(onTap sectionChannelTableViewHeaderFooterView: SectionChannelTableViewHeaderFooterView)
    func sectionChannelTableViewHeaderFooterView(onAddTap sectionChannelTableViewHeaderFooterView: SectionChannelTableViewHeaderFooterView)
}

class SectionChannelTableViewHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            baseView.addGestureRecognizer(longpress)
            baseView.layer.cornerRadius = 5
            baseView.clipsToBounds = true
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var chevronImage: UIImageView! {
        didSet {
            chevronImage.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            chevronImage.tintColor = Theme.themify(key: .stringSecondary)
            chevronImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
            self.collapsed = false
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var addButton: IconButton! {
        didSet {
            addButton.buttonImage = R.image.plus()!.withRenderingMode(.alwaysTemplate)
            addButton.imageColor = Theme.themify(key: .stringSecondary)
            addButton.buttonInset = 5
            addButton.delegate = self
            addButton.layer.cornerRadius = addButton.bounds.width / 2
            addButton.clipsToBounds = true
            addButton.isHidden = true
        }
    }
    @IBInspectable var collapsed: Bool = false {
        didSet {
            let angle = collapsed ? 0 * CGFloat.pi / 180 : 90 * CGFloat.pi / 180
            UIView.animate(withDuration: 0.3, animations: {
                self.chevronImage.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
            })
        }
    }
    
    weak var delegate: SectionChannelTableViewHeaderFooterViewDelegate?
    var repository: SectionChannelEntity = SectionChannelEntity()
    
    static var viewHeight: CGFloat = 32
    static func getHeight(_ repository: SectionChannelEntity) -> CGFloat {
        return repository.is_empty || repository.id == 0 || repository.name == "" ? 0 : viewHeight
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func setRepository(_ repository: SectionChannelEntity, permission: PermissionModel) {
        self.collapsed = repository.collapsed
        let angle = self.collapsed ? 0 * CGFloat.pi / 180 : 90 * CGFloat.pi / 180
        UIView.animate(withDuration: 0.2, animations: {
            self.chevronImage.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
        })
        self.repository = repository
        self.nameLabel.text = repository.name
        self.addButton.isHidden = !permission.Guild.channels_managable && !repository.is_dm
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.sectionChannelTableViewHeaderFooterView(onTap: self)
    }
    
    @objc func onAddTap(_ sender: Any) {
        self.delegate?.sectionChannelTableViewHeaderFooterView(onAddTap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.backgroundColor = Theme.themify(key: .backgroundLight)
            })
        default:
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.backgroundColor = Theme.themify(key: .backgroundLight)
            })
            return
        }
    }
}

extension SectionChannelTableViewHeaderFooterView: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        self.delegate?.sectionChannelTableViewHeaderFooterView(onAddTap: self)
    }
}

extension SectionChannelTableViewHeaderFooterView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
