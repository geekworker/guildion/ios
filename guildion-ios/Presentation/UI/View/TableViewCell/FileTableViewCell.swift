//
//  FileTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/18.
//

import Foundation
import UIKit
import Nuke
import ESTMusicIndicator

protocol FileTableViewCellDelegate: class {
}

class FileTableViewCell: UITableViewCell {
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.isUserInteractionEnabled = false
            overView.isHidden = true
            overView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        }
    }
    @IBOutlet weak var fileImageView: UIImageView! {
        didSet {
            fileImageView.contentMode = .scaleAspectFill
            fileImageView.layer.cornerRadius = 3
            fileImageView.backgroundColor = Theme.themify(key: .background)
            fileImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var lockedImageView: UIImageView! {
        didSet {
            lockedImageView.image = R.image.locked()?.withRenderingMode(.alwaysTemplate)
            lockedImageView.tintColor = Theme.themify(key: .warning)
            lockedImageView.isHidden = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var durationLabel: UILabel! {
        didSet {
            durationLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var musicIndicatorView: ESTMusicIndicatorView! {
        didSet {
            musicIndicatorView.tintColor = Theme.themify(key: .main)
            musicIndicatorView.state = .paused
            musicIndicatorView.isHidden = true
        }
    }
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    public var repository: FileEntity = FileEntity()
    public weak var delegate: FileTableViewCellDelegate?
    
    static var cellHeight: CGFloat = 54
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
        self.backgroundColor = Theme.themify(key: .backgroundThick)
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundThick).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
        let longpress = UILongPressGestureRecognizer(
            target: self,
            action: #selector(self.hover(_:))
        )
        longpress.minimumPressDuration = 0.01
        longpress.cancelsTouchesInView = false
        longpress.delegate = self
        contentView.addGestureRecognizer(longpress)
    }
    
    public func setRepository(_ repository: FileEntity, playing: Bool = false) {
        self.musicIndicatorView.state = .paused
        self.musicIndicatorView.isHidden = true
        if playing {
            self.musicIndicatorView.isHidden = false
            self.musicIndicatorView.state = .playing
        }
        
        if self.repository.thumbnail != repository.thumbnail, let url = URL(string: repository.thumbnail) {
            self.fileImageView.image = nil
            Nuke.loadImage(with: url, into: self.fileImageView)
        }
        
        self.lockedImageView.isHidden = !repository.is_private
        
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                
                self.nameLabel.text = repository.name
                
                self.durationLabel.text = repository.formattedDuration
                self.dateLabel.text = self.formatter.string(from: repository.updated_at)
            }
        }
    }
    
    @objc func onTap(_ sender: Any) {
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            overView.fadeIn(type: .Normal, completed: nil)
        case .ended:
            overView.fadeOut(type: .Normal, completed: nil)
        default:
            overView.isHidden = true
        }
    }
}

extension FileTableViewCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
