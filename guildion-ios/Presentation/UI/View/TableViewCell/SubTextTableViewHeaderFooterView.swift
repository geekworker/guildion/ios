//
//  SubTextTableViewHeaderFooterView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/03.
//

import Foundation
import UIKit

class SubTextTableViewHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var subTextLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    static var height: CGFloat = 32
    
    override func awakeFromNib() {
    }
}
