//
//  MemberTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/09.
//

import Foundation
import UIKit
import Nuke

class MemberTableViewCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView! {
        didSet {
            userImageView.layer.cornerRadius = userImageView.frame.width / 2
            userImageView.clipsToBounds = true
            userImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var speakerImageView: UIImageView! {
        didSet {
            speakerImageView.image = R.image.speaker()!.withRenderingMode(.alwaysTemplate)
            speakerImageView.tintColor = Theme.themify(key: .stringSecondary)
            speakerImageView.isHidden = true
        }
    }
    @IBOutlet weak var activeView: ActiveView! {
        didSet {
            activeView.baseView.layer.cornerRadius = activeView.frame.width / 2
            activeView.baseView.layer.borderWidth = 0.5
            activeView.baseView.layer.borderColor = Theme.themify(key: .border).cgColor
            activeView.baseView.clipsToBounds = true
            activeView.isHidden = true
        }
    }
    static var rowMargin: CGFloat = 2
    static var sideMargin: CGFloat = 8
    static var cellHeight: CGFloat = 44 + (rowMargin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(
                x: newFrame.minX + MemberTableViewCell.sideMargin,
                y: newFrame.minY + MemberTableViewCell.rowMargin,
                width: newFrame.width - (MemberTableViewCell.sideMargin * 2),
                height: newFrame.height - (MemberTableViewCell.rowMargin * 2)
            )
        }
    }
    
    public var repository: MemberEntity = MemberEntity()
    
    public func setRepository(_ repository: MemberEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                self.nameLabel.text = repository.nickname
                if let url = URL(string: repository.picture_small) {
                    Nuke.loadImage(with: url, into: self.userImageView)
                }
                if let user = repository.User {
                    self.activeView.setRepository(repository: user)
                }
            }
        }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .background)
        self.backgroundColor = Theme.themify(key: .background)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .background).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
}
