//
//  GuildHeaderTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import Nuke

protocol GuildHeaderTableViewCellDelegate: class {
    func guildHeaderTableViewCell(onTapSetting guildHeaderTableViewCell: GuildHeaderTableViewCell)
}

class GuildHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var guildImage: UIImageView! {
        didSet {
            guildImage.layer.cornerRadius = 5
            guildImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var idLabel: UILabel! {
        didSet {
            idLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var settingImage: UIImageView! {
        didSet {
            settingImage.image = R.image.more()!.withRenderingMode(.alwaysTemplate)
            settingImage.tintColor = Theme.themify(key: .stringSecondary)
            settingImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapSetting(_:))))
        }
    }
    var repository: GuildEntity = GuildEntity()
    weak var delegate: GuildHeaderTableViewCellDelegate?
    
    static var margin: CGFloat = 6
    static var cellHeight: CGFloat = 70 + (margin * 2)
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            super.frame = CGRect(x: GuildHeaderTableViewCell.margin, y: GuildHeaderTableViewCell.margin, width: newFrame.width - (GuildHeaderTableViewCell.margin * 2), height: newFrame.height - (GuildHeaderTableViewCell.margin * 2))
        }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
    
    func setRepository(_ repository: GuildEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                self.nameLabel.text = repository.name
                self.idLabel.text = "@" + repository.uid
                if repository.picture_small != "", let url = URL(string: repository.picture_small) {
                    Nuke.loadImage(with: url, into: self.guildImage)
                } else {
                    self.guildImage.image = UIImage(url: DataConfig.Image.default_guild_image_url)
                }
            }
        }
    }
    
    @objc func onTapSetting(_ sender: Any) {
        self.delegate?.guildHeaderTableViewCell(onTapSetting: self)
    }
}
