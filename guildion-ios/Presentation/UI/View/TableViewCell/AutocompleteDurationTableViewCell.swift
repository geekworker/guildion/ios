//
//  AutocompleteDurationTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/29.
//

import Foundation
import UIKit
import TTTAttributedLabel

class AutocompleteDurationTableViewCell: UITableViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var durationLabel: TTTAttributedLabel! {
        didSet {
            durationLabel.text = ""
            durationLabel.textColor = Theme.themify(key: .string)
            messageLabelManager.label = durationLabel
            messageLabelManager.highlightDurationsInLabel()
        }
    }
    lazy var messageLabelManager: MessageLabelManager = {
        return MessageLabelManager()
    }()
    public var repository: Float = 0
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        let bgColorView = UIView()
        bgColorView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 10)
        self.selectedBackgroundView = bgColorView
    }
    
    public func setRepository(_ repository: Float) {
        self.repository = repository
        self.durationLabel.text = R.string.localizable.channelCurrentDuration("\(repository)")
        messageLabelManager.highlightDurationsInLabel()
    }
}
