//
//  StreamAudioTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/21.
//

import Foundation
import UIKit
import Nuke

protocol StreamAudioTableViewCellDelegate: class {
    func streamAudioTableViewCell(onChangeVolume value: Float, in streamAudioTableViewCell: StreamAudioTableViewCell)
}

class StreamAudioTableViewCell: UITableViewCell {
    @IBOutlet weak var streamImageView: UIImageView! {
        didSet {
            streamImageView.tintColor = Theme.themify(key: .string)
            streamImageView.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var volumeLabel: UILabel! {
        didSet {
            volumeLabel.text = R.string.localizable.streamSoundVolume()
            volumeLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var volumeSlider: UISlider! {
        didSet {
            volumeSlider.maximumValue = 1
            volumeSlider.tintColor = Theme.themify(key: .main)
            volumeSlider.value = 0
            volumeSlider.isContinuous = true
        }
    }
    
    var repository: StreamEntity = StreamEntity()
    weak var delegate: StreamAudioTableViewCellDelegate?
    
    func setRepository(_ repository: StreamEntity, volume: Float) {
        self.repository = repository
        switch repository.mode {
        case .fileReferenceYouTube:
            self.nameLabel.text = repository.FileReference?.File?.name
            self.streamImageView.image = R.image.yt_icon_rgb()?.withRenderingMode(.alwaysOriginal)
        case .fileReferenceMovie:
            self.nameLabel.text = repository.FileReference?.File?.name
            self.streamImageView.image = R.image.videoPlay()?.withRenderingMode(.alwaysTemplate)
        case .fileMovie:
            self.nameLabel.text = repository.File?.name
            self.streamImageView.image = R.image.videoPlay()?.withRenderingMode(.alwaysTemplate)
        case .fileYouTube:
            self.nameLabel.text = repository.File?.name
            self.streamImageView.image = R.image.yt_icon_rgb()?.withRenderingMode(.alwaysOriginal)
        default:
            self.nameLabel.text = R.string.localizable.streamCallVoiceVolume()
            self.streamImageView.image = R.image.speaker()?.withRenderingMode(.alwaysTemplate)
        }
        self.volumeSlider.value = volume
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        self.delegate?.streamAudioTableViewCell(onChangeVolume: sender.value, in: self)
    }
}
