//
//  SimpleButtonTableViewCell.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit

protocol SimpleButtonTableViewCellDelegate: class {
    func simpleButtonTableViewCell(onTap simpleButtonTableViewCell: SimpleButtonTableViewCell)
}


class SimpleButtonTableViewCell: UITableViewCell {
    @IBOutlet weak var simpleButton: SimpleButton! {
        didSet {
            self.simpleButton.canceled = false
            self.simpleButton.normalColor = Theme.themify(key: .cancel)
            self.simpleButton.textColor = Theme.themify(key: .backgroundContrast)
            self.simpleButton.textLabel.font = Font.FT(size: .FM, family: .B)
            self.simpleButton.delegate = self
        }
    }
    @IBOutlet weak var topInset: NSLayoutConstraint!
    @IBOutlet weak var bottomInset: NSLayoutConstraint!
    @IBOutlet weak var leftInset: NSLayoutConstraint!
    @IBOutlet weak var rightInset: NSLayoutConstraint!
    
    weak var delegate: SimpleButtonTableViewCellDelegate?
    
    override func awakeFromNib() {
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        self.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
    }
}

extension SimpleButtonTableViewCell: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.delegate?.simpleButtonTableViewCell(onTap: self)
    }
}
