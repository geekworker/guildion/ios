//
//  PanContainerViewNavigationController.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/22.
//

import Foundation
import UIKit

protocol PanContainerViewNavigationControllerSearchDelegate: class {
    var isContainer: Bool { get }
    var current_url: URL { get set }
    var domainType: SearchDomainType { get set }
    func panContainerViewNavigationControllerShouldClose()
    func panContainerViewNavigationControllerShouldReload()
    func panContainerViewNavigationController(_ panContainerViewNavigationController: PanContainerViewNavigationController, searched keyword: String?)
}

class PanContainerViewNavigationController: UINavigationController, PanContainerPresenter {
    var containerTransitionContext: PanContainerViewTransitionContext = PanContainerViewTransitionContext()
    public var searchBrowserNavigationBar: SearchBrowserNavigationBar = SearchBrowserNavigationBar()
    weak var searchDelegate: PanContainerViewNavigationControllerSearchDelegate?
    var current_url: URL {
        get {
            self.searchDelegate?.current_url ?? URL(string: Constant.APP_URL)!
        } set {
            self.searchBrowserNavigationBar.current_url = newValue
        }
    }
    var domainType: SearchDomainType {
        get {
            self.searchDelegate?.domainType ?? .google
        } set {
            self.searchBrowserNavigationBar.domainType = newValue
        }
    }
    var shouldAdjustSafeArea: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateSafeArea()
        configureSearchMode()
    }
    
    func updateSafeArea() {
        if shouldAdjustSafeArea {
            var newSafeArea = UIEdgeInsets()
            if let window = UIApplication.shared.keyWindow {
                newSafeArea = UIEdgeInsets(top: 0, left: window.safeAreaInsets.left, bottom: window.safeAreaInsets.bottom - 40, right: window.safeAreaInsets.right)
            }
            self.additionalSafeAreaInsets = newSafeArea
        } else {
            guard searchDelegate?.isContainer ?? false else { return }
            var newSafeArea = UIEdgeInsets()
            if let window = UIApplication.shared.keyWindow {
                newSafeArea = UIEdgeInsets(
                    top: 0,
                    left: window.safeAreaInsets.left,
                    bottom: UIScreen.main.hasNotch ? window.safeAreaInsets.bottom : window.safeAreaInsets.bottom + 18,
                    right: window.safeAreaInsets.right
                )
            }
            self.additionalSafeAreaInsets = newSafeArea
        }
    }
}

extension PanContainerViewNavigationController: SearchBrowserNavigationBarDelegate {
    fileprivate func configureSearchMode() {
        self.switchEditMode(self.searchDelegate != nil)
        self.searchBrowserNavigationBar.delegate = self
        self.searchBrowserNavigationBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.navigationBar.frame.height)
        self.navigationBar.addSubview(self.searchBrowserNavigationBar)
        self.searchBrowserNavigationBar.delegate = self
    }
    
    public func switchEditMode(_ show: Bool) {
        self.searchBrowserNavigationBar.isHidden = !show
        self.navigationBar.bringSubviewToFront(self.searchBrowserNavigationBar)
    }
    
    func searchBrowserNavigationBarShouldClose(_ searchBrowserNavigationBar: SearchBrowserNavigationBar) {
        self.searchDelegate?.panContainerViewNavigationControllerShouldClose()
    }
    
    func searchBrowserNavigationBarShouldReload(_ searchBrowserNavigationBar: SearchBrowserNavigationBar) {
        self.searchDelegate?.panContainerViewNavigationControllerShouldReload()
    }
    
    func searchBrowserNavigationBar(_ searchBrowserNavigationBar: SearchBrowserNavigationBar, searched keyword: String?) {
        self.searchDelegate?.panContainerViewNavigationController(self, searched: keyword)
    }
}
