//
//  PanContainerViewTransitionContext.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/21.
//

import Foundation
import UIKit

public class PanContainerViewTransitionContext: NSObject {
    var isPanContainerPresented: Bool = false
    var sourceRect: CGRect = .zero
    weak var containerView: PanContainerAnimatable?
    
    func forceDismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.containerView?.dismiss(animated: flag, completion: completion)
    }
}
