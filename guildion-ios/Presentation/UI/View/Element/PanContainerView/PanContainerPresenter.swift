//
//  PanContainerPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/20.
//

import Foundation
import UIKit

public protocol PanContainerPresenter where Self: UIViewController {
    var containerTransitionContext: PanContainerViewTransitionContext { get }
    func presentPanContainer(
        _ viewControllerToPresent: PanContainerAnimatable,
        completion: (() -> Void)?
    )
    func willDismissPanContainer(_ viewToPresent: PanContainerAnimatable)
    func didDismissPanContainer(_ viewToPresent: PanContainerAnimatable)
}

extension PanContainerPresenter {
    public func presentPanContainer(
        _ viewToPresent: PanContainerAnimatable,
        completion: (() -> Void)?
    ) {
        viewToPresent.configurePanContainer(parent: self)
        viewToPresent.willPanContainerAnimate()
        let window = UIApplication.shared.windows[0]
        self.animate({
            viewToPresent.frame.origin.y = self.containerTransitionContext.sourceRect.minY + window.safeAreaInsets.bottom + 10
        }) { _ in
            self.containerTransitionContext.isPanContainerPresented = true
            viewToPresent.didPanContainerAnimate()
        }
    }
    
    func willDismissPanContainer(_ viewToPresent: PanContainerAnimatable) {}
    func didDismissPanContainer(_ viewToPresent: PanContainerAnimatable) {}
    
    fileprivate func animate(
        _ animations: @escaping (() -> Void),
        _ completion: ((Bool) -> Void)? = nil
    ) {

        let transitionDuration: TimeInterval = 0.5
        let springDamping: CGFloat = 1.0
        let animationOptions: UIView.AnimationOptions = []
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)

        UIView.animate(
            withDuration: transitionDuration,
            delay: 0,
            usingSpringWithDamping: springDamping,
            initialSpringVelocity: 0,
            options: animationOptions,
            animations: animations,
            completion: completion
        )
    }
}
