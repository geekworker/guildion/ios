//
//  PanContainerView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/20.
//

import Foundation
import UIKit

public protocol PanContainerAnimatable where Self: UIView {
    var containerParentViewController: PanContainerPresenter! { get set }
    func configurePanContainer(parent: PanContainerPresenter)
    func willPanContainerAnimate()
    func didPanContainerAnimate()
    func willPanContainerDismiss()
    func didPanContainerDismiss()
    func dismiss(animated flag: Bool, completion: (() -> Void)?)
}

extension PanContainerAnimatable {
    func configurePanContainer(parent: PanContainerPresenter) {
        self.containerParentViewController = parent
        let window = UIApplication.shared.windows[0]
        self.containerParentViewController.containerTransitionContext.isPanContainerPresented = false
        self.frame = CGRect(
            x: parent.containerTransitionContext.sourceRect.minX,
            y: parent.containerTransitionContext.sourceRect.minY,
            width: parent.containerTransitionContext.sourceRect.width,
            height: parent.containerTransitionContext.sourceRect.height - window.safeAreaInsets.bottom
        )
        parent.containerTransitionContext.containerView = self
        self.frame.origin.y = parent.view.frame.height
        parent.view.addSubview(self)
    }
    
    func willPanContainerAnimate() {
    }
    
    func didPanContainerAnimate() {
    }
    
    func willPanContainerDismiss() {
    }
    
    func didPanContainerDismiss() {
    }
    
    func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.containerParentViewController.willDismissPanContainer(self)
        self.willPanContainerDismiss()
        if flag {
            self.animate({
                self.frame.origin.y = self.containerParentViewController.view.frame.height
            }) { [weak self] _ in
                guard let self = self, self.containerParentViewController != nil else { return }
                DispatchQueue.main.async {
                    self.containerParentViewController?.containerTransitionContext.isPanContainerPresented = false
                    self.containerParentViewController?.containerTransitionContext.containerView = nil
                    self.containerParentViewController?.didDismissPanContainer(self)
                    self.didPanContainerDismiss()
                    self.containerParentViewController = nil
                    self.removeFromSuperview()
                    completion?()
                    UIViewController.topMostController().viewWillAppear(false)
                    UIViewController.topMostController().viewDidAppear(false)
                }
            }
        } else {
            guard self.containerParentViewController != nil else { return }
            DispatchQueue.main.async {
                self.frame.origin.y = self.containerParentViewController.view.frame.height
                self.containerParentViewController?.containerTransitionContext.isPanContainerPresented = false
                self.containerParentViewController?.containerTransitionContext.containerView = nil
                self.containerParentViewController?.didDismissPanContainer(self)
                self.didPanContainerDismiss()
                self.containerParentViewController = nil
                self.removeFromSuperview()
                completion?()
                UIViewController.topMostController().viewWillAppear(false)
                UIViewController.topMostController().viewDidAppear(false)
            }
        }
    }
    
    fileprivate func animate(
        _ animations: @escaping (() -> Void),
        _ completion: ((Bool) -> Void)? = nil
    ) {

        let transitionDuration: TimeInterval = 0.5
        let springDamping: CGFloat = 1.0
        let animationOptions: UIView.AnimationOptions = []
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)

        UIView.animate(
            withDuration: transitionDuration,
            delay: 0,
            usingSpringWithDamping: springDamping,
            initialSpringVelocity: 0,
            options: animationOptions,
            animations: animations,
            completion: completion
        )
    }
}


class SamplePanContainerView: UIView, PanContainerAnimatable {
    weak var containerParentViewController: PanContainerPresenter!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = .white
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
    }
}
