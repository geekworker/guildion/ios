//
//  PanContainerNavigationView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/21.
//

import Foundation
import UIKit

protocol PanContainerNavigationViewChildDelegate: UIViewController {
    var parentContainerView: PanContainerNavigationView! { get set }
    func dismissContainer(animated flag: Bool, completion: (() -> Void)?)
}

extension PanContainerNavigationViewChildDelegate {
    func dismissContainer(animated flag: Bool, completion: (() -> Void)?) {
        self.parentContainerView?.dismiss(animated: flag, completion: completion)
    }
}

class PanContainerNavigationView: UIView, PanContainerAnimatable {
    weak var containerParentViewController: PanContainerPresenter!
    weak var childViewController: PanContainerNavigationViewChildDelegate!
    lazy var navigationController: PanContainerViewNavigationController = {
        let vc = PanContainerViewNavigationController()
        return vc
    }()
    override var safeAreaInsets: UIEdgeInsets {
        if let window = UIApplication.shared.keyWindow {
            return UIEdgeInsets(top: 0, left: window.safeAreaInsets.left, bottom: window.safeAreaInsets.bottom, right: window.safeAreaInsets.right)
        }
        return super.safeAreaInsets
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        updateLayout()
    }
    
    func updateLayout() {
        self.backgroundColor = .none
    }
    
    func willPanContainerAnimate() {
        configureChildViewController()
    }
    
    func willPanContainerDismiss() {
        self.navigationController.removeFromParent()
    }
    
    func configureChildViewController() {
        navigationController.view.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        navigationController.viewControllers.append(self.childViewController)
        navigationController.willMove(toParent: self.containerParentViewController)
        self.childViewController.parentContainerView = self
        if let searchVC = self.childViewController as? PanContainerViewNavigationControllerSearchDelegate {
            navigationController.searchDelegate = searchVC
        }
        self.containerParentViewController.addChild(navigationController)
        self.addSubview(navigationController.view)
        navigationController.didMove(toParent: self.containerParentViewController)
        
        if self.containerParentViewController as? PanContainerViewNavigationController != nil {
            self.navigationController.viewWillAppear(true)
        }
    }
}
