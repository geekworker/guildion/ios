//
//  MessageTimeSectionView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/26.
//

import Foundation
import UIKit

@IBDesignable
class TimeSectionView: UIView {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.isHidden = true
            baseView.layer.cornerRadius = 5
            baseView.clipsToBounds = true
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.text = ""
            dateLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    public var repository: Date = Date.CurrentDate()
    
    static func isVisible(current_date: Date, before_date: Date) -> Bool {
        return before_date.fullDistance(from: current_date, resultIn: .day) ?? 0 > 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.timeSectionView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.backgroundColor = .none
        self.backgroundColor = .none
    }
    
    func setRepository(_ repository: Date) {
        self.repository = repository
        let formatter = DateFormatter()
        formatter.setTemplate(.date)
        baseView.isHidden = false
        self.dateLabel.text = formatter.string(from: repository)
    }
}
