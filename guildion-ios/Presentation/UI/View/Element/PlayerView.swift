//
//  PlayerView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/02.
//

import Foundation
import UIKit
import AVFoundation

protocol PlayerViewDelegate: class {
    func playerView(didPlayerStateChange state: AVPlayerLoader.Result, in playerView: PlayerView)
    func playerView(didPlayerBufferingStateChange buffering: Bool, in playerView: PlayerView)
    func playerView(didPlayerTimeChange playTime: Float, in playerView: PlayerView)
}

class PlayerView: UIView {
    var player: AVPlayer {
        get {
            if playerLayer.player == nil {
                playerLayer.player = AVPlayer()
            }
            return playerLayer.player!
        }
        set {
            self.removePeriodicTimeObserver()
            self.observation?.invalidate()
            self.playerLayer.player = newValue
            self.startObserve()
            self.addPeriodicTimeObserver()
        }
    }
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    private var observation: NSKeyValueObservation?
    private var rateObservation: NSKeyValueObservation?
    private var stallObservation: NSKeyValueObservation?
    private var timeObserverToken: Any?
    public weak var delegate: PlayerViewDelegate?
    public var playTime: Float = 0
    @objc dynamic public var duration: TimeInterval = 0
    @objc dynamic public var playing: Bool = false
    @objc dynamic public var paused: Bool = false
    
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        load()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        load()
    }
    
    fileprivate func load() {
        self.playerLayer.player = AVPlayer.init()
    }
    
    func startObserve() {
        guard let item = self.player.currentItem, self.observation == nil else { return }
        self.observation = item.observe(\.status) { [weak self] item, change in
            guard let self = self else { return }
            self.playing = self.player.isPlaying()
            self.paused = self.player.isPaused()
            switch item.status {
            case .readyToPlay:
                self.delegate?.playerView(didPlayerStateChange: .success(self.player), in: self)
            case .failed:
                self.playing = false
                self.paused = false
                self.delegate?.playerView(didPlayerStateChange: .failed, in: self)
            case .unknown:
                self.playing = false
                self.paused = false
                self.delegate?.playerView(didPlayerStateChange: .unknown, in: self)
            @unknown default: break
            }
        }
        item.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
        item.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: nil)
        
        self.stallObservation = self.player.observe(\.timeControlStatus, options: [.new, .old], changeHandler: {
            [weak self] (playerItem, change) in
            guard let self = self else { return }
            switch (playerItem.timeControlStatus) {
            case AVPlayerTimeControlStatus.paused:
                self.playing = false
                self.paused = true
                self.delegate?.playerView(didPlayerStateChange: .paused, in: self)
            case AVPlayerTimeControlStatus.playing:
                self.playing = true
                self.paused = false
                self.delegate?.playerView(didPlayerStateChange: .playing, in: self)
            case AVPlayerTimeControlStatus.waitingToPlayAtSpecifiedRate: break
            @unknown default: break
            }
        })
    }
    
    func addPeriodicTimeObserver() {
        // Notify every half second
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let interval = min(max(Double(self.duration / 10000), 0.01), 0.1)
        let time = CMTime(seconds: interval, preferredTimescale: timeScale)
        timeObserverToken = player.addPeriodicTimeObserver(
            forInterval: time,
            queue: .main
        )
        { [weak self] time in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.playTime = time.float
                self.delegate?.playerView(didPlayerTimeChange: time.float, in: self)
            }
        }
    }

    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            player.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is AVPlayerItem {
            switch keyPath {
            case "playbackBufferEmpty": self.delegate?.playerView(didPlayerBufferingStateChange: true, in: self)
            case "playbackLikelyToKeepUp": self.delegate?.playerView(didPlayerBufferingStateChange: false, in: self)
            case "playbackBufferFull": break
            case .none: break
            case .some(_): break
            }
        }
    }
    
    func resetPlayer() {
        self.removePeriodicTimeObserver()
        self.observation?.invalidate()
        self.observation = nil
        self.rateObservation?.invalidate()
        self.rateObservation = nil
        self.stallObservation?.invalidate()
        self.stallObservation = nil
    }
    
    func replaceCurrentItem(with item: AVPlayerItem, duration: TimeInterval) {
        DispatchQueue.main.async {
            self.duration = duration
            self.resetPlayer()
            self.player.replaceCurrentItem(with: item)
            self.startObserve()
            self.addPeriodicTimeObserver()
        }
    }
}
