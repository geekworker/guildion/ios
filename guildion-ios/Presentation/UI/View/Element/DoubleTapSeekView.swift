//
//  DoubleTapSeekView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/18.
//

import Foundation
import UIKit

protocol DoubleTapSeekViewDelegate: class {
    func doubleTapSeekView(shouldSeek count: Float, incrementable: Bool, in doubleTapSeekView: DoubleTapSeekView)
}

class DoubleTapSeekView: UIView {
    @IBOutlet weak var baseViewLeadingConstraint: NSLayoutConstraint! {
        didSet {
            baseViewLeadingConstraint.constant = incrementable ? 0 : -self.frame.width
        }
    }
    @IBOutlet weak var baseViewTrailingConstraint: NSLayoutConstraint! {
        didSet {
            baseViewTrailingConstraint.constant = incrementable ? self.frame.width : 0
        }
    }
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.isUserInteractionEnabled = true
            baseView.backgroundColor = UIColor.gray.lighter(by: 20)?.withAlphaComponent(0.5)
            baseView.isHidden = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(_:)))
            tap.numberOfTapsRequired = 2
            tap.delegate = self
            //baseView.addGestureRecognizer(tap)
        }
    }
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.isHidden = true
            containerView.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.tintColor = UIColor.white
            iconImageView.image = incrementable ? R.image.incrementPlaying()?.withRenderingMode(.alwaysTemplate) : R.image.decrementPlaying()?.withRenderingMode(.alwaysTemplate)
        }
    }
    @IBOutlet weak var timeLabel: UILabel! {
        didSet {
            timeLabel.text = ""
            timeLabel.textColor = UIColor.white
        }
    }
    @IBInspectable var incrementable: Bool = true {
        didSet {
            iconImageView.image = incrementable ? R.image.incrementPlaying()?.withRenderingMode(.alwaysTemplate) : R.image.decrementPlaying()?.withRenderingMode(.alwaysTemplate)
            baseViewLeadingConstraint.constant = incrementable ? 0 : -self.frame.width
            baseViewTrailingConstraint.constant = incrementable ? self.frame.width : 0
        }
    }
    @IBInspectable var skipDuration: Float = SyncPlayerConfig.min_skip_duration_sc
    weak var delegate: DoubleTapSeekViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.doubleTapSeekView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.updateLayout(incrementable: self.incrementable)
    }
    
    func updateLayout(incrementable: Bool) {
        self.clipsToBounds = true
        self.incrementable = incrementable
        self.baseView.layer.cornerRadius = self.baseView.frame.height / 2
        self.baseView.clipsToBounds = true
    }
    
    func updateLayout(incrementable: Bool, height: CGFloat) {
        self.clipsToBounds = true
        self.incrementable = incrementable
        self.baseView.layer.cornerRadius = (height + 200) / 2
        self.baseView.clipsToBounds = true
    }
    
    fileprivate var gestureStartTime: TimeInterval!
    fileprivate var gestureTimer: Timer!
    fileprivate var gestureDuration: TimeInterval!
    @objc func doubleTapped(_ sender: UITapGestureRecognizer) {
        self.containerView.fadeIn(type: .Normal, completed: nil)
        self.baseView.fadeIn(type: .Normal, completed: nil)
        //let location = sender.location(in: baseView)
        //self.baseView.doubleTapSeekViewWaveAnimation(point: location, color: UIColor.white)
        self.timeLabel.text = "\(Int(self.skipDuration * Float(sender.numberOfTouches))) \(R.string.localizable.seconds())"
        self.delegate?.doubleTapSeekView(shouldSeek: self.skipDuration * Float(sender.numberOfTouches), incrementable: self.incrementable, in: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
            self.containerView.fadeOut(type: .Normal, completed: nil)
            self.baseView.fadeOut(type: .Normal, completed: nil)
        })
    }
    
    func doubleTapped(_ sender: UITapGestureRecognizer, count: Int) {
        self.containerView.fadeIn(type: .Normal, completed: nil)
        self.baseView.fadeIn(type: .Normal, completed: nil)
        //let location = sender.location(in: baseView)
        //self.baseView.doubleTapSeekViewWaveAnimation(point: location, color: UIColor.white)
        self.timeLabel.text = "\(Int(self.skipDuration * Float(count - 1))) \(R.string.localizable.seconds())"
        self.delegate?.doubleTapSeekView(shouldSeek: self.skipDuration * Float(count - 1), incrementable: self.incrementable, in: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
            self.containerView.fadeOut(type: .Normal, completed: nil)
            self.baseView.fadeOut(type: .Normal, completed: nil)
        })
    }
}

extension DoubleTapSeekView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension UIView {
    func doubleTapSeekViewWaveAnimation(point: CGPoint, color: UIColor) {
        let location = point
        let layer = CAShapeLayer.init()
        self.layer.addSublayer(layer)
        layer.frame = CGRect.init(x: location.x, y: location.y, width: 100, height: 100)
        layer.position = CGPoint.init(x: location.x, y: location.y)
        layer.contents = {
         let size: CGFloat = self.frame.height
            UIGraphicsBeginImageContext(CGSize.init(width: size, height: size))
            let context = UIGraphicsGetCurrentContext()!
            context.saveGState()
            context.setFillColor(UIColor.clear.cgColor)
            context.fill(self.frame)
            let r = CGFloat.init(size/2-10)
            let center = CGPoint.init(x: size/2, y: size/2)
            let path : CGMutablePath = CGMutablePath()
            path.addArc(center: center, radius: r, startAngle: 0, endAngle: CGFloat(Double.pi*2), clockwise: false)
            context.addPath(path)
            context.setFillColor(color.cgColor)
            context.setStrokeColor(color.cgColor)
            context.drawPath(using: .fillStroke)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            context.restoreGState()
            return image!.cgImage
        }()
        
        let animationGroup: CAAnimationGroup = {
            let animation: CABasicAnimation = {
                let animation = CABasicAnimation(keyPath: "transform.scale")
                animation.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut)
                animation.duration = 0.5
                animation.isRemovedOnCompletion = false
                animation.fillMode = CAMediaTimingFillMode.forwards
                animation.fromValue = NSNumber(value: 0.5)
                animation.toValue = NSNumber(value: 5.0)
                return animation
            }()
        
            let animation2: CABasicAnimation = {
                let animation = CABasicAnimation(keyPath: "opacity")
                animation.duration = 0.5
                animation.isRemovedOnCompletion = false
                animation.fillMode = CAMediaTimingFillMode.forwards
                animation.fromValue = NSNumber(value: 0.5)
                animation.toValue = NSNumber(value: 0.0)
                return animation
            }()
        
            let group = CAAnimationGroup()
            group.beginTime = CACurrentMediaTime()
            group.animations = [animation, animation2]
            group.isRemovedOnCompletion = false
            group.fillMode = CAMediaTimingFillMode.backwards
            return group
        }()
        CATransaction.setAnimationDuration(5.0)
        CATransaction.setCompletionBlock({
            layer.removeFromSuperlayer()
        })
        CATransaction.begin()
        layer.add(animationGroup, forKey: nil)
        layer.opacity = 0.0
        CATransaction.commit()
    }
}
