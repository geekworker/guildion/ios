//
//  ActiveView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

@IBDesignable
class ActiveView: UIView {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            self.baseView.layer.borderColor = Theme.themify(key: .transparent).cgColor
            self.baseView.layer.borderWidth = 2
            self.baseView.backgroundColor = Theme.themify(key: .success)
        }
    }
    public var repository: UserEntity = UserEntity()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.activeView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        updateLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.baseView.layer.cornerRadius = baseView.frame.width / 2
        self.baseView.clipsToBounds = true
    }
    
    func setRepository(repository: UserEntity) {
        self.repository = repository
        updateLayout()
    }
    
    func updateLayout() {
        self.baseView.layer.cornerRadius = baseView.frame.width / 2
        self.baseView.backgroundColor = self.repository.active ? Theme.themify(key: .success) : Theme.themify(key: .transparent)
        self.baseView.isHidden = !self.repository.active
    }
}
