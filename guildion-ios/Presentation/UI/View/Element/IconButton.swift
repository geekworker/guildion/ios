//
//  IconButton.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/18.
//

import Foundation
import UIKit

protocol IconButtonDelegate: class {
    func iconButton(onTap iconButton: IconButton)
}

@IBDesignable
class IconButton: UIView {
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.tintColor = imageColor
        }
    }
    @IBOutlet weak var buttonTopInset: NSLayoutConstraint!
    @IBOutlet weak var buttonLeftInset: NSLayoutConstraint!
    @IBOutlet weak var buttonBottomInset: NSLayoutConstraint!
    @IBOutlet weak var buttonRightInset: NSLayoutConstraint!
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.alpha = hoverAlpha
            overView.backgroundColor = .none
            
            overView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(self.tap(_:))
                )
            )
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            overView.addGestureRecognizer(longpress)
        }
    }
    @IBInspectable var buttonImage: UIImage = R.image.plus()!.withRenderingMode(.alwaysTemplate) {
        didSet {
            imageView.image = buttonImage
        }
    }
    @IBInspectable var imageColor: UIColor = Theme.themify(key: .string) {
        didSet {
            imageView.tintColor = imageColor
        }
    }
    @IBInspectable var hoverColor: UIColor = UIColor.white
    @IBInspectable var hoverAlpha: CGFloat = 0.2 {
        didSet {
            overView.alpha = hoverAlpha
        }
    }
    @IBInspectable var buttonInset: CGFloat = 0 {
        didSet {
            buttonTopInset.constant = buttonInset
            buttonLeftInset.constant = buttonInset
            buttonBottomInset.constant = buttonInset
            buttonRightInset.constant = buttonInset
        }
    }
    
    public weak var delegate: IconButtonDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.iconButton.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = .none
    }
    
    @objc fileprivate func tap(_ sender: UITapGestureRecognizer) {
        self.delegate?.iconButton(onTap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = self.hoverColor
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .none
            })
        default:
            self.overView.backgroundColor = .none
        }
    }
}

extension IconButton: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
