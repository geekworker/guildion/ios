//
//  StreamSlider.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/01.
//

import Foundation
import UIKit

@IBDesignable
class StreamSlider: UISlider {
    @IBInspectable var trackHeight: CGFloat = 3
    @IBInspectable var thumbRadius: CGFloat = 26
    @IBInspectable var thumbColor: UIColor = .white

    private lazy var thumbView: UIView = {
        let thumb = UIView()
        thumb.backgroundColor = thumbColor
        return thumb
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        updateThumb()
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
//        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self)

        let positionOfSlider: CGPoint = self.frame.origin
        let widthOfSlider: CGFloat = self.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.maximumValue) / widthOfSlider)

        self.setValue(Float(newValue), animated: true)
    }
    
    private func updateThumb() {
        let thumb = thumbImage(diameter: thumbRadius),
            highLightThumb = thumbImage(diameter: thumbRadius * 1.2)
        setThumbImage(thumb, for: .normal)
        setThumbImage(highLightThumb, for: .highlighted)
    }
    
    
    private func thumbImage(diameter: CGFloat) -> UIImage {
        thumbView.frame = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        thumbView.layer.cornerRadius = diameter / 2
        let renderer = UIGraphicsImageRenderer(bounds: CGRect(x: -(diameter * 0.5 / 2), y: -(diameter * 0.5 / 2), width: diameter * 1.5, height: diameter * 1.5))
        return renderer.image { rendererContext in
            thumbView.layer.render(in: rendererContext.cgContext)
        }
    }

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }

}
