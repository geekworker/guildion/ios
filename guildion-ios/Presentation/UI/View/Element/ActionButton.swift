//
//  ActionButton.swift
//  nowy-ios
//
//  Created by Apple on 2020/05/17.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol ActionButtonDelegate: class {
    func actionButton(onTap actionButton: ActionButton)
}

@IBDesignable
class ActionButton: UIView {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .main)
            baseView.layer.borderColor = UIColor.white.cgColor // Theme.themify(key: .background).cgColor
            baseView.layer.borderWidth = 0.3
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            self.imageView.tintColor = .white // Theme.themify(key: .background)
            self.imageView.image = R.image.plus()!.withRenderingMode(.alwaysTemplate)
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.layer.cornerRadius = overView.bounds.width / 2
            overView.clipsToBounds = true
            overView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
            overView.alpha = 0.3
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            overView.addGestureRecognizer(longpress)
        }
    }
    @IBInspectable var renderTemplate: Bool = true {
        didSet {
            self.imageView.image = renderTemplate ? image.withRenderingMode(.alwaysTemplate) : image.withRenderingMode(.alwaysOriginal)
        }
    }
    @IBInspectable var image: UIImage = R.image.plus()!.withRenderingMode(.alwaysTemplate) {
        didSet {
            self.imageView.image = renderTemplate ? image.withRenderingMode(.alwaysTemplate) : image.withRenderingMode(.alwaysOriginal)
        }
    }
    public weak var delegate: ActionButtonDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.actionButton.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        baseView.layer.cornerRadius = baseView.bounds.width / 2
        baseView.clipsToBounds = true
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate.actionButton(onTap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = Theme.themify(key: .backgroundContrast)
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = Theme.themify(key: .transparent)
            })
        default: self.overView.backgroundColor = Theme.themify(key: .transparent)
        }
    }
}

extension ActionButton: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
