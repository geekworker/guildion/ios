//
//  ChannelMessageTextHeightSizeCalculator.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/05.
//

import Foundation
import UIKit
import TTTAttributedLabel

class ChannelMessageTextHeightSizeCalculator: UIView {
    @IBOutlet weak var messageLabel: TTTAttributedLabel! {
        didSet {
            messageLabel.text = ""
            MessageLabelManager.configure(label: messageLabel)
            messageLabel.adjustsFontSizeToFitWidth = true
            messageLabel.minimumScaleFactor = 0.5
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.channelMessageTextHeightSizeCalculator.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
}
