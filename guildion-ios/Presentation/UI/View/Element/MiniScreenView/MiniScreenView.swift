//
//  MiniScreenView.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/07.
//

import Foundation
import UIKit

public protocol MiniScreenAnimatable where Self: UIView {
    var presentingSubViewController: MiniScreenPresenter? { get set }
    func configureMiniScreen()
    func willMiniScreenAnimate()
    func didMiniScreenAnimate()
    func willMiniScreenDismiss()
    func didMiniScreenDismiss()
    func dismiss(animated flag: Bool, completion: (() -> Void)?)
}

extension MiniScreenAnimatable {
    func configureMiniScreen() {}
    func willMiniScreenAnimate() {}
    func didMiniScreenAnimate() {}
    func willMiniScreenDismiss() {}
    func didMiniScreenDismiss() {}
    
    func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.willMiniScreenDismiss()
        self.presentingSubViewController?.willDismissMiniScreen(self)
        self.presentingSubViewController?.viewWillAppear(true)
        if flag {
            self.fadeOut(type: .Normal, completed: {
                DispatchQueue.main.async {
                    self.removeAllSubviews()
                    self.removeFromSuperview()
                    self.presentingSubViewController?.miniScreenTransitionContext.isMiniScreenViewPresented = false
                    self.presentingSubViewController?.viewDidAppear(true)
                    self.presentingSubViewController?.didDismissMiniScreen(self)
                    self.presentingSubViewController = nil
                    self.didMiniScreenDismiss()
                }
            })
        } else {
            self.isHidden = true
            self.removeAllSubviews()
            self.removeFromSuperview()
            self.presentingSubViewController?.miniScreenTransitionContext.isMiniScreenViewPresented = false
            self.presentingSubViewController?.viewDidAppear(true)
            self.presentingSubViewController?.didDismissMiniScreen(self)
            self.presentingSubViewController = nil
            self.didMiniScreenDismiss()
        }
    }
}

class MiniScreenView: UIView, MiniScreenAnimatable {
    static var shared: MiniScreenView?
    static func getInstance() -> MiniScreenView {
        if shared == nil { shared = MiniScreenView() }
        return shared!
    }
    weak var presentingSubViewController: MiniScreenPresenter?
    static var marginTop: CGFloat = UIApplication.firstWindow.safeAreaInsets.top + 12 + 44
    static var marginBottom: CGFloat = UIApplication.firstWindow.safeAreaInsets.bottom + 12 + 44
    static var marginLeft: CGFloat = UIApplication.firstWindow.safeAreaInsets.left + 8
    static var marginRight: CGFloat = UIApplication.firstWindow.safeAreaInsets.right + 8
    static func getSize() -> CGSize {
        let width = (UIScreen.main.bounds.width / 5) * 3
        let height = (UIScreen.main.bounds.width / 5) * 2
        return CGSize(width: width, height: height)
    }
    
    enum PositionType {
        case topLeft(size: CGSize, marginTop: CGFloat, marginLeft: CGFloat)
        case topRight(size: CGSize, marginTop: CGFloat, marginRight: CGFloat)
        case bottomLeft(size: CGSize, marginBottom: CGFloat, marginLeft: CGFloat)
        case bottomRight(size: CGSize, marginBottom: CGFloat, marginRight: CGFloat)
        
        var frame: CGRect {
            switch self {
            case .topLeft(let size, let marginTop, let marginLeft):
                return CGRect(x: marginLeft, y: marginTop, width: size.width, height: size.height)
            case .topRight(let size, let marginTop, let marginRight):
                return CGRect(x: UIScreen.main.bounds.width - marginRight - size.width, y: marginTop, width: size.width, height: size.height)
            case .bottomLeft(let size, let marginBottom, let marginLeft):
                return CGRect(x: marginLeft, y: UIScreen.main.bounds.height - marginBottom - size.height, width: size.width, height: size.height)
            case .bottomRight(let size, let marginBottom, let marginRight):
                return CGRect(x: UIScreen.main.bounds.width - marginRight - size.width, y: UIScreen.main.bounds.height - marginBottom - size.height, width: size.width, height: size.height)
            }
        }
        
        var isTop: Bool {
            switch self {
            case .topLeft, .topRight: return true
            case .bottomLeft, .bottomRight: return false
            }
        }
        
        var isBottom: Bool {
            switch self {
            case .topLeft, .topRight: return false
            case .bottomLeft, .bottomRight: return true
            }
        }
        
        var isRight: Bool {
            switch self {
            case .bottomRight, .topRight: return true
            case .bottomLeft, .topLeft: return false
            }
        }
        
        var isLeft: Bool {
            switch self {
            case .bottomRight, .topRight: return false
            case .bottomLeft, .topLeft: return true
            }
        }
    }
    
    var position: PositionType = .topRight(size: MiniScreenView.getSize(), marginTop: MiniScreenView.marginTop, marginRight: MiniScreenView.marginRight)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.miniScreenView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.backgroundColor = Theme.themify(key: .transparent)
        self.backgroundColor = Theme.themify(key: .transparent)
        
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        let dragGesture = UIPanGestureRecognizer(target: self, action: #selector(onDragging(_:)))
        dragGesture.delegate = self
        self.addGestureRecognizer(dragGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap(_:)))
        tapGesture.delegate = self
        self.addGestureRecognizer(tapGesture)
        
        guard UIApplication.shared.windows.count > 0 else { return }
        self.frame = self.position.frame
    }
    
    @objc func onTap(_ gesture: UITapGestureRecognizer) {
        self.presentingSubViewController?.shouldExitMiniScreen(animated: true)
    }
    
    func updatePosition(_ position: PositionType, animated: Bool) {
        self.position = position
        if animated {
            UIView.animate(withDuration: 0.3, delay: 0, animations: {
                self.transform = .identity
                self.frame = position.frame
            })
        } else {
            self.frame = position.frame
        }
    }
    
    fileprivate var moveX: CGFloat = 0
    fileprivate var moveY: CGFloat = 0
    fileprivate let shouldMoveABSX: CGFloat = 20
    fileprivate let shouldMoveABSY: CGFloat = 40
    @objc func onDragging(_ gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began: break
        case .changed:
            let transition = gesture.translation(in: UIApplication.firstWindow)
            moveX += transition.x
            moveY += transition.y
            self.transform = CGAffineTransform(translationX: transition.x, y: transition.y)
        case .ended:
            self.switchPosition(moveX: moveX, moveY: moveY)
            moveX = 0
            moveY = 0
        default: break
        }
    }
    
    func switchPosition(moveX: CGFloat, moveY: CGFloat) {
        let shouldMovePositiveX = moveX > 0 && abs(moveX) > shouldMoveABSX
        let shouldMoveNegativeX = moveX < 0 && abs(moveX) > shouldMoveABSX
        let shouldMovePositiveY = moveY > 0 && abs(moveY) > shouldMoveABSY
        let shouldMoveNegativeY = moveY < 0 && abs(moveY) > shouldMoveABSY
        
        switch self.position {
        case .topLeft:
            if shouldMovePositiveX && !shouldMovePositiveY {
                updatePosition(
                    .topRight(
                        size: self.frame.size,
                        marginTop: MiniScreenView.marginTop,
                        marginRight: MiniScreenView.marginRight
                    ), animated: true
                )
            } else if !shouldMovePositiveX && shouldMovePositiveY {
                updatePosition(
                    .bottomLeft(
                        size: self.frame.size,
                        marginBottom: MiniScreenView.marginBottom,
                        marginLeft: MiniScreenView.marginLeft
                    ), animated: true
                )
            } else if shouldMovePositiveX && shouldMovePositiveY {
                updatePosition(
                    .bottomRight(
                        size: self.frame.size,
                        marginBottom: MiniScreenView.marginBottom,
                        marginRight: MiniScreenView.marginRight
                    ), animated: true
                )
            } else {
                updatePosition(position, animated: true)
            }
        case .topRight:
            if shouldMoveNegativeX && !shouldMovePositiveY {
                updatePosition(
                    .topLeft(
                        size: self.frame.size,
                        marginTop: MiniScreenView.marginTop,
                        marginLeft: MiniScreenView.marginLeft
                    ), animated: true
                )
            } else if !shouldMoveNegativeX && shouldMovePositiveY {
                updatePosition(
                    .bottomRight(
                        size: self.frame.size,
                        marginBottom: MiniScreenView.marginBottom,
                        marginRight: MiniScreenView.marginRight
                    ), animated: true
                )
            } else if shouldMoveNegativeX && shouldMovePositiveY {
                updatePosition(
                    .bottomLeft(
                        size: self.frame.size,
                        marginBottom: MiniScreenView.marginBottom,
                        marginLeft: MiniScreenView.marginLeft
                    ), animated: true
                )
            } else {
                updatePosition(position, animated: true)
            }
        case .bottomLeft:
            if shouldMovePositiveX && !shouldMoveNegativeY {
                updatePosition(
                    .bottomRight(
                        size: self.frame.size,
                        marginBottom: MiniScreenView.marginBottom,
                        marginRight: MiniScreenView.marginRight
                    ), animated: true
                )
            } else if !shouldMovePositiveX && shouldMoveNegativeY {
                updatePosition(
                    .topLeft(
                        size: self.frame.size,
                        marginTop: MiniScreenView.marginTop,
                        marginLeft: MiniScreenView.marginLeft
                    ), animated: true
                )
            } else if shouldMovePositiveX && shouldMoveNegativeY {
                updatePosition(
                    .topRight(
                        size: self.frame.size,
                        marginTop: MiniScreenView.marginTop,
                        marginRight: MiniScreenView.marginRight
                    ), animated: true
                )
            } else {
                updatePosition(position, animated: true)
            }
        case .bottomRight:
            if shouldMoveNegativeX && !shouldMoveNegativeY {
                updatePosition(
                    .bottomLeft(
                        size: self.frame.size,
                        marginBottom: MiniScreenView.marginBottom,
                        marginLeft: MiniScreenView.marginLeft
                    ), animated: true
                )
            } else if !shouldMoveNegativeX && shouldMoveNegativeY {
                updatePosition(
                    .topRight(
                        size: self.frame.size,
                        marginTop: MiniScreenView.marginTop,
                        marginRight: MiniScreenView.marginRight
                    ), animated: true
                )
            } else if shouldMoveNegativeX && shouldMoveNegativeY {
                updatePosition(
                    .topLeft(
                        size: self.frame.size,
                        marginTop: MiniScreenView.marginTop,
                        marginLeft: MiniScreenView.marginLeft
                    ), animated: true
                )
            } else {
                updatePosition(position, animated: true)
            }
        }
    }
}

extension MiniScreenView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
