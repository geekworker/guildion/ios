//
//  MiniScreenViewTransitionContext.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/08.
//

import Foundation
import UIKit

public class MiniScreenViewTransitionContext: NSObject {
    var isMiniScreenViewPresented: Bool = false
    var sourceRect: CGRect = .zero
    weak var containerView: MiniScreenAnimatable?
}
