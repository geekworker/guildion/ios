//
//  MiniScreenPresenter.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/08.
//

import Foundation
import UIKit

public protocol MiniScreenPresenter where Self: UIViewController {
    var miniScreenTransitionContext: MiniScreenViewTransitionContext { get }
    func presentMiniScreen(
        _ viewToPresent: MiniScreenAnimatable,
        completion: (() -> Void)?
    )
    func willDismissMiniScreen(_ viewToPresent: MiniScreenAnimatable)
    func didDismissMiniScreen(_ viewToPresent: MiniScreenAnimatable)
    func shouldExitMiniScreen(animated flag: Bool)
}

extension MiniScreenPresenter {
    func presentMiniScreen(
        _ viewToPresent: MiniScreenAnimatable,
        completion: (() -> Void)?
    ) {
        miniScreenTransitionContext.isMiniScreenViewPresented = false
        miniScreenTransitionContext.containerView = viewToPresent
        viewToPresent.configureMiniScreen()
        viewToPresent.presentingSubViewController = self
        viewToPresent.willMiniScreenAnimate()
        self.view.frame = self.miniScreenTransitionContext.sourceRect
        viewToPresent.addSubview(self.view)
        viewToPresent.isHidden = true
        UIApplication.firstWindow.addSubview(viewToPresent)
        UIApplication.firstWindow.bringSubviewToFront(viewToPresent)
        viewToPresent.fadeIn(type: .Normal, completed: {
            self.miniScreenTransitionContext.isMiniScreenViewPresented = true
            viewToPresent.didMiniScreenAnimate()
        })
    }
    func willDismissMiniScreen(_ viewToPresent: MiniScreenAnimatable) {}
    func didDismissMiniScreen(_ viewToPresent: MiniScreenAnimatable) {}
    
    fileprivate func animate(
        _ animations: @escaping (() -> Void),
        _ completion: ((Bool) -> Void)? = nil
    ) {

        let transitionDuration: TimeInterval = 0.5
        let springDamping: CGFloat = 1.0
        let animationOptions: UIView.AnimationOptions = []
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)

        UIView.animate(
            withDuration: transitionDuration,
            delay: 0,
            usingSpringWithDamping: springDamping,
            initialSpringVelocity: 0,
            options: animationOptions,
            animations: animations,
            completion: completion
        )
    }
}
