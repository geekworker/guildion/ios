//
//  StreamButton.swift
//  nowy-ios
//
//  Created by Apple on 2020/05/06.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import BadgeSwift

protocol StreamButtonDelegate: class {
    func streamButton(onTap streamButton: StreamButton)
}

@IBDesignable
class StreamButton: UIView {
    @IBOutlet weak var badgeView: BadgeSwift! {
        didSet {
            badgeView.badgeColor = Theme.themify(key: .badge)
            badgeView.textColor = .white
            badgeView.isHidden = true
        }
    }
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.layer.cornerRadius = baseView.frame.width / 2
            baseView.clipsToBounds = true
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            self.imageView.image = image?.withRenderingMode(.alwaysTemplate)
             self.imageView.contentMode = .scaleAspectFit
            self.imageView.tintColor = .white
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(self.tap(_:))
                )
            )
            overView.backgroundColor = .none
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            overView.addGestureRecognizer(longpress)
        }
    }
    @IBInspectable var image: UIImage? {
        didSet {
            self.imageView.image = image?.withRenderingMode(.alwaysTemplate)
        }
    }
    @IBInspectable var color: UIColor? {
        didSet {
            self.imageView.tintColor = isSwitch ? (isActive ? highlightedColor : color) : color
        }
    }
    @IBInspectable var highlightedColor: UIColor? {
        didSet {
            self.imageView.tintColor = isSwitch ? (isActive ? highlightedColor : color) : color
        }
    }
    @IBInspectable var isSwitch: Bool = false {
        didSet {
            self.imageView.tintColor = isSwitch ? (isActive ? highlightedColor : color) : color
        }
    }
    @IBInspectable var isActive: Bool = false {
        didSet {
            self.imageView.tintColor = isSwitch ? (isActive ? highlightedColor : color) : color
        }
    }
    @IBInspectable var badgeShow: Bool = false {
        didSet {
            self.badgeView.isHidden = !badgeShow
        }
    }
    @IBOutlet weak var imageTopInset: NSLayoutConstraint!
    @IBOutlet weak var imageBottomInset: NSLayoutConstraint!
    @IBOutlet weak var imageRightInset: NSLayoutConstraint!
    @IBOutlet weak var imageLeftInset: NSLayoutConstraint!
    @IBInspectable var imageInset: CGFloat = 14 {
        didSet {
            imageTopInset.constant = imageInset
            imageBottomInset.constant = imageInset
            imageRightInset.constant = imageInset
            imageLeftInset.constant = imageInset
        }
    }
    
    public weak var delegate: StreamButtonDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.streamButton.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.imageView.image = image?.withRenderingMode(.alwaysTemplate)
    }
    
    @objc func tap(_ sender: Any) {
        if self.isSwitch {
            self.isActive = !self.isActive
        }
        self.delegate.streamButton(onTap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .none
            })
        default: self.overView.backgroundColor = .none
        }
    }
}

extension StreamButton: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
