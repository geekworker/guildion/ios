//
//  StreamChannelCounterView.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/25.
//

import Foundation
import UIKit

class StreamChannelCounterView: UIView {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .transparent)
            baseView.layer.cornerRadius = 4
            baseView.clipsToBounds = true
        }
    }
    @IBOutlet weak var leftView: UIView! {
        didSet {
            let angle = 10 * CGFloat.pi / 180.0;
            leftView.transform = CGAffineTransform(rotationAngle: angle)
            leftView.backgroundColor = Theme.themify(key: .backgroundLight).lighter(by: 5)
        }
    }
    @IBOutlet weak var rightView: UIView! {
        didSet {
            let angle = 10 * CGFloat.pi / 180.0;
            rightView.transform = CGAffineTransform(rotationAngle: angle)
            rightView.backgroundColor = Theme.themify(key: .background)
        }
    }
    @IBOutlet weak var currentCountLabel: UILabel! {
        didSet {
            currentCountLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var maxCountLabel: UILabel! {
        didSet {
            maxCountLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    public var repository: StreamChannelEntity = StreamChannelEntity()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.streamChannelCounterView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.backgroundColor = Theme.themify(key: .transparent)
        view.backgroundColor = Theme.themify(key: .transparent)
    }
    
    func setRepository(_ repository: StreamChannelEntity) {
        self.repository = repository
        self.currentCountLabel.text = "\(repository.LiveStream?.Participants?.count ?? 0)"
        self.maxCountLabel.text = "\(repository.max_participant_count)"
        self.isHidden = repository.max_participant_count >= PlanConfig.free_plan_max_participant_count
    }
}
