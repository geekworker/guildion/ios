//
//  SimpleButton.swift
//  nowy-ios
//
//  Created by Apple on 2020/04/21.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol SimpleButtonDelegate: class {
    func simpleButton(onTap simpleButton: SimpleButton)
}

extension SimpleButtonDelegate where Self: UIViewController {
    func simpleButton(onTap simpleButton: SimpleButton) { }
}

@objcMembers
@IBDesignable
class SimpleButton: UIView {
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.layer.cornerRadius = 5
            baseView.clipsToBounds = true
        }
    }
    @IBOutlet weak var textLabel: UILabel! {
        didSet {
            textLabel.textColor = textColor
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(self.tap(_:))
                )
            )
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            overView.addGestureRecognizer(longpress)
        }
    }
    @IBInspectable var text: String? {
        didSet {
            textLabel.text = self.text
        }
    }
    @IBOutlet weak var iconImageViewWidthConstraint: NSLayoutConstraint! {
        didSet {
            isIconShow = false
            iconImageViewWidthConstraint.constant = 0
        }
    }
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.tintColor = textColor
        }
    }
    @IBInspectable var isIconShow: Bool = false {
        didSet {
            iconImageViewWidthConstraint.constant = isIconShow ? 14 : 0
        }
    }
    @IBInspectable var textColor: UIColor? = Theme.themify(key: .backgroundContrast)
    @IBInspectable var normalColor: UIColor? {
        didSet {
            self.baseView.backgroundColor = self.canceled ? canceledColor : normalColor
        }
    }
    @IBInspectable var canceledColor: UIColor? {
        didSet {
            self.baseView.backgroundColor = self.canceled ? canceledColor : normalColor
        }
    }
    var canceled: Bool = false {
        didSet {
            self.baseView.backgroundColor = self.canceled ? canceledColor : normalColor
        }
    }
    public weak var delegate: SimpleButtonDelegate?
    public var loginRequired: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.simpleButton.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    @objc fileprivate func tap(_ sender: UITapGestureRecognizer) {
        guard !canceled else { return }
//        if loginRequired && CurrentUser.getCurrentUserEntity() == nil {
//            BseWireframe.toSession()
//            return
//        } else if loginRequired, let current_user = CurrentUser.getCurrentUserEntity(), !current_user.verified {
//            AuthWireframe.toMailConfirm()
//            return
//        }
        self.delegate?.simpleButton(onTap: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        guard !canceled else { return }
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.backgroundColor = self.canceled ? self.canceledColor?.darker(by: 10) : self.normalColor?.darker(by: 10)
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.baseView.backgroundColor = self.canceled ? self.canceledColor : self.normalColor
            })
        default:
            self.baseView.backgroundColor = self.canceled ? canceledColor : normalColor
        }
    }
}

extension SimpleButton: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
