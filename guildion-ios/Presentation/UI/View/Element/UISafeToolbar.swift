//
//  UISafeToolbar.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/22.
//

import Foundation
import UIKit

public class UISafeToolbar: UIToolbar {
    private static let toolbarHeight: CGFloat = 44
    
    private weak var toolbarContentView: UIView!
    
    public convenience init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: .zero)
        self.insetsLayoutMarginsFromSafeArea = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.insetsLayoutMarginsFromSafeArea = true
    }
    
    public override func didAddSubview(_ subview: UIView) {
        super.didAddSubview(subview)
        
        let viewClassString = String(describing: type(of: subview))
        if viewClassString == "_UIToolbarContentView" {
            toolbarContentView = subview
        }
    }
    
    public override func safeAreaInsetsDidChange() {
        super.safeAreaInsetsDidChange()
        
        invalidateIntrinsicContentSize()
        setNeedsUpdateConstraints()
    }
    
    public override func updateConstraints() {
        super.updateConstraints()
        
        for constraint in toolbarContentView.constraints {
            if isBottomConstraint(constraint) {
                constraint.constant = -safeAreaInsets.bottom
                break
            }
        }
    }
    
    public override var intrinsicContentSize: CGSize {
        let height = UISafeToolbar.toolbarHeight + safeAreaInsets.bottom
        return CGSize(width: bounds.width, height: height)
    }
    
    private func isBottomConstraint(_ constraint: NSLayoutConstraint) -> Bool {
        return constraint.firstAttribute == .bottom &&
               constraint.relation == .equal &&
               constraint.secondItem === toolbarContentView &&
               constraint.secondAttribute == .bottom
    }
}
