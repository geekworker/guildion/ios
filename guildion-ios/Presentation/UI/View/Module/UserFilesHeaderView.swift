//
//  UserFilesHeaderView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/04.
//

import Foundation
import UIKit
import LinearProgressBar
import Gradients

protocol UserFilesHeaderViewDelegate: class {
    func userFilesHeaderView(onTap userFilesHeaderView: UserFilesHeaderView)
}

class UserFilesHeaderView: UIView {
    @IBOutlet weak var gradationView: GradationView! {
        didSet {
//            gradationView.startX = -1
//            gradationView.endX = 1
//            gradationView.startY = 1
//            gradationView.endY = -1
            gradationView.topColor = UIColor.init(hex: "96deda").darker(by: 20)!
            gradationView.bottomColor = UIColor.init(hex: "50c9c3").darker(by: 20)!
        }
    }
    @IBOutlet weak var premiumView: UIView! {
        didSet {
            premiumView.backgroundColor = .none
            premiumView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
            premiumView.layer.cornerRadius = 5
            premiumView.clipsToBounds = true
        }
    }
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var premiumLabelView: UIView! {
        didSet {
            premiumLabelView.layer.cornerRadius = 2
            premiumLabelView.clipsToBounds = true
        }
    }
    @IBOutlet weak var premiumLabel: UILabel!
    @IBOutlet weak var planDetailLabel: UILabel! {
        didSet {
            planDetailLabel.textColor = .white
            planDetailLabel.text = R.string.localizable.increaseStorage()
        }
    }
    @IBOutlet weak var chevronImageView: UIImageView! {
        didSet {
            chevronImageView.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            chevronImageView.tintColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var fileSizeLabel: UILabel! {
        didSet {
            fileSizeLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var maxSizeLabel: UILabel! {
        didSet {
            maxSizeLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var usedStorageLabel: UILabel! {
        didSet {
            usedStorageLabel.textColor = Theme.themify(key: .stringSecondary)
            usedStorageLabel.text = R.string.localizable.usedStorage()
        }
    }
    @IBOutlet weak var progressBar: LinearProgressBar! {
        didSet {
            progressBar.progressValue = 0
            progressBar.barColor = Theme.themify(key: .main)
            progressBar.trackColor = Theme.themify(key: .cancel)
        }
    }
    weak var delegate: UserFilesHeaderViewDelegate?
    var repository: UserEntity = UserEntity()
    static let viewHeight: CGFloat = 220
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.userFilesHeaderView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .backgroundThick)
        self.backgroundColor = Theme.themify(key: .backgroundThick)
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.userFilesHeaderView(onTap: self)
    }
    
    public func setRepository(_ repository: UserEntity) {
        self.repository = repository
        self.fileSizeLabel.text = repository.size
        self.maxSizeLabel.text = "/ \(PlanConfig.free_plan_max_size)"
        self.progressBar.progressValue = CGFloat(repository.size_byte / PlanConfig.free_plan_max_size_byte) * 100
    }
}
