//
//  SearchBrowserBar.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/23.
//

import Foundation
import UIKit

protocol SearchBrowserBarDelegate: class {
    var current_url: URL { get set }
    var domainType: SearchDomainType { get set }
    func searchBrowserBar(_ searchBrowserBar: SearchBrowserBar, searched keyword: String?)
    func searchBrowserBar(_ searchBrowserBar: SearchBrowserBar, accessoryButtonTapped keyword: String?) -> Bool
    func searchBrowserBarDidChange(_ searchBrowserBar: SearchBrowserBar, keyword: String?)
    func searchBrowserBarDidBeginEditing()
    func searchBrowserBarDidEndEditing()
}

extension SearchBrowserBarDelegate where Self: UIViewController {
    func searchBrowserBar(_ searchBrowserBar: SearchBrowserBar, searched keyword: String?) {}
    func searchBrowserBar(_ searchBrowserBar: SearchBrowserBar, accessoryButtonTapped keyword: String?) -> Bool { return true }
    func searchBrowserBarDidChange(_ searchBrowserBar: SearchBrowserBar, keyword: String?) {}
    func searchBrowserBarDidBeginEditing() {}
    func searchBrowserBarDidEndEditing() {}
}

@IBDesignable
class SearchBrowserBar: UIView {
    @IBOutlet weak var searchView: UIView! {
        didSet {
            searchView.backgroundColor = Theme.themify(key: .searchBar)
            searchView.layer.cornerRadius = searchView.frame.height / 2
            searchView.clipsToBounds = true
        }
    }
    @IBOutlet weak var searchField: UITextField! {
        didSet {
            searchField.textColor = Theme.themify(key: .stringContrast)
            searchField.font = Font.FT(size: .FM, family: .L)
            searchField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
            searchField.isHidden = true
            searchField.delegate = self
        }
    }
    @IBOutlet weak var searchLabelLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchLabel: UILabel! {
        didSet {
            searchLabel.text = ""
            searchLabel.textColor = Theme.themify(key: .stringContrast)
            searchLabel.font = Font.FT(size: .FM, family: .L)
        }
    }
    @IBOutlet weak var accessoryButton: UIButton!
    @IBOutlet weak var searchViewTrailingToSafeAreaTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchImage: UIImageView! {
        didSet {
            searchImage.isHidden = true
            searchImage.image = self.delegate?.domainType.image ?? SearchDomainType.google.image
        }
    }
    public weak var delegate: SearchBrowserBarDelegate?

    @IBInspectable
    var buttonText: String? {
        get {
            return accessoryButton?.titleLabel?.text
        }
        set {
            accessoryButton.setTitle(newValue, for: .normal)
        }
    }
    var placeholder: String? {
        get {
            return searchField.placeholder
        }
        set {
            searchField.placeholder = newValue
        }
    }
    var keyword: String? {
        get {
            return searchField.text
        }
        set {
            searchField.text = newValue
        }
    }
    var title: String? {
        get {
            return searchLabel.text
        }
        set {
            searchLabel.text = newValue
        }
    }

    @IBAction func accessoryButtonTapped(_ sender: Any) {
        if self.delegate?.searchBrowserBar(self, accessoryButtonTapped: self.searchField.text) ?? true {
            self.endEditing(true)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.delegate?.searchBrowserBarDidChange(self, keyword: textField.text)
    }

    override func becomeFirstResponder() -> Bool {
        self.searchField.becomeFirstResponder()
        return true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }

    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed("SearchBrowserBar", owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)

        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.backgroundColor = .clear
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.searchField.becomeFirstResponder()
    }

    private func showAccessoryButton() {
        self.searchViewTrailingToSafeAreaTrailingConstraint.constant = 64
        self.searchLabelLeftConstraint.constant = -12
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.searchView.layer.cornerRadius = 5
            self.accessoryButton.isHidden = false
            self.layoutIfNeeded()
        }, completion: nil)
    }

    private func hideAccessoryButton() {
        self.searchViewTrailingToSafeAreaTrailingConstraint.constant = 10
        self.searchLabelLeftConstraint.constant = 12
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.searchView.layer.cornerRadius = self.searchView.frame.height / 2
            self.accessoryButton.isHidden = true
            self.layoutIfNeeded()
        }, completion: nil)
    }
    
    private func showLabel() {
        self.searchLabel.fadeIn(type: .Normal, completed: nil)
        self.searchField.fadeOut(type: .Normal, completed: nil)
        self.searchImage.fadeOut(type: .Normal, completed: nil)
    }

    private func hideLabel() {
        self.searchLabel.fadeOut(type: .Normal, completed: nil)
        self.searchField.fadeIn(type: .Normal, completed: nil)
        self.searchImage.fadeIn(type: .Normal, completed: nil)
    }
    
    public func updateSearchTypeLayout() {
        searchImage.image = self.delegate?.domainType.image ?? SearchDomainType.google.image
    }
    
    public func setLabel(_ url: URL) {
        if let host = url.host {
            self.title = String(describing: host)
        }
    }
}

extension SearchBrowserBar: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showAccessoryButton()
        hideLabel()
        self.delegate?.searchBrowserBarDidBeginEditing()
    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        hideAccessoryButton()
        showLabel()
        self.delegate?.searchBrowserBarDidEndEditing()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.delegate?.searchBrowserBar(self, searched: textField.text)
        self.keyword = ""
        return true
    }
}
