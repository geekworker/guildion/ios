//
//  FolderHeaderView.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/25.
//

import Foundation
import UIKit
import Nuke

protocol FolderHeaderViewDelegate: class {
    func folderHeaderView(onTapMember repository: MemberEntity, in folderHeaderView: FolderHeaderView)
}

class FolderHeaderView: UIView {
    @IBOutlet weak var folderBackgroundImage: UIImageView! {
        didSet {
            folderBackgroundImage.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var effectView: UIVisualEffectView!
    @IBOutlet weak var folderForegroundImage: UIImageView! {
        didSet {
            folderForegroundImage.contentMode = .scaleAspectFill
            folderForegroundImage.image = R.image.videoPlay()!.withRenderingMode(.alwaysTemplate)
            folderForegroundImage.tintColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var countLabel: UILabel! {
        didSet {
            countLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var listImage: UIImageView! {
        didSet {
            listImage.image = R.image.list()!.withRenderingMode(.alwaysTemplate)
            listImage.tintColor = Theme.themify(key: .stringSecondary)
        }
    }
    public weak var delegate: FolderHeaderViewDelegate?
    public var repository: FolderEntity = FolderEntity()
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    static let height: CGFloat = 78
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.folderHeaderView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func setRepository(_ repository: FolderEntity) {
        self.repository = repository
        self.nameLabel.text = repository.name
        if repository.temporary, let guild = GuildConnector.shared.current_guild {
            self.countLabel.text = "\(guild.file_count)"
        } else {
            self.countLabel.text = "\(repository.file_count)"
        }
        self.dateLabel.text = formatter.string(from: repository.updated_at)
    }
    
    @objc func onTapOwner(_ sender: Any) {
        guard let member = self.repository.Member else { return }
        self.delegate?.folderHeaderView(onTapMember: member, in: self)
    }
}
