//
//  YoutubeOverlayView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/25.
//

import Foundation
import UIKit

protocol YoutubeOverlayViewDelegate: class {
    func youtubeOverlayView(shouldAdd youtubeOverlayView: YoutubeOverlayView)
}

class YoutubeOverlayView: UIView {
    weak var delegate: YoutubeOverlayViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.youtubeOverlayView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .transparent)
        self.backgroundColor = Theme.themify(key: .transparent)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.youtubeOverlayView(shouldAdd: self)
    }
}
