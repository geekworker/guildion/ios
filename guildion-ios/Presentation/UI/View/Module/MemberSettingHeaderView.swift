//
//  MemberSettingHeaderView.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/01.
//

import Foundation
import UIKit
import Nuke

class MemberSettingHeaderView: UIView {
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var memberImageView: UIImageView! {
        didSet {
            memberImageView.layer.cornerRadius = memberImageView.frame.width / 2
            memberImageView.contentMode = .scaleAspectFill
        }
    }
    
    static let viewHeight: CGFloat = 140
    
    var repository: MemberEntity = MemberEntity()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.memberSettingHeaderView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .background)
        self.backgroundColor = Theme.themify(key: .background)
    }
    
    public func setRepository(_ repository: MemberEntity) {
        self.repository = repository
        self.nameLabel.text = repository.nickname
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: memberImageView)
        }
    }
}

