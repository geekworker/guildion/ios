//
//  EditModeTabBar.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/26.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol EditModeTabBarDelegate: class {
    func editModeTabBar(shouldTrash editModeTabBar: EditModeTabBar)
    func editModeTabBar(shouldShare editModeTabBar: EditModeTabBar)
}

class EditModeTabBar: UIView {
    @IBOutlet weak var trashButton: UIButton! {
        didSet {
            trashButton.tintColor = Theme.themify(key: .main)
            trashButton.setImage(R.image.trash()!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
            trashButton.setImage(R.image.trash()!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .highlighted)
            trashButton.addTarget(self, action: #selector(onTapTrash(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var shareButton: UIButton! {
        didSet {
            shareButton.tintColor = Theme.themify(key: .main)
            shareButton.setImage(R.image.share()!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
            shareButton.setImage(R.image.share()!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .highlighted)
            shareButton.addTarget(self, action: #selector(onTapShare(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = R.string.localizable.selectItems("\(0)")
        }
    }
    public weak var delegate: EditModeTabBarDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.editModeTabBar.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .backgroundThick)
    }
    
    @objc func onTapTrash(_ sender: Any) {
        guard !self.isHidden else { return }
        self.delegate?.editModeTabBar(shouldTrash: self)
    }
    
    @objc func onTapShare(_ sender: Any) {
        guard !self.isHidden else { return }
        self.delegate?.editModeTabBar(shouldShare: self)
    }
    
    public func setCount(_ count: Int) {
        titleLabel.text = R.string.localizable.selectItems("\(count)")
    }
    
    public func switchMode(_ show: Bool) {
        show ? self.fadeIn(type: .Normal, completed: nil) : self.fadeOut(type: .Normal, completed: nil)
    }
}
