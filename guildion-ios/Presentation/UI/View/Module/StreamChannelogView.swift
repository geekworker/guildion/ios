//
//  StreamChannelogView.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/24.
//

import Foundation
import UIKit
import TTTAttributedLabel
import Nuke

protocol StreamChannelogViewDelegate: class {
    func streamChannelogView(onTap repository: ChannelogEntity, in streamChannelogView: StreamChannelogView)
}

class StreamChannelogView: UIView {
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = Theme.themify(key: .string)
            nameLabel.font = Font.FT(size: .FS, family: .L)
            nameLabel.frame = CGRect(x: nameLabel.frame.minX, y: nameLabel.frame.minY, width: nameLabel.frame.width, height: 16)
            nameLabel.numberOfLines = 1
            nameLabel.isUserInteractionEnabled = true
            setTapGestureRecognizer(nameLabel)
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var fileView: UIView! {
        didSet {
            fileView.layer.shadowColor = Theme.themify(key: .shadow).cgColor
            fileView.layer.shadowOffset = CGSize(width: 2, height: 2)
            setTapGestureRecognizer(fileView)
        }
    }
    @IBOutlet weak var fileImageView: UIImageView! {
        didSet {
            fileImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var messageLabel: TTTAttributedLabel! {
        didSet {
            StreamChannelogView.configureMessageLabel(messageLabel)
        }
    }
    public weak var delegate: StreamChannelogViewDelegate?
    public var repository: ChannelogEntity = ChannelogEntity()
    
    static func configureMessageLabel(_ label: UILabel) {
        label.textColor = Theme.themify(key: .string)
        label.font = Font.FT(size: .FM, family: .B)
        label.numberOfLines = 0
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        paragraphStyle.lineHeightMultiple = 0
        paragraphStyle.lineBreakMode = .byWordWrapping

        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: label.text ?? "")
        attributedString.addAttribute(
            NSAttributedString.Key.paragraphStyle,
            value: paragraphStyle,
            range: NSMakeRange(0, attributedString.length)
        )
        attributedString.addAttribute(
            NSAttributedString.Key.baselineOffset,
            value: 0,
            range: NSMakeRange(0, attributedString.length)
        )
        label.attributedText = attributedString
        label.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.streamChannelogView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .backgroundLight)
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        
        setTapGestureRecognizer(self)
    }
    
    func setRepository(_ repository: ChannelogEntity) {
        guard let stream = repository.Stream else { return }
        self.repository = repository
        self.dateLabel.text = "\(repository.created_at.timeAgo())・\(repository.created_at.getFormattedTime())"
        
        if let file = stream.File {
            self.nameLabel.text = R.string.localizable.streamChannelStartedStream()
            self.messageLabel.text = file.name
            if let url = URL(string: file.thumbnail) {
                Nuke.loadImage(with: url, into: fileImageView)
            } else {
                fileImageView.image = R.image.brandLogo()
            }
        }
        
        if let reference = stream.FileReference, let file = reference.File {
            self.nameLabel.text = R.string.localizable.streamChannelStartedStream()
            self.messageLabel.text = file.name
            if let url = URL(string: file.thumbnail) {
                Nuke.loadImage(with: url, into: fileImageView)
            } else {
                fileImageView.image = R.image.brandLogo()
            }
        }
    }
    
    fileprivate func setTapGestureRecognizer(_ view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTap(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.streamChannelogView(onTap: repository, in: self)
    }
}
