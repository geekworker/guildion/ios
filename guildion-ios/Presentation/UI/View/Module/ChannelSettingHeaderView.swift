//
//  ChannelSettingHeaderView.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/01.
//

import Foundation
import UIKit

class ChannelSettingHeaderView: UIView {
    @IBOutlet weak var channelImageView: UIImageView! {
        didSet {
            channelImageView.tintColor = Theme.themify(key: .stringSecondary)
            channelImageView.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    
    static let viewHeight: CGFloat = 100
    
    var repository: ChannelableProtocol = TextChannelEntity()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.channelSettingHeaderView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .background)
        self.backgroundColor = Theme.themify(key: .background)
    }
    
    func setRepository(_ repository: ChannelableProtocol) {
        self.repository = repository
        self.nameLabel.text = repository.name
        self.channelImageView.image = repository.channelable_type.image
    }
}
