//
//  StreamButtonsView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/20.
//

import Foundation
import UIKit
import AVKit

protocol StreamButtonsViewDelegate: class {
    var muted: Bool { get set }
    func streamButtonsView(didTapPick streamButtonsView: StreamButtonsView)
    func streamButtonsView(didTapInfo streamButtonsView: StreamButtonsView)
    func streamButtonsView(didTapYouTube streamButtonsView: StreamButtonsView)
    func streamButtonsView(didTapMute muted: Bool, streamButtonsView: StreamButtonsView)
}

class StreamButtonsView: UIView {
    @IBOutlet weak var youtubeButton: IconButton! {
        didSet {
            youtubeButton.delegate = self
            youtubeButton.buttonImage = R.image.yt_link_icon()!.withRenderingMode(.alwaysOriginal)
            youtubeButton.imageColor = .clear
            youtubeButton.buttonInset = 4
            youtubeButton.layer.cornerRadius = youtubeButton.bounds.width / 2
            youtubeButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var youtubeButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var youtubeButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var routePickerButton: UIView! {
        didSet {
            routePickerButton.layer.cornerRadius = routePickerButton.frame.width / 2
            routePickerButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var pickMovieButton: IconButton! {
        didSet {
            pickMovieButton.delegate = self
            pickMovieButton.buttonImage = R.image.pickMovie()!.withRenderingMode(.alwaysTemplate)
            pickMovieButton.imageColor = .white
            pickMovieButton.buttonInset = 4
            pickMovieButton.layer.cornerRadius = pickMovieButton.bounds.width / 2
            pickMovieButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var pickMovieButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickMovieButtonHeightConstraint: NSLayoutConstraint!
    lazy var avRoutePickerView: AVRoutePickerView = {
        let pickerView = AVRoutePickerView(frame: CGRect(x: 0, y: 0, width: ButtonType.route.height, height: ButtonType.route.height))
        self.routePickerButton.addSubview(pickerView)
        return pickerView
    }()
    @IBOutlet weak var infoButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoButton: IconButton! {
        didSet {
            infoButton.delegate = self
            infoButton.buttonImage = R.image.list()!.withRenderingMode(.alwaysTemplate)
            infoButton.imageColor = .white
            infoButton.buttonInset = 6
            infoButton.layer.cornerRadius = infoButton.bounds.width / 2
            infoButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var muteButtonViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var muteButtonViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var muteHighlightView: UIView! {
        didSet {
            muteHighlightView.isHidden = true
            muteHighlightView.layer.cornerRadius = muteHighlightView.frame.width / 2
            muteHighlightView.clipsToBounds = true
            muteHighlightView.backgroundColor = (self.delegate?.muted ?? false) ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var muteButton: IconButton! {
        didSet {
            muteButton.delegate = self
            muteButton.buttonImage = R.image.mic()!.withRenderingMode(.alwaysTemplate)
            //muteButton.imageColor = .white
            muteButton.imageColor = (self.delegate?.muted ?? true) ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
            muteButton.buttonInset = 4
            muteButton.layer.cornerRadius = infoButton.bounds.width / 2
            muteButton.clipsToBounds = true
        }
    }
    weak var delegate: StreamButtonsViewDelegate?
    enum ButtonType: Int, CaseIterable {
        case route
        case info
        case pick
        case youtube
        case mute
        case count
        
        var height: CGFloat {
            switch self {
            case .route: return 28
            case .info, .pick, .youtube: return 34
            case .mute: return 36
            default: return 0
            }
        }
        
        var margin: CGFloat {
            switch self {
            case .route: return 16
            case .info, .pick, .mute, .youtube: return 10
            case .count: return 16
            }
        }
    }
    static let viewWidth: CGFloat = 54
    static func viewHeight(_ repositories: [ButtonType]) -> CGFloat {
        let height: CGFloat = repositories.reduce(0) { $0 + $1.height + $1.margin } + ButtonType.count.margin
        return height
    }
    public var repositories: [ButtonType] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.streamButtonsView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        _ = avRoutePickerView
    }
    
    func setRepositories(_ repositories: [ButtonType]) {
        self.repositories = repositories
        self.muteButtonViewHeightConstraint.constant = repositories.contains(.mute) ? ButtonType.mute.height : 0
        self.muteButton.isHidden = !repositories.contains(.mute)
        self.infoButton.isHidden = !repositories.contains(.info)
        self.infoButtonTopConstraint.constant = repositories.contains(.info) ? ButtonType.info.margin : 0
        self.infoButtonHeightConstraint.constant = repositories.contains(.info) ? ButtonType.info.height : 0
        self.pickMovieButton.isHidden = !repositories.contains(.pick)
        self.pickMovieButtonTopConstraint.constant = repositories.contains(.pick) ? ButtonType.pick.margin : 0
        self.pickMovieButtonHeightConstraint.constant = repositories.contains(.pick) ? ButtonType.pick.height : 0
        self.youtubeButton.isHidden = !repositories.contains(.youtube)
        self.youtubeButtonTopConstraint.constant = repositories.contains(.youtube) ? ButtonType.youtube.margin : 0
        self.youtubeButtonHeightConstraint.constant = repositories.contains(.youtube) ? ButtonType.youtube.height : 0
        self.muteButton.isHidden = !repositories.contains(.mute)
        self.muteButtonViewTopConstraint.constant = repositories.contains(.mute) ? ButtonType.mute.margin : 0
        self.muteButtonViewHeightConstraint.constant = repositories.contains(.mute) ? ButtonType.mute.height : 0
        self.muteButton.imageColor = (self.delegate?.muted ?? true) ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
        //self.muteHighlightView.backgroundColor = (self.delegate?.muted ?? false) ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
    }
}

extension StreamButtonsView: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        switch iconButton {
        case pickMovieButton: self.delegate?.streamButtonsView(didTapPick: self)
        case infoButton: self.delegate?.streamButtonsView(didTapInfo: self)
        case youtubeButton: self.delegate?.streamButtonsView(didTapYouTube: self)
        case muteButton:
            muteButton.imageColor = !(self.delegate?.muted ?? false) ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
            self.delegate?.streamButtonsView(didTapMute: !(self.delegate?.muted ?? false), streamButtonsView: self)
        default: break
        }
    }
}

extension StreamButtonsView: ActionButtonDelegate {
    func actionButton(onTap actionButton: ActionButton) {
        muteHighlightView.backgroundColor = !(self.delegate?.muted ?? true) ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
    }
}
