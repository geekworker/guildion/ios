//
//  TypingIndicatorView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/26.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class TypingIndicatorView: UIView {
    @IBOutlet weak var textLabel: UILabel! {
        didSet {
            textLabel.text = ""
            textLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView! {
        didSet {
            activityIndicatorView.color = Theme.themify(key: .backgroundContrast)
            activityIndicatorView.type = .ballPulse
            activityIndicatorView.startAnimating()
        }
    }
    public var repositories: MemberEntities = MemberEntities()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.typingIndicatorView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.backgroundColor = Theme.themify(key: .transparent)
    }
    
    func setRepositories(_ repositories: MemberEntities) {
        self.repositories = repositories
        self.updateLayout()
    }
    
    func addRepository(_ repository: MemberEntity) {
        guard !repositories.contains(repository) else { return }
        self.repositories.append(repository)
        self.updateLayout()
    }
    
    func removeRepository(_ repository: MemberEntity) {
        self.repositories.remove(repository)
        self.updateLayout()
    }
    
    func updateLayout() {
        guard repositories.count > 0 else {
            self.isHidden = true
            return
        }
        self.isHidden = false
        self.textLabel.text = R.string.localizable.channelTyping(repositories.compactMap({ $0.nickname }).joined(separator: ", "))
    }
}
