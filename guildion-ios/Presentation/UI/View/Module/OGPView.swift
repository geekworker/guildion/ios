//
//  OGPView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/26.
//

import Foundation
import UIKit
import FRHyperLabel
import Nuke
import TTTAttributedLabel
import URLEmbeddedView

protocol OGPViewDelegate: class {
    func ogpView(onClickURL url: URL, in ogpView: OGPView)
}

class OGPView: UIView {
    @IBOutlet weak var hostLabel: UILabel! {
        didSet {
            hostLabel.text = ""
            hostLabel.textColor = Theme.themify(key: .string)
            hostLabel.font = Font.FT(size: .S, family: .L)
        }
    }
    @IBOutlet weak var rightContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            rightContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet weak var rightContainerView: UIView!
    @IBOutlet weak var ogpImageView: UIImageView! {
        didSet {
            ogpImageView.image = UIImage()
            ogpImageView.contentMode = .scaleAspectFill
            ogpImageView.layer.cornerRadius = 5
            ogpImageView.clipsToBounds = true
            ogpImageView.isUserInteractionEnabled = true
            ogpImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapURL(_:))))
        }
    }
    @IBOutlet weak var ogpTitleLabel: UILabel! {
        didSet {
            ogpTitleLabel.textColor = Theme.themify(key: .link)
            ogpTitleLabel.font = Font.FT(size: .FM, family: .B)
            ogpTitleLabel.text = ""
            ogpTitleLabel.numberOfLines = 2
            ogpTitleLabel.isUserInteractionEnabled = true
            ogpTitleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapURL(_:))))
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 7
            paragraphStyle.lineHeightMultiple = 0
            paragraphStyle.lineBreakMode = .byWordWrapping
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: ogpTitleLabel.text ?? "")
            attributedString.addAttribute(
                NSAttributedString.Key.paragraphStyle,
                value: paragraphStyle,
                range: NSMakeRange(0, attributedString.length)
            )
            attributedString.addAttribute(
                NSAttributedString.Key.baselineOffset,
                value: 0,
                range: NSMakeRange(0, attributedString.length)
            )
            ogpTitleLabel.attributedText = attributedString
        }
    }
    @IBOutlet weak var ogpDescriptionLabel: TTTAttributedLabel! {
        didSet {
            ogpDescriptionLabel.textColor = Theme.themify(key: .string)
            ogpDescriptionLabel.font = Font.FT(size: .FS, family: .L)
            ogpDescriptionLabel.text = ""
        }
    }
    weak var delegate: OGPViewDelegate?
    public var repository: URL = SearchDomainType.google.url
    
    func calcHeight() -> CGFloat {
        return ChannelMessageSizeCalculator.ogpViewHeight
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.ogpView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        view.backgroundColor = Theme.themify(key: .backgroundLight)
    }
    
    func setRepository(_ repository: URL) {
        self.repository = repository
        self.fetchOGP(repository)
    }
    
    func fetchOGP(_ url: URL) {
        DispatchQueue.main.async {
            OGDataProvider.shared.fetchOGData(urlString: url.absoluteString) { result, error in
                DispatchQueue.main.async {
                    self.hostLabel.text = url.host
                    self.ogpTitleLabel.text = result.pageTitle ?? url.absoluteString
                    if let imageURL = result.imageUrl {
                        self.rightContainerViewConstraint.constant = 80
                        Nuke.loadImage(with: imageURL, into: self.ogpImageView)
                    } else if let imageURL = URL(string: DataConfig.Image.faviconURL + (url.host ?? "")) {
                        self.rightContainerViewConstraint.constant = 50
                        Nuke.loadImage(with: imageURL, into: self.ogpImageView)
                    } else {
                        self.rightContainerViewConstraint.constant = 60
                        self.ogpImageView.image = R.image.compass()
                    }
                    self.ogpDescriptionLabel.text = "" // result.pageDescription
                }
            }
        }
    }
    
    @objc func onTapURL(_ sender: Any) {
        self.delegate?.ogpView(onClickURL: self.repository, in: self)
    }
}
