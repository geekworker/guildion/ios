//
//  MiniScreenPlayerCoverView.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/07.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import RxSwift

class MiniScreenPlayerCoverView: UIView {
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView! {
        didSet {
            activityIndicatorView.type = .ballClipRotateMultiple
            activityIndicatorView.startAnimating()
        }
    }
    public weak var syncManager: SyncManagerImpl? {
        didSet {
            self.startObserveManager()
        }
    }
    private var disposeBag: DisposeBag = DisposeBag()
    public weak var delegate: SyncManagerPlayable?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.miniScreenPlayerCoverView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .transparent)
        self.backgroundColor = Theme.themify(key: .transparent)
    }
    
    private func startObserveManager() {
        guard let manager = self.syncManager else { return }
        manager.loadingEvent
            .subscribe(
                onNext: { [weak self] loading in
                    self?.activityIndicatorView.isHidden = !loading
                }
            ).disposed(
                by: disposeBag
            )
    }
}
