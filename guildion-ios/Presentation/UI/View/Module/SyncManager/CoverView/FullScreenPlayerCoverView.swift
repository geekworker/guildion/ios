//
//  FullScreenPlayerCoverView.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/14.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import RxSwift

class FullScreenPlayerCoverView: UIView {
    @IBOutlet weak var thumbnailImageView: UIImageView! {
        didSet {
            thumbnailImageView.isHidden = true // !loading
            thumbnailImageView.image = thumbnailImage
            thumbnailImageView.backgroundColor = Theme.themify(key: .backgroundThick)
        }
    }
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView! {
        didSet {
            activityIndicatorView.type = .ballClipRotateMultiple
            activityIndicatorView.startAnimating()
        }
    }
    @IBInspectable var thumbnailImage: UIImage = R.image.brandLogo()!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var _filterView: UIView! {
        didSet {
            _filterView.backgroundColor = .black
            _filterView.alpha = 0.5
        }
    }
    @IBOutlet weak var loopButton: IconButton! {
        didSet {
            loopButton.buttonImage = R.image.loop()!.withRenderingMode(.alwaysTemplate)
            loopButton.delegate = self
            loopButton.buttonInset = 0
            loopButton.layer.cornerRadius = loopButton.bounds.width / 2
            loopButton.clipsToBounds = true
            loopButton.isHidden = false
            updateLoopButtonLayout()
        }
    }
    @IBOutlet weak var reloadButton: IconButton! {
        didSet {
            reloadButton.buttonImage = R.image.reload()!.withRenderingMode(.alwaysTemplate)
            reloadButton.imageColor = Theme.themify(key: .playerTint)
            reloadButton.delegate = self
            reloadButton.buttonInset = 5
            reloadButton.layer.cornerRadius = reloadButton.bounds.width / 2
            reloadButton.clipsToBounds = true
            reloadButton.isHidden = true
        }
    }
    @IBOutlet weak var exitButton: IconButton! {
        didSet {
            exitButton.buttonImage = R.image.stoppingCircle()!.withRenderingMode(.alwaysTemplate)
            exitButton.imageColor = Theme.themify(key: .warning)
            exitButton.delegate = self
            exitButton.buttonInset = 5
            exitButton.layer.cornerRadius = exitButton.bounds.width / 2
            exitButton.clipsToBounds = true
            exitButton.isHidden = false
        }
    }
    @IBOutlet weak var switchScreenButton: IconButton! {
        didSet {
            switchScreenButton.buttonImage = R.image.exitscreen()!.withRenderingMode(.alwaysTemplate)
            switchScreenButton.imageColor = Theme.themify(key: .playerTint)
            switchScreenButton.delegate = self
            switchScreenButton.buttonInset = 5
            switchScreenButton.layer.cornerRadius = switchScreenButton.bounds.width / 2
            switchScreenButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var playButton: IconButton! {
        didSet {
            playButton.buttonImage = (self.syncManager?.playing ?? false) ? R.image.stopping()!.withRenderingMode(.alwaysTemplate) : R.image.playing()!.withRenderingMode(.alwaysTemplate)
            playButton.imageColor = Theme.themify(key: .playerTint)
            playButton.delegate = self
            playButton.buttonInset = 5
            playButton.layer.cornerRadius = playButton.bounds.width / 2
            playButton.clipsToBounds = true
            playButton.isHidden = true
        }
    }
    @IBOutlet weak var rightCutButton: IconButton! {
        didSet {
            rightCutButton.buttonImage = R.image.rightCut()!.withRenderingMode(.alwaysTemplate)
            rightCutButton.imageColor = (self.delegate?.nextCuttable ?? false) ? Theme.themify(key: .playerTint) : Theme.themify(key: .playerCancel)
            rightCutButton.delegate = self
            rightCutButton.buttonInset = 5
            rightCutButton.layer.cornerRadius = rightCutButton.bounds.width / 2
            rightCutButton.clipsToBounds = true
            rightCutButton.isHidden = true
        }
    }
    @IBOutlet weak var leftCutButton: IconButton! {
        didSet {
            leftCutButton.buttonImage = R.image.leftCut()!.withRenderingMode(.alwaysTemplate)
            leftCutButton.imageColor = (self.delegate?.backCuttable ?? false) ? Theme.themify(key: .playerTint) : Theme.themify(key: .playerCancel)
            leftCutButton.delegate = self
            leftCutButton.buttonInset = 2
            leftCutButton.buttonInset = 5
            leftCutButton.layer.cornerRadius = leftCutButton.bounds.width / 2
            leftCutButton.clipsToBounds = true
            leftCutButton.isHidden = true
        }
    }
    @IBOutlet weak var incrementDoubleTapView: DoubleTapSeekView! {
        didSet {
            incrementDoubleTapView.delegate = self
            incrementDoubleTapView.incrementable = true
        }
    }
    @IBOutlet weak var decrementDoubleTapView: DoubleTapSeekView! {
        didSet {
            decrementDoubleTapView.delegate = self
            decrementDoubleTapView.incrementable = false
        }
    }
    @IBOutlet weak var playTimeLabel: UILabel! {
        didSet {
            playTimeLabel.textColor = Theme.themify(key: .playerTint)
        }
    }
    @IBOutlet weak var slider: StreamSlider! {
        didSet {
            slider.tintColor = Theme.themify(key: .main)
            slider.thumbColor = Theme.themify(key: .main)
            self.slider.trackHeight = 4
            slider.thumbRadius = 13
            slider.value = 0
            slider.isContinuous = true
            self.slider.addTarget(self, action: #selector(onSeekStartSlider(_:)), for: .touchDown)
            self.slider.addTarget(self, action: #selector(onSeekingSlider(_:)), for: .valueChanged)
            self.slider.addTarget(self, action: #selector(onSeekEndSlider(_:)), for: [.touchUpInside, .touchUpOutside, .touchCancel])
            slider.isUserInteractionEnabled = false
        }
    }
    private var disposeBag: DisposeBag = DisposeBag()
    private var tapHiddenTimer: Timer = Timer()
    var showing: Bool = false {
        didSet {
            self.showing ? self.filterView.fadeIn(type: .Fast, completed: nil) : self.filterView.fadeOut(type: .Fast, completed: nil)
            if showing {
                tapHiddenTimer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { (timer) in
                    guard self.showing else { return }
                    self.showing = false
                    self.tapHiddenTimer.invalidate()
                })
            } else {
                self.tapHiddenTimer.invalidate()
            }
        }
    }
    public weak var delegate: SyncManagerPlayable?
    public weak var syncManager: SyncManagerImpl? {
        didSet {
            self.startObserveManager()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.fullScreenPlayerCoverView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tap)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.onDoubleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        view.addGestureRecognizer(doubleTap)
        
        tap.require(toFail: doubleTap)
    }
    
    private func startObserveManager() {
        guard let manager = self.syncManager else { return }
        manager.playerDurationEvent
            .subscribe(
                onNext: { [weak self] duration in
                    self?.setDuration(duration: duration)
                }
            ).disposed(
                by: disposeBag
            )
        
        manager.currentDurationEvent
            .subscribe(
                onNext: { [weak self] duration in
                    self?.updateSlider(current_time: duration)
                }
            ).disposed(
                by: disposeBag
            )
        
        manager.playingEvent
            .subscribe(
                onNext: { [weak self] playing in
                    self?.playButton.buttonImage = playing ? R.image.stopping()!.withRenderingMode(.alwaysTemplate) : R.image.playing()!.withRenderingMode(.alwaysTemplate)
                }
            ).disposed(
                by: disposeBag
            )
        
        manager.pausedEvent
            .subscribe(
                onNext: { [weak self] paused in
                    self?.playButton.buttonImage = !paused ? R.image.stopping()!.withRenderingMode(.alwaysTemplate) : R.image.playing()!.withRenderingMode(.alwaysTemplate)
                }
            ).disposed(
                by: disposeBag
            )
        
        manager.loadingEvent
            .subscribe(
                onNext: { [weak self] loading in
                    self?.activityIndicatorView.isHidden = !loading
                    self?.playButton.isHidden = loading
                    self?.leftCutButton.isHidden = loading
                    self?.rightCutButton.isHidden = loading
                    self?.slider.isUserInteractionEnabled = !loading
                    self?.incrementDoubleTapView.isHidden = loading
                    self?.decrementDoubleTapView.isHidden = loading
                }
            ).disposed(
                by: disposeBag
            )
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.showing = !self.showing
    }
    
    @objc func onDoubleTap(_ sender: UITapGestureRecognizer) {
        guard !(self.syncManager?.loading ?? true) else { return }
        let location = sender.location(in: self)
        let incrementable = location.x > incrementDoubleTapView.frame.minX
        self.showing = false
        incrementable ? incrementDoubleTapView.doubleTapped(sender) : decrementDoubleTapView.doubleTapped(sender)
    }
    
    func switchShowingForce(isHidden: Bool) {
        self.showing = false
        self.filterView.isHidden = isHidden
        UIView.animate(withDuration: 0, animations: {
            self.slider.trackHeight = self.showing ? 4 : 2
            self.slider.layoutIfNeeded()
            self.slider.layoutSubviews()
        })
        self.tapHiddenTimer.invalidate()
    }
    
    func updateLayout() {
        self.incrementDoubleTapView.updateLayout(incrementable: true, height: UIDevice.current.orientation == .portrait ? UIScreen.main.bounds.width : UIScreen.main.bounds.height)
        self.decrementDoubleTapView.updateLayout(incrementable: false, height: UIDevice.current.orientation == .portrait ? UIScreen.main.bounds.width : UIScreen.main.bounds.height)
    }
    
    fileprivate var seeking: Bool = false
    fileprivate var onSeekingValue: Float?
    @objc func onSeekStartSlider(_ sender: Any) {
        self.onSeekingValue = self.slider.value
        seeking = true
    }
    
    @objc func onSeekingSlider(_ sender: Any) {
        self.onSeekingValue = self.slider.value
    }
    
    @objc func onSeekEndSlider(_ sender: Any) {
        seeking = false
        self.onSeekingValue = slider.value
        let seconds = slider.value * (self.syncManager?.player_duration ?? 0)
        self.delegate?.seekDispatch(at: Float(seconds))
    }
    
    public func setDuration(duration: Float) {
        self.playTimeLabel.text = "0:00 / \(FileEntity.getFormattedDuration(duration: TimeInterval(duration)))"
    }
    
    func updateSlider(current_time: Float) {
        if let value = self.onSeekingValue {
            guard value < current_time / (self.syncManager?.player_duration ?? 0) + 0.05, value > current_time / (self.syncManager?.player_duration ?? 0) - 0.05 else { return }
        }
        if let manager = self.syncManager, manager.player_duration != .zero {
            if !seeking {
                slider.value = current_time / manager.player_duration
                self.onSeekingValue = nil
            }
            self.playTimeLabel.text = "\(FileEntity.getFormattedDuration(duration: TimeInterval(current_time))) / \(FileEntity.getFormattedDuration(duration: TimeInterval(manager.player_duration)))"
        }
    }
    
    func updateRightCutLayout() {
        rightCutButton.imageColor = (self.delegate?.nextCuttable ?? false) ? Theme.themify(key: .playerTint) : Theme.themify(key: .playerCancel)
    }
    
    func updateLeftCutLayout() {
        leftCutButton.imageColor = (self.delegate?.backCuttable ?? false) ? Theme.themify(key: .playerTint) : Theme.themify(key: .playerCancel)
    }
    
    func updateLoopButtonLayout() {
        loopButton.imageColor = (self.delegate?.loopable ?? false) ? Theme.themify(key: .main) : Theme.themify(key: .playerTint)
    }
    
    func getCurrentTime() -> Float {
        if let manager = self.syncManager, manager.player_duration != .zero {
            return slider.value * manager.player_duration
        } else {
            return 0
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let seconds = sender.value * (self.syncManager?.player_duration ?? 0)
        if seeking {
            self.updateSlider(current_time: seconds)
        } else {
            self.delegate?.seekDispatch(at: seconds)
        }
    }
}

extension FullScreenPlayerCoverView: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        switch iconButton {
        case playButton:
            (self.syncManager?.playing ?? false) ? self.delegate?.pauseDispatch() : self.delegate?.playDispatch()
        case rightCutButton:
            guard self.delegate?.nextCuttable ?? false else { break }
            self.delegate?.nextCut()
        case leftCutButton:
            guard self.delegate?.backCuttable ?? false else { break }
            self.delegate?.backCut()
        case switchScreenButton:
            self.delegate?.makeExitFullScreen()
        case exitButton:
            self.delegate?.close()
        case reloadButton:
            self.delegate?.reload()
        case loopButton:
            self.delegate?.switchLoop(loopable: self.delegate?.loopable ?? false)
        default: break
        }
    }
}

extension FullScreenPlayerCoverView: DoubleTapSeekViewDelegate {
    func doubleTapSeekView(shouldSeek count: Float, incrementable: Bool, in doubleTapSeekView: DoubleTapSeekView) {
        self.switchShowingForce(isHidden: true)
        let duration = self.getCurrentTime() + (incrementable ? count : -count)
        self.updateSlider(current_time: duration)
        self.delegate?.seekDispatch(at: duration)
    }
}
