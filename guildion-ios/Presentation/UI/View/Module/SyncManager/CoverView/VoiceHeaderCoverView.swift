//
//  VoiceHeaderCoverView.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/13.
//

import Foundation
import UIKit

protocol VoiceHeaderCoverViewDelegate: class {
}

class VoiceHeaderCoverView: UIView {
    @IBOutlet weak var participantCollectionView: ParticipantCollectionView!
    public weak var delegate: VoiceHeaderCoverViewDelegate?
    public var repositories: ParticipantEntities = ParticipantEntities()
    public var current_member: MemberEntity = MemberEntity()
    public var current_participant: ParticipantEntity? {
        return repositories.filter({ $0.Member == self.current_member }).first
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.voiceHeaderCoverView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .transparent)
        self.backgroundColor = Theme.themify(key: .transparent)
    }
    
    func setRepositories(_ repositories: ParticipantEntities, current_member: MemberEntity) {
        self.configureCollectionView()
        self.repositories = repositories
        self.participantCollectionView.setRepositories(repositories)
    }
}

extension VoiceHeaderCoverView: UICollectionViewDelegate, UICollectionViewDataSource {
    fileprivate func configureCollectionView() {
        participantCollectionView.configure(row: 1, size: self.participantCollectionView.bounds.size)
        participantCollectionView.delegate = self
        participantCollectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return participantCollectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return participantCollectionView.collectionView(cellForItemAt: indexPath)
    }
}

extension VoiceHeaderCoverView: StreamChannelViewControllerDelegate {
    func streamChannelViewController(didChange meterLevel: Float, in streamChannelViewController: StreamChannelViewController) {
        current_participant?.meterLevel = meterLevel
    }
    
    func streamChannelViewController(didChange meterLevel: Float, of peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController) {
        guard let participant = self.repositories.filter({ $0.Member?.UserId == peerConnection.user?.id }).first else { return }
        participant.meterLevel = meterLevel
        print(participant.delegate, participant.delegate is ParticipantCollectionViewCell)
    }
    
    func streamChannelViewController(didAdd peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController) {
    }
    
    func streamChannelViewController(didRemove peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController) {
    }
}
