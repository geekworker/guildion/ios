//
//  SyncManagerPlayable.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/13.
//

import Foundation

protocol SyncManagerPlayable: class {
    var loopable: Bool { get set }
    var nextCuttable: Bool { get }
    var backCuttable: Bool { get }
    
    func play()
    func playDispatch()
    func pause()
    func pauseDispatch()
    func seek(at: Float)
    func seekDispatch(at: Float)
    func reload()
    func close()
    func switchLoop(loopable: Bool)
    func nextCut()
    func backCut()
    func makeFullScreen()
    func makeMiniScreen()
    func makeExitMiniScreen()
    func makeExitFullScreen()
}
