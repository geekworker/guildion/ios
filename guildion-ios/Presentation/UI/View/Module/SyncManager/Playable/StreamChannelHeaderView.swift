//
//  StreamChannelHeaderView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/19.
//

import Foundation
import UIKit
import PKHUD
import AVFoundation
import AVKit
import MediaPlayer
import YoutubePlayer_in_WKWebView
import Hero
import NVActivityIndicatorView
import Pastel

protocol StreamChannelHeaderViewDelegate: class  {
    var syncPlayManager: SyncPlayManagerModel { get set }
    var movieConnectable: Bool { get }
    func streamChannelHeaderView(toStreamableSelects repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(didPlay repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(didPlayTime duration: Float, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(didPause repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(didSeek repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(willStart repository: StreamEntity, loggable: Bool, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(willSync repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(didChangeStatus status: SyncPlayManagerStatus, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(shouldFullScreen streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(shouldExitFullScreen streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(shouldReload repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(shouldClose repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(enableEndIncrement repository: StreamableEntity, in streamChannelHeaderView: StreamChannelHeaderView) -> Bool
    func streamChannelHeaderView(becomeReady streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(seekReady streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(switchLoop is_loop: Bool, in streamChannelHeaderView: StreamChannelHeaderView)
    func streamChannelHeaderView(didInterrupt repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView)
}

class StreamChannelHeaderView: UIView, SyncManagerPlayable {
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var voiceHeaderViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var voiceHeaderCoverView: VoiceHeaderCoverView! {
        didSet {
            voiceHeaderCoverView.isHidden = true
            voiceHeaderCoverView.delegate = self
        }
    }
    @IBOutlet weak var miniScreenPlayerCoverView: MiniScreenPlayerCoverView! {
        didSet {
            miniScreenPlayerCoverView.isHidden = true
            miniScreenPlayerCoverView.delegate = self
        }
    }
    @IBOutlet weak var fullScreenPlayerCoverView: FullScreenPlayerCoverView! {
        didSet {
            fullScreenPlayerCoverView.isHidden = true
            fullScreenPlayerCoverView.delegate = self
        }
    }
    @IBOutlet weak var playerCoverView: PlayerCoverView! {
        didSet {
            playerCoverView.delegate = self
        }
    }
    @IBOutlet weak var pastelView: PastelView! {
        didSet {
            pastelView.isHidden = true
        }
    }
    @IBOutlet weak var notfoundView: UIView! {
        didSet {
            notfoundView.isHidden = true
        }
    }
    @IBOutlet weak var notfoundImage: UIImageView!
    @IBOutlet weak var notfoundCoverView: UIView!
    @IBOutlet weak var notfoundTitleLabel: UILabel! {
        didSet {
            self.notfoundTitleLabel.text = R.string.localizable.streamNotfoundTitle()
            self.notfoundTitleLabel.textColor = Theme.themify(key: .string)
            self.notfoundTitleLabel.font = Font.FT(size: .M, family: .B)
        }
    }
    @IBOutlet weak var notfoundButton: SimpleButton! {
        didSet {
            self.notfoundButton.text = R.string.localizable.streamNotfoundPick()
            self.notfoundButton.canceled = false
            self.notfoundButton.normalColor = Theme.themify(key: .cancel)
            self.notfoundButton.textColor = Theme.themify(key: .string)
            self.notfoundButton.delegate = self
        }
    }
    
    lazy var wkytPlayerViewSyncManager: WKYTPlayerViewSyncManager = {
        let manager = WKYTPlayerViewSyncManager()
        manager.delegate = self
        return manager
    }()
    lazy var avPlayerViewSyncManager: AVPlayerViewSyncManager = {
        let manager = AVPlayerViewSyncManager()
        manager.delegate = self
        return manager
    }()
    public var reloading: Bool = false
    public var syncing: Bool = false
    public var loopable: Bool = false {
        didSet {
            playerCoverView.updateLoopButtonLayout()
            fullScreenPlayerCoverView.updateLoopButtonLayout()
        }
    }
    weak var delegate: StreamChannelHeaderViewDelegate?
    public var repository: StreamEntity = StreamEntity()
    public var repositories: ParticipantEntities = ParticipantEntities()
    public var folder: FolderEntity = FolderEntity()
    public var nextStream: StreamableEntity?
    public var backStream: StreamableEntity?
    public var nextCuttable: Bool {
        nextStream != nil
    }
    public var backCuttable: Bool {
        backStream != nil
    }
    public var screenMode: StreamScreenType = .normal {
        didSet {
            self.updateScreenLayout()
        }
    }
    public var playerMode: StreamPlayerType {
        switch self.repository.mode {
        case .fileReferenceYouTube, .fileYouTube: return .youtube
        case .fileReferenceMovie, .fileMovie: return .avplayer
        case .other: return .other
        }
    }
    
    public var current_manager: SyncManagerImpl {
        switch self.playerMode {
        case .avplayer: return self.avPlayerViewSyncManager
        case .youtube: return self.wkytPlayerViewSyncManager
        default: return self.wkytPlayerViewSyncManager
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }

    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.streamChannelHeaderView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)

        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.backgroundColor = Theme.themify(key: .transparent)
        
        self.updateScreenLayout()
    }
    
    fileprivate func initializeState() {
        self.playerCoverView.updateLayout()
        self.syncing = true
        self.avPlayerViewSyncManager.disconnect()
        self.wkytPlayerViewSyncManager.disconnect()
        self.updateSlider(current_time: 0)
    }

    func setRepository(repository: StreamEntity) {
        self.repository = repository
        self.configureStream()
        self.updateScreenLayout()
        self.playerCoverView.syncManager = self.current_manager
        self.fullScreenPlayerCoverView.syncManager = self.current_manager
        self.miniScreenPlayerCoverView.syncManager = self.current_manager
        self.delegate?.streamChannelHeaderView(didPlayTime: 0, in: self)
    }
    
    func setRepositories(_ repositories: ParticipantEntities, current_member: MemberEntity) {
        self.repositories = repositories
        self.voiceHeaderCoverView.setRepositories(repositories, current_member: current_member)
    }

    func setRepository(repository: FolderEntity) {
        self.folder = repository
    }
    
    func setNextStream(repository: StreamableEntity?) {
        self.nextStream = repository
    }
    
    func setBackStream(repository: StreamableEntity?) {
        self.backStream = repository
    }

    func play() {
        self.current_manager.play()
    }
    
    func playDispatch() {
        self.play()
        self.delegate?.streamChannelHeaderView(didPlay: repository, in: self)
    }
    
    func pause() {
        self.current_manager.pause()
    }
    
    func pauseDispatch() {
        self.pause()
        self.delegate?.streamChannelHeaderView(didPause: repository, in: self)
    }

    func seek(at: Float) {
        self.current_manager.seek(at: at)
        self.syncing = true
    }
    
    func seekDispatch(at: Float) {
        self.seek(at: at)
        self.delegate?.streamChannelHeaderView(didSeek: repository, at: at, in: self)
    }
    
    func playAfterSeeking() {
        self.current_manager.handleAfterSeeking()
        self.delegate!.syncPlayManager.ready_status == .readyToPlay ? self.playDispatch() : self.pauseDispatch()
    }
    
    func playAfterSyncing() {
        self.current_manager.handleAfterSyncing()
        self.delegate!.syncPlayManager.ready_status == .readyToPlay ? self.playDispatch() : self.pauseDispatch()
    }
    
    func syncStatusAfterSeek() {
        self.current_manager.syncStatusAfterSeek()
        self.delegate!.syncPlayManager.ready_status == .readyToPlay ? self.playDispatch() : self.pauseDispatch()
    }
    
    func seekAndPause(at: Float) {
        self.current_manager.seekAndPause(at: at)
    }
    
    func seekAndPlay(at: Float) {
        self.current_manager.seekAndPlay(at: at)
    }

    func makeFullScreen() {
        self.screenMode = .full
        self.delegate?.streamChannelHeaderView(shouldFullScreen: self)
    }
    
    func makeMiniScreen() {
        self.screenMode = .mini
    }

    func makeExitFullScreen() {
        self.screenMode = .normal
        self.delegate?.streamChannelHeaderView(shouldExitFullScreen: self)
    }
    
    func makeExitMiniScreen() {
        self.screenMode = .normal
    }
    
    fileprivate func configureNotfoundView() {
        self.notfoundView.isHidden = !(repository.id == 0 || repository.id == nil || repository.mode == .other)
    }
    
    fileprivate var pastelViewAnimated: Bool = false
    fileprivate func configurePastelView() {
        self.pastelView.isHidden = (self.delegate?.movieConnectable ?? true)
        guard !self.pastelView.isHidden && !pastelViewAnimated else { return }
        self.pastelView.startPastelPoint = .bottomLeft
        self.pastelView.endPastelPoint = .topRight
        self.pastelView.animationDuration = 2.0
        pastelView.setColors(
            [
                UIColor(hex: "3ab5b0").darker(by: 15)!,
                UIColor(hex: "3d99be").darker(by: 15)!,
                UIColor(hex: "56317a").darker(by: 15)!,
            ]
        )
        self.pastelView.startAnimation()
        self.pastelViewAnimated = true
    }
    
    func updateScreenLayout() {
        configureNotfoundView()
        configurePastelView()
        switch self.screenMode {
        case .full:
            self.playerCoverView.isHidden = true
            self.miniScreenPlayerCoverView.isHidden = true
            self.fullScreenPlayerCoverView.isHidden = !self.notfoundView.isHidden
            self.voiceHeaderCoverView.isHidden = true
            self.fullScreenPlayerCoverView.showing = false
            self.fullScreenPlayerCoverView.updateLayout()
        case .mini:
            self.playerCoverView.isHidden = true
            self.fullScreenPlayerCoverView.isHidden = true
            self.miniScreenPlayerCoverView.isHidden = !self.notfoundView.isHidden
            self.voiceHeaderCoverView.isHidden = true
        case .normal:
            self.playerCoverView.isHidden = !self.notfoundView.isHidden
            self.miniScreenPlayerCoverView.isHidden = true
            self.fullScreenPlayerCoverView.isHidden = true
            self.voiceHeaderCoverView.isHidden = true
            //self.playerCoverView.updateLayout()
        }
        
        if !(self.delegate?.movieConnectable ?? true) {
            self.voiceHeaderCoverView.isHidden = false
            self.playerCoverView.isHidden = true
            self.miniScreenPlayerCoverView.isHidden = true
            self.notfoundView.isHidden = true
            self.fullScreenPlayerCoverView.isHidden = true
            self.voiceHeaderViewTopConstraint.constant = self.screenMode == .mini ? 0 : 44
        }
    }
    
    func reload() {
        guard !reloading else { return }
        self.reloading = true
        self.delegate?.streamChannelHeaderView(shouldReload: self.repository, at: self.getCurrentTime(), in: self)
    }
    
    func switchLoop(loopable: Bool) {
        self.loopable = !self.loopable
        self.delegate?.streamChannelHeaderView(switchLoop: self.loopable, in: self)
    }
    
    func nextCut() {
        switch repository.mode {
        case .fileReferenceYouTube, .fileReferenceMovie:
            guard let increment = nextStream as? FileReferenceEntity else { return }
            let entity = StreamEntity()
            entity.Streamable = increment
            entity.streamable_type = .FileReference
            entity.StreamableId = increment.id
            self.pause()
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: true, in: self)
        case .fileYouTube, .fileMovie:
            guard let increment = nextStream as? FileEntity else { return }
            let entity = StreamEntity()
            entity.Streamable = increment
            entity.streamable_type = .File
            entity.StreamableId = increment.id
            self.pause()
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: true, in: self)
        default: break
        }
    }
    
    func backCut() {
        switch repository.mode {
        case .fileReferenceYouTube, .fileReferenceMovie:
            guard let increment = backStream as? FileReferenceEntity else { return }
            let entity = StreamEntity()
            entity.Streamable = increment
            entity.streamable_type = .FileReference
            entity.StreamableId = increment.id
            self.pause()
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: true, in: self)
        case .fileYouTube, .fileMovie:
            guard let increment = backStream as? FileEntity else { return }
            let entity = StreamEntity()
            entity.Streamable = increment
            entity.streamable_type = .File
            entity.StreamableId = increment.id
            self.pause()
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: true, in: self)
        default: break
        }
    }
    
    func getCurrentTime() -> Float {
        return self.current_manager.current_duration
    }
    
    func updateSlider(current_time: Float) {
        self.playerCoverView.updateSlider(current_time: current_time)
        self.fullScreenPlayerCoverView.updateSlider(current_time: current_time)
    }

    fileprivate func configureStream() {
        self.initializeState()
        switch repository.mode {
        case .fileYouTube:
            self.playerCoverView.thumbnailImage = UIImage(url: repository.File!.thumbnail)
            self.fullScreenPlayerCoverView.thumbnailImage = UIImage(url: repository.File!.thumbnail)
            self.wkytPlayerViewSyncManager.setRepository(repository.File!)
            self.wkytPlayerViewSyncManager.configurePlayerView(containerView: videoContainerView)
            self.wkytPlayerViewSyncManager.connect(startDuration: repository.duration)
            self.delegate?.streamChannelHeaderView(didChangeStatus: .unknown, in: self)
        case .fileMovie:
            self.avPlayerViewSyncManager.setRepository(repository.File!)
            self.avPlayerViewSyncManager.configurePlayerView(containerView: videoContainerView)
            self.avPlayerViewSyncManager.connect(startDuration: repository.duration)
            self.delegate?.streamChannelHeaderView(didChangeStatus: .unknown, in: self)
        case .fileReferenceYouTube:
            self.playerCoverView.thumbnailImage = UIImage(url: repository.FileReference!.File!.thumbnail)
            self.fullScreenPlayerCoverView.thumbnailImage = UIImage(url: repository.FileReference!.File!.thumbnail)
            self.wkytPlayerViewSyncManager.setRepository(repository.FileReference!.File!)
            self.wkytPlayerViewSyncManager.configurePlayerView(containerView: videoContainerView)
            self.wkytPlayerViewSyncManager.connect(startDuration: repository.duration)
            self.delegate?.streamChannelHeaderView(didChangeStatus: .unknown, in: self)
        case .fileReferenceMovie:
            self.avPlayerViewSyncManager.setRepository(repository.FileReference!.File!)
            self.avPlayerViewSyncManager.configurePlayerView(containerView: videoContainerView)
            self.avPlayerViewSyncManager.connect(startDuration: repository.duration)
            self.delegate?.streamChannelHeaderView(didChangeStatus: .unknown, in: self)
        default: break
        }
    }
    
    func loop() {
        switch self.repository.streamable_type {
        case .File:
            guard let file = self.repository.File else { return }
            let entity = StreamEntity()
            entity.Streamable = file
            entity.streamable_type = repository.streamable_type
            entity.StreamableId = file.id
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: false, in: self)
        case .FileReference:
            guard let reference = self.repository.FileReference else { return }
            let entity = StreamEntity()
            entity.Streamable = reference
            entity.streamable_type = repository.streamable_type
            entity.StreamableId = reference.id
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: false, in: self)
        }
    }
    
    func close() {
        self.delegate?.streamChannelHeaderView(shouldClose: repository, in: self)
    }
}

extension StreamChannelHeaderView: WKYTPlayerViewSyncManagerDelegate {
    func playerViewSyncManager(didChangeStatus status: SyncPlayManagerStatus, in playerViewSyncManager: WKYTPlayerViewSyncManager) {
        guard repository.mode == .fileYouTube || repository.mode == .fileReferenceYouTube else { return }
        switch status {
        case .end:
            if loopable { self.loop(); return }
            guard let increment = self.nextStream, let type = increment.streamable_type, self.delegate?.streamChannelHeaderView(enableEndIncrement: increment, in: self) ?? false else { break }
            let entity = StreamEntity()
            entity.Streamable = increment
            entity.streamable_type = type
            entity.StreamableId = increment.streamable_id
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: true, in: self)
        default: break
        }
        self.delegate?.streamChannelHeaderView(didChangeStatus: status, in: self)
    }
    
    func playerViewSyncManager(becomeReady playerView: WKYTPlayerView, in playerViewSyncManager: WKYTPlayerViewSyncManager) {
        guard repository.mode == .fileYouTube || repository.mode == .fileReferenceYouTube else { return }
        self.delegate?.streamChannelHeaderView(becomeReady: self)
    }
    
    func playerViewSyncManager(seekReady playerView: WKYTPlayerView, in playerViewSyncManager: WKYTPlayerViewSyncManager) {
        self.delegate?.streamChannelHeaderView(seekReady: self)
    }
    
    func playerViewSyncManager(didPlayTime time: Float, in playerViewSyncManager: WKYTPlayerViewSyncManager) {
        guard repository.mode == .fileYouTube || repository.mode == .fileReferenceYouTube else { return }
        self.delegate?.streamChannelHeaderView(didPlayTime: time, in: self)
    }
    
    func playerViewSyncManager(didSeek time: Float, in playerViewSyncManager: WKYTPlayerViewSyncManager) {
        guard self.repository.mode == .fileYouTube || self.repository.mode == .fileReferenceYouTube else { return }
    }
    
    func playerViewSyncManager(didBuffer playerViewSyncManager: WKYTPlayerViewSyncManager) {
        guard !AppConfig.is_background else { return }
        self.delegate?.streamChannelHeaderView(didInterrupt: repository, at: getCurrentTime(), in: self)
    }
    
    func playerViewSyncManager(didInterrupt playerViewSyncManager: WKYTPlayerViewSyncManager) {
        guard !AppConfig.is_background else { return }
        self.delegate?.streamChannelHeaderView(didInterrupt: repository, at: getCurrentTime(), in: self)
    }
}

extension StreamChannelHeaderView: AVPlayerViewSyncManagerDelegate {
    func playerViewSyncManager(didChangeStatus status: SyncPlayManagerStatus, in playerViewSyncManager: AVPlayerViewSyncManager) {
        guard repository.mode == .fileMovie || repository.mode == .fileReferenceMovie else { return }
        switch status {
        case .end:
            if loopable { self.loop(); return }
            guard let increment = self.nextStream, let type = increment.streamable_type, self.delegate?.streamChannelHeaderView(enableEndIncrement: increment, in: self) ?? false else { break }
            let entity = StreamEntity()
            entity.Streamable = increment
            entity.streamable_type = type
            entity.StreamableId = increment.streamable_id
            self.initializeState()
            self.delegate?.streamChannelHeaderView(willStart: entity, loggable: true, in: self)
        default: break
        }
        self.delegate?.streamChannelHeaderView(didChangeStatus: status, in: self)
    }
    
    func playerViewSyncManager(becomeReady playerView: PlayerView, in playerViewSyncManager: AVPlayerViewSyncManager) {
        guard self.repository.mode == .fileMovie || self.repository.mode == .fileReferenceMovie else { return }
        self.delegate?.streamChannelHeaderView(becomeReady: self)
    }
    
    func playerViewSyncManager(seekReady playerView: PlayerView, in playerViewSyncManager: AVPlayerViewSyncManager) {
        guard self.repository.mode == .fileMovie || self.repository.mode == .fileReferenceMovie else { return }
        self.delegate?.streamChannelHeaderView(seekReady: self)
    }
    
    func playerViewSyncManager(didPlayTime time: Float, in playerViewSyncManager: AVPlayerViewSyncManager) {
        guard self.repository.mode == .fileMovie || self.repository.mode == .fileReferenceMovie else { return }
        self.delegate?.streamChannelHeaderView(didChangeStatus: .playing, in: self)
        self.delegate?.streamChannelHeaderView(didPlayTime: time, in: self)
        self.syncing = false
    }
    
    func playerViewSyncManager(didSeek time: Float, in playerViewSyncManager: AVPlayerViewSyncManager) {
        guard self.repository.mode == .fileMovie || self.repository.mode == .fileReferenceMovie else { return }
    }
    
    func playerViewSyncManager(didSync playerViewSyncManager: AVPlayerViewSyncManager) {
        guard self.repository.mode == .fileMovie || self.repository.mode == .fileReferenceMovie else { return }
    }
    
    func playerViewSyncManager(didBuffer playerViewSyncManager: AVPlayerViewSyncManager) {
    }
    
    func playerViewSyncManager(didInterrupt playerViewSyncManager: AVPlayerViewSyncManager) {
    }
}

extension StreamChannelHeaderView: VoiceHeaderCoverViewDelegate {
}

extension StreamChannelHeaderView: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.delegate?.streamChannelHeaderView(toStreamableSelects: repository, in: self)
    }
}
