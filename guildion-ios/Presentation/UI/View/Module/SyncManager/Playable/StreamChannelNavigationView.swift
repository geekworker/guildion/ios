//
//  StreamChannelNavigationView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/06.
//

import Foundation
import UIKit
import Gradients

protocol StreamChannelNavigationViewDelegate: class {
    func streamChannelNavigationView(shouldClose repository: StreamChannelEntity, in streamChannelNavigationView: StreamChannelNavigationView)
    func streamChannelNavigationView(toMembersMenu repository: StreamChannelEntity, in streamChannelNavigationView: StreamChannelNavigationView)
}

class StreamChannelNavigationView: UIView {
    @IBOutlet weak var gradationView: GradationView! {
        didSet {
            gradationView.topColor = UIColor.black.withAlphaComponent(0.5)
            gradationView.bottomColor = .clear
            gradationView.startX = 0.5
            gradationView.startY = 0
            gradationView.endX = 0.5
            gradationView.endY = 1
            gradationView.setGradation()
        }
    }
    @IBOutlet weak var closeIconButton: IconButton! {
        didSet {
            closeIconButton.buttonImage = R.image.eyeClose()!.withRenderingMode(.alwaysTemplate)
            closeIconButton.imageColor = Theme.themify(key: .danger)
            closeIconButton.delegate = self
            closeIconButton.buttonInset = 5
            closeIconButton.layer.cornerRadius = closeIconButton.bounds.width / 2
            closeIconButton.clipsToBounds = true
            closeIconButton.hoverColor = .white
            closeIconButton.hoverAlpha = 0.3
        }
    }
    @IBOutlet weak var membersIconButton: IconButton! {
        didSet {
            membersIconButton.buttonImage = R.image.members()!.withRenderingMode(.alwaysTemplate)
            membersIconButton.imageColor = .white
            membersIconButton.delegate = self
            membersIconButton.buttonInset = 5
            membersIconButton.layer.cornerRadius = membersIconButton.bounds.width / 2
            membersIconButton.clipsToBounds = true
            membersIconButton.hoverColor = .white
            membersIconButton.hoverAlpha = 0.3
            membersIconButton.imageView.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.image = R.image.watchParty()?.withRenderingMode(.alwaysTemplate)
            iconImageView.tintColor = .white
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.tintColor = Theme.themify(key: .string)
            nameLabel.text = ""
        }
    }
    
    weak var delegate: StreamChannelNavigationViewDelegate?
    var repository: StreamChannelEntity = StreamChannelEntity()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.streamChannelNavigationView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func setRepository(repository: StreamChannelEntity) {
        self.repository = repository
        self.nameLabel.text = repository.name
        self.iconImageView.image = repository.channelable_type.image
    }
}

extension StreamChannelNavigationView: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        switch iconButton {
        case membersIconButton: self.delegate?.streamChannelNavigationView(toMembersMenu: repository, in: self)
        case closeIconButton: self.delegate?.streamChannelNavigationView(shouldClose: repository, in: self)
        default: break
        }
    }
}
