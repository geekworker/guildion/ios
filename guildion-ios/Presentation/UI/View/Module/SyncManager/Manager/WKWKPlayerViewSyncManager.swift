//
//  WKYTPlayerViewSyncManager.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/02.
//

import Foundation
import UIKit
import PKHUD
import AVFoundation
import AVKit
import MediaPlayer
import YoutubePlayer_in_WKWebView
import Hero
import NVActivityIndicatorView
import RxSwift

protocol WKYTPlayerViewSyncManagerDelegate: class {
    func playerViewSyncManager(didChangeStatus status: SyncPlayManagerStatus, in playerViewSyncManager: WKYTPlayerViewSyncManager)
    func playerViewSyncManager(becomeReady playerView: WKYTPlayerView, in playerViewSyncManager: WKYTPlayerViewSyncManager)
    func playerViewSyncManager(didBuffer playerViewSyncManager: WKYTPlayerViewSyncManager)
    func playerViewSyncManager(didInterrupt playerViewSyncManager: WKYTPlayerViewSyncManager)
    func playerViewSyncManager(didPlayTime time: Float, in playerViewSyncManager: WKYTPlayerViewSyncManager)
    func playerViewSyncManager(didSeek time: Float, in playerViewSyncManager: WKYTPlayerViewSyncManager)
    func playerViewSyncManager(seekReady playerView: WKYTPlayerView, in playerViewSyncManager: WKYTPlayerViewSyncManager)
}

class WKYTPlayerViewSyncManager: NSObject, SyncManagerImpl {
    public var repository: FileEntity = FileEntity()
    public weak var delegate: WKYTPlayerViewSyncManagerDelegate?
    public lazy var playerView: WKYTPlayerView = {
        let playerView = WKYTPlayerView()
        playerView.delegate = self
        return playerView
    }()
    fileprivate var configured: Bool = false
    fileprivate var shouldFirstStop: Bool = false
    private (set) public var becameReady: Bool = false
    private var bufferingDate: Date = Date()
    private (set) public var buffering: Bool = false
    private (set) public var seeking: Bool = false
    fileprivate var willPlay: Bool = false
    fileprivate var willStop: Bool = false
    public var is_single: Bool = true
    internal (set) public var current_status: SyncPlayManagerStatus = .unknown
    private (set) public var current_duration: Float = 0 {
        didSet {
            currentDurationSubject.onNext(current_duration)
        }
    }
    private (set) public var player_duration: Float = 0 {
        didSet {
            playerDurationSubject.onNext(player_duration)
        }
    }
    private (set) public var playing: Bool = false {
        didSet {
            playingSubject.onNext(playing)
        }
    }
    private (set) public var paused: Bool = false {
        didSet {
            pausedSubject.onNext(paused)
        }
    }
    private (set) public var loading: Bool = false {
        didSet {
            loadingSubject.onNext(loading)
        }
    }
    
    private var loadingSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    public var loadingEvent: Observable<Bool> { return loadingSubject }
    private var currentDurationSubject: PublishSubject<Float>  = PublishSubject<Float>()
    public var currentDurationEvent: Observable<Float> { return currentDurationSubject }
    private var playerDurationSubject: PublishSubject<Float>  = PublishSubject<Float>()
    public var playerDurationEvent: Observable<Float> { return playerDurationSubject }
    private var playingSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    public var playingEvent: Observable<Bool> { return playingSubject }
    private var pausedSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    public var pausedEvent: Observable<Bool> { return pausedSubject }
    
    func setRepository(_ repository: FileEntity) {
        guard repository.format == .youtube else { return }
        self.repository = repository
        self.player_duration = Float(repository.duration)
    }
    
    internal func initializeState() {
        self.configured = false
        self.shouldFirstStop = false
        self.becameReady = false
        self.buffering = false
        self.seeking = false
        self.willPlay = false
        self.willStop = false
        self.is_single = true
        self.current_status = .unknown
        self.current_duration = 0
        self.player_duration = 0
        self.playing = false
        self.paused = false
        self.playerView.removeWebView()
        
        self.loading = true
        
        self.loadingSubject.onCompleted()
        self.currentDurationSubject.onCompleted()
        self.playerDurationSubject.onCompleted()
        self.playingSubject.onCompleted()
        self.pausedSubject.onCompleted()
        self.loadingSubject = PublishSubject<Bool>()
        self.currentDurationSubject = PublishSubject<Float>()
        self.playerDurationSubject = PublishSubject<Float>()
        self.playingSubject = PublishSubject<Bool>()
        self.pausedSubject = PublishSubject<Bool>()
    }
    
    func configurePlayerView(containerView: UIView) {
        guard !configured else { return }
        self.playerView.frame = containerView.bounds
        containerView.insertSubview(playerView, at: 0)
        containerView.constrainToEdges(playerView)
        self.configured = true
    }
    
    func connect(startDuration: Double) {
        guard let youtubeID = repository.Provider?.youtubeID else { return }
        shouldFirstStop = true
        self.loading = true
        self.playerView.isHidden = false
        self.playerView.load(
            withVideoId: youtubeID,
            playerVars: [
                "controls" : 0,
                "playsinline" : 1,
                "start": Int(startDuration < repository.duration - LiveConfig.max_start_offset && startDuration > LiveConfig.min_start_offset ? startDuration : 0),
                "autohide" : 1,
                "showinfo" : 0,
                "modestbranding" : 1,
                "rel" : 0,
                "origin": "http://www.youtube.com",
            ],
            templatePath: Bundle.main.path(forResource: R.file.ytPlayerViewIframePlayerHtml.name, ofType: R.file.ytPlayerViewIframePlayerHtml.pathExtension)!
        )
        self.playerView.webView?.configuration.allowsPictureInPictureMediaPlayback = false
        self.didChangeTo(state: .unknown)
    }
    
    func didBecomeReady() {
        DispatchQueue.main.async {
            self.becameReady = true
            self.delegate?.playerViewSyncManager(becomeReady: self.playerView, in: self)
            self.playerView.getDuration({ time, error in
                if error != nil {
                    self.player_duration = Float(time)
                }
            })
        }
    }
    
    func play() {
        DispatchQueue.main.async {
            guard self.becameReady else { return }
            self.shouldFirstStop = false
            self.seeking = false
            self.willPlay = true
            self.playerView.playVideo()
        }
    }
    
    func pause() {
        DispatchQueue.main.async {
            guard self.becameReady else { return }
            self.shouldFirstStop = false
            self.seeking = false
            self.willStop = true
            self.playerView.pauseVideo()
        }
    }
    
    func seek(at: Float) {
        DispatchQueue.main.async {
            guard self.becameReady else { return }
            self.shouldFirstStop = false
            self.willStop = true
            self.playerView.playVideo()
            self.seeking = true
            self.playerView.seek(toSeconds: at, allowSeekAhead: true)
            self.current_duration = at
            self.playerView.pauseVideo()
            self.didChangeTo(state: .unknown)
            if at >= Float(self.repository.duration) - 0.5 { self.didChangeTo(state: .end) }
        }
    }
    
    func handleAfterSyncing() {
        DispatchQueue.main.async {
            self.shouldFirstStop = false
            self.seeking = false
        }
    }
    
    func handleAfterSeeking() {
        DispatchQueue.main.async {
            self.shouldFirstStop = false
            self.seeking = false
        }
    }
    
    func syncStatusAfterSeek() {}
    
    func seekAndPause(at: Float) {
        DispatchQueue.main.async {
            self.shouldFirstStop = false
            self.seeking = false
            self.willStop = true
            self.playerView.seek(toSeconds: at, allowSeekAhead: true)
            self.pause()
        }
    }
    
    func seekAndPlay(at: Float) {
        DispatchQueue.main.async {
            self.shouldFirstStop = false
            self.seeking = false
            self.willPlay = true
            self.playerView.seek(toSeconds: at, allowSeekAhead: true)
            self.play()
        }
    }
    
    func didPlayTime(playTime: Float) {
        self.current_duration = playTime
        self.delegate?.playerViewSyncManager(didPlayTime: playTime, in: self)
    }
    
    func didChangeTo(state: SyncPlayManagerStatus) {
        DispatchQueue.main.async {
            if self.handleState(stateFor: state) {
                self.current_status = state
                self.delegate?.playerViewSyncManager(didChangeStatus: state, in: self)
            }
        }
    }
    
    func handleState(stateFor state: SyncPlayManagerStatus) -> Bool {
        self.playing = false
        self.paused = false
        switch state {
        case .playing:
            if self.buffering && !self.shouldFirstStop && self.becameReady && !self.seeking && Date().timeIntervalSince(self.bufferingDate) > SyncPlayerConfig.min_buffering_dispatch_interval_sc {
                self.delegate?.playerViewSyncManager(didBuffer: self)
            }
            self.buffering = false
            if shouldFirstStop {
                playerView.pauseVideo()
                return false
            } else if self.seeking {
                self.didSeek()
                return false
            } else if !self.willStop && !self.willPlay && self.current_status != .playing {
            }
            self.willPlay = false
            self.playing = true
            self.loading = false
        case .paused:
            if self.buffering && !self.shouldFirstStop && self.becameReady && !self.seeking && Date().timeIntervalSince(self.bufferingDate) > SyncPlayerConfig.min_buffering_dispatch_interval_sc {
                self.delegate?.playerViewSyncManager(didBuffer: self)
            }
            if self.shouldFirstStop {
                self.buffering = false
                self.didChangeTo(state: .readyToPlay)
                self.didBecomeReady()
                return false
            } else if self.seeking {
                self.buffering = false
                self.delegate?.playerViewSyncManager(seekReady: self.playerView, in: self)
                self.didSeek()
                return false
            } else if !self.willStop && !self.willPlay && self.current_status != .paused {
                self.delegate?.playerViewSyncManager(didInterrupt: self)
            }
            self.buffering = false
            self.willStop = false
            self.paused = true
            self.loading = false
        case .buffering:
            self.loading = true
            self.bufferingDate = Date()
        case .readyToPlay, .readyToPause:
            self.loading = true
        default: break
        }
        return true
    }
    
    func didFailed() {
        self.delegate?.playerViewSyncManager(didChangeStatus: .failed, in: self)
    }
    
    func didSeek() {
        if self.seeking {
            self.delegate?.playerViewSyncManager(didChangeStatus: .readyToPlay, in: self)
        }
    }
    
    func disconnect() {
        self.initializeState()
        self.playerView.stopVideo()
        self.playerView.removeWebView()
        self.playerView.removeFromSuperview()
    }
}

extension WKYTPlayerViewSyncManager: WKYTPlayerViewDelegate {
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        if shouldFirstStop {
            self.playerView.playVideo()
        }
    }

    func playerViewIframeAPIDidFailed(toLoad playerView: WKYTPlayerView) {
        self.didFailed()
    }

    func playerView(_ playerView: WKYTPlayerView, didPlayTime playTime: Float) {
        self.loading = false
        self.current_duration = playTime
        self.didPlayTime(playTime: playTime)
    }

    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        DispatchQueue.main.async {
            switch state {
            case .playing: self.didChangeTo(state: .playing)
            case .paused: self.didChangeTo(state: .paused)
            case .ended:
                self.pause()
                self.playerView.stopVideo()
                self.didChangeTo(state: .end)
            case .buffering:
                self.buffering = true
                self.didChangeTo(state: .buffering)
            default: self.didChangeTo(state: .unknown)
            }
        }
    }
}
