//
//  AVPlayerViewSyncManager.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/02.
//

import Foundation
import UIKit
import PKHUD
import AVFoundation
import AVKit
import MediaPlayer
import YoutubePlayer_in_WKWebView
import Hero
import NVActivityIndicatorView
import RxSwift

protocol AVPlayerViewSyncManagerDelegate: class {
    func playerViewSyncManager(didChangeStatus status: SyncPlayManagerStatus, in playerViewSyncManager: AVPlayerViewSyncManager)
    func playerViewSyncManager(becomeReady playerView: PlayerView, in playerViewSyncManager: AVPlayerViewSyncManager)
    func playerViewSyncManager(didBuffer playerViewSyncManager: AVPlayerViewSyncManager)
    func playerViewSyncManager(didInterrupt playerViewSyncManager: AVPlayerViewSyncManager)
    func playerViewSyncManager(didPlayTime time: Float, in playerViewSyncManager: AVPlayerViewSyncManager)
    func playerViewSyncManager(didSeek time: Float, in playerViewSyncManager: AVPlayerViewSyncManager)
    func playerViewSyncManager(seekReady playerView: PlayerView, in playerViewSyncManager: AVPlayerViewSyncManager)
}

class AVPlayerViewSyncManager: NSObject, SyncManagerImpl {
    public weak var delegate: AVPlayerViewSyncManagerDelegate?
    public lazy var playerView: PlayerView = {
        let playerView = PlayerView()
        playerView.delegate = self
        return playerView
    }()
    public var repository: FileEntity = FileEntity()
    fileprivate var configured: Bool = false
    fileprivate var playerReady: Bool = false
    private (set) public var becameReady: Bool = false
    private (set) public var buffering: Bool = false
    private (set) public var seeking: Bool = false
    fileprivate var willPlay: Bool = false
    fileprivate var willStop: Bool = false
    public var is_single: Bool = true
    internal (set) public var current_status: SyncPlayManagerStatus = .unknown
    private (set) public var current_duration: Float = 0 {
        didSet {
            currentDurationSubject.onNext(current_duration)
        }
    }
    public var player_duration: Float { Float(self.playerView.duration) }
    public var playing: Bool { playerView.playing }
    public var paused: Bool { playerView.paused }
    private (set) public var loading: Bool = false {
        didSet {
            loadingSubject.onNext(loading)
        }
    }
    
    private var loadingSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    public var loadingEvent: Observable<Bool> { return loadingSubject }
    private var currentDurationSubject: PublishSubject<Float>  = PublishSubject<Float>()
    public var currentDurationEvent: Observable<Float> { return currentDurationSubject }
    private var playerDurationSubject: PublishSubject<Float>  = PublishSubject<Float>()
    public var playerDurationEvent: Observable<Float> { return playerDurationSubject }
    private var playingSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    public var playingEvent: Observable<Bool> { return playingSubject }
    private var pausedSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    public var pausedEvent: Observable<Bool> { return pausedSubject }
    
    private var playingObservation: NSKeyValueObservation?
    private var pausedObservation: NSKeyValueObservation?
    private var durationObservation: NSKeyValueObservation?
    
    private let subject = PublishSubject<String>()
    public var observable: Observable<String> {
        return subject.asObservable()
    }
    
    func setRepository(_ repository: FileEntity) {
        guard repository.format.content_type.is_movie else { return }
        self.repository = repository
    }
    
    internal func initializeState() {
        self.configured = false
        self.playerReady = false
        self.becameReady = false
        self.buffering = false
        self.seeking = false
        self.willPlay = false
        self.willStop = false
        self.is_single = true
        self.current_status = .unknown
        self.current_duration = 0
        self.loading = true
        
        self.loadingSubject.onCompleted()
        self.currentDurationSubject.onCompleted()
        self.playerDurationSubject.onCompleted()
        self.playingSubject.onCompleted()
        self.pausedSubject.onCompleted()
        self.loadingSubject = PublishSubject<Bool>()
        self.currentDurationSubject = PublishSubject<Float>()
        self.playerDurationSubject = PublishSubject<Float>()
        self.playingSubject = PublishSubject<Bool>()
        self.pausedSubject = PublishSubject<Bool>()
    }
    
    func configurePlayerView(containerView: UIView) {
        guard !configured else { return }
        self.playerView.frame = containerView.bounds
        containerView.insertSubview(playerView, at: 0)
        containerView.constrainToEdges(playerView)
        NotificationCenter.default.addObserver(self, selector: #selector(didFinishPlaying(_:)), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        self.configured = true
    }
    
    func connect(startDuration: Double) {
        guard let url = URL(string: repository.url) else { return }
        self.loading = true
        self.playerView.isHidden = false
        let item = AVPlayerItem(url: url)
        playerView.replaceCurrentItem(with: item, duration: repository.duration)
        startObserve()
        self.didChangeTo(state: .unknown)
    }
    
    fileprivate func startObserve() {
        playingObservation = playerView.observe(\.playing, options: [.new, .old]) { [weak self] player, change in
            guard let self = self else { return }
            if let newValue = change.newValue {
                self.playingSubject.onNext(newValue)
            }
        }
        pausedObservation = playerView.observe(\.paused, options: [.new, .old]) { [weak self] player, change in
            guard let self = self else { return }
            if let newValue = change.newValue {
                self.pausedSubject.onNext(newValue)
            }
        }
        durationObservation = playerView.observe(\.duration, options: [.new]) { [weak self] player, change in
            guard let self = self else { return }
            if let newValue = change.newValue {
                self.playerDurationSubject.onNext(Float(newValue))
            }
        }
    }
    
    func didBecomeReady() {
        guard !becameReady && playerReady else { return }
        self.becameReady = true
        self.delegate?.playerViewSyncManager(didChangeStatus: .readyToPlay, in: self)
        self.delegate?.playerViewSyncManager(becomeReady: self.playerView, in: self)
    }
    
    func play() {
        guard self.becameReady else { return }
        self.seeking = false
        self.willPlay = true
        self.playerView.player.play()
    }
    
    func pause() {
        guard self.becameReady else { return }
        self.loading = false
        self.seeking = false
        self.willStop = true
        self.playerView.player.pause()
    }
    
    func seek(at: Float) {
        guard self.becameReady else { return }
        self.willStop = true
        self.seeking = true
        self.loading = true
        let cmTime = CMTime(seconds: TimeInterval(at), preferredTimescale: 1000000)
        self.playerView.player.seek(to: cmTime)
        self.current_duration = at
        self.playerView.player.pause()
    }
    
    func syncStatusAfterSeek() {
        if self.playerView.player.status == .readyToPlay && self.playerView.player.currentItem?.status == .readyToPlay {
            self.seeking = false
            self.delegate?.playerViewSyncManager(didChangeStatus: .readyToPlay, in: self)
            self.delegate?.playerViewSyncManager(seekReady: self.playerView, in: self)
            self.delegate?.playerViewSyncManager(didSeek: playerView.playTime, in: self)
//            // MARK: if seek in buffered, can't play soon because of some reason.
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                self.didChangeTo(state: .readyToPlay)
//            }
        }
    }
    
    func handleAfterSyncing() {
        self.seeking = false
        self.becameReady = true
        self.playerReady = true
    }
    
    func handleAfterSeeking() {
        self.seeking = false
    }
    
    func seekAndPlay(at: Float) {
        guard self.becameReady else { return }
        let cmTime = CMTime(seconds: TimeInterval(at), preferredTimescale: 1000000)
        self.playerView.player.seek(to: cmTime)
        self.loading = true
        self.willPlay = true
        self.play()
    }
    
    func seekAndPause(at: Float) {
        guard self.becameReady else { return }
        let cmTime = CMTime(seconds: TimeInterval(at), preferredTimescale: 1000000)
        self.playerView.player.seek(to: cmTime)
        self.loading = true
        self.willStop = true
        self.pause()
    }
    
    func didPlayTime(playTime: Float) {
        guard !self.seeking else { return }
        self.current_duration = playTime
        self.delegate?.playerViewSyncManager(didPlayTime: playTime, in: self)
    }
    
    @objc func didFinishPlaying(_ notification: Notification) {
        guard (notification.object as? AVPlayerItem) != nil, self.becameReady, self.playerReady else { return }
        self.didChangeTo(state: .end)
        self.willStop = true
        self.pause()
        self.initializeState()
    }
    
    func didChangeTo(state: SyncPlayManagerStatus) {
        DispatchQueue.main.async {
            if self.handleState(stateFor: state) {
                self.current_status = state
                self.delegate?.playerViewSyncManager(didChangeStatus: state, in: self)
            }
        }
    }
    
    func handleState(stateFor state: SyncPlayManagerStatus) -> Bool {
        switch state {
        case .playing:
            self.loading = false
        case .paused:
            if !willStop && !self.seeking && TimeInterval(self.playerView.playTime) < self.repository.duration - 0.5 {
                self.delegate?.playerViewSyncManager(didInterrupt: self)
            }
            self.loading = false
        case .buffering:
            self.loading = true
        case .readyToPlay, .readyToPause:
            self.didBecomeReady()
            self.didSeek()
            self.loading = true
        default: break
        }
        return true
    }
    
    func didFailed() {
        self.delegate?.playerViewSyncManager(didChangeStatus: .failed, in: self)
    }
    
    func didSeek() {
        if self.seeking {
            self.seeking = false
            self.delegate?.playerViewSyncManager(didChangeStatus: .readyToPlay, in: self)
            self.delegate?.playerViewSyncManager(seekReady: self.playerView, in: self)
            self.delegate?.playerViewSyncManager(didSeek: playerView.playTime, in: self)
        }
    }
    
    func disconnect() {
        self.initializeState()
        self.invalidateObserve()
        self.playerView.player.pause()
        self.playerView.removeFromSuperview()
        self.playerView.resetPlayer()
    }
    
    fileprivate func invalidateObserve() {
        playingObservation?.invalidate()
        playingObservation = nil
        pausedObservation?.invalidate()
        pausedObservation = nil
        durationObservation?.invalidate()
        durationObservation = nil
    }
}

extension AVPlayerViewSyncManager: PlayerViewDelegate {
    func playerView(didPlayerBufferingStateChange buffering: Bool, in playerView: PlayerView) {
        DispatchQueue.main.async { [unowned self] in
            if buffering {
                self.loading = true
                self.didChangeTo(state: .buffering)
            } else if !becameReady {
                self.didChangeTo(state: .readyToPlay)
            } else if seeking {
                self.didChangeTo(state: .readyToPlay)
            } else {
                self.loading = false
            }
            self.buffering = buffering
        }
    }
    
    func playerView(didPlayerStateChange state: AVPlayerLoader.Result, in playerView: PlayerView) {
        switch state {
        case .success(_):
            self.playerReady = true
            self.didChangeTo(state: .unknown)
        case .failed, .timedOut: self.didFailed()
        case .paused: self.didChangeTo(state: .paused)
        case .playing: self.didChangeTo(state: .playing)
        default: self.didChangeTo(state: .unknown)
        }
    }

    func playerView(didPlayerTimeChange playTime: Float, in playerView: PlayerView) {
        guard !seeking else { return }
        self.didPlayTime(playTime: playTime)
    }
}
