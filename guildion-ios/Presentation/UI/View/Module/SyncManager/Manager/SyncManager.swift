//
//  SyncManager.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/02.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import YoutubePlayer_in_WKWebView
import RxSwift

protocol SyncManagerImpl: class {
    var repository: FileEntity { get set }
    var current_status: SyncPlayManagerStatus { get set }
    var is_single: Bool { get set }
    
    var currentDurationEvent: Observable<Float> { get }
    var playerDurationEvent: Observable<Float> { get }
    var loadingEvent: Observable<Bool> { get }
    var playingEvent: Observable<Bool> { get }
    var pausedEvent: Observable<Bool> { get }
    
    var current_duration: Float { get }
    var player_duration: Float { get }
    var loading: Bool { get }
    var playing: Bool { get }
    var paused: Bool { get }
    var becameReady: Bool { get }
    var buffering: Bool { get }
    var seeking: Bool { get }
    
    func setRepository(_ repository: FileEntity)
    func configurePlayerView(containerView: UIView)
    func initializeState()
    func connect(startDuration: Double)
    func didBecomeReady()
    func play()
    func pause()
    func seek(at: Float)
    func seekAndPlay(at: Float)
    func seekAndPause(at: Float)
    func didPlayTime(playTime: Float)
    func didChangeTo(state: SyncPlayManagerStatus)
    func didFailed()
    func disconnect()
    
    func handleAfterSeeking()
    func handleAfterSyncing()
    func syncStatusAfterSeek()
}
