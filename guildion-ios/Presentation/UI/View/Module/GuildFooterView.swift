//
//  GuildFooterView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/11.
//

import Foundation
import UIKit
import Nuke

protocol GuildFooterViewDelegate: class {
    func guildFooterView(onJoin repository: GuildEntity, in guildFooterView: GuildFooterView)
}

class GuildFooterView: UIView {
    @IBOutlet weak var effectView: UIVisualEffectView!
    @IBOutlet weak var guildImageView: UIImageView! {
        didSet {
            guildImageView.contentMode = .scaleAspectFill
            guildImageView.layer.borderColor = Theme.themify(key: .border).cgColor
            guildImageView.layer.borderWidth = 0.5
            guildImageView.layer.cornerRadius = 5
            guildImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var joinButton: SimpleButton! {
        didSet {
            joinButton.text = R.string.localizable.guildJoin()
            joinButton.normalColor = Theme.themify(key: .main)
            joinButton.canceledColor = Theme.themify(key: .cancel)
            joinButton.canceled = false
            joinButton.delegate = self
        }
    }
    var repository: GuildEntity = GuildEntity()
    weak var delegate: GuildFooterViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.guildFooterView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    func setRepository(_ repository: GuildEntity) {
        self.repository = repository
        self.nameLabel.text = repository.name
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: guildImageView)
        }
        self.joinButton.canceled = repository.MemberRequests?.items.filter({ $0.VoterId == CurrentUser.getCurrentUserEntity()?.id }).count ?? 0 > 0
        self.joinButton.text = self.joinButton.canceled ? R.string.localizable.guildRequesting() : R.string.localizable.guildJoin()
        if repository.Members?.items.filter({ $0.UserId == CurrentUser.getCurrentUserEntity()?.id }).count ?? 0 > 0 {
            self.joinButton.canceled = true
            self.joinButton.text = R.string.localizable.guildJoined()
        }
        
        if UserDispatcher.checkBlocked(guild: repository, state: store.state.user) {
            self.joinButton.canceled = true
            self.joinButton.text = R.string.localizable.baned()
        }
    }
}

extension GuildFooterView: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.delegate?.guildFooterView(onJoin: repository, in: self)
    }
}
