//
//  SearchBrowserNavigationBar.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/23.
//

import Foundation
import UIKit

protocol SearchBrowserNavigationBarDelegate: class {
    var current_url: URL { get set }
    var domainType: SearchDomainType { get set }
    func searchBrowserNavigationBarShouldClose(_ searchBrowserNavigationBar: SearchBrowserNavigationBar)
    func searchBrowserNavigationBarShouldReload(_ searchBrowserNavigationBar: SearchBrowserNavigationBar)
    func searchBrowserNavigationBar(_ searchBrowserNavigationBar: SearchBrowserNavigationBar, searched keyword: String?)
}

@IBDesignable
class SearchBrowserNavigationBar: UIView {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var searchBarLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: SearchBrowserBar! {
        didSet {
            searchBar.placeholder = R.string.localizable.webkitKeyword()
            searchBar.buttonText = R.string.localizable.cancel()
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var closeButton: IconButton! {
        didSet {
            closeButton.buttonImage = R.image.close()!.withRenderingMode(.alwaysTemplate)
            closeButton.imageColor = Theme.themify(key: .string)
            closeButton.delegate = self
            closeButton.buttonInset = 5
            closeButton.layer.cornerRadius = closeButton.bounds.width / 2
            closeButton.clipsToBounds = true
        }
    }
    @IBOutlet weak var reloadButton: IconButton! {
        didSet {
            reloadButton.buttonImage = R.image.reload()!.withRenderingMode(.alwaysTemplate)
            reloadButton.imageColor = Theme.themify(key: .string)
            reloadButton.delegate = self
            reloadButton.buttonInset = 5
            reloadButton.layer.cornerRadius = reloadButton.bounds.width / 2
            reloadButton.clipsToBounds = true
        }
    }
    weak var delegate: SearchBrowserNavigationBarDelegate?
    var current_url: URL {
        get {
            self.delegate?.current_url ?? URL(string: Constant.APP_URL)!
        }
        set {
            self.searchBar.setLabel(newValue)
        }
    }
    var domainType: SearchDomainType {
        get {
            self.delegate?.domainType ?? SearchDomainType.google
        }
        set {
            self.searchBar.updateSearchTypeLayout()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.searchBrowserNavigationBar.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.backgroundColor = .none
        self.backgroundColor = Theme.themify(key: .backgroundLight)
        view.backgroundColor = Theme.themify(key: .backgroundLight)
    }
    
    func widenSeachBar() {
        self.searchBarLeadingConstraint.constant = 12
        self.searchBarTrailingConstraint.constant = 12
        self.closeButton.fadeOut(type: .Normal, completed: nil)
        self.reloadButton.fadeOut(type: .Normal, completed: nil)
        UIView.animate(withDuration: 0.2, animations: {
            self.layoutIfNeeded()
        })
    }
    
    func closenSeachBar() {
        self.searchBarLeadingConstraint.constant = 46
        self.searchBarTrailingConstraint.constant = 36
        self.closeButton.fadeIn(type: .Normal, completed: nil)
        self.reloadButton.fadeIn(type: .Normal, completed: nil)
        UIView.animate(withDuration: 0.2, animations: {
            self.layoutIfNeeded()
        })
    }
}

extension SearchBrowserNavigationBar: SearchBrowserBarDelegate {
    func searchBrowserBar(_ searchBrowserBar: SearchBrowserBar, searched keyword: String?) {
        self.delegate?.searchBrowserNavigationBar(self, searched: keyword)
    }
    
    func searchBrowserBar(_ searchBrowserBar: SearchBrowserBar, accessoryButtonTapped keyword: String?) -> Bool {
        return true
    }
    
    func searchBrowserBarDidChange(_ searchBrowserBar: SearchBrowserBar, keyword: String?) {
    }
    
    func searchBrowserBarDidBeginEditing() {
        widenSeachBar()
    }
    
    func searchBrowserBarDidEndEditing() {
        closenSeachBar()
    }
}

extension SearchBrowserNavigationBar: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        switch iconButton {
        case closeButton: self.delegate?.searchBrowserNavigationBarShouldClose(self)
        case reloadButton: self.delegate?.searchBrowserNavigationBarShouldReload(self)
        default: break
        }
    }
}
