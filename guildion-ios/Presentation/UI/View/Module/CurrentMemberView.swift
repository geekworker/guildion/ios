//
//  CurrentMemberView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/04.
//

import Foundation
import UIKit
import Nuke

protocol CurrentMemberViewDelegate: class {
    func currentMemberView(onTap repository: MemberEntity, in currentMemberView: CurrentMemberView)
}

class CurrentMemberView: UIView {
    @IBOutlet weak var effectView: UIVisualEffectView!
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(self.tap(_:))
                )
            )
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            overView.addGestureRecognizer(longpress)
            overView.alpha = 0.1
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.layer.cornerRadius = imageView.frame.width / 2
            imageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = .white
            nameLabel.font = Font.FT(size: .FM, family: .B)
            nameLabel.text = ""
        }
    }
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.image = R.image.more()?.withRenderingMode(.alwaysTemplate)
            iconImageView.tintColor = UIColor.white.darker(by: 5)
        }
    }
    
    var repository: MemberEntity = MemberEntity()
    weak var delegate: CurrentMemberViewDelegate?
    
    static let viewSize: CGSize = CGSize(width: UIScreen.main.bounds.width, height: 44)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.currentMemberView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.backgroundColor = .none
        self.backgroundColor = .none
    }
    
    func setRepository(_ repository: MemberEntity) {
        self.repository = repository
        self.nameLabel.text = repository.nickname
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: imageView)
        }
    }
    
    @objc fileprivate func tap(_ sender: UITapGestureRecognizer) {
        self.delegate?.currentMemberView(onTap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .black
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .none
            })
        default:
            self.overView.backgroundColor = .none
        }
    }
}

extension CurrentMemberView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
