//
//  ImagePagingView.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import Parchment

class ImagePagingView: PagingView {
    var menuHeightConstraint: NSLayoutConstraint?
    
    override func configure() {
        super.configure()
        self.backgroundColor = Theme.themify(key: .backgroundThick)
        self.pageView.backgroundColor = Theme.themify(key: .backgroundThick)
        self.collectionView.backgroundColor = Theme.themify(key: .backgroundThick)
    }
  
    override func setupConstraints() {
        pageView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        menuHeightConstraint = collectionView.heightAnchor.constraint(equalToConstant: options.menuHeight)
        menuHeightConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            
            pageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            pageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            pageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            pageView.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
}
