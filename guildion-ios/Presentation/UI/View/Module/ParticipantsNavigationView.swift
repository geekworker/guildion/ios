//
//  ParticipantsNavigationView.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/20.
//

import Foundation
import UIKit

protocol ParticipantsNavigationViewDelegate: class {
    func participantsNavigationView(onTap repositories: ParticipantEntities, in participantsNavigationView: ParticipantsNavigationView)
    func participantsNavigationView(onTapMore repositories: ParticipantEntities, in participantsNavigationView: ParticipantsNavigationView)
    func participantsNavigationView(onTapProfile repository: ParticipantEntity, in participantsNavigationView: ParticipantsNavigationView)
}

class ParticipantsNavigationView: UIView {
    @IBOutlet weak var buttonContainerView: UIView! {
        didSet {
            buttonContainerView.layer.cornerRadius = buttonContainerView.frame.height / 2
            buttonContainerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var moreButton: IconButton! {
        didSet {
            moreButton.buttonImage = R.image.more()!.withRenderingMode(.alwaysTemplate)
            moreButton.imageColor = .white
            moreButton.delegate = self
            moreButton.buttonInset = 5
            moreButton.layer.cornerRadius = moreButton.bounds.width / 2
            moreButton.clipsToBounds = true
            moreButton.hoverColor = .white
            moreButton.hoverAlpha = 0.3
        }
    }
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            self.configureCollectionView()
        }
    }
    @IBOutlet weak var activeView: UIView! {
        didSet {
            activeView.backgroundColor = Theme.themify(key: .main)
            activeView.layer.cornerRadius = activeView.frame.width / 2
            activeView.clipsToBounds = true
        }
    }
    @IBOutlet weak var countLabel: UILabel!
    weak var delegate: ParticipantsNavigationViewDelegate?
    public var repositories: ParticipantEntities = ParticipantEntities()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.participantsNavigationView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.delegate?.participantsNavigationView(onTap: repositories, in: self)
    }
    
    func setRepositories(_ repositories: ParticipantEntities) {
        self.repositories = repositories
        self.countLabel.text = "\(repositories.count)"
        self.collectionView.reloadData()
    }
}

extension ParticipantsNavigationView: UICollectionViewDelegate, UICollectionViewDataSource {
    fileprivate func configureCollectionView() {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.minimumLineSpacing = 4
        flowLayout.minimumInteritemSpacing = 4
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = ParticipantNavigationCollectionViewCell.cellSize
        collectionView.register([
            UINib(resource: R.nib.participantNavigationCollectionViewCell): R.nib.participantNavigationCollectionViewCell.name,
        ])
        collectionView.backgroundColor = Theme.themify(key: .transparent)
        collectionView.backgroundView?.backgroundColor = Theme.themify(key: .transparent)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.repositories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.participantNavigationCollectionViewCell.name, for: indexPath) as? ParticipantNavigationCollectionViewCell, let repository = self.repositories[safe: indexPath.row] else {
            fatalError("The dequeued cell is not an instance of CollectionCell.")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let repository = repositories[safe: indexPath.row] else { return }
        self.delegate?.participantsNavigationView(onTapProfile: repository, in: self)
    }
}

extension ParticipantsNavigationView: ParticipantNavigationCollectionViewCellDelegate {
    func participantNavigationCollectionViewCell(onTap repository: ParticipantEntity, in participantNavigationCollectionViewCell: ParticipantNavigationCollectionViewCell) {
        self.delegate?.participantsNavigationView(onTapProfile: repository, in: self)
    }
}

extension ParticipantsNavigationView: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        self.delegate?.participantsNavigationView(onTapMore: repositories, in: self)
    }
}
