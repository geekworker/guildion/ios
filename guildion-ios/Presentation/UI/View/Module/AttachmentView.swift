//
//  AttachmentView.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/21.
//

import Foundation
import UIKit
import Nuke
import M13ProgressSuite

protocol AttachmentViewDelegate: class {
    func attachmentView(tap repository: FileEntity, in attachmentView: AttachmentView)
    func attachmentView(onProgressEnd repository: FileEntity, in attachmentView: AttachmentView)
}

class AttachmentView: UIView {
    @IBOutlet weak var lockedImageView: UIImageView! {
        didSet {
            lockedImageView.image = R.image.locked()?.withRenderingMode(.alwaysTemplate)
            lockedImageView.tintColor = Theme.themify(key: .warning)
            lockedImageView.isHidden = true
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.backgroundColor = Theme.themify(key: .backgroundThick).darker(by: 5)!
        }
    }
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.isHidden = true
            iconImageView.tintColor = .white
        }
    }
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(self.tap(_:))
                )
            )
            let longpress = UILongPressGestureRecognizer(
                target: self,
                action: #selector(self.hover(_:))
            )
            longpress.minimumPressDuration = 0.01
            longpress.cancelsTouchesInView = false
            longpress.delegate = self
            overView.addGestureRecognizer(longpress)
            overView.alpha = 0.1
        }
    }
    @IBOutlet weak var progressViewPie: M13ProgressViewPie! {
        didSet {
            progressViewPie.isUserInteractionEnabled = false
            progressViewPie.isHidden = true
            progressViewPie.primaryColor = Theme.themify(key: .main)
            progressViewPie.secondaryColor = Theme.themify(key: .main)
            progressViewPie.setProgress(0, animated: false)
        }
    }
    @IBOutlet weak var progressView: UIView! {
        didSet {
            progressView.isUserInteractionEnabled = false
            progressView.isHidden = true
            progressView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }
    }
    public weak var delegate: AttachmentViewDelegate?
    public var repository: FileEntity = FileEntity()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.attachmentView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    public func setRepository(_ repository: FileEntity) {
        self.repository = repository
        self.repository.delegate = self
        self.progressView.isHidden = repository.progress == nil || repository.progress ?? 0 >= 1
        self.progressViewPie.isHidden = repository.progress == nil || repository.progress ?? 0 >= 1
        self.progressViewPie.setProgress(CGFloat(repository.progress ?? 0), animated: false)
        
        self.lockedImageView.isHidden = !repository.is_private
        
        if repository.format.content_type.is_movie, let url = URL(string: repository.thumbnail) {
            Nuke.loadImage(with: url, into: self.imageView)
            self.iconImageView.image = R.image.videoPlay()?.withRenderingMode(.alwaysTemplate)
            self.iconImageView.isHidden = repository.progress != nil && repository.progress ?? 0 < 1
        }
        if let image = repository.url_image, repository.format.content_type.is_image {
            imageView.image = image
            self.iconImageView.isHidden = true
        } else if repository.format.content_type.is_image, let url = URL(string: repository.url) {
            Nuke.loadImage(with: url, into: self.imageView)
            self.iconImageView.isHidden = true
        }
        if repository.provider == .youtube, let url = URL(string: repository.thumbnail) {
            Nuke.loadImage(with: url, into: self.imageView)
            self.iconImageView.image = R.image.yt_icon_rgb()
            self.iconImageView.isHidden = repository.progress != nil && repository.progress ?? 0 < 1
        }
    }
    
    public func getSize(maxWidth: CGFloat) -> CGSize {
        guard repository.provider != .youtube else { return self.getSizeForYouTube(maxWidth: maxWidth) }
        let heightAspect = repository.pixel_height / repository.pixel_width,
        width = min(maxWidth, CGFloat(repository.pixel_width) * ChannelMessageSizeCalculator.attachmentRawPixelRatio),
        height = min(maxWidth, width * CGFloat(heightAspect))
        return CGSize(width: width, height: height)
    }
    
    public func getSizeForYouTube(maxWidth: CGFloat) -> CGSize {
        let heightAspect = repository.pixel_height / repository.pixel_width,
        width = min(maxWidth, CGFloat(repository.pixel_width * 2) * ChannelMessageSizeCalculator.attachmentRawPixelRatio),
        height = min(maxWidth, width * CGFloat(heightAspect))
        return CGSize(width: width, height: height)
    }
    
    @objc fileprivate func tap(_ sender: UITapGestureRecognizer) {
        self.delegate?.attachmentView(tap: repository, in: self)
    }
    
    @objc fileprivate func hover(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .white
            })
        case .ended:
            UIView.animate(withDuration: 0.2, animations: {
                self.overView.backgroundColor = .none
            })
        default:
            self.overView.backgroundColor = .none
        }
    }
}

extension AttachmentView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension AttachmentView: FileEntityDelegate {
    func fileEntity(didChange progress: Float, in fileEntity: FileEntity) {
        if progress > 0, progress < 1 {
            self.progressView.isHidden = false
            self.progressViewPie.isHidden = false
            self.progressViewPie.setProgress(CGFloat(progress), animated: true)
        } else if progress >= 1 {
            self.progressView.fadeOut(type: .Normal, completed: nil)
            self.progressViewPie.fadeOut(type: .Normal, completed: nil)
            self.delegate?.attachmentView(onProgressEnd: fileEntity, in: self)
            if repository.format.content_type.is_movie || repository.provider == .youtube {
                self.iconImageView.fadeIn(type: .Normal, completed: nil)
                self.iconImageView.image = repository.format.content_type.is_movie ? R.image.videoPlay()?.withRenderingMode(.alwaysTemplate) : R.image.yt_icon_rgb()
            }
        }
    }
}
