//
//  MypageHeaderView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Nuke
import LinearProgressBar
import Gradients

protocol MypageHeaderViewDelegate: class {
    func mypageHeaderView(onTap mypageHeaderView: MypageHeaderView)
    func mypageHeaderView(willToFiles mypageHeaderView: MypageHeaderView)
}

class MypageHeaderView: UIView {
    @IBOutlet weak var gradationView: GradationView! {
        didSet {
//            gradationView.startX = -1
//            gradationView.endX = 1
//            gradationView.startY = 1
//            gradationView.endY = -1
            gradationView.topColor = UIColor.init(hex: "96deda").darker(by: 20)!
            gradationView.bottomColor = UIColor.init(hex: "50c9c3").darker(by: 20)!
        }
    }
    @IBOutlet weak var premiumView: UIView! {
        didSet {
            premiumView.backgroundColor = .none
            premiumView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
            premiumView.layer.cornerRadius = 5
            premiumView.clipsToBounds = true
            premiumView.isHidden = true
        }
    }
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var premiumLabelView: UIView! {
        didSet {
            premiumLabelView.layer.cornerRadius = 2
            premiumLabelView.clipsToBounds = true
        }
    }
    @IBOutlet weak var filesButton: UIButton! {
        didSet {
            filesButton.tintColor = Theme.themify(key: .main)
            filesButton.titleLabel?.textColor = Theme.themify(key: .main)
            filesButton.titleLabel?.font = Font.FT(size: .FM, family: .L)
            filesButton.setTitle(R.string.localizable.viewFiles(), for: .normal)
            filesButton.setTitle(R.string.localizable.viewFiles(), for: .highlighted)
            filesButton.addTarget(self, action: #selector(self.onTapFilesButton(_:)), for: .touchUpInside)
            filesButton.isHidden = true
        }
    }
    @IBOutlet weak var premiumLabel: UILabel!
    @IBOutlet weak var planDetailLabel: UILabel! {
        didSet {
            planDetailLabel.textColor = .white
            planDetailLabel.text = R.string.localizable.increaseStorage()
        }
    }
    @IBOutlet weak var chevronImageView: UIImageView! {
        didSet {
            chevronImageView.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            chevronImageView.tintColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var activeView: ActiveView! {
        didSet {
            activeView.baseView.layer.cornerRadius = activeView.frame.width / 2
            activeView.baseView.layer.borderWidth = 2
            activeView.baseView.layer.borderColor = Theme.themify(key: .border).cgColor
            activeView.baseView.clipsToBounds = true
            activeView.isHidden = true
        }
    }
    @IBOutlet weak var userImageView: UIImageView! {
        didSet {
            userImageView.layer.borderWidth = 2
            userImageView.layer.cornerRadius = userImageView.frame.width / 2
            userImageView.layer.borderColor = Theme.themify(key: .border).cgColor
            userImageView.clipsToBounds = true
            userImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var fileSizeLabel: UILabel! {
        didSet {
            fileSizeLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var maxSizeLabel: UILabel! {
        didSet {
            maxSizeLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var usedStorageLabel: UILabel! {
        didSet {
            usedStorageLabel.textColor = Theme.themify(key: .stringSecondary)
            usedStorageLabel.text = R.string.localizable.usedStorage()
        }
    }
    @IBOutlet weak var progressBar: LinearProgressBar! {
        didSet {
            progressBar.progressValue = 0
            progressBar.barColor = Theme.themify(key: .main)
            progressBar.trackColor = Theme.themify(key: .cancel)
        }
    }
    
    static let viewHeight: CGFloat = 290 // 390
    
    var repository: UserEntity = UserEntity()
    weak var delegate: MypageHeaderViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.mypageHeaderView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        view.backgroundColor = Theme.themify(key: .backgroundThick)
        self.backgroundColor = Theme.themify(key: .backgroundThick)
        
        self.layer.cornerRadius = 20
        self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.clipsToBounds = true
    }
    
    public func setRepository(_ repository: UserEntity) {
        self.repository = repository
        self.nameLabel.text = repository.nickname
        // self.activeView.setRepository(repository: repository)
        
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: userImageView)
        }
        
        self.fileSizeLabel.text = repository.size
        self.maxSizeLabel.text = "/ \(PlanConfig.free_plan_max_size)"
        self.progressBar.progressValue = CGFloat(repository.size_byte / PlanConfig.free_plan_max_size_byte) * 100
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.mypageHeaderView(onTap: self)
    }
    
    @objc func onTapFilesButton(_ sender: Any) {
        self.delegate?.mypageHeaderView(willToFiles: self)
    }
}
