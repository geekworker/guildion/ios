//
//  PlanCarouselView.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit
import FSPagerView

class PlanCarouselView: UIView {
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.setStrokeColor(nil, for: .normal)
            self.pageControl.setStrokeColor(nil, for: .selected)
            self.pageControl.setFillColor(nil, for: .normal)
            self.pageControl.setFillColor(.white, for: .selected)
            self.pageControl.setImage(nil, for: .normal)
            self.pageControl.setImage(nil, for: .selected)
            self.pageControl.setPath(nil, for: .normal)
            self.pageControl.setPath(nil, for: .selected)
            
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.numberOfPages = repositories.count
            self.pageControl.currentPage = 0
            self.pageControl.contentInsets = UIEdgeInsets(top: -20, left: 20, bottom: 0, right: 20)
            
            self.pageControl.subviews.forEach {
                $0.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }
        }
    }
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(resource: R.nib.planCarouselCollectionViewCell), forCellWithReuseIdentifier: R.nib.planCarouselCollectionViewCell.name)
            self.pagerView.isInfinite = true
            self.pagerView.automaticSlidingInterval = 3
            self.pagerView.layer.shadowRadius = 0
            self.pagerView.delegate = self
            self.pagerView.dataSource = self

//            let gesture = UITapGestureRecognizer(target: self, action: #selector(pageClicked))
//            self.pagerView.isUserInteractionEnabled = true
//            self.pagerView.addGestureRecognizer(gesture)
        }
    }
    
    var repositories: [PlanCarouselCollectionViewCell.MenuType] = [.storage, .reaction, .participant]
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.planCarouselView.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.pageControl.numberOfPages = repositories.count
        self.pagerView.reloadData()
    }
}

extension PlanCarouselView: FSPagerViewDataSource, FSPagerViewDelegate {
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard let cell = pagerView.dequeueReusableCell(withReuseIdentifier: R.nib.planCarouselCollectionViewCell.name, at: index) as? PlanCarouselCollectionViewCell, let repository = repositories[safe: index] else { return FSPagerViewCell() }
        cell.setRepository(repository)
        return cell
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else { return }
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return repositories.count
    }
    
    @objc func pageClicked() {
        guard self.pagerView.currentIndex < repositories.count else { return }
    }
}
