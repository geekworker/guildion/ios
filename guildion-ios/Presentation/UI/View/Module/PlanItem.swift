//
//  PlanItem.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit
import BadgeSwift

protocol PlanItemDelegate: class {
    func planItem(onTap planItem: PlanItem)
}

class PlanItem: UIView {
    @IBOutlet weak var monthCountLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.layer.cornerRadius = 5
            baseView.layer.borderWidth = 3
            baseView.layer.borderColor = Theme.themify(key: .transparent).cgColor
            baseView.clipsToBounds = true
            baseView.isUserInteractionEnabled = true
            baseView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap(_:))))
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    
    @IBOutlet weak var footerLabel: UILabel!
    @IBOutlet weak var badgeView: BadgeSwift! {
        didSet {
            badgeView.badgeColor = Theme.themify(key: .main)
            badgeView.isHidden = true
        }
    }
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var topInsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftInsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightInsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomInsetConstraint: NSLayoutConstraint!
    
    private var isSelected: Bool = false
    
    public var repository: SubscriptionPlanModel = SubscriptionPlanModel()
    public var span: PlanConfig.SubscriptionSpan = .month1
    public weak var delegate: PlanItemDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    private func loadNib() {
        guard let view = Bundle(for: type(of: self)).loadNibNamed(R.nib.planItem.name, owner: self, options: nil)?.first as? UIView else {
            return
        }
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.backgroundColor = .none
        self.updateLayout()
    }
    
    @objc func onTap(_ sender: Any) {
        self.delegate?.planItem(onTap: self)
    }
    
    func setRepository(_ repository: SubscriptionPlanModel, at: PlanConfig.SubscriptionSpan) {
        self.repository = repository
        self.span = at
        self.isSelected = repository.span == at
        self.updateLayout()
    }
    
    func updateLayout() {
        self.monthCountLabel.text = "\(self.span.count)"
        self.monthLabel.text = self.span.string
        self.priceLabel.text = self.span.priceString(plan: self.repository.plan)
        self.footerLabel.text = self.span.discountString(plan: self.repository.plan)
        self.bottomLabel.text = self.span.monthPriceString(plan: self.repository.plan)
        self.badgeView.isHidden = !self.isSelected
        self.baseView.layer.borderColor = Theme.themify(key: self.isSelected ? .main : .transparent).cgColor
        self.footerView.backgroundColor = Theme.themify(key: self.isSelected ? .main : .transparent)
        self.monthLabel.textColor = Theme.themify(key: self.isSelected ? .main : .string)
        self.monthCountLabel.textColor = Theme.themify(key: self.isSelected ? .main : .string)
        self.priceLabel.textColor = Theme.themify(key: self.isSelected ? .main : .string)
        self.bottomLabel.textColor = Theme.themify(key: self.isSelected ? .main : .string)
        self.layoutIfNeeded()
    }
}
