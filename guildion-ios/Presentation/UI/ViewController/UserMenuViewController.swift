//
//  UserMenuViewController.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/25.
//

import Foundation
import UIKit
import PanModal
import Nuke
import PKHUD

class UserMenuViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var headerView: UIView! {
        didSet {
            headerView.layer.cornerRadius = 8
            headerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var memberImageView: UIImageView! {
        didSet {
            memberImageView.layer.cornerRadius = memberImageView.frame.width / 2
            memberImageView.clipsToBounds = true
            memberImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
            nameLabel.text = ""
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Theme.themify(key: .string)
            descriptionLabel.text = R.string.localizable.noneIntrouction()
        }
    }
    @IBOutlet weak var activeView: ActiveView! {
        didSet {
            activeView.baseView.layer.borderWidth = 1
            activeView.baseView.layer.borderColor = Theme.themify(key: .border).cgColor
            activeView.layer.cornerRadius = activeView.frame.width / 2
            activeView.clipsToBounds = true
        }
    }
    @IBOutlet weak var collectionView: MenuCollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    var presenter: UserMenuPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var permission: PermissionModel = PermissionModel()
    var current_guild: GuildEntity = GuildEntity()
    var current_member: MemberEntity = MemberEntity()
    var repository: UserEntity = UserEntity()
    var menus: [MenuCollectionViewCell.MenuType] = [
        .none, .none, .ban, .none, .none,
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        UserMenuViewControllerBuilder.rebuild(self)
        configureCollectionView()
        updateLayout()
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func setRepository(repository: UserEntity) {
        self.repository = repository
        if isViewLoaded {
            self.updateLayout()
        }
    }
    
    func updateLayout() {
        self.activeView.setRepository(repository: repository)
        self.nameLabel.text = repository.nickname
        self.descriptionLabel.text = repository.description
        if self.descriptionLabel.text?.count ?? 0 == 0 {
            self.descriptionLabel.text = R.string.localizable.noneIntrouction()
        }
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: memberImageView)
        }
        self.evalMenus()
        self.collectionView.reloadData()
        
        panModalSetNeedsLayoutUpdate()
    }
    
    func evalMenus() {
        if current_member.id == nil || current_guild.id == nil {
            self.menus = [.none, .none, .none, .none, .none]
        } else if permission.Guild.banable {
            self.menus = [.none, .none, .ban, .none, .none]
        } else {
            self.menus = [.none, .none, .none, .none, .none]
        }
    }
}

extension UserMenuViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        self.collectionView.configure()
        self.collectionView.setRepositories(menus)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionViewHeightConstraint.constant = self.collectionView.calcHeight()
    }
}

extension UserMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.collectionView.collectionView(cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.collectionView.collectionView(didSelectItemAt: indexPath) {
        case .ban: self.presenter?.block(repository: repository, from: current_guild)
        default: break
        }
    }
}

extension UserMenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MenuCollectionViewCell.cellSize
    }
}

extension UserMenuViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return scrollView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(260 + descriptionLabel.frame.height)
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    var allowsExtendedPanScrolling: Bool { return true }
}

extension UserMenuViewController: UserMenuPresenterOutput {
    func sync(didFetchRepository repository: UserEntity) {
        DispatchQueue.main.async {
            self.repository = repository
            self.updateLayout()
        }
    }
    
    func userMenuPresenter(onError error: Error, in memberMenuPresenter: UserMenuPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func userMenuPresenter(didBlock repository: UserEntity, in memberMenuPresenter: UserMenuPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.success)
            self.dismissShouldAppear(animated: true)
        }
    }
}
