//
//  ParticipantsViewController.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/21.
//

import Foundation
import UIKit

protocol ParticipantsViewControllerDelegate: class {
    var voiceConnectable: Bool { get }
    var muted: Bool { get set }
    func mute()
    func unmute()
    func participantsViewControllerWillDismiss(in participantsViewController: ParticipantsViewController)
    func participantsViewControllerDidDismiss(in participantsViewController: ParticipantsViewController)
}

extension ParticipantsViewControllerDelegate {
    func participantsViewControllerWillDismiss(in participantsViewController: ParticipantsViewController) {}
    func participantsViewControllerDidDismiss(in participantsViewController: ParticipantsViewController) {}
}

class ParticipantsViewController: BaseViewController, PanContainerNavigationViewChildDelegate {
    weak var parentContainerView: PanContainerNavigationView!
    @IBOutlet weak var participantCollectionViewTopConstraint: NSLayoutConstraint! {
        didSet {
            participantCollectionViewTopConstraint.constant = UIScreen.main.hasNotch ? 40 : 0
        }
    }
    @IBOutlet weak var participantCollectionView: ParticipantCollectionView!
    @IBOutlet weak var footerView: UIView! {
        didSet {
            footerView.backgroundColor = Theme.themify(key: .transparent)
        }
    }
    @IBOutlet weak var muteButton: ActionButton! {
        didSet {
            muteButton.delegate = self
            muteButton.image = R.image.mic()!.withRenderingMode(.alwaysTemplate)
            muteButton.imageView.tintColor = .white
            muteButton.baseView.backgroundColor = self.delegate?.muted ?? true ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
        }
    }
    var presenter: ParticipantsPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var permission: PermissionModel = PermissionModel()
    var current_member: MemberEntity = MemberEntity()
    var current_guild: GuildEntity = GuildEntity()
    var current_channel: StreamChannelEntity = StreamChannelEntity()
    var repositories: ParticipantEntities = ParticipantEntities()
    var current_participant: ParticipantEntity? {
        return repositories.filter({ $0.Member == self.current_member }).first
    }
    weak var delegate: ParticipantsViewControllerDelegate?
    
    override func viewDidLoad() {
        navigationMode = .modalBorder
        super.viewDidLoad()
        ParticipantsViewControllerBuilder.rebuild(self)
        self.view.backgroundColor = Theme.themify(key: .backgroundThick)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.muteButton.isHidden = !(self.delegate?.voiceConnectable ?? false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.fetchParticipants()
        configureCollectionView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func onTapNavigationCloseButton(_ sender: Any) {
        self.delegate?.participantsViewControllerWillDismiss(in: self)
        self.dismissContainer(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            self.delegate?.participantsViewControllerDidDismiss(in: self)
        })
    }
    
    func setRepositories(_ repositories: ParticipantEntities) {
        self.repositories = repositories
        self.participantCollectionView.setRepositories(repositories)
    }
}

extension ParticipantsViewController: ActionButtonDelegate {
    func actionButton(onTap actionButton: ActionButton) {
        self.delegate?.muted ?? true ? self.delegate?.unmute() : self.delegate?.mute()
        muteButton.baseView.backgroundColor = self.delegate?.muted ?? true ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
    }
}

extension ParticipantsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    fileprivate func configureCollectionView() {
        participantCollectionView.configure(row: 2, size: self.participantCollectionView.bounds.size)
        participantCollectionView.delegate = self
        participantCollectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return participantCollectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return participantCollectionView.collectionView(cellForItemAt: indexPath)
    }
}

extension ParticipantsViewController: StreamChannelViewControllerDelegate {
    func streamChannelViewController(didChange meterLevel: Float, in streamChannelViewController: StreamChannelViewController) {
        current_participant?.meterLevel = meterLevel
    }
    
    func streamChannelViewController(didChange meterLevel: Float, of peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController) {
        guard let participant = self.repositories.filter({ $0.Member?.UserId == peerConnection.user?.id }).first else { return }
        participant.meterLevel = meterLevel
    }
    
    func streamChannelViewController(didAdd peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController) {
        presenter?.fetchParticipants()
    }
    
    func streamChannelViewController(didRemove peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController) {
        presenter?.fetchParticipants()
    }
}

extension ParticipantsViewController: ParticipantsPresenterOutput {
    func sync(didFetchRepositories repositories: ParticipantEntities) {
        self.setRepositories(repositories)
    }
    
    func participantsPresenter(onError error: Error, in participantsPresenter: ParticipantsPresenter) {
    }
}
