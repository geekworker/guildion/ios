//
//  GuildsEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka

class GuildsEditViewController: BaseFormViewController {
    var presenter: GuildsEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: GuildEntities = GuildEntities()
    fileprivate var selectedGuild: GuildEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildsEditViewControllerBuilder.rebuild(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        title = R.string.localizable.guildsSetting()
        self.tableView.setEditing(true, animated: false)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildSettingViewController, segue.identifier == R.segue.guildsEditViewController.toGuildSetting.identifier, let repository = selectedGuild {
            vc.repository = repository
            selectedGuild = nil
        }
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }

    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        let section = MultivaluedSection(multivaluedOptions: .Reorder, header: R.string.localizable.menuGuilds())
        section.tag = "section"
        
        if form.sectionBy(tag: "section") == nil {
            form +++ section
        }

        for repository in repositories.items.enumerated() {
            repository.element.index = repository.offset + 1
            form.last! <<< ButtonRow(repository.element.uid){ lrow in
                lrow.title = repository.element.name
                lrow.selectableValue = repository.element.uid
            }
            .cellUpdate { [unowned self] in
                if let url = URL(string: repository.element.picture_small) {
                    self.buttonImageRowCellUpdate($0, $1, url: url)
                } else {
                    self.buttonImageRowCellUpdate($0, $1, url: URL(string: DataConfig.Image.default_guild_image_url)!)
                }
            }
            .onCellSelection { [unowned self] (cell, row) in
                self.selectedGuild = self.repositories.filter({ $0.uid == row.tag }).first
                self.presenter?.toGuildSetting()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard let source_repository = self.repositories[safe: sourceIndexPath.row], sourceIndexPath.row != destinationIndexPath.row else { return }
        repositories.remove(source_repository)
        repositories.insert(source_repository, at: destinationIndexPath.row)
        let entities = GuildEntities()
        entities.items = repositories
            .enumerated()
            .compactMap({
                $0.element.index = $0.offset + 1
                return $0.element
            })
            .filter({
                $0.index_updated
            })
        self.presenter?.updateIndexes(repositories: entities)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.setReorderControl(cell)
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension GuildsEditViewController: GuildsEditPresenterOutput {
    func sync(didFetchRepositories repositories: GuildEntities) {
        let shouldConfigureForm = self.repositories.count == 0 && repositories.count > 0
        self.repositories = repositories
        if shouldConfigureForm {
            self.configureForm()
        } else {
            self.reloadForm()
        }
    }
}
