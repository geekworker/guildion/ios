//
//  MemberMenuController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import PanModal
import Nuke
import SwiftMessages
import PKHUD

class MemberMenuViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var headerView: UIView! {
        didSet {
            headerView.layer.cornerRadius = 8
            headerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var memberImageView: UIImageView! {
        didSet {
            memberImageView.layer.cornerRadius = memberImageView.frame.width / 2
            memberImageView.clipsToBounds = true
            memberImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
            nameLabel.text = ""
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Theme.themify(key: .string)
            descriptionLabel.text = R.string.localizable.noneIntrouction()
        }
    }
    @IBOutlet weak var activeView: ActiveView! {
        didSet {
            activeView.baseView.layer.borderWidth = 1
            activeView.baseView.layer.borderColor = Theme.themify(key: .border).cgColor
            activeView.layer.cornerRadius = activeView.frame.width / 2
            activeView.clipsToBounds = true
        }
    }
    @IBOutlet weak var statusLabel: UILabel! {
        didSet {
            statusLabel.textColor = Theme.themify(key: .main)
            statusLabel.text = ""
        }
    }
    @IBOutlet weak var simpleButton: SimpleButton! {
        didSet {
            simpleButton.canceled = false
            simpleButton.normalColor = Theme.themify(key: .cancel)
            simpleButton.delegate = self
            simpleButton.textLabel.text = R.string.localizable.memberManage()
        }
    }
    @IBOutlet weak var simpleButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var simpleButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: MenuCollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    var presenter: MemberMenuPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var permission: PermissionModel = PermissionModel()
    var current_guild: GuildEntity = GuildEntity()
    var current_member: MemberEntity = MemberEntity()
    var repository: MemberEntity = MemberEntity()
    var menus: [MenuCollectionViewCell.MenuType] = [
        .message, .voiceChannel, .watchParty, .movieChannel, .setting,
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        MemberMenuViewControllerBuilder.rebuild(self)
        configureCollectionView()
        updateLayout()
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        presenter?.fetch(repository)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func setRepository(repository: MemberEntity) {
        self.repository = repository
        if isViewLoaded {
            self.updateLayout()
        }
    }
    
    func updateLayout() {
        if let user = repository.User {
            self.activeView.setRepository(repository: user)
        }
        self.activeView.isHidden = repository.User == nil
        self.nameLabel.text = repository.nickname == "" ? repository.User?.nickname : repository.nickname
        self.descriptionLabel.text = repository.description == "" ? repository.User?.description : repository.description
        self.statusLabel.text = repository.MemberRoles?.sorted().filter({ $0.position.count > 0 }).first?.position ?? ""
        if self.descriptionLabel.text?.count ?? 0 == 0 {
            self.descriptionLabel.text = R.string.localizable.noneIntrouction()
        }
        if let picture_small = repository.picture_small == "" ? repository.User?.picture_small : repository.picture_small, let url = URL(string: picture_small) {
            Nuke.loadImage(with: url, into: memberImageView)
        }
        
        let isMyAccount = false // repository.UserId == CurrentUser.getCurrentUserEntity()?.id
        self.simpleButtonTopConstraint.constant = isMyAccount ? 20 : 0
        self.simpleButtonHeightConstraint.constant = isMyAccount ? 32 : 0
        self.simpleButton.isHidden = !isMyAccount
        
        self.evalMenus()
        self.collectionView.reloadData()
        
        panModalSetNeedsLayoutUpdate()
    }
    
    func evalMenus() {
        let isMyAccount = repository.UserId == CurrentUser.getCurrentUserEntity()?.id
        if isMyAccount {
            self.menus = [
                .message, .none, .watchParty, .none, .setting,
            ]
            return
        }
        
        let user = UserEntity()
        user.id = repository.UserId
        if UserDispatcher.checkBlocked(target: user, state: store.state.user) {
            self.menus = [.none, .none, .setting, .none, .none]
        } else {
            self.menus = [
                .message, .voiceChannel, .watchParty, .movieChannel, .setting,
            ]
        }
        
        if current_member.id == nil || current_guild.id == nil {
            self.menus = [.none, .none, .setting, .none, .none]
        }
    }
}

extension MemberMenuViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        self.collectionView.configure()
        self.collectionView.setRepositories(menus)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionViewHeightConstraint.constant = self.collectionView.calcHeight()
    }
}

extension MemberMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.collectionView.collectionView(cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.collectionView.collectionView(didSelectItemAt: indexPath) {
        case .setting: self.presenter?.toMemberEdit()
        case .message: self.presenter?.toDMChannel(repository: repository)
        case .watchParty: self.presenter?.startStreamChannel(repository: repository)
        case .movieChannel: self.presenter?.startPlayChannel(repository: repository)
        case .voiceChannel: self.presenter?.startVoiceChannel(repository: repository)
        case .copyLink:
            self.presenter?.copy_url(repository)
            self.dismissShouldAppear(animated: true, completion: {
                let messageAlert = MessageView.viewFromNib(layout: .cardView)
                messageAlert.configureTheme(.success)
                messageAlert.configureDropShadow()
                messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successCopy())
                messageAlert.button?.isHidden = true
                var messageAlertConfig = SwiftMessages.defaultConfig
                messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
                SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
            })
        default: break
        }
    }
}

extension MemberMenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MenuCollectionViewCell.cellSize
    }
}

extension MemberMenuViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return scrollView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(260 + descriptionLabel.frame.height)
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    var allowsExtendedPanScrolling: Bool { return true }
}

extension MemberMenuViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.presenter?.toMemberEdit()
    }
}

extension MemberMenuViewController: MemberMenuPresenterOutput {
    func sync(didFetchRepository repository: MemberEntity) {
        DispatchQueue.main.async {
            self.repository = repository
            self.updateLayout()
        }
    }
    
    func memberMenuPresenter(willToStreamChannel repository: StreamChannelEntity, in memberMenuPresenter: MemberMenuPresenter) {
        DispatchQueue.main.async {
            self.presenter?.toStreamChannel(repository: repository)
        }
    }
    
    func memberMenuPresenter(onError error: Error, in memberMenuPresenter: MemberMenuPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
}
