//
//  ChannelNotificationEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class ChannelNotificationEditViewController: BaseFormViewController {
    var presenter: ChannelNotificationEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var channel: ChannelableProtocol = TextChannelEntity()
    var repository: ChannelEntryableProtocol = TextChannelEntryEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: ChannelEntryableProtocol = TextChannelEntryEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelNotificationEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        presenter?.fetch(repository: channel)
        title = R.string.localizable.channelNotificationSetting()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismissShouldAppear(animated: true)
    }
    
    func handleValueChange() {
        guard repository.channelable_type.superChannel == new_repository.channelable_type.superChannel else { return }
        switch new_repository.channelable_type.superChannel {
        case .TextChannel:
            guard let unwrapped = repository as? TextChannelEntryEntity, let new_unwrapped = new_repository as? TextChannelEntryEntity else { return }
            if unwrapped.mute == new_unwrapped.mute && unwrapped.messages_mute == new_unwrapped.messages_mute && unwrapped.mentions_mute == new_unwrapped.mentions_mute && unwrapped.everyone_mentions_mute == new_unwrapped.everyone_mentions_mute {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        case .StreamChannel:
            guard let unwrapped = repository as? StreamChannelEntryEntity, let new_unwrapped = new_repository as? StreamChannelEntryEntity else { return }
            if unwrapped.mute == new_unwrapped.mute && unwrapped.messages_mute == new_unwrapped.messages_mute && unwrapped.mentions_mute == new_unwrapped.mentions_mute && unwrapped.everyone_mentions_mute == new_unwrapped.everyone_mentions_mute {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        case .DMChannel:
            guard let unwrapped = repository as? DMChannelEntryEntity, let new_unwrapped = new_repository as? DMChannelEntryEntity else { return }
            if unwrapped.mute == new_unwrapped.mute && unwrapped.messages_mute == new_unwrapped.messages_mute && unwrapped.mentions_mute == new_unwrapped.mentions_mute && unwrapped.everyone_mentions_mute == new_unwrapped.everyone_mentions_mute {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        default: self.resetRightNavigationBarButton()
        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
         self.presenter?.update(new_repository)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        switch new_repository.channelable_type.superChannel {
        case .TextChannel:
            guard let new_unwrapped = new_repository as? TextChannelEntryEntity else { return }
            self.configureTextChannelForm(new_unwrapped)
        case .StreamChannel:
            guard let new_unwrapped = new_repository as? StreamChannelEntryEntity else { return }
            self.configureStreamChannelForm(new_unwrapped)
        case .DMChannel:
            guard let new_unwrapped = new_repository as? DMChannelEntryEntity else { return }
            self.configureDMChannelForm(new_unwrapped)
        default: break
        }
    }
    
    fileprivate func configureStreamChannelForm(_ channel: StreamChannelEntryEntity) {
        form +++
            Section(R.string.localizable.channelNotificationSetting())
            <<< SwitchRow(R.string.localizable.channelNotificationMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.repository.mute
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationMessageMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.messages_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.messages_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationMentionMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.mentions_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationEveryoneMentionMute()) {
                $0.title = $0.tag
                $0.value = self.new_repository.everyone_mentions_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.everyone_mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
    }
    
    fileprivate func configureTextChannelForm(_ channel: TextChannelEntryEntity) {
        form +++
            Section(R.string.localizable.channelNotificationSetting())
            <<< SwitchRow(R.string.localizable.channelNotificationMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.repository.mute
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationMessageMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.messages_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.messages_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationMentionMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.mentions_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationEveryoneMentionMute()) {
                $0.title = $0.tag
                $0.value = self.new_repository.everyone_mentions_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.everyone_mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
    }
    
    fileprivate func configureDMChannelForm(_ channel: DMChannelEntryEntity) {
        form +++
            Section(R.string.localizable.channelNotificationSetting())
            <<< SwitchRow(R.string.localizable.channelNotificationMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.repository.mute
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationMessageMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.messages_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.messages_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationMentionMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.mentions_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.channelNotificationEveryoneMentionMute()) {
                $0.title = $0.tag
                $0.value = self.new_repository.everyone_mentions_mute
                $0.hidden = .function([R.string.localizable.channelNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.channelNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.everyone_mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
    }
}

extension ChannelNotificationEditViewController: ChannelNotificationEditPresenterOutput {
    func channelNotificationEditPresenter(didUpdate repository: ChannelEntryableProtocol, in channelNotificationEditPresenter: ChannelNotificationEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    func channelNotificationEditPresenter(onError error: Error, in channelNotificationEditPresenter: ChannelNotificationEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func sync(didFetchRepository repository: ChannelEntryableProtocol) {
        self.repository = repository
        self.reloadForm()
    }
}
