//
//  UserFilesViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit
import Eureka
import PKHUD
import Nuke

class UserFilesViewController: BaseFormViewController {
    var presenter: UserFilesPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: FileEntities = FileEntities()
    var repository: UserEntity = CurrentUser.getCurrentUserEntity() ?? UserEntity()
    fileprivate var selectedFile: FileEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        UserFilesViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        presenter?.fetch()
        title = R.string.localizable.userFiles()
        self.tableView.setEditing(true, animated: false)
        
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FileEditViewController, segue.identifier == R.segue.userFilesViewController.toFileEdit.identifier, let repository = selectedFile {
            vc.repository = repository
            selectedFile = nil
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        let section = MultivaluedSection(multivaluedOptions: .Delete) { [unowned self] in
            var header = HeaderFooterView<UserFilesHeaderView>(.class)
            header.height = { UserFilesHeaderView.viewHeight }
            header.onSetupView = { [unowned self] (view, section) -> () in
                view.setRepository(self.repository)
                view.delegate = self
            }
            $0.tag = "section"
            $0.header = header
        }
        
        if form.sectionBy(tag: "section") == nil {
            form +++ section
        }
        
        for repository in repositories.items {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = "\(repository.size) \(repository.name)"
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                if let url = URL(string: repository.thumbnail) {
                    self.buttonImageRowCellUpdate($0, $1, url: url)
                } else {
                    self.buttonRowCellUpdate($0, $1)
                }
            }
            .onCellSelection { [unowned self] cell, row in
                guard let repository = self.repositories.filter({ "\($0.id ?? 0)" == row.tag }).first else { return }
                self.selectedFile = repository
                self.presenter?.toFileMenu(repository: repository)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete, let repository = self.repositories[safe: indexPath.row] else { return }
        HUD.show(.progress)
        self.presenter?.delete(repository: repository)
        self.repositories.items = self.repositories.filter({ $0 != repository })
        self.reloadForm()
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension UserFilesViewController: UserFilesHeaderViewDelegate {
    func userFilesHeaderView(onTap userFilesHeaderView: UserFilesHeaderView) {
        self.presenter?.toPlans()
    }
}

extension UserFilesViewController: UserFilesPresenterOutput {
    func sync(didFetchRepositories repositories: FileEntities) {
        let shouldConfigure = self.repositories.count == 0 && repositories.count > 0
        self.repositories = repositories
        if shouldConfigure {
            self.configureForm()
        } else {
            self.reloadForm()
        }
    }
    
    func userFilesPresenter(shouldReload repository: UserEntity) {
        self.repository = repository
        self.tableView.reloadData()
    }
    
    func userFilesPresenter(onError error: Error, in userFilesPresenter: UserFilesPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func userFilesPresenter(didDelete repository: FileEntity, in userFilesPresenter: UserFilesPresenter) {
        HUD.hide()
        HUD.flash(.success)
    }
}
