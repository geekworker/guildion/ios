//
//  MypageViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/07.
//

import Foundation
import UIKit
import Eureka
import WebBrowser
import PKHUD
import SwiftMessages

class MypageViewController: BaseFormViewController {
    public lazy var webBrowserViewController: WebBrowserViewController = {
        let webBrowserViewController = WebBrowserViewController()
        webBrowserViewController.delegate = self
        webBrowserViewController.language = .english
        webBrowserViewController.tintColor = Theme.themify(key: .main)
        webBrowserViewController.barTintColor = .white // Theme.themify(key: .background)
        webBrowserViewController.navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
        webBrowserViewController.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Theme.themify(key: .string)]
        webBrowserViewController.isToolbarHidden = false
        webBrowserViewController.isShowActionBarButton = true
        webBrowserViewController.toolbarItemSpace = 50
        webBrowserViewController.isShowURLInNavigationBarWhenLoading = true
        webBrowserViewController.isShowPageTitleInNavigationBar = true
        return webBrowserViewController
    }()
    var presenter: MypagePresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: UserEntity = CurrentUser.getCurrentUserEntity() ?? UserEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.localizable.mypage()
        MypageViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? UserEditViewController, segue.identifier == R.segue.mypageViewController.toUserEdit.identifier {
            vc.repository = repository
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        form +++
            Section() { [unowned self] in
                var header = HeaderFooterView<MypageHeaderView>(.class)
                header.height = { MypageHeaderView.viewHeight }
                header.onSetupView = { [unowned self] (view, section) -> () in
                    view.delegate = self
                    view.setRepository(self.repository)
                }
                $0.header = header
            }
            +++ Section(R.string.localizable.userMenu())
            <<< ButtonRow(R.string.localizable.userMenuUserEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.mypageViewController.toUserEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
            <<< ButtonRow(R.string.localizable.userMenuMembersEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.mypageViewController.toMembersEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
            <<< ButtonRow(R.string.localizable.userMenuGuildsEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.mypageViewController.toGuildsEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
//            <<< ButtonRow(R.string.localizable.userMenuAccountEdit()) {
//                $0.title = $0.tag
//                $0.presentationMode = .segueName(segueName: R.segue.mypageViewController.toAccountEdit.identifier, onDismiss: nil)
//            }.cellUpdate { self.buttonRowCellUpdate($0, $1) }
            <<< ButtonRow(R.string.localizable.userMenuNotificationEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.mypageViewController.toUserNotificationEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
//            <<< ButtonRow(R.string.localizable.userFiles()) {
//                $0.title = $0.tag
//            }
//            .cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
//            .onCellSelection { [unowned self] _,_ in self.presenter?.toUserFiles() }
        
            +++ Section(R.string.localizable.userMenuApp())
//            <<< PushRow<Themes>(R.string.localizable.userMenuTheme()) {
//                $0.title = $0.tag
//                $0.options = Themes.allCases
//                $0.value = Themes.current
//                }.onPresent({ (_, vc) in
//                    vc.enableDeselection = false
//                    vc.dismissOnSelection = false
//                }).cellUpdate { self.pushRowCellUpdate($0, $1) }
            <<< ButtonRow(R.string.localizable.userMenuLang()) {
                $0.title = $0.tag
            }.onCellSelection { _, _ in
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
            .cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
 
            +++ Section(R.string.localizable.userMenuSupport())
            <<< ButtonRow(R.string.localizable.userMenuFAQ()) {
                $0.title = $0.tag
            }.onCellSelection { [unowned self] cell, row in
                self.webBrowserViewController.loadURL(URL(string: Constant.APP_URL + "/document/supports")!)
                let navigationWebBrowser = WebBrowserViewController.rootNavigationWebBrowser(webBrowser: self.webBrowserViewController)
                self.present(navigationWebBrowser, animated: true, completion: nil)
            }.cellUpdate { self.buttonRowLeftAlignmentUpdate($0, $1) }
            <<< ButtonRow(R.string.localizable.userMenuTerms()) {
                $0.title = $0.tag
            }.onCellSelection { [unowned self] cell, row in
                self.webBrowserViewController.loadURL(URL(string: Constant.APP_URL + "/document/legals/terms_and_conditions")!)
                let navigationWebBrowser = WebBrowserViewController.rootNavigationWebBrowser(webBrowser: self.webBrowserViewController)
                self.present(navigationWebBrowser, animated: true, completion: nil)
            }.cellUpdate { self.buttonRowLeftAlignmentUpdate($0, $1) }
            <<< ButtonRow(R.string.localizable.userMenuPrivacyPolicy()) {
                $0.title = $0.tag
            }.onCellSelection { [unowned self] cell, row in
                self.webBrowserViewController.loadURL(URL(string: Constant.APP_URL + "/document/legals/privacy_policies")!)
                let navigationWebBrowser = WebBrowserViewController.rootNavigationWebBrowser(webBrowser: self.webBrowserViewController)
                self.present(navigationWebBrowser, animated: true, completion: nil)
            }.cellUpdate { self.buttonRowLeftAlignmentUpdate($0, $1) }
        
        
            +++ Section("")
            <<< ButtonRow(R.string.localizable.userMenuLogout()) {
                $0.title = $0.tag
            }.onCellSelection { [unowned self] cell, row in
                let alert: UIAlertController = UIAlertController(title: R.string.localizable.logout(), message: R.string.localizable.logoutReally(), preferredStyle:  UIAlertController.Style.alert)
                            
                let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.logout(), style: UIAlertAction.Style.default, handler:{
                    [unowned self](action: UIAlertAction!) -> Void in
                    self.presenter?.logout()
                })
                let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                    (action: UIAlertAction!) -> Void in
                })
                alert.addAction(cancelAction)
                alert.addAction(defaultAction)
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                self.present(alert, animated: true, completion: nil)
            }
            .cellUpdate { [unowned self] in
                self.buttonRowCellUpdate($0, $1)
                $0.textLabel?.textColor = Theme.themify(key: .danger)
                $0.textLabel?.font = Font.FT(size: .FM, family: .B)
            }
    }
}

extension MypageViewController: WebBrowserDelegate {
}

extension MypageViewController: MypageHeaderViewDelegate {
    func mypageHeaderView(onTap mypageHeaderView: MypageHeaderView) {
        self.presenter?.toPlans()
    }
    
    func mypageHeaderView(willToFiles mypageHeaderView: MypageHeaderView) {
        self.presenter?.toUserFiles()
    }
}

extension MypageViewController: MypagePresenterOutput {
    func sync(didFetchRepository repository: UserEntity) {
        self.repository = repository
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func mypagePresenter(didLogout mypagePresenter: MypagePresenter) {
        DispatchQueue.main.async {
            self.dismiss(animated: true)
            self.navigationController?.popViewController(animated: true)
            BaseWireframe.toWelcome()
        }
    }
    
    func mypagePresenter(onError error: Error, in mypagePresenter: MypagePresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.error)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.error(), body: error.getLocalizedReason())
            messageAlert.button?.isHidden = true
            var messageAlertConfig = SwiftMessages.defaultConfig
            messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
        }
    }
}
