//
//  ChannelMembersEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/02.
//

import Foundation
import UIKit
import PKHUD
import Eureka

class ChannelMembersEditViewController: BaseFormViewController {
    var presenter: ChannelMembersEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: MemberEntities = MemberEntities()
    var repository: ChannelableProtocol = TextChannelEntity()
    fileprivate var selectedMember: MemberEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelMembersEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setEditing(true, animated: false)
        presenter?.bind()
        self.presenter?.fetch(repository: repository)
        
        guard navigationController?.viewControllers.count == 1 else { return }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.rightBarButtonItems = [editButtonItem]
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if !editing {
            self.dismissShouldAppear(animated: true)
        } else {
            self.tableView.setEditing(editing, animated: animated)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MemberEditViewController, segue.identifier == R.segue.guildMembersEditViewController.toMemberEdit.identifier, let repository = selectedMember, let guild = GuildConnector.shared.current_guild {
            vc.repository = repository
            vc.guild = guild
            selectedMember = nil
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        let section = MultivaluedSection(multivaluedOptions: .Delete)
        section.tag = "section"
        
        if form.sectionBy(tag: "section") == nil {
            form +++ section
        }
        
        for repository in repositories.items {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = repository.nickname == "" ? repository.User?.nickname ?? "" : repository.nickname
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                if let url = URL(string: repository.picture_small) {
                    self.buttonImageRowCellUpdate($0, $1, url: url)
                } else {
                    self.buttonRowCellUpdate($0, $1)
                }
            }
            .onCellSelection { [unowned self] cell, row in
                self.selectedMember = self.repositories.filter({ "\($0.id ?? 0)" == row.tag }).first
                self.presenter?.toMemberEdit()
            }
        }
        
        guard self.repositories.count > 0 else { return }

        form
            +++ Section("")
            <<< ButtonRow(R.string.localizable.invite()) {
                $0.title = $0.tag
            }
            .cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
            .onCellSelection { [unowned self] _, _ in
                self.presenter?.toMemberSelect()
            }
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete, let repository = self.repositories[safe: indexPath.row] else { return }
        let entities = MemberEntities()
        entities.items = [repository.toNewMemory()]
        self.presenter?.kick(repository: self.repository, targets: entities)
        self.repositories.items = self.repositories.filter({ $0 != repository })
        self.reloadForm()
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension ChannelMembersEditViewController: MemberSelectViewControllerDelegate {
    func memberSelectViewController(shouldDismiss selecteds: MemberEntities, in memberSelectViewController: MemberSelectViewController) {
        guard selecteds.count > 0 else { return }
        HUD.show(.progress)
        self.presenter?.invite(repository: repository, repositories: selecteds)
    }
}

extension ChannelMembersEditViewController: ChannelMembersEditPresenterOutput {
    func sync(didFetchRepositories repositories: MemberEntities) {
        let shouldConfigure = self.repositories.count == 0 && repositories.count > 0
        self.repositories = repositories
        if shouldConfigure {
            configureForm()
        } else {
            reloadForm()
        }
    }
    
    func channelMembersEditPresenter(didInvite repositories: MemberEntities, in channelMembersEditPresenter: ChannelMembersEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.presenter?.fetch(repository: repository)
    }
    
    func channelMembersEditPresenter(didKick repositories: MemberEntities, in channelMembersEditPresenter: ChannelMembersEditPresenter) {
    }
    
    func channelMembersEditPresenter(onError error: Error, in channelMembersEditPresenter: ChannelMembersEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
