//
//  BaseFormViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import Eureka
import Nuke
import UIKit

class BaseFormViewController: FormViewController {
    var sessionChecking: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureForm()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sessionCheck()
        setupNavigationBar()
        self.view.backgroundColor = Theme.themify(key: .backgroundLight)
        self.tableView.backgroundColor = Theme.themify(key: .backgroundLight)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func sessionCheck() {
        guard CurrentUser.getCurrentUserEntity() == nil, sessionChecking else { return }
        self.view.isHidden = true
        BaseWireframe.toWelcome()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = Theme.themify(key: .backgroundThick)
        navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)]
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        form.removeAll()
    }
    
    func dismissShouldAppear(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.dismiss(animated: flag, completion: {
            UIViewController.topMostController().viewWillAppear(false)
            UIViewController.topMostController().viewDidAppear(false)
            completion?()
        })
    }
    
    private var hasSaveButton: Bool = false
    func setupSaveNavigationBarButton() {
        guard !hasSaveButton else { return }
        let barButton = UIBarButtonItem(title: R.string.localizable.save(), style: .done, target: self, action: #selector(navigationBarSaveTapped(_:)))
        self.navigationItem.setRightBarButtonItems([barButton], animated: true)
        hasSaveButton = true
    }
    
    private var hasPlusButton: Bool = false
    func setupPlusNavigationBarButton() {
        guard !hasPlusButton else { return }
        let barButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(navigationBarAddTapped(_:)))
        self.navigationItem.setRightBarButtonItems([barButton], animated: true)
        hasPlusButton = true
    }
    
    func resetRightNavigationBarButton() {
        self.navigationItem.setRightBarButtonItems([], animated: true)
        hasSaveButton = false
        hasPlusButton = false
    }
    
    // MARK: override this method
    var isFormConfigured: Bool = false
    func configureForm() {
        guard !isFormConfigured else { return }
        ImageRow.defaultCellUpdate = { cell, row in
           cell.accessoryView?.layer.cornerRadius = 17
           cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        }
        isFormConfigured = true
    }
    
    func reloadForm(animated: Bool = false) {
        isFormConfigured = false
        guard !animated else { self.form.removeAll(); self.configureForm(); return }
        let offset = self.tableView.contentOffset
        self.performWithoutAnimation({
            self.form.removeAll()
            self.configureForm()
        }) {
            self.tableView.setContentOffset(offset, animated: false)
        }
    }
    
    private func performWithoutAnimation(_ closureToPerform: @escaping(() -> Void), completion: (() -> Void)?) {
        UIView.setAnimationsEnabled(false)
        UIView.animate(withDuration: 0, animations: {
            closureToPerform()
        }, completion: { _ in
            UIView.setAnimationsEnabled(true)
            completion?()
        })
    }
    
//    override func insertAnimation(forRows rows: [BaseRow]) -> UITableView.RowAnimation {
//        return .none
//    }
//    
//    override func deleteAnimation(forRows rows: [BaseRow]) -> UITableView.RowAnimation {
//        return .none
//    }
//    
//    override func reloadAnimation(oldRows: [BaseRow], newRows: [BaseRow]) -> UITableView.RowAnimation {
//        return .none
//    }
//
//    override func insertAnimation(forSections sections: [Section]) -> UITableView.RowAnimation {
//        return .none
//    }
//
//    override func deleteAnimation(forSections sections: [Section]) -> UITableView.RowAnimation {
//        return .none
//    }
//
//    override func reloadAnimation(oldSections: [Section], newSections: [Section]) -> UITableView.RowAnimation {
//        return .none
//    }
    
    @objc func navigationBarSaveTapped(_ sender: Any) {}
    @objc func navigationBarAddTapped(_ sender: Any) {}
    
    func setReorderControl(_ cell: UITableViewCell) {
        for subViewA in cell.subviews {
            if (subViewA.classForCoder.description() == "UITableViewCellReorderControl") {
                for subViewB in subViewA.subviews {
                    if (subViewB.isKind(of: UIImageView.classForCoder())) {
                        let imageView = subViewB as! UIImageView;
                        imageView.image = R.image.list()?.withRenderingMode(.alwaysTemplate)
                        imageView.tintColor = Theme.themify(key: .stringSecondary)
                        break
                    }
                }
                break
            }
        }
    }
    
    func setDisclosureIndicator(_ cell: UITableViewCell) {
        let image = R.image.chevron()?.withRenderingMode(.alwaysTemplate)
        let imageView  = UIImageView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        imageView.image = image
        imageView.tintColor = Theme.themify(key: .stringSecondary)
        cell.accessoryView = imageView
        cell.editingAccessoryView = imageView
    }
    
    func buttonRowCellUpdate(_ cell: ButtonCell, _ row: ButtonRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .string)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
        cell.selectedBackgroundView = bgColorView
    }
    
    func buttonImageRowCellUpdate(_ cell: ButtonCell, _ row: ButtonRow, url: URL) {
        cell.textLabel?.textAlignment = .left
        setDisclosureIndicator(cell)
        self.buttonRowCellUpdate(cell, row)
        cell.indentationLevel = 1
        cell.indentationWidth = 40
        let imageView  = UIImageView(frame: CGRect(x: 20, y: 7, width: 30, height: 30))
        cell.contentView.addSubview(imageView)
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        Nuke.loadImage(with: url, into: imageView)
    }
    
    func buttonImageRowCellUpdate(_ cell: ButtonCell, _ row: ButtonRow, image: UIImage) {
        cell.textLabel?.textAlignment = .left
        setDisclosureIndicator(cell)
        self.buttonRowCellUpdate(cell, row)
        cell.indentationLevel = 1
        cell.indentationWidth = 40
        let imageView  = UIImageView(frame: CGRect(x: 20, y: 7, width: 30, height: 30))
        cell.contentView.addSubview(imageView)
        imageView.image = image
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
    }
    
    func buttonIconRowCellUpdate(_ cell: ButtonCell, _ row: ButtonRow, icon: UIImage) {
        cell.textLabel?.textAlignment = .left
        setDisclosureIndicator(cell)
        self.buttonRowCellUpdate(cell, row)
        cell.indentationLevel = 1
        cell.indentationWidth = 26
        let imageView  = UIImageView(frame: CGRect(x: 20, y: 14, width: 16, height: 16))
        cell.contentView.addSubview(imageView)
        imageView.image = icon
        imageView.tintColor = Theme.themify(key: .stringSecondary)
        imageView.contentMode = .scaleAspectFill
    }
    
    func buttonImageRowCellUpdate(_ cell: ButtonCell, _ row: ButtonRow, color: UIColor) {
        cell.textLabel?.textAlignment = .left
        setDisclosureIndicator(cell)
        self.buttonRowCellUpdate(cell, row)
        cell.indentationLevel = 1
        cell.indentationWidth = 26
        let imageView  = UIImageView(frame: CGRect(x: 20, y: 14, width: 16, height: 16))
        cell.contentView.addSubview(imageView)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        view.backgroundColor = color
        imageView.image = view.asImage()
        imageView.tintColor = Theme.themify(key: .stringSecondary)
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
    }
    
    func buttonRowLeftAlignmentUpdate(_ cell: ButtonCell, _ row: ButtonRow) {
        cell.textLabel?.textAlignment = .left
        setDisclosureIndicator(cell)
        self.buttonRowCellUpdate(cell, row)
    }
    
    func pushRowCellUpdate<T: Equatable>(_ cell: PushSelectorCell<T>, _ row: PushRow<T>) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .string)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .stringSecondary)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        setDisclosureIndicator(cell)
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
        cell.selectedBackgroundView = bgColorView
    }
    
    func imageRowCellUpdate(_ cell: PushSelectorCell<UIImage>, _ row: ImageRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .string)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
        cell.selectedBackgroundView = bgColorView
        // cell.accessoryView?.layer.cornerRadius = 17
        // cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
    }
    
    func textRowCellUpdate(_ cell: TextCell, _ row: TextRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .stringSecondary)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        cell.textField.textColor = Theme.themify(key: .string)
        cell.textField.font = Font.FT(size: .FM, family: .L)
        cell.textField.attributedPlaceholder = NSAttributedString(string: cell.textField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : Theme.themify(key: .placeholder)])
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
    }
    
    func textAreaRowCellUpdate(_ cell: TextAreaCell, _ row: TextAreaRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .stringSecondary)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        cell.textView.textColor = Theme.themify(key: .string)
        cell.textView.font = Font.FT(size: .FM, family: .L)
        cell.textView.placeholderColor = Theme.themify(key: .placeholder)
        cell.placeholderLabel?.textColor = Theme.themify(key: .placeholder)
        cell.placeholderLabel?.font = Font.FT(size: .FM, family: .L)
        // cell.textView.attributedPlaceholder = NSAttributedString(string: cell.textView.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : Theme.themify(key: .placeholder)])
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
        
    }
    
    func intRowCellUpdate(_ cell: IntCell, _ row: IntRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .string)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        cell.textField.textColor = Theme.themify(key: .string)
        cell.textField.font = Font.FT(size: .FM, family: .L)
        cell.textField.attributedPlaceholder = NSAttributedString(string: cell.textField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : Theme.themify(key: .placeholder)])
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
    }
    
    func emailRowCellUpdate(_ cell: EmailCell, _ row: EmailRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .stringSecondary)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        cell.textField.textColor = Theme.themify(key: .string)
        cell.textField.font = Font.FT(size: .FM, family: .L)
        cell.textField.attributedPlaceholder = NSAttributedString(string: cell.textField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : Theme.themify(key: .placeholder)])
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
    }
    
    func passwordRowCellUpdate(_ cell: PasswordCell, _ row: PasswordRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .stringSecondary)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        cell.textField.textColor = Theme.themify(key: .string)
        cell.textField.font = Font.FT(size: .FM, family: .L)
        cell.textField.attributedPlaceholder = NSAttributedString(string: cell.textField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : Theme.themify(key: .placeholder)])
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
    }
    
    func switchRowCellUpdate(_ cell: SwitchCell, _ row: SwitchRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .string)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
    }
    func sliderRowCellUpdate(_ cell: SliderCell, _ row: SliderRow) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .string)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
    }
    
    func segmentedRowCellUpdate<T: Equatable>(_ cell: SegmentedCell<T>, _ row: SegmentedRow<T>) {
        cell.backgroundColor = Theme.themify(key: .background)
        cell.textLabel?.textColor = Theme.themify(key: .string)
        cell.textLabel?.font = Font.FT(size: .FM, family: .L)
        cell.detailTextLabel?.textColor = Theme.themify(key: .string)
        cell.detailTextLabel?.font = Font.FT(size: .FM, family: .L)
        let bgColorView = UIView()
        bgColorView.backgroundColor = cell.backgroundColor?.lighter(by: 10)
        cell.segmentedControl.backgroundColor = Theme.themify(key: .background)
        cell.segmentedControl.tintColor = Theme.themify(key: .cancel)
        if #available(iOS 13.0, *) {
            cell.segmentedControl.selectedSegmentTintColor = Theme.themify(key: .main)
        } else {
            cell.segmentedControl.tintColor = Theme.themify(key: .cancel)
        }
        cell.segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)], for: .selected)
        cell.segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)], for: .normal)
    }
}
