//
//  ChannelSettingViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class ChannelSettingViewController: BaseFormViewController {
    var presenter: ChannelSettingPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: ChannelableProtocol = TextChannelEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: ChannelableProtocol = TextChannelEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelSettingViewControllerBuilder.rebuild(self)
        title = R.string.localizable.channelSetting()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        presenter?.fetch(repository: repository)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ChannelRolesEditViewController {
            vc.repository = repository
        } else if let vc = segue.destination as? ChannelNotificationEditViewController {
            vc.channel = repository
        } else if let vc = segue.destination as? ChannelMembersEditViewController {
            vc.repository = repository
        }
    }

    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }

    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }

    func handleValueChange() {
        guard repository.channelable_type.superChannel == new_repository.channelable_type.superChannel else { return }
        switch new_repository.channelable_type.superChannel {
        case .TextChannel:
            guard let unwrapped = repository as? TextChannelEntity, let new_unwrapped = new_repository as? TextChannelEntity else { return }
            if unwrapped.name == new_unwrapped.name && unwrapped.description == new_unwrapped.description && unwrapped.is_nsfw == new_unwrapped.is_nsfw && unwrapped.is_default == new_unwrapped.is_default && unwrapped.permission == new_unwrapped.permission {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        case .StreamChannel:
            guard let unwrapped = repository as? StreamChannelEntity, let new_unwrapped = new_repository as? StreamChannelEntity else { return }
            if unwrapped.name == new_unwrapped.name && unwrapped.description == new_unwrapped.description && unwrapped.is_nsfw == new_unwrapped.is_nsfw && unwrapped.is_default == new_unwrapped.is_default && unwrapped.max_participant_count == new_unwrapped.max_participant_count  && unwrapped.permission == new_unwrapped.permission {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        case .DMChannel:
            guard let unwrapped = repository as? DMChannelEntity, let new_unwrapped = new_repository as? DMChannelEntity else { return }
            if unwrapped.description == new_unwrapped.description && unwrapped.permission == new_unwrapped.permission {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        default: self.resetRightNavigationBarButton()
        }
    }

    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)

        form
            +++ Section() { [unowned self] in
                var header = HeaderFooterView<ChannelSettingHeaderView>(.class)
                header.height = { ChannelSettingHeaderView.viewHeight }
                header.onSetupView = { [unowned self] (view, section) -> () in
                    view.setRepository(self.repository)
                }
                $0.header = header
            }

        switch new_repository.channelable_type.superChannel {
        case .TextChannel: self.configureTextChannelForm(new_repository as! TextChannelEntity)
        case .StreamChannel:
            self.configureStreamChannelForm(new_repository as! StreamChannelEntity)
        case .DMChannel:
            self.configureDMChannelForm(new_repository as! DMChannelEntity)
        default: break
        }
    }

    fileprivate func configureTextChannelForm(_ channel: TextChannelEntity) {

        if GuildConnector.shared.current_permission.TextChannel.channel_managable {
            form
                +++ Section(R.string.localizable.channelMenu())
                <<< TextRow(R.string.localizable.channelName()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.value = channel.name
                    $0.placeholder = $0.tag
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.name = value
                    self.handleValueChange()
                }.cellUpdate(textRowCellUpdate(_:_:))
                <<< TextAreaRow(R.string.localizable.description()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.placeholder = R.string.localizable.description()
                    $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
                    $0.value = channel.description
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.description = value
                    self.handleValueChange()
                }.cellUpdate(textAreaRowCellUpdate(_:_:))
        }

        let section =
            Section("")
            <<< ButtonRow(R.string.localizable.channelMenuNotificationEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.channelSettingViewController.toChannelNotificationEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }

        if GuildConnector.shared.current_permission.TextChannel.channel_members_managable {
            section
                <<< ButtonRow(R.string.localizable.channelMenuRoleEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.channelSettingViewController.toChannelRolesEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                <<< ButtonRow(R.string.localizable.menuMembersEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.channelSettingViewController.toChannelMembersEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
        }

        if GuildConnector.shared.current_permission.Guild.channels_managable {
            section
                <<< SwitchRow(R.string.localizable.channelFreeze()) {
                    $0.title = $0.tag
                    $0.value = !channel.permission
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.permission = !value
                    self.handleValueChange()
                }.cellUpdate(switchRowCellUpdate(_:_:))
        }

        form +++ section
        
        if GuildConnector.shared.current_permission.Guild.channels_managable && channel.deletable {
            form
                +++ Section("")
                <<< ButtonRow(R.string.localizable.channelMenuDelete()) {
                    $0.title = $0.tag
                    $0.onCellHighlightChanged({ _, _ in })
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: R.string.localizable.channelMenuDelete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)

                    let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.channelMenuDelete(), style: UIAlertAction.Style.default, handler:{
                        [unowned self] (action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        self.presenter?.delete(repository: self.repository)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }

    fileprivate func configureStreamChannelForm(_ channel: StreamChannelEntity) {
        
        if (GuildConnector.shared.current_permission.StreamChannel.channel_managable || channel.is_dm) && channel.configable
            {
            form
                +++ Section(R.string.localizable.channelMenu())
                <<< TextRow(R.string.localizable.channelName()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.value = channel.name
                    $0.placeholder = $0.tag
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.name = value
                    self.handleValueChange()
                }.cellUpdate(textRowCellUpdate(_:_:))
                <<< TextAreaRow(R.string.localizable.description()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.placeholder = R.string.localizable.description()
                    $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
                    $0.value = channel.description
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.description = value
                    self.handleValueChange()
                }.cellUpdate(textAreaRowCellUpdate(_:_:))
        }

        let section = Section("")
            <<< ButtonRow(R.string.localizable.channelMenuNotificationEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.channelSettingViewController.toChannelNotificationEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }

        if GuildConnector.shared.current_permission.StreamChannel.channel_members_managable && !channel.is_dm && channel.configable {
            section
                <<< ButtonRow(R.string.localizable.channelMenuRoleEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.channelSettingViewController.toChannelRolesEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                <<< ButtonRow(R.string.localizable.menuMembersEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.channelSettingViewController.toChannelMembersEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                <<< IntRow(R.string.localizable.streamChannelParticipantLimit()) {
                    $0.title = $0.tag
                    $0.value = channel.max_participant_count
                    $0.add(rule: RuleGreaterThan(min: 1))
                    $0.add(rule: RuleSmallerThan(max: PlanConfig.free_plan_max_participant_count))
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    $0.value = min(max(1, value), PlanConfig.free_plan_max_participant_count)
                    channel.max_participant_count = min(max(1, value), PlanConfig.free_plan_max_participant_count)
                    self.handleValueChange()
                }.cellUpdate(intRowCellUpdate(_:_:))
        }

        if GuildConnector.shared.current_permission.Guild.channels_managable && !channel.is_dm && channel.configable {
            section
                <<< SwitchRow(R.string.localizable.channelFreeze()) {
                    $0.title = $0.tag
                    $0.value = !channel.permission
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.permission = !value
                    self.handleValueChange()
                }.cellUpdate(switchRowCellUpdate(_:_:))
        }

        form +++ section
        
        if (GuildConnector.shared.current_permission.Guild.channels_managable || channel.is_dm) && channel.deletable {
            form
                +++ Section("")
                <<< ButtonRow(R.string.localizable.channelMenuDelete()) {
                    $0.title = $0.tag
                    $0.onCellHighlightChanged({ _, _ in })
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: R.string.localizable.channelMenuDelete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)

                    let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.channelMenuDelete(), style: UIAlertAction.Style.default, handler:{
                        [unowned self] (action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        self.presenter?.delete(repository: self.repository)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }

    fileprivate func configureDMChannelForm(_ channel: DMChannelEntity) {
        if channel.configable {
            form
                +++ Section(R.string.localizable.channelMenu())
                <<< TextRow(R.string.localizable.channelName()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.value = channel.name
                    $0.placeholder = $0.tag
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.name = value
                    self.handleValueChange()
                }.cellUpdate(textRowCellUpdate(_:_:))
                <<< TextAreaRow(R.string.localizable.description()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.placeholder = R.string.localizable.description()
                    $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
                    $0.value = channel.description
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    channel.description = value
                    self.handleValueChange()
                }.cellUpdate(textAreaRowCellUpdate(_:_:))
        }
        
        form
            +++ ButtonRow(R.string.localizable.channelMenuNotificationEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.channelSettingViewController.toChannelNotificationEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowCellUpdate($0, $1) }
        
        if channel.deletable {
            form
                +++ Section("")
                <<< ButtonRow(R.string.localizable.channelMenuDelete()) {
                    $0.title = $0.tag
                    $0.onCellHighlightChanged({ _, _ in })
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: R.string.localizable.channelMenuDelete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)

                    let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.channelMenuDelete(), style: UIAlertAction.Style.default, handler:{
                        [unowned self] (action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        self.presenter?.delete(repository: self.repository)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
}

extension ChannelSettingViewController: ChannelSettingPresenterOutput {
    func channelSettingPresenter(didUpdate repository: ChannelableProtocol, in channelSettingPresenter: ChannelSettingPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }

    func channelSettingPresenter(onError error: Error, in channelSettingPresenter: ChannelSettingPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }

    func sync(didFetchRepository repository: ChannelableProtocol) {
        repository.Guild = self.repository.Guild
        self.repository = repository
        self.reloadForm()
    }
}
