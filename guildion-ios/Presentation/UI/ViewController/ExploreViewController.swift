//
//  ExploreViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/07.
//

import UIKit

class ExploreViewController: BaseViewController {
    @IBOutlet weak var searchCompleteViewContainer: UIView! {
        didSet {
            searchCompleteViewContainer.isHidden = true
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var searchBar: SearchBar! {
        didSet {
            searchBar.placeholder = R.string.localizable.searchGuildPlaceholder()
            searchBar.buttonText = R.string.localizable.cancel()
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var categoriesListViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoriesListView: TagListView! {
        didSet {
            categoriesListView.delegate = self
            categoriesListView.tagSelectedBackgroundColor = Theme.themify(key: .main).darker(by: 10)
        }
    }
    @IBOutlet weak var storiesLabel: UILabel! {
        didSet {
            storiesLabel.isHidden = true
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    var presenter: ExplorePresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var categories: CategoryEntities = CategoryEntities()
    var keyword: String?
    var searchCompleteViewController: SearchCompleteViewController = R.storyboard.explore.searchCompleteViewController()!
    
    enum Mode: Int {
        case normal
        case searching
        var searchBarTopConstant: CGFloat {
            switch self {
            case .normal: return 20
            case .searching: return 0
            }
        }
    }
    var mode: Mode = .normal {
        didSet {
            if isViewLoaded {
                self.updateSearchLayout()
            }
        }
    }

    override func viewDidLoad() {
        self.navigationMode = .explore
        super.viewDidLoad()
        title = R.string.localizable.explore()
        ExploreViewControllerBuilder.rebuild(self)
        configureSearchCompleteViewController()
        configureCollectionView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.navigationController?.setNavigationBarHidden(mode == .searching, animated: false)
        updateSearchLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildSearchViewController, segue.identifier == R.segue.exploreViewController.toGuildSearch.identifier, let unwrarpped = keyword {
            GuildSearchViewControllerBuilder.rebuild(vc)
            vc.keyword = unwrarpped
        }
    }
    
    override func onTapNavigationQRScannerButton(_ sender: Any) {
        self.presenter?.toGuildQRCodeReader()
    }
     
    func updateSearchLayout() {
        self.scrollView.isScrollEnabled = self.mode == .normal
        switch mode {
        case .normal: searchCompleteViewContainer.fadeOut(type: .Normal, completed: nil)
        case .searching:
            searchCompleteViewContainer.fadeIn(type: .Normal, completed: nil)
            self.scrollView.setContentOffset(.zero, animated: true)
            _ = self.searchBar.becomeFirstResponder()
        }
        self.navigationController?.setNavigationBarHidden(mode == .searching, animated: true)
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBarTopConstraint.constant = self.mode.searchBarTopConstant
        })
    }
}
extension ExploreViewController: SearchCompleteViewControllerDelegate {
    fileprivate func configureSearchCompleteViewController() {
        searchCompleteViewController.willMove(toParent: self)
        self.searchCompleteViewController.view.frame = self.searchCompleteViewContainer.frame
        self.searchCompleteViewContainer.addSubview(self.searchCompleteViewController.view)
        self.searchCompleteViewContainer.constrainToEdges(self.searchCompleteViewController.view)
        self.searchCompleteViewController.delegate = self
        self.addChild(searchCompleteViewController)
        searchCompleteViewController.didMove(toParent: self)
    }
}

extension ExploreViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
    }
}

extension ExploreViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
}

extension ExploreViewController: UICollectionViewDelegateFlowLayout {
}

extension ExploreViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
    }
}

extension ExploreViewController: TagListViewDelegate {
    func updateCategoriesListView() {
        self.categoriesListView.removeAllTags()
        _ = self.categories.items
            .compactMap { self.categoriesListView.addTag($0.getLocalableName(locale: LocaleConfig.devise_default_locale)) }
        self.categoriesListViewHeightConstraint.constant = self.categoriesListView.intrinsicContentSize.height
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        guard title.count > 0 else { return }
        self.keyword = title
        self.presenter?.toGuildSearch(keyword: title)
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {}
}

extension ExploreViewController: SearchBarDelegate {
    func searchBarDidBeginEditing() {
        self.mode = .searching
    }
    
    func searchBarDidEndEditing() {
        guard keyword == nil || keyword?.count == 0 else { return }
        self.mode = .normal
    }
    
    func searchBar(_ searchBar: SearchBar, searched keyword: String?) {
        self.mode = .searching
        self.keyword = keyword
        guard let unwrapped = keyword, unwrapped.count > 0 else { return }
        self.presenter?.toGuildSearch(keyword: unwrapped)
    }
    
    func searchBar(_ searchBar: SearchBar, accessoryButtonTapped keyword: String?) -> Bool {
        self.keyword = nil
        searchBar.keyword = nil
        return true
    }
}

extension ExploreViewController: ExplorePresenterOutput {
    func sync(didFetchRepositories repositories: CategoryEntities) {
        self.categories = repositories
        self.updateCategoriesListView()
    }
}

