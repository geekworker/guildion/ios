//
//  GuildMemberRequestsViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Nuke
import SwiftMessages
import PKHUD

class GuildMemberRequestsViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var presenter: GuildMemberRequestsPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: MemberRequestEntities = MemberRequestEntities()
    var current_guild: GuildEntity = GuildEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildMemberRequestsViewControllerBuilder.rebuild(self)
        configureTableView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        presenter?.fetch()
        title = R.string.localizable.guildMemberRequests()
        self.view.backgroundColor = Theme.themify(key: .background)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension GuildMemberRequestsViewController: UITableViewDelegate {
    fileprivate func configureTableView() {
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.register([
            UINib(resource: R.nib.memberRequestTableViewCell): R.nib.memberRequestTableViewCell.name,
        ])
    }
}

extension GuildMemberRequestsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.memberRequestTableViewCell.name, for: indexPath) as? MemberRequestTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repositories.items[indexPath.row])
        cell.delegate = self
        return cell
    }
    
}

extension GuildMemberRequestsViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
    }
}

extension GuildMemberRequestsViewController: MemberRequestTableViewCellDelegate {
    func memberRequestTableViewCell(onTap memberRequestTableViewCell: MemberRequestTableViewCell) {
    }
    
    func memberRequestTableViewCell(toProfile memberRequestTableViewCell: MemberRequestTableViewCell) {
        guard let user = memberRequestTableViewCell.repository.Voter else { return }
        self.presenter?.toUserMenu(user)
    }
    
    func memberRequestTableViewCell(willDeny repository: MemberRequestEntity, memberRequestTableViewCell: MemberRequestTableViewCell) {
        self.presenter?.deny(repository: repository)
    }
    
    func memberRequestTableViewCell(willAccept repository: MemberRequestEntity, memberRequestTableViewCell: MemberRequestTableViewCell) {
        self.presenter?.accept(repository: repository)
    }
}

extension GuildMemberRequestsViewController: GuildMemberRequestsPresenterOutput {
    func guildMemberRequestsPresenter(onError error: Error) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func sync(didFetchRepositories repositories: MemberRequestEntities) {
        DispatchQueue.main.async {
            self.repositories = repositories
            self.tableView.reloadData()
        }
    }
    
    func guildMemberRequestsPresenter(didDeny repository: MemberRequestEntity) {
        DispatchQueue.main.async {
            self.repositories.items = self.repositories.items.filter({ $0.id != repository.id })
            self.tableView.reloadData()
        }
    }
    
    func guildMemberRequestsPresenter(didAccept repository: MemberRequestEntity) {
        DispatchQueue.main.async {
            self.repositories.items = self.repositories.items.filter({ $0.id != repository.id })
            self.tableView.reloadData()
            HUD.hide()
            HUD.flash(.success)
        }
    }
}
