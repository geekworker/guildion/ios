//
//  AccountEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class AccountEditViewController: BaseFormViewController {
    var presenter: AccountEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: UserEntity = UserEntity() {
        didSet {
            new_repository.nickname = repository.nickname
            new_repository.description = repository.description
            new_repository.picture_small = repository.picture_small
            new_repository.picture_large = repository.picture_large
            new_repository.id = repository.id
            new_repository.username = repository.username
            new_repository.Identity = repository.Identity
        }
    }
    var new_repository: UserEntity = UserEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        AccountEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func handleValueChange() {
        form.validate()
        if repository.Identity?.password == "" && repository.Identity?.email == new_repository.Identity?.email {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        //self.presenter?.update(repository: new_repository)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section()
            <<< EmailRow() {
                $0.title = R.string.localizable.email()
                $0.value = new_repository.Identity?.email ?? ""
            }.cellUpdate(emailRowCellUpdate(_:_:))
            <<< PasswordRow() {
                $0.title = R.string.localizable.current_password()
                var ruleSet = RuleSet<String>()
                ruleSet.add(rule: RuleRequired())
            }.cellUpdate(passwordRowCellUpdate(_:_:))
            <<< PasswordRow() {
                $0.title = R.string.localizable.new_password()
            }.cellUpdate(passwordRowCellUpdate(_:_:))
            +++ Section("")
            <<< ButtonRow(R.string.localizable.userMenuDeleteAccount()) {
                $0.title = $0.tag
                $0.onCellHighlightChanged({ _, _ in })
            }.onCellSelection { [unowned self] cell, row in
                let alert: UIAlertController = UIAlertController(title: R.string.localizable.userMenuDeleteAccount(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                            
                let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.userMenuDeleteAccount(), style: UIAlertAction.Style.default, handler:{ [unowned self] (action: UIAlertAction!) -> Void in
                    HUD.show(.progress)
                    self.presenter?.delete()
                })
                let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{ (action: UIAlertAction!) -> Void in
                })
                alert.addAction(cancelAction)
                alert.addAction(defaultAction)
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                self.present(alert, animated: true, completion: nil)
            }.cellUpdate { [unowned self] in
                self.buttonRowCellUpdate($0, $1)
                $0.textLabel?.textColor = Theme.themify(key: .danger)
                $0.textLabel?.font = Font.FT(size: .FM, family: .B)
            }
    }
}

extension AccountEditViewController: AccountEditPresenterOutput {
    func accountEditPresenter(onError error: Error, in accountEditPresenter: AccountEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func sync(didFetchRepository repository: UserEntity) {
        self.repository = repository
    }
    
    func accountEditPresenter(didDeleted accountEditPresenter: AccountEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        BaseWireframe.toWelcome()
    }
}
