//
//  FileNewViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import SPPermissions
import AssetsPickerViewController
import Photos
import PKHUD

protocol FileNewViewControllerDelegate: class {
    func fileNewViewController(willCreate repositories: FileEntities, in fileNewViewController: FileNewViewController)
    func fileNewViewController(didCreate repositories: FileEntities)
}

class FileNewViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = R.string.localizable.fileNewTitle()
        }
    }
    @IBOutlet weak var uploadView: UIView! {
        didSet {
            uploadView.backgroundColor = Theme.themify(key: .backgroundLight)
            uploadView.layer.cornerRadius = 5
            uploadView.clipsToBounds = true
            uploadView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectUpload(_:))))
        }
    }
    @IBOutlet weak var uploadTitleLabel: UILabel! {
        didSet {
            uploadTitleLabel.textColor = Theme.themify(key: .string)
            uploadTitleLabel.text = R.string.localizable.uploadTitle()
        }
    }
    @IBOutlet weak var uploadDetailLabel: UILabel! {
        didSet {
            uploadDetailLabel.textColor = Theme.themify(key: .string)
            uploadDetailLabel.text = R.string.localizable.uploadDesc()
            uploadDetailLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var uploadImage: UIImageView! {
        didSet {
            uploadImage.layer.cornerRadius = uploadImage.frame.width / 2
            uploadImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var uploadChevron: UIImageView! {
        didSet {
            uploadChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            uploadChevron.tintColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var uploadFromURLView: UIView! {
        didSet {
            uploadFromURLView.backgroundColor = Theme.themify(key: .backgroundLight)
            uploadFromURLView.layer.cornerRadius = 5
            uploadFromURLView.clipsToBounds = true
            uploadFromURLView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectUploadFromURL(_:))))
        }
    }
    @IBOutlet weak var uploadFromURLTitleLabel: UILabel! {
        didSet {
            uploadFromURLTitleLabel.textColor = Theme.themify(key: .string)
            uploadFromURLTitleLabel.text = R.string.localizable.uploadTitleFromURL()
        }
    }
    @IBOutlet weak var uploadFromURLDetailLabel: UILabel! {
        didSet {
            uploadFromURLDetailLabel.textColor = Theme.themify(key: .string)
            uploadFromURLDetailLabel.text = R.string.localizable.uploadDescFromURL()
            uploadFromURLDetailLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var uploadFromURLLinkImage: UIImageView! {
        didSet {
            uploadFromURLLinkImage.image = R.image.link()!.withRenderingMode(.alwaysTemplate)
            uploadFromURLLinkImage.tintColor = Theme.themify(key: .backgroundContrast)
        }
    }
    @IBOutlet weak var uploadFromURLImage: UIImageView! {
        didSet {
        }
    }
    @IBOutlet weak var uploadFromURLChevron: UIImageView! {
        didSet {
            uploadFromURLChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            uploadFromURLChevron.tintColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var uploadFromBrowserView: UIView! {
        didSet {
            uploadFromBrowserView.backgroundColor = Theme.themify(key: .backgroundLight)
            uploadFromBrowserView.layer.cornerRadius = 5
            uploadFromBrowserView.clipsToBounds = true
            uploadFromBrowserView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectUploadFromBrowser(_:))))
        }
    }
    @IBOutlet weak var uploadFromBrowserTitleLabel: UILabel! {
        didSet {
            uploadFromBrowserTitleLabel.textColor = Theme.themify(key: .string)
            uploadFromBrowserTitleLabel.text = R.string.localizable.uploadTitleFromBrowser()
        }
    }
    @IBOutlet weak var uploadFromBrowserDetailLabel: UILabel! {
        didSet {
            uploadFromBrowserDetailLabel.textColor = Theme.themify(key: .string)
            uploadFromBrowserDetailLabel.text = R.string.localizable.uploadDescFromBrowser()
            uploadFromBrowserDetailLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var uploadFromBrowserSearchImage: UIImageView! {
        didSet {
            uploadFromBrowserSearchImage.image = R.image.search()!.withRenderingMode(.alwaysTemplate)
            uploadFromBrowserSearchImage.tintColor = Theme.themify(key: .backgroundContrast)
        }
    }
    @IBOutlet weak var uploadFromBrowserImage: UIImageView! {
        didSet {
        }
    }
    @IBOutlet weak var uploadFromBrowserChevron: UIImageView! {
        didSet {
            uploadFromBrowserChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            uploadFromBrowserChevron.tintColor = Theme.themify(key: .border)
        }
    }
    var presenter: FileNewPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public var didCreateCallback: ((_ repositories: FileEntities) -> ())? = nil
    var repositories: FileEntities = FileEntities()
    var current_guild: GuildEntity = GuildEntity()
    var current_member: MemberEntity = MemberEntity()
    weak var delegate: FileNewViewControllerDelegate?
    public lazy var assetsPickerHandler: AssetsPickerHandler = {
        let handler = AssetsPickerHandler()
        handler.delegate = self
        return handler
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        FileNewViewControllerBuilder.rebuild(self)
        navigationMode = .modal
        self.setFlatBackground()
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FileNewFromBrowserViewController {
            vc.initRepository(current_guild: current_guild, current_member: current_member)
        } else if let vc = segue.destination as? FileNewFromURLViewController {
            vc.initRepository(current_guild: current_guild, current_member: current_member)
        }
    }
    
    func initRepository(current_guild: GuildEntity, current_member: MemberEntity) {
        self.current_guild = current_guild
        self.current_member = current_member
    }
    
    @objc func onSelectUpload(_ sender: Any) {
        guard self.configurePhotoPermission() else { return }
        self.assetsPickerHandler.openImagePicker()
    }
    
    @objc func onSelectUploadFromURL(_ sender: Any) {
        self.presenter?.toFileNewFromURL()
    }
    
    @objc func onSelectUploadFromBrowser(_ sender: Any) {
        self.presenter?.toWebKit()
    }
}

extension FileNewViewController: SPPermissionsDelegate, SPPermissionsDataSource {
    func configurePhotoPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self)
        return false
    }
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        switch permission {
        case .camera:
            cell.permissionTitleLabel.text = R.string.localizable.permissionCamera()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionCameraText()
        case .microphone:
            cell.permissionTitleLabel.text = R.string.localizable.permissionVoice()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionVoiceText()
        case .photoLibrary:
            cell.permissionTitleLabel.text = R.string.localizable.permissionPhoto()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionPhotoText()
        default: break
        }
        return cell
    }
    
    func didHide(permissions ids: [Int]) {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard permissions.isEmpty else { return }
        self.assetsPickerHandler.openImagePicker()
    }
}

extension FileNewViewController: AssetsPickerHandlerDelegate {
    var viewController: UIViewController {
        get {
            self
        }
    }
    
    func updateAssets(_ assets: FileEntities) {
        self.repositories.items += assets.items
        if repositories.storage_count > 0, !repositories.available_storage {
            BaseWireframe.toPlansWithAlert()
            return
        }
        self.presenter?.creates(repositories, viewController: self)
    }
    
    func cancelAssets() {
    }
}

extension FileNewViewController: WebKitViewControllerDelegate {
    func webKitViewController(didSelects repositories: FileEntities, in webKitViewController: WebKitViewController) {
        self.presenter?.creates(repositories, viewController: self)
    }
    
    func webKitViewController(didSelect url: URL, in webKitViewController: WebKitViewController) {
    }
    
    func webKitViewController(shouldSkip duration: Float, in webKitViewController: WebKitViewController) {
    }
}

extension FileNewViewController: FileNewPresenterOutput {
    func fileNewPresenter(onError error: Error) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func syncCurrentMember(didFetchRepository repository: MemberEntity) {
        DispatchQueue.main.async {
            self.current_member = repository
        }
    }
    
    func sync(didFetchRepositories repositories: FileEntities) {
        DispatchQueue.main.async {
            self.repositories = repositories
        }
    }
    
    func fileNewPresenter(shouldCreate repositories: FileEntities) {
        DispatchQueue.main.async {
            self.delegate?.fileNewViewController(willCreate: repositories, in: self)
            self.dismissShouldAppear(animated: true)
        }
    }
    
    func fileNewPresenter(didCreated repositories: FileEntities) {
        DispatchQueue.main.async {
            self.repositories = repositories
            self.delegate?.fileNewViewController(didCreate: repositories)
        }
    }
}
