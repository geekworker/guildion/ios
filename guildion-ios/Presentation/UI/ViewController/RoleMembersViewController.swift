//
//  RoleMembersViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit
import Eureka
import Nuke
import PKHUD

class RoleMembersViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var presenter: RoleMembersPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: MemberEntities = MemberEntities()
    var repository: RoleEntity = RoleEntity()
    
    private var loadingDecrementStatus: Int?
    private var loadingIncrementStatus: Int?
    private var lastWillDisplay: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        RoleMembersViewControllerBuilder.rebuild(self)
        self.title = repository.name
        configureCollectionView()
        presenter?.fetch(repository: repository)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension RoleMembersViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = MemberRowCollectionViewCell.getSize(collectionView.frame)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionView.collectionViewLayout = layout
        self.collectionView.register([
            UINib(resource: R.nib.memberCollectionViewCell): R.nib.memberCollectionViewCell.name,
            UINib(resource: R.nib.memberRowCollectionViewCell): R.nib.memberRowCollectionViewCell.name,
        ])
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.prefetchDataSource = self
        self.collectionView.backgroundColor = Theme.themify(key: .background)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if repositories.count > indexPath.row, let repository = repositories[safe: indexPath.row], let last_id = repositories.last?.id, repository.id == last_id, last_id != loadingDecrementStatus {
            loadingDecrementStatus = last_id
            self.presenter?.decrement(repository: self.repository, first_id: last_id)
        }
        lastWillDisplay = indexPath.row
    }
}

extension RoleMembersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.memberRowCollectionViewCell.name, for: indexPath) as? MemberRowCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository, role: self.repository)
        cell.delegate = self
        return cell
    }
}

extension RoleMembersViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row], let url = URL(string: repository.picture_small) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension RoleMembersViewController: MemberCollectionViewCellDelegate {
    func memberCollectionViewCell(onTap repository: MemberEntity, in memberCollectionViewCell: MemberCollectionViewCell) {
        self.presenter?.toMemberMenu(repository: repository)
    }
}

extension RoleMembersViewController: MemberRowCollectionViewCellDelegate {
    func memberRowCollectionViewCell(onTap repository: MemberEntity, in memberRowCollectionViewCell: MemberRowCollectionViewCell) {
        self.presenter?.toMemberMenu(repository: repository)
    }
}

extension RoleMembersViewController: RoleMembersPresenterOutput {
    func sync(didFetchRepositories repositories: MemberEntities) {
        self.repositories = repositories
        self.collectionView.reloadData()
    }
    
    func roleMembersPresenter(onError error: Error, in roleMembersPresenter: RoleMembersPresenter) {
    }
    
    func sync(didDecrementRepositories repositories: MemberEntities) {
        if repositories.count != 0, repositories.last?.id != loadingDecrementStatus {
            loadingDecrementStatus = nil
            DispatchQueue.main.async {
                self.repositories = repositories
                self.collectionView.reloadData()
            }
        }
    }
}
