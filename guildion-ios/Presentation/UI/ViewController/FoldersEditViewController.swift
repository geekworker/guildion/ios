//
//  FoldersEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/31.
//

import Foundation
import UIKit
import Eureka

class FoldersEditViewController: BaseFormViewController {
    var presenter: FoldersEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var current_guild: GuildEntity = GuildEntity()
    var repositories: FolderEntities = FolderEntities()
    fileprivate var selectedFolder: FolderEntity?
    var is_playlist: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        FoldersEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.setEditing(true, animated: false)
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.presenter?.fetch()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FolderEditViewController, segue.identifier == R.segue.foldersEditViewController.toFolderEdit.identifier, let repository = selectedFolder {
            vc.repository = repository
            selectedFolder = nil
        }
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        let section = MultivaluedSection(multivaluedOptions: .Reorder, header: is_playlist ? R.string.localizable.playlists() : R.string.localizable.folders())
        section.tag = "section"
        
        if form.sectionBy(tag: "section") == nil {
            form +++ section
        }
        
        for repository in repositories.items {
            form.last! <<< ButtonRow(repository.uid){ lrow in
                lrow.title = repository.name
                lrow.selectableValue = repository.uid
            }
            .cellUpdate { [unowned self] in
                if let thumbnail = repository.FileReferences?.last?.File?.thumbnail, let url = URL(string: thumbnail) {
                    self.buttonImageRowCellUpdate($0, $1, url: url)
                } else {
                    self.buttonRowLeftAlignmentUpdate($0, $1)
                }
            }
            .onCellSelection { [unowned self] (cell, row) in
                self.selectedFolder = self.repositories.filter({ $0.uid == row.tag }).first
                self.presenter?.toFolderEdit()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard let source_repository = self.repositories[safe: sourceIndexPath.row], sourceIndexPath.row != destinationIndexPath.row else { return }
        repositories.remove(source_repository)
        repositories.insert(source_repository, at: destinationIndexPath.row)
        let entities = FolderEntities()
        entities.items = repositories
            .enumerated()
            .compactMap({
                $0.element.index = $0.offset + 1
                return $0.element
            })
            .filter({
                $0.index_updated
            })
        self.presenter?.updateIndexes(repositories: entities)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.setReorderControl(cell)
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension FoldersEditViewController: FoldersEditPresenterOutput {
    func sync(didFetchRepositories repositories: FolderEntities) {
        let shouldConfigureForm = self.repositories.count == 0 && repositories.count > 0
        self.repositories = repositories
        if shouldConfigureForm {
            self.configureForm()
        } else {
            self.reloadForm()
        }
    }
}
