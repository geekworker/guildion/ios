//
//  MessageMenuViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import PanModal
import SwiftMessages
import PKHUD

class MessageMenuViewController: BaseViewController {
    @IBOutlet weak var collectionView: MenuCollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    public var viewControllerParent: ChannelMessagesViewController?
    var menus: [MenuCollectionViewCell.MenuType] = [
        .reactions, .copy, .editMessage, .deleteMessage, .mention
    ]
    var presenter: MessageMenuPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var permission: PermissionModel = PermissionModel()
    var current_guild: GuildEntity = GuildEntity()
    var current_channel: MessageableEntity = MessageableEntity()
    var current_member: MemberEntity = MemberEntity()
    var repository: MessageEntity = MessageEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        MessageMenuViewControllerBuilder.rebuild(self)
        evalMenus()
        configureCollectionView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        view.backgroundColor = Theme.themify(key: .backgroundLight)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func initRepository(_ repository: MessageEntity, current_guild: GuildEntity, current_channel: MessageableEntity, current_member: MemberEntity, permission: PermissionModel) {
        self.repository = repository
        self.current_guild = current_guild
        self.current_channel = current_channel
        self.current_member = current_member
        self.permission = permission
    }
    
    func evalMenus() {
        guard let id = repository.id, id != 0 else { self.menus = []; return }
        let isMyAccount = repository.SenderId == current_member.id
        
        if repository.is_log {
            if isMyAccount || permission.TextChannel.messages_managable {
                self.menus = [
                    .deleteMessage
                ]
            } else if !permission.TextChannel.message_reactionable {
                self.menus = [
                    .deleteMessage
                ]
            } else {
                self.menus = [
                    .deleteMessage
                ]
            }
            return
        }
        
        switch repository.messageable_type {
        case .DMChannel:
            if isMyAccount || permission.DMChannel.messages_managable {
                self.menus = permission.DMChannel.message_reactionable ? [
                    .reactions, .copy, .editMessage, .deleteMessage, .mention
                    ] : [
                        .copy, .editMessage, .deleteMessage, .mention
                    ]
            } else if !permission.DMChannel.message_reactionable {
                self.menus = [
                    .copy, .mention
                ]
            } else {
                self.menus = [
                    .reactions, .copy, .mention
                ]
            }
        case .TextChannel:
            if isMyAccount || permission.TextChannel.messages_managable {
                self.menus = permission.TextChannel.message_reactionable ? [
                    .reactions, .copy, .editMessage, .deleteMessage, .mention
                    ] : [
                        .copy, .editMessage, .deleteMessage, .mention
                    ]
            } else if !permission.TextChannel.message_reactionable {
                self.menus = [
                    .copy, .mention
                ]
            } else {
                self.menus = [
                    .reactions, .copy, .mention
                ]
            }
        case .StreamChannel:
            if isMyAccount || permission.StreamChannel.messages_managable {
                self.menus = permission.StreamChannel.message_reactionable ? [
                    .reactions, .copy, .editMessage, .deleteMessage, .mention
                    ] : [
                        .copy, .editMessage, .deleteMessage, .mention
                    ]
            } else if !permission.StreamChannel.message_reactionable {
                self.menus = [
                    .copy, .mention
                ]
            } else {
                self.menus = [
                    .reactions, .copy, .mention
                ]
            }
        }
    }
}

extension MessageMenuViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        self.collectionView.configure()
        self.collectionView.setRepositories(menus)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionViewHeightConstraint.constant = self.collectionView.calcHeight()
    }
}

extension MessageMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.collectionView.collectionView(cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.collectionView.collectionView(didSelectItemAt: indexPath) {
        case .editMessage:
            self.viewControllerParent?.newRepository = repository
            self.dismissShouldAppear(animated: true)
        case .deleteMessage: self.presenter?.delete(repository)
        case .copy:
            self.presenter?.copy(repository)
            self.dismissShouldAppear(animated: true, completion: {
                let messageAlert = MessageView.viewFromNib(layout: .cardView)
                messageAlert.configureTheme(.success)
                messageAlert.configureDropShadow()
                messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successCopy())
                messageAlert.button?.isHidden = true
                SwiftMessages.show(config: SwiftMessages.defaultConfig, view: messageAlert)
            })
        case .mention:
            guard let sender = repository.Sender else { return }
            self.viewControllerParent?.newRepository.text = "@\(sender.nickname == "" ? sender.User?.nickname ?? "" : sender.nickname) " + (self.viewControllerParent?.newRepository.text ?? "")
            self.dismissShouldAppear(animated: true)
        case .reactions:
            self.dismissShouldAppear(animated: true, completion: {
                self.viewControllerParent?.showReactionPicker(at: self.repository)
            })
        default: break
        }
    }
}

extension MessageMenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MenuCollectionViewCell.cellSize
    }
}

extension MessageMenuViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return collectionView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(self.collectionView.calcHeight())
    }
    
//    var scrollIndicatorInsets: UIEdgeInsets {
//        let bottomOffset = presentingViewController?.bottomLayoutGuide.length ?? 0
//        return UIEdgeInsets(top: headerView.frame.size.height, left: 0, bottom: bottomOffset, right: 0)
//    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .shortForm = state
            else { return }

        self.panModalTransition(to: .longForm)
        panModalSetNeedsLayoutUpdate()
    }
}

extension MessageMenuViewController: MessageMenuPresenterOutput {
    func messageMenuPresenter(onError error: Error, messageMenuPresenter: MessageMenuPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func sync(didFetchRepository repository: MessageEntity) {
        DispatchQueue.main.async {
            self.repository = repository
            self.evalMenus()
            self.collectionView.reloadData()
        }
    }
    
    func messageMenuPresenter(didDelete repository: MessageEntity, messageMenuPresenter: MessageMenuPresenter) {
        DispatchQueue.main.async {
            self.dismissShouldAppear(animated: true)
        }
    }
    
    func messageMenuPresenter(didUpdate repository: MessageEntity, messageMenuPresenter: MessageMenuPresenter) {
        DispatchQueue.main.async {
            self.dismissShouldAppear(animated: true)
        }
    }
}
