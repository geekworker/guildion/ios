//
//  GuildNewViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/12.
//

import Foundation
import UIKit
import TextFieldEffects
import SPPermissions
import Nuke

class GuildNewViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Theme.themify(key: .string)
            titleLabel.text = R.string.localizable.guildNewTitle()
        }
    }
    @IBOutlet weak var backgroundImage: UIImageView!  {
           didSet {
            self.backgroundImage.layer.borderColor = Theme.themify(key: .borderContrast).cgColor
               self.backgroundImage.layer.borderWidth = 0.5
           }
       }
    @IBOutlet weak var backgroundCameraImage: UIImageView! {
        didSet {
            backgroundCameraImage.image = R.image.camera()?.withRenderingMode(.alwaysTemplate)
            backgroundCameraImage.layer.shadowColor = Theme.themify(key: .background).cgColor
            backgroundCameraImage.tintColor = .white
        }
    }
    @IBOutlet weak var backgroundDeleteImage: UIImageView! {
        didSet {
            backgroundDeleteImage.image = R.image.close()?.withRenderingMode(.alwaysTemplate)
            backgroundDeleteImage.layer.shadowColor = Theme.themify(key: .background).cgColor
            backgroundDeleteImage.tintColor = .white
        }
    }
    @IBOutlet weak var nameTextField: HoshiTextField! {
        didSet {
            self.nameTextField.font = Font.FT(size: .FL, family: .L)
            self.nameTextField.textColor = Theme.themify(key: .string)
            self.nameTextField.placeholderColor = Theme.themify(key: .placeholder)
            self.nameTextField.placeholder = R.string.localizable.guildName()
            self.nameTextField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.nameTextField.borderActiveColor = Theme.themify(key: .main)
            self.nameTextField.delegate = self
        }
    }
    public var imagePickerHandler = ImagePickerHandler()
    
    var presenter: GuildNewPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public var repository: GuildEntity = GuildEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GuildNewViewControllerBuilder.rebuild(self)
        self.repository.picture_small = DataConfig.Image.default_guild_image_url
        navigationMode = .buttonModal(title: R.string.localizable.next())
        self.setFlatBackground()
        imagePickerHandler.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard(_:)))
        gesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        nameTextField.text == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
        self.updateLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildPublicSettingViewController, segue.identifier == R.segue.guildNewViewController.toPublicSetting.identifier {
            vc.repository = self.repository
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.nameTextField.resignFirstResponder()
    }
    
    override func onTapNavigationDoneButton(_ sender: Any) {
        self.presenter?.submit(repository)
    }
    
    @objc func closeKeyboard(_ sender: Any) {
        self.nameTextField.resignFirstResponder()
    }
    
    @IBAction func backgroundCameraTap(_ sender: Any) {
        guard configurePhotoPermission() else { return }
        self.imagePickerHandler.openImagePicker()
    }
    
    @IBAction func backgroundCloseTap(_ sender: Any) {
        self.repository.picture_small = DataConfig.Image.default_guild_image_url
        if let url = URL(string: self.repository.picture_small) {
            Nuke.loadImage(with: url, into: backgroundImage)
        }
    }
    
    func updateLayout() {
        self.nameTextField.text = repository.name
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: backgroundImage)
        }
    }
}

extension GuildNewViewController: SPPermissionsDelegate, SPPermissionsDataSource {
    func configurePhotoPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary].filter { !$0.isAuthorized }
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self)
        return false
    }
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        switch permission {
        case .camera:
            cell.permissionTitleLabel.text = R.string.localizable.permissionCamera()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionCameraText()
        case .microphone:
            cell.permissionTitleLabel.text = R.string.localizable.permissionVoice()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionVoiceText()
        case .photoLibrary:
            cell.permissionTitleLabel.text = R.string.localizable.permissionPhoto()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionPhotoText()
        default: break
        }
        return cell
    }
    
    func didHide(permissions ids: [Int]) {
        let permissions: [SPPermission] = [.photoLibrary].filter { !$0.isAuthorized }
        guard permissions.isEmpty else { return }
        self.imagePickerHandler.openImagePicker()
    }
}

extension GuildNewViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        self.repository.name = textField.text ?? ""
        self.repository.name == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.repository.name = textField.text ?? ""
        self.repository.name == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
        textField.resignFirstResponder()
        return true
    }
}

extension GuildNewViewController: ImagePickerHandlerDelegate {
    var viewController: UIViewController {
        get {
            self
        }
    }
    
    func updateAssets(_ image: UIImage, info: [UIImagePickerController.InfoKey : Any]) {
        self.backgroundImage.image = image
        self.repository.picture_small = "\(imagePickerHandler.generateImageURL(image))"
    }
    
    func cancelAssets() {
    }
}

extension GuildNewViewController: GuildNewPresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
    }
}

