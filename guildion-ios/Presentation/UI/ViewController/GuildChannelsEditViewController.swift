//
//  GuildChannelsEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka

class GuildChannelsEditViewController: BaseFormViewController {
    var presenter: GuildChannelsEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: SectionChannelEntities = SectionChannelEntities()
    var repository: GuildEntity = GuildEntity()
    var selectedChannel: ChannelableProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildChannelsEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.setEditing(true, animated: false)
        presenter?.bind()
        self.presenter?.fetch(repository: repository)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ChannelSettingViewController, segue.identifier == R.segue.guildChannelsEditViewController.toChannelSetting.identifier, let repository = selectedChannel {
            vc.repository = repository
            vc.repository.Guild = self.repository
            selectedChannel = nil
        } else if let vc = segue.destination as? GuildSectionChannelsEditViewController, segue.identifier == R.segue.guildChannelsEditViewController.toSectionChannelsEdit.identifier {
            vc.repository = repository
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        for sectionChannel in self.repositories.items {
            self.configureSectionChannelForm(sectionChannel)
        }
        
        guard self.repositories.count > 0 else { return }
        
        form
            +++ Section("")
            <<< ButtonRow(R.string.localizable.sectionChannelsEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.guildChannelsEditViewController.toSectionChannelsEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
    }
    
    fileprivate func configureSectionChannelForm(_ sectionChannel: SectionChannelEntity) {
        guard !sectionChannel.is_dm else { return }
        
        let section = MultivaluedSection(multivaluedOptions: .Reorder, header: sectionChannel.name)
        section.tag = sectionChannel.uid
        
        if form.sectionBy(tag: sectionChannel.uid) == nil {
            form +++ section
        }
        
        for row in sectionChannel.rows {
            switch row.sectionChannelRowType {
            case .StreamChannel:
                guard let repository = row as? StreamChannelEntity, !repository.temporary else { break }
                form.last! <<< ButtonRow(repository.uid) { lrow in
                    lrow.title = repository.name
                    lrow.selectableValue = repository.uid
                }
                .cellUpdate { [unowned self] in
                    self.buttonIconRowCellUpdate($0, $1, icon: repository.channelable_type.image)
                }
                .onCellSelection { [unowned self] (cell, row) in
                    self.selectedChannel = repository
                    self.presenter?.toChannelSetting()
                }
            case .TextChannel:
                guard let repository = row as? TextChannelEntity else { break }
                form.last! <<< ButtonRow(repository.uid) { lrow in
                    lrow.title = repository.name
                    lrow.selectableValue = repository.uid
                }
                .cellUpdate { [unowned self] in
                    self.buttonIconRowCellUpdate($0, $1, icon: repository.channelable_type.image)
                }
                .onCellSelection { [unowned self] (cell, row) in
                    self.selectedChannel = repository
                    self.presenter?.toChannelSetting()
                }
            case .DMChannel: break
            case .Participant: break
            case .Stream: break
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        sourceIndexPath.section == destinationIndexPath.section ? self.tableView(self.tableView, sameSectionMoveRowAt: sourceIndexPath, to: destinationIndexPath) : self.tableView(self.tableView, differenceSectionMoveRowAt: sourceIndexPath, to: destinationIndexPath)
    }
    
    fileprivate func tableView(_ tableView: UITableView, differenceSectionMoveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard sourceIndexPath.section != destinationIndexPath.section else { return }
        guard let source_section = self.repositories[safe: sourceIndexPath.section], let source_repository = source_section.rows[safe: sourceIndexPath.row], let destination_section = self.repositories[safe: destinationIndexPath.section] else { return }
        
        source_section.rows.remove(at: sourceIndexPath.row)
        destination_section.rows.insert(source_repository, at: destinationIndexPath.row)
        
        let st_entities = TextChannelEntities()
        let ss_entities = StreamChannelEntities()
        st_entities.items = source_section
            .rows
            .enumerated()
            .compactMap({
                guard let repository = $0.element as? TextChannelEntity else { return nil }
                repository.index = $0.offset + 1
                repository.SectionId = source_section.id
                return repository
            })
        ss_entities.items = source_section
            .rows
            .enumerated()
            .compactMap({
                guard let repository = $0.element as? StreamChannelEntity else { return nil }
                repository.index = $0.offset + 1
                repository.SectionId = source_section.id
                return repository
            })
        
        let dt_entities = TextChannelEntities()
        let ds_entities = StreamChannelEntities()
        dt_entities.items = destination_section
            .rows
            .enumerated()
            .compactMap({
                guard let repository = $0.element as? TextChannelEntity else { return nil }
                repository.index = $0.offset + 1
                repository.SectionId = destination_section.id
                return repository
            })
        ds_entities.items = destination_section
            .rows
            .enumerated()
            .compactMap({
                guard let repository = $0.element as? StreamChannelEntity else { return nil }
                repository.index = $0.offset + 1
                repository.SectionId = destination_section.id
                return repository
            })
        
        self.presenter?.updateIndexes(repositories: st_entities)
        self.presenter?.updateIndexes(repositories: ss_entities)
        self.presenter?.updateIndexes(repositories: dt_entities)
        self.presenter?.updateIndexes(repositories: ds_entities)
    }
    
    fileprivate func tableView(_ tableView: UITableView, sameSectionMoveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard sourceIndexPath.section == destinationIndexPath.section else { return }
        guard let source_section = self.repositories[safe: sourceIndexPath.section], let source_repository = source_section.rows[safe: sourceIndexPath.row], sourceIndexPath.row != destinationIndexPath.row else { return }
        source_section.rows.remove(at: sourceIndexPath.row)
        source_section.rows.insert(source_repository, at: destinationIndexPath.row)
        let t_entities = TextChannelEntities()
        let s_entities = StreamChannelEntities()
        t_entities.items = source_section
            .rows
            .enumerated()
            .compactMap({
                $0.element.index = $0.offset + 1
                return $0.element as? TextChannelEntity
            })
        s_entities.items = source_section
            .rows
            .enumerated()
            .compactMap({
                $0.element.index = $0.offset + 1
                return $0.element as? StreamChannelEntity
            })
        self.presenter?.updateIndexes(repositories: t_entities)
        self.presenter?.updateIndexes(repositories: s_entities)
    }
    
    override func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        guard proposedDestinationIndexPath.section != repositories.count else { return sourceIndexPath }

        let sourceSection = sourceIndexPath.section
        let destSection = proposedDestinationIndexPath.section
        
//        if destSection > sourceSection {
//            return IndexPath(row: 0, section: destSection)
//        } else if destSection < sourceSection {
//            return IndexPath(row: max(0, self.tableView(tableView, numberOfRowsInSection: destSection) - 1), section: destSection)
//        }

        return destSection == sourceSection ? proposedDestinationIndexPath : IndexPath(row: 0, section: destSection)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.setReorderControl(cell)
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension GuildChannelsEditViewController: GuildChannelsEditPresenterOutput {
    func sync(didFetchRepositories repositories: SectionChannelEntities) {
        let shouldUpdate = self.repositories.count == 0 && repositories.count > 0
        self.repositories.items = repositories.filter({ !$0.is_dm })
        if shouldUpdate {
            configureForm()
        } else {
            reloadForm()
        }
    }
}
