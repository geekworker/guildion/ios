//
//  GuildBlocksViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka

class GuildBlocksViewController: BaseFormViewController {
    var presenter: GuildBlocksPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: UserEntity = UserEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildBlocksViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension GuildBlocksViewController: GuildBlocksPresenterOutput {
    func sync(didFetchRepository repository: UserEntity) {
        self.repository = repository
    }
}
