//
//  GuildInputDescriptionViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import UITextView_Placeholder
import PKHUD

class GuildInputDescriptionViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = R.string.localizable.guildInfoTitle()
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Theme.themify(key: .string)
            descriptionLabel.text = R.string.localizable.guildDescription()
        }
    }
    @IBOutlet weak var textView: PlaceHolderTextView! {
        didSet {
            textView.text = ""
            textView.textColor = Theme.themify(key: .string)
            textView.backgroundColor = Theme.themify(key: .background)
            textView.placeholderColor = Theme.themify(key: .stringSecondary)
            textView.placeholder = R.string.localizable.guildDescription()
            textView.font = Font.FT(size: .FM, family: .L)
            textView.layer.borderWidth = 2
            textView.layer.borderColor = Theme.themify(key: .border).cgColor
            textView.delegate = self
        }
    }
    var presenter: GuildInputDescriptionPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public var repository: GuildEntity = GuildEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GuildInputDescriptionViewControllerBuilder.rebuild(self)
        self.navigationMode = .buttonModal(title: repository.id == nil || repository.id == 0 ? R.string.localizable.create() : R.string.localizable.update())
        self.setFlatBackground()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard(_:)))
        gesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        resetRightNavigationBarButton()
        self.textView.text = repository.description
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.textView.resignFirstResponder()
    }
    
    override func onTapNavigationDoneButton(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository)
    }
    
    @objc func closeKeyboard(_ sender: Any) {
        self.textView.resignFirstResponder()
    }
}

extension GuildInputDescriptionViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.repository.description = textView.text
        self.repository.description == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
    }
}

extension GuildInputDescriptionViewController: GuildInputDescriptionPresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
        self.textView.text = repository.description
    }
    
    func guildInputDescriptionPresenter(didCreated repository: GuildEntity) {
        HUD.hide()
        HUD.flash(.success)
        self.dismissShouldAppear(animated: true)
    }
    
    func guildInputDescriptionPresenter(onError error: Error) {
        HUD.hide()
        HUD.flash(.error)
    }
    
}
