//
//  GuildMembersEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka

class GuildMembersEditViewController: BaseFormViewController {
    var presenter: GuildMembersEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: MemberEntities = MemberEntities()
    var repository: GuildEntity = GuildEntity()
    fileprivate var selectedMember: MemberEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildMembersEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.presenter?.fetch(repository: repository)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MemberEditViewController, segue.identifier == R.segue.guildMembersEditViewController.toMemberEdit.identifier, let repository = selectedMember {
            vc.repository = repository
            vc.guild = self.repository
            selectedMember = nil
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        let section = SelectableSection<ButtonRow>()
        form +++ section
        for repository in repositories.items {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = repository.nickname == "" ? repository.User?.nickname ?? "" : repository.nickname
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                if let url = URL(string: repository.picture_small) {
                    self.buttonImageRowCellUpdate($0, $1, url: url)
                } else {
                    self.buttonImageRowCellUpdate($0, $1, url: URL(string: DataConfig.Image.default_guild_image_url)!)
                }
            }
        }
        
        section.onSelectSelectableRow = { [unowned self] (cell, row) in
            self.selectedMember = self.repositories.filter({ "\($0.id ?? 0)" == row.tag }).first
            self.presenter?.toMemberEdit()
        }
    }
}

extension GuildMembersEditViewController: GuildMembersEditPresenterOutput {
    func sync(didFetchRepositories repositories: MemberEntities) {
        self.repositories = repositories
        self.reloadForm()
    }
}
