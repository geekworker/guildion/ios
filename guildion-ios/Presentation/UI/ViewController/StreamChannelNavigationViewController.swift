//
//  StreamChannelNavigationViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/20.
//

import Foundation
import UIKit

class StreamChannelNavigationViewController: UINavigationController {
    var streamChannelViewController: StreamChannelViewController? {
        self.viewControllers.first as? StreamChannelViewController
    }
    
    override var shouldAutorotate: Bool {
        return streamChannelViewController?.repository.LiveStream != nil && streamChannelViewController?.repository.LiveStream?.mode != .other
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return (streamChannelViewController?.repository.LiveStream != nil && streamChannelViewController?.repository.LiveStream?.mode != .other) ? .all : .portrait
    }
}
