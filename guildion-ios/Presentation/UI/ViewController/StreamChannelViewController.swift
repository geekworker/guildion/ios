//
//  StreamChannelViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import PKHUD
import AVFoundation
import AVKit
import MediaPlayer
import YoutubePlayer_in_WKWebView
import Hero
import SideMenu
import WebRTC
import SwiftyJSON
import RxSwift

protocol StreamChannelViewControllerDelegate: class {
    func streamChannelViewController(didChange meterLevel: Float, in streamChannelViewController: StreamChannelViewController)
    func streamChannelViewController(didChange meterLevel: Float, of peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController)
    func streamChannelViewController(didAdd peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController)
    func streamChannelViewController(didRemove peerConnection: PeerConnectionModel, in streamChannelViewController: StreamChannelViewController)
}

class StreamChannelViewController: WebRTCMessagesViewControlller, MiniScreenPresenter {
    var miniScreenTransitionContext: MiniScreenViewTransitionContext = MiniScreenViewTransitionContext()
    weak var delegate: StreamChannelViewControllerDelegate?
    @IBOutlet weak var streamChannelNavigationView: StreamChannelNavigationView! {
        didSet {
            streamChannelNavigationView.delegate = self
            streamChannelNavigationView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:))))
        }
    }
    @IBOutlet weak var fullScreenCoverView: UIView!
    @IBOutlet weak var streamChannelHeaderViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var streamButtonsView: StreamButtonsView! {
        didSet {
            streamButtonsView.delegate = self
        }
    }
    @IBOutlet weak var streamButtonsViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var streamButtonsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var streamButtonsViewWidthConstraint: NSLayoutConstraint! {
        didSet {
            streamButtonsViewWidthConstraint.constant = StreamButtonsView.viewWidth
        }
    }
    @IBOutlet weak var buttonContainerView: UIView! {
        didSet {
            buttonContainerView.layer.cornerRadius = 5
            buttonContainerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var streamChannelHeaderViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            streamChannelHeaderViewHeightConstraint.constant = SyncPlayerConfig.header_view_height
        }
    }
    @IBOutlet weak var streamChannelHeaderView: StreamChannelHeaderView! {
        didSet {
            streamChannelHeaderView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:))))
            streamChannelHeaderView.delegate = self
        }
    }
    @IBOutlet weak var participantsNavigationContainerView: UIView! {
        didSet {
            participantsNavigationContainerView.layer.cornerRadius = 5
            participantsNavigationContainerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var participantsNavigationViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var participantsNavigationView: ParticipantsNavigationView! {
        didSet {
            participantsNavigationView.delegate = self
        }
    }
    var presenter: StreamChannelPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var participants: ParticipantEntities = ParticipantEntities()
    var current_participant: ParticipantEntity? {
        return participants.filter({ $0.Member == self.current_member }).first
    }
    var repository: StreamChannelEntity = StreamChannelEntity() {
        didSet {
            if let vc = SideMenuManager.default.rightMenuNavigationController?.viewControllers.first as? ChannelMenuViewController {
                vc.repository = repository
            }
        }
    }
    override var channel: MessageableEntity {
        get {
            self.repository
        }
        set {
            super.channel = newValue
        }
    }
    var movieConnectable: Bool {
        return !repository.voice_only
    }
    var syncPlayManager: SyncPlayManagerModel = SyncPlayManagerModel()
    var folder: FolderEntity?
    var fetched: Bool = false
    var movieViewable: Bool {
        return repository.channelable_type != .VoiceChannel
    }
    public enum ScrollMode: Int {
        case up
        case down
        
        var participantsNavigationViewTopConstant: CGFloat {
            switch self {
            case .up: return 12
            case .down: return -100
            }
        }
        
        var streamButtonsViewTrailingConstant: CGFloat {
            switch self {
            case .up: return 10
            case .down: return -100
            }
        }
    }
    public var scrollMode: ScrollMode = .down
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Theme.themify(key: .background)
        self.view.clipsToBounds = true
        self.presenter?.join_sound()
        updateChannelLayout()
        updateMusicChannelLayout()
        updatePlayChannelLayout()
        updateVoiceChannelLayout()
        if !fetched {
            presenter?.fetch(repository: repository)
        }
        self.presenter?.fetchMembers()
        
        self.view.bringSubviewToFront(buttonContainerView)
        self.view.bringSubviewToFront(participantsNavigationContainerView)
        self.view.bringSubviewToFront(streamChannelHeaderView)
        self.view.bringSubviewToFront(streamChannelNavigationView)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LiveConfig.playingViewController = self.navigationController
        self.title = repository.name
        self.messagesCollectionView.backgroundColor = Theme.themify(key: .background)
        setupNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        self.inputAccessoryView?.isHidden = containerTransitionContext.isPanContainerPresented || isFullScreen
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        updatePreservedStream()
        self.presenter?.fetchMessages()
        if shouldReUpdateLiveStream { updateLiveStream() }
        self.inputAccessoryView?.isHidden = containerTransitionContext.isPanContainerPresented || isFullScreen
    }
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func viewWebSocketHealthChecked(_ health: Bool) {
        super.viewWebSocketHealthChecked(health)
        presenter?.join()
    }
    
    override func viewWebSocketDidDisconnect() {
        presenter?.webscoketDidDisconnect()
    }
    
    func unbind() {
        presenter?.leave()
        self.streamChannelHeaderView.pause()
        presenter?.leave_sound()
        LiveConfig.playingViewController = nil
        self.closeWebRTC()
        WebSocketHandler.shared.reject(self)
        store.unsubscribe(self)
        self.presenter?.unbind()
    }
    
    @objc func onTapLeaveButton(_ sender: Any) {
        unbind()
        self.dismissShouldAppear(animated: true, completion: nil)
    }
    
    override var shouldAutorotate: Bool {
        return repository.LiveStream != nil && repository.LiveStream?.mode != .other && self.movieConnectable
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return (repository.LiveStream != nil && repository.LiveStream?.mode != .other) && self.movieConnectable ? .all : .portrait
    }
    
    override func mute() {
        super.mute()
        self.streamButtonsView.muteButton.imageColor = muted ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
    }
    
    override func unmute() {
        super.unmute()
        self.streamButtonsView.muteButton.imageColor = muted ? Theme.themify(key: .cancel) : Theme.themify(key: .main)
    }
    
    override func toMemberMenu(repository: MemberEntity) {
        self.presenter?.toMemberMenu(repository: repository)
    }
    
    override func toMessageMenu(repository: MessageEntity) {
        self.presenter?.toMessageMenu(repository: repository)
    }
    
    override func toWebBrowser(_ url: URL, repositories: FileEntities? = nil) {
        super.toWebBrowser(url, repositories: repositories)
        self.presenter?.toWebBrowser(url, repositories: repositories)
    }
    
    override func toImageViewer(repository: FileEntity) {
        self.presenter?.toImageViewer(repository: repository)
    }
    
    override func decrement() {
        self.presenter?.decrements()
    }
    
    override func sendMessage(_ newRepository: MessageEntity) {
        super.sendMessage(newRepository)
        if newRepository.id == 0 || newRepository.id == nil {
            for file in newRepository.Files?.items ?? [] { file.is_private = repository.is_private }
            self.presenter?.create(newRepository)
        } else {
            self.presenter?.update(newRepository)
        }
    }
    
    override func sendReaction(_ newRepository: ReactionEntity) {
        self.presenter?.create(newRepository)
    }
    
    override func configureMessageCollectionView() {
        super.configureMessageCollectionView()
        self.messagesCollectionView.contentInset = UIEdgeInsets(top: streamChannelHeaderViewHeightConstraint.constant, left: 0, bottom: 0, right: 0)
    }
    
    override func verticalScrollDirection(_ direction: ScrollDirection) {
        guard self.messagesCollectionView.frame.height - streamChannelHeaderViewHeightConstraint.constant < self.messagesCollectionView.contentSize.height else {
            self.switchStreamContainer(mode: .up)
            return
        }
        let offsetY = self.messagesCollectionView.contentOffset.y
        switch direction {
        case .Down:
            guard offsetY < self.messagesCollectionView.contentSize.height - self.view.frame.height else { return }
            DispatchQueue.main.async { [unowned self] in
                self.switchStreamContainer(mode: .down)
            }
        case .Up:
            guard offsetY > -self.messagesCollectionView.contentInset.top else { return }
            DispatchQueue.main.async { [unowned self] in
                self.switchStreamContainer(mode: .up)
            }
        default: break
        }
    }
    
    override func willTyping() {
        self.presenter?.willTyping()
    }
    
    override func didTyping() {
        self.presenter?.didTyping()
    }
    
    override func seek(at duration: Float) {
        super.seek(at: duration)
        guard duration > 0, self.syncPlayManager.status == .paused || self.syncPlayManager.status == .playing else { return }
        self.streamChannelHeaderView.seek(at: duration)
        self.streamChannelHeaderView.delegate?.streamChannelHeaderView(
            didSeek: self.streamChannelHeaderView.repository,
            at: duration,
            in: self.streamChannelHeaderView
        )
        self.streamChannelHeaderView.syncStatusAfterSeek()
    }
    
    func initalizeRepository(_ repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        guard !fetched else { return }
        StreamChannelViewControllerBuilder.rebuild(self)
        self.repository = repository
        self.current_member = current_member
        self.current_guild = current_guild
        self.permission = permission
        self.presenter?.repositories = repository
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = Theme.themify(key: .backgroundThick)
        navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)]
        navigationController?.navigationBar.isTranslucent = false
        let members = UIBarButtonItem(image: R.image.members()!.withRenderingMode(.alwaysTemplate).resizedImage(newSize: CGSize(width: 26, height: 20)), style: .plain, target: self, action: #selector(onTapNavigationMembersButton(_:)
                                    ))
        
        let leave = UIBarButtonItem(image: R.image.close()!.withRenderingMode(.alwaysTemplate).tint(color: Theme.themify(key: .danger)).resizeSizeFit(width: 18).withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.onTapLeaveButton(_:)))
        navigationItem.rightBarButtonItems?.append(leave)
        
        navigationItem.rightBarButtonItems = [members]
        navigationItem.leftBarButtonItems = [leave]
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc func onTapNavigationMembersButton(_ sender: Any) {
        self.presenter?.toChannelMenu()
    }
    
    private var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.navigationController?.view?.window)

        if sender.state == .began {
            initialTouchPoint = touchPoint
        } else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 50 {
                self.navigationController?.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.toMiniScreen()
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.navigationController?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    func switchStreamContainer(mode: ScrollMode, animated: Bool = true) {
        self.scrollMode = mode
        self.participantsNavigationViewTopConstraint.constant = self.scrollMode.participantsNavigationViewTopConstant
        self.streamButtonsViewTrailingConstraint.constant = self.scrollMode.streamButtonsViewTrailingConstant
        UIView.animate(withDuration: animated ? 0.2 : 0, delay: 0, options: UIView.AnimationOptions(), animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    fileprivate var shouldReUpdateLiveStream: Bool = false
    public func updateLiveStream() {
        self.syncPlayManager.duration = 0
        self.syncPlayManager.ready_status = .readyToPlay
        guard let stream = repository.LiveStream else { return }
        self.shouldReUpdateLiveStream = !isViewLoaded
        if isViewLoaded {
            self.streamChannelHeaderView.setRepository(repository: stream)
            self.presenter?.increments()
            self.updatePreservedStream()
            var menus: [StreamButtonsView.ButtonType] = stream.StreamableId != nil && stream.StreamableId != 0 ? [.route, .info, .pick, .mute] : [.route, .pick, .mute]
            if stream.mode == .fileYouTube || stream.mode == .fileReferenceYouTube {
                menus.append(.youtube)
            }
            if !self.voiceConnectable {
                menus = menus.filter({ $0 != .mute })
            }
            if !self.movieConnectable {
                menus = menus.filter({ $0 != .pick && $0 != .info })
            }
            self.streamButtonsView.setRepositories(menus)
            self.streamButtonsViewHeightConstraint.constant = StreamButtonsView.viewHeight(self.streamButtonsView.repositories)
        }
        guard let reference = self.repository.LiveStream?.FileReference, let folder_id = reference.FolderId else { return }
        self.presenter?.fetch_folder(id: folder_id)
    }
    
    func updatePreservedStream() {
        guard let stream = repository.LiveStream else {
            return
        }
        if let file = stream.File {
            self.presenter?.increment(file)
            self.presenter?.decrement(file)
        }
        
        if let reference = stream.FileReference {
            self.presenter?.increment(reference)
            self.presenter?.decrement(reference)
        }
    }
    
    func updateChannelLayout() {
        let shouldUpdate = self.repository.channelable_type == .StreamChannel || self.repository.channelable_type == .PlayChannel || self.repository.channelable_type == .StreamDMChannel
        guard shouldUpdate else { return }
        self.navigationController?.setNavigationBarHidden(shouldUpdate, animated: false)
        self.streamChannelHeaderView.isHidden = !shouldUpdate
        self.streamChannelHeaderViewHeightConstraint.constant = !shouldUpdate ? 0 : SyncPlayerConfig.header_view_height
        self.streamChannelNavigationView.isHidden = !shouldUpdate
        self.participantsNavigationContainerView.isHidden = !shouldUpdate
        self.messagesCollectionView.contentInset = UIEdgeInsets(top: streamChannelHeaderViewHeightConstraint.constant, left: 0, bottom: 0, right: 0)
        self.streamButtonsView.setRepositories([.route, .pick, .mute])
        self.streamButtonsViewHeightConstraint.constant = StreamButtonsView.viewHeight(self.streamButtonsView.repositories)
    }
    
    func updateVoiceChannelLayout() {
        let shouldUpdate = self.repository.channelable_type == .VoiceChannel
        guard shouldUpdate else { return }
        self.navigationController?.setNavigationBarHidden(shouldUpdate, animated: false)
        self.streamChannelHeaderViewHeightConstraint.constant = SyncPlayerConfig.header_view_height
        self.messagesCollectionView.contentInset = UIEdgeInsets(top: streamChannelHeaderViewHeightConstraint.constant, left: 0, bottom: 0, right: 0)
        self.streamButtonsView.setRepositories([.route, .mute])
        self.streamButtonsViewHeightConstraint.constant = StreamButtonsView.viewHeight(self.streamButtonsView.repositories)
        self.participantsNavigationView.isHidden = shouldUpdate
        self.participantsNavigationContainerView.isHidden = shouldUpdate
        self.delegate = self.streamChannelHeaderView.voiceHeaderCoverView
    }
    
    func updateMusicChannelLayout() {
        let shouldUpdate = self.repository.channelable_type == .MusicChannel
        guard shouldUpdate else { return }
        self.navigationController?.setNavigationBarHidden(shouldUpdate, animated: false)
        self.streamButtonsView.setRepositories([.route, .pick])
        self.streamButtonsViewHeightConstraint.constant = StreamButtonsView.viewHeight(self.streamButtonsView.repositories)
        self.participantsNavigationView.isHidden = false
        self.participantsNavigationContainerView.isHidden = false
    }
    
    func updatePlayChannelLayout() {
        let shouldUpdate = self.repository.channelable_type == .PlayChannel
        guard shouldUpdate else { return }
        self.navigationController?.setNavigationBarHidden(shouldUpdate, animated: false)
        self.streamButtonsView.setRepositories([.route, .pick])
        self.streamButtonsViewHeightConstraint.constant = StreamButtonsView.viewHeight(self.streamButtonsView.repositories)
        self.participantsNavigationView.isHidden = shouldUpdate
        self.participantsNavigationContainerView.isHidden = shouldUpdate
    }
    
    @objc func rotated() {
        guard repository.LiveStream != nil else { return }
        switch UIDevice.current.orientation {
        case .landscapeLeft:
            self.toFullScreen(left: true, orientationOverride: false)
        case .landscapeRight:
            self.toFullScreen(left: false, orientationOverride: false)
        case .portrait: self.toNormalScreen()
        default: break
        }
    }
    
    fileprivate var isFullScreen: Bool = false
    fileprivate var isMiniScreen: Bool = false
    func toFullScreen(left: Bool = false, orientationOverride: Bool = true) {
        guard !isFullScreen, self.shouldAutorotate else {
            guard orientationOverride, self.shouldAutorotate else { return }
            let value = left ? UIInterfaceOrientation.landscapeLeft.rawValue : UIInterfaceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            return
        }
        self.containerTransitionContext.forceDismiss(animated: false)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.inputAccessoryView?.isHidden = true
        self.fullScreenCoverView.isHidden = false
        isFullScreen = true
        isMiniScreen = false
        self.inputAccessoryView?.isHidden = isFullScreen
        self.streamChannelHeaderView.screenMode = .full
        self.streamChannelNavigationView.isHidden = true
        if orientationOverride {
            let value = left ? UIInterfaceOrientation.landscapeLeft.rawValue : UIInterfaceOrientation.landscapeRight.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
        self.view.layoutIfNeeded()
    }
    
    func toNormalScreen() {
        guard isFullScreen, self.shouldAutorotate else { return }
        self.inputAccessoryView?.isHidden = false
        self.fullScreenCoverView.isHidden = false
        self.streamChannelHeaderView.screenMode = .normal
        isFullScreen = false
        isMiniScreen = false
        self.streamChannelNavigationView.isHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        self.view.layoutIfNeeded()
    }
    
    func toMiniScreen() {
        // guard movieConnectable else { self.dismissShouldAppear(animated: true); return }
        self.miniScreenTransitionContext.sourceRect = CGRect(x: self.streamChannelHeaderView.frame.minX, y: self.streamChannelHeaderView.frame.minY, width: MiniScreenView.getSize().width, height: self.view.frame.height)
        self.streamChannelHeaderViewHeightConstraint.constant = MiniScreenView.getSize().height
        self.streamChannelHeaderView.screenMode = .mini
        self.streamChannelNavigationView.isHidden = true
        isMiniScreen = true
        self.dismissShouldAppear(animated: true, completion: {
            self.presentMiniScreen(MiniScreenView.getInstance(), completion: nil)
        })
    }
    
    func exitMiniScreen() {
        self.inputAccessoryView?.isHidden = false
        self.fullScreenCoverView.isHidden = false
        self.streamChannelHeaderView.screenMode = .normal
        isFullScreen = false
        isMiniScreen = false
        self.streamChannelNavigationView.isHidden = false
        self.streamChannelHeaderViewHeightConstraint.constant = SyncPlayerConfig.header_view_height
    }
    
    func shouldExitMiniScreen(animated flag: Bool) {
        guard let navVC = self.navigationController else { return }
        UIViewController.topMostController().dismiss(animated: true, completion: nil)
        navVC.modalPresentationStyle = .overCurrentContext
        MiniScreenView.shared?.dismiss(animated: true)
        UIViewController.topMostController().present(navVC, animated: true, completion: nil)
    }
    
    func willDismissMiniScreen(_ viewToPresent: MiniScreenAnimatable) {
        self.exitMiniScreen()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
            
        coordinator.animate(alongsideTransition: { _ in
        }, completion: { _ in
            if UIDevice.current.orientation.isLandscape {
                self.streamChannelHeaderViewHeightConstraint.constant = UIDevice.current.orientation != .landscapeLeft && UIDevice.current.orientation != .landscapeRight ? self.view.frame.width + 30 : self.view.frame.height + 30
                self.streamChannelHeaderViewTopConstraint.constant = 0
            } else {
                self.streamChannelHeaderViewHeightConstraint.constant = SyncPlayerConfig.header_view_height
                self.streamChannelHeaderViewTopConstraint.constant = 0
            }
        })
    }
    
    public func start(_ repository: FileEntity) {
        self.streamChannelHeaderView.pause()
        let entity = StreamEntity()
        entity.Streamable = repository
        entity.StreamableId = repository.id
        entity.streamable_type = .File
        self.presenter?.start(entity)
    }
    
    public func start(_ repository: FileReferenceEntity) {
        self.streamChannelHeaderView.pause()
        let entity = StreamEntity()
        entity.Streamable = repository
        entity.StreamableId = repository.id
        entity.streamable_type = .FileReference
        self.presenter?.start(entity)
    }
    
    override func didTalkRequest() {
        super.didTalkRequest()
    }
    
    override func didChangeAudioSession() {
        DispatchQueue.main.async {
            self.syncPlayManager.ready_status == .readyToPlay ? self.streamChannelHeaderView.play() : self.streamChannelHeaderView.pause()
        }
    }
    
    override func didAddPeerConnection(_ peerConnectionModel: PeerConnectionModel) {
        DispatchQueue.main.async {
            super.didAddPeerConnection(peerConnectionModel)
            self.delegate?.streamChannelViewController(didAdd: peerConnectionModel, in: self)
            self.presenter?.fetchParticipants()
        }
    }
    
    override func didRemovePeerConnection(_ peerConnectionModel: PeerConnectionModel) {
        DispatchQueue.main.async {
            super.didRemovePeerConnection(peerConnectionModel)
            self.delegate?.streamChannelViewController(didRemove: peerConnectionModel, in: self)
            self.presenter?.fetchParticipants()
            if self.peerConnections.syncings.count == 0 {
                self.syncPlayManager.host = true
                self.syncPlayManager.completed = true
                self.syncPlayManager.ready_status == .readyToPlay ? self.streamChannelHeaderView.play() : self.streamChannelHeaderView.pause()
            }
        }
    }
    
    override func didChange(meterLevel: Float) {
        self.current_participant?.meterLevel = meterLevel
        self.delegate?.streamChannelViewController(didChange: meterLevel, in: self)
    }
    
    override func didChange(_ peerConnectionModel: PeerConnectionModel, meterLevel: Float) {
        guard let participant = participants.filter({ peerConnectionModel.user?.id == $0.Member?.UserId }).first else { return }
        participant.meterLevel = meterLevel
        self.delegate?.streamChannelViewController(didChange: meterLevel, of: peerConnectionModel, in: self)
    }
}

extension StreamChannelViewController: StreamButtonsViewDelegate {
    func streamButtonsView(didTapPick streamButtonsView: StreamButtonsView) {
        self.presenter?.toStreamableSelects()
    }
    
    func streamButtonsView(didTapInfo streamButtonsView: StreamButtonsView) {
        self.presenter?.toStreamChannelInfo()
    }
    
    func streamButtonsView(didTapYouTube streamButtonsView: StreamButtonsView) {
        guard let stream = repository.LiveStream, stream.mode == .fileYouTube || stream.mode == .fileReferenceYouTube, let file = stream.mode == .fileYouTube ? stream.File : stream.FileReference?.File, let url = URL(string: file.url) else { return }
        self.toWebBrowser(url)
    }
    
    func streamButtonsView(didTapMute muted: Bool, streamButtonsView: StreamButtonsView) {
        self.muted ? self.unmute() : self.mute()
    }
}

extension StreamChannelViewController: StreamChannelInfoViewControllerDelegate {
    func streamChannelInfoViewController(willAppear streamChannelInfoViewController: StreamChannelInfoViewController) {}
    
    func streamChannelInfoViewController(toSelects streamChannelInfoViewController: StreamChannelInfoViewController) {
        self.presenter?.toStreamableSelects()
    }
    
    func streamChannelInfoViewController(shouldStart stream: StreamEntity, from streamChannelInfoViewController: StreamChannelInfoViewController) {
        self.presenter?.start(stream)
    }
    
    func streamChannelInfoViewControllerDidDismiss(in streamChannelInfoViewController: StreamChannelInfoViewController) {}
    
    func streamChannelInfoViewControllerWillDismiss(in streamChannelInfoViewController: StreamChannelInfoViewController) {
        self.inputAccessoryView?.fadeIn(type: .Normal, completed: nil)
    }
}

extension StreamChannelViewController: ParticipantsNavigationViewDelegate {
    func participantsNavigationView(onTap repositories: ParticipantEntities, in participantsNavigationView: ParticipantsNavigationView) {
        self.presenter?.toParticipants()
    }
    
    func participantsNavigationView(onTapProfile repository: ParticipantEntity, in participantsNavigationView: ParticipantsNavigationView) {
        guard let member = repository.Member else { return }
        self.presenter?.toMemberMenu(repository: member)
    }
    
    func participantsNavigationView(onTapMore repositories: ParticipantEntities, in participantsNavigationView: ParticipantsNavigationView) {
        self.presenter?.toParticipants()
    }
}

extension StreamChannelViewController: ParticipantsViewControllerDelegate {
    func participantsViewControllerWillDismiss(in participantsViewController: ParticipantsViewController) {
        self.inputAccessoryView?.fadeIn(type: .Normal, completed: nil)
    }
}

extension StreamChannelViewController: StreamableSelectsViewControllerDelegate {
    func streamableSelectsViewController(shouldStart repository: StreamEntity, in streamableSelectsViewController: StreamableSelectsViewController) {
        DispatchQueue.main.async {
            self.repository.Streams?.append(repository)
            self.presenter?.syncingStream = true
            self.presenter?.seekingStream = false
            self.updateLiveStream()
        }
    }
    
    func streamableSelectsViewControllerWillDismiss(in streamableSelectsViewController: StreamableSelectsViewController) {
        self.inputAccessoryView?.fadeIn(type: .Normal, completed: nil)
        self.becomeFirstResponder()
    }
    
    func streamableSelectsViewControllerDidDismiss(in streamableSelectsViewController: StreamableSelectsViewController) {
        self.inputAccessoryView?.fadeIn(type: .Normal, completed: nil)
        self.becomeFirstResponder()
    }
}

extension StreamChannelViewController: StreamChannelHeaderViewDelegate {
    func streamChannelHeaderView(toStreamableSelects repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.toStreamableSelects()
    }
    
    func streamChannelHeaderView(didPlay repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.play()
    }
    
    func streamChannelHeaderView(didPause repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.pause()
    }
    
    func streamChannelHeaderView(willStart repository: StreamEntity, loggable: Bool, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.start(repository, loggable: loggable)
        if repository.StreamableId == nil && repository.Streamable == nil && isFullScreen {
            self.toNormalScreen()
        }
    }
    
    func streamChannelHeaderView(willSync repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.syncStream(stream: repository)
    }
    
    func streamChannelHeaderView(didSeek repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.seek(at: duration)
    }
    
    func streamChannelHeaderView(didChangeStatus status: SyncPlayManagerStatus, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.syncPlayManager.duration = self.streamChannelHeaderView.getCurrentTime()
        self.presenter?.changePlayerStatus(status)
    }
    
    func streamChannelHeaderView(shouldFullScreen streamChannelHeaderView: StreamChannelHeaderView) {
        self.toFullScreen()
    }
    
    func streamChannelHeaderView(shouldExitFullScreen streamChannelHeaderView: StreamChannelHeaderView) {
        self.toNormalScreen()
    }
    
    func streamChannelHeaderView(shouldReload repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.syncPlayManager.stream = repository
        self.syncPlayManager.duration = duration
        self.presenter?.fetchRequest()
    }
    
    func streamChannelHeaderView(shouldClose repository: StreamEntity, in streamChannelHeaderView: StreamChannelHeaderView) {
        let alert: UIAlertController = UIAlertController(title: R.string.localizable.streamChannelFinish(), message: R.string.localizable.finishReally(), preferredStyle:  UIAlertController.Style.alert)

        let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.finish(), style: UIAlertAction.Style.default, handler:{
            [unowned self](action: UIAlertAction!) -> Void in
            let entity = StreamEntity()
            entity.Streamable = nil
            entity.StreamableId = nil
            self.presenter?.start(entity)
            DispatchQueue.main.async {
                self.toNormalScreen()
            }
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })
        alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func streamChannelHeaderView(didPlayTime duration: Float, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.syncPlayManager.duration = duration
        self.didPlayTime(duration)
    }
    
    func streamChannelHeaderView(enableEndIncrement repository: StreamableEntity, in streamChannelHeaderView: StreamChannelHeaderView) -> Bool {
        return self.syncPlayManager.host
    }
    
    func streamChannelHeaderView(seekReady streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.handleSyncSeekStream()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.presenter?.fetchRequest()
        })
    }
    
    func streamChannelHeaderView(becomeReady streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.handleSyncStream()
        if peerConnections.count > 0 {
            self.configureWebRTCAudioSession()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.presenter?.fetchRequest()
        })
    }
    
    func streamChannelHeaderView(switchLoop is_loop: Bool, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.switchLoop(is_loop: is_loop)
    }
    
    func streamChannelHeaderView(didInterrupt repository: StreamEntity, at duration: Float, in streamChannelHeaderView: StreamChannelHeaderView) {
        self.presenter?.transferHost(onSuccess: {
            self.presenter?.fetchRequest()
        })
        self.syncPlayManager.ready_status == .readyToPlay ? self.streamChannelHeaderView.play() : self.streamChannelHeaderView.pause()
    }
}

extension StreamChannelViewController: StreamChannelNavigationViewDelegate {
    func streamChannelNavigationView(shouldClose repository: StreamChannelEntity, in streamChannelNavigationView: StreamChannelNavigationView) {
        unbind()
        self.dismissShouldAppear(animated: true, completion: nil)
    }
    
    func streamChannelNavigationView(toMembersMenu repository: StreamChannelEntity, in streamChannelNavigationView: StreamChannelNavigationView) {
        self.presenter?.toChannelMenu()
    }
}

extension StreamChannelViewController: StreamChannelPresenterOutput {
    
    func syncCurrentMember(didFetchIncrementRepository repository: StreamableEntity?) {
        DispatchQueue.main.async {
            self.streamChannelHeaderView.setNextStream(repository: repository)
        }
    }
    
    func syncCurrentMember(didFetchDecrementRepository repository: StreamableEntity?) {
        DispatchQueue.main.async {
            self.streamChannelHeaderView.setBackStream(repository: repository)
        }
    }
    
    func streamChannelPresenter(didSyncStream syncPlayManager: SyncPlayManagerModel) {
        self.streamChannelHeaderView.playAfterSyncing()
    }
    
    func streamChannelPresenter(didSeekStream syncPlayManager: SyncPlayManagerModel) {
        self.streamChannelHeaderView.playAfterSeeking()
    }
    
    func streamChannelPresenter(fetchRequest syncPlayManager: SyncPlayManagerModel, from: PeerConnectionModel) {
        // MARK: magic number of duration diff in fetching
        self.syncPlayManager.duration = self.streamChannelHeaderView.getCurrentTime() + 0.5
        guard self.syncPlayManager.completed else { return }
        self.presenter?.fetchResponse(syncPlayManager: syncPlayManager)
    }
    
    func streamChannelPresenter(fetchResponse syncPlayManager: SyncPlayManagerModel, from: PeerConnectionModel) {
        syncPlayManager.ready_status == .readyToPlay ? self.streamChannelHeaderView.seekAndPlay(at: syncPlayManager.duration) : self.streamChannelHeaderView.seekAndPause(at: syncPlayManager.duration)
    }
    
    func streamChannelPresenterStopPlayer() {
        streamChannelHeaderView.pause()
        self.presenter?.changePlayerStatus(.paused)
    }
    
    func streamChannelPresenterPlayPlayer() {
        streamChannelHeaderView.play()
        self.presenter?.changePlayerStatus(.playing)
    }
    
    func streamChannelPresenterSeekPlayer(syncPlayManager: SyncPlayManagerModel) {
        // MARK: magic number of duration diff in seeking
        streamChannelHeaderView.seek(at: syncPlayManager.duration + 0.5)
    }
    
    func streamChannelPresenter(willSyncStream syncPlayManager: SyncPlayManagerModel) {
        self.repository.Streams?.append(syncPlayManager.stream)
        self.updateLiveStream()
        if syncPlayManager.stream.StreamableId == nil && syncPlayManager.stream.Streamable == nil && isFullScreen {
            self.toNormalScreen()
        }
    }
    
    func streamChannelPresenter(shouldStart repository: StreamEntity) {
        DispatchQueue.main.async {
            if let streams = self.repository.Streams {
                streams.append(repository)
            } else {
                self.repository.Streams = StreamEntities()
                self.repository.Streams?.append(repository)
            }
            self.updateLiveStream()
        }
    }
    
    func streamChannelPresenter(onError error: Error) {
    }
    
    func syncCurrentMember(didFetchRepository repository: MemberEntity) {
        self.current_member = repository
    }
    
    func sync(didFetchRepository repository: StreamChannelEntity) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.fetched = true
            self.repository = repository
            self.streamChannelHeaderView.loopable = repository.is_loop
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented || self.isFullScreen
            self.evalPermission()
            self.streamChannelNavigationView.setRepository(repository: repository)
            self.title = repository.name
            self.updateChannelLayout()
            self.updateMusicChannelLayout()
            self.updatePlayChannelLayout()
            self.updateVoiceChannelLayout()
            self.streamChannelHeaderView.updateScreenLayout()
        }
    }
    
    func sync(didFetchRepositories repositories: MessageEntities) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented || self.isFullScreen
            guard repositories.count > 0 else { return }
            let shouldScrollToBottom: Bool = self.repositories.count == 0 && repositories.count > 0
            self.repositories = repositories
            self.messagesCollectionView.reloadData()
            if shouldScrollToBottom {
                self.messagesCollectionView._scrollToBottom(animated: false)
            }
        }
    }
    
    func sync(didFetchRepositories repositories: ParticipantEntities) {
        DispatchQueue.main.async {
            if !self.movieConnectable {
                self.streamChannelHeaderView.setRepositories(repositories, current_member: self.current_member)
            } else {
                self.participantsNavigationView.setRepositories(repositories)
            }
        }
    }
    
    func sync(didFetchRepositories repositories: MemberEntities) {
        DispatchQueue.main.async {
            self.current_members = repositories
            self.messagesCollectionView.reloadData()
        }
    }
    
    func sync(didFetchFolder repository: FolderEntity) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented || self.isFullScreen
            self.view.hideAllToasts()
            self.folder = repository
            if self.isViewLoaded {
                self.streamChannelHeaderView.setRepository(repository: repository)
            }
        }
    }
    
    func streamChannelPresenter(didJoin repository: StreamChannelEntity) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented || self.isFullScreen
            self.view.hideAllToasts()
            self.talkRequest()
            self.presenter?.checkBecomableHostMessage()
        }
    }
    
    func streamChannelPresenter(didIncrements repositories: MessageEntities) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented || self.isFullScreen
            self.view.hideAllToasts()
            self.didIncrements(repositories)
        }
    }
    
    func streamChannelPresenter(didDecrements repositories: MessageEntities) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented || self.isFullScreen
            self.view.hideAllToasts()
            self.didDecrements(repositories)
        }
    }
    
    func streamChannelPresenter(didCreated repository: MessageEntity) {
        DispatchQueue.main.async {
            self.didCreated(repository)
        }
    }
    
    func streamChannelPresenter(didCreated repository: ReactionEntity) {
        DispatchQueue.main.async {
            self.messagesCollectionView.reloadDataAndKeepOffset()
        }
    }
    
    func streamChannelPresenter(willTyping repository: MemberEntity) {
        self.addTypingMember(repository: repository)
    }
    
    func streamChannelPresenter(didTyping repository: MemberEntity) {
        self.removeTypingMember(repository: repository)
    }
    
    func streamChannelPresenter(didSwitchLoop is_loop: Bool) {
        self.streamChannelHeaderView.loopable = is_loop
    }
    
    func streamChannelPresenter(didCache repositories: MessageEntities, shouldScroll: Bool, in streamChannelPresenter: StreamChannelPresenter) {
        DispatchQueue.main.async {
            self.didCaches(repositories: repositories, shouldScroll: shouldScroll)
        }
    }
    
    func streamChannelPresenter(didRetry count: Int, in streamChannelPresenter: StreamChannelPresenter) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.fadeOut(type: .Normal, completed: nil)
            self.view.makeToast(R.string.localizable.domainErrorDetail())
        }
    }
    
    func streamChannelPresenter(shouldReload streamChannelPresenter: StreamChannelPresenter) {
        DispatchQueue.main.async {
            self.repositories = self._repositories
            self.messagesCollectionView.reloadDataAndKeepOffset()
        }
    }
    
    func streamChannelPresenter(didCheckFirst streamChannelPresenter: StreamChannelPresenter) {
        self.syncPlayManager.host = true
        self.syncPlayManager.completed = true
    }
    
    func streamChannelPresenter(willExitBackground is_background: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.presenter?.fetchRequest()
        })
        if peerConnections.syncings.count == 0 {
            self.syncPlayManager.host = true
            self.syncPlayManager.completed = true
        }
        if self.syncPlayManager.host {
            self.syncPlayManager.ready_status == .readyToPlay ? self.streamChannelHeaderView.play() : self.streamChannelHeaderView.pause()
        }
    }
    
    func streamChannelPresenter(willEnterBackground is_background: Bool) {
        self.presenter?.transferHost()
    }
    
    func streamChannelPresenter(shouldTransferHost syncPlayManager: SyncPlayManagerModel, from: PeerConnectionModel) {
        self.syncPlayManager.ready_status == .readyToPlay ? self.streamChannelHeaderView.play() : self.streamChannelHeaderView.pause()
    }
}
