//
//  GuildInfoViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Hero
import Nuke
import SwiftMessages
import PKHUD

class GuildInfoViewController: BaseViewController {
    @IBOutlet weak var _baseView: UIView! {
        didSet {
            _baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.alwaysBounceVertical = false
            scrollView.delegate = self
            scrollView.contentInsetAdjustmentBehavior = .never
        }
    }
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .backgroundThick)
        }
    }
    @IBOutlet weak var guildImageView: UIImageView! {
        didSet {
            guildImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var closeButton: IconButton! {
        didSet {
            closeButton.layer.cornerRadius = closeButton.frame.width / 2
            closeButton.clipsToBounds = true
            closeButton.buttonImage = R.image.close()!.withRenderingMode(.alwaysTemplate)
            closeButton.backgroundColor = Theme.themify(key: .background).withAlphaComponent(0.7)
            closeButton.imageColor = Theme.themify(key: .backgroundContrast)
            closeButton.delegate = self
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var footerView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .background)
        }
    }
    @IBOutlet weak var guildFooterImageView: UIImageView! {
        didSet {
            guildFooterImageView.contentMode = .scaleAspectFill
            guildFooterImageView.layer.cornerRadius = 10
            guildFooterImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameFooterLabel: UILabel! {
        didSet {
            nameFooterLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var joinButton: SimpleButton! {
        didSet {
            joinButton.text = R.string.localizable.guildJoin()
            joinButton.normalColor = Theme.themify(key: .main)
            joinButton.canceledColor = Theme.themify(key: .cancel)
            joinButton.canceled = false
            joinButton.delegate = self
        }
    }
    @IBOutlet weak var guildFooterView: GuildFooterView! {
        didSet {
            guildFooterView.delegate = self
            guildFooterView.layer.cornerRadius = 5
            guildFooterView.clipsToBounds = true
        }
    }
    @IBOutlet weak var guildFooterViewBottomConstraint: NSLayoutConstraint!
    var presenter: GuildInfoPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: GuildEntity = GuildEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildInfoViewControllerBuilder.rebuild(self)
        self.guildFooterViewBottomConstraint.constant = -(self.guildFooterView.frame.height + 44)
        self.presenter?.fetch(uid: repository.uid)
        self.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:))))
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        presenter?.fetch(uid: repository.uid)
        self.view.backgroundColor = .clear
        updateLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateGuildFooterView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func setHeroId(repository: GuildEntity) {
        self.hero.isEnabled = true
        self.view.hero.id = "view/\(repository.uid)"
        self.guildImageView.hero.id = "guildImageView/\(repository.uid)"
        self.nameLabel.hero.id = "nameLabel/\(repository.uid)"
        // self.descriptionLabel.hero.id = "descriptionLabel/\(repository.uid)"
    }
    
    func updateLayout() {
        self.nameLabel.text = repository.name
        self.descriptionLabel.text = repository.description.count == 0 ? R.string.localizable.descriptionNone() : repository.description
        self.nameFooterLabel.text = repository.name
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: guildImageView)
            Nuke.loadImage(with: url, into: guildFooterImageView)
        }
        self.joinButton.canceled = repository.MemberRequests?.items.filter({ $0.VoterId == CurrentUser.getCurrentUserEntity()?.id }).count ?? 0 > 0
        self.joinButton.text = self.joinButton.canceled ? R.string.localizable.guildRequesting() : R.string.localizable.guildJoin()
        
        if repository.Members?.items.filter({ $0.UserId == CurrentUser.getCurrentUserEntity()?.id }).count ?? 0 > 0 {
            self.joinButton.canceled = true
            self.joinButton.text = R.string.localizable.guildJoined()
        }
        
        if UserDispatcher.checkBlocked(guild: repository, state: store.state.user) {
            self.joinButton.canceled = true
            self.joinButton.text = R.string.localizable.baned()
        }
        
        self.guildFooterView.setRepository(repository)
    }
    
    func updateNotfound() {
        let messageAlert = MessageView.viewFromNib(layout: .cardView)
        messageAlert.configureTheme(.error)
        messageAlert.configureDropShadow()
        messageAlert.configureContent(title: R.string.localizable.error(), body: R.string.localizable.errorNotfoundGuild())
        messageAlert.button?.isHidden = true
        var messageAlertConfig = SwiftMessages.defaultConfig
        messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
        self.dismissShouldAppear(animated: true)
    }
    
    func updateGuildFooterView() {
        self.guildFooterViewBottomConstraint.constant = self.scrollView.contentOffset.y + self.scrollView.frame.height > self.footerView.frame.minY + self.guildFooterView.frame.height + 12 ? -(self.guildFooterView.frame.height + 44) : 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    private var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        if sender.state == .began {
            initialTouchPoint = touchPoint
        } else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 70 {
                self.dismissShouldAppear(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
}

extension GuildInfoViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateGuildFooterView()
        if -scrollView.contentOffset.y > 0 {
            // self.view.frame = CGRect(x: 0, y: -scrollView.contentOffset.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        if -scrollView.contentOffset.y > 100 {
            self.dismissShouldAppear(animated: true, completion: nil)
        }
    }
}

extension GuildInfoViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.presenter?.join(repository)
    }
}

extension GuildInfoViewController: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        self.dismissShouldAppear(animated: true, completion: nil)
    }
}

extension GuildInfoViewController: GuildFooterViewDelegate {
    func guildFooterView(onJoin repository: GuildEntity, in guildFooterView: GuildFooterView) {
        self.presenter?.join(repository)
    }
}

extension GuildInfoViewController: GuildInfoPresenterOutput {
    func guildInfoPresenter(afterJoined repository: MemberEntity, in guildInfoPresenter: GuildInfoPresenter) {
        DispatchQueue.main.async {
            self.repository.Members?.append(repository)
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.success)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.guildJoinSuccess())
            messageAlert.button?.isHidden = true
            var messageAlertConfig = SwiftMessages.defaultConfig
            messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
            self.updateLayout()
        }
    }
    
    func guildInfoPresenter(afterCreated repository: MemberRequestEntity, in guildInfoPresenter: GuildInfoPresenter) {
        DispatchQueue.main.async {
            self.repository.MemberRequests?.append(repository)
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.success)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.guildRequestSuccess())
            messageAlert.button?.isHidden = true
            var messageAlertConfig = SwiftMessages.defaultConfig
            messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
            self.updateLayout()
        }
    }
    
    func sync(didFetchRepository repository: GuildEntity) {
        DispatchQueue.main.async {
            self.repository = repository
            repository.id == nil ? self.updateNotfound() : self.updateLayout()
        }
    }
    
    func guildInfoPresenter(onError error: Error, in guildInfoPresenter: GuildInfoPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
}

