//
//  StreamableSelectsViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit
import SPPermissions
import PKHUD
import Nuke
import AssetsPickerViewController
import Photos
import SwiftMessages

protocol StreamableSelectsViewControllerDelegate: class {
    func streamableSelectsViewController(shouldStart repository: StreamEntity, in streamableSelectsViewController: StreamableSelectsViewController)
    func streamableSelectsViewControllerWillDismiss(in streamableSelectsViewController: StreamableSelectsViewController)
    func streamableSelectsViewControllerDidDismiss(in streamableSelectsViewController: StreamableSelectsViewController)
}

class StreamableSelectsViewController: BaseViewController, PanContainerNavigationViewChildDelegate, PanContainerPresenter {
    var containerTransitionContext: PanContainerViewTransitionContext = PanContainerViewTransitionContext()
    weak var parentContainerView: PanContainerNavigationView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var uploadView: UIView! {
        didSet {
            uploadView.backgroundColor = Theme.themify(key: .backgroundLight)
            uploadView.layer.cornerRadius = 5
            uploadView.clipsToBounds = true
            uploadView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectUpload(_:))))
        }
    }
    @IBOutlet weak var uploadTitleLabel: UILabel! {
        didSet {
            uploadTitleLabel.textColor = Theme.themify(key: .string)
            uploadTitleLabel.text = R.string.localizable.uploadTitle()
        }
    }
    @IBOutlet weak var uploadDetailLabel: UILabel! {
        didSet {
            uploadDetailLabel.textColor = Theme.themify(key: .string)
            uploadDetailLabel.text = R.string.localizable.uploadDesc()
            uploadDetailLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var uploadImage: UIImageView! {
        didSet {
            uploadImage.layer.cornerRadius = uploadImage.frame.width / 2
            uploadImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var uploadChevron: UIImageView! {
        didSet {
            uploadChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            uploadChevron.tintColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var uploadFromURLView: UIView! {
        didSet {
            uploadFromURLView.backgroundColor = Theme.themify(key: .backgroundLight)
            uploadFromURLView.layer.cornerRadius = 5
            uploadFromURLView.clipsToBounds = true
            uploadFromURLView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectUploadFromURL(_:))))
        }
    }
    @IBOutlet weak var uploadFromURLTitleLabel: UILabel! {
        didSet {
            uploadFromURLTitleLabel.textColor = Theme.themify(key: .string)
            uploadFromURLTitleLabel.text = R.string.localizable.uploadTitleFromURL()
        }
    }
    @IBOutlet weak var uploadFromURLDetailLabel: UILabel! {
        didSet {
            uploadFromURLDetailLabel.textColor = Theme.themify(key: .string)
            uploadFromURLDetailLabel.text = R.string.localizable.uploadDescFromURL()
            uploadFromURLDetailLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var uploadFromURLLinkImage: UIImageView! {
        didSet {
            uploadFromURLLinkImage.image = R.image.link()!.withRenderingMode(.alwaysTemplate)
            uploadFromURLLinkImage.tintColor = Theme.themify(key: .backgroundContrast)
        }
    }
    @IBOutlet weak var uploadFromURLImage: UIImageView! {
        didSet {
//            uploadFromURLImage.layer.cornerRadius = uploadFromURLImage.frame.width / 2
//            uploadFromURLImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var uploadFromURLChevron: UIImageView! {
        didSet {
            uploadFromURLChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            uploadFromURLChevron.tintColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var uploadFromBrowserView: UIView! {
        didSet {
            uploadFromBrowserView.backgroundColor = Theme.themify(key: .backgroundLight)
            uploadFromBrowserView.layer.cornerRadius = 5
            uploadFromBrowserView.clipsToBounds = true
            uploadFromBrowserView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectUploadFromBrowser(_:))))
        }
    }
    @IBOutlet weak var uploadFromBrowserTitleLabel: UILabel! {
        didSet {
            uploadFromBrowserTitleLabel.textColor = Theme.themify(key: .string)
            uploadFromBrowserTitleLabel.text = R.string.localizable.uploadTitleFromBrowser()
        }
    }
    @IBOutlet weak var uploadFromBrowserDetailLabel: UILabel! {
        didSet {
            uploadFromBrowserDetailLabel.textColor = Theme.themify(key: .string)
            uploadFromBrowserDetailLabel.text = R.string.localizable.uploadDescFromBrowser()
            uploadFromBrowserDetailLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var uploadFromBrowserSearchImage: UIImageView! {
        didSet {
            uploadFromBrowserSearchImage.image = R.image.search()!.withRenderingMode(.alwaysTemplate)
            uploadFromBrowserSearchImage.tintColor = Theme.themify(key: .backgroundContrast)
        }
    }
    @IBOutlet weak var uploadFromBrowserImage: UIImageView! {
        didSet {
        }
    }
    @IBOutlet weak var uploadFromBrowserChevron: UIImageView! {
        didSet {
            uploadFromBrowserChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            uploadFromBrowserChevron.tintColor = Theme.themify(key: .border)
        }
    }
    var presenter: StreamableSelectsPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var current_guild: GuildEntity = GuildEntity()
    var current_member: MemberEntity = MemberEntity()
    var repositories: StreamableModel = StreamableModel()
    public var folders: FolderEntities = FolderEntities()
    weak var delegate: StreamableSelectsViewControllerDelegate?
    public lazy var assetsPickerHandler: AssetsPickerHandler = {
        let handler = AssetsPickerHandler()
        handler.delegate = self
        handler.allowImages = false
        return handler
    }()

    override func viewDidLoad() {
        self.navigationMode = .modalBorder
        super.viewDidLoad()
        // self.setFlatBackground()
        StreamableSelectsViewControllerBuilder.rebuild(self)
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.fetch()
        title = R.string.localizable.fileNewTitle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FileNewFromURLViewController, segue.identifier == R.segue.streamableSelectsViewController.toFileNewFromURL.identifier {
            vc.initRepository(current_guild: current_guild, current_member: current_member)
            vc.delegate = self
        } else if let vc = segue.destination as? FileNewFromBrowserViewController, segue.identifier == R.segue.streamableSelectsViewController.toFileNewFromBrowser.identifier {
            vc.initRepository(current_guild: current_guild, current_member: current_member)
            vc.delegate = self
        }
    }
    
    override func onTapNavigationCloseButton(_ sender: Any) {
        self.delegate?.streamableSelectsViewControllerWillDismiss(in: self)
        self.dismissContainer(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            self.delegate?.streamableSelectsViewControllerDidDismiss(in: self)
        })
    }
    
    @objc func onSelectUpload(_ sender: Any) {
        guard self.configurePhotoPermission() else { return }
        self.assetsPickerHandler.openImagePicker()
    }
    
    @objc func onSelectUploadFromURL(_ sender: Any) {
        self.presenter?.toFileNewFromURL()
    }
    
    @objc func onSelectUploadFromBrowser(_ sender: Any) {
        self.presenter?.toWebBrowser()
    }
}

extension StreamableSelectsViewController: UITableViewDelegate {
    fileprivate func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.register([
            UINib(resource: R.nib.folderTableViewCell): R.nib.folderTableViewCell.name,
        ])
        tableView.register(
            UINib(resource: R.nib.textTableViewHeaderFooterView),
            forHeaderFooterViewReuseIdentifier: R.nib.textTableViewHeaderFooterView.name
        )
        self.tableView.backgroundColor = Theme.themify(key: .background)
        self.tableView.backgroundView?.backgroundColor = Theme.themify(key: .background)
        self.tableView.alwaysBounceVertical = false
    }
    
    fileprivate func configureTableViewHeight() {
        self.tableViewHeightConstraint.constant = folders.count + 1 > 0 ? CGFloat(folders.count + 1) * FolderTableViewCell.height + TextTableViewHeaderFooterView.height : 0
    }
}

extension StreamableSelectsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folders.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.folderTableViewCell.name, for: indexPath) as? FolderTableViewCell else {
            return UITableViewCell()
        }
        if indexPath.row == 0 {
            cell.all_files = true
            cell.contentView.backgroundColor = Theme.themify(key: .background)
            cell.setRepository(FolderEntity())
            cell.delegate = self
            return cell
        }
        guard let repository = folders[safe: indexPath.row - 1] else { return UITableViewCell() }
        cell.setRepository(repository)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        guard indexPath.row > 0 else { self.presenter?.toSelectableAllFiles(); return }
        guard let repository = folders[safe: indexPath.row - 1] else { return }
        self.presenter?.toSelectablePlaylist(repository: repository)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.nib.textTableViewHeaderFooterView.name) as? TextTableViewHeaderFooterView else {
            fatalError("This section is not exist")
        }
        view.titleLabel.text = R.string.localizable.playlists()
        view.backgroundColor = Theme.themify(key: .backgroundThick)
        view.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return folders.count + 1 > 0 ? TextTableViewHeaderFooterView.height : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 { return FolderTableViewCell.height }
        guard folders[safe: indexPath.row - 1] != nil else {
            return 0
        }
        return FolderTableViewCell.height
    }
}

extension StreamableSelectsViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if $0.row > 0, let repositories = folders[safe: $0.row - 1]?.FileReferences {
                let fileRequests: [ImageRequest] = repositories.compactMap({
                    guard let file = $0.File, let url = URL(string: file.thumbnail) else { return nil }
                    let request = ImageRequest(url: url)
                    return request
                })
                requests += fileRequests
            }
        }
        return requests
    }
}

extension StreamableSelectsViewController: FolderTableViewCellDelegate {
}

extension StreamableSelectsViewController: PlaylistViewControllerSelectDelegate {
    func playlistViewController(didSelect repository: FileReferenceEntity, in playlistViewController: PlaylistViewController) {
        if playlistViewController.all_files {
            self.repositories.file = repository.File
        } else {
            self.repositories.reference = repository
        }
        self.presenter?.start(self.repositories)
        HUD.show(.progress)
    }
}

extension StreamableSelectsViewController: SPPermissionsDelegate, SPPermissionsDataSource {
    func configurePhotoPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self)
        return false
    }
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        switch permission {
        case .camera:
            cell.permissionTitleLabel.text = R.string.localizable.permissionCamera()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionCameraText()
        case .microphone:
            cell.permissionTitleLabel.text = R.string.localizable.permissionVoice()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionVoiceText()
        case .photoLibrary:
            cell.permissionTitleLabel.text = R.string.localizable.permissionPhoto()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionPhotoText()
        default: break
        }
        return cell
    }
    
    func didHide(permissions ids: [Int]) {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard permissions.isEmpty else { return }
        self.assetsPickerHandler.openImagePicker()
    }
}

extension StreamableSelectsViewController: AssetsPickerHandlerDelegate {
    var viewController: UIViewController {
        get {
            self
        }
    }
    
    func updateAssets(_ assets: FileEntities) {
        if self.repositories.files == nil {
            self.repositories.files = FileEntities()
        }
        self.repositories.files?.items += assets.items
        if let files = repositories.files, files.storage_count > 0, !files.available_storage {
            BaseWireframe.toPlansWithAlert()
            return
        }
        self.presenter?.uploads(assets)
        
        let messageAlert = MessageView.viewFromNib(layout: .cardView)
        messageAlert.configureTheme(.success)
        messageAlert.configureDropShadow()
        messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successUploading())
        messageAlert.button?.isHidden = true
        SwiftMessages.show(config: SwiftMessages.defaultConfig, view: messageAlert)
    }
    
    func cancelAssets() {
    }
}

extension StreamableSelectsViewController: WebKitViewControllerDelegate {
    func webKitViewController(didSelects repositories: FileEntities, in webKitViewController: WebKitViewController) {
        self.repositories.files = repositories
        self.presenter?.start(self.repositories)
        HUD.show(.progress)
    }
    
    func webKitViewController(didSelect url: URL, in webKitViewController: WebKitViewController) {
    }
    
    func webKitViewController(shouldSkip duration: Float, in webKitViewController: WebKitViewController) {
    }
}

extension StreamableSelectsViewController: FileNewFromURLViewControllerDelegate {
    func fileNewFromURLViewController(didSelects repositories: FileEntities) {
        self.repositories.files = repositories
        self.presenter?.start(self.repositories)
        HUD.show(.progress)
    }
}

extension StreamableSelectsViewController: FileNewFromBrowserViewControllerDelegate {
    func fileNewFromBrowserViewController(didSelects repositories: FileEntities) {
        self.repositories.files = repositories
        self.presenter?.start(self.repositories)
        HUD.show(.progress)
    }
}

extension StreamableSelectsViewController: StreamableSelectsPresenterOutput {
    func sync(didFetchFolders repositories: FolderEntities) {
        self.folders.items = repositories.filter({ $0.FileReferences?.count ?? 0 > 0 })
        self.tableView.reloadData()
        self.configureTableViewHeight()
    }
    
    func sync(didFetchRepositories repositories: StreamableModel) {
        self.repositories = repositories
    }
    
    func syncCurrentMember(didFetchRepository repository: MemberEntity) {
        self.current_member = repository
    }
    
    func streamableSelectsPresenter(shouldStart repository: StreamEntity) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.success)
            self.delegate?.streamableSelectsViewControllerWillDismiss(in: self)
            self.dismissContainer(animated: true, completion: { [weak self] in
                guard let self = self else { return }
                self.delegate?.streamableSelectsViewController(shouldStart: repository, in: self)
                self.delegate?.streamableSelectsViewControllerDidDismiss(in: self)
            })
        }
    }
    
    func streamableSelectsPresenter(didUpload repositories: FileEntities) {
    }
    
    func streamableSelectsPresenter(onError error: Error) {
        HUD.hide()
        HUD.flash(.error)
    }
}

