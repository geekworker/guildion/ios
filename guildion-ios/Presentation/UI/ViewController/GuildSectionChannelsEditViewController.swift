//
//  GuildSectionChannelsEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/31.
//

import Foundation
import UIKit
import Eureka

class GuildSectionChannelsEditViewController: BaseFormViewController {
    var presenter: GuildSectionChannelsEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: SectionChannelEntities = SectionChannelEntities()
    var repository: GuildEntity = GuildEntity()
    fileprivate var selectedSectionChannel: SectionChannelEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildSectionChannelsEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.tableView.setEditing(true, animated: false)
        self.presenter?.fetch(repository: repository)
        
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildSectionChannelEditViewController, segue.identifier == R.segue.guildSectionChannelsEditViewController.toSectionChannelEdit.identifier, let repository = selectedSectionChannel {
            vc.repository = repository
            selectedSectionChannel = nil
        }
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        let section = MultivaluedSection(multivaluedOptions: .Reorder, header: R.string.localizable.sectionChannel())
        section.tag = "section"
        
        if form.sectionBy(tag: "section") == nil {
            form +++ section
        }
        
        for repository in repositories.items {
            form.last! <<< ButtonRow(repository.uid){ lrow in
                lrow.title = repository.name
                lrow.selectableValue = repository.uid
            }
            .cellUpdate { [unowned self] in
                self.buttonRowLeftAlignmentUpdate($0, $1)
            }
            .onCellSelection { [unowned self] (cell, row) in
                self.selectedSectionChannel = self.repositories.filter({ $0.uid == row.tag }).first
                self.presenter?.toSectionChannelEdit()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard let source_repository = self.repositories[safe: sourceIndexPath.row], sourceIndexPath.row != destinationIndexPath.row else { return }
        repositories.remove(source_repository)
        repositories.insert(source_repository, at: destinationIndexPath.row)
        let entities = SectionChannelEntities()
        entities.items = repositories
            .enumerated()
            .compactMap({
                $0.element.index = $0.offset + 1
                return $0.element
            })
            .filter({ !$0.is_empty && !$0.is_dm })
        self.presenter?.updateIndexes(repositories: entities)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.setReorderControl(cell)
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension GuildSectionChannelsEditViewController: GuildSectionChannelsEditPresenterOutput {
    func sync(didFetchRepositories repositories: SectionChannelEntities) {
        let shouldConfigureForm = self.repositories.count == 0 && repositories.count > 0
        self.repositories.items = repositories.filter({ !$0.is_empty && !$0.is_dm })
        self.repositories.sort_mode = false
        if shouldConfigureForm {
            self.configureForm()
        } else {
            self.reloadForm()
        }
    }
}
