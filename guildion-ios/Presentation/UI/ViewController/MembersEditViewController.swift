//
//  MembersEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka

class MembersEditViewController: BaseFormViewController {
    var presenter: MembersEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: MemberEntities = MemberEntities()
    fileprivate var selectedMember: MemberEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        MembersEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MemberEditViewController, segue.identifier == R.segue.membersEditViewController.toMemberEdit.identifier, let repository = selectedMember, let guild = repository.Guild {
            vc.repository = repository
            vc.guild = guild
            selectedMember = nil
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        let section = SelectableSection<ButtonRow>()
        form +++ section
        
        for repository in repositories.items {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                let nickname = repository.nickname == "" ? repository.User?.nickname ?? "" : repository.nickname
                let guildName = repository.Guild?.name ?? ""
                lrow.title = "\(nickname) (\(guildName))"
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                if let url = URL(string: repository.picture_small) {
                    self.buttonImageRowCellUpdate($0, $1, url: url)
                } else {
                    self.buttonImageRowCellUpdate($0, $1, image: R.image.brandLogo()!)
                }
            }
        }
        
        section.onSelectSelectableRow = { [unowned self] (cell, row) in
            self.selectedMember = self.repositories.filter({ "\($0.id ?? 0)" == row.tag }).first
            self.presenter?.toMemberEdit()
        }
    }
}

extension MembersEditViewController: MembersEditPresenterOutput {
    func sync(didFetchRepositories repositories: MemberEntities) {
        let shouldUpdate = self.repositories.count == 0 && repositories.count > 0
        self.repositories = repositories
        if shouldUpdate {
            self.reloadForm()
        }
    }
}
