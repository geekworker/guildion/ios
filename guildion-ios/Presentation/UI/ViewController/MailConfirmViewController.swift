//
//  MailConfirmViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit
import TextFieldEffects
import PKHUD

class MailConfirmViewController: BaseViewController {
    @IBOutlet weak var codeField: HoshiTextField! {
        didSet {
            self.codeField.keyboardType = .numberPad
            self.codeField.font = Font.FT(size: .FL, family: .L)
            self.codeField.textColor = Theme.themify(key: .string)
            self.codeField.placeholderColor = Theme.themify(key: .placeholder)
            self.codeField.placeholder = R.string.localizable.emailCode()
            self.codeField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.codeField.borderActiveColor = Theme.themify(key: .main)
            self.codeField.delegate = self
            if #available(iOS 12.0, *) {
                self.codeField.textContentType = .oneTimeCode
            } else {
                // Fallback on earlier versions
            }
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = R.string.localizable.emailConfirm()
        }
    }
    @IBOutlet weak var detailText: UILabel! {
        didSet {
            self.detailText.text = R.string.localizable.confirmDetail()
        }
    }
    @IBOutlet weak var signupButton: SimpleButton! {
        didSet {
            self.signupButton.text = R.string.localizable.send()
            self.signupButton.normalColor = Theme.themify(key: .main)
            self.signupButton.canceledColor = Theme.themify(key: .cancel)
            self.signupButton.canceled = true
            self.signupButton.delegate = self
        }
    }
    
    var presenter: MailConfirmPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: SessionModel = SessionModel()
    var mailConfirmed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sessionChecking = false
        navigationMode = .noneBorder
        self.setFlatBackground()
        MailConfirmViewControllerBuilder.rebuild(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.bind()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProfileSettingViewController, segue.identifier == R.segue.mailConfirmViewController.toProfileSetting.identifier {
            vc.repository = self.repository
        }
    }
    
    func close(_ sender: UITapGestureRecognizer) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func resend(_ sender: UITapGestureRecognizer) {
        self.presenter?.resend(repository: self.repository)
    }
    
    func confirm() {
        HUD.show(.progress)
        self.presenter?.confirm(repository: self.repository)
    }
}

extension MailConfirmViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.confirm()
    }
}

extension MailConfirmViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if (textField == self.codeField) {
            self.repository.code = textField.text ?? ""
            guard self.repository.code != "", self.repository.code?.count == DataConfig.Session.email_code_digits else {
                self.signupButton.canceled = true
                return
            }
            self.signupButton.canceled = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.codeField) {
            self.repository.code = textField.text ?? ""
            guard self.repository.code != "", self.repository.code?.count == DataConfig.Session.email_code_digits else {
                self.signupButton.canceled = true
                return true
            }
            self.signupButton.canceled = false
            textField.resignFirstResponder()
            self.confirm()
        }
        return true
    }
}

extension MailConfirmViewController: MailConfirmPresenterOutput {
    func sync(didFetchRepository repository: SessionModel) {
        self.repository = repository
        DispatchQueue.main.async {
            self.codeField.text = self.repository.code
        }
    }
    
    func mailConfirmPresenter(didResend mailConfirmPresenter: MailConfirmPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.success)
        }
    }
    
    func confirmed() {
        DispatchQueue.main.async {
            guard !self.mailConfirmed else { return }
            self.mailConfirmed = true
            HUD.hide()
            HUD.flash(.success)
            self.presenter?.toProfileSetting()
        }
    }
    
    func mailConfirmPresenter(onError error: Error) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
}
