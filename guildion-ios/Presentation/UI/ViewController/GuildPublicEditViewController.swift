//
//  GuildPublicEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class GuildPublicEditViewController: BaseFormViewController {
    var presenter: GuildPublicEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: GuildEntity = GuildEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: GuildEntity = GuildEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildPublicEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    func handleValueChange() {
        if repository.description == new_repository.description &&  repository.is_public == new_repository.is_public && repository.is_private == new_repository.is_private {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section(header: R.string.localizable.guildPublicTitle(), footer: "\(R.string.localizable.public()) :\n \(R.string.localizable.guildPublicSettingTitle())\n\n\(R.string.localizable.approvalSystem()) :\n \(R.string.localizable.guildApprovalSettingTitle())\n\n\(R.string.localizable.private()) :\n \(R.string.localizable.guildPrivateSettingTitle())")
            <<< SegmentedRow<String>() {
                $0.options = [R.string.localizable.public(), R.string.localizable.approvalSystem(), R.string.localizable.private()]
                if new_repository.is_public && !new_repository.is_private {
                    $0.value = R.string.localizable.public()
                } else if !new_repository.is_public && !new_repository.is_private {
                    $0.value = R.string.localizable.approvalSystem()
                } else if !new_repository.is_public && new_repository.is_private {
                    $0.value = R.string.localizable.private()
                }
            }.onChange { [unowned self] in
                if $0.value == R.string.localizable.public() {
                    self.new_repository.is_public = true
                    self.new_repository.is_private = false
                } else if $0.value == R.string.localizable.approvalSystem() {
                    self.new_repository.is_public = false
                    self.new_repository.is_private = false
                } else if $0.value == R.string.localizable.private() {
                    self.new_repository.is_public = false
                    self.new_repository.is_private = true
                }
                self.handleValueChange()
            }.cellUpdate { [unowned self] in self.segmentedRowCellUpdate($0, $1) }
            +++ Section(R.string.localizable.description())
            <<< TextAreaRow(R.string.localizable.description()) {
                var ruleSet = RuleSet<String>()
                ruleSet.add(rule: RuleRequired())
                $0.add(ruleSet: ruleSet)
                $0.placeholder = R.string.localizable.description()
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
                $0.value = new_repository.description
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.description = value
                self.handleValueChange()
            }.cellUpdate(textAreaRowCellUpdate(_:_:))
    }
}

extension GuildPublicEditViewController: GuildPublicEditPresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
    }
    
    func guildPublicEditPresenter(didUpdate repository: GuildEntity, in guildPublicEditPresenter: GuildPublicEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func guildPublicEditPresenter(onError error: Error, in guildPublicEditPresenter: GuildPublicEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
