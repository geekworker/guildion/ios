//
//  MemberSelectViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/13.
//

import Foundation
import UIKit
import PKHUD
import Nuke

protocol MemberSelectViewControllerDelegate: class {
    func memberSelectViewController(shouldDismiss selecteds: MemberEntities, in memberSelectViewController: MemberSelectViewController)
}

class MemberSelectViewController: BaseViewController {
    @IBOutlet weak var searchContainerView: UIView! {
        didSet {
            searchContainerView.backgroundColor = Theme.themify(key: .background)
        }
    }
    @IBOutlet weak var searchBar: SearchBar! {
        didSet {
            searchBar.placeholder = R.string.localizable.searchGuildPlaceholder()
            searchBar.buttonText = R.string.localizable.cancel()
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var tableView: UITableView!
    public var selecteds: [MemberEntity] = []
    public var is_invite: Bool = false
    public weak var delegate: MemberSelectViewControllerDelegate?
    
    var presenter: MemberSelectPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var _repositories: MemberEntities {
        guard searchBar != nil, let keyword = searchBar.keyword else { return repositories }
        let entities = MemberEntities()
        entities.items = keyword.count == 0 ? repositories.items : repositories.items.filter({ $0.nickname.contains(keyword) })
        entities.items = entities.items.filter({ $0.UserId != CurrentUser.getCurrentUserEntity()?.id })
        return entities
    }
    var repositories: MemberEntities = MemberEntities()
    var repository: ChannelableProtocol = TextChannelEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        MemberSelectViewControllerBuilder.rebuild(self)
        configureTableView()
        self.navigationItem.title = R.string.localizable.selectItems("\(selecteds.count)")
        navigationMode = .edit
        if is_invite {
            self.presenter?.fetchChannelInvitableMembers(repository)
        } else {
        }
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.view.backgroundColor = Theme.themify(key: .background)
        self.setEditing(true, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.allowsMultipleSelection = editing
        guard let indexPaths = self.tableView.indexPathsForVisibleRows else { return }
        for indexPath in indexPaths {
            if let cell = self.tableView.cellForRow(at: indexPath) as? MemberSelectTableViewCell {
                cell.setEditing(editing, animated: animated)
            }
            if !editing {
                self.tableView.deselectRow(at: indexPath, animated: false)
            }
        }
        if !editing {
            let entities = MemberEntities()
            entities.items = selecteds.unique
            entities.items = entities.items.filter({ $0.id != nil })
            self.delegate?.memberSelectViewController(
                shouldDismiss: entities,
                in: self
            )
            self.dismissShouldAppear(animated: true)
        }
    }
}

extension MemberSelectViewController: UITableViewDelegate {
    fileprivate func configureTableView() {
        self.tableView.register([
            UINib(resource: R.nib.memberSelectTableViewCell): R.nib.memberSelectTableViewCell.name,
        ])
        self.tableView.backgroundColor = Theme.themify(key: .background)
        self.tableView.rowHeight = 48
        self.tableView.alwaysBounceVertical = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard isEditing, let repository = _repositories[safe: indexPath.row] else { return }
        if self.selecteds.contains(repository) {
            self.selecteds.remove(element: repository)
            self.selecteds = self.selecteds.unique
            self.navigationItem.title = R.string.localizable.selectItems("\(selecteds.count)")
        } else {
            self.selecteds.append(repository)
            self.selecteds = self.selecteds.unique
            self.navigationItem.title = R.string.localizable.selectItems("\(selecteds.count)")
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.tableView.reloadData()
    }
}

extension MemberSelectViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.memberSelectTableViewCell.name, for: indexPath) as? MemberSelectTableViewCell, let repository = _repositories[safe: indexPath.row] else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.setEditing(true, animated: false)
        cell.isInEditingMode = true
        cell.checkImageView.image = selecteds.contains(repository) ? R.image.success()!.withRenderingMode(.alwaysOriginal) : UIImage()
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.memberSelectTableViewCell.name, for: indexPath) as? MemberSelectTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setEditing(true, animated: false)
    }
}

extension MemberSelectViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row], let url = URL(string: repository.picture_small) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}
extension MemberSelectViewController: SearchBarDelegate {
    func searchBarDidChange(_ searchBar: SearchBar, keyword: String?) {
        self.tableView.reloadData()
    }
    
    func searchBarDidEndEditing() {
        self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: SearchBar, accessoryButtonTapped keyword: String?) -> Bool {
        self.tableView.reloadData()
        return true
    }
}

extension MemberSelectViewController: MemberSelectPresenterOutput {
    func sync(didFetchRepositories repositories: MemberEntities) {
        self.repositories = repositories
        self.repositories.items = repositories.items.filter({ $0.UserId != CurrentUser.getCurrentUserEntity()?.id })
        self.tableView.reloadData()
    }
}
