//
//  RoleEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/14.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class RoleEditViewController: BaseFormViewController {
    var presenter: RoleEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: RoleEntity = RoleEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: RoleEntity = RoleEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        RoleEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismissShouldAppear(animated: true)
    }
    
    func handleValueChange() {
        if repository.name == new_repository.name &&
            repository.color == new_repository.color &&
            repository.admin == new_repository.admin &&
            repository.priority == new_repository.priority &&
            repository.guild_managable == new_repository.guild_managable &&
            repository.channels_managable == new_repository.channels_managable &&
            repository.roles_managable == new_repository.roles_managable &&
            repository.member_acceptable == new_repository.member_acceptable &&
            repository.channels_viewable == new_repository.channels_viewable &&
            repository.message_sendable == new_repository.message_sendable &&
            repository.messages_managable == new_repository.messages_managable &&
            repository.message_attachable == new_repository.message_attachable &&
            repository.messages_readable == new_repository.messages_readable &&
            repository.message_reactionable == new_repository.message_reactionable &&
            repository.folders_viewable == new_repository.folders_viewable &&
            repository.folder_creatable == new_repository.folder_creatable &&
            repository.folders_managable == new_repository.folders_managable &&
            repository.files_viewable == new_repository.files_viewable &&
            repository.file_creatable == new_repository.file_creatable &&
            repository.files_managable == new_repository.files_managable &&
            repository.channels_invitable == new_repository.channels_invitable &&
            repository.kickable == new_repository.kickable &&
            repository.banable == new_repository.banable {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        form +++
            Section()
            
        if repository.is_default {
            form
                +++ Section(R.string.localizable.roleName())
                <<< ButtonRow(R.string.localizable.name()) {
                    $0.title = new_repository.name
                }.cellUpdate(buttonRowLeftAlignmentUpdate(_:_:))
                
                +++ Section(R.string.localizable.rolePriority())
                <<< ButtonRow(R.string.localizable.rolePriority()) {
                    $0.title = "\(new_repository.priority)"
                }.cellUpdate(buttonRowLeftAlignmentUpdate(_:_:))
            
        } else {
            form
                +++ Section(R.string.localizable.roleName())
                <<< TextRow(R.string.localizable.name()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.value = new_repository.name
                    $0.placeholder = $0.tag
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.name = value
                    self.handleValueChange()
                }.cellUpdate(textRowCellUpdate(_:_:))
                
                +++ Section(R.string.localizable.rolePriority())
                <<< SliderRow(R.string.localizable.rolePriority()) {
                    $0.title = $0.tag
                    $0.value = Float(new_repository.priority)
                    $0.cell.slider.maximumValue = Float(DataConfig.Role.max_priority_limit)
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.priority = Int(value)
                    $0.value = Float(Int(value))
                    self.handleValueChange()
                }.cellUpdate { [unowned self] in
                    self.sliderRowCellUpdate($0, $1)
                    $0.slider.isContinuous = false
                    $0.slider.maximumValue = Float(DataConfig.Role.max_priority_limit)
                }
        }
        
        form
        
        
            +++ Section(header: "", footer: R.string.localizable.roleAdminDesc())
            <<< SwitchRow(R.string.localizable.roleAdmin()) {
                $0.title = $0.tag
                $0.value = new_repository.admin
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.admin = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleGuildManageableDesc())
            <<< SwitchRow(R.string.localizable.roleGuildManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.guild_managable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.guild_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleRoleManageableDesc())
            <<< SwitchRow(R.string.localizable.roleRoleManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.roles_managable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.roles_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleChannelsManageableDesc())
            <<< SwitchRow(R.string.localizable.roleChannelsManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.channels_managable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.channels_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleMemberAcceptableDesc())
            <<< SwitchRow(R.string.localizable.roleMemberAcceptable()) {
                $0.title = $0.tag
                $0.value = new_repository.member_acceptable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.member_acceptable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section()
            <<< SwitchRow(R.string.localizable.roleChannelInvitable()) {
                $0.title = $0.tag
                $0.value = new_repository.channels_invitable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.channels_invitable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleKickable()) {
                $0.title = $0.tag
                $0.value = new_repository.kickable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.kickable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleBanable()) {
                $0.title = $0.tag
                $0.value = new_repository.banable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.banable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
            +++ Section(R.string.localizable.roleMessageManageableDesc())
            <<< SwitchRow(R.string.localizable.roleMessageManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_managable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.messages_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section()
            <<< SwitchRow(R.string.localizable.roleMessageSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_sendable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.message_sendable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReadable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_readable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.messages_readable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageAttachable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_attachable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.message_attachable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReactionSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_reactionable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.message_reactionable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
            +++ Section()
            <<< SwitchRow(R.string.localizable.roleStreamConnctable()) {
                $0.title = $0.tag
                $0.value = new_repository.stream_connectable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.stream_connectable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleStreamSpeekable()) {
                $0.title = $0.tag
                $0.value = new_repository.stream_speekable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.stream_speekable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section()
            <<< SwitchRow(R.string.localizable.roleFolderManagable()) {
                $0.title = $0.tag
                $0.value = new_repository.folders_managable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.folders_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleFoldersViewable()) {
                $0.title = $0.tag
                $0.value = new_repository.folders_viewable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.folders_viewable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleFolderCreatable()) {
                $0.title = $0.tag
                $0.value = new_repository.folder_creatable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.folder_creatable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
            +++ Section()
            <<< SwitchRow(R.string.localizable.roleFileManagable()) {
                $0.title = $0.tag
                $0.value = new_repository.files_managable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.files_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleFilesViewable()) {
                $0.title = $0.tag
                $0.value = new_repository.files_viewable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.files_viewable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleFileCreatable()) {
                $0.title = $0.tag
                $0.value = new_repository.file_creatable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.file_creatable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
        if !repository.is_default {
            form
                +++ Section()
                <<< ButtonRow(R.string.localizable.guildMenuDelete()) {
                    $0.title = $0.tag
                    $0.onCellHighlightChanged({ _, _ in })
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: R.string.localizable.roleDelete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                                
                    let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.roleDelete(), style: UIAlertAction.Style.default, handler:{
                        [unowned self](action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                         self.presenter?.delete(repository: self.repository)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
}

extension RoleEditViewController: RoleEditPresenterOutput {
    func roleEditPresenter(didUpdate repository: RoleEntity, in roleEditPresenter: RoleEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func roleEditPresenter(didDelete repository: RoleEntity, in roleEditPresenter: RoleEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func roleEditPresenter(onError error: Error, in roleEditPresenter: RoleEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func sync(didFetchRepository repository: RoleEntity) {
        self.repository = repository
        self.tableView.reloadData()
    }
}
