//
//  FileMenuViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/10.
//

import Foundation
import UIKit
import PanModal
import Nuke
import SwiftMessages
import Photos
import PKHUD

protocol FileMenuViewControllerDelegate: class {
    func toImageViewer(repository: FileEntity)
    func showReactionPicker(at repository: MessageEntity)
    func toWebBrowser(url: URL)
}

extension FileMenuViewControllerDelegate {
    func toImageViewer(repository: FileEntity) {}
    func showReactionPicker(at repository: MessageEntity) {}
    func toWebBrowser(url: URL) {}
}

class FileMenuViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var fileView: UIView! {
        didSet {
            fileView.layer.shadowColor = Theme.themify(key: .shadow).cgColor
            fileView.layer.shadowOffset = CGSize(width: 1, height: 1)
            fileView.layer.cornerRadius = 3
            fileView.clipsToBounds = true
            self.fileView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapFileView(_:))))
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
            nameLabel.font = Font.FT(size: .FM, family: .L)
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Theme.themify(key: .stringSecondary)
            descriptionLabel.font = Font.FT(size: .S, family: .L)
        }
    }
    @IBOutlet weak var fileImageView: UIImageView! {
        didSet {
            fileImageView.contentMode = .scaleAspectFill
            fileImageView.backgroundColor = Color.black.darker(by: 5)
        }
    }
    @IBOutlet weak var lockedImageView: UIImageView! {
        didSet {
            lockedImageView.image = R.image.locked()?.withRenderingMode(.alwaysTemplate)
            lockedImageView.tintColor = Theme.themify(key: .warning)
            lockedImageView.isHidden = true
        }
    }
    @IBOutlet weak var gradationView: GradationView! {
        didSet {
            let angle = 180 * CGFloat.pi / 180.0;
            gradationView.transform = CGAffineTransform(rotationAngle: angle)
            gradationView.setGradation()
        }
    }
    @IBOutlet weak var fileImageAspectConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var formatImage: UIImageView!
    @IBOutlet weak var ownerImage: UIImageView! {
        didSet {
            ownerImage.layer.cornerRadius = ownerImage.frame.width / 2
            ownerImage.clipsToBounds = true
            ownerImage.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var headerView: UIView! {
        didSet {
            headerView.backgroundColor = Theme.themify(key: .backgroundThick)
            headerView.layer.cornerRadius = 8
            headerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var collectionView: MenuCollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    public lazy var activityHandler: ActivityHandler = {
        let activityHandler = ActivityHandler()
        activityHandler.delegate = self
        return activityHandler
    }()
    var menus: [MenuCollectionViewCell.MenuType] = [
        .copyLink, .share, .watchParty, .play, .deleteMessage
    ]
    var presenter: FileMenuPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: FileEntity = FileEntity()
    var permission: PermissionModel = PermissionModel()
    var current_guild: GuildEntity = GuildEntity()
    var current_member: MemberEntity = MemberEntity()
    var reference: FileReferenceEntity?
    var channelogMessage: MessageEntity?
    public var previewMode: Bool = false
    weak var delegate: FileMenuViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        FileMenuViewControllerBuilder.rebuild(self)
        evalMenus()
        configureCollectionView()
        self.updateLayout()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        if let id = repository.id, id != 0 {
            presenter?.fetch(repository)
        }
        view.backgroundColor = Theme.themify(key: .backgroundLight)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func initRepository(repository: FileEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        self.repository = repository
        self.current_guild = current_guild
        self.current_member = current_member
        self.permission = permission
        self.evalMenus()
        if isViewLoaded {
            self.updateLayout()
        }
    }
    
    func setRepository(repository: FileEntity) {
        self.repository = repository
        if isViewLoaded {
            self.updateLayout()
        }
    }
    
    func setChannelog(repository: MessageEntity, current_guild: GuildEntity, current_member: MemberEntity) {
        guard let channelog = repository.Channelog, channelog.id != 0 && channelog.id != nil, channelog.channelogable_type == .Stream, let stream = channelog.Stream else { return }
        self.channelogMessage = repository
        switch stream.streamable_type {
        case .File: self.repository = stream.File ?? FileEntity()
        case .FileReference:
            self.reference = stream.FileReference
            self.repository = stream.FileReference?.File ?? FileEntity()
        }
        if isViewLoaded {
            self.updateLayout()
        }
    }
    
    func updateLayout() {
        self.nameLabel.text = repository.name
        self.descriptionLabel.text = "\(R.string.localizable.size()) : \(repository.size)"
        self.formatImage.image = repository.format.iconImage
        self.lockedImageView.isHidden = !repository.is_private
        if let picture_small = repository.Member?.picture_small, let url = URL(string: picture_small) {
            Nuke.loadImage(with: url, into: ownerImage)
        }

        if let url = URL(string: repository.thumbnail) {
            Nuke.loadImage(with: url, into: fileImageView)
        } else {
            fileImageView.image = nil
        }

        if repository.format.content_type.is_movie {
            self.formatImage.tintColor = .white
            self.fileImageView.contentMode = .scaleAspectFill
            self.durationLabel.text = repository.formattedDuration
            self.durationLabel.isHidden = false
        }

        if repository.format == .youtube {
            self.durationLabel.text = repository.formattedDuration
            self.fileImageView.contentMode = .scaleAspectFill
            self.durationLabel.isHidden = false
        }

        if repository.format.content_type.is_image {
            self.formatImage.tintColor = .white
            self.fileImageView.contentMode = .scaleAspectFit
            self.durationLabel.isHidden = true
        }
    }
    
    func evalMenus() {
        guard let id = repository.id, id != 0 else { self.menus = []; return }
        
        if let channelog = channelogMessage?.Channelog, channelog.id != 0 && channelog.id != nil, channelog.channelogable_type == .Stream {
            self.evalStreamChannelogMenu()
            return
        }
        
        if repository.format.content_type.is_image {
            self.evalImageMenus()
        } else if repository.format.content_type.is_movie {
            previewMode ? self.evalPreviewMovieMenus() : self.evalMovieMenus()
        } else if repository.provider == .youtube {
            self.evalYouTubeMenus()
        }
        
        if current_member.id == nil || current_guild.id == nil {
            self.menus = self.menus.filter({ $0 != .play && $0 != .watchParty })
        }
    }
    
    fileprivate func evalStreamChannelogMenu() {
        guard let channelog = channelogMessage?.Channelog, channelog.id != 0 && channelog.id != nil, channelog.channelogable_type == .Stream, let stream = channelog.Stream else { return }
        
        switch stream.streamable_type {
        case .File:
            guard let file = stream.File else { break }
            self.menus = [
                file.provider == .youtube ? .youtubeLink : .none, GuildConnector.shared.current_permission.StreamChannel.message_reactionable ? .reactions : .none, .play, .deleteMessage, .none
            ]
        case .FileReference:
            guard let file = stream.FileReference?.File else { break }
            self.menus = [
                file.provider == .youtube ? .youtubeLink : .none, GuildConnector.shared.current_permission.StreamChannel.message_reactionable ? .reactions : .none, .play, .deleteMessage, .none
            ]
        }
    }
    
    fileprivate func evalImageMenus() {
        let isOwner = repository.MemberId == self.current_member.id
        
        if isOwner || permission.Guild.files_managable {
            self.menus = [
                .save, .share, .preview, .editMessage, .deleteMessage
            ]
        } else {
            self.menus = [
                .save, .share, .preview
            ]
        }
    }
    
    fileprivate func evalMovieMenus() {
        let isOwner = repository.MemberId == self.current_member.id
        
        if isOwner || permission.Guild.files_managable {
            self.menus = [
                .save, .share, .play, .editMessage, .deleteMessage
            ]
        } else {
            self.menus = [
                .save, .share, .play
            ]
        }
    }
    
    fileprivate func evalPreviewMovieMenus() {
        let isOwner = repository.MemberId == current_member.id
        
        if isOwner || permission.Guild.files_managable {
            self.menus = [
                .save, .preview, .editMessage, .deleteMessage
            ]
        } else {
            self.menus = [
                .save, .share, .preview
            ]
        }
    }
    
    fileprivate func evalYouTubeMenus() {
        let isOwner = repository.MemberId == current_member.id
        
        if isOwner || permission.Guild.files_managable {
            self.menus = [
                .copyLink, .youtubeLink, .play, .editMessage, .deleteMessage
            ]
        } else {
            self.menus = [
                .copyLink, .youtubeLink, .play
            ]
        }
    }
    
    @objc func onTapFileView(_ sender: Any) {
        guard repository.format.content_type.is_image else { return }
        self.dismiss(animated: true, completion: {
            self.delegate?.toImageViewer(repository: self.repository)
        })
    }
    
    @objc func showResultOfSaveImage(_ image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutableRawPointer) {
        if let unwrapped = error {
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.error)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.error(), body: String(describing: unwrapped))
            messageAlert.button?.isHidden = true
            SwiftMessages.show(config: SwiftMessages.defaultConfig, view: messageAlert)
        } else {
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.success)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successSave())
            messageAlert.button?.isHidden = true
            SwiftMessages.show(config: SwiftMessages.defaultConfig, view: messageAlert)
        }
    }
}

extension FileMenuViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        self.collectionView.configure()
        self.collectionView.setRepositories(menus)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionViewHeightConstraint.constant = self.collectionView.calcHeight()
    }
}

extension FileMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionView.collectionView(numberOfItemsInSection: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.collectionView.collectionView(cellForItemAt: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.collectionView.collectionView(didSelectItemAt: indexPath) {
        case .save:
            if repository.format.content_type.is_image {
                UIImageWriteToSavedPhotosAlbum(self.fileImageView.image!, self, #selector(self.showResultOfSaveImage(_:didFinishSavingWithError:contextInfo:)), nil)
            } else if repository.format.content_type.is_movie {
                HUD.show(.progress)
                DispatchQueue.global(qos: .background).async {
                    if let url = URL(string: self.repository.url), let urlData = NSData(contentsOf: url) {
                        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                        let filePath="\(documentsPath)/\(UUID().uuidString).\(self.repository.format.content_type.file_extension)"
                        DispatchQueue.main.async {
                            urlData.write(toFile: filePath, atomically: true)
                            PHPhotoLibrary.shared().performChanges({
                                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                            }) { completed, error in
                                DispatchQueue.main.async {
                                    if completed {
                                        HUD.hide()
                                        HUD.flash(.success)
                                    } else {
                                        HUD.hide()
                                        HUD.flash(.error)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        case .preview:
            self.dismiss(animated: true, completion: {
                self.delegate?.toImageViewer(repository: self.repository)
            })
        case .share: self.activityHandler.show()
        case .play:
            if let channelog = channelogMessage?.Channelog, channelog.id != 0 && channelog.id != nil, channelog.channelogable_type == .Stream {
                self.playChannelog()
                return
            }
            if LiveConfig.playingStreamChannelViewController != nil {
                self.playFile()
                return
            }
            self.presenter?.startPrivateStreamChannel(repository: reference ?? repository)
        case .watchParty: self.presenter?.startStreamChannel(repository: reference ?? repository)
        case .editMessage: self.presenter?.toFileEdit()
        case .reactions:
            guard let message = self.channelogMessage else { return }
            self.dismiss(animated: true, completion: {
                self.delegate?.showReactionPicker(at: message)
            })
        case .deleteMessage:
            if let channelogMessage = self.channelogMessage {
                self.presenter?.delete(channelogMessage)
                return
            }
            if let unwrapped = self.reference {
                let entities = FileReferenceEntities()
                entities.items = [unwrapped]
                self.presenter?.deleteFileReferences(repositories: entities)
                return
            }
            let entities = FileEntities()
            entities.items = [repository]
            self.presenter?.deleteFiles(repositories: entities)
        case .copyLink:
            self.presenter?.copy_url(repository)
            self.dismissShouldAppear(animated: true, completion: {
                let messageAlert = MessageView.viewFromNib(layout: .cardView)
                messageAlert.configureTheme(.success)
                messageAlert.configureDropShadow()
                messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successCopy())
                messageAlert.button?.isHidden = true
                SwiftMessages.show(config: SwiftMessages.defaultConfig, view: messageAlert)
            })
        case .youtubeLink:
            if let channelog = channelogMessage?.Channelog, channelog.id != 0 && channelog.id != nil, channelog.channelogable_type == .Stream {
                self.openBrowserFromChannelog()
                return
            }
            self.openBrowser(repository.url)
        default: break
        }
    }
    
    fileprivate func playChannelog() {
        guard let channelog = channelogMessage?.Channelog, channelog.id != 0 && channelog.id != nil, channelog.channelogable_type == .Stream, let stream = channelog.Stream, let vc = LiveConfig.playingStreamChannelViewController else { return }
        switch stream.streamable_type {
        case .File:
            guard let file = stream.File else { break }
            vc.start(file)
            self.dismiss(animated: true)
        case .FileReference:
            guard let reference = stream.FileReference else { break }
            vc.start(reference)
            self.dismiss(animated: true)
        }
    }
    
    fileprivate func playFile() {
        guard let vc = LiveConfig.playingStreamChannelViewController else { return }
        vc.start(repository)
        self.dismiss(animated: true)
    }
    
    fileprivate func openBrowserFromChannelog() {
        guard let channelog = channelogMessage?.Channelog, channelog.id != 0 && channelog.id != nil, channelog.channelogable_type == .Stream, let stream = channelog.Stream else { return }
        switch stream.streamable_type {
        case .File:
            guard let file = stream.File else { break }
            self.openBrowser(file.url)
        case .FileReference:
            guard let file = stream.FileReference?.File else { break }
            self.openBrowser(file.url)
        }
    }
    
    fileprivate func openBrowser(_ url: String) {
        guard let url = URL(string: url) else { return }
        if let vc = LiveConfig.playingStreamChannelViewController {
            self.dismiss(animated: true, completion: {
                vc.toWebBrowser(url)
            })
        } else {
            self.dismiss(animated: true, completion: {
                self.delegate?.toWebBrowser(url: url)
            })
        }
    }
}

extension FileMenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MenuCollectionViewCell.cellSize
    }
}

extension FileMenuViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return scrollView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(260)
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    var allowsExtendedPanScrolling: Bool { return true }
}

extension FileMenuViewController: ActivityHandlerDelegate {
    var activityItems: [Any] {
        [repository.provider == .youtube ? URL(string: repository.url)! : URL(string: "\(Constant.APP_URL)/guild/\(self.current_guild.uid)/file/\(repository.uid)")!]
    }
    
    var activityParent: UIViewController {
        return self
    }
}

extension FileMenuViewController: FileMenuPresenterOutput {
    func fileMenuPresenter(onError error: Error, in fileMenuPresenter: FileMenuPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func fileMenuPresenter(didDelete fileMenuPresenter: FileMenuPresenter) {
        DispatchQueue.main.async {
            self.dismissShouldAppear(animated: true)
        }
    }
    
    func fileMenuPresenter(willToStreamChannel repository: StreamChannelEntity, in fileMenuPresenter: FileMenuPresenter) {
        DispatchQueue.main.async {
            self.presenter?.toStreamChannel(repository: repository)
        }
    }
    
    func sync(didFetchRepository repository: FileEntity) {
        DispatchQueue.main.async {
            self.evalMenus()
            self.setRepository(repository: repository)
        }
    }
}

