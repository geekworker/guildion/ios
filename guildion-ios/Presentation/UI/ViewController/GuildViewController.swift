//
//  GuildViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import UIKit
import ReSwift
import Nuke
import Rswift
import SideMenu

protocol GuildViewControllerDelegate: class {
    func guildViewControllerDidScroll(_: GuildViewController)
    func guildViewController(didSelectChannel channel: ChannelableProtocol, in guildViewController: GuildViewController)
    func guildViewController(didSelectOpenSetting repository: GuildEntity, guildViewController: GuildViewController)
    func guildViewController(didSelectMemberRequests repository: GuildEntity, guildViewController: GuildViewController)
    func guildViewController(didSelectSectionRow row: SectionChannelRowable, in guildViewController: GuildViewController)
    func guildViewController(didSelectFolders guildViewController: GuildViewController)
    func guildViewController(didSelectPlaylists guildViewController: GuildViewController)
    func guildViewController(didSelectMembers repository: GuildEntity, guildViewController: GuildViewController)
    func guildViewControllerWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>)
}

extension GuildViewControllerDelegate {
    func guildViewControllerDidScroll(_: GuildViewController) {}
    func guildViewControllerWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {}
}

class GuildViewController: BaseViewController {
    @IBOutlet weak var currentMemberView: CurrentMemberView! {
        didSet {
            currentMemberView.delegate = self
            currentMemberView.isHidden = true
        }
    }
    @IBOutlet weak var tableView: UITableView!
    var presenter: GuildPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public weak var delegate: GuildViewControllerDelegate?
    public var repository: GuildEntity = GuildEntity()
    public var current_member: MemberEntity = MemberEntity()
    public var permission: PermissionModel = PermissionModel()
    public var viewAppeared: Bool = false
    private var fetched: Bool = false
    private var reloaded: Bool = false
    private var sections: [Section] = [
        .header,
        .invite,
        .currentMember,
        .playlists,
        .channels
    ]
    private var section_count_other_channels: Int {
        sections.contains(.channels) ? sections.count - Section.channels.count : sections.count
    }
    
    enum Section: Int {
        case header
        case invite
        case openSetting
        case currentMember
        case members
        case memberRequests
        case folders
        case playlists
        case channels
        case count
        
        var count: Int {
            switch self {
            case .header: return HeaderRow.count.rawValue
            case .invite: return InviteRow.count.rawValue
            case .openSetting: return OpenSettingRow.count.rawValue
            case .memberRequests: return MemberRequestRow.count.rawValue
            case .currentMember: return CurrentMemberRow.count.rawValue
            case .members: return MembersRow.count.rawValue
            case .folders: return FoldersRow.count.rawValue
            case .playlists: return PlaylistsRow.count.rawValue
            case .channels: return 1
            case .count: fatalError("This row is not exists")
            }
        }
        
        func getCount(_ repositories: SectionChannelEntities) -> Int {
            switch self {
            case .header: return HeaderRow.count.rawValue
            case .invite: return InviteRow.count.rawValue
            case .openSetting: return OpenSettingRow.count.rawValue
            case .memberRequests: return MemberRequestRow.count.rawValue
            case .currentMember: return CurrentMemberRow.count.rawValue
            case .members: return MembersRow.count.rawValue
            case .folders: return FoldersRow.count.rawValue
            case .playlists: return PlaylistsRow.count.rawValue
            case .channels: return repositories.count
            case .count: fatalError("This row is not exists")
            }
        }
        
    }
    
    enum HeaderRow: Int {
        case content
        case count
    }
    
    enum InviteRow: Int {
        case content
        case count
    }
    
    enum OpenSettingRow: Int {
        case content
        case count
    }
    
    enum MemberRequestRow: Int {
        case content
        case count
    }
    
    enum CurrentMemberRow: Int {
        case content
        case count
    }
    
    enum MembersRow: Int {
        case content
        case count
    }
    
    enum FoldersRow: Int {
        case content
        case count
    }
    
    enum PlaylistsRow: Int {
        case content
        case count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GuildViewControllerBuilder.rebuild(self)
        self.view.backgroundColor = Theme.themify(key: .backgroundLight)
        evalSections()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewAppeared = true
        SideMenuManager.default.rightMenuNavigationController = nil
    }
    
    override func viewWebSocketHealthChecked(_ health: Bool) {
        super.viewWebSocketHealthChecked(health)
        self.presenter?.fetch(uid: self.repository.uid)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter?.joinChildRoom()
        self.viewAppeared = false
        //presenter?.unbind()
    }
    
    public func leave() {
        self.presenter?.joinChildRoom()
    }
    
    public func setRepository(_ repository: GuildEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.repository = repository
                self.evalSections()
                self.tableView.reloadData()
            }
        }
    }
    
    public func initRepository(_ repository: GuildEntity) {
        self.fetched = false
        self.repository = repository
    }
    
    public func evalSections() {
        if repository.id == nil || repository.id == 0 || repository.SectionChannels == nil || repository.SectionChannels?.count == 0 {
            self.sections = []
            return
        }
        
        self.sections = [
            .header,
            .invite,
            .currentMember,
            .members,
            .playlists,
            .channels
        ]
        
        if self.permission.Guild.member_acceptable && repository.member_request_count > 0 {
            self.sections.insert(.memberRequests, at: (sections.firstIndex(of: .header) ?? 0) + 1)
        }
        
        if self.permission.Guild.guild_managable && !repository.is_private && (repository.description == "" || repository.CategoryId == nil || repository.CategoryId == 0) {
            self.sections.insert(.openSetting, at: (sections.firstIndex(of: .header) ?? 0) + 1)
        }
        
        if !self.permission.Guild.files_viewable {
            self.sections = self.sections.filter({ $0 != .playlists && $0 != .folders })
        }
        
        if !self.permission.Guild.messages_readable {
            self.sections = self.sections.filter({ $0 != .channels })
        }
    }
}

extension GuildViewController: UITableViewDelegate, UITableViewDataSource {
    private func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.backgroundColor = Theme.themify(key: .backgroundLight)
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        let insets = UIEdgeInsets(top: ImagePagingCell.menuHeight, left: 0, bottom: 0, right: 0)
        self.tableView.contentInset = insets
        self.tableView.scrollIndicatorInsets = insets
        self.tableView.register([
            UINib(resource: R.nib.simpleButtonTableViewCell): R.nib.simpleButtonTableViewCell.name,
            UINib(resource: R.nib.membersTableViewCell): R.nib.membersTableViewCell.name,
            UINib(resource: R.nib.foldersTableViewCell): R.nib.foldersTableViewCell.name,
            UINib(resource: R.nib.playlistsTableViewCell): R.nib.playlistsTableViewCell.name,
            UINib(resource: R.nib.currentMemberTableViewCell): R.nib.currentMemberTableViewCell.name,
            UINib(resource: R.nib.guildHeaderTableViewCell): R.nib.guildHeaderTableViewCell.name,
            UINib(resource: R.nib.textChannelTableViewCell): R.nib.textChannelTableViewCell.name,
            UINib(resource: R.nib.dmChannelTableViewCell): R.nib.dmChannelTableViewCell.name,
            UINib(resource: R.nib.streamChannelTableViewCell): R.nib.streamChannelTableViewCell.name,
            UINib(resource: R.nib.streamTableViewCell): R.nib.streamTableViewCell.name,
            UINib(resource: R.nib.participantTableViewCell): R.nib.participantTableViewCell.name,
            UINib(resource: R.nib.navigationTableViewCell): R.nib.navigationTableViewCell.name,
        ])
        self.tableView.register(
            UINib(resource: R.nib.sectionChannelTableViewHeaderFooterView),
            forHeaderFooterViewReuseIdentifier: R.nib.sectionChannelTableViewHeaderFooterView.name
        )
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return section_count_other_channels + (repository.SectionChannels?.count ?? 0)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let section = sections[safe: section] {
            switch section {
            case .header: return Section.header.count
            case .invite: return Section.invite.count
            case .openSetting: return Section.openSetting.count
            case .memberRequests: return Section.memberRequests.count
            case .members: return Section.members.count
            case .currentMember: return Section.currentMember.count
            case .folders: return Section.folders.count
            case .playlists: return Section.playlists.count
            default: break
            }
        }
        
        if let channel = repository.SectionChannels?[safe: section - section_count_other_channels] {
            return channel.collapsed ? 0 : channel.rows.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let section = sections[safe: indexPath.section] {
            switch section {
            case .header: return self.tableView(tableView, cellForGuildHeaderRowAt: indexPath)
            case .invite: return self.tableView(tableView, cellForInviteRowAt: indexPath)
            case .openSetting: return self.tableView(tableView, cellForOpenSettingRowAt: indexPath)
            case .memberRequests: return self.tableView(tableView, cellForMemberRequestRowAt: indexPath)
            case .currentMember: return self.tableView(tableView, cellForCurrentMemberRowAt: indexPath)
            case .members: return self.tableView(tableView, cellForMembersRowAt: indexPath)
            case .folders: return self.tableView(tableView, cellForFoldersRowAt: indexPath)
            case .playlists: return self.tableView(tableView, cellForPlaylistsRowAt: indexPath)
            default: break
            }
        }
        
        guard let section = repository.SectionChannels?[safe: indexPath.section - section_count_other_channels], let row = section.rows[safe: indexPath.row] else {
            return UITableViewCell()
        }
        switch row.sectionChannelRowType {
        case .StreamChannel:
            guard let repository = row as? StreamChannelEntity else { return UITableViewCell() }
            return self.tableView(tableView, cellForStreamChannelRowAt: indexPath, repository: repository)
        case .TextChannel:
            guard let repository = row as? TextChannelEntity else { return UITableViewCell() }
            return self.tableView(tableView, cellForTextChannelRowAt: indexPath, repository: repository)
        case .DMChannel:
            guard let repository = row as? DMChannelEntity else { return UITableViewCell() }
            return self.tableView(tableView, cellForDMChannelRowAt: indexPath, repository: repository)
        case .Participant:
            guard let repository = row as? ParticipantEntity else { return UITableViewCell() }
            return self.tableView(tableView, cellForParticipantRowAt: indexPath, repository: repository)
        case .Stream:
            guard let repository = row as? StreamEntity else { return UITableViewCell() }
            return self.tableView(tableView, cellForStreamRowAt: indexPath, repository: repository)
        }
    }
    
    private func tableView(_ tableView: UITableView, cellForGuildHeaderRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.guildHeaderTableViewCell.name, for: indexPath) as? GuildHeaderTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForCurrentMemberRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.currentMemberTableViewCell.name, for: indexPath) as? CurrentMemberTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.setRepository(current_member)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForInviteRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.simpleButtonTableViewCell.name, for: indexPath) as? SimpleButtonTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.simpleButton.text = "\(R.string.localizable.invite())"
        cell.simpleButton.isIconShow = true
        cell.simpleButton.iconImageView.image = R.image.plus()?.withRenderingMode(.alwaysTemplate)
        cell.simpleButton.normalColor = Theme.themify(key: .main)
        cell.topInset.constant = 4
        cell.bottomInset.constant = 12
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForFoldersRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.foldersTableViewCell.name, for: indexPath) as? FoldersTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForPlaylistsRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.playlistsTableViewCell.name, for: indexPath) as? PlaylistsTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.nameLabel.text = "\(repository.playlist_count) \(R.string.localizable.playlists()), \(repository.movie_count) \(R.string.localizable.movies())"
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForMembersRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.membersTableViewCell.name, for: indexPath) as? MembersTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundLight)
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForOpenSettingRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.navigationTableViewCell.name, for: indexPath) as? NavigationTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.topInset.constant = 4
        cell.bottomInset.constant = 12
        cell.titleLabel.text = R.string.localizable.guildShouldSetInfo()
        cell.badgeText = "!"
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForMemberRequestRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.navigationTableViewCell.name, for: indexPath) as? NavigationTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.topInset.constant = 4
        cell.bottomInset.constant = 12
        cell.titleLabel.text = R.string.localizable.guildMemberRequests()
        cell.badgeText = "\(repository.member_request_count)"
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForTextChannelRowAt indexPath: IndexPath, repository: TextChannelEntity) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.textChannelTableViewCell.name, for: indexPath) as? TextChannelTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForDMChannelRowAt indexPath: IndexPath, repository: DMChannelEntity) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.dmChannelTableViewCell.name, for: indexPath) as? DMChannelTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForStreamChannelRowAt indexPath: IndexPath, repository: StreamChannelEntity) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.streamChannelTableViewCell.name, for: indexPath) as? StreamChannelTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForStreamRowAt indexPath: IndexPath, repository: StreamEntity) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.streamTableViewCell.name, for: indexPath) as? StreamTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForParticipantRowAt indexPath: IndexPath, repository: ParticipantEntity) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.participantTableViewCell.name, for: indexPath) as? ParticipantTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let section = sections[safe: section] {
            switch section {
            case .header, .invite, .openSetting, .memberRequests, .currentMember, .members, .folders, .playlists: return 0
            default: break
            }
        }
        guard let row = repository.SectionChannels?[safe: section - section_count_other_channels] else {
            fatalError("This row is not exist")
        }
        return SectionChannelTableViewHeaderFooterView.getHeight(row)
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let section = sections[safe: section] {
            switch section {
            case .header, .invite, .openSetting, .memberRequests, .currentMember, .members, .folders, .playlists: return UIView()
            default: break
            }
        }
        guard let section = repository.SectionChannels?[safe: section - section_count_other_channels], let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.nib.sectionChannelTableViewHeaderFooterView.name) as? SectionChannelTableViewHeaderFooterView else {
            fatalError("This section is not exist")
        }
        view.setRepository(section, permission: self.permission)
        view.delegate = self
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let section = sections[safe: indexPath.section] {
            switch section {
            case .header: return GuildHeaderTableViewCell.cellHeight
            case .invite: return 48
            case .openSetting, .memberRequests: return UITableView.automaticDimension
            case .members, .folders, .playlists, .currentMember: return 36
            default: break
            }
        }
        
        if let section = repository.SectionChannels?[safe: indexPath.section - section_count_other_channels], section.rows[safe: indexPath.row] != nil {
            return 36
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let section = sections[safe: indexPath.section] {
            switch section {
            case .header: return GuildHeaderTableViewCell.cellHeight
            case .invite: return 48
            case .openSetting, .memberRequests: return 44
            case .members, .folders, .playlists, .currentMember: return 36
            default: break
            }
        }
        
        if let section = repository.SectionChannels?[safe: indexPath.section - section_count_other_channels], section.rows[safe: indexPath.row] != nil {
            return 36
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let section = sections[safe: indexPath.section] {
            switch section {
            case .header: self.presenter?.toGuildMenu()
            case .invite: self.presenter?.toGuildShare()
            case .openSetting: break // self.presenter?.toGuildOpenSetting()
            case .memberRequests: break // self.presenter?.toMemberRequests()
            case .currentMember: self.presenter?.toMemberMenu(repository: current_member)
            case .members: self.delegate?.guildViewController(didSelectMembers: repository, guildViewController: self)
            case .folders: self.delegate?.guildViewController(didSelectFolders: self)
            case .playlists: self.delegate?.guildViewController(didSelectPlaylists: self)
            default: break
            }
        }
        if let section = repository.SectionChannels?[safe: indexPath.section - section_count_other_channels], let row = section.rows[safe: indexPath.row] {
            if row.sectionChannelRowType == .Stream, let stream = row as? StreamEntity, let repository = section.StreamChannels?.filter({ $0.id == stream.ChannelId }).first {
                repository.Guild = self.repository.toNewMemory()
                self.delegate?.guildViewController(didSelectSectionRow: repository, in: self)
            } else {
                (row as? TextChannelEntity)?.Guild = repository.toNewMemory()
                (row as? DMChannelEntity)?.Guild = repository.toNewMemory()
                (row as? StreamChannelEntity)?.Guild = repository.toNewMemory()
                self.delegate?.guildViewController(didSelectSectionRow: row, in: self)
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension GuildViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            guard let section = sections[safe: $0.section], section == .channels, let sectionChannel = repository.SectionChannels?[safe: $0.section - section_count_other_channels], let row = sectionChannel.rows[safe: $0.row] else { return }
            switch row.sectionChannelRowType {
            case .Participant:
                guard let repository = row as? ParticipantEntity, let picture_small = repository.Member?.picture_small, let url = URL(string: picture_small) else { break }
                requests.append(ImageRequest(url: url))
            default: break
            }
        }
        return requests
    }
}

extension GuildViewController {
    func calcHeadItemsHeight() -> CGFloat {
        return 48 + GuildHeaderTableViewCell.cellHeight
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        DispatchQueue.main.async { [unowned self] in
            let offsetY = self.tableView.contentOffset.y + ImagePagingCell.menuHeight
            if (offsetY < SectionChannelTableViewHeaderFooterView.viewHeight + self.calcHeadItemsHeight() && offsetY >= 0) {
                   self.tableView.contentInset = UIEdgeInsets(top: -offsetY + ImagePagingCell.menuHeight, left: 0, bottom: 0, right: 0)
            } else if (offsetY >= SectionChannelTableViewHeaderFooterView.viewHeight + self.calcHeadItemsHeight()) {
                self.tableView.contentInset = UIEdgeInsets(top: -ImagePagingCell.minHeight + 4, left: 0, bottom: 0, right: 0)
            }
            self.delegate?.guildViewControllerDidScroll(self)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        self.delegate?.guildViewControllerWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
}

extension GuildViewController: GuildHeaderTableViewCellDelegate {
    func guildHeaderTableViewCell(onTapSetting guildHeaderTableViewCell: GuildHeaderTableViewCell) {
        self.presenter?.toGuildMenu()
    }
}

extension GuildViewController: SimpleButtonTableViewCellDelegate {
    func simpleButtonTableViewCell(onTap simpleButtonTableViewCell: SimpleButtonTableViewCell) {
        self.presenter?.toGuildShare()
    }
}

extension GuildViewController: MembersTableViewCellDelegate {
}

extension GuildViewController: FoldersTableViewCellDelegate {
}

extension GuildViewController: PlaylistsTableViewCellDelegate {
}

extension GuildViewController: CurrentMemberTableViewCellDelegate {
}

extension GuildViewController: DMChannelTableViewCellDelegate {
}

extension GuildViewController: TextChannelTableViewCellDelegate {
}

extension GuildViewController: StreamChannelTableViewCellDelegate {
}

extension GuildViewController: StreamTableViewCellDelegate {
}

extension GuildViewController: ParticipantTableViewCellDelegate {
}

extension GuildViewController: NavigationTableViewCellDelegate {
    func navigationTableViewCell(onTap navigationTableViewCell: NavigationTableViewCell) {
        switch navigationTableViewCell.titleLabel.text {
        case R.string.localizable.guildShouldSetInfo(): self.presenter?.toGuildOpenSetting()
        case R.string.localizable.guildMemberRequests(): self.delegate?.guildViewController(didSelectMemberRequests: repository, guildViewController: self)
        default: break
        }
    }
}

extension GuildViewController: SectionChannelTableViewHeaderFooterViewDelegate {
    func sectionChannelTableViewHeaderFooterView(onTap sectionChannelTableViewHeaderFooterView: SectionChannelTableViewHeaderFooterView) {
//        let section = sectionChannelTableViewHeaderFooterView.repository
//        guard section.uid != "", let sections = self.repository.SectionChannels, sections.contains(section), let index = sections.indexOf(section) else { return }
//        self.tableView.beginUpdates()
//        if section.collapsed {
//            self.tableView.insertRows(
//                at: section.rows.enumerated().compactMap { return IndexPath(row: $0.offset, section: index + section_count_other_channels) },
//                with: .fade
//            )
//            sections[safe: index]?.collapsed = false
//            sectionChannelTableViewHeaderFooterView.repository.collapsed = false
//            sectionChannelTableViewHeaderFooterView.collapsed = false
//        } else {
//            self.tableView.deleteRows(
//                at: section.rows.enumerated().compactMap { return IndexPath(row: $0.offset, section: index + section_count_other_channels) },
//                with: .fade
//            )
//            sections[safe: index]?.collapsed = true
//            sectionChannelTableViewHeaderFooterView.repository.collapsed = true
//            sectionChannelTableViewHeaderFooterView.collapsed = true
//        }
//        self.tableView.endUpdates()
//        tableView.reloadSections(IndexSet(integer: index + section_count_other_channels), with: .fade)
    }
    
    func sectionChannelTableViewHeaderFooterView(onAddTap sectionChannelTableViewHeaderFooterView: SectionChannelTableViewHeaderFooterView) {
        self.presenter?.toChannelNew(section: sectionChannelTableViewHeaderFooterView.repository)
    }
}

extension GuildViewController: CurrentMemberViewDelegate {
    func currentMemberView(onTap repository: MemberEntity, in currentMemberView: CurrentMemberView) {
        self.presenter?.toMemberMenu(repository: repository)
    }
}

extension GuildViewController: GuildPresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.fetched = true
                self.view.hideAllToasts()
                self.setRepository(repository)
            }
        }
    }
    
    func guildPresenter(didRetry count: Int, in guildPresenter: GuildPresenter) {
        DispatchQueue.main.async {
            self.view.makeToast(R.string.localizable.domainErrorDetail())
        }
    }
    
    func guildPresenter(didJoin repository: GuildEntity, in guildPresenter: GuildPresenter) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.view.hideAllToasts()
                self.repository = repository
                self.evalSections()
            }
        }
    }
    
    func guildPresenter(shouldUpdatePermission repository: PermissionModel, in guildPresenter: GuildPresenter) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.evalSections()
                self.view.hideAllToasts()
                self.tableView.reloadData()
            }
        }
    }
}
