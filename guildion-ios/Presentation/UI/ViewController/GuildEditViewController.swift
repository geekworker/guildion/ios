//
//  GuildEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import PKHUD
import Eureka

class GuildEditViewController: BaseFormViewController {
    var presenter: GuildEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: GuildEntity = GuildEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: GuildEntity = GuildEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    func handleValueChange() {
        if repository.name == new_repository.name &&  repository.picture_small == new_repository.picture_small && repository.picture_large == new_repository.picture_large {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section()
            <<< ImageRow() {
                $0.title = R.string.localizable.profileImage()
                $0.value = UIImage(url: new_repository.picture_small)
            }.onChange { [unowned self] in
                guard let image = $0.value else {
                    self.new_repository.picture_small = DataConfig.Image.default_guild_image_url
                    self.reloadForm()
                    self.handleValueChange()
                    return
                }
                self.new_repository.picture_small = "\(ImagePickerHandler().generateImageURL(image))"
                self.handleValueChange()
            }.cellUpdate(imageRowCellUpdate(_:_:))
            +++ Section(R.string.localizable.guildName())
            <<< TextRow(R.string.localizable.name()) {
                var ruleSet = RuleSet<String>()
                ruleSet.add(rule: RuleRequired())
                $0.add(ruleSet: ruleSet)
                $0.value = new_repository.name
                $0.placeholder = $0.tag
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.name = value
                self.handleValueChange()
            }.cellUpdate(textRowCellUpdate(_:_:))
    }
}

extension GuildEditViewController: GuildEditPresenterOutput {
    func guildEditPresenter(afterUpdated repository: GuildEntity, in guildEditPresenter: GuildEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func guildEditPresenter(onError error: Error, in guildEditPresenter: GuildEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
    }
}
