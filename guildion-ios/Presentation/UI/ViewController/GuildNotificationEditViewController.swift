//
//  GuildNotificationEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class GuildNotificationEditViewController: BaseFormViewController {
    var presenter: GuildNotificationEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: MemberEntity = MemberEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: MemberEntity = MemberEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildNotificationEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleValueChange() {
        if self.repository.mute == self.new_repository.mute && self.repository.messages_mute == self.new_repository.messages_mute && self.repository.mentions_mute == self.new_repository.mentions_mute && self.repository.everyone_mentions_mute == self.new_repository.everyone_mentions_mute {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.updateNotification(repository: new_repository)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section(R.string.localizable.guildNotificationSetting())
            <<< SwitchRow(R.string.localizable.guildNotificationMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.mute
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.guildNotificationMessageMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.messages_mute
                $0.hidden = .function([R.string.localizable.guildNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.guildNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.messages_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.guildNotificationMentionMute()) { [unowned self] in
                $0.title = $0.tag
                $0.value = self.new_repository.mentions_mute
                $0.hidden = .function([R.string.localizable.guildNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.guildNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.guildNotificationEveryoneMentionMute()) {
                $0.title = $0.tag
                $0.value = self.new_repository.everyone_mentions_mute
                $0.hidden = .function([R.string.localizable.guildNotificationMute()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.guildNotificationMute())
                    return row.value ?? false == true
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.everyone_mentions_mute = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
    }
}

extension GuildNotificationEditViewController: GuildNotificationEditPresenterOutput {
    func sync(didFetchRepository repository: MemberEntity) {
        self.repository = repository
    }
    
    func guildNotificationEditPresenter(didUpdate repository: MemberEntity, in guildNotificationEditPresenter: GuildNotificationEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        if let member = GuildConnector.shared.current_member {
            member.mute = repository.mute
            member.messages_mute = repository.messages_mute
            member.mentions_mute = repository.mentions_mute
            member.everyone_mentions_mute = repository.everyone_mentions_mute
        }
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    func guildNotificationEditPresenter(onError error: Error, in guildNotificationEditPresenter: GuildNotificationEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
