//
//  FolderEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/19.
//

import Foundation
import UIKit
import PKHUD
import Nuke
import Eureka

class FolderEditViewController: BaseFormViewController {
    var presenter: FolderEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var is_playlist: Bool = false
    var repository: FolderEntity = FolderEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: FolderEntity = FolderEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        FolderEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    func handleValueChange() {
        if repository.name == new_repository.name &&  repository.description == new_repository.description && repository.thumbnail == new_repository.thumbnail {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section()
            +++ Section(R.string.localizable.name())
            <<< TextRow(R.string.localizable.name()) {
                var ruleSet = RuleSet<String>()
                ruleSet.add(rule: RuleRequired())
                $0.add(ruleSet: ruleSet)
                $0.value = new_repository.name
                $0.placeholder = $0.tag
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.name = value
                self.handleValueChange()
            }.cellUpdate(textRowCellUpdate(_:_:))
        
            +++ Section("")
            <<< ButtonRow(R.string.localizable.delete()) {
                $0.title = $0.tag
                $0.onCellHighlightChanged({ _, _ in })
            }.cellUpdate { [unowned self] in
                self.buttonRowCellUpdate($0, $1)
                $0.textLabel?.textColor = Theme.themify(key: .danger)
                $0.textLabel?.font = Font.FT(size: .FM, family: .B)
            }.onCellSelection { [unowned self] cell, row in
                let alert: UIAlertController = UIAlertController(title: R.string.localizable.delete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                            
                let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.delete(), style: UIAlertAction.Style.default, handler:{
                    [unowned self](action: UIAlertAction!) -> Void in
                    HUD.show(.progress)
                    self.presenter?.delete(repository: self.repository)
                })
                let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                    (action: UIAlertAction!) -> Void in
                })
                alert.addAction(cancelAction)
                alert.addAction(defaultAction)
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                self.present(alert, animated: true, completion: nil)
            }
    }
}

extension FolderEditViewController: FolderEditPresenterOutput {
    func folderEditPresenter(didUpdate repository: FolderEntity, in folderEditPresenter: FolderEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func folderEditPresenter(didDelete repository: FolderEntity, in folderEditPresenter: FolderEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func sync(didFetchRepository repository: FolderEntity) {
        self.repository = repository
    }
    
    func folderEditPresenter(onError error: Error, in folderEditPresenter: FolderEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
