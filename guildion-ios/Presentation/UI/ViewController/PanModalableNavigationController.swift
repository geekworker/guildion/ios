//
//  PanModalableNavigationController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/21.
//

import Foundation
import UIKit
import PanModal

protocol PanModalableNavigationControllerSearchDelegate: class {
    var current_url: URL { get set }
    var domainType: SearchDomainType { get set }
    func panModalableNavigationControllerShouldClose()
    func panModalableNavigationControllerShouldReload()
    func panModalableNavigationController(_ panModalableNavigationController: PanModalableNavigationController, searched keyword: String?)
}

class PanModalableNavigationController: UINavigationController {
    public var searchBrowserNavigationBar: SearchBrowserNavigationBar = SearchBrowserNavigationBar()
    weak var searchDelegate: PanModalableNavigationControllerSearchDelegate?
    var current_url: URL {
        get {
            self.searchDelegate?.current_url ?? URL(string: Constant.APP_URL)!
        } set {
            self.searchBrowserNavigationBar.current_url = newValue
        }
    }
    var domainType: SearchDomainType {
        get {
            self.searchDelegate?.domainType ?? .google
        } set {
            self.searchBrowserNavigationBar.domainType = newValue
        }
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        let vc = super.popViewController(animated: animated)
        panModalSetNeedsLayoutUpdate()
        return vc
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        panModalSetNeedsLayoutUpdate()
    }
}

extension PanModalableNavigationController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return (viewControllers.first as? PanModalPresentable)?.panScrollable
    }

    var longFormHeight: PanModalHeight {
        return (viewControllers.first as? PanModalPresentable)?.longFormHeight ?? .maxHeight
    }

    var shortFormHeight: PanModalHeight {
        return (viewControllers.first as? PanModalPresentable)?.shortFormHeight ?? longFormHeight
    }
    
    var allowsExtendedPanScrolling: Bool { return true }
}

extension PanModalableNavigationController: SearchBrowserNavigationBarDelegate {
    fileprivate func configureSearchMode() {
        self.switchEditMode(self.searchDelegate != nil)
        self.searchBrowserNavigationBar.delegate = self
        self.searchBrowserNavigationBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.navigationBar.frame.height)
        self.navigationBar.addSubview(self.searchBrowserNavigationBar)
        self.searchBrowserNavigationBar.delegate = self
    }
    
    public func switchEditMode(_ show: Bool) {
        self.searchBrowserNavigationBar.isHidden = !show
        self.navigationBar.bringSubviewToFront(self.searchBrowserNavigationBar)
    }
    
    func searchBrowserNavigationBarShouldClose(_ searchBrowserNavigationBar: SearchBrowserNavigationBar) {
        self.searchDelegate?.panModalableNavigationControllerShouldClose()
    }
    
    func searchBrowserNavigationBarShouldReload(_ searchBrowserNavigationBar: SearchBrowserNavigationBar) {
        self.searchDelegate?.panModalableNavigationControllerShouldReload()
    }
    
    func searchBrowserNavigationBar(_ searchBrowserNavigationBar: SearchBrowserNavigationBar, searched keyword: String?) {
        self.searchDelegate?.panModalableNavigationController(self, searched: keyword)
    }
}
