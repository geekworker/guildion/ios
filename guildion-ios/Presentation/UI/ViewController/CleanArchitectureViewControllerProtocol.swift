//
//  CleanArchitectureViewControllerProtocol.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/10.
//

import Foundation
import UIKit

protocol CleanArchitectureViewControllerProtocol: class {
    associatedtype PresenterType
    associatedtype RepositoryType
    associatedtype RepositoriesType
    
    var presenter: PresenterType? { get set }
    var repository: RepositoryType? { get set }
    var repositories: RepositoriesType? { get set }
    
    func build()
    func setRepository(_ repository: RepositoryType)
    func setRepositories(_ repositories: RepositoriesType)
}

extension CleanArchitectureViewControllerProtocol where Self: UIViewController {
    func setRepository(_ repository: RepositoryType) {
        self.repository = repository
    }
    
    func setRepositories(_ repositories: RepositoriesType) {
        self.repositories = repositories
    }
}
