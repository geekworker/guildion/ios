//
//  StreamChannelLiveSettingViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/02.
//

import Foundation
import UIKit
import PanModal
import Nuke
import PKHUD

protocol StreamChannelLiveSettingViewControllerDelegate: class {
    var streamChannelHeaderView: StreamChannelHeaderView! { get set }
    func streamChannelLiveSettingViewController(willAppear streamChannelLiveSettingViewController: StreamChannelLiveSettingViewController)
}

class StreamChannelLiveSettingViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var presenter: StreamChannelLiveSettingPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: StreamChannelEntity = StreamChannelEntity()
    var repositories: ParticipantEntities = ParticipantEntities()
    weak var delegate: StreamChannelLiveSettingViewControllerDelegate?
    
    enum Section: Int {
        case port
        case stream
        case participants
        case count
    }
    
    override func viewDidLoad() {
        self.navigationMode = .modalBorder
        super.viewDidLoad()
        StreamChannelLiveSettingViewControllerBuilder.rebuild(self)
        self.view.backgroundColor = Theme.themify(key: .background)
        configureTableView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        presenter?.fetchParticipants(uid: repository.uid)
        self.delegate?.streamChannelLiveSettingViewController(willAppear: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func updateLayout() {
    }
}

extension StreamChannelLiveSettingViewController: UITableViewDelegate {
    fileprivate func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.register([
            UINib(resource: R.nib.streamAudioTableViewCell): R.nib.streamAudioTableViewCell.name,
            UINib(resource: R.nib.streamAudioPortTableViewCell): R.nib.streamAudioPortTableViewCell.name,
            UINib(resource: R.nib.participantAudioTableViewCell): R.nib.participantAudioTableViewCell.name,
        ])
        self.tableView.register(
            UINib(resource: R.nib.subTextTableViewHeaderFooterView),
            forHeaderFooterViewReuseIdentifier: R.nib.subTextTableViewHeaderFooterView.name
        )
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let section = Section.init(rawValue: section) else { return UIView() }
        switch section {
        case .participants:
            let view = UIView()
            view.backgroundColor = .none
            return view
        default: return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let section = Section.init(rawValue: section) else { return 0 }
        switch section {
        case .participants: return 200
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let section = Section.init(rawValue: indexPath.section) else { return }
        switch section {
        case .port: break
        default: break
        }
    }
}

extension StreamChannelLiveSettingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section.init(rawValue: section) else { return 0 }
        switch section {
        case .port: return 1
        case .stream: return 0 // repository.LiveStream == nil || repository.LiveStream?.mode == .none ? 0 : 1
        case .participants: return repositories.count
        case .count: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section.init(rawValue: indexPath.section) else { return UITableViewCell() }
        switch section {
        case .port: return self.tableView(tableView, cellPortForRowAt: indexPath)
        case .stream: return self.tableView(tableView, cellStreamForRowAt: indexPath)
        case .participants: return self.tableView(tableView, cellParticipantForRowAt: indexPath)
        case .count: return UITableViewCell()
        }
    }
    
    fileprivate func tableView(_ tableView: UITableView, cellPortForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.streamAudioPortTableViewCell.name, for: indexPath) as? StreamAudioPortTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.updateLayout()
        return cell
    }
    
    fileprivate func tableView(_ tableView: UITableView, cellStreamForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.streamAudioTableViewCell.name, for: indexPath) as? StreamAudioTableViewCell, let repository = repository.LiveStream else {
            fatalError("cell is not exists")
        }
        cell.delegate = self
        //cell.setRepository(repository, volume: self.delegate?.streamChannelHeaderView.volume ?? 1)
        return cell
    }
    
    fileprivate func tableView(_ tableView: UITableView, cellParticipantForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.participantAudioTableViewCell.name, for: indexPath) as? ParticipantAudioTableViewCell, let repository = repositories[safe: indexPath.row], let model = delegate?.webRTCHandler.peerConnections.getConnection(user: repository.Member?.User ?? UserEntity()) else {
//            let cell = UITableViewCell()
//            cell.backgroundColor = .none
//            return cell
//        }
//        cell.delegate = self
//        cell.setRepository(repository, volume: model.volume)
//        return cell
        
        let cell = UITableViewCell()
        cell.backgroundColor = .none
        return cell
    }
}

extension StreamChannelLiveSettingViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row]?.Member, let url = URL(string: repository.picture_small) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension StreamChannelLiveSettingViewController: AudioActionSheetProtocol {
}

extension StreamChannelLiveSettingViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return tableView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(400)
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
}

extension StreamChannelLiveSettingViewController: StreamAudioTableViewCellDelegate {
    func streamAudioTableViewCell(onChangeVolume value: Float, in streamAudioTableViewCell: StreamAudioTableViewCell) {
    }
}

extension StreamChannelLiveSettingViewController: ParticipantAudioTableViewCellDelegate {
    func participantAudioTableViewCell(onChangeVolume value: Float, in participantAudioTableViewCell: ParticipantAudioTableViewCell) {
    }
}

extension StreamChannelLiveSettingViewController: StreamChannelLiveSettingPresenterOutput {
    func sync(didFetchRepository repository: StreamChannelEntity) {
        self.repository = repository
    }
    
    func sync(didFetchRepositories repositories: ParticipantEntities) {
        self.repositories.items = repositories.items.filter({ CurrentUser.getCurrentUserEntity()?.id != $0.Member?.UserId })
        self.tableView.reloadData()
    }
    
    func streamChannelLiveSettingPresenter(onError error: Error, in streamChannelLiveSettingPresenter: StreamChannelLiveSettingPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}

