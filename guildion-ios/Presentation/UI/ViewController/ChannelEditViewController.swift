//
//  ChannelEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class ChannelEditViewController: BaseFormViewController {
    var presenter: ChannelEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: ChannelableProtocol = TextChannelEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: ChannelableProtocol = TextChannelEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ChannelEditViewController: ChannelEditPresenterOutput {
    func sync(didFetchRepository repository: ChannelableProtocol) {
        self.repository = repository
    }
}

