//
//  ProfileSettingViewController.swift
//  nowy-ios
//
//  Created by Apple on 2020/07/10.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Nuke
import TextFieldEffects
import SPPermissions
import PKHUD

class ProfileSettingViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = R.string.localizable.sessionProfileSetting()
        }
    }
    @IBOutlet weak var profileImage: UIImageView! {
         didSet {
             self.profileImage.layer.cornerRadius = self.profileImage.bounds.width / 2
             self.profileImage.clipsToBounds = true
             self.profileImage.contentMode = .scaleAspectFill
         }
     }
    @IBOutlet weak var profileCameraImage: UIImageView! {
        didSet {
            profileCameraImage.image = R.image.camera()?.withRenderingMode(.alwaysTemplate)
            profileCameraImage.tintColor = .white
        }
    }
    @IBOutlet weak var profileCoverView: UIView! {
        didSet {
            self.profileCoverView.layer.cornerRadius = self.profileCoverView.bounds.width / 2
            self.profileCoverView.clipsToBounds = true
            self.profileCoverView.contentMode = .scaleAspectFill
            self.profileCoverView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileCameraTap(_:))))
        }
    }
//    @IBOutlet weak var detailTextField: HoshiTextField! {
//        didSet {
//            self.detailTextField.font = Font.FT(size: "FM", family: "L")
//            self.detailTextField.placeholderColor = Theme.themify(key: "base")
//            self.detailTextField.placeholder = R.string.localizable.detail()
//            self.detailTextField.borderInactiveColor = Theme.themify(key: "backgroundLight")
//            self.detailTextField.borderActiveColor = Theme.themify(key: "main")
//            self.detailTextField.tintColor = Theme.themify(key: "backgroundContrast")
//            self.detailTextField.delegate = self
//        }
//    }
    @IBOutlet weak var submitButton: SimpleButton! {
        didSet {
            submitButton.canceled = false
            submitButton.text = R.string.localizable.send()
            submitButton.delegate = self
        }
    }
    private var pickingProfileImage: Bool = false
    private var pickingBackgroundImage: Bool = false
    public var imagePickerHandler = ImagePickerHandler()
    var repository: SessionModel = SessionModel()
    var presenter: ProfileSettingPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    
    override func viewDidLoad() {
        self.navigationMode = .noneBorder
        ProfileSettingViewControllerBuilder.rebuild(self)
        super.viewDidLoad()
        self.setFlatBackground()
        self.repository.initializeUser()
        self.presenter?.repositories = self.repository
        imagePickerHandler.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.bind()
        updateLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter?.unbind()
    }
    
    @objc func profileCameraTap(_ sender: Any) {
        self.pickingProfileImage = true
        guard configurePhotoPermission() else { return }
        self.imagePickerHandler.openImagePicker()
    }
    
    func updateLayout() {
        if let picture_small = repository.user?.picture_small, let url = URL(string: picture_small) {
            Nuke.loadImage(with: url, into: profileImage)
        }
    }
}

extension ProfileSettingViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.presenter?.submit(self.repository)
        HUD.show(.progress)
    }
}

extension ProfileSettingViewController: ImagePickerHandlerDelegate {
    
    var viewController: UIViewController {
        get {
            self
        }
    }
    
    func updateAssets(_ image: UIImage, info: [UIImagePickerController.InfoKey : Any]) {
        if pickingProfileImage {
            self.profileImage.image = image
            self.repository.user?.picture_small = "\(imagePickerHandler.generateImageURL(image))"
            pickingProfileImage = false
        } else if pickingBackgroundImage {
            // self.backgroundImage.image = image
            self.repository.user?.picture_large = "\(imagePickerHandler.generateImageURL(image))"
            pickingBackgroundImage = false
        }
    }
    
    func cancelAssets() {
        pickingProfileImage = false
        pickingBackgroundImage = false
    }
}

extension ProfileSettingViewController: SPPermissionsDelegate, SPPermissionsDataSource {
    
    func configurePhotoPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary].filter { !$0.isAuthorized }
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self)
        return false
    }
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        switch permission {
        case .camera:
            cell.permissionTitleLabel.text = R.string.localizable.permissionCamera()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionCameraText()
        case .microphone:
            cell.permissionTitleLabel.text = R.string.localizable.permissionVoice()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionVoiceText()
        case .photoLibrary:
            cell.permissionTitleLabel.text = R.string.localizable.permissionPhoto()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionPhotoText()
        default: break
        }
        return cell
    }
    
    func didHide(permissions ids: [Int]) {
        let permissions: [SPPermission] = [.photoLibrary].filter { !$0.isAuthorized }
        guard permissions.isEmpty else {
            pickingProfileImage = false
            pickingBackgroundImage = false
            return
        }
        self.imagePickerHandler.openImagePicker()
    }
}

extension ProfileSettingViewController: ProfileSettingPresenterOutput {
    func sync(didFetchRepository repository: SessionModel) {
        self.repository = repository
    }
    
    func profileSettingPresenter(onError error: Error) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func profileSettingPresenter(didUpdate repository: UserEntity) {
        HUD.hide()
        HUD.flash(.success)
        BaseWireframe.toMain()
    }
}
