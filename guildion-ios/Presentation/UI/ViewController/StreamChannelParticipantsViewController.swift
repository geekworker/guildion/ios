//
//  StreamChannelParticipantsViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/21.
//

import Foundation
import UIKit
import PanModal
import Nuke
import PKHUD

protocol StreamChannelParticipantsViewControllerDelegate: class {
    var viewControllerParent: StreamChannelViewController! { get set }
    func streamChannelParticipantsViewController(willAppear streamChannelParticipantsViewController: StreamChannelParticipantsViewController)
}

class StreamChannelParticipantsViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var presenter: StreamChannelParticipantsPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: StreamChannelEntity = StreamChannelEntity()
    var repositories: ParticipantEntities = ParticipantEntities()
    weak var delegate: StreamChannelParticipantsViewControllerDelegate?
    
    override func viewDidLoad() {
        self.navigationMode = .modalBorder
        super.viewDidLoad()
        StreamChannelParticipantsViewControllerBuilder.rebuild(self)
        self.view.backgroundColor = Theme.themify(key: .background)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.delegate?.streamChannelParticipantsViewController(willAppear: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func updateLayout() {
    }
}

extension StreamChannelParticipantsViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return tableView
    }
    
    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .shortForm = state else { return }

        self.panModalTransition(to: .longForm)
        panModalSetNeedsLayoutUpdate()
    }
}

extension StreamChannelParticipantsViewController: StreamChannelParticipantsPresenterOutput {
    func sync(didFetchRepository repository: StreamChannelEntity) {
        self.repository = repository
    }
    
    func sync(didFetchRepositories repositories: ParticipantEntities) {
        self.repositories = repositories
    }
}
