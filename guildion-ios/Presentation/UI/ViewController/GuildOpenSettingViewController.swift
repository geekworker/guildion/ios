//
//  GuildOpenSettingViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/12.
//

import Foundation
import UIKit
import TextFieldEffects
import DropDown
import PKHUD

class GuildOpenSettingViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = R.string.localizable.guildInfoTitle()
        }
    }
    @IBOutlet weak var localeLabel: UILabel! {
        didSet {
            localeLabel.isHidden = true
            localeLabel.text = R.string.localizable.locale()
        }
    }
    @IBOutlet weak var localeButton: BorderButton! {
        didSet {
            localeButton.isHidden = true
            localeButton.setTitle(R.string.localizable.select(R.string.localizable.locale()), for: .normal)
            localeButton.setTitle(R.string.localizable.select(R.string.localizable.locale()), for: .highlighted)
            localeButton.addTarget(self, action: #selector(chooseLocale(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var categoryLabel: UILabel! {
        didSet {
            categoryLabel.text = R.string.localizable.category()
        }
    }
    @IBOutlet weak var categoryButton: BorderButton! {
        didSet {
            categoryButton.setTitle(R.string.localizable.select(R.string.localizable.category()), for: .normal)
            categoryButton.setTitle(R.string.localizable.select(R.string.localizable.category()), for: .highlighted)
            categoryButton.addTarget(self, action: #selector(chooseCategory(_:)), for: .touchUpInside)
        }
    }
    @IBOutlet weak var tagListView: TagListView! {
        didSet {
            tagListView.isHidden = true
        }
    }
    @IBOutlet weak var tagField: HoshiTextField! {
        didSet {
            self.tagField.font = Font.FT(size: .M, family: .L)
            self.tagField.placeholderLabel.font = Font.FT(size: .M, family: .B)
            self.tagField.textColor = Theme.themify(key: .string)
            self.tagField.placeholderColor = Theme.themify(key: .string)
            self.tagField.placeholder = R.string.localizable.tag()
            self.tagField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.tagField.borderActiveColor = Theme.themify(key: .main)
            self.tagField.delegate = self
            tagField.isHidden = true
        }
    }
    public var chooseLocaleDropDown = DropDown()
    public var chooseCategoryDropDown = DropDown()
    
    var presenter: GuildOpenSettingPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public var repository: GuildEntity = GuildEntity()
    public var categories: CategoryEntities = CategoryEntities()
    public var locales: [String] = LocaleConfig.Locales.languadges
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GuildOpenSettingViewControllerBuilder.rebuild(self)
        self.navigationMode = .buttonModal(title: R.string.localizable.next())
        self.setFlatBackground()
        self.configureDropDown()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        resetRightNavigationBarButton()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildInputDescriptionViewController, segue.identifier == R.segue.guildOpenSettingViewController.toInputDescription.identifier {
            vc.repository = self.repository
        }
    }
    
    @objc func chooseLocale(_ sender: AnyObject) {
        chooseLocaleDropDown.show()
    }
    
    @objc func chooseCategory(_ sender: AnyObject) {
        chooseCategoryDropDown.show()
    }
    
    override func onTapNavigationDoneButton(_ sender: Any) {
        self.presenter?.toInputDescription()
    }
    
    func updateCategoryDropDown() {
        self.chooseCategoryDropDown.dataSource = categories.compactMap { $0.getLocalableName(locale: LocaleConfig.Locales(locale: repository.locale)) }
    }
    
    func configureDropDown() {
        self.repository.locale = NSLocale.current.languageCode ?? LocaleConfig.default_locale.rawValue
        chooseCategoryDropDown.anchorView = categoryButton
        chooseCategoryDropDown.bottomOffset = CGPoint(x: 0, y: categoryButton.bounds.height)
        self.updateCategoryDropDown()
        chooseCategoryDropDown.selectionAction = { [unowned self] (index, item) in
            self.categoryButton.setTitle(item, for: .normal)
            self.repository.Category = self.categories.filter { $0.ja_name == item || $0.en_name == item } .first ?? CategoryEntity()
            self.repository.CategoryId = (self.categories.filter { $0.ja_name == item || $0.en_name == item } .first ?? CategoryEntity()).id ?? 0
            self.repository.Category == nil || repository.locale == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
        }
    }
}

extension GuildOpenSettingViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if repository.Tags == nil {
            repository.Tags = TagEntities()
        }
        self.repository.Tags?.items = self.tagListView.tagViews.compactMap { (tagView: TagView) -> TagEntity in
            let t = TagEntity()
            t.name = tagView.titleLabel?.text ?? ""
            return t
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if repository.Tags == nil {
            repository.Tags = TagEntities()
        }
        self.repository.Tags?.items = self.tagListView.tagViews.compactMap { (tagView: TagView) -> TagEntity in
            let t = TagEntity()
            t.name = tagView.titleLabel?.text ?? ""
            return t
        }
        if 0 < textField.text!.count {
            tagListView.addTag(textField.text!)
            self.tagField.text = nil
        }
        tagField.resignFirstResponder()
        return true
    }
}

extension GuildOpenSettingViewController: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        sender.removeTagView(tagView)
        self.repository.Tags?.items = self.repository.Tags?.filter { $0.name != title } ?? []
    }
}

extension GuildOpenSettingViewController: GuildOpenSettingPresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
    }
    
    func syncCategories(didFetchRepositories repositories: CategoryEntities) {
        self.categories = repositories
        self.updateCategoryDropDown()
    }
    
    func guildOpenSettingPresenter(onError error: Error) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func guildOpenSettingPresenter(didCreated repository: GuildEntity) {
        HUD.hide()
        HUD.flash(.success)
        self.presenter?.toInputDescription()
    }
}

