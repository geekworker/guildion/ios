//
//  FileNewFromBrowserViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import PKHUD

protocol FileNewFromBrowserViewControllerDelegate: class {
    func fileNewFromBrowserViewController(didSelects repositories: FileEntities)
}

class FileNewFromBrowserViewController: BaseViewController {
    var presenter: FileNewFromBrowserPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: FileEntities = FileEntities()
    var current_member: MemberEntity = MemberEntity()
    var current_guild: GuildEntity = GuildEntity()
    weak var delegate: FileNewFromBrowserViewControllerDelegate?
    lazy var youtubeSelectWebViewController: YouTubeSelectWebViewController = {
        return YouTubeSelectWebViewController(nibName: R.nib.youTubeSelectWebViewController.name, bundle: nil)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.localizable.folders()
        FileNewFromBrowserViewControllerBuilder.rebuild(self)
        configureYouTubeSelectWebViewController()
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func initRepository(current_guild: GuildEntity, current_member: MemberEntity) {
        self.current_member = current_member
        self.current_guild = current_guild
    }
}

extension FileNewFromBrowserViewController: YouTubeSelectWebViewControllerDelegate {
    fileprivate func configureYouTubeSelectWebViewController() {
        youtubeSelectWebViewController.willMove(toParent: self)
        youtubeSelectWebViewController.delegate = self
        youtubeSelectWebViewController.view.frame = self.view.frame
        self.addChild(youtubeSelectWebViewController)
        self.view.addSubview(youtubeSelectWebViewController.view)
        youtubeSelectWebViewController.didMove(toParent: self)
        youtubeSelectWebViewController.startExploreYouTube()
    }
    
    func youTubeSelectWebViewController(didSelects repositories: FileEntities, in youTubeSelectWebViewController: YouTubeSelectWebViewController) {
        self.repositories = repositories
        if delegate == nil {
            self.presenter?.creates(repositories)
        } else {
            self.delegate?.fileNewFromBrowserViewController(didSelects: repositories)
        }
    }
}

extension FileNewFromBrowserViewController: FileNewFromBrowserPresenterOutput {
    func fileNewFromBrowserPresenter(onError error: Error) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func sync(didFetchRepositories repositories: FileEntities) {
        DispatchQueue.main.async {
            self.repositories = repositories
        }
    }
    
    func sync(didFetchRepository repository: MemberEntity) {
        DispatchQueue.main.async {
            self.current_member = repository
        }
    }
    
    func fileNewFromBrowserPresenter(shouldCreate repositories: FileEntities) {
        DispatchQueue.main.async {
            self.dismissShouldAppear(animated: true)
        }
    }
    
    func fileNewFromBrowserPresenter(didCreated repositories: FileEntities) {
        DispatchQueue.main.async {
            self.repositories = repositories
        }
    }
}
