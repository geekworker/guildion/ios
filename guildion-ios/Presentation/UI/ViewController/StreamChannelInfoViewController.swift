//
//  StreamChannelInfoViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/14.
//

import Foundation
import UIKit
import PanModal
import PKHUD
import Nuke

protocol StreamChannelInfoViewControllerDelegate: class {
    func streamChannelInfoViewControllerWillDismiss(in streamChannelInfoViewController: StreamChannelInfoViewController)
    func streamChannelInfoViewControllerDidDismiss(in streamChannelInfoViewController: StreamChannelInfoViewController)
    func streamChannelInfoViewController(willAppear streamChannelInfoViewController: StreamChannelInfoViewController)
    func streamChannelInfoViewController(toSelects streamChannelInfoViewController: StreamChannelInfoViewController)
    func streamChannelInfoViewController(shouldStart stream: StreamEntity, from streamChannelInfoViewController: StreamChannelInfoViewController)
}

class StreamChannelInfoViewController: BaseViewController, PanContainerNavigationViewChildDelegate {
    weak var parentContainerView: PanContainerNavigationView!
    enum Section: Int {
        case info
        case playlists
        case count
        
        var count: Int {
            switch self {
            case .info: return InfoRow.count.rawValue
            case .playlists: return 0
            case .count: return 0
            }
        }
        
        func getCount(repositories: FileReferenceEntities) -> Int {
            switch self {
            case .info: return InfoRow.count.rawValue
            case .playlists: return repositories.count
            case .count: return 0
            }
        }
    }
    
    enum SelectRow: Int {
        case content
        case count
    }
    
    enum InfoRow: Int {
        case content
        case count
    }
    
    @IBOutlet weak var tableView: UITableView!
    var presenter: StreamChannelInfoPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var all_files: Bool = false
    var current_guild: GuildEntity = GuildEntity()
    var repository: StreamChannelEntity = StreamChannelEntity()
    var stream: StreamEntity = StreamEntity()
    var streamable: StreamableEntity = StreamableEntity()
    var repositories: FileReferenceEntities = FileReferenceEntities()
    var folder: FolderEntity = FolderEntity()
    weak var delegate: StreamChannelInfoViewControllerDelegate?
    private var loadingDecrementStatus: Int?
    private var loadingIncrementStatus: Int?
    private var lastWillDisplay: Int?
    
    override func viewDidLoad() {
        self.navigationMode = .modalBorder
        super.viewDidLoad()
        StreamChannelInfoViewControllerBuilder.rebuild(self)
        self.view.backgroundColor = Theme.themify(key: .backgroundThick)
        configureTableView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate?.streamChannelInfoViewController(willAppear: self)
        loadingDecrementStatus = nil
        loadingIncrementStatus = nil
        lastWillDisplay = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        if let stream = repository.LiveStream, stream.mode == .fileMovie || stream.mode == .fileYouTube {
            self.all_files = true
        } else {
            self.all_files = false
        }
        self.presenter?.fetch(folder: self.folder)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func onTapNavigationCloseButton(_ sender: Any) {
        self.delegate?.streamChannelInfoViewControllerWillDismiss(in: self)
        self.dismissContainer(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            self.delegate?.streamChannelInfoViewControllerDidDismiss(in: self)
        })
    }
    
    func setRepository(_ repository: StreamChannelEntity) {
        self.repository = repository
        self.stream = repository.LiveStream ?? StreamEntity()
        switch stream.mode {
        case .fileMovie, .fileYouTube:
            self.streamable = stream.File!
        case .fileReferenceMovie, .fileReferenceYouTube:
            self.streamable = stream.FileReference!
        default: break
        }
    }
    
    func updateLayout() {
        self.tableView.reloadData()
    }
    
    func getLiveReference() -> FileReferenceEntity? {
        guard let stream = repository.LiveStream else { return nil }
        switch stream.mode {
        case .fileReferenceMovie, .fileReferenceYouTube:
            return stream.FileReference
        case .fileMovie, .fileYouTube:
            let entity = FileReferenceEntity()
            entity.File = stream.File
            entity.FileId = stream.File?.id
            return entity
        default: return nil
        }
    }
}

extension StreamChannelInfoViewController: UITableViewDelegate {
    private func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.register([
            UINib(resource: R.nib.simpleButtonTableViewCell): R.nib.simpleButtonTableViewCell.name,
            UINib(resource: R.nib.streamableInfoTableViewCell): R.nib.streamableInfoTableViewCell.name,
            UINib(resource: R.nib.fileTableViewCell): R.nib.fileTableViewCell.name,
        ])
        self.tableView.register(
            UINib(resource: R.nib.subTextTableViewHeaderFooterView),
            forHeaderFooterViewReuseIdentifier: R.nib.subTextTableViewHeaderFooterView.name
        )
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard repository.LiveStream?.mode != .other else { return 0 }
        return Section.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = Section.init(rawValue: indexPath.section) else { return 0 }
        switch section {
        case .info: return 80
        case .playlists: return 64
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = Section.init(rawValue: indexPath.section) else { return 0 }
        switch section {
        case .info: return UITableView.automaticDimension
        case .playlists: return FileTableViewCell.cellHeight
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        guard let section = Section.init(rawValue: indexPath.section) else { return }
        switch section {
        case .playlists:
            guard let repository = repositories[safe: indexPath.row], let stream = self.repository.LiveStream else { return }
            switch stream.mode {
            case .fileReferenceMovie, .fileReferenceYouTube:
                let entity = StreamEntity()
                entity.Streamable = repository
                entity.StreamableId = repository.id
                entity.streamable_type = .FileReference
                self.delegate?.streamChannelInfoViewController(shouldStart: entity, from: self)
                self.delegate?.streamChannelInfoViewControllerWillDismiss(in: self)
                self.dismissContainer(animated: true, completion: nil)
            case .fileMovie, .fileYouTube:
                let entity = StreamEntity()
                entity.Streamable = repository.File
                entity.StreamableId = repository.File?.id
                entity.streamable_type = .File
                self.delegate?.streamChannelInfoViewController(shouldStart: entity, from: self)
                self.delegate?.streamChannelInfoViewControllerWillDismiss(in: self)
                self.dismissContainer(animated: true, completion: nil)
            default: break
            }
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let section = Section.init(rawValue: indexPath.section), section == .playlists else { return }
        if repositories.count > indexPath.row, let repository = repositories[safe: indexPath.row], let last_id = repositories.last?.id, repository.id == last_id, last_id != loadingIncrementStatus {
            loadingIncrementStatus = last_id
            self.presenter?.increment()
        }
        lastWillDisplay = indexPath.row
    }
}

extension StreamChannelInfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section.init(rawValue: section) else { return 0 }
        return section.getCount(repositories: self.repositories)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section.init(rawValue: indexPath.section) else { return UITableViewCell() }
        switch section {
        case .info: return self.tableView(tableView, cellForInfoRowAt: indexPath)
        case .playlists: return self.tableView(tableView, cellForPlaylistRowAt: indexPath)
        default: return UITableViewCell()
        }
    }
    
    private func tableView(_ tableView: UITableView, cellForSelectRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.simpleButtonTableViewCell.name, for: indexPath) as? SimpleButtonTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundThick)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
        cell.simpleButton.text = R.string.localizable.streamNotfoundPick()
        cell.topInset.constant = 12
        cell.bottomInset.constant = 12
        cell.delegate = self
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForInfoRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.streamableInfoTableViewCell.name, for: indexPath) as? StreamableInfoTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundThick)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
        guard let stream = repository.LiveStream else { return cell }
        switch stream.streamable_type {
        case .File: cell.setRepository(stream.File ?? StreamableEntity())
        case .FileReference: cell.setRepository(stream.FileReference ?? StreamableEntity())
        }
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForPlaylistRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.fileTableViewCell.name, for: indexPath) as? FileTableViewCell, let reference = repositories[safe: indexPath.row], let repository = reference.File else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .backgroundThick)
        cell.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
        cell.setRepository(repository)
        if all_files, let entity = streamable as? FileEntity, entity == repository {
            cell.musicIndicatorView.state = .playing
        }
        if !all_files, let entity = streamable as? FileReferenceEntity, entity == reference {
            cell.musicIndicatorView.state = .playing
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = Section.init(rawValue: section) else { return UIView() }
        switch section {
        case .playlists:
            guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.nib.subTextTableViewHeaderFooterView.name) as? SubTextTableViewHeaderFooterView else { return UIView() }
            view.backgroundColor = Theme.themify(key: .backgroundThick)
            view.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
            if let stream = repository.LiveStream, stream.mode == .fileReferenceMovie || stream.mode == .fileReferenceYouTube {
                view.titleLabel.text = folder.name
                view.subTextLabel.text = repository.temporary ? R.string.localizable.playlist() : R.string.localizable.playlist() + " : "
            } else if let stream = repository.LiveStream, stream.mode == .fileMovie || stream.mode == .fileYouTube {
                view.titleLabel.text = R.string.localizable.allFiles()
                view.subTextLabel.text = R.string.localizable.playlist()
            }
            return view
        default: return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let section = Section.init(rawValue: section) else { return 0 }
        switch section {
        case .playlists: return SubTextTableViewHeaderFooterView.height
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let section = Section.init(rawValue: section) else { return UIView() }
        switch section {
        case .playlists:
            let view = UIView()
            return view
        default: return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let section = Section.init(rawValue: section) else { return 0 }
        switch section {
        case .playlists: return 200
        default: return 0
        }
    }
}

extension StreamChannelInfoViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row]?.File, let url = URL(string: repository.thumbnail) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension StreamChannelInfoViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return tableView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(400)
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
}

extension StreamChannelInfoViewController: SimpleButtonTableViewCellDelegate {
    func simpleButtonTableViewCell(onTap simpleButtonTableViewCell: SimpleButtonTableViewCell) {
        self.delegate?.streamChannelInfoViewController(toSelects: self)
    }
}

extension StreamChannelInfoViewController: StreamChannelInfoPresenterOutput {
    func sync(didFetchRepository repository: StreamChannelEntity) {
        self.setRepository(repository)
    }
    
    func sync(didFetchFolder repository: FolderEntity) {
        if let reference = self.repository.LiveStream?.FileReference, reference.Folder == repository {
            reference.Folder = repository
            self.folder = repository
            self.tableView.reloadData()
        } else {
            self.folder = repository
            self.tableView.reloadData()
        }
    }
    
    func streamChannelInfoPresenter(didFetch repositories: FileReferenceEntities, in streamChannelInfoPresenter: StreamChannelInfoPresenter) {
        self.repositories = repositories
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    func streamChannelInfoPresenter(didIncrement repositories: FileReferenceEntities, in streamChannelInfoPresenter: StreamChannelInfoPresenter) {
        self.repositories = repositories
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    func streamChannelInfoPresenter(didDecrement repositories: FileReferenceEntities, in streamChannelInfoPresenter: StreamChannelInfoPresenter) {
        self.repositories = repositories
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
    }
}
