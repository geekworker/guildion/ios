//
//  PlansViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit
import FSPagerView

class PlansViewController: BaseViewController {
    @IBOutlet weak var planCarouselView: PlanCarouselView!
    @IBOutlet weak var tableView: UITableView!
    
    enum Section: Int {
        case plans
        case functions
        case count
        
        var count: Int {
            switch self {
            case .plans: return PlansRow.count.rawValue
            case .functions: return PlanConfig.PlanFunction.allCases.count
            case .count: return 0
            }
        }
    }
    
    enum PlansRow: Int {
        case content
        case count
    }
    
    var presenter: PlansPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: SubscriptionPlanModel = SubscriptionPlanModel()
    
    override func viewDidLoad() {
        self.navigationMode = .modal
        super.viewDidLoad()
        PlansViewControllerBuilder.rebuild(self)
        self.configureTableView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.view.backgroundColor = Theme.themify(key: .background)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension PlansViewController: UITableViewDelegate {
    fileprivate func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.register([
            UINib(resource: R.nib.plansTableViewCell): R.nib.plansTableViewCell.name,
            UINib(resource: R.nib.planDetailTableViewCell): R.nib.planDetailTableViewCell.name,
        ])
        self.tableView.register(
            UINib(resource: R.nib.textTableViewHeaderFooterView),
            forHeaderFooterViewReuseIdentifier: R.nib.textTableViewHeaderFooterView.name
        )
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
    }
}

extension PlansViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section.init(rawValue: section) else { return 0 }
        return section.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section.init(rawValue: indexPath.section) else { return UITableViewCell() }
        switch section {
        case .plans: return self.tableView(self.tableView, cellForPlansRowAt: indexPath)
        case .functions: return self.tableView(self.tableView, cellForPlanFunctionRowAt: indexPath)
        case .count: return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForPlansRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.plansTableViewCell.name, for: indexPath) as? PlansTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForPlanFunctionRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.planDetailTableViewCell.name, for: indexPath) as? PlanDetailTableViewCell, let repository = PlanConfig.PlanFunction.allCases[safe: indexPath.row] else {
            fatalError("cell is not exists")
        }
        cell.setRepository(self.repository, at: repository)
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = Section.init(rawValue: section) else { return UIView() }
        switch section {
        case .functions:
            guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.nib.textTableViewHeaderFooterView.name) as? TextTableViewHeaderFooterView else { return UIView() }
            view.backgroundColor = Theme.themify(key: .background)
            view.contentView.backgroundColor = Theme.themify(key: .background)
            view.titleLabel.text = "\(repository.plan.string) \(R.string.localizable.planFunctions())"
            return view
        default: return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let section = Section.init(rawValue: section) else { return 0 }
        switch section {
        case .functions: return TextTableViewHeaderFooterView.height
        default: return 0
        }
    }
}

extension PlansViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
    }
}

extension PlansViewController: PlansTableViewCellDelegate {
    func plansTableViewCell(toBilling repository: SubscriptionPlanModel, in plansTableViewCell: PlansTableViewCell) {
        self.repository = repository
    }
    
    func plansTableViewCell(sync repository: SubscriptionPlanModel, in plansTableViewCell: PlansTableViewCell) {
        self.repository = repository
        self.tableView.reloadData()
    }
    
    func plansTableViewCell(shouldDismiss plansTableViewCell: PlansTableViewCell) {
        self.dismissShouldAppear(animated: true)
    }
}

extension PlansViewController: PlansPresenterOutput {
    func sync(didFetchRepository repository: SubscriptionPlanModel) {
        self.repository = repository
    }
}
