//
//  GuildShareViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import EFQRCode
import PanModal
import PKHUD
import Nuke
import Lightbox
import SwiftMessages

class GuildShareViewController: BaseViewController {
    @IBOutlet weak var headerView: UIView! {
        didSet {
            headerView.backgroundColor = Theme.themify(key: .backgroundThick)
            headerView.layer.cornerRadius = 8
            headerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .background)
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = R.string.localizable.guildInviteTitle()
            titleLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.text = R.string.localizable.guildInviteDesc()
            descriptionLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var saveButton: IconButton! {
        didSet {
            saveButton.buttonImage = R.image.download()!.withRenderingMode(.alwaysTemplate)
            saveButton.imageColor = Theme.themify(key: .backgroundContrast)
            saveButton.hoverColor = Theme.themify(key: .backgroundContrast)
            saveButton.hoverAlpha = 0.3
            saveButton.layer.cornerRadius = saveButton.frame.width / 2
            saveButton.clipsToBounds = true
            saveButton.buttonInset = 8
            saveButton.delegate = self
        }
    }
    @IBOutlet weak var fullscreenButton: IconButton! {
        didSet {
            fullscreenButton.buttonImage = R.image.fullscreen()!.withRenderingMode(.alwaysTemplate)
            fullscreenButton.imageColor = Theme.themify(key: .backgroundContrast)
            fullscreenButton.hoverColor = Theme.themify(key: .backgroundContrast)
            fullscreenButton.hoverAlpha = 0.3
            fullscreenButton.layer.cornerRadius = fullscreenButton.frame.width / 2
            fullscreenButton.clipsToBounds = true
            fullscreenButton.buttonInset = 8
            fullscreenButton.delegate = self
        }
    }
    @IBOutlet weak var idFieldView: UIView!
    @IBOutlet weak var idLabel: UILabel! {
        didSet {
            idLabel.text = ""
            idLabel.textColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var idDescriptionLabel: UILabel! {
        didSet {
            idDescriptionLabel.text = R.string.localizable.guildID()
            idDescriptionLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var idIconButton: IconButton! {
        didSet {
            idIconButton.buttonImage = R.image.search()!.withRenderingMode(.alwaysTemplate)
            idIconButton.imageColor = Theme.themify(key: .main)
            idIconButton.hoverColor = Theme.themify(key: .backgroundContrast)
            idIconButton.hoverAlpha = 0.3
            idIconButton.layer.cornerRadius = idIconButton.frame.width / 2
            idIconButton.clipsToBounds = true
            idIconButton.delegate = self
        }
    }
    @IBOutlet weak var idBorderView: UIView!
    @IBOutlet weak var urlFieldView: UIView!
    @IBOutlet weak var urlLabel: UILabel! {
        didSet {
            urlLabel.text = ""
            urlLabel.textColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var urlDescriptionLabel: UILabel! {
        didSet {
            urlDescriptionLabel.text = R.string.localizable.guildURL()
            urlDescriptionLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var urlIconButton: IconButton! {
        didSet {
            urlIconButton.buttonImage = R.image.link()!.withRenderingMode(.alwaysTemplate)
            urlIconButton.imageColor = Theme.themify(key: .main)
            urlIconButton.hoverColor = Theme.themify(key: .backgroundContrast)
            urlIconButton.hoverAlpha = 0.3
            urlIconButton.layer.cornerRadius = urlIconButton.frame.width / 2
            urlIconButton.clipsToBounds = true
            urlIconButton.delegate = self
        }
    }
    @IBOutlet weak var urlBorderView: UIView!
    @IBOutlet weak var shareButton: SimpleButton! {
        didSet {
            shareButton.normalColor = Theme.themify(key: .main)
            shareButton.textColor = Theme.themify(key: .string)
            shareButton.text = R.string.localizable.guildInviteShare()
            shareButton.delegate = self
        }
    }
    public lazy var activityHandler: ActivityHandler = {
        let activityHandler = ActivityHandler()
        activityHandler.delegate = self
        return activityHandler
    }()
    var presenter: GuildSharePresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: GuildEntity = GuildEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildShareViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        updateLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func updateLayout() {
        self.idLabel.text = "@\(repository.uid)"
        self.urlLabel.text = "\(Constant.APP_URL)/guild/welcome/\(repository.uid)"
        self.qrCodeImageView.image = generateQRCode()
    }
    
    func generateQRCode() -> UIImage? {
        if let tryImage = EFQRCode.generate(
            content: "\(Constant.APP_URL)/guild/welcome/\(repository.uid)",
            watermark: repository.picture_small == "" ? R.image.brandLogo()!.cgImage : UIImage(url: repository.picture_small).cgImage
        ) {
            return UIImage(cgImage: tryImage)
        } else {
            return nil
        }
    }
}

extension GuildShareViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return scrollView
    }
    
    var shortFormHeight: PanModalHeight {
        return longFormHeight
    }
    
//    var scrollIndicatorInsets: UIEdgeInsets {
//        let bottomOffset = presentingViewController?.bottomLayoutGuide.length ?? 0
//        return UIEdgeInsets(top: headerView.frame.size.height, left: 0, bottom: bottomOffset, right: 0)
//    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    func willTransition(to state: PanModalPresentationController.PresentationState) {
        guard case .shortForm = state
            else { return }

        self.panModalTransition(to: .longForm)
        panModalSetNeedsLayoutUpdate()
    }
}

extension GuildShareViewController: IconButtonDelegate {
    func iconButton(onTap iconButton: IconButton) {
        switch iconButton {
        case fullscreenButton:
            guard let image = qrCodeImageView.image else { return }
            let lImage = LightboxImage(
                image: image,
                text: repository.name
            )
            let controller = LightboxController(images: [lImage])
            controller.dynamicBackground = true
            controller.modalPresentationStyle = .currentContext
            controller.dismissalDelegate = self
            self.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(controller, animated: true, completion: nil)
            })
        case saveButton: UIImageWriteToSavedPhotosAlbum(qrCodeImageView.image!, self, #selector(self.showResultOfSaveImage(_:didFinishSavingWithError:contextInfo:)), nil)
        case idIconButton:
            UIPasteboard.general.string = idLabel.text
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.success)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successCopy())
            messageAlert.button?.isHidden = true
            var messageAlertConfig = SwiftMessages.defaultConfig
            messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
        case urlIconButton:
            UIPasteboard.general.string = urlLabel.text
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.success)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successCopy())
            messageAlert.button?.isHidden = true
            var messageAlertConfig = SwiftMessages.defaultConfig
            messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
        default: break
        }
    }
    
    @objc func showResultOfSaveImage(_ image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutableRawPointer) {
        if let unwrapped = error {
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.error)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.error(), body: String(describing: unwrapped))
            messageAlert.button?.isHidden = true
            var messageAlertConfig = SwiftMessages.defaultConfig
            messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
        } else {
            let messageAlert = MessageView.viewFromNib(layout: .cardView)
            messageAlert.configureTheme(.success)
            messageAlert.configureDropShadow()
            messageAlert.configureContent(title: R.string.localizable.success(), body: R.string.localizable.successSave())
            messageAlert.button?.isHidden = true
            var messageAlertConfig = SwiftMessages.defaultConfig
            messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
        }
    }
}

extension GuildShareViewController: LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {}
    
    func lightboxControllerDidDismiss(_ controller: LightboxController) {
        UIViewController.topMostController().presentPanModal(self)
    }
}

extension GuildShareViewController: ActivityHandlerDelegate {
    var activityItems: [Any] {
        [URL(string: urlLabel.text!)!, qrCodeImageView.image!]
    }
    
    var activityParent: UIViewController {
        return self
    }
}

extension GuildShareViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.activityHandler.show()
    }
}

extension GuildShareViewController: GuildSharePresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
    }
}

