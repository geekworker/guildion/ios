//
//  GuildsViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/07.
//

import UIKit
import ReSwift
import Parchment
import NotificationBannerSwift
import AVFoundation

protocol GuildsViewControllerDelegate: class {
    func guildsViewController(didFetch repositories: GuildEntities, in guildsViewController: GuildsViewController)
}

class GuildsViewController: BaseViewController {
   var presenter: GuildsPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: GuildEntities = GuildEntities()
    weak var delegate: GuildsViewControllerDelegate?
    var temp: GuildEntity?
    private var pagingViewController = ImagePagingViewController()
    private var viewControllers: [Int: GuildViewController] = [:]
    private var selectedChannel: ChannelableProtocol?
    private var selectedGuild: GuildEntity?
    private var selectedMember: MemberEntity?
    private var selectedPermission: PermissionModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationMode = .guilds
        title = R.string.localizable.home()
        GuildsViewControllerBuilder.rebuild(self)
        configurePaging()
        updateMenu(height: ImagePagingCell.menuHeight)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.hidesBottomBarWhenPushed = false
    }
    
    override func viewWebSocketHealthChecked(_ health: Bool) {
        super.viewWebSocketHealthChecked(health)
        presenter?.fetch()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func onTapNavigationEditButton(_ sender: Any) {
        self.presenter?.toGuildsEdit()
    }
    
    override func onTapNavigationAddButton(_ sender: Any) {
        self.presenter?.toGuildNew()
    }
    
    override func onTapNavigationQRScannerButton(_ sender: Any) {
        self.presenter?.toGuildQRCodeReader()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? TextChannelViewController, segue.identifier == R.segue.guildsViewController.toTextChannel.identifier, let repository = selectedChannel as? TextChannelEntity, let current_guild = selectedGuild, let current_member = selectedMember, let permission = selectedPermission {
            TextChannelViewControllerBuilder.rebuild(vc)
            vc.presenter?.repositories = repository
            vc.initalizeRepository(repository, current_guild: current_guild, current_member: current_member, permission: permission)
            self.selectedChannel = nil
            self.selectedGuild = nil
            self.selectedMember = nil
            self.selectedPermission = nil
        } else if let vc = segue.destination as? DMChannelViewController, segue.identifier == R.segue.guildsViewController.toDMChannel.identifier, let repository = selectedChannel as? DMChannelEntity, let current_guild = selectedGuild, let current_member = selectedMember, let permission = selectedPermission {
            DMChannelViewControllerBuilder.rebuild(vc)
            vc.presenter?.repositories = repository
            vc.initalizeRepository(repository, current_guild: current_guild, current_member: current_member, permission: permission)
            self.selectedChannel = nil
            self.selectedGuild = nil
            self.selectedMember = nil
            self.selectedPermission = nil
        } else if let vc = segue.destination as? StreamChannelViewController, segue.identifier == R.segue.guildsViewController.toStreamChannel.identifier, let repository = selectedChannel as? StreamChannelEntity, let current_guild = selectedGuild, let current_member = selectedMember, let permission = selectedPermission {
            StreamChannelViewControllerBuilder.rebuild(vc)
            vc.initalizeRepository(repository, current_guild: current_guild, current_member: current_member, permission: permission)
            self.selectedChannel = nil
            self.selectedGuild = nil
            self.selectedMember = nil
            self.selectedPermission = nil
        } else if let vc = segue.destination as? FoldersViewController, segue.identifier == R.segue.guildsViewController.toPlaylists.identifier, let repository = selectedGuild, let current_member = selectedMember, let permission = selectedPermission {
            vc.current_guild = repository
            vc.current_member = current_member
            vc.permission = permission
            vc.is_playlist = true
            self.selectedGuild = nil
            self.selectedMember = nil
            self.selectedPermission = nil
        } else if let vc = segue.destination as? FoldersViewController, segue.identifier == R.segue.guildsViewController.toFolders.identifier, let repository = selectedGuild, let current_member = selectedMember, let permission = selectedPermission {
            vc.current_guild = repository
            vc.current_member = current_member
            vc.permission = permission
            vc.is_playlist = false
            self.selectedGuild = nil
            self.selectedMember = nil
            self.selectedPermission = nil
        } else if let vc = segue.destination as? GuildMemberRequestsViewController, segue.identifier == R.segue.guildsViewController.toMemberRequests.identifier, let repositories = selectedGuild?.MemberRequests, let repository = selectedGuild {
            repositories.items = repositories.compactMap({ $0.Guild = selectedGuild?.toNewMemory(); return $0 })
            vc.current_guild = repository
            selectedGuild = nil
        } else if let vc = segue.destination as? MembersViewController, segue.identifier == R.segue.guildsViewController.toMembers.identifier, let repository = selectedGuild {
            vc.repository = repository
            selectedGuild = nil
        }
    }
    
    func setRepositories(_ repositories: GuildEntities) {
        self.repositories = repositories
        self.viewControllers = [:]
        updateLayout()
    }
    
    func setTemp(_ repository: GuildEntity) {
        self.temp = repository
        GuildsViewControllerBuilder.rebuild(self)
        self.presenter?.fetch(repository.uid)
    }
    
    func setSelectedGuild(_ repository: GuildEntity) {
        guard let index = repositories.indexOf(repository) else { return }
        self.pagingViewController.select(index: index)
    }
    
    func updateLayout() {
        self.pagingViewController.reloadData()
    }
    
    func configurePaging() {
        pagingViewController.register(ImagePagingCell.self, for: ImagePagingModel.self)
        pagingViewController.menuItemSize = .fixed(width: ImagePagingCell.menuItemSize.width, height: ImagePagingCell.menuItemSize.height)
        pagingViewController.menuItemSpacing = 8
        pagingViewController.menuInsets = ImagePagingCell.menuInsets
        pagingViewController.borderColor = Theme.themify(key: .border)
        pagingViewController.indicatorColor = Theme.themify(key: .borderSecondary)
        pagingViewController.indicatorOptions = .visible(
            height: 3,
            zIndex: Int.max,
            spacing: UIEdgeInsets.zero,
            insets: UIEdgeInsets.zero
        )
        pagingViewController.borderOptions = .hidden
//        pagingViewController.borderOptions = .visible(
//            height: 1,
//            zIndex: Int.max - 1,
//            insets: UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 18)
//        )
        addChild(pagingViewController)
        view.addSubview(pagingViewController.view)
        view.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        pagingViewController.dataSource = self
        pagingViewController.delegate = self
        pagingViewController.menuBackgroundColor = Theme.themify(key: .backgroundSecondary)
        if let first = repositories.first {
            pagingViewController.select(pagingItem: Guild_ImagePagingItemTranslator.translate(first))
        }
    }
    
    private func updateMenu(height: CGFloat) {
        guard let menuView = pagingViewController.view as? ImagePagingView else { return }
        menuView.menuHeightConstraint?.constant = height
        pagingViewController.menuItemSize = .fixed(
            width: ImagePagingCell.menuItemSize.width,
            height: height - ImagePagingCell.menuInsets.top - ImagePagingCell.menuInsets.bottom
        )
        pagingViewController.collectionViewLayout.invalidateLayout()
        pagingViewController.collectionView.layoutIfNeeded()
    }
}

extension GuildsViewController: PagingViewControllerDataSource {
    func pagingViewController(_: PagingViewController, viewControllerAt index: Int) -> UIViewController {
        if let unwrapped = temp, index != 0 {
            self.temp = nil
            self.repositories.items = self.repositories.filter({ $0 != unwrapped })
            self.updateLayout()
        }
        
        if let viewController = viewControllers[index] {
            let insets = UIEdgeInsets(top: ImagePagingCell.menuHeight, left: 0, bottom: 0, right: 0)
            viewController.tableView.contentInset = insets
            viewController.tableView.scrollIndicatorInsets = insets
            return viewController
        } else {
            let viewController = R.storyboard.guilds.guildViewController()!
            viewController.delegate = self
            viewController.repository = repositories[safe: index] ?? GuildEntity()
            viewControllers[index] = viewController
            return viewController
        }
    }
  
    func pagingViewController(_ pagingViewController: PagingViewController, pagingItemAt index: Int) -> PagingItem {
        return Guild_ImagePagingItemTranslator.translate(repositories.items[index])
    }
      
    func numberOfViewControllers(in pagingViewController: PagingViewController) -> Int {
        return repositories.count
    }
}

extension GuildsViewController: GuildViewControllerDelegate {
    func guildViewControllerDidScroll(_ guildViewController: GuildViewController) {
        let height = ImagePagingCell.calculateMenuHeight(for: guildViewController.tableView)
        updateMenu(height: height)
    }
    
    func guildViewController(didSelectChannel channel: ChannelableProtocol, in guildViewController: GuildViewController) {
        self.selectedChannel = channel
        self.presenter?.toChannel(channel, current_guild: guildViewController.repository, current_member: guildViewController.current_member, permission: guildViewController.permission)
    }
    
    func guildViewController(didSelectSectionRow row: SectionChannelRowable, in guildViewController: GuildViewController) {
        switch row.sectionChannelRowType {
        case .Participant:
            guard let participant = row as? ParticipantEntity, let member = participant.Member else { return }
            self.presenter?.toMemberMenu(repository: member, current_guild: guildViewController.repository, current_member: guildViewController.current_member, permission: guildViewController.permission)
        case .Stream: break
        case .DMChannel, .TextChannel:
            guard let channel = row as? ChannelableProtocol else { return }
            self.selectedChannel = channel
            self.selectedMember = guildViewController.current_member
            self.selectedGuild = guildViewController.repository
            self.selectedPermission = guildViewController.permission
            self.presenter?.toChannel(channel, current_guild: guildViewController.repository, current_member: guildViewController.current_member, permission: guildViewController.permission)
        case .StreamChannel:
            guard let channel = row as? ChannelableProtocol, let streamChannel = row as? StreamChannelEntity else { return }
            self.selectedChannel = channel
            self.selectedPermission = guildViewController.permission
            
            if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController, vc.repository == streamChannel {
                guildViewController.viewDidDisappear(true)
                navVC.modalPresentationStyle = .overCurrentContext
                MiniScreenView.shared?.dismiss(animated: true)
                UIViewController.topMostController().present(navVC, animated: true, completion: nil)
            } else if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController {
                vc.unbind()
                MiniScreenView.shared?.dismiss(animated: true)
                LiveConfig.playingViewController = nil
                guildViewController.viewDidDisappear(true)
                self.presenter?.toChannel(channel, current_guild: guildViewController.repository, current_member: guildViewController.current_member, permission: guildViewController.permission)
            } else {
                guildViewController.viewDidDisappear(true)
                self.presenter?.toChannel(channel, current_guild: guildViewController.repository, current_member: guildViewController.current_member, permission: guildViewController.permission)
            }
        }
    }
    
    func guildViewController(didSelectMembers  repository: GuildEntity, guildViewController: GuildViewController) {
        self.selectedGuild = repository
        self.selectedPermission = guildViewController.permission
        self.presenter?.toMembers()
    }
    
    func guildViewController(didSelectFolders guildViewController: GuildViewController) {
        self.selectedGuild = guildViewController.repository
        self.selectedMember = guildViewController.current_member
        self.selectedPermission = guildViewController.permission
        self.presenter?.toFolders()
    }
    
    func guildViewController(didSelectPlaylists guildViewController: GuildViewController) {
        self.selectedGuild = guildViewController.repository
        self.selectedMember = guildViewController.current_member
        self.selectedPermission = guildViewController.permission
        self.presenter?.toPlaylists()
    }
    
    func guildViewController(didSelectOpenSetting repository: GuildEntity, guildViewController: GuildViewController) {
    }
    
    func guildViewController(didSelectMemberRequests repository: GuildEntity, guildViewController: GuildViewController) {
        self.selectedGuild = repository
        self.presenter?.toMemberRequests()
    }
    
    func guildViewControllerWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        self.switchNavigationBarScrollViewWillEnd(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
}

extension GuildsViewController: PagingViewControllerDelegate {
    func pagingViewController(_: PagingViewController, isScrollingFromItem currentPagingItem: PagingItem, toItem upcomingPagingItem: PagingItem?, startingViewController: UIViewController, destinationViewController: UIViewController?, progress: CGFloat) {
        guard let destinationViewController = destinationViewController as? GuildViewController else { return }
        guard let startingViewController = startingViewController as? GuildViewController else { return }
        let from = ImagePagingCell.calculateMenuHeight(for: startingViewController.tableView)
        let to = ImagePagingCell.calculateMenuHeight(for: destinationViewController.tableView)
        let height = ((to - from) * abs(progress)) + from
        updateMenu(height: height)
    }
}

extension GuildsViewController: GuildsPresenterOutput {
    func sync(didFetchRepositories repositories: GuildEntities) {
        guard self.repositories != repositories else { return }
        self.repositories = repositories
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.setRepositories(repositories)
            self.delegate?.guildsViewController(didFetch: repositories, in: self)
        }
    }
    
    func sync(didFetchRepository repository: GuildEntity) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
        }
        self.temp = repository
        self.repositories.items.insert(repository, at: 0)
        self.viewControllers = [:]
        updateLayout()
        self.pagingViewController.select(index: 0, animated: false)
    }
    
    func guildsPresenter(didRetry count: Int, in guildsPresenter: GuildsPresenter) {
        DispatchQueue.main.async {
            self.view.makeToast(R.string.localizable.domainErrorDetail())
        }
    }
}
