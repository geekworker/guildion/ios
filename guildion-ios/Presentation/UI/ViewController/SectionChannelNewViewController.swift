//
//  SectionChannelNewViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import PKHUD

class SectionChannelNewViewController: BaseViewController {
    var presenter: SectionChannelNewPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: SectionChannelEntity = SectionChannelEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        SectionChannelNewViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension SectionChannelNewViewController: SectionChannelNewPresenterOutput {
    func sectionChannelNewPresenter(onError error: Error) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func sectionChannelNewPresenter(didCreated repository: SectionChannelEntity) {
        HUD.hide()
        HUD.flash(.success)
    }
}
