//
//  FoldersViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/23.
//

import Foundation
import UIKit
import Nuke
import RxSwift
import RxCocoa
import PKHUD
import Lightbox

class FoldersViewController: BaseViewController, PanContainerPresenter {
    var containerTransitionContext: PanContainerViewTransitionContext = PanContainerViewTransitionContext()
    @IBOutlet weak var dragHintLabel: UILabel!
    @IBOutlet weak var overView: UIView! {
        didSet {
            overView.isHidden = true
            overView.alpha = 1
            overView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        }
    }
    @IBOutlet weak var visualEffectView: UIVisualEffectView! {
        didSet {
        }
    }
    @IBOutlet weak var foldersView: UIView!
    @IBOutlet weak var foldersCollectionView: UICollectionView!
    @IBOutlet weak var foldersViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var foldersViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var filesCollectionView: UICollectionView!
    @IBOutlet weak var actionButton: ActionButton! {
        didSet {
            actionButton.delegate = self
            actionButton.isHidden = !permission.Guild.file_creatable
        }
    }
    var presenter: FoldersPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public var _repositories: FileEntities = FileEntities()
    public var repositories: FileEntities {
        get {
            _repositories.items += caches.filter({ !_repositories.contains($0) })
            return _repositories
        }
        set {
            _repositories = newValue
            _repositories.items = _repositories.items.unique
        }
    }
    public var caches: FileEntities = FileEntities()
    public var selecteds: [FileEntity] = []
    public var current_member: MemberEntity = MemberEntity()
    public var current_guild: GuildEntity = GuildEntity()
    public var permission: PermissionModel = PermissionModel()
    public var folders: FolderEntities = FolderEntities()
    public var selectedFolder: FolderEntity?
    public enum FolderMode: Int {
        case on = 100
        case off = 0
    }
    public var folderMode: FolderMode = .on
    public var is_playlist: Bool = false
    
    private var loadingDecrementStatus: Int?
    private var loadingIncrementStatus: Int?
    private var lastWillDisplay: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationMode = permission.Guild.files_managable ? .edit : .other
        title = is_playlist ? R.string.localizable.playlists() : R.string.localizable.folders()
        FoldersViewControllerBuilder.rebuild(self)
        configureCollectionView()
        configureEditModeTabBar()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        presenter?.join()
        presenter?.fetch()
        presenter?.fetchFolders()
        
        guard navigationItem.rightBarButtonItems?.count == 1 && permission.Guild.folders_managable else { return }
        let setting = UIBarButtonItem(image: R.image.listEdit()!.withRenderingMode(.alwaysTemplate).resizedImage(newSize: CGSize(width: 18, height: 14)), style: .plain, target: self, action: #selector(self.onTapSettingButton(_:)))
        navigationItem.rightBarButtonItems?.append(setting)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if repositories.count > 0 {
            self.repositories = FileDispatcher.binds(contents: self.repositories, state: store.state.file)
            self.filesCollectionView.reloadData()
        }
        presenter?.increments()
        loadingDecrementStatus = nil
        loadingIncrementStatus = nil
        lastWillDisplay = nil
    }
    
    override func viewWebSocketHealthChecked(_ health: Bool) {
        super.viewWebSocketHealthChecked(health)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
        self.setEditing(false, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FolderViewController, segue.identifier == R.segue.foldersViewController.toFolder.identifier, let repository = selectedFolder {
            FolderViewControllerBuilder.rebuild(vc)
            vc.initRepository(repository, current_guild: current_guild, current_member: current_member)
            vc.repository = repository
        } else if let vc = segue.destination as? PlaylistViewController, segue.identifier == R.segue.foldersViewController.toPlaylist.identifier, let repository = selectedFolder {
            PlaylistViewControllerBuilder.rebuild(vc)
            vc.initRepository(repository, current_guild: current_guild, current_member: current_member, permission: permission)
            vc.repository = repository
        }
    }
    
    @objc func onTapSettingButton(_ sender: Any) {
        self.presenter?.toFoldersEdit()
    }
    
    @objc func singleTapGesture(_ sender: Any) {
    }
    
    func toFolderNew() {
        let ac = UIAlertController(title: is_playlist ? R.string.localizable.playlistNew() : R.string.localizable.folderNew(), message: nil, preferredStyle: .alert)
        let ok = UIAlertAction(title: R.string.localizable.save(), style: .default, handler: {[unowned ac] (action) -> Void in
            guard let textFields = ac.textFields, !textFields.isEmpty else {
                return
            }

            for textField in textFields {
                if textField.tag == 1 {
                    guard let text = textField.text, text.count > 0 else {
                        HUD.flash(.error)
                        return
                    }
                    let entity = FolderEntity()
                    entity.Guild = self.current_guild.toNewMemory()
                    entity.GuildId = self.current_guild.id
                    entity.Member = self.current_member.toNewMemory()
                    entity.MemberId = self.current_member.id
                    entity.is_playlist = self.is_playlist
                    entity.name = text
                    HUD.show(.progress)
                    self.presenter?.create(entity)
                }
            }
        })
        
        let cancel = UIAlertAction(title: R.string.localizable.cancel(), style: .cancel, handler: nil)
        ac.addTextField(configurationHandler: { [unowned self] (textField: UITextField!) -> Void in
            textField.tag  = 1
            textField.rx.text.subscribe(onNext: { text in
                ok.isEnabled = text?.count ?? 0 > 0
            }).disposed(by: self.disposeBag)
        })
        ok.isEnabled = false
        ac.addAction(ok)
        ac.addAction(cancel)
        present(ac, animated: true, completion: nil)
    }
}

extension FoldersViewController: UICollectionViewDelegate {
    private func configureCollectionView() {
        let glayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        glayout.minimumLineSpacing = FolderCollectionViewCell.inset.left
        glayout.minimumInteritemSpacing = FolderCollectionViewCell.inset.left
        glayout.scrollDirection = .horizontal
        let height: CGFloat = 80 + FolderCollectionViewCell.inset.top + FolderCollectionViewCell.inset.bottom
        glayout.itemSize = FolderCollectionViewCell.getSize(height: height)
        foldersViewHeightConstraint.constant = height
        foldersCollectionView.collectionViewLayout = glayout
        foldersCollectionView.alwaysBounceHorizontal = true
        self.foldersCollectionView.register([
            UINib(resource: R.nib.addCollectionViewCell): R.nib.addCollectionViewCell.name,
            UINib(resource: R.nib.folderCollectionViewCell): R.nib.folderCollectionViewCell.name,
        ])
        self.foldersCollectionView.delegate = self
        self.foldersCollectionView.dataSource = self
        self.foldersCollectionView.prefetchDataSource = self
        self.foldersCollectionView.dropDelegate = self
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(singleTapGesture(_:)))
        singleTap.cancelsTouchesInView = false
        foldersCollectionView.addGestureRecognizer(singleTap)
        
        let flayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flayout.minimumLineSpacing = FileCollectionViewCell.inset.left
        flayout.minimumInteritemSpacing = FileCollectionViewCell.inset.left
        flayout.itemSize = FileCollectionViewCell.getSize()
        flayout.scrollDirection = .vertical
        filesCollectionView.collectionViewLayout = flayout
        filesCollectionView.alwaysBounceVertical = true
        filesCollectionView.contentInset = FileCollectionViewCell.inset
        self.filesCollectionView.register([
            UINib(resource: R.nib.fileCollectionViewCell): R.nib.fileCollectionViewCell.name,
        ])
        self.filesCollectionView.contentInset = UIEdgeInsets(top: height, left: 0, bottom: 0, right: 0)
        self.filesCollectionView.delegate = self
        self.filesCollectionView.dataSource = self
        self.filesCollectionView.prefetchDataSource = self
        self.filesCollectionView.dragDelegate = self
        self.filesCollectionView.addGestureRecognizer(singleTap)
        self.filesCollectionView.dragInteractionEnabled = permission.Guild.folders_managable
        
        self.foldersCollectionView.backgroundColor = Theme.themify(key: .transparent)
        self.filesCollectionView.backgroundColor = Theme.themify(key: .background)
        
        self.filesCollectionView.rx.verticalScrollDirection.subscribe(
            onNext: { [unowned self] result in
                guard self.filesCollectionView.frame.height < self.filesCollectionView.contentSize.height else {
                    self.switchFolderContainer(show: true)
                    return
                }
                let offsetY = self.filesCollectionView.contentOffset.y
                switch result {
                case .Down:
                    guard offsetY < self.filesCollectionView.contentSize.height - self.view.frame.height &&  self.foldersViewTopConstraint.constant == -CGFloat(FolderMode.on.rawValue) else { return }
                    DispatchQueue.main.async { [unowned self] in
                        self.switchFolderContainer(show: true)
                    }
                case .Up:
                    guard offsetY > -self.filesCollectionView.contentInset.top && self.foldersViewTopConstraint.constant == CGFloat(FolderMode.off.rawValue) else { return }
                    DispatchQueue.main.async { [unowned self] in
                        self.switchFolderContainer(show: false)
                    }
                default: break
                }
            }
        ).disposed(by: disposeBag)
        
        self.filesCollectionView.rx.didScroll.subscribe(
            onNext: { [unowned self] result in
                self.scrollViewDidScroll(self.filesCollectionView)
            }
        ).disposed(by: disposeBag)
    }
    
    func switchFolderContainer(show: Bool, animated: Bool = true) {
        self.folderMode = show ? .on : .off
        self.foldersViewTopConstraint.constant = show ? CGFloat(FolderMode.off.rawValue) : -CGFloat(FolderMode.on.rawValue)
        UIView.animate(withDuration: animated ? 0.2 : 0, delay: 0, options: UIView.AnimationOptions(), animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let upcasted = cell as? FileCollectionViewCell {
            self.collectionView(filesCollectionView: filesCollectionView, willDisplay: upcasted, forItemAt: indexPath)
        } else if let upcasted = cell as? FolderCollectionViewCell {
            self.collectionView(foldersCollectionView: foldersCollectionView, willDisplay: upcasted, forItemAt: indexPath)
        }
    }
    
    fileprivate func collectionView(foldersCollectionView: UICollectionView, willDisplay cell: FolderCollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    fileprivate func collectionView(filesCollectionView: UICollectionView, willDisplay cell: FileCollectionViewCell, forItemAt indexPath: IndexPath) {
        if repositories.count > indexPath.row, let repository = repositories[safe: indexPath.row], let last_id = repositories.last?.id, repository.id == last_id, last_id != loadingIncrementStatus {
            loadingIncrementStatus = last_id
            self.presenter?.increments()
        }
        lastWillDisplay = indexPath.section
        cell.setEditing(isEditing, animated: false)
        if isEditing, let repository = repositories[safe: indexPath.row] {
            if selecteds.contains(repository), !cell.isSelected {
                cell.isSelected = selecteds.contains(repository)
                filesCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: .top)
            } else {
                cell.isSelected = selecteds.contains(repository)
            }
        }
    }
}

extension FoldersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case foldersCollectionView: return self.collectionView(foldersCollectionView: collectionView, numberOfItemsInSection: section)
        case filesCollectionView: return self.collectionView(filesCollectionView: collectionView, numberOfItemsInSection: section)
        default: return 0
        }
    }
    
    func collectionView(foldersCollectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return permission.Guild.folders_viewable ? folders.count + 1 : 0
    }
    
    func collectionView(filesCollectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return permission.Guild.files_viewable ? repositories.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case foldersCollectionView: return indexPath.row == 0 ? self.collectionView(collectionView, cellFolderAddForItemAt: indexPath) : self.collectionView(collectionView, cellFolderForItemAt: indexPath)
        case filesCollectionView: return self.collectionView(collectionView, cellFileForItemAt: indexPath)
        default: return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellFolderAddForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.addCollectionViewCell.name, for: indexPath) as? AddCollectionViewCell else {
            fatalError("The dequeued cell is not an instance of CollectionViewCell.")
        }
        cell.delegate = self
        cell.isHidden = !permission.Guild.folder_creatable
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellFolderForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.folderCollectionViewCell.name, for: indexPath) as? FolderCollectionViewCell, let repository = folders[safe: indexPath.row - 1] else {
                    fatalError("The dequeued cell is not an instance of CollectionViewCell.")
                }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellFileForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.fileCollectionViewCell.name, for: indexPath) as? FileCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
                    fatalError("The dequeued cell is not an instance of CollectionViewCell.")
                }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case foldersCollectionView: return self.collectionView(foldersCollectionView: collectionView, didSelectItemAt: indexPath)
        case filesCollectionView: return self.collectionView(filesCollectionView: collectionView, didSelectItemAt: indexPath)
        default: break
        }
    }
    
    fileprivate func collectionView(foldersCollectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.setEditing(false, animated: true)
        foldersCollectionView.deselectItem(at: indexPath, animated: true)
        if indexPath.row == 0 {
            self.toFolderNew()
            return
        }
        guard let repository = folders[safe: indexPath.row - 1] else { return }
        self.selectedFolder = repository
        repository.is_playlist ? self.presenter?.toPlaylist() : self.presenter?.toFolder()
    }
    
    fileprivate func collectionView(filesCollectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing {
            if let baseTabBarController = self.tabBarController as? BaseTabBarController, isEditing, let repository = repositories[safe: indexPath.row], !repository.is_private {
                
                if self.selecteds.contains(repository) {
                    self.selecteds.remove(element: repository)
                    self.selecteds = self.selecteds.unique
                } else {
                    self.selecteds.append(repository)
                    self.selecteds = self.selecteds.unique
                }
                
                baseTabBarController.setEditCount(selecteds.count)
            }
        } else {
            filesCollectionView.deselectItem(at: indexPath, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        switch collectionView {
        case foldersCollectionView: return self.collectionView(foldersCollectionView: collectionView, didDeselectItemAt: indexPath)
        case filesCollectionView: return self.collectionView(filesCollectionView: collectionView, didDeselectItemAt: indexPath)
        default: break
        }
    }
    
    fileprivate func collectionView(foldersCollectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    fileprivate func collectionView(filesCollectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if isEditing {
            if let baseTabBarController = self.tabBarController as? BaseTabBarController, isEditing, let repository = repositories[safe: indexPath.row], !repository.is_private {
                
                if self.selecteds.contains(repository) {
                    self.selecteds.remove(element: repository)
                    self.selecteds = self.selecteds.unique
                } else {
                    self.selecteds.append(repository)
                    self.selecteds = self.selecteds.unique
                }
                
                baseTabBarController.setEditCount(selecteds.count)
            }
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.filesCollectionView.allowsMultipleSelection = editing
        let indexPaths = self.filesCollectionView.indexPathsForVisibleItems
        for indexPath in indexPaths {
            if let cell = self.filesCollectionView.cellForItem(at: indexPath) as? FileCollectionViewCell {
                cell.setEditing(editing, animated: animated)
            }
            if !editing {
                self.filesCollectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        self.filesCollectionView.reloadData()
        
        if !editing {
            self.selecteds = []
        }
        
        if let baseTabBarController = self.tabBarController as? BaseTabBarController {
            baseTabBarController.switchEditMode(editing)
            baseTabBarController.setEditCount(selecteds.unique.count)
        }
    }
}

extension FoldersViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch collectionView {
        case foldersCollectionView: return self.collectionView(foldersCollectionView: collectionView, layout: collectionViewLayout, insetForSectionAt: section)
        case filesCollectionView: return self.collectionView(filesCollectionView: collectionView, layout: collectionViewLayout, insetForSectionAt: section)
        default: return .zero
        }
    }
    
    fileprivate func collectionView(foldersCollectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return FolderCollectionViewCell.inset
    }
    
    fileprivate func collectionView(filesCollectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return FileCollectionViewCell.inset
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case foldersCollectionView: return self.collectionView(collectionView, layout: collectionViewLayout, sizeForFolderItemAt: indexPath)
        case filesCollectionView: return self.collectionView(collectionView, layout: collectionViewLayout, sizeForFileItemAt: indexPath)
        default: return .zero
        }
    }
    
    fileprivate func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForFolderItemAt indexPath: IndexPath) -> CGSize {
        return indexPath.row == 0 ? (permission.Guild.folder_creatable ? AddCollectionViewCell.getSize() : .zero) : FolderCollectionViewCell.getSize(height: foldersViewHeightConstraint.constant - FolderCollectionViewCell.inset.top - FolderCollectionViewCell.inset.bottom)
    }
    
    fileprivate func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForFileItemAt indexPath: IndexPath) -> CGSize {
        guard let repository = repositories[safe: indexPath.row] else { return .zero }
        if isEditing && repository.is_private { return .zero }
        return (collectionViewLayout as! UICollectionViewFlowLayout).itemSize
    }
}

extension FoldersViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeFilesCacheRequests(indexPaths: indexPaths) + makeFoldersCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeFoldersCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if $0.row != 0, let picture = folders[safe: $0.row - 1]?.thumbnail, let url = URL(string: picture) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
    
    private func makeFilesCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let picture = repositories[safe: $0.row]?.thumbnail, let url = URL(string: picture) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension FoldersViewController: AddCollectionViewCellDelegate {
    func addCollectionViewCell(onTap addCollectionViewCell: AddCollectionViewCell) {
        self.toFolderNew()
    }
}

extension FoldersViewController: UIScrollViewDelegate {
    private func scrollViewDidScroll(_ filesCollectionView: UICollectionView) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                if self.filesCollectionView.contentOffset.y < -self.filesCollectionView.contentInset.top, self.loadingDecrementStatus == nil, let repository = self.repositories[safe: 0], let first_id = self.repositories.first?.id, repository.id == first_id, first_id != self.loadingDecrementStatus {
                    self.loadingDecrementStatus = first_id
                    self.presenter?.decrements()
                }
            }
        }
    }
}

extension FoldersViewController: FolderCollectionViewCellDelegate {
    func folderCollectionViewCell(onTap repository: FolderEntity, in folderCollectionViewCell: FolderCollectionViewCell) {
        self.selectedFolder = repository
        repository.is_playlist ? self.presenter?.toPlaylist() : self.presenter?.toFolder()
    }
}

extension FoldersViewController: FileCollectionViewCellDelegate {
    func fileCollectionViewCell(onProgressEnd repository: FileEntity, in fileCollectionViewCell: FileCollectionViewCell) {
        self.caches.items = caches.filter({ $0.progress ?? 0 < 1 })
    }
    
    func fileCollectionViewCell(onTap repository: FileEntity, in fileCollectionViewCell: FileCollectionViewCell) {
        self.presenter?.toFile(repository)
    }
}

extension FoldersViewController: BaseTabBarControllerEditDelegate {
    fileprivate func configureEditModeTabBar() {
        if let unwrapped = self.tabBarController as? BaseTabBarController {
            unwrapped.editDelegate = self
        }
    }
    
    func baseTabBarController(shouldTrash baseTabBarController: BaseTabBarController) {
        let entities = FileEntities()
        entities.items = selecteds
        self.presenter?.deleteFiles(repositories: entities)
        self.setEditing(false, animated: true)
        self.repositories = FileDispatcher.binds(contents: self.repositories, state: store.state.file)
        self.filesCollectionView.reloadData()
    }
    
    func baseTabBarController(shouldShare baseTabBarController: BaseTabBarController) {
    }
}

extension FoldersViewController: ActionButtonDelegate {
    func actionButton(onTap actionButton: ActionButton) {
        self.presenter?.toNewFile()
    }
}

extension FoldersViewController: UICollectionViewDragDelegate {
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        guard let cell = self.filesCollectionView.cellForItem(at: indexPath) as? FileCollectionViewCell, cell.repository.id != nil, !cell.repository.is_private else { return [] }
        let provider = NSItemProvider(object: cell.fileImageView.image ?? UIImage())
        let item = UIDragItem(itemProvider: provider)
        item.localObject = cell
        return [item]
    }
    
    func collectionView(_ collectionView: UICollectionView, dragSessionWillBegin session: UIDragSession) {
        self.overView.fadeIn(type: .Normal, completed: nil)
        self.switchFolderContainer(show: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, dragSessionDidEnd session: UIDragSession) {
        self.overView.fadeOut(type: .Normal, completed: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidExit session: UIDropSession) {
        self.overView.fadeOut(type: .Normal, completed: nil)
    }
}

extension FoldersViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard collectionView == foldersCollectionView, let destinationIndexPath = coordinator.destinationIndexPath, destinationIndexPath.row != 0, let folder = folders[safe: destinationIndexPath.row - 1], let cells = coordinator.items.compactMap({ $0.dragItem.localObject }) as? [FileCollectionViewCell] else { return }
        let entities = FileEntities()
        entities.items = cells.compactMap({ $0.repository })
        self.presenter?.createFileReferences(repositories: entities, to: folder)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        guard collectionView == foldersCollectionView, session.localDragSession != nil else { return UICollectionViewDropProposal(operation: .copy, intent: .insertIntoDestinationIndexPath) }
        return UICollectionViewDropProposal(operation: .copy, intent: .insertIntoDestinationIndexPath)
    }
}

extension FoldersViewController: FileNewViewControllerDelegate {
    func fileNewViewController(willCreate repositories: FileEntities, in fileNewViewController: FileNewViewController) {
        self.repositories.items += repositories.items
        self.filesCollectionView.reloadData()
    }
    
    func fileNewViewController(didCreate repositories: FileEntities) {
        self.presenter?.increments()
    }
}

extension FoldersViewController: FileMenuViewControllerDelegate {
    func toImageViewer(repository: FileEntity) {
        let lImage = LightboxImage(
            image: UIImage(url: repository.url),
            text: ""
        )
        let controller = LightboxController(images: [lImage])
        //controller.dismissalDelegate = self
        //controller.pageDelegate = self
        controller.dynamicBackground = true
        controller.modalPresentationStyle = .currentContext
        self.present(controller, animated: true, completion: nil)
    }
    
    func toWebBrowser(url: URL) {
        let webkitViewController = WebKitViewController(nibName: R.nib.webKitViewController.name, bundle: nil)
        let navVC = PanContainerViewNavigationController()
        navVC.shouldAdjustSafeArea = true
        navVC.viewControllers.append(webkitViewController)
        webkitViewController.defaultMedia = true
        webkitViewController.setURL(url)
        navVC.searchDelegate = webkitViewController
        navVC.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(navVC, animated: true)
    }
}

extension FoldersViewController: FoldersPresenterOutput {
    func foldersPresenter(didRetry count: Int, in foldersPresenter: FoldersPresenter) {
        DispatchQueue.main.async {
            self.view.makeToast(R.string.localizable.domainErrorDetail())
        }
    }
    
    func foldersPresenter(didCache repositories: FileEntities, in foldersPresenter: FoldersPresenter) {
        guard repositories.count > 0 else { return }
        DispatchQueue.main.async {
            self.caches = repositories
            self.filesCollectionView.reloadData()
        }
    }

    func foldersPresenter(didIncrement repositories: FileEntities, in foldersPresenter: FoldersPresenter) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            if repositories.count != 0, repositories.last?.id != self.loadingIncrementStatus {
                self.loadingIncrementStatus = nil
                self.repositories = repositories
                self.filesCollectionView.reloadKeepOffset()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
                    self?.switchFolderContainer(show: true, animated: false)
                }
            }
        }
    }
    
    func foldersPresenter(didDecrement repositories: FileEntities, in foldersPresenter: FoldersPresenter) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            if repositories.count != 0, repositories.first?.id != self.loadingDecrementStatus {
                self.loadingDecrementStatus = nil
                self.repositories = repositories
                self.filesCollectionView.reloadKeepOffset()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [unowned self] in
                    self.switchFolderContainer(show: true, animated: false)
                }
            }
        }
    }
    
    func sync(didFetchRepositories repositories: FileEntities) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            let shouldScroll = self.repositories.count == 0
            self.repositories = FileDispatcher.binds(contents: repositories, state: store.state.file)
            self.filesCollectionView.reloadData()
            if shouldScroll {
                self.filesCollectionView.scrollToBottomWith(animated: false)
                self.switchFolderContainer(show: true, animated: false)
            }
        }
    }
    
    func sync(didFetchFolders repositories: FolderEntities) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.folders = repositories
            self.foldersCollectionView.reloadData()
        }
    }
    
    func syncCurrentMember(didFetchRepository repository: MemberEntity) {
        self.current_member = repository
    }
    
    func foldersPresenter(didDelete foldersPresenter: FoldersPresenter) {
        DispatchQueue.main.async {
            self.repositories = FileDispatcher.binds(contents: self.repositories, state: store.state.file)
            self.filesCollectionView.reloadData()
        }
    }
    
    func foldersPresenter(didCreate repositories: FileReferenceEntities, in foldersPresenter: FoldersPresenter) {
        self.foldersCollectionView.reloadData()
    }
    
    func foldersPresenter(onError error: Error, in foldersPresenter: FoldersPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func foldersPresenter(didCreate repository: FolderEntity, in foldersPresenter: FoldersPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.selectedFolder = repository
        self.presenter?.toPlaylist()
    }
}
