//
//  FileSelectFromFolderViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/26.
//

import Foundation
import UIKit
import Nuke
import ReSwift

protocol FileSelectFromFolderViewControllerDelegate: class {
    func fileSelectFromFolderViewController(shouldDismiss selecteds: FileEntities, in fileSelectFromFolderViewController: FileSelectFromFolderViewController)
}

class FileSelectFromFolderViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    public var current_guild: GuildEntity = GuildEntity()
    public var repositories: FileEntities = FileEntities()
    public weak var delegate: FileSelectFromFolderViewControllerDelegate?
    public var selecteds: [FileEntity] = []
    public var is_playlist: Bool = false
    var presenter: FileSelectFromFolderPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    private var loadingDecrementStatus: Int?
    private var loadingIncrementStatus: Int?
    private var lastWillDisplay: Int?
    private var previousCount: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FileSelectFromFolderViewControllerBuilder.rebuild(self)
        configureCollectionView()
        self.navigationItem.title = R.string.localizable.selectItems("\(collectionView.indexPathsForSelectedItems?.count ?? 0)")
        navigationMode = .edit
        self.presenter?.fetch()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setEditing(true, animated: false)
        
        loadingDecrementStatus = nil
        loadingIncrementStatus = nil
        lastWillDisplay = nil
        previousCount = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.collectionView.allowsMultipleSelection = editing
        let indexPaths = self.collectionView.indexPathsForVisibleItems
        for indexPath in indexPaths {
            if let cell = self.collectionView.cellForItem(at: indexPath) as? FileCollectionViewCell {
                cell.setEditing(editing, animated: animated)
            }
            if !editing {
                self.collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        
        if !editing {
            let files: [FileEntity] = selecteds.unique,
                entities = FileEntities()
            entities.items = files
            self.delegate?.fileSelectFromFolderViewController(
                shouldDismiss: entities,
                in: self
            )
            self.dismissShouldAppear(animated: true)
        }
    }
}

extension FileSelectFromFolderViewController: UICollectionViewDelegate {
    private func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        self.collectionView.collectionViewLayout = layout
        self.collectionView.register([
            UINib(resource: R.nib.fileCollectionViewCell): R.nib.fileCollectionViewCell.name,
        ])
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.prefetchDataSource = self
        self.collectionView.backgroundColor = Theme.themify(key: .background)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing, let repository = repositories[safe: indexPath.row] {
            
            if self.selecteds.contains(repository) {
                self.selecteds.remove(element: repository)
                self.selecteds = self.selecteds.unique
            } else {
                self.selecteds.append(repository)
                self.selecteds = self.selecteds.unique
            }
            
            self.navigationItem.title = R.string.localizable.selectItems("\(selecteds.count)")
        } else {
            collectionView.deselectItem(at: indexPath, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if isEditing, let repository = repositories[safe: indexPath.row] {
            
            if self.selecteds.contains(repository) {
                self.selecteds.remove(element: repository)
                self.selecteds = self.selecteds.unique
            } else {
                self.selecteds.append(repository)
                self.selecteds = self.selecteds.unique
            }
            
            self.navigationItem.title = R.string.localizable.selectItems("\(selecteds.count)")
        }
    }
}

extension FileSelectFromFolderViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.fileCollectionViewCell.name, for: indexPath) as? FileCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
            return UICollectionViewCell()
        }
        cell.setRepository(repository)
        cell.setEditing(true, animated: false)
        cell.isSelected = selecteds.contains(repository)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.fileCollectionViewCell.name, for: indexPath) as? FileCollectionViewCell else {
            return
        }
        if repositories.count > indexPath.section, let repository = repositories[safe: indexPath.section], let last_id = repositories.last?.id, repository.id == last_id, last_id != loadingIncrementStatus {
            loadingIncrementStatus = last_id
            self.previousCount = repositories.count
            self.presenter?.increments()
        }
        lastWillDisplay = indexPath.section
        cell.setEditing(true, animated: false)
        
        if isEditing, let repository = repositories[safe: indexPath.row] {
            if selecteds.contains(repository), !cell.isSelected {
                cell.isSelected = selecteds.contains(repository)
                collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .top)
            } else {
                cell.isSelected = selecteds.contains(repository)
            }
        }
    }
}

extension FileSelectFromFolderViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row], let url = URL(string: repository.thumbnail) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension FileSelectFromFolderViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let repository = repositories[safe: indexPath.row], !repository.is_private else { return .zero }
        return FileCollectionViewCell.getSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return FileCollectionViewCell.inset.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return FileCollectionViewCell.inset.top
    }
}

extension FileSelectFromFolderViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -scrollView.contentInset.top, loadingDecrementStatus == nil, let repository = repositories[safe: 0], let first_id = repositories.first?.id, repository.id == first_id, first_id != loadingDecrementStatus {
            loadingDecrementStatus = first_id
            self.previousCount = repositories.count
            self.presenter?.decrements()
        }
    }
}

extension FileSelectFromFolderViewController: FileSelectFromFolderPresenterOutput {
    func fileSelectFromFolderPresenter(didFetch repositories: FileEntities, in fileSelectFromFolderPresenter: FileSelectFromFolderPresenter) {
        DispatchQueue.main.async { [unowned self] in
            let shouldScroll = self.repositories.count == 0
            self.repositories = FileDispatcher.binds(contents: repositories, state: store.state.file)
            self.collectionView.reloadData()
            if shouldScroll {
                self.collectionView.scrollToBottomWith(animated: false)
            }
        }
    }
    
    func fileSelectFromFolderPresenter(didIncrement repositories: FileEntities, in fileSelectFromFolderPresenter: FileSelectFromFolderPresenter) {
        previousCount = nil
        if repositories.count > 0, repositories.last?.id != loadingIncrementStatus {
            loadingIncrementStatus = nil
        }
        self.repositories = FileDispatcher.binds(contents: repositories, state: store.state.file)
        let indexPaths = self.collectionView.indexPathsForSelectedItems
        self.collectionView.reloadKeepOffset()
        _ = indexPaths?.compactMap({
            self.collectionView.selectItem(at: $0, animated: false, scrollPosition: .bottom)
        })
    }
    
    func fileSelectFromFolderPresenter(didDecrement repositories: FileEntities, in fileSelectFromFolderPresenter: FileSelectFromFolderPresenter) {
        let offset = repositories.count - (previousCount ?? 0)
        previousCount = nil
        if repositories.count > 0, repositories.first?.id != loadingDecrementStatus {
            loadingDecrementStatus = nil
            self.repositories = FileDispatcher.binds(contents: repositories, state: store.state.file)
            let indexPaths = self.collectionView.indexPathsForSelectedItems
            self.collectionView.reloadKeepOffset()
            _ = indexPaths?.compactMap({
                let indexPath = IndexPath(row: $0.row + offset, section: $0.section)
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.fileCollectionViewCell.name, for: indexPath) as? FileCollectionViewCell else {
                    return
                }
                cell.setEditing(true, animated: false)
                cell.isSelected = true
                self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .bottom)
            })
        }
    }
}
