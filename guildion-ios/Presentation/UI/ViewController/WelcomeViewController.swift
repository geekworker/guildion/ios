//
//  WelcomeViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit
import AVFoundation

class WelcomeViewController: BaseViewController {
    @IBOutlet weak var authButton: SimpleButton! {
        didSet {
            self.authButton.text = R.string.localizable.login()
            self.authButton.canceled = false
            self.authButton.normalColor = Theme.themify(key: .cancel)
            self.authButton.textColor = Theme.themify(key: .backgroundContrast)
            self.authButton.delegate = self
        }
    }
    @IBOutlet weak var sessionButton: SimpleButton! {
        didSet {
            self.sessionButton.text = R.string.localizable.memberRegister()
            self.sessionButton.canceled = false
            self.sessionButton.normalColor = Theme.themify(key: .main)
            self.sessionButton.textColor = Theme.themify(key: .backgroundContrast)
            self.sessionButton.delegate = self
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Theme.themify(key: .string)
            titleLabel.text = R.string.localizable.welcomeTitle()
        }
    }
    @IBOutlet weak var detailLabel: UILabel! {
        didSet {
            detailLabel.textColor = Theme.themify(key: .stringSecondary)
            detailLabel.text = R.string.localizable.welcomeDesc()
        }
    }
    var wireframe: WelcomeWireframe?
    private var videos: [URL] = [
        URL(fileURLWithPath: Bundle.main.path(forResource: "group-watching@iOS", ofType: "mp4")!),
    ]
    private var current_video_key: Int = 0
    private lazy var player: AVPlayer = {
        let _player = AVPlayer(url: videos[0])
        _player.actionAtItemEnd = .none
        _player.isMuted = true
        return _player
    }()
    private var playerLayer: AVPlayerLayer?
    private var willEnterForegroundObserver: NSObjectProtocol?
    private var boundsObserver: NSKeyValueObservation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPlayer()
        self.wireframe = WelcomeWireframe(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.restartPlayer()
    }
    
    func setupPlayer() {
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = view.bounds
        playerLayer?.videoGravity = .resizeAspectFill
        playerLayer?.zPosition = -2
        view.layer.insertSublayer(playerLayer!, at: 0)
        playerLayer?.backgroundColor = UIColor.black.cgColor
        player.play()
        
        let dimOverlay = CALayer()
        dimOverlay.frame = view.bounds
        dimOverlay.backgroundColor = UIColor.black.cgColor
        dimOverlay.zPosition = -1
        dimOverlay.opacity = 0.4
        view.layer.insertSublayer(dimOverlay, at: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        
        willEnterForegroundObserver = NotificationCenter.default.addObserver(
            forName: UIApplication.willEnterForegroundNotification,
            object: nil,
            queue: .main) { [weak playerLayer] _ in
                playerLayer?.player?.play()
        }
        
        boundsObserver = view.layer.observe(\.bounds) { [weak playerLayer, weak dimOverlay] view, _ in
            DispatchQueue.main.async {
                playerLayer?.frame = view.bounds
                dimOverlay?.frame = view.bounds
            }
        }
    }
    
    func restartPlayer() {
        self.player.seek(to: CMTime(seconds: 0, preferredTimescale: 1000000))
        player.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if let unwrapped = willEnterForegroundObserver {
            NotificationCenter.default.removeObserver(unwrapped)
        }
        boundsObserver?.invalidate()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if let unwrapped = willEnterForegroundObserver {
            NotificationCenter.default.removeObserver(unwrapped)
        }
        boundsObserver?.invalidate()
    }
    
    func incrementVideoPlayer() {
        let incremented_key = current_video_key + 1 < videos.count ? current_video_key + 1 : 0
        player.replaceCurrentItem(with: AVPlayerItem(url: videos[incremented_key]))
        self.current_video_key = incremented_key
        self.restartPlayer()
    }
    
    @objc func playerDidFinishPlaying() {
        incrementVideoPlayer()
    }
}


extension WelcomeViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        switch simpleButton {
        case authButton:
            self.wireframe?.toAuth()
            break
        case sessionButton:
            self.wireframe?.toSession()
            break
        default: break
        }
    }
}
