//
//  FileReferencesNewViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/20.
//

import Foundation
import UIKit

class FileReferencesNewViewController: BaseViewController {
    var presenter: FileReferencesNewPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: FileEntities = FileEntities()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.localizable.mypage()
        FileReferencesNewViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension FileReferencesNewViewController: FileReferencesNewPresenterOutput {
    func sync(didFetchRepositories repositories: FileEntities) {
        self.repositories = repositories
    }
}

