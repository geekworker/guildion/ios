//
//  MembersViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/14.
//

import Foundation
import UIKit
import Eureka
import Parchment

class MembersViewController: BaseViewController {
    var presenter: MembersPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: RoleEntities = RoleEntities()
    var repository: GuildEntity = GuildEntity()
    var pagingViewController: NavigationBarPagingViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        MembersViewControllerBuilder.rebuild(self)
        self.presenter?.fetch(repository: repository)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
         navigationController?.navigationBar.barTintColor = Theme.themify(key: .backgroundThick)
         navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
         navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)]
         navigationController?.navigationBar.isTranslucent = false
         self.view.backgroundColor = Theme.themify(key: .background)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let navigationBar = navigationController?.navigationBar else { return }
        navigationItem.titleView?.frame = CGRect(origin: .zero, size: navigationBar.bounds.size)
        pagingViewController?.menuItemSize = .selfSizing(estimatedWidth: 100, height: navigationBar.bounds.height)
        pagingViewController?.view.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    fileprivate func configurePagingViewController() {
        // navigationController?.setViewControllers([self], animated: false)
        self.pagingViewController = NavigationBarPagingViewController(
            viewControllers: repositories.compactMap({
                let vc = R.storyboard.guilds.roleMembersViewController()
                vc?.repository = $0
                vc?.title = $0.name
                return vc
            })
        )
        pagingViewController?.borderOptions = .hidden
        pagingViewController?.menuBackgroundColor = .clear
        pagingViewController?.indicatorColor = Theme.themify(key: .main)
        pagingViewController?.textColor = Theme.themify(key: .border)
        pagingViewController?.selectedTextColor = Theme.themify(key: .main)
        pagingViewController?.view.isHidden = true
        addChild(pagingViewController!)
        view.addSubview(pagingViewController!.view)
        view.constrainToEdges(pagingViewController!.view)
        pagingViewController?.didMove(toParent: self)
        navigationItem.titleView = pagingViewController!.collectionView
    }
}

extension MembersViewController: MembersPresenterOutput {
    func sync(didFetchRepositories repositories: MemberEntities) {
    }
    
    func sync(didFetchRepositories repositories: RoleEntities) {
        self.repositories = repositories
        configurePagingViewController()
    }
    
    func membersPresenter(onError error: Error, in membersPresenter: MembersPresenter) {
    }
}


class NavigationBarPagingView: PagingView {
    override func setupConstraints() {
        // Use our convenience extension to constrain the page view to all
        // of the edges of the super view.
        constrainToEdges(pageView)
    }
}

class NavigationBarPagingViewController: PagingViewController {
    override func loadView() {
        view = NavigationBarPagingView(
            options: options,
            collectionView: collectionView,
            pageView: pageViewController.view
        )
    }
}
