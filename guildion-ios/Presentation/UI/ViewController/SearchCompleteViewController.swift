//
//  SearchCompleteViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/11.
//

import Foundation
import UIKit

protocol SearchCompleteViewControllerDelegate: class {
}

class SearchCompleteViewController: UIViewController {
    var presenter: SearchCompletePresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: SearchModels = SearchModels()
    weak var delegate: SearchCompleteViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        SearchCompleteViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

extension SearchCompleteViewController: SearchCompletePresenterOutput {
    func sync(didFetchRepositories repositories: SearchModels) {
        self.repositories = repositories
    }
}
