//
//  ProgressOverlayViewController.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/04.
//

import Foundation
import UIKit
import M13ProgressSuite

protocol ProgressOverlayViewControllerDelegate: class {
    func progressOverlayViewController(didFinish progressOverlayViewController: ProgressOverlayViewController)
    func progressOverlayViewController(didCancel progressOverlayViewController: ProgressOverlayViewController)
}

class ProgressOverlayViewController: UIViewController {
    @IBInspectable var progressTitle: String?
    @IBOutlet weak var progressView: M13ProgressViewRing! {
        didSet {
            progressView.setProgress(0, animated: false)
            progressView.tintColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = progressTitle
            titleLabel.textColor = .white
            titleLabel.font = Font.FT(size: .FM, family: .L)
        }
    }
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            cancelButton.setTitle(R.string.localizable.cancel(), for: .normal)
            cancelButton.setTitle(R.string.localizable.cancel(), for: .highlighted)
            cancelButton.tintColor = Theme.themify(key: .main)
            cancelButton.titleLabel?.font = Font.FT(size: .FM, family: .L)
        }
    }
    weak var delegate: ProgressOverlayViewControllerDelegate?
    
    @objc func onTapCancelButton(_ sender: Any) {
        self.delegate?.progressOverlayViewController(didCancel: self)
    }
    
    func updateProgress(_ progress: CGFloat, animated: Bool) {
        self.progressView.setProgress(progress, animated: animated)
        if progress >= 1 {
            self.delegate?.progressOverlayViewController(didFinish: self)
        }
    }
}
