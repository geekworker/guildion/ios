//
//  ChannelRolesEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class ChannelRolesEditViewController: BaseFormViewController {
    var presenter: ChannelRolesEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: [ChannelRoleableProtocol] = []
    var roles: RoleEntities = RoleEntities()
    var repository: ChannelableProtocol = TextChannelEntity()
    var selectedRole: ChannelRoleableProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelRolesEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.presenter?.fetch(repository: repository)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ChannelRoleEditViewController, let repository = selectedRole {
            vc.repository = repository
        }
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        let section = Section()
        section.tag = "section"
        
        if form.sectionBy(tag: "section") == nil {
            form +++
                section
                <<< PushRow<String>(R.string.localizable.roleAdd()) {
                $0.title = $0.tag
                $0.options = roles.items.filter({ [unowned self] in !self.repositories.compactMap({ $0.RoleId }).contains($0.id) }).compactMap({ $0.name })
                $0.value = nil
                }.onPresent({ (_, vc) in
                    vc.enableDeselection = false
                    vc.dismissOnSelection = false
                }).cellUpdate { [unowned self] in self.pushRowCellUpdate($0, $1) }
                .onChange { [unowned self] in
                    guard let value = $0.value, let repository = self.roles.filter({ $0.name == value }).first else { return }
                    self.presenter?.create(repository: repository, channel: self.repository)
                }
        }
        
        switch repository.channelable_type.superChannel {
        case .TextChannel: configureTextChannelRolesForm(repositories: repositories as! [TextChannelRoleEntity])
        case .DMChannel: configureDMChannelRolesForm(repositories: repositories as! [DMChannelRoleEntity])
        case .StreamChannel: configureStreamChannelRolesForm(repositories: repositories as! [StreamChannelRoleEntity])
        default: break
        }
    }
    
    fileprivate func configureTextChannelRolesForm(repositories: [TextChannelRoleEntity]) {
        let section = SelectableSection<ButtonRow>()
        form +++ section
        
        for repository in repositories {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = repository.Role?.name
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate {
                self.buttonImageRowCellUpdate($0, $1, color: UIColor(hex: repository.Role?.color ?? "FFFFFF"))
            }
        }
        
        section.onSelectSelectableRow = { [unowned self] (cell, row) in
            self.selectedRole = repositories.filter({ "\($0.id ?? 0)" == row.tag }).first
            self.presenter?.toChannelEdit()
        }
    }
    
    fileprivate func configureDMChannelRolesForm(repositories: [DMChannelRoleEntity]) {
        let section = SelectableSection<ButtonRow>()
        form +++ section
        
        for repository in repositories {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = repository.Role?.name
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                self.buttonImageRowCellUpdate($0, $1, color: UIColor(hex: repository.Role?.color ?? "FFFFFF"))
            }
        }
        
        section.onSelectSelectableRow = { (cell, row) in
            self.selectedRole = repositories.filter({ "\($0.id ?? 0)" == row.tag }).first
            self.presenter?.toChannelEdit()
        }
    }
    
    fileprivate func configureStreamChannelRolesForm(repositories: [StreamChannelRoleEntity]) {
        let section = SelectableSection<ButtonRow>()
        form +++ section
        
        for repository in repositories {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = repository.Role?.name
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                self.buttonImageRowCellUpdate($0, $1, color: UIColor(hex: repository.Role?.color ?? "FFFFFF"))
            }
        }
        
        section.onSelectSelectableRow = { [unowned self] (cell, row) in
            self.selectedRole = repositories.filter({ "\($0.id ?? 0)" == row.tag }).first
            self.presenter?.toChannelEdit()
        }
    }
}

extension ChannelRolesEditViewController: ChannelRolesEditPresenterOutput {
    func sync(didFetchRepositories repositories: [ChannelRoleableProtocol]) {
        self.repositories = repositories
        self.reloadForm()
    }
    
    func sync(didFetchRepositories repositories: RoleEntities) {
        self.roles = repositories
        self.reloadForm()
    }
    
    func channelRolesEditPresenter(onError error: Error, in channelRolesEditPresenter: ChannelRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func channelRolesEditPresenter(didCreated repository: ChannelRoleableProtocol, in channelRolesEditPresenter: ChannelRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.presenter?.fetch(repository: self.repository)
    }
}
