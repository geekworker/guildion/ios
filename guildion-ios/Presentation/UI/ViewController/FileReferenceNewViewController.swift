//
//  FileReferenceNewViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/20.
//

import Foundation
import UIKit

class FileReferenceNewViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var referenceSelectButton: BorderButton!
    @IBOutlet weak var submitButton: SimpleButton!
    var presenter: FileReferenceNewPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: FileEntity = FileEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.localizable.mypage()
        FileReferenceNewViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension FileReferenceNewViewController: FileReferenceNewPresenterOutput {
    func sync(didFetchRepository repository: FileEntity) {
        self.repository = repository
    }
}
