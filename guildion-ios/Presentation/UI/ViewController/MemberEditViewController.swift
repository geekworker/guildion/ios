//
//  MemberEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Eureka
import PKHUD
import SwiftMessages

class MemberEditViewController: BaseFormViewController {
    var presenter: MemberEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: MemberEntity = MemberEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: MemberEntity = MemberEntity()
    var guild: GuildEntity = GuildEntity()
    var blocked: Bool = false
    var muted: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        MemberEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.repositories = repository
        presenter?.bind()
        presenter?.fetch(repostiroy: repository)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MemberRolesEditViewController /*, segue.identifier == R.segue.memberEditViewController.toMemberRolesEdit.identifier */ {
            vc.repository = repository
            vc.current_guild = guild
        }
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    func handleValueChange() {
        if repository.nickname == new_repository.nickname && repository.description == new_repository.description && repository.picture_small == new_repository.picture_small && repository.picture_large == new_repository.picture_large && self.repository.mute == self.new_repository.mute && self.repository.messages_mute == self.new_repository.messages_mute && self.repository.mentions_mute == self.new_repository.mentions_mute && self.repository.everyone_mentions_mute == self.new_repository.everyone_mentions_mute {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        let isMyAccount = CurrentUser.getCurrentUserEntity()?.id == repository.UserId
        
        form
            +++ Section() { [unowned self] in
            var header = HeaderFooterView<MemberSettingHeaderView>(.class)
                header.height = { MemberSettingHeaderView.viewHeight }
                header.onSetupView = { [unowned self] (view, section) -> () in
                    view.setRepository(self.repository)
                }
                $0.header = header
            }
            
        if (GuildConnector.shared.current_permission.Guild.members_managable && !isMyAccount) || (GuildConnector.shared.current_permission.Guild.member_managable && isMyAccount) {
            form
                +++ Section(R.string.localizable.userMenu())
                <<< ImageRow() {
                    $0.title = R.string.localizable.profileImage()
                    $0.value = UIImage(url: new_repository.picture_small)
                }.onChange { [unowned self] in
                    guard let image = $0.value else {
                        self.new_repository.picture_small = DataConfig.Image.default_profile_image_url
                        self.reloadForm()
                        self.handleValueChange()
                        return
                    }
                    self.new_repository.picture_small = "\(ImagePickerHandler().generateImageURL(image))"
                    self.handleValueChange()
                }.cellUpdate(imageRowCellUpdate(_:_:))
                <<< TextRow(R.string.localizable.nickname()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.value = new_repository.nickname == "" ? new_repository.User?.nickname ?? "" : new_repository.nickname
                    $0.placeholder = $0.tag
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.nickname = value
                    self.handleValueChange()
                }.cellUpdate(textRowCellUpdate(_:_:))
                <<< TextAreaRow(R.string.localizable.introduction()) {
                    var ruleSet = RuleSet<String>()
                    ruleSet.add(rule: RuleRequired())
                    $0.add(ruleSet: ruleSet)
                    $0.placeholder = R.string.localizable.introduction()
                    $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
                    $0.value = new_repository.description
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.description = value
                    self.handleValueChange()
                }.cellUpdate(textAreaRowCellUpdate(_:_:))
        }
        
        if GuildConnector.shared.current_permission.Guild.roles_managable {
            form
                +++ Section(R.string.localizable.guildMenuRoleEdit())
                <<< ButtonRow(R.string.localizable.guildMenuRoleEdit()) {
                    $0.title = $0.tag
                }.cellUpdate { [unowned self] in
                    self.buttonRowLeftAlignmentUpdate($0, $1)
                }.onCellSelection { [unowned self] _, _ in
                    self.presenter?.toMemberRolesEdit()
                }
        }
        
        if isMyAccount {
            form +++
                Section(R.string.localizable.guildNotificationSetting())
                <<< SwitchRow(R.string.localizable.guildNotificationMute()) {
                    $0.title = $0.tag
                    $0.value = self.new_repository.mute
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.mute = value
                    self.handleValueChange()
                }.cellUpdate(switchRowCellUpdate(_:_:))
                <<< SwitchRow(R.string.localizable.guildNotificationMessageMute()) { [unowned self] in
                    $0.title = $0.tag
                    $0.value = self.new_repository.messages_mute
                    $0.hidden = .function([R.string.localizable.guildNotificationMute()], { form -> Bool in
                        let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.guildNotificationMute())
                        return row.value ?? false == true
                    })
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.messages_mute = value
                    self.handleValueChange()
                }.cellUpdate(switchRowCellUpdate(_:_:))
                <<< SwitchRow(R.string.localizable.guildNotificationMentionMute()) { [unowned self] in
                    $0.title = $0.tag
                    $0.value = self.new_repository.mentions_mute
                    $0.hidden = .function([R.string.localizable.guildNotificationMute()], { form -> Bool in
                        let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.guildNotificationMute())
                        return row.value ?? false == true
                    })
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.mentions_mute = value
                    self.handleValueChange()
                }.cellUpdate(switchRowCellUpdate(_:_:))
                <<< SwitchRow(R.string.localizable.guildNotificationEveryoneMentionMute()) { [unowned self] in
                    $0.title = $0.tag
                    $0.value = self.new_repository.everyone_mentions_mute
                    $0.hidden = .function([R.string.localizable.guildNotificationMute()], { form -> Bool in
                        let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.guildNotificationMute())
                        return row.value ?? false == true
                    })
                }.onChange { [unowned self] in
                    guard let value = $0.value else { return }
                    self.new_repository.everyone_mentions_mute = value
                    self.handleValueChange()
                }.cellUpdate(switchRowCellUpdate(_:_:))
        }
        
        if !isMyAccount, let user = repository.User {
            form +++
                Section("")
                <<< ButtonRow(R.string.localizable.userReport()) {
                    $0.title = $0.tag
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .warning)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    self.shouldReportUser(user)
                }
                <<< ButtonRow(muted ? R.string.localizable.userUnMute() : R.string.localizable.userMute()) {
                    $0.title = $0.tag
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: muted ? R.string.localizable.userUnMute() : R.string.localizable.userMute(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                                
                    let defaultAction: UIAlertAction = UIAlertAction(title: muted ? R.string.localizable.userUnMute() : R.string.localizable.userMute(), style: UIAlertAction.Style.default, handler:{ [unowned self] (action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        muted ? self.presenter?.unmute(target: user) : self.presenter?.mute(target: user)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
                <<< ButtonRow(blocked ? R.string.localizable.userUnBlock() : R.string.localizable.userBlock()) {
                    $0.title = $0.tag
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: blocked ? R.string.localizable.userUnBlock() : R.string.localizable.userBlock(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                                
                    let defaultAction: UIAlertAction = UIAlertAction(title: blocked ? R.string.localizable.userUnBlock() : R.string.localizable.userBlock(), style: UIAlertAction.Style.default, handler:{ [unowned self] (action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        blocked ? self.presenter?.unblock(target: user) : self.presenter?.block(target: user)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
        
        if GuildConnector.shared.current_permission.Guild.kickable && !isMyAccount {
            form +++
                Section("")
                <<< ButtonRow(R.string.localizable.guildKick()) {
                    $0.title = $0.tag
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: R.string.localizable.guildKick(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                                
                    let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.guildKick(), style: UIAlertAction.Style.default, handler:{
                        [unowned self](action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        self.presenter?.kick(guild: self.guild.toNewMemory(), repository: self.repository)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
        
        if GuildConnector.shared.current_permission.Guild.banable && !isMyAccount {
            form +++
                Section("")
                <<< ButtonRow(R.string.localizable.guildBlock()) {
                    $0.title = $0.tag
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: R.string.localizable.guildBlock(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                                
                    let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.guildBlock(), style: UIAlertAction.Style.default, handler:{
                        [unowned self](action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        self.presenter?.block(guild: self.guild.toNewMemory(), repository: self.repository)
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    private func shouldReportUser(_ repository: UserEntity) {
        let alert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle:  UIAlertController.Style.actionSheet)
        let action1: UIAlertAction = UIAlertAction(title: R.string.localizable.reportSex(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(target: repository, detail: R.string.localizable.reportSex())
        })
        let action2: UIAlertAction = UIAlertAction(title: R.string.localizable.reportViolate(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(target: repository, detail: R.string.localizable.reportViolate())
        })
        let action3: UIAlertAction = UIAlertAction(title: R.string.localizable.reportRight(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(target: repository, detail: R.string.localizable.reportRight())
        })
        let action4: UIAlertAction = UIAlertAction(title: R.string.localizable.reportInvalid(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(target: repository, detail: R.string.localizable.reportInvalid())
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler: { (action: UIAlertAction!) -> Void in
        })
        DispatchQueue.main.async {
            alert.addAction(action1)
            alert.addAction(action2)
            alert.addAction(action3)
            alert.addAction(action4)
            alert.addAction(cancelAction)
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension MemberEditViewController: MemberEditPresenterOutput {
    func sync(didFetchRepository repository: MemberEntity) {
        self.repository = repository
        self.reloadForm()
    }
    
    func memberEditPresenter(shouldBack memberEditPresenter: MemberEditPresenter) {
        HUD.hide()
        self.dismissShouldAppear(animated: true)
    }
    
    func memberEditPresenter(shouldReload memberEditPresenter: MemberEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.reloadForm()
    }
    
    func memberEditPresenter(onError error: Error, in memberEditPresenter: MemberEditPresenter) {
        HUD.hide()
        let messageAlert = MessageView.viewFromNib(layout: .cardView)
        messageAlert.configureTheme(.error)
        messageAlert.configureDropShadow()
        messageAlert.configureContent(title: R.string.localizable.error(), body: error.getLocalizedReason())
        messageAlert.button?.isHidden = true
        var messageAlertConfig = SwiftMessages.defaultConfig
        messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
    }
    
    func memberEditPresenter(didUpdate repository: MemberEntity, in memberEditPresenter: MemberEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
}
