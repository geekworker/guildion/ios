//
//  MemberRolesEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class MemberRolesEditViewController: BaseFormViewController {
    var presenter: MemberRolesEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: MemberRoleEntities = MemberRoleEntities()
    var repository: MemberEntity = MemberEntity()
    var current_guild: GuildEntity = GuildEntity()
    var roles: RoleEntities = RoleEntities()
    private var selectedRole: MemberRoleEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MemberRolesEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.presenter?.fetch(repository: repository)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MemberRoleEditViewController, let repository = selectedRole {
            vc.repository = repository
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section()
            <<< PushRow<String>(R.string.localizable.roleAdd()) {
            $0.title = $0.tag
            $0.options = roles.filter({ !self.repositories.items.compactMap({ $0.RoleId }).contains($0.id) }).compactMap({ $0.name })
            $0.value = nil
            }.onPresent({ (_, vc) in
                vc.enableDeselection = false
                vc.dismissOnSelection = false
            }).cellUpdate { [unowned self] in self.pushRowCellUpdate($0, $1) }
            .onChange { [unowned self] in
                guard let value = $0.value, let repository = self.roles.filter({ $0.name == value }).first else { return }
                self.presenter?.create(repository: repository, member: self.repository)
            }
        
        let section = SelectableSection<ButtonRow>()
        form +++ section
        
        for repository in repositories.items {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = repository.Role?.name
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                self.buttonImageRowCellUpdate($0, $1, color: UIColor(hex: repository.Role?.color ?? "FFFFFF"))
            }
        }
        
        section.onSelectSelectableRow = { [unowned self] (cell, row) in
            self.selectedRole = self.repositories.items.filter({ "\($0.id ?? 0)" == row.tag }).first
            self.presenter?.toMemberRoleEdit()
        }
    }
}

extension MemberRolesEditViewController: MemberRolesEditPresenterOutput {
    func sync(didFetchRepositories repositories: MemberRoleEntities) {
        self.repositories = repositories
        self.reloadForm()
    }
    
    func sync(didFetchRepositories repositories: RoleEntities) {
        self.roles = repositories
        self.reloadForm()
    }
    
    func memberRolesEditPresenter(didCreate repository: MemberRoleEntity, in memberRolesEditPresenter: MemberRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.presenter?.fetch(repository: self.repository)
    }
    
    func memberRolesEditPresenter(didUpdate repository: MemberRoleEntity, in memberRolesEditPresenter: MemberRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.presenter?.fetch(repository: self.repository)
    }
    
    func memberRolesEditPresenter(didDelete repository: MemberRoleEntity, in memberRolesEditPresenter: MemberRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.presenter?.fetch(repository: self.repository)
    }
    
    func memberRolesEditPresenter(onError error: Error, in memberRolesEditPresenter: MemberRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
