//
//  PlaylistViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/25.
//

import Foundation
import UIKit
import Nuke
import PKHUD
import Lightbox

protocol PlaylistViewControllerSelectDelegate: class {
    func playlistViewController(didSelect repository: FileReferenceEntity, in playlistViewController: PlaylistViewController)
}

class PlaylistViewController: BaseViewController {
    @IBOutlet weak var actionButton: ActionButton! {
        didSet {
            actionButton.delegate = self
            actionButton.isHidden = selectable
            actionButton.isHidden = !permission.Guild.folders_managable && !isOwner
        }
    }
    @IBOutlet weak var tableView: UITableView!
    var presenter: PlaylistPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var permission: PermissionModel = PermissionModel()
    var current_member: MemberEntity = MemberEntity()
    var current_guild: GuildEntity = GuildEntity()
    var repository: FolderEntity = FolderEntity()
    var repositories: FileReferenceEntities = FileReferenceEntities()
    var selectable: Bool = false
    var all_files: Bool = false
    weak var selectDelegate: PlaylistViewControllerSelectDelegate?
    enum Section: Int {
        case header
        case play
        case references
        case count
        
        func getCount(_ repository: FolderEntity, _ repositories: FileReferenceEntities) -> Int {
            switch self {
            case .header: return HeaderRow.count.rawValue
            case .play: return PlayRow.count.rawValue
            case .references: return repositories.count
            case .count: return 0
            }
        }
    }
    enum PlayRow: Int {
        case content
        case count
    }
    enum HeaderRow: Int {
        case content
        case count
    }
    fileprivate var fetched: Bool = false
    private var loadingDecrementStatus: Int?
    private var loadingIncrementStatus: Int?
    private var lastWillDisplay: Int?
    var isOwner: Bool {
        return current_member.id == repository.MemberId
    }

    override func viewDidLoad() {
        title = all_files ? R.string.localizable.allFiles() : repository.name
        if selectable {
            navigationMode = .other
        } else if permission.Guild.folders_managable || isOwner {
            navigationMode = .edit
        } else {
            navigationMode = .other
        }
        super.viewDidLoad()
        PlaylistViewControllerBuilder.rebuild(self)
        self.view.backgroundColor = Theme.themify(key: .backgroundThick)
        self.actionButton.isHidden = (!permission.Guild.folders_managable && !isOwner) || selectable
        configureTableView()
        presenter?.fetch(folder: repository)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        guard navigationItem.rightBarButtonItems?.count == 1 && permission.Guild.folders_managable || isOwner else { return }
        let setting = UIBarButtonItem(image: R.image.gear()!.withRenderingMode(.alwaysTemplate).resizedImage(newSize: CGSize(width: 18, height: 18)), style: .plain, target: self, action: #selector(self.onTapSettingButton(_:)))
        navigationItem.rightBarButtonItems?.append(setting)
        loadingDecrementStatus = nil
        loadingIncrementStatus = nil
        lastWillDisplay = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FolderEditViewController, segue.identifier == R.segue.playlistViewController.toEdit.identifier {
            vc.repository = self.repository
            vc.is_playlist = true
        }
    }
    
    @objc func onTapSettingButton(_ sender: Any) {
        self.presenter?.toEdit()
    }
    
    public func setRepository(_ repository: FolderEntity) {
        self.repository = repository
        self.actionButton.isHidden = (!permission.Guild.folders_managable && !isOwner) || selectable
        self.tableView.reloadData()
    }
    
    public func setRepositories(_ repositories: FileReferenceEntities) {
        self.repositories.items = repositories.sorted().filter({ $0.FileId != nil && $0.FileId != 0 })
        self.actionButton.isHidden = (!permission.Guild.folders_managable && !isOwner) || selectable
        self.tableView.reloadData()
    }
    
    public func initRepository(_ repository: FolderEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        guard !fetched else { return }
        self.repository = repository
        self.current_guild = current_guild
        self.current_member = current_member
        self.presenter?.fetch(folder: repository)
        self.permission = permission
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: animated)
    }
}

extension PlaylistViewController: UITableViewDelegate {
    private func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.register([
            UINib(resource: R.nib.fileTableViewCell): R.nib.fileTableViewCell.name,
            UINib(resource: R.nib.playlistPlayTableViewCell): R.nib.playlistPlayTableViewCell.name,
            UINib(resource: R.nib.folderHeaderViewTableViewCell): R.nib.folderHeaderViewTableViewCell.name,
        ])
        self.tableView.backgroundColor = Theme.themify(key: .backgroundThick)
        self.tableView.alwaysBounceVertical = true
        self.tableView.allowsSelectionDuringEditing = true
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        guard let section = Section.init(rawValue: indexPath.section) else { return }
        if section == .references && selectable {
            guard let reference = repositories[safe: indexPath.row] else { return }
            self.selectDelegate?.playlistViewController(didSelect: reference, in: self)
            self.navigationController?.popViewController(animated: true)
        } else if section == .references && !selectable {
            guard let reference = repositories[safe: indexPath.row], let file = reference.File else { return }
            self.presenter?.toFileMenu(file, reference: reference)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let section = Section.init(rawValue: indexPath.section), section == .references else { return }
        if repositories.count > indexPath.row, let repository = repositories[safe: indexPath.row], let last_id = repositories.last?.id, repository.id == last_id, last_id != loadingIncrementStatus {
            loadingIncrementStatus = last_id
            self.presenter?.increment()
        }
        lastWillDisplay = indexPath.row
    }
}

extension PlaylistViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section.init(rawValue: section) else { return 0 }
        return section.getCount(repository, repositories)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section.init(rawValue: indexPath.section) else { return UITableViewCell() }
        switch section {
        case .header: return self.tableView(tableView, cellForHeaderRowAt: indexPath)
        case .play: return self.tableView(tableView, cellForPlayRowAt: indexPath)
        case .references: return self.tableView(tableView, cellForReferenceRowAt: indexPath)
        default: return UITableViewCell()
        }
    }
    
    fileprivate func tableView(_ tableView: UITableView, cellForHeaderRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.folderHeaderViewTableViewCell.name, for: indexPath) as? FolderHeaderViewTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    fileprivate func tableView(_ tableView: UITableView, cellForPlayRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.playlistPlayTableViewCell.name, for: indexPath) as? PlaylistPlayTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.delegate = self
        return cell
    }
    
    fileprivate func tableView(_ tableView: UITableView, cellForReferenceRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.fileTableViewCell.name, for: indexPath) as? FileTableViewCell, let repository = repositories[safe: indexPath.row] else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository.File ?? FileEntity())
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard let section = Section.init(rawValue: indexPath.section), section == .references, editingStyle == .delete, let repository = repositories[safe: indexPath.row] else { return }
        let entities = FileReferenceEntities()
        entities.items = [repository]
        self.repositories.remove(repository)
        self.presenter?.deleteFileReferences(repositories: entities)
        let count = self.repositories.count
        guard indexPath.row + 1 < count else { return }
        let updates = self.repositories.items[indexPath.row + 1..<count]
        let _entities = FileReferenceEntities()
        _entities.items = updates.compactMap {
            $0.index -= 1
            return $0
        }
        self.repositories.items = (self.repositories.items).compactMap({ reference in
            if let entity = _entities.filter({ $0 == reference }).first {
                reference.index = entity.index
            }
            return reference
        })
        self.repositories.items = self.repositories.sorted()
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        guard let section = Section.init(rawValue: indexPath.section), section == .references else { return false }
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        guard let section = Section.init(rawValue: destinationIndexPath.section), section == .references else { self.tableView.reloadData(); return }
        guard let source_repository = self.repositories[safe: sourceIndexPath.row], sourceIndexPath.row != destinationIndexPath.row else { return }
        repositories.remove(source_repository)
        repositories.insert(source_repository, at: destinationIndexPath.row)
        let entities = FileReferenceEntities()
        entities.items = repositories
            .enumerated()
            .compactMap({
                $0.element.index = $0.offset + 1
                return $0.element
            })
            .filter({
                $0.index_updated
            })
        self.presenter?.updateIndexes(repositories: entities)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        guard let section = Section.init(rawValue: indexPath.section), section == .references, permission.Guild.folders_managable || isOwner else { return .none }
        return .delete
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let section = Section.init(rawValue: indexPath.section), section == .references else { return false }
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = Section.init(rawValue: indexPath.section) else { return 0 }
        switch section {
        case .header: return self.tableView(tableView, heightForHeaderRowAt: indexPath)
        case .play: return self.tableView(tableView, heightForPlayRowAt: indexPath)
        case .references: return self.tableView(tableView, heightForFileRowAt: indexPath)
        default: return 0
        }
    }
    
    fileprivate func tableView(_ tableView: UITableView, heightForHeaderRowAt indexPath: IndexPath) -> CGFloat {
        return FolderHeaderView.height
    }
    
    fileprivate func tableView(_ tableView: UITableView, heightForPlayRowAt indexPath: IndexPath) -> CGFloat {
        return (!permission.Guild.folders_managable && !isOwner) || selectable ? 0 : PlaylistPlayTableViewCell.cellHeight
    }
    
    fileprivate func tableView(_ tableView: UITableView, heightForFileRowAt indexPath: IndexPath) -> CGFloat {
        return FileTableViewCell.cellHeight
    }
}

extension PlaylistViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row], let file = repository.File, let url = URL(string: file.thumbnail) {
                requests.append(ImageRequest(url: url))
            }
        }
        if let url = URL(string: repository.thumbnail) {
            requests.append(ImageRequest(url: url))
        }
        return requests
    }
}

extension PlaylistViewController: FolderHeaderViewDelegate {
    func folderHeaderView(onTapMember repository: MemberEntity, in folderHeaderView: FolderHeaderView) {
    }
}

extension PlaylistViewController: PlaylistPlayTableViewCellDelegate {
    func playlistPlayTableViewCell(onTapPlay playlistPlayTableViewCell: PlaylistPlayTableViewCell) {
        guard let first = repositories.first else { return }
        if let vc = LiveConfig.playingStreamChannelViewController {
            vc.start(first)
            return
        } else {
            self.presenter?.startPrivateStreamChannel(repository: first)
        }
    }
    
    func playlistPlayTableViewCell(onTapAdd playlistPlayTableViewCell: PlaylistPlayTableViewCell) {
        self.presenter?.toFileSelect()
    }
}

extension PlaylistViewController: FileSelectFromFolderViewControllerDelegate {
    func fileSelectFromFolderViewController(shouldDismiss selecteds: FileEntities, in fileSelectFromFolderViewController: FileSelectFromFolderViewController) {
        selecteds.items = selecteds.filter({ $0.format.content_type.is_movie || $0.provider == .youtube })
        self.presenter?.createFileReferences(repositories: selecteds, to: self.repository)
    }
}

extension PlaylistViewController: ActionButtonDelegate {
    func actionButton(onTap actionButton: ActionButton) {
        self.presenter?.toFileSelect()
    }
}

extension PlaylistViewController: FileMenuViewControllerDelegate {
    func toImageViewer(repository: FileEntity) {
        let lImage = LightboxImage(
            image: UIImage(url: repository.url),
            text: ""
        )
        let controller = LightboxController(images: [lImage])
        //controller.dismissalDelegate = self
        //controller.pageDelegate = self
        controller.dynamicBackground = true
        controller.modalPresentationStyle = .currentContext
        self.present(controller, animated: true, completion: nil)
    }
    
    func toWebBrowser(url: URL) {
        let webkitViewController = WebKitViewController(nibName: R.nib.webKitViewController.name, bundle: nil)
        let navVC = PanContainerViewNavigationController()
        navVC.viewControllers.append(webkitViewController)
        webkitViewController.defaultMedia = true
        webkitViewController.setURL(url)
        navVC.searchDelegate = webkitViewController
        navVC.modalPresentationStyle = .overCurrentContext
        self.present(navVC, animated: true)
    }
}

extension PlaylistViewController: PlaylistPresenterOutput {
    func playlistPresenter(didRetry count: Int, in playlistPresenter: PlaylistPresenter) {
        DispatchQueue.main.async {
            self.view.makeToast(R.string.localizable.domainErrorDetail())
        }
    }
    
    func playlistPresenter(onError error: Error, in playlistPresenter: PlaylistPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func sync(didFetchRepository repository: FolderEntity) {
        DispatchQueue.main.async {
            self.setRepository(repository)
            self.fetched = true
            self.tableView.reloadData()
        }
    }
    
    func playlistPresenter(didFetch repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.setRepositories(repositories)
            self.fetched = true
            self.tableView.reloadData()
        }
    }
    
    func playlistPresenter(didIncrement repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.setRepositories(repositories)
            self.tableView.reloadData()
        }
    }
    
    func playlistPresenter(didDecrement repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.setRepositories(repositories)
            self.tableView.reloadData()
        }
    }
    
    func playlistPresenter(didCreate repositories: FileReferenceEntities, in playlistPresenter: PlaylistPresenter) {
        presenter?.fetch(folder: repository)
    }
    
    func playlistPresenter(didDelete playlistPresenter: PlaylistPresenter) {
        presenter?.fetch(folder: repository)
    }
    
    func playlistPresenter(willToStreamChannel repository: StreamChannelEntity, in playlistPresenter: PlaylistPresenter) {
        DispatchQueue.main.async {
            self.presenter?.toStreamChannel(repository: repository)
        }
    }
}
