//
//  GuildRolesEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class GuildRolesEditViewController: BaseFormViewController {
    var presenter: GuildRolesEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: RoleEntities = RoleEntities()
    var repository: GuildEntity = GuildEntity()
    var selectedRole: RoleEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildRolesEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        self.presenter?.fetch(uid: repository.uid)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? RoleEditViewController, segue.identifier == R.segue.guildRolesEditViewController.toRoleEdit.identifier, let repository = selectedRole {
            vc.repository = repository
            selectedRole = nil
        }
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        self.setupPlusNavigationBarButton()
    }
    
    override func navigationBarAddTapped(_ sender: Any) {
        guard let everyone_role = repositories.filter({ $0.is_everyone }).first else { return }
        self.presenter?.create(repository: repository, everyone_role: everyone_role)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        let section = SelectableSection<ButtonRow>()
        form +++ section
        
        for repository in repositories.items {
            form.last! <<< ButtonRow("\(repository.id ?? 0)"){ lrow in
                lrow.title = repository.name
                lrow.selectableValue = "\(repository.id ?? 0)"
            }
            .cellUpdate { [unowned self] in
                self.buttonImageRowCellUpdate($0, $1, color: UIColor(hex: repository.color))
            }
        }
        
        section.onSelectSelectableRow = { [unowned self] (cell, row) in
            self.selectedRole = self.repositories.filter({ "\($0.id ?? 0)" == row.tag }).first
            self.presenter?.toRoleEdit()
        }
    }
}

extension GuildRolesEditViewController: GuildRolesEditPresenterOutput {
    func sync(didFetchRepositories repositories: RoleEntities) {
        self.repositories = repositories
        self.reloadForm()
    }
    
    func guildRolesEditPresenter(afterCreate repository: RoleEntity, in guildRolesEditPresenter: GuildRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            self.presenter?.fetch(uid: self.repository.uid)
        })
    }
    
    func guildRolesEditPresenter(onError error: Error, in guildRolesEditPresenter: GuildRolesEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
