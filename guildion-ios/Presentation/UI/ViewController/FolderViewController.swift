//
//  FolderViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import Nuke
import PKHUD
import Lightbox

class FolderViewController: BaseViewController {
    @IBOutlet weak var actionButton: ActionButton! {
        didSet {
            actionButton.delegate = self
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!
    var presenter: FolderPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var current_guild: GuildEntity = GuildEntity()
    var repository: FolderEntity = FolderEntity()
    enum Section: Int {
        case header
        case references
        case count
        
        func getCount(_ repository: FolderEntity) -> Int {
            switch self {
            case .header: return HeaderRow.count.rawValue
            case .references: return repository.FileReferences?.count ?? 0
            case .count: return 0
            }
        }
    }
    enum HeaderRow: Int {
        case content
        case count
    }
    fileprivate var fetched: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        title = repository.name
        navigationMode = .edit
        FolderViewControllerBuilder.rebuild(self)
        configureCollectionView()
        configureEditModeTabBar()
        let setting = UIBarButtonItem(image: R.image.gear()!.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.onTapSettingButton(_:)))
        navigationItem.rightBarButtonItems?.append(setting)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        presenter?.fetch(id: repository.id ?? 0)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
        self.setEditing(false, animated: true)
    }
    
    @objc func onTapSettingButton(_ sender: Any) {
    }
    
    public func setRepository(_ repository: FolderEntity) {
        self.repository = repository
        self.collectionView.reloadData()
    }
    
    public func initRepository(_ repository: FolderEntity, current_guild: GuildEntity, current_member: MemberEntity) {
        guard !fetched, let id = repository.id else { return }
        self.repository = repository
        self.current_guild = current_guild
        self.presenter?.fetch(id: id)
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.collectionView.allowsMultipleSelection = editing
        let indexPaths = self.collectionView.indexPathsForVisibleItems
        for indexPath in indexPaths {
            if let cell = self.collectionView.cellForItem(at: indexPath) as? FileCollectionViewCell {
                cell.setEditing(editing, animated: animated)
            }
            if !editing {
                self.collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        
        if let baseTabBarController = self.tabBarController as? BaseTabBarController {
            baseTabBarController.switchEditMode(editing)
            baseTabBarController.setEditCount(collectionView.indexPathsForSelectedItems?.count ?? 0)
        }
    }
}

extension FolderViewController: UICollectionViewDelegate {
    private func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        self.collectionView.collectionViewLayout = layout
        self.collectionView.register([
            UINib(resource: R.nib.fileCollectionViewCell): R.nib.fileCollectionViewCell.name,
            UINib(resource: R.nib.folderHeaderViewCollectionViewCell): R.nib.folderHeaderViewCollectionViewCell.name,
        ])
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.prefetchDataSource = self
        self.collectionView.backgroundColor = Theme.themify(key: .background)
        self.collectionView.alwaysBounceVertical = true
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Section.count.rawValue
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditing {
            if let baseTabBarController = self.tabBarController as? BaseTabBarController, isEditing {
                baseTabBarController.setEditCount(collectionView.indexPathsForSelectedItems?.count ?? 0)
            }
        } else {
            collectionView.deselectItem(at: indexPath, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if isEditing {
            if let baseTabBarController = self.tabBarController as? BaseTabBarController, isEditing {
                baseTabBarController.setEditCount(collectionView.indexPathsForSelectedItems?.count ?? 0)
            }
        }
    }
}

extension FolderViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let section = Section.init(rawValue: section) else { return 0 }
        return section.getCount(repository)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let section = Section.init(rawValue: indexPath.section) else { return UICollectionViewCell() }
        switch section {
        case .header: return self.collectionView(collectionView, cellForHeaderItemAt: indexPath)
        case .references: return self.collectionView(collectionView, cellForReferenceItemAt: indexPath)
        default: return UICollectionViewCell()
        }
    }
    
    fileprivate func collectionView(_ collectionView: UICollectionView, cellForHeaderItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.folderHeaderViewCollectionViewCell.name, for: indexPath) as? FolderHeaderViewCollectionViewCell else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    fileprivate func collectionView(_ collectionView: UICollectionView, cellForReferenceItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.fileCollectionViewCell.name, for: indexPath) as? FileCollectionViewCell, let repository = repository.FileReferences?[safe: indexPath.row] else {
            fatalError("cell is not exists")
        }
        cell.setRepository(repository.File ?? FileEntity())
        return cell
    }
}

extension FolderViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repository.FileReferences?[safe: $0.row], let file = repository.File, let url = URL(string: file.thumbnail) {
                requests.append(ImageRequest(url: url))
            }
        }
        if let url = URL(string: repository.thumbnail) {
            requests.append(ImageRequest(url: url))
        }
        return requests
    }
}

extension FolderViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let section = Section.init(rawValue: indexPath.section) else { return .zero }
        switch section {
        case .header: return FolderHeaderViewCollectionViewCell.getSize()
        case .references: return FileCollectionViewCell.getSize()
        default: return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        guard let section = Section.init(rawValue: section) else { return 0}
        switch section {
        case .header: return 0
        case .references: return FileCollectionViewCell.inset.left
        default: return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        guard let section = Section.init(rawValue: section) else { return 0}
        switch section {
        case .header: return 0
        case .references: return FileCollectionViewCell.inset.top
        default: return 0
        }
    }
}

extension FolderViewController: FolderHeaderViewDelegate {
    func folderHeaderView(onTapMember repository: MemberEntity, in folderHeaderView: FolderHeaderView) {
    }
}

extension FolderViewController: BaseTabBarControllerEditDelegate {
    func baseTabBarController(shouldTrash baseTabBarController: BaseTabBarController) {
        let entities = FileReferenceEntities()
        entities.items = self.collectionView.indexPathsForSelectedItems?.compactMap({ repository.FileReferences?[safe: $0.row] }) ?? []
        self.presenter?.deleteFileReferences(repositories: entities)
    }
    
    func baseTabBarController(shouldShare baseTabBarController: BaseTabBarController) {
    }
    
    fileprivate func configureEditModeTabBar() {
        if let unwrapped = self.tabBarController as? BaseTabBarController {
            unwrapped.editDelegate = self
        }
    }
}

extension FolderViewController: FileSelectFromFolderViewControllerDelegate {
    func fileSelectFromFolderViewController(shouldDismiss selecteds: FileEntities, in fileSelectFromFolderViewController: FileSelectFromFolderViewController) {
        self.presenter?.createFileReferences(repositories: selecteds, to: self.repository)
    }
}

extension FolderViewController: ActionButtonDelegate {
    func actionButton(onTap actionButton: ActionButton) {
        self.presenter?.toFileSelect()
    }
}

extension FolderViewController: FileMenuViewControllerDelegate {
    func toImageViewer(repository: FileEntity) {
        let lImage = LightboxImage(
            image: UIImage(url: repository.url),
            text: ""
        )
        let controller = LightboxController(images: [lImage])
        //controller.dismissalDelegate = self
        //controller.pageDelegate = self
        controller.dynamicBackground = true
        controller.modalPresentationStyle = .currentContext
        self.present(controller, animated: true, completion: nil)
    }
    
    func toWebBrowser(url: URL) {
        let webkitViewController = WebKitViewController(nibName: R.nib.webKitViewController.name, bundle: nil)
        let navVC = PanContainerViewNavigationController()
        navVC.viewControllers.append(webkitViewController)
        webkitViewController.defaultMedia = true
        webkitViewController.setURL(url)
        navVC.searchDelegate = webkitViewController
        navVC.modalPresentationStyle = .overCurrentContext
        self.present(navVC, animated: true)
    }
}

extension FolderViewController: FolderPresenterOutput {
    func folderPresenter(onError error: Error, in folderPresenter: FolderPresenter) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func folderPresenter(didCreate repositories: FileReferenceEntities, in folderPresenter: FolderPresenter) {
        presenter?.fetch(id: repository.id ?? 0)
    }
    
    func folderPresenter(didDelete folderPresenter: FolderPresenter) {
        presenter?.fetch(id: repository.id ?? 0)
    }
    
    func sync(didFetchRepository repository: FolderEntity) {
        self.repository = repository
        self.repository.FileReferences = FileReferenceDispatcher.binds(contents: repository.FileReferences ?? FileReferenceEntities(), state: store.state.fileReference)
        self.fetched = true
        DispatchQueue.main.async { [unowned self] in
            self.collectionView.reloadData()
        }
    }
}
