//
//  ChannelNewViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import PKHUD
import TextFieldEffects

class ChannelNewViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var nameField: HoshiTextField! {
        didSet {
            self.nameField.font = Font.FT(size: .FL, family: .L)
            self.nameField.textColor = Theme.themify(key: .string)
            self.nameField.placeholderColor = Theme.themify(key: .placeholder)
            switch mode {
            case .normal: self.nameField.placeholder = R.string.localizable.channelName()
            case .section: self.nameField.placeholder =  R.string.localizable.sectionChannelName()
            case .dm: self.nameField.placeholder = R.string.localizable.channelName()
            }
            self.nameField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.nameField.borderActiveColor = Theme.themify(key: .main)
            self.nameField.delegate = self
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            switch mode {
            case .normal: self.titleLabel.text = R.string.localizable.channelNewTitle()
            case .section: self.titleLabel.text = R.string.localizable.sectionChannelNewTitle()
            case .dm: self.titleLabel.text = R.string.localizable.dmNewTitle()
            }
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            tableViewHeightConstraint.constant = 0
        }
    }
    var presenter: ChannelNewPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: ChannelableProtocol = TextChannelEntity()
    var section: SectionChannelEntity = SectionChannelEntity()
    var guild: GuildEntity = GuildEntity()
    var member: MemberEntity = MemberEntity()
    enum Mode: Int {
        case normal
        case section
        case dm
        
        var numberOfRows: Int {
            switch self {
            case .normal: return 5
            case .section: return 0
            case .dm: return 4
            }
        }
    }
    public var mode: Mode = .normal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelNewViewControllerBuilder.rebuild(self)
        self.navigationMode = .buttonModal(title: R.string.localizable.create())
        self.setFlatBackground()
        configureTableView()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard(_:)))
        gesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(gesture)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        nameField.text == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.nameField.resignFirstResponder()
    }
    
    @objc func closeKeyboard(_ sender: Any) {
        self.nameField.resignFirstResponder()
    }
    
    override func onTapNavigationDoneButton(_ sender: Any) {
        switch mode {
        case .normal:
            switch repository.channelable_type.superChannel {
            case .TextChannel:
                guard let channel = repository.toTextChannel() else {
                    HUD.flash(.error)
                    return
                }
                channel.name = nameField.text ?? ""
                channel.Guild = guild.toNewMemory()
                channel.GuildId = guild.id
                channel.Section = section.toNewMemorySection()
                channel.SectionId = section.id
                channel.is_nsfw = false
                channel.read_only = repository.channelable_type == .AnnouncementChannel
                HUD.show(.progress)
                self.presenter?.create(repository: channel)
            case .StreamChannel:
                guard let channel = repository.toStreamChannel() else {
                    HUD.flash(.error)
                    return
                }
                channel.name = nameField.text ?? ""
                channel.Guild = guild.toNewMemory()
                channel.GuildId = guild.id
                channel.Section = section.toNewMemorySection()
                channel.SectionId = section.id
                channel.voice_only = repository.channelable_type == .VoiceChannel
                channel.movie_only = repository.channelable_type == .MusicChannel
                channel.is_dm = false
                channel.is_nsfw = false
                channel.temporary = false
                HUD.show(.progress)
                self.presenter?.create(repository: channel)
            default: break
            }
        case .section:
            self.repository = SectionChannelEntity()
            guard let channel = repository.toSectionChannel() else {
                HUD.flash(.error)
                return
            }
            channel.name = nameField.text ?? ""
            channel.Guild = guild.toNewMemory()
            channel.GuildId = guild.id
            HUD.show(.progress)
            self.presenter?.create(repository: channel)
        case .dm:
            switch repository.channelable_type.superChannel {
            case .TextChannel:
                self.repository = DMChannelEntity()
                guard let channel = repository.toDMChannel(), section.is_dm else {
                    HUD.flash(.error)
                    return
                }
                channel.name = nameField.text ?? ""
                channel.Guild = guild.toNewMemory()
                channel.GuildId = guild.id
                channel.Section = section.toNewMemorySection()
                channel.SectionId = section.id
                self.repository = channel
                self.presenter?.toMemberSelect()
            case .StreamChannel:
                guard let channel = repository.toStreamChannel(), section.is_dm else {
                    HUD.flash(.error)
                    return
                }
                channel.name = nameField.text ?? ""
                channel.Guild = guild.toNewMemory()
                channel.GuildId = guild.id
                channel.Section = section.toNewMemorySection()
                channel.SectionId = section.id
                channel.is_dm = true
                channel.voice_only = repository.channelable_type == .VoiceChannel
                channel.movie_only = repository.channelable_type == .MusicChannel
                self.repository = channel
                self.presenter?.toMemberSelect()
            default: break
            }
        }
    }
}

extension ChannelNewViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        nameField.text == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nameField.text == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        nameField.text == "" ? self.resetRightNavigationBarButton() : self.setupRightButtonBarButton()
    }
}

extension ChannelNewViewController: UITableViewDelegate {
    private func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register([
            UINib(resource: R.nib.channelSelectTableViewCell): R.nib.channelSelectTableViewCell.name,
        ])
        tableView.register(
            UINib(resource: R.nib.textTableViewHeaderFooterView),
            forHeaderFooterViewReuseIdentifier: R.nib.textTableViewHeaderFooterView.name
        )
        self.tableView.isScrollEnabled = false
        self.tableViewHeightConstraint.constant = mode == .section ? 0 : (ChannelSelectTableViewCell.cellHeight * CGFloat(mode.numberOfRows)) + TextTableViewHeaderFooterView.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ChannelSelectTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: self.repository = TextChannelEntity()
        case 1: self.repository = StreamChannelEntity()
        case 2: self.repository = StreamChannelEntity(channelable_type: .MusicChannel)
        case 3: self.repository = StreamChannelEntity(channelable_type: .VoiceChannel)
        case 4: self.repository = TextChannelEntity(channelable_type: .AnnouncementChannel)
        default: break
        }
        self.tableView.reloadData()
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.nib.textTableViewHeaderFooterView.name) as? TextTableViewHeaderFooterView else {
            fatalError("This section is not exist")
        }
        view.titleLabel.text = R.string.localizable.kindChannel()
        view.titleLabel.font = Font.FT(size: .FM, family: .L)
        view.backgroundColor = Theme.themify(key: .backgroundThick)
        view.contentView.backgroundColor = Theme.themify(key: .backgroundThick)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return TextTableViewHeaderFooterView.height
    }
}

extension ChannelNewViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mode.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0: return self.tableView(tableView, cellForTextChannelRowAt: indexPath)
        case 1: return self.tableView(tableView, cellForStreamChannelRowAt: indexPath)
        case 2: return self.tableView(tableView, cellForMusicChannelRowAt: indexPath)
        case 3: return self.tableView(tableView, cellForVoiceChannelRowAt: indexPath)
        case 4: return self.tableView(tableView, cellForAnnouncementChannelRowAt: indexPath)
        default: return UITableViewCell()
        }
    }
    
    private func tableView(_ tableView: UITableView, cellForTextChannelRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.channelSelectTableViewCell.name, for: indexPath) as? ChannelSelectTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.nameLabel.text = ChannelableType.TextChannel.string
        cell.channelImageView.image = ChannelableType.TextChannel.image
        cell.channelImageView.isHidden = false
        cell.streamChannelImageView.isHidden = true
        cell.checked = repository.channelable_type == .TextChannel
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForStreamChannelRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.channelSelectTableViewCell.name, for: indexPath) as? ChannelSelectTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.nameLabel.text = ChannelableType.StreamChannel.string
        cell.channelImageView.isHidden = true
        cell.streamChannelImageView.isHidden = false
        cell.streamChannelImageView.image = ChannelableType.StreamChannel.image
        cell.checked = repository.channelable_type == .StreamChannel
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForAnnouncementChannelRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.channelSelectTableViewCell.name, for: indexPath) as? ChannelSelectTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.nameLabel.text = ChannelableType.AnnouncementChannel.string
        cell.streamChannelImageView.image = ChannelableType.AnnouncementChannel.image
        cell.streamChannelImageView.isHidden = false
        cell.channelImageView.isHidden = true
        cell.checked = repository.channelable_type == .AnnouncementChannel
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForMusicChannelRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.channelSelectTableViewCell.name, for: indexPath) as? ChannelSelectTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.nameLabel.text = ChannelableType.MusicChannel.string
        cell.channelImageView.isHidden = true
        cell.streamChannelImageView.isHidden = false
        cell.streamChannelImageView.image = ChannelableType.MusicChannel.image
        cell.checked = repository.channelable_type == .MusicChannel
        return cell
    }
    
    private func tableView(_ tableView: UITableView, cellForVoiceChannelRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.channelSelectTableViewCell.name, for: indexPath) as? ChannelSelectTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.nameLabel.text = ChannelableType.VoiceChannel.string
        cell.channelImageView.isHidden = true
        cell.streamChannelImageView.isHidden = false
        cell.streamChannelImageView.image = ChannelableType.VoiceChannel.image
        cell.checked = repository.channelable_type == .VoiceChannel
        return cell
    }
}

extension ChannelNewViewController: MemberSelectViewControllerDelegate {
    func memberSelectViewController(shouldDismiss selecteds: MemberEntities, in memberSelectViewController: MemberSelectViewController) {
        guard selecteds.count > 0 else { return }
        switch repository.channelable_type.superChannel {
        case .TextChannel: break
        case .StreamChannel:
            guard let channel = repository.toStreamChannel(), mode == .dm else {
                HUD.flash(.error)
                return
            }
            channel.Members = MemberEntities()
            channel.Members = selecteds.toNewMemory()
            channel.Members?.append(member)
            channel.is_private = true
            HUD.show(.progress)
            self.presenter?.create(repository: channel)
        case .DMChannel:
            guard let channel = repository.toDMChannel(), mode == .dm else {
                HUD.flash(.error)
                return
            }
            channel.Members = MemberEntities()
            channel.Members = selecteds.toNewMemory()
            channel.Members?.append(member)
            channel.Members?.unified()
            channel.is_private = true
            HUD.show(.progress)
            self.presenter?.create(repository: channel)
        default: break
        }
    }
}

extension ChannelNewViewController: ChannelNewPresenterOutput {
    func channelNewPresenter(onError error: Error) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func channelNewPresenter(didCreated repository: ChannelableProtocol) {
        HUD.hide()
        HUD.flash(.success)
        self.dismissShouldAppear(animated: true)
    }
}
