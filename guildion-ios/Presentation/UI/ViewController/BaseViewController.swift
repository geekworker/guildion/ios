//
//  BaseViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/09/28.
//

import UIKit
import Nuke
import M13ProgressSuite
import ReSwift
import RxSwift
import RxCocoa
import Hydra
import PKHUD
import NotificationBannerSwift
import SwiftMessages
import Toast_Swift

class BaseViewController: UIViewController {
    private var progressing = false
    private var progress: CGFloat = 0
    public var disposeBag = DisposeBag()
    
    enum NavigationMode {
        case guilds
        case explore
        case noneBorder
        case modal
        case modalBorder
        case buttonModal(title: String)
        case edit
        case none
        case other
    }
    
    var navigationMode = NavigationMode.other
    var sessionChecking: Bool = true
    var preheater = ImagePreheater()
    fileprivate var _flatBackgroundImageView: UIImageView?

    override func viewDidLoad() {
        switch navigationMode {
        case .guilds:
            self.view.backgroundColor = Theme.themify(key: .background)
        case .explore:
            self.view.backgroundColor = Theme.themify(key: .background)
        case .edit:
            self.view.backgroundColor = Theme.themify(key: .background)
        case .noneBorder:
            self.view.backgroundColor = Theme.themify(key: .backgroundThick)
        case .modal, .modalBorder:
            self.view.backgroundColor = Theme.themify(key: .backgroundThick)
        case .buttonModal:
            self.resetRightNavigationBarButton()
            self.view.backgroundColor = Theme.themify(key: .backgroundThick)
        case .other:
            self.view.backgroundColor = Theme.themify(key: .backgroundThick)
        case .none: break
        }
        configureProgressBar()
        configureObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        store.subscribe(self)
        WebSocketHandler.shared.inject(self)
        self.sessionCheck()
        self.setupNavigationBar()
        self.fixOrientation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.global(qos: .background).async {
            self.websocket_health_check().then({ self.viewWebSocketHealthChecked($0) })
        }
    }
    
    func viewWebSocketHealthChecked(_ health: Bool) {
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        store.unsubscribe(self)
        WebSocketHandler.shared.reject(self)
    }
    
    override var shouldAutorotate: Bool {
        if let vc = LiveConfig.playingStreamChannelViewController, !vc.miniScreenTransitionContext.isMiniScreenViewPresented {
            return vc.shouldAutorotate
        }
        return UIDevice.current.orientation == .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if let vc = LiveConfig.playingStreamChannelViewController, !vc.miniScreenTransitionContext.isMiniScreenViewPresented {
            return vc.supportedInterfaceOrientations
        }
        return .portrait
    }
    
    func dismissShouldAppear(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard self.tabBarController as? BaseTabBarController == nil else { return }
        self.dismiss(animated: flag, completion: {
            UIViewController.topMostController().viewWillAppear(false)
            UIViewController.topMostController().viewDidAppear(false)
            UIApplication.firstWindow.bringSubviewToFront(MiniScreenView.getInstance())
            completion?()
        })
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard self.tabBarController as? BaseTabBarController == nil else { completion?(); return }
        super.dismiss(animated: flag, completion: completion)
    }
    
    fileprivate func fixOrientation() {
        guard UIDevice.current.orientation != .portrait else { return }
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func configureObserver() {
        AppConfig.sessionOutEvent
            .subscribe(
                onNext: { [weak self] result in
                    DispatchQueue.main.async {
                        if result {
                            self?.view.isHidden = result
                            BaseWireframe.toWelcome()
                        }
                    }
                }
            ).disposed(
                by: disposeBag
            )
    }
    
    func sessionCheck() {
        guard CurrentUser.getCurrentUserEntity() == nil, ((self as? WelcomeViewController) == nil), sessionChecking else { return }
        self.view.isHidden = true
        BaseWireframe.toWelcome()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = Theme.themify(key: .backgroundThick)
        navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)]
        navigationController?.navigationBar.isTranslucent = false
        
        switch navigationMode {
        case .guilds:
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onTapNavigationAddButton(_:)))
            let space = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            space.width = -8
            let scanner = UIBarButtonItem(image: R.image.qrScanner()!.withRenderingMode(.alwaysTemplate).resizedImage(newSize: CGSize(width: 18, height: 18)), style: .plain, target: self, action: #selector(self.onTapNavigationQRScannerButton(_:)))
            navigationItem.rightBarButtonItems = [add, space, scanner]
            let setting = UIBarButtonItem(image: R.image.listEdit()!.withRenderingMode(.alwaysTemplate).resizedImage(newSize: CGSize(width: 20, height: 18)), style: .plain, target: self, action: #selector(self.onTapNavigationEditButton(_:)))
            navigationItem.leftBarButtonItems = [setting]
        case .explore:
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            let scanner = UIBarButtonItem(image: R.image.qrScanner()!.withRenderingMode(.alwaysTemplate).resizedImage(newSize: CGSize(width: 18, height: 18)), style: .plain, target: self, action: #selector(self.onTapNavigationQRScannerButton(_:)))
            navigationItem.rightBarButtonItems = [scanner]
        case .edit:
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            navigationItem.rightBarButtonItems = [editButtonItem]
        case .noneBorder:
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
        case .modal:
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
            if navigationController?.viewControllers.count == 1 {
                let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
                navigationItem.leftBarButtonItems = [exit]
            }
        case .buttonModal(_):
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
            if navigationController?.viewControllers.count == 1 {
                let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
                navigationItem.leftBarButtonItems = [exit]
            }
        case .modalBorder:
            if navigationController?.viewControllers.count == 1 {
                let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
                navigationItem.leftBarButtonItems = [exit]
            }
        case .other:
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        case .none:
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    @objc func onTapNavigationAddButton(_ sender: Any) {
    }
    
    @objc func onTapNavigationDoneButton(_ sender: Any) {
    }
    
    @objc func onTapNavigationEditButton(_ sender: Any) {
    }
    
    @objc func onTapNavigationQRScannerButton(_ sender: Any) {
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    func setupRightButtonBarButton() {
        switch navigationMode {
        case .buttonModal(let title):
            guard self.navigationItem.rightBarButtonItems?.count == 0 else { return }
            let next = UIBarButtonItem(title: title, style: .done, target: self, action: #selector(onTapNavigationDoneButton(_:)))
            self.navigationItem.setRightBarButtonItems([next], animated: true)
        default: break
        }
    }
    
    func resetRightNavigationBarButton() {
        self.navigationItem.setRightBarButtonItems([], animated: true)
    }
    
    func switchNavigationBarScrollViewWillEnd(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y > 0) {
            UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
            }, completion: nil)
        }
    }
    
    func setFlatBackground() {
        let width = view.bounds.width
        let height = view.bounds.height
        _flatBackgroundImageView = UIImageView()
        _flatBackgroundImageView?.image = R.image.cloudBackgroundIphone()?.withRenderingMode(.alwaysOriginal)
        _flatBackgroundImageView?.alpha = 0.03
        _flatBackgroundImageView?.contentMode = .scaleAspectFit
        _flatBackgroundImageView?.frame = CGRect(x: -width * 0.1, y: -height * 0.1, width: width * 1.2, height: height * 1.2)
        view.insertSubview(_flatBackgroundImageView!, at: 0)
    }
    
    internal func selectedQueuePosition() -> QueuePosition {
        return .front
    }
    
    internal func selectedBannerPosition() -> BannerPosition {
        return .top
    }
}

extension BaseViewController {
    func configureProgressBar() {
        self.navigationController?.setBackgroundColor(Theme.themify(key: .backgroundThick))
        self.navigationController?.setPrimaryColor(Theme.themify(key: .main))
        self.navigationController?.setSecondaryColor(Theme.themify(key: .borderSecondary))
    }
    
    func updateProgress() {
        DispatchQueue.main.async { [weak self] in
            if self?.progressing ?? false {
                if !(self?.navigationController?.isShowingProgressBar() ?? false) {
                    self?.navigationController?.showProgress()
                }
                self?.navigationController?.setProgress(self?.progress ?? 0, animated: true)
                if self?.progress ?? 0 >= 1 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                        self?.navigationController?.finishProgress()
                        store.dispatch(AppDispatcher.FINISH_PROGRESS())
                    }
                }
            } else {
                self?.navigationController?.cancelProgress()
            }
        }
    }
}

extension BaseViewController: WebSocketHandlerDelegate {
    public func websocket_health_check() -> Promise<Bool> {
        return Promise<Bool> { resolve, reject, _ in
            guard !WebSocketHandler.shared.connected else { resolve(true); return }
            if !WebSocketHandler.shared.retrying {
                WebSocketHandler.shared.startWebSocketWithAttempt(
                ).then(in: .main, {
                    DispatchQueue.main.async {
                        self.view.hideAllToasts()
                    }
                    resolve(true)
                }).catch({ _ in
                    self.websocket_health_check().then({ _ in })
                })
            } else {
                WebSocketHandler.shared.connect_promise?.then(in: .main, {
                    DispatchQueue.main.async {
                        self.view.hideAllToasts()
                    }
                    resolve(true)
                }).catch({ _ in
                    self.websocket_health_check().then({ _ in })
                })
            }
        }
    }
    
    func webSocketHandler(didRetry count: Int, error: Error) {
        DispatchQueue.main.async {
            self.view.makeToast(R.string.localizable.domainErrorDetail())
        }
    }
    
    func webSocketHandlerDidDisconnect() {
        DispatchQueue.main.async {
            if !AppConfig.is_background {
                self.view.makeToast(R.string.localizable.errorDisconnect())
            }
        }
        DispatchQueue.global(qos: .background).async {
            self.websocket_health_check().then({ self.viewWebSocketHealthChecked($0) })
        }
    }
}

extension BaseViewController: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    func newState(state: StoreSubscriberStateType) {
        if self.progress != state.app.progress || self.progressing != state.app.progressing {
            self.progress = state.app.progress
            self.progressing = state.app.progressing
            updateProgress()
        }
    }
}

extension UIViewController {
    func setBaseTabBarHidden(_ hidden: Bool, animated: Bool = true, duration: TimeInterval = 0.3) {
        guard let baseTabBarController = self.tabBarController as? BaseTabBarController else { return }
        baseTabBarController.tabBar.isHidden = hidden
    }
}
