//
//  SessionViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit
import TextFieldEffects
import WebBrowser
import PKHUD
import SwiftMessages

class SessionViewController: BaseViewController {
    @IBOutlet weak var nicknameField: HoshiTextField! {
        didSet {
            self.nicknameField.font = Font.FT(size: .FL, family: .L)
            self.nicknameField.textColor = Theme.themify(key: .string)
            self.nicknameField.placeholderColor = Theme.themify(key: .placeholder)
            self.nicknameField.placeholder = R.string.localizable.nickname()
            self.nicknameField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.nicknameField.borderActiveColor = Theme.themify(key: .main)
            self.nicknameField.delegate = self
        }
    }
    @IBOutlet weak var passwordField: HoshiTextField! {
        didSet {
            self.passwordField.font = Font.FT(size: .FL, family: .L)
            self.passwordField.textColor = Theme.themify(key: .string)
            self.passwordField.placeholderColor = Theme.themify(key: .placeholder)
            self.passwordField.placeholder = R.string.localizable.password()
            self.passwordField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.passwordField.borderActiveColor = Theme.themify(key: .main)
            self.passwordField.delegate = self
            self.passwordField.isSecureTextEntry = true
            if #available(iOS 12.0, *) {
                self.passwordField.textContentType = .newPassword
            } else {
                self.passwordField.textContentType = .password
            }
        }
    }
    @IBOutlet weak var emailField: HoshiTextField! {
        didSet {
            self.emailField.font = Font.FT(size: .FL, family: .L)
            self.emailField.textColor = Theme.themify(key: .string)
            self.emailField.placeholderColor = Theme.themify(key: .placeholder)
            self.emailField.placeholder = R.string.localizable.email()
            self.emailField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.emailField.borderActiveColor = Theme.themify(key: .main)
            self.emailField.delegate = self
            self.emailField.textContentType = .username
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = R.string.localizable.memberRegister()
        }
    }
    @IBOutlet weak var nicknameLabel: UILabel! {
        didSet {
            self.nicknameLabel.text = R.string.localizable.sessionNickname()
        }
    }
    @IBOutlet weak var accountLabel: UILabel! {
        didSet {
            self.accountLabel.text = R.string.localizable.sessionAccountInfo()
        }
    }
    @IBOutlet weak var signupButton: SimpleButton! {
        didSet {
            self.signupButton.text = R.string.localizable.memberRegister()
            self.signupButton.normalColor = Theme.themify(key: .main)
            self.signupButton.canceledColor = Theme.themify(key: .cancel)
            self.signupButton.canceled = true
            self.signupButton.delegate = self
        }
    }
    var repository: SessionModel = SessionModel()
    var presenter: SessionPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        SessionViewControllerBuilder.rebuild(self)
        sessionChecking = false
        self.navigationMode = .noneBorder
        self.setFlatBackground()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        sessionChecking = false
        self.presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProfileSettingViewController, segue.identifier == R.segue.sessionViewController.toProfileSetting.identifier {
            vc.repository = self.repository
        }
    }
    
    func close(_ sender: UITapGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
    
    func register() {
        HUD.show(.progress)
        self.presenter?.register(repository: self.repository)
    }
    
    @IBAction func onTapPrivacyButton(_ sender: Any) {
        let webBrowserViewController = WebBrowserViewController()
        webBrowserViewController.language = .english
        webBrowserViewController.tintColor = Theme.themify(key: .main)
        webBrowserViewController.barTintColor = Theme.themify(key: .backgroundContrast)
        webBrowserViewController.isToolbarHidden = false
        webBrowserViewController.isShowActionBarButton = true
        webBrowserViewController.toolbarItemSpace = 50
        webBrowserViewController.isShowURLInNavigationBarWhenLoading = true
        webBrowserViewController.isShowPageTitleInNavigationBar = true

        webBrowserViewController.loadURLString(Constant.APP_URL + "/privacy")
        let navigationWebBrowser = WebBrowserViewController.rootNavigationWebBrowser(webBrowser: webBrowserViewController)
        UIViewController.topMostController().present(navigationWebBrowser, animated: true, completion: nil)
    }
    
    @IBAction func onTapTermButton(_ sender: Any) {
        let webBrowserViewController = WebBrowserViewController()
        webBrowserViewController.language = .english
        webBrowserViewController.tintColor = Theme.themify(key: .main)
        webBrowserViewController.barTintColor = Theme.themify(key: .backgroundContrast)
        webBrowserViewController.isToolbarHidden = false
        webBrowserViewController.isShowActionBarButton = true
        webBrowserViewController.toolbarItemSpace = 50
        webBrowserViewController.isShowURLInNavigationBarWhenLoading = true
        webBrowserViewController.isShowPageTitleInNavigationBar = true

        webBrowserViewController.loadURLString(Constant.APP_URL + "/term")
        let navigationWebBrowser = WebBrowserViewController.rootNavigationWebBrowser(webBrowser: webBrowserViewController)
        UIViewController.topMostController().present(navigationWebBrowser, animated: true, completion: nil)
    }
}

extension SessionViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.register()
    }
}

extension SessionViewController: UITextFieldDelegate {

    func textFieldDidChangeSelection(_ textField: UITextField) {
         if (textField == self.nicknameField) {
            self.repository.nickname = textField.text ?? ""
            guard self.repository.identity?.email != "", self.repository.identity?.password != "", self.repository.nickname != "" else {
                self.signupButton.canceled = true
                return
            }
            self.signupButton.canceled = false
         } else if (textField == self.emailField) {
            self.repository.identity?.email = textField.text ?? ""
            guard self.repository.identity?.email != "", self.repository.identity?.password != "", self.repository.nickname != "" else {
                self.signupButton.canceled = true
                return
            }
            self.signupButton.canceled = false
        } else if (textField == self.passwordField) {
            self.repository.identity?.password = textField.text ?? ""
            guard self.repository.identity?.password != "", self.repository.identity?.email != "", self.repository.nickname != "" else {
                self.signupButton.canceled = true
                return
            }
            self.signupButton.canceled = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.nicknameField) {
            self.repository.nickname = textField.text ?? ""
            self.signupButton.canceled = !(self.repository.identity?.email != "" && self.repository.identity?.password != "" && self.repository.nickname != "")
            textField.resignFirstResponder()
            emailField.becomeFirstResponder()
        } else if (textField == self.emailField) {
            self.repository.identity?.email = textField.text ?? ""
            self.signupButton.canceled = !(self.repository.identity?.email != "" && self.repository.identity?.password != "" && self.repository.nickname != "")
            textField.resignFirstResponder()
            passwordField.becomeFirstResponder()
        } else if (textField == self.passwordField) {
            self.repository.identity?.password = textField.text ?? ""
            self.signupButton.canceled = !(self.repository.identity?.email != "" && self.repository.identity?.password != "" && self.repository.nickname != "")
            textField.resignFirstResponder()
            self.register()
        }
        return true
    }
}

extension SessionViewController: SessionPresenterOutput {
    func sync(didFetchRepository repository: SessionModel) {
        self.repository = repository
        DispatchQueue.main.async { [unowned self] in
            self.nicknameField.text = self.repository.nickname
            self.emailField.text = self.repository.identity?.email
            self.passwordField.text = self.repository.identity?.password
        }
    }
    
    func registered() {
        DispatchQueue.main.async { [unowned self] in
            HUD.hide()
            HUD.flash(.success)
            AppConfig.email_confirm_mode ? self.presenter?.toMailConfirm() : self.presenter?.toProfileSetting()
        }
    }
    
    func sessionPresenter(onError error: Error) {
        HUD.hide()
        let messageAlert = MessageView.viewFromNib(layout: .cardView)
        messageAlert.configureTheme(.error)
        messageAlert.configureDropShadow()
        messageAlert.configureContent(title: R.string.localizable.error(), body: error.getLocalizedReason())
        messageAlert.button?.isHidden = true
        var messageAlertConfig = SwiftMessages.defaultConfig
        messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
    }
}
