//
//  GuildSearchViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import Nuke

class GuildSearchViewController: BaseViewController {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var searchBar: SearchBar! {
        didSet {
            searchBar.placeholder = R.string.localizable.searchGuildPlaceholder()
            searchBar.buttonText = R.string.localizable.cancel()
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchCompleteViewContainer: UIView! {
        didSet {
            searchCompleteViewContainer.isHidden = true
        }
    }
    var presenter: GuildSearchPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: GuildEntities = GuildEntities()
    var keyword: String?
    var new_keyword: String?
    var searchCompleteViewController: SearchCompleteViewController = R.storyboard.explore.searchCompleteViewController()!
    
    enum Mode: Int {
        case normal
        case searching
        var searchBarTopConstant: CGFloat {
            switch self {
            case .normal: return 20
            case .searching: return 40
            }
        }
    }
    var mode: Mode = .normal {
        didSet {
            if isViewLoaded {
                self.updateSearchLayout()
            }
        }
    }
    
    override func viewDidLoad() {
        self.navigationMode = .explore
        super.viewDidLoad()
        GuildSearchViewControllerBuilder.rebuild(self)
        configureCollectionView()
        configureSearchCompleteViewController()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.title = keyword
        self.navigationController?.setNavigationBarHidden(mode == .searching, animated: false)
        updateSearchLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildSearchViewController, segue.identifier == R.segue.guildSearchViewController.toGuildSearch.identifier, let unwrarpped = new_keyword {
            GuildSearchViewControllerBuilder.rebuild(vc)
            vc.keyword = unwrarpped
        }
    }
    
    override func onTapNavigationQRScannerButton(_ sender: Any) {
        self.presenter?.toGuildQRCodeReader()
    }
    
    func updateSearchLayout() {
        // self.scrollView.isScrollEnabled = self.mode == .normal
        switch mode {
        case .normal: searchCompleteViewContainer.fadeOut(type: .Normal, completed: nil)
        case .searching:
            searchCompleteViewContainer.fadeIn(type: .Normal, completed: nil)
            // self.scrollView.setContentOffset(.zero, animated: true)
            _ = self.searchBar.becomeFirstResponder()
        }
        self.navigationController?.setNavigationBarHidden(mode == .searching, animated: true)
//        UIView.animate(withDuration: 0.3, animations: {
            self.searchBarTopConstraint.constant = self.mode.searchBarTopConstant
//        })
    }
    
    private var loadingIncrementStatus: Int?
}

extension GuildSearchViewController: SearchCompleteViewControllerDelegate {
    fileprivate func configureSearchCompleteViewController() {
        searchCompleteViewController.willMove(toParent: self)
        self.searchCompleteViewController.view.frame = self.searchCompleteViewContainer.frame
        self.searchCompleteViewContainer.addSubview(self.searchCompleteViewController.view)
        self.searchCompleteViewContainer.constrainToEdges(self.searchCompleteViewController.view)
        self.searchCompleteViewController.delegate = self
        self.addChild(searchCompleteViewController)
        searchCompleteViewController.didMove(toParent: self)
    }
}

extension GuildSearchViewController: SearchBarDelegate {
    func searchBarDidBeginEditing() {
        self.mode = .searching
    }
    
    func searchBarDidEndEditing() {
        guard new_keyword == nil || new_keyword?.count == 0 else { return }
        self.mode = .normal
    }
    
    func searchBar(_ searchBar: SearchBar, searched keyword: String?) {
        self.mode = .searching
        self.new_keyword = keyword
        guard let unwrapped = new_keyword, unwrapped.count > 0 else { return }
        self.presenter?.toGuildSearch(keyword: unwrapped)
    }
    
    func searchBar(_ searchBar: SearchBar, accessoryButtonTapped keyword: String?) -> Bool {
        self.new_keyword = nil
        searchBar.keyword = nil
        return true
    }
}

extension GuildSearchViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = GuildCollectionViewCell.margin
        layout.minimumInteritemSpacing = GuildCollectionViewCell.margin
        layout.scrollDirection = .vertical
        layout.itemSize = GuildCollectionViewCell.getSize(collectionViewFrame: UIScreen.main.bounds)
        collectionView.collectionViewLayout = layout
        collectionView.isScrollEnabled = true
        self.collectionView.register([
            UINib(resource: R.nib.guildCollectionViewCell): R.nib.guildCollectionViewCell.name,
        ])
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.prefetchDataSource = self
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(singleTapGesture(_:)))
        singleTap.cancelsTouchesInView = false
        collectionView.addGestureRecognizer(singleTap)
        // collectionViewHeightConstraint.constant = collectionView.contentSize.height
        collectionView.contentInset = UIEdgeInsets(top: 0, left: GuildCollectionViewCell.margin, bottom: GuildCollectionViewCell.margin, right: GuildCollectionViewCell.margin)
    }
    
    @objc func singleTapGesture(_ sender: Any) {}
}

extension GuildSearchViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.guildCollectionViewCell.name, for: indexPath) as? GuildCollectionViewCell, let repository = repositories[safe: indexPath.row] else {
            fatalError("The dequeued cell is not an instance of CollectionViewCell.")
        }
        cell.setRepository(repository)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if repositories.count > indexPath.row, let repository = repositories[safe: indexPath.row], let last_id = repositories.last?.id, repository.id == last_id, last_id != loadingIncrementStatus {
            loadingIncrementStatus = last_id
            self.presenter?.searchMore()
        }
    }
}

extension GuildSearchViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let picture = repositories[safe: $0.row]?.picture_small, let url = URL(string: picture) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension GuildSearchViewController: UICollectionViewDelegateFlowLayout {
}

extension GuildSearchViewController: GuildCollectionViewCellDelegate {
    func guildCollectionViewCell(onTap repository: GuildEntity, in guildCollectionViewCell: GuildCollectionViewCell) {
        self.presenter?.toGuildInfo(repository: repository)
    }
}

extension GuildSearchViewController: GuildSearchPresenterOutput {
    func sync(didFetchRepositories repositories: GuildEntities) {
        DispatchQueue.main.async { [unowned self] in
            self.repositories = repositories
            UIView.animate(withDuration: 0.0, animations: {
                self.collectionView.reloadData()
            }, completion: { _ in
                // self.collectionViewHeightConstraint.constant = self.collectionView.contentSize.height
            })
        }
    }
}
