//
//  DMChannelViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import PKHUD
import SideMenu

class DMChannelViewController: ChannelMessagesViewController {
    var presenter: DMChannelPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: DMChannelEntity = DMChannelEntity() {
        didSet {
            if let vc = SideMenuManager.default.rightMenuNavigationController?.viewControllers.first as? ChannelMenuViewController {
                vc.repository = repository
            }
        }
    }
    override var channel: MessageableEntity {
        get {
            self.repository
        }
        set {
            super.channel = newValue
        }
    }
    var fetched: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DMChannelViewControllerBuilder.rebuild(self)
        self.view.backgroundColor = Theme.themify(key: .background)
        self.view.clipsToBounds = true
        self.presenter?.fetchMembers()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.title = repository.name
        self.messagesCollectionView.backgroundColor = Theme.themify(key: .background)
        setupNavigationBar()
    }
    
    override func viewWebSocketHealthChecked(_ health: Bool) {
        super.viewWebSocketHealthChecked(health)
        self.presenter?.join()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
        if self.isMovingFromParent {
            presenter?.unbind()
        }
    }
    
    override func toMemberMenu(repository: MemberEntity) {
        self.presenter?.toMemberMenu(repository: repository)
    }
    
    override func toMessageMenu(repository: MessageEntity) {
        self.presenter?.toMessageMenu(repository: repository)
    }
    
    override func toWebBrowser(_ url: URL, repositories: FileEntities? = nil) {
        super.toWebBrowser(url, repositories: repositories)
        self.presenter?.toWebBrowser(url, repositories: repositories)
    }
    
    override func toImageViewer(repository: FileEntity) {
        self.presenter?.toImageViewer(repository: repository)
    }
    
    override func decrement() {
        self.presenter?.decrements()
    }
    
    override func sendMessage(_ newRepository: MessageEntity) {
        super.sendMessage(newRepository)
        if newRepository.id == 0 || newRepository.id == nil {
            for file in newRepository.Files?.items ?? [] { file.is_private = repository.is_private }
            self.presenter?.create(newRepository)
        } else {
            self.presenter?.update(newRepository)
        }
    }
    
    override func sendReaction(_ newRepository: ReactionEntity) {
        self.presenter?.create(newRepository)
    }
    
    override func willTyping() {
        self.presenter?.willTyping()
    }
    
    override func didTyping() {
        self.presenter?.didTyping()
    }
    
    func initalizeRepository(_ repository: DMChannelEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        guard !fetched else { return }
        self.repository = repository
        self.presenter?.repositories = repository
        self.current_guild = current_guild
        self.current_member = current_member
        self.permission = permission
        fetched = true
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = Theme.themify(key: .backgroundThick)
        navigationController?.navigationBar.tintColor = Theme.themify(key: .string)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Theme.themify(key: .string)]
        navigationController?.navigationBar.isTranslucent = false
        let members = UIBarButtonItem(image: R.image.members()!.withRenderingMode(.alwaysTemplate).resizedImage(newSize: CGSize(width: 26, height: 20)), style: .plain, target: self, action: #selector(onTapNavigationMembersButton(_:)
                                    ))
        navigationItem.rightBarButtonItems = [members]
    }
    
    @objc func onTapNavigationMembersButton(_ sender: Any) {
        self.presenter?.toChannelMenu()
    }
}

extension DMChannelViewController: DMChannelPresenterOutput {
    func sync(didFetchRepository repository: DMChannelEntity) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented
            self.view.hideAllToasts()
            self.fetched = true
            self.evalPermission()
            self.repository = repository
            self.title = repository.name
        }
    }
    
    func sync(didFetchRepositories repositories: MessageEntities) {
        DispatchQueue.main.async {
            self.view.hideAllToasts()
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented
            guard repositories.count > 0 else { return }
            let shouldScrollToBottom: Bool = self.repositories.count == 0 && repositories.count > 0
            self.repositories = repositories
            self.messagesCollectionView.reloadData()
            if shouldScrollToBottom {
                self.messagesCollectionView._scrollToBottom(animated: false)
            }
        }
    }
    
    func sync(didFetchRepositories repositories: MemberEntities) {
        DispatchQueue.main.async {
            self.current_members = repositories
            self.messagesCollectionView.reloadData()
        }
    }
    
    func syncCurrentMember(didFetchRepository repository: MemberEntity) {
        self.current_member = repository
    }
    
    func dmChannelPresenter(didRetry count: Int, in dmChannelPresenter: DMChannelPresenter) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.fadeOut(type: .Normal, completed: nil)
            self.view.makeToast(R.string.localizable.domainErrorDetail())
        }
    }
    
    func dmChannelPresenter(didIncrements repositories: MessageEntities) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented
            self.view.hideAllToasts()
            self.didIncrements(repositories)
        }
    }
    
    func dmChannelPresenter(didDecrements repositories: MessageEntities) {
        DispatchQueue.main.async {
            self.inputAccessoryView?.isHidden = self.containerTransitionContext.isPanContainerPresented
            self.view.hideAllToasts()
            self.didDecrements(repositories)
        }
    }
    
    func dmChannelPresenter(didCreated repository: MessageEntity) {
        didCreated(repository)
    }
    
    func dmChannelPresenter(onError error: Error) {
    }
    
    func dmChannelPresenter(didCreated repository: ReactionEntity) {
        DispatchQueue.main.async {
            self.messagesCollectionView.reloadDataAndKeepOffset()
        }
    }
    
    func dmChannelPresenter(willTyping repository: MemberEntity) {
        self.addTypingMember(repository: repository)
    }
    
    func dmChannelPresenter(didTyping repository: MemberEntity) {
        self.removeTypingMember(repository: repository)
    }
    
    func dmChannelPresenter(didCache repositories: MessageEntities, shouldScroll: Bool, in dmChannelPresenter: DMChannelPresenter) {
        self.didCaches(repositories: repositories, shouldScroll: shouldScroll)
    }
    
    func dmChannelPresenter(shouldReload dmChannelPresenter: DMChannelPresenter) {
        DispatchQueue.main.async {
            self.repositories = self._repositories
            self.messagesCollectionView.reloadDataAndKeepOffset()
        }
    }
}
