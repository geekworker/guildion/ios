//
//  FolderNewViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class FolderNewViewController: BaseViewController {
    var presenter: FolderNewPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: FolderEntity = FolderEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        FolderNewViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension FolderNewViewController: FolderNewPresenterOutput {
    func sync(didFetchRepository repository: FolderEntity) {
        self.repository = repository
    }
}

