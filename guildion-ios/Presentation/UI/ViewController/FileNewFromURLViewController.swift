//
//  FileNewFromURLViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit
import SPPermissions
import TextFieldEffects
import URLEmbeddedView
import Hydra
import PKHUD
import YoutubePlayer_in_WKWebView

protocol FileNewFromURLViewControllerDelegate: class {
    func fileNewFromURLViewController(didSelects repositories: FileEntities)
}

class FileNewFromURLViewController: BaseViewController {
    @IBOutlet weak var urlField: HoshiTextField! {
        didSet {
            self.urlField.font = Font.FT(size: .FL, family: .L)
            self.urlField.textColor = Theme.themify(key: .string)
            self.urlField.placeholderColor = Theme.themify(key: .placeholder)
            self.urlField.placeholder = R.string.localizable.inputYouTubeURL()
            self.urlField.placeholderLabel.font = Font.FT(size: .M, family: .L)
            self.urlField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.urlField.borderActiveColor = Theme.themify(key: .main)
            self.urlField.delegate = self
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = R.string.localizable.uploadTitleFromURL()
            titleLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var mediaPickCollectionViewContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaPickCollectionViewContainer: UIView!
    @IBOutlet weak var submitButton: SimpleButton! {
        didSet {
            self.submitButton.text = R.string.localizable.send()
            self.submitButton.normalColor = Theme.themify(key: .main)
            self.submitButton.canceledColor = Theme.themify(key: .cancel)
            self.submitButton.canceled = true
            self.submitButton.isHidden = true
            self.submitButton.delegate = self
        }
    }
    public var mediaPickCollectionViewController: MediaPickCollectionViewController = MediaPickCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
    var presenter: FileNewFromURLPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: FileEntities = FileEntities()
    fileprivate var players: [String: WKYTPlayerView] = [:]
    fileprivate var pl_resolve: ((_ value: WKYTPlayerView) -> ())?
    fileprivate var pl_reject: ((_ error: Error) -> ())?
    var current_member: MemberEntity = MemberEntity()
    var current_guild: GuildEntity = GuildEntity()
    weak var delegate: FileNewFromURLViewControllerDelegate?
    enum MediaMode: Int {
        case on = 200
        case off = 0
        
        var height: CGFloat {
            return CGFloat(self.rawValue)
        }
    }
    var mode: MediaMode = .off
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationMode = .noneBorder
        setFlatBackground()
        FileNewFromURLViewControllerBuilder.rebuild(self)
        configureMediaPickCollectionViewController()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard(_:)))
        gesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(gesture)
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        self.title = ""
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.urlField.resignFirstResponder()
    }
    
    @objc func closeKeyboard(_ sender: Any) {
        self.urlField.resignFirstResponder()
    }
    
    func initRepository(current_guild: GuildEntity, current_member: MemberEntity) {
        self.current_member = current_member
        self.current_guild = current_guild
    }
    
    func updateLayout() {
        self.mode = repositories.count > 0 ? .on : .off
        self.submitButton.canceled = mode == .off
        if mode == .off {
            self.mediaPickCollectionViewContainer.fadeOut(type: .Normal, completed: nil)
            self.submitButton.fadeOut(type: .Normal, completed: nil)
        } else {
            self.mediaPickCollectionViewContainer.fadeIn(type: .Normal, completed: nil)
            self.submitButton.fadeIn(type: .Normal, completed: nil)
        }
    }
    
    func handleNewText(shouldCreates: Bool = false) {
        guard let text = urlField.text, text.count > 0, repositories.filter({ $0.Provider?.youtubeID == text }).count == 0 else {
            urlField.text = ""
            if shouldCreates && self.delegate == nil {
                self.presenter?.creates(self.repositories)
            } else if shouldCreates && self.delegate != nil {
                self.delegate?.fileNewFromURLViewController(didSelects: self.repositories)
//                self.dismissShouldAppear(animated: true)
            }
            return
        }
        
        let provider = ProviderEntity.init(url: text)
        if provider.type == .youtube {
            HUD.show(.progress)
            self.loadYouTube(provider: provider, callback: {
                self.fetchDuration(player: $0, provider: provider, callback: { time in
                    provider.duration = time * 1000
                    self.fetchYouTubeData(provider: provider, shouldCreates: shouldCreates)
                }, onError: { _ in
                    self.fetchYouTubeData(provider: provider, shouldCreates: shouldCreates)
                })
            })
        }
        urlField.text = ""
    }
    
    func fetchYouTubeData(provider: ProviderEntity, shouldCreates: Bool = false) {
        guard provider.type == .youtube else { return }
        provider.fetchYouTubeData().then(in: .main, { result in
            let file = provider.convertFileEntity()
            file.duration = Double(provider.duration ?? .zero)
            file.Member = self.current_member.toNewMemory()
            file.MemberId = self.current_member.id
            file.provider = .youtube
            file.format = .youtube
            self.repositories.append(file)
            DispatchQueue.main.async { [unowned self] in
                self.mediaPickCollectionViewController.setRepositories(self.repositories)
                self.updateLayout()
                if shouldCreates && self.delegate == nil {
                    self.presenter?.creates(self.repositories)
                } else if shouldCreates && self.delegate != nil {
                    self.delegate?.fileNewFromURLViewController(didSelects: self.repositories)
                    // self.dismissShouldAppear(animated: true)
                }
            }
            HUD.hide()
            HUD.flash(.success)
        }).catch({ error in
            HUD.hide()
            HUD.flash(.error)
        }).cancelled({
            HUD.hide()
            HUD.flash(.error)
        })
    }
}

extension FileNewFromURLViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        handleNewText()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        handleNewText()
    }
}

extension FileNewFromURLViewController: WKYTPlayerViewDelegate {
    fileprivate func loadYouTube(provider: ProviderEntity, callback: @escaping ((_ value: WKYTPlayerView) -> ())) {
        guard provider.type == .youtube, let youtubeID = provider.youtubeID else { return }
        let player = WKYTPlayerView()
        player.delegate = self
        self.players[provider.url] = player
        self.pl_resolve = callback
        player.load(withVideoId: youtubeID)
    }
    
    fileprivate func fetchDuration(player: WKYTPlayerView, provider: ProviderEntity, callback: @escaping ((_ value: TimeInterval) -> ()), onError: @escaping ((_ error: Error) -> ())) {
        player.getDuration({ time, error in
            if let unwrapped_error = error {
                onError(unwrapped_error)
            } else {
                callback(time)
            }
        })
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        self.pl_resolve?(playerView)
    }
}

extension FileNewFromURLViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        handleNewText(shouldCreates: true)
    }
}

extension FileNewFromURLViewController: MediaPickCollectionViewControllerDelegate {
    func configureMediaPickCollectionViewController() {
        mediaPickCollectionViewController.willMove(toParent: self)
        self.addChild(mediaPickCollectionViewController)
        mediaPickCollectionViewController.collectionView.contentInset = UIEdgeInsets(top: 5, left: 44, bottom: 5, right: 44);
        mediaPickCollectionViewController.mediaDelegate = self
        mediaPickCollectionViewController.setRepositories(repositories)
        mediaPickCollectionViewController.view.frame = CGRect(x: 0, y: 0, width: mediaPickCollectionViewContainer.frame.width, height: MediaMode.on.height)
        mediaPickCollectionViewController.setItemSize(CGSize(width: MediaMode.on.height - 10, height: MediaMode.on.height - 10))
        mediaPickCollectionViewContainer.addSubview(mediaPickCollectionViewController.view)
        mediaPickCollectionViewContainer.constrainToEdges(mediaPickCollectionViewController.view)
        mediaPickCollectionViewController.didMove(toParent: self)
        self.mediaPickCollectionViewContainerHeightConstraint.constant = MediaMode.on.height // self.mode.height
        self.mediaPickCollectionViewContainer.isHidden = self.mode == .off
    }
    
    func mediaPickCollectionViewController(sync repositories: FileEntities) {
        self.repositories = repositories
        self.updateLayout()
    }
}

extension FileNewFromURLViewController: FileNewFromURLPresenterOutput {
    func fileNewFromURLPresenter(onError error: Error) {
        DispatchQueue.main.async {
            HUD.hide()
            HUD.flash(.error)
        }
    }
    
    func sync(didFetchRepositories repositories: FileEntities) {
        self.repositories = repositories
        DispatchQueue.main.async {
            self.mode = repositories.count > 0 ? .on : .off
            UIView.animate(withDuration: 0.3, animations: {
                self.mediaPickCollectionViewContainerHeightConstraint.constant = self.mode.height
            })
        }
    }
    
    func syncCurrentMember(didFetchRepository repository: MemberEntity) {
        DispatchQueue.main.async {
            self.current_member = repository
        }
    }
    
    func fileNewFromURLPresenter(shouldCreate repositories: FileEntities) {
        DispatchQueue.main.async {
            self.dismissShouldAppear(animated: true)
        }
    }
    
    func fileNewFromURLPresenter(didCreated repositories: FileEntities) {
        DispatchQueue.main.async {
            self.repositories = repositories
        }
    }
}

