//
//  ChannelRoleEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class ChannelRoleEditViewController: BaseFormViewController {
    var presenter: ChannelRoleEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: ChannelRoleableProtocol = TextChannelRoleEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: ChannelRoleableProtocol = TextChannelRoleEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelRoleEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismissShouldAppear(animated: true)
    }
    
    func handleValueChange() {
        switch repository.channelable_type.superChannel {
        case .TextChannel:
            guard let repository = repository as? TextChannelRoleEntity, let new_repository = new_repository as? TextChannelRoleEntity else { return }
            if repository.roles_managable == new_repository.roles_managable &&
                repository.channel_managable == new_repository.channel_managable &&
                repository.channel_members_managable == new_repository.channel_members_managable &&
                repository.kickable == new_repository.kickable &&
                repository.invitable == new_repository.invitable &&
                repository.channel_viewable == new_repository.channel_viewable &&
                repository.message_sendable == new_repository.message_sendable &&
                repository.messages_managable == new_repository.messages_managable &&
                repository.message_embeddable == new_repository.message_embeddable &&
                repository.message_attachable == new_repository.message_attachable &&
                repository.messages_readable == new_repository.messages_readable &&
                repository.message_mentionable == new_repository.message_mentionable &&
                repository.message_reactionable == new_repository.message_reactionable {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        case .DMChannel:
            guard let repository = repository as? DMChannelRoleEntity, let new_repository = new_repository as? DMChannelRoleEntity else { return }
            if repository.roles_managable == new_repository.roles_managable &&
                repository.channel_managable == new_repository.channel_managable &&
                repository.channel_members_managable == new_repository.channel_members_managable &&
                repository.kickable == new_repository.kickable &&
                repository.invitable == new_repository.invitable &&
                repository.channel_viewable == new_repository.channel_viewable &&
                repository.message_sendable == new_repository.message_sendable &&
                repository.messages_managable == new_repository.messages_managable &&
                repository.message_embeddable == new_repository.message_embeddable &&
                repository.message_attachable == new_repository.message_attachable &&
                repository.messages_readable == new_repository.messages_readable &&
                repository.message_mentionable == new_repository.message_mentionable &&
                repository.message_reactionable == new_repository.message_reactionable {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        case .StreamChannel:
            guard let repository = repository as? StreamChannelRoleEntity, let new_repository = new_repository as? StreamChannelRoleEntity else { return }
            if repository.roles_managable == new_repository.roles_managable &&
                repository.channel_managable == new_repository.channel_managable &&
                repository.channel_members_managable == new_repository.channel_members_managable &&
                repository.kickable == new_repository.kickable &&
                repository.invitable == new_repository.invitable &&
                repository.channel_viewable == new_repository.channel_viewable &&
                repository.message_sendable == new_repository.message_sendable &&
                repository.messages_managable == new_repository.messages_managable &&
                repository.message_embeddable == new_repository.message_embeddable &&
                repository.message_attachable == new_repository.message_attachable &&
                repository.messages_readable == new_repository.messages_readable &&
                repository.message_mentionable == new_repository.message_mentionable &&
                repository.message_reactionable == new_repository.message_reactionable &&
                repository.stream_connectable == new_repository.stream_connectable &&
                repository.stream_speekable == new_repository.stream_speekable &&
                repository.stream_livestreamable == new_repository.stream_livestreamable &&
                repository.movie_selectable == new_repository.movie_selectable &&
                repository.stream_mutable == new_repository.stream_mutable &&
                repository.stream_deafenable == new_repository.stream_deafenable &&
                repository.stream_movable == new_repository.stream_movable {
                self.resetRightNavigationBarButton()
            } else {
                self.setupSaveNavigationBarButton()
            }
        default: break
        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        switch repository.channelable_type.superChannel {
        case .TextChannel: configureTextChannelRoleForm(repository as! TextChannelRoleEntity, new_repository: new_repository as! TextChannelRoleEntity)
        case .DMChannel: configureDMChannelRoleForm(repository as! DMChannelRoleEntity, new_repository: new_repository as! DMChannelRoleEntity)
        case .StreamChannel: configureStreamChannelRoleForm(repository as! StreamChannelRoleEntity, new_repository: new_repository as! StreamChannelRoleEntity)
        default: break
        }
        
        form
            +++ Section()
            <<< ButtonRow(R.string.localizable.guildMenuDelete()) {
                $0.title = $0.tag
                $0.onCellHighlightChanged({ _, _ in })
            }.cellUpdate { [unowned self] in
                self.buttonRowCellUpdate($0, $1)
                $0.textLabel?.textColor = Theme.themify(key: .danger)
                $0.textLabel?.font = Font.FT(size: .FM, family: .B)
            }.onCellSelection { [unowned self] cell, row in
                let alert: UIAlertController = UIAlertController(title: R.string.localizable.roleDelete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                            
                let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.roleDelete(), style: UIAlertAction.Style.default, handler:{
                    [unowned self] (action: UIAlertAction!) -> Void in
                    HUD.show(.progress)
                    switch self.repository.channelable_type.superChannel {
                    case .TextChannel: self.presenter?.delete(repository: (self.repository as! TextChannelRoleEntity).Role ?? RoleEntity(), channel: (self.repository as! TextChannelRoleEntity).Channel ?? TextChannelEntity())
                    case .DMChannel: self.presenter?.delete(repository: (self.repository as! DMChannelRoleEntity).Role ?? RoleEntity(), channel: (self.repository as! DMChannelRoleEntity).Channel ?? DMChannelEntity())
                    case .StreamChannel: self.presenter?.delete(repository: (self.repository as! StreamChannelRoleEntity).Role ?? RoleEntity(), channel: (self.repository as! StreamChannelRoleEntity).Channel ?? StreamChannelEntity())
                    default: break
                    }
                })
                let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                    (action: UIAlertAction!) -> Void in
                })
                alert.addAction(cancelAction)
                alert.addAction(defaultAction)
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                self.present(alert, animated: true, completion: nil)
            }
    }
    
    fileprivate func configureTextChannelRoleForm(_ repository: TextChannelRoleEntity, new_repository: TextChannelRoleEntity) {
        form
            +++ Section(header: "", footer: R.string.localizable.roleRoleViewableDesc())
            <<< SwitchRow(R.string.localizable.roleRoleViewable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_viewable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_viewable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleRoleManageableDesc()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleRoleManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.roles_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.roles_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleChannelManageable()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleChannelManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleChannelMembersManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_members_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_members_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section() {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleInvitable()) {
                $0.title = $0.tag
                $0.value = new_repository.invitable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.invitable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleKickable()) {
                $0.title = $0.tag
                $0.value = new_repository.kickable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.kickable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
            +++ Section(R.string.localizable.roleMessageManageableDesc()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleMessageManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.messages_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section() {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleMessageSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_sendable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_sendable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReadable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_readable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.messages_readable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageAttachable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_attachable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_attachable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReactionSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_reactionable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_reactionable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
    }
    
    fileprivate func configureDMChannelRoleForm(_ repository: DMChannelRoleEntity, new_repository: DMChannelRoleEntity) {
        form
            +++ Section(header: "", footer: R.string.localizable.roleRoleViewableDesc())
            <<< SwitchRow(R.string.localizable.roleRoleViewable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_viewable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_viewable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            +++ Section(header: "", footer: R.string.localizable.roleRoleManageableDesc()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleRoleManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.roles_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.roles_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleChannelManageable()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleChannelManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleChannelMembersManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_members_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_members_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section() {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleInvitable()) {
                $0.title = $0.tag
                $0.value = new_repository.invitable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.invitable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleKickable()) {
                $0.title = $0.tag
                $0.value = new_repository.kickable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.kickable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
            +++ Section(R.string.localizable.roleMessageManageableDesc()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleMessageManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.messages_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section() {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleMessageSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_sendable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_sendable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReadable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_readable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.messages_readable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageAttachable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_attachable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_attachable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReactionSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_reactionable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_reactionable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
    }
    
    fileprivate func configureStreamChannelRoleForm(_ repository: StreamChannelRoleEntity, new_repository: StreamChannelRoleEntity) {
        form
            +++ Section(header: "", footer: R.string.localizable.roleRoleViewableDesc())
            <<< SwitchRow(R.string.localizable.roleRoleViewable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_viewable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_viewable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            +++ Section(header: "", footer: R.string.localizable.roleRoleManageableDesc()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleRoleManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.roles_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.roles_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section(header: "", footer: R.string.localizable.roleChannelManageable()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleChannelManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleChannelMembersManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.channel_members_managable
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.channel_members_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section() {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleInvitable()) {
                $0.title = $0.tag
                $0.value = new_repository.invitable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.invitable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleKickable()) {
                $0.title = $0.tag
                $0.value = new_repository.kickable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.kickable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
            +++ Section(R.string.localizable.roleMessageManageableDesc()) {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleMessageManageable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_managable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.messages_managable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            +++ Section() {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleMessageSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_sendable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_sendable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReadable()) {
                $0.title = $0.tag
                $0.value = new_repository.messages_readable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.messages_readable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageAttachable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_attachable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_attachable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            
            <<< SwitchRow(R.string.localizable.roleMessageReactionSendable()) {
                $0.title = $0.tag
                $0.value = new_repository.message_reactionable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.message_reactionable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
        
            +++ Section() {
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }
            <<< SwitchRow(R.string.localizable.roleStreamConnctable()) {
                $0.title = $0.tag
                $0.value = new_repository.stream_connectable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.stream_connectable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< SwitchRow(R.string.localizable.roleStreamSpeekable()) {
                $0.title = $0.tag
                $0.value = new_repository.stream_speekable
                $0.hidden = .function([R.string.localizable.roleRoleViewable()], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: R.string.localizable.roleRoleViewable())
                    return !(row.value ?? false == true)
                })
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                new_repository.stream_speekable = value
                self.handleValueChange()
            }.cellUpdate(switchRowCellUpdate(_:_:))
    }
}

extension ChannelRoleEditViewController: ChannelRoleEditPresenterOutput {
    func channelRoleEditPresenter(didCreated repository: ChannelRoleableProtocol, in channelRoleEditPresenter: ChannelRoleEditPresenter) {
    }
    
    func channelRoleEditPresenter(didUpdated repository: ChannelRoleableProtocol, in channelRoleEditPresenter: ChannelRoleEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func channelRoleEditPresenter(didDeleted channelRoleEditPresenter: ChannelRoleEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func sync(didFetchRepository repository: ChannelRoleableProtocol) {
        self.repository = repository
    }
    
    func channelRoleEditPresenter(onError error: Error, in channelRoleEditPresenter: ChannelRoleEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
