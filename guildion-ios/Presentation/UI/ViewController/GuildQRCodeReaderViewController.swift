//
//  QRCodeReaderViewController.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/15.
//

import Foundation
import UIKit
import QRCodeReader
import EFQRCode
import AVFoundation

class GuildQRCodeReaderViewController: BaseViewController {
    @IBOutlet weak var readerView: QRCodeReaderView! {
        didSet {
            readerView.setupComponents(with: QRCodeReaderViewControllerBuilder {
                $0.reader                 = reader
                $0.showTorchButton        = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton       = false
                $0.showOverlayView        = true
                $0.rectOfInterest         = CGRect(x: 0.25, y: 0.3, width: 0.5, height: 0.4)
            })
        }
    }
    @IBOutlet weak var libraryImageView: UIImageView! {
        didSet {
            libraryImageView.layer.cornerRadius = 5
            libraryImageView.layer.borderWidth = 1.5
            libraryImageView.layer.borderColor = UIColor.white.cgColor
            libraryImageView.clipsToBounds = true
            libraryImageView.isUserInteractionEnabled = true
            libraryImageView.image = R.image.insertPicture()?.withRenderingMode(.alwaysTemplate)
            libraryImageView.tintColor = .white
            libraryImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapLibraryImage(_:))))
        }
    }
    @IBOutlet weak var footerView: UIView! {
        didSet {
            footerView.backgroundColor = Theme.themify(key: .backgroundThick)
        }
    }
    @IBOutlet weak var footerLabel: UILabel! {
        didSet {
            footerLabel.font = Font.FT(size: .M, family: .L)
            footerLabel.text = R.string.localizable.qrReaderGuildTitle()
            footerLabel.textColor = Theme.themify(key: .stringSecondary)
            footerLabel.textAlignment = .center
        }
    }
    lazy var reader: QRCodeReader = QRCodeReader()
    var presenter: GuildQRCodeReaderPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: GuildEntity = GuildEntity()
    lazy var imagePickerHandler: ImagePickerHandler = {
        let handler = ImagePickerHandler()
        handler.delegate = self
        return handler
    }()
    
    override func viewDidLoad() {
        self.navigationMode = .modal
        super.viewDidLoad()
        GuildQRCodeReaderViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
        configureQRReader()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
        reader.stopScanning()
    }
    
    fileprivate func configureQRReader() {
        guard !reader.isRunning else { return }
        reader.didFindCode = { result in
            if let url = URL(string: result.value), UniversalLinkHandler.execSegue(url: url) {
            } else {
                self.presentAlertInValidQR()
            }
        }
        reader.startScanning()
        self.view.bringSubviewToFront(libraryImageView)
    }
    
    fileprivate func configureLibraryImage() {
    }
    
    @objc func onTapLibraryImage(_ sender: Any) {
        self.imagePickerHandler.openImagePicker(allowsEditing: false)
    }
    
    func presentAlertInValidQR() {
        let alert: UIAlertController = UIAlertController(title: R.string.localizable.qrReaderGuildAlertTitle(), message: nil, preferredStyle:  UIAlertController.Style.alert)
                    
        let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
            self.configureQRReader()
        })
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
}

extension GuildQRCodeReaderViewController: ImagePickerHandlerDelegate {
    var viewController: UIViewController { self }
    
    func updateAssets(_ image: UIImage, info: [UIImagePickerController.InfoKey : Any]) {
        if let cgImage = image.cgImage, let codes = EFQRCode.recognize(image: cgImage) {
            if let code = codes.first, let url = URL(string: code), UniversalLinkHandler.execSegue(url: url) {
            } else {
                self.presentAlertInValidQR()
            }
        }
    }
    
    func cancelAssets() {
    }
}

extension GuildQRCodeReaderViewController: GuildQRCodeReaderPresenterOutput {
}
