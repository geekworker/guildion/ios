//
//  GuildSettingViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class GuildSettingViewController: BaseFormViewController {
    var presenter: GuildSettingPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: GuildEntity = GuildEntity()
    fileprivate var selectedGuild: GuildEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildSettingViewControllerBuilder.rebuild(self)
        self.presenter?.repositories = repository
        self.presenter?.fetch(repository)
        title = R.string.localizable.guildSetting()
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
        guard navigationController?.viewControllers.count == 1 else { return }
        let exit = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(onTapNavigationCloseButton(_:)))
        navigationItem.leftBarButtonItems = [exit]
    }
    
    @objc func onTapNavigationCloseButton(_ sender: Any) {
        self.dismissShouldAppear(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildEditViewController, segue.identifier == R.segue.guildSettingViewController.toGuildEdit.identifier {
            vc.repository = repository
        } else if let vc = segue.destination as? GuildPublicEditViewController, segue.identifier == R.segue.guildSettingViewController.toGuildPublicEdit.identifier {
            vc.repository = repository
        } else if let vc = segue.destination as? GuildNotificationEditViewController, segue.identifier == R.segue.guildSettingViewController.toGuildNotificationEdit.identifier, let member = GuildConnector.shared.current_member {
            vc.repository = member
        } else if let vc = segue.destination as? GuildChannelsEditViewController {
            vc.repository = repository
        } else if let vc = segue.destination as? GuildMembersEditViewController {
            vc.repository = repository
        } else if let vc = segue.destination as? GuildRolesEditViewController {
            vc.repository = repository
        }
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        form +++
            Section() { [unowned self] in
                var header = HeaderFooterView<GuildSettingHeaderView>(.class)
                header.height = { GuildSettingHeaderView.viewHeight }
                header.onSetupView = { [unowned self] (view, section) -> () in
                    view.setRepository(self.repository)
                }
                $0.header = header
            }
            let menuSection = Section(R.string.localizable.guildMenu())
        if GuildConnector.shared.current_permission.Guild.guild_managable {
            menuSection
                <<< ButtonRow(R.string.localizable.guildMenuGuildEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.guildSettingViewController.toGuildEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                <<< ButtonRow(R.string.localizable.guildMenuPublicEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.guildSettingViewController.toGuildPublicEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                <<< PushRow<String>(R.string.localizable.guildMenuLang()) {
                    $0.title = $0.tag
                    $0.options = LocaleConfig.Locales.languadges
                    $0.value = LocaleConfig.Locales.allCases.filter({ $0.rawValue == repository.locale }).first?.languadge ?? LocaleConfig.devise_default_locale.languadge
                }.onPresent({ (_, vc) in
                    vc.enableDeselection = false
                    vc.dismissOnSelection = false
                }).cellUpdate { [unowned self] in
                    self.pushRowCellUpdate($0, $1)
                }.onChange { [unowned self] in
                    guard let value = $0.value, let repository = LocaleConfig.Locales.allCases.filter({ $0.languadge == value }).first else { return }
                    self.repository.locale = repository.rawValue
                    self.presenter?.update(repository: self.repository)
                }
        }
        
        if GuildConnector.shared.current_permission.Guild.channels_managable {
            menuSection
                <<< ButtonRow(R.string.localizable.guildMenuChannelEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.guildSettingViewController.toGuildChannelsEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                <<< ButtonRow(R.string.localizable.sectionChannelsEdit()) {
                    $0.title = $0.tag
                }
                .cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                .onCellSelection { cell, row in self.presenter?.toSectionChannelsEdit() }
        }
        
        if GuildConnector.shared.current_permission.Guild.folders_managable {
            menuSection
                <<< ButtonRow(R.string.localizable.guildMenuPlaylistsEdit()) {
                    $0.title = $0.tag
                }
                .cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
                .onCellSelection { cell, row in self.presenter?.toPlaylistsEdit() }
        }
        
        menuSection
            <<< ButtonRow(R.string.localizable.guildMenuNotificationEdit()) {
                $0.title = $0.tag
                $0.presentationMode = .segueName(segueName: R.segue.guildSettingViewController.toGuildNotificationEdit.identifier, onDismiss: nil)
            }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
        
        form
            +++ menuSection

        let memberSection = Section(R.string.localizable.guildMenuMember())
            
        if GuildConnector.shared.current_permission.Guild.members_managable {
            memberSection
                <<< ButtonRow(R.string.localizable.guildMenuMemberEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.guildSettingViewController.toGuildMembersEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
        }
        
        if GuildConnector.shared.current_permission.Guild.roles_managable {
            memberSection
                <<< ButtonRow(R.string.localizable.guildMenuRoleEdit()) {
                    $0.title = $0.tag
                    $0.presentationMode = .segueName(segueName: R.segue.guildSettingViewController.toGuildRolesEdit.identifier, onDismiss: nil)
                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
        }
            
        memberSection
            <<< ButtonRow(R.string.localizable.guildMenuInviteEdit()) {
                $0.title = $0.tag
            }
            .onCellSelection { [unowned self] _, _ in self.presenter?.toGuildShare() }
            .cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
            
//        if GuildConnector.shared.current_permission.Guild.banable {
//            memberSection
//                <<< ButtonRow(R.string.localizable.guildMenuBlock()) {
//                    $0.title = $0.tag
//                    $0.presentationMode = .segueName(segueName: R.segue.guildSettingViewController.toGuildBlocks.identifier, onDismiss: nil)
//                }.cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
//        }
        
        form
            +++ memberSection
        
        if repository.OwnerId != CurrentUser.getCurrentUserEntity()?.id {
            form
                +++ Section("")
                <<< ButtonRow(R.string.localizable.userReport()) {
                    $0.title = $0.tag
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .warning)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    self.shouldReportGuild(repository.toNewMemory())
                }
        }
            
        if repository.OwnerId == CurrentUser.getCurrentUserEntity()?.id {
            form
                +++ Section("")
                <<< ButtonRow(R.string.localizable.guildMenuDelete()) {
                    $0.title = $0.tag
                    $0.onCellHighlightChanged({ _, _ in })
                }.cellUpdate { [unowned self] in
                    self.buttonRowCellUpdate($0, $1)
                    $0.textLabel?.textColor = Theme.themify(key: .danger)
                    $0.textLabel?.font = Font.FT(size: .FM, family: .B)
                }.onCellSelection { [unowned self] cell, row in
                    let alert: UIAlertController = UIAlertController(title: R.string.localizable.guildMenuDelete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                                
                    let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.guildMenuDelete(), style: UIAlertAction.Style.default, handler:{
                        [unowned self](action: UIAlertAction!) -> Void in
                        HUD.show(.progress)
                        self.presenter?.delete(repository: self.repository.toNewMemory())
                    })
                    let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                        (action: UIAlertAction!) -> Void in
                    })
                    alert.addAction(cancelAction)
                    alert.addAction(defaultAction)
                    alert.popoverPresentationController?.sourceView = self.view
                    alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    private func shouldReportGuild(_ repository: GuildEntity) {
        let alert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle:  UIAlertController.Style.actionSheet)
        let action1: UIAlertAction = UIAlertAction(title: R.string.localizable.reportSex(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(guild: repository, description: R.string.localizable.reportSex())
        })
        let action2: UIAlertAction = UIAlertAction(title: R.string.localizable.reportViolate(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(guild: repository, description: R.string.localizable.reportViolate())
        })
        let action3: UIAlertAction = UIAlertAction(title: R.string.localizable.reportRight(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(guild: repository, description: R.string.localizable.reportRight())
        })
        let action4: UIAlertAction = UIAlertAction(title: R.string.localizable.reportInvalid(), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) -> Void in
            self.presenter?.report(guild: repository, description: R.string.localizable.reportInvalid())
        })
        let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler: { (action: UIAlertAction!) -> Void in
        })
        DispatchQueue.main.async {
            alert.addAction(action1)
            alert.addAction(action2)
            alert.addAction(action3)
            alert.addAction(action4)
            alert.addAction(cancelAction)
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension GuildSettingViewController: GuildSettingPresenterOutput {
    func guildSettingPresenter(afterDeleted repository: GuildEntity, in guildSettingPresenter: GuildSettingPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    func guildSettingPresenter(afterUpdated repository: GuildEntity, in guildSettingPresenter: GuildSettingPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.presenter?.fetch(repository)
    }
    
    func guildSettingPresenter(didSuccess guildSettingPresenter: GuildSettingPresenter) {
        HUD.hide()
        HUD.flash(.success)
    }
    
    func guildSettingPresenter(didJoin repository: GuildEntity, in guildSettingPresenter: GuildSettingPresenter) {
        self.reloadForm()
    }
    
    func guildSettingPresenter(onError error: Error, in guildSettingPresenter: GuildSettingPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
        self.reloadForm()
    }
}
