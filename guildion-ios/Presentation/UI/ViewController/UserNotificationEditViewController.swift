//
//  UserNotificationEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class UserNotificationEditViewController: BaseFormViewController {
    var presenter: UserNotificationEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: UserEntity = UserEntity() {
        didSet {
            new_repository.nickname = repository.nickname
            new_repository.description = repository.description
            new_repository.picture_small = repository.picture_small
            new_repository.picture_large = repository.picture_large
            new_repository.id = repository.id
            new_repository.username = repository.username
        }
    }
    var new_repository: UserEntity = UserEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        UserNotificationEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func handleValueChange() {
        self.setupSaveNavigationBarButton()
//        if  {
//            self.resetRightNavigationBarButton()
//        } else {
//            self.setupSaveNavigationBarButton()
//        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section()
//            <<< SwitchRow(R.string.localizable.userInternalNotificationEnabled()) {
//                $0.title = $0.tag
//                $0.value = true
//            }.onChange { _ in
//                // guard let value = $0.value else { return }
//                self.handleValueChange()
//            }.cellUpdate(switchRowCellUpdate(_:_:))
            <<< ButtonRow(R.string.localizable.userNotificationEnabled()) {
                $0.title = $0.tag
            }.onCellSelection { _, _ in
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
            .cellUpdate { [unowned self] in self.buttonRowLeftAlignmentUpdate($0, $1) }
    }
}

extension UserNotificationEditViewController: UserNotificationEditPresenterOutput {
    func sync(didFetchRepository repository: UserEntity) {
        self.repository = repository
    }
}
