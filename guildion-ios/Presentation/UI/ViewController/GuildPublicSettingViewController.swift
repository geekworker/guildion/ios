//
//  GuildPublicSettingViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/12.
//

import Foundation
import UIKit
import PKHUD

class GuildPublicSettingViewController: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Theme.themify(key: .string)
            titleLabel.text = R.string.localizable.guildPublicTitle()
        }
    }
    @IBOutlet weak var publicSettingView: UIView! {
        didSet {
            publicSettingView.backgroundColor = Theme.themify(key: .backgroundLight)
            publicSettingView.layer.cornerRadius = 5
            publicSettingView.clipsToBounds = true
            publicSettingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectPublicSetting(_:))))
        }
    }
    @IBOutlet weak var publicTitleLabel: UILabel! {
        didSet {
            publicTitleLabel.textColor = Theme.themify(key: .string)
            publicTitleLabel.text = R.string.localizable.public()
        }
    }
    @IBOutlet weak var publicDetailLabel: UILabel! {
        didSet {
            publicDetailLabel.textColor = Theme.themify(key: .string)
            publicDetailLabel.text = R.string.localizable.guildPublicSettingTitle()
        }
    }
    @IBOutlet weak var publicImage: UIImageView! {
        didSet {
            publicImage.layer.cornerRadius = publicImage.frame.width / 2
            publicImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var publicChevron: UIImageView! {
        didSet {
            publicChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            publicChevron.tintColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var requestSettingView: UIView! {
        didSet {
            requestSettingView.backgroundColor = Theme.themify(key: .backgroundLight)
            requestSettingView.layer.cornerRadius = 5
            requestSettingView.clipsToBounds = true
            requestSettingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectRequestSetting(_:))))
        }
    }
    @IBOutlet weak var requestTitleLabel: UILabel! {
        didSet {
            requestTitleLabel.textColor = Theme.themify(key: .string)
            requestTitleLabel.text = R.string.localizable.approvalSystem()
        }
    }
    @IBOutlet weak var requestDetailLabel: UILabel! {
        didSet {
            requestDetailLabel.textColor = Theme.themify(key: .string)
            requestDetailLabel.text = R.string.localizable.guildApprovalSettingTitle()
        }
    }
    @IBOutlet weak var requestImage: UIImageView! {
        didSet {
            requestImage.layer.cornerRadius = requestImage.frame.width / 2
            requestImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var requestChevron: UIImageView! {
        didSet {
            requestChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            requestChevron.tintColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var privateSettingView: UIView! {
        didSet {
            privateSettingView.backgroundColor = Theme.themify(key: .backgroundLight)
            privateSettingView.layer.cornerRadius = 5
            privateSettingView.clipsToBounds = true
            privateSettingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectPrivateSetting(_:))))
        }
    }
    @IBOutlet weak var privateTitleLabel: UILabel! {
        didSet {
            privateTitleLabel.textColor = Theme.themify(key: .string)
            privateTitleLabel.text = R.string.localizable.private()
        }
    }
    @IBOutlet weak var privateDetailLabel: UILabel! {
        didSet {
            privateDetailLabel.textColor = Theme.themify(key: .string)
            privateDetailLabel.text = R.string.localizable.guildPrivateSettingTitle()
        }
    }
    @IBOutlet weak var privateImage: UIImageView! {
        didSet {
            privateImage.layer.cornerRadius = privateImage.frame.width / 2
            privateImage.clipsToBounds = true
        }
    }
    @IBOutlet weak var privateChevron: UIImageView! {
        didSet {
            privateChevron.image = R.image.chevron()!.withRenderingMode(.alwaysTemplate)
            privateChevron.tintColor = Theme.themify(key: .border)
        }
    }
    
    var presenter: GuildPublicSettingPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public var repository: GuildEntity = GuildEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GuildPublicSettingViewControllerBuilder.rebuild(self)
        self.navigationMode = .modal
        self.setFlatBackground()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GuildOpenSettingViewController, segue.identifier == R.segue.guildPublicSettingViewController.toOpenSetting.identifier {
            vc.repository = self.repository
        }
    }
    
    @objc func onSelectPublicSetting(_ sender: Any) {
        repository.is_private = false
        repository.is_public = true
//        self.presenter?.toOpenSetting()
        HUD.show(.progress)
        self.presenter?.create(self.repository)
    }
    
    @objc func onSelectRequestSetting(_ sender: Any) {
        repository.is_private = false
        repository.is_public = false
//        self.presenter?.toOpenSetting()
        HUD.show(.progress)
        self.presenter?.create(self.repository)
    }
    
    @objc func onSelectPrivateSetting(_ sender: Any) {
        repository.is_private = true
        repository.is_public = false
        HUD.show(.progress)
        self.presenter?.create(self.repository)

    }
}

extension GuildPublicSettingViewController: GuildPublicSettingPresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
    }
    
    func guildPublicSettingPresenter(onError error: Error) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func guildPublicSettingPresenter(didCreated repository: GuildEntity) {
        HUD.hide()
        HUD.flash(.success)
        self.dismissShouldAppear(animated: true)
    }
}
