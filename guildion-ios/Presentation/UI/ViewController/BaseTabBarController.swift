//
//  BaseTabBarController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/07.
//

import UIKit
import ReSwift
import SPPermissions
import Nuke
import RAMAnimatedTabBarController
import M13ProgressSuite

protocol BaseTabBarControllerEditDelegate: class {
    func baseTabBarController(shouldTrash baseTabBarController: BaseTabBarController)
    func baseTabBarController(shouldShare baseTabBarController: BaseTabBarController)
}

class BaseTabBarController: RAMAnimatedTabBarController {
    public weak var editDelegate: BaseTabBarControllerEditDelegate?
    public var editModeView: EditModeTabBar = EditModeTabBar()
    enum Tabs: Int {
        case home
        case explore
        case mypage
        
        var image: UIImage {
            switch self {
            case .home: return (
                UIImage(named: R.image.logoIcon.name)?
                    .withRenderingMode(.alwaysTemplate)
                    .resizedImage(newSize: CGSize(width: 22, height: 22))
                )!
            case .explore: return (
                UIImage(named: R.image.search.name)?
                    .withRenderingMode(.alwaysTemplate)
                    .resizedImage(newSize: CGSize(width: 22, height: 22))
                )!
            case .mypage: return (
                UIImage(named: R.image.person.name)?
                    .withRenderingMode(.alwaysTemplate)
                    .resizedImage(newSize: CGSize(width: 22, height: 22))
                )!
            }
        }
        
        var string: String {
            switch self {
            case .home: return R.string.localizable.home()
            case .explore: return R.string.localizable.explore()
            case .mypage: return R.string.localizable.mypage()
            }
        }
        
        var label: UILabel {
            let l = UILabel()
            l.text = self.string
            return l
        }
        
        var tabbarItem: RAMAnimatedTabBarItem {
            let item = RAMAnimatedTabBarItem(
                title: self.string,
                image: self.image,
                tag: self.rawValue
            )
            item.iconColor = Theme.themify(key: .border)
            item.animation = self.animation
            item.animation.duration = self.animationDuration
            item.animation.textSelectedColor = Theme.themify(key: .string)
            item.animation.iconSelectedColor = Theme.themify(key: .string)
            return item
        }
        
        var animation: RAMItemAnimation {
            switch self {
            case .home: return RAMBounceAnimation()
            case .explore: return RAMBounceAnimation()
            case .mypage: return RAMBounceAnimation()
            }
        }
        
        var animationDuration: CGFloat {
            switch self {
            case .home: return 0.5
            case .explore: return 0.5
            case .mypage: return 0.5
            }
        }
    }
    
    override func viewDidLoad() {
        self.tabBar.isTranslucent = false
        self.tabBar.barTintColor = Theme.themify(key: .backgroundThick)
        self.tabBar.tintColor = Theme.themify(key: .transparent)
        UITabBar.appearance().barTintColor = Theme.themify(key: .backgroundThick)
        UITabBar.appearance().tintColor = Theme.themify(key: .transparent)
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: Theme.themify(key: .transparent)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: Theme.themify(key: .transparent)], for: .normal)
        setupTabItem()
        self.customizableViewControllers = nil
        self.extendedLayoutIncludesOpaqueBars = true
        delegate = self
        // MARK: should last call and not remove
        super.viewDidLoad()
        self.configureEditMode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBar.barTintColor = Theme.themify(key: .backgroundThick)
        self.tabBar.tintColor = Theme.themify(key: .string)
        UITabBar.appearance().barTintColor = Theme.themify(key: .backgroundThick)
        UITabBar.appearance().tintColor = Theme.themify(key: .string)
        _ = self.viewControllers?.compactMap({ $0.viewWillAppear(animated) })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBar.barTintColor = Theme.themify(key: .backgroundThick)
        self.tabBar.tintColor = Theme.themify(key: .string)
        UITabBar.appearance().barTintColor = Theme.themify(key: .backgroundThick)
        UITabBar.appearance().tintColor = Theme.themify(key: .string)
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: Theme.themify(key: .transparent)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: Theme.themify(key: .transparent)], for: .normal)
        _ = self.viewControllers?.compactMap({ $0.viewDidAppear(animated) })
        self.handleUniversalLink()
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        super.present(viewControllerToPresent, animated: flag, completion: {
            UIApplication.firstWindow.bringSubviewToFront(MiniScreenView.getInstance())
            completion?()
        })
    }
    
    fileprivate func setupTabItem() {
        guard let viewControllers = self.viewControllers else {
            fatalError("couldn't set up TabViewController")
        }
        for viewController in viewControllers.enumerated() {
            viewController.element.extendedLayoutIncludesOpaqueBars = true
            viewController.element.tabBarItem = Tabs(rawValue: viewController.offset)?.tabbarItem
            viewController.element.tabBarItem.selectedImage = Tabs(rawValue: viewController.offset)?.image
            viewController.element.view.backgroundColor = Theme.themify(key: .border)
        }
    }
    
    fileprivate func handleUniversalLink() {
        if let url = AppConfig.should_open_universal_url {
            _ = UniversalLinkHandler.execSegue(url: url)
        }
    }
}

extension BaseTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
}

extension BaseTabBarController: EditModeTabBarDelegate {
    
    fileprivate func configureEditMode() {
        self.switchEditMode(false)
        self.editModeView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.tabBar.frame.height)
        self.tabBar.addSubview(self.editModeView)
        self.editModeView.delegate = self
    }
    
    public func setEditCount(_ count: Int) {
        self.editModeView.setCount(count)
    }
    
    public func switchEditMode(_ show: Bool) {
        self.editModeView.switchMode(show)
        self.tabBar.bringSubviewToFront(self.editModeView)
    }
    
    func editModeTabBar(shouldTrash editModeTabBar: EditModeTabBar) {
        self.editDelegate?.baseTabBarController(shouldTrash: self)
    }
    
    func editModeTabBar(shouldShare editModeTabBar: EditModeTabBar) {
        self.editDelegate?.baseTabBarController(shouldShare: self)
    }
}

extension UITabBarController {
    open override var shouldAutorotate: Bool {
        if let vc = selectedViewController {
            return vc.shouldAutorotate
        } else {
            return true
        }
    }

    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if let vc = selectedViewController {
            return vc.supportedInterfaceOrientations
        } else {
            return .allButUpsideDown
        }
    }
}

extension UINavigationController {

    open override var shouldAutorotate: Bool {
        if let vc = viewControllers.last {
            return vc.shouldAutorotate
        } else {
            return true
        }
    }

    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if let vc = viewControllers.last {
            return vc.supportedInterfaceOrientations
        } else {
            return .allButUpsideDown
        }
    }

}

extension UIViewController {
    var guildsVC: GuildsViewController? {
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? BaseTabBarController, let mainNavigationViewController = rootViewController.viewControllers?[0] as? UINavigationController, let
            topViewController = mainNavigationViewController.topViewController as? GuildsViewController else { return nil }
        return topViewController
    }
}
