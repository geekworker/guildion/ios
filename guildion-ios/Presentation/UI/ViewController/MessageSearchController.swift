//
//  MessageSearchController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class MessageSearchViewController: BaseViewController {
    @IBOutlet weak var searchContainerView: UIView! {
        didSet {
            searchContainerView.backgroundColor = Theme.themify(key: .background)
        }
    }
    @IBOutlet weak var searchBar: SearchBar! {
        didSet {
            searchBar.placeholder = R.string.localizable.searchGuildPlaceholder()
            searchBar.buttonText = R.string.localizable.cancel()
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var searchCompleteViewContainer: UIView!
    
    var presenter: MessageSearchPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: MessageEntities = MessageEntities()

    override func viewDidLoad() {
        super.viewDidLoad()
        MessageSearchViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension MessageSearchViewController: SearchBarDelegate {
}

extension MessageSearchViewController: MessageSearchPresenterOutput {
    func sync(didFetchRepositories repositories: MessageEntities) {
        self.repositories = repositories
    }
}
