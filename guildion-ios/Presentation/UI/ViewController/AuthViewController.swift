//
//  AuthViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit
import TextFieldEffects
import PKHUD
import SwiftMessages

class AuthViewController: BaseViewController {
    @IBOutlet weak var passwordField: HoshiTextField! {
        didSet {
            self.passwordField.font = Font.FT(size: .FL, family: .L)
            self.passwordField.textColor = Theme.themify(key: .string)
            self.passwordField.placeholderColor = Theme.themify(key: .placeholder)
            self.passwordField.placeholder = R.string.localizable.password()
            self.passwordField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.passwordField.borderActiveColor = Theme.themify(key: .main)
            self.passwordField.delegate = self
            self.passwordField.isSecureTextEntry = true
            self.passwordField.textContentType = .password
        }
    }
    @IBOutlet weak var emailField: HoshiTextField! {
        didSet {
            self.emailField.font = Font.FT(size: .FL, family: .L)
            self.emailField.textColor = Theme.themify(key: .string)
            self.emailField.placeholderColor = Theme.themify(key: .placeholder)
            self.emailField.placeholder = R.string.localizable.email()
            self.emailField.borderInactiveColor = Theme.themify(key: .backgroundContrast)
            self.emailField.borderActiveColor = Theme.themify(key: .main)
            self.emailField.delegate = self
            self.emailField.textContentType = .username
        }
    }
    @IBOutlet weak var loginButton: SimpleButton! {
        didSet {
            self.loginButton.text = R.string.localizable.login()
            self.loginButton.normalColor = Theme.themify(key: .main)
            self.loginButton.canceledColor = Theme.themify(key: .cancel)
            self.loginButton.canceled = true
            self.loginButton.delegate = self
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = R.string.localizable.login()
            self.titleLabel.textColor = Theme.themify(key: .string)
        }
    }
    
    var presenter: AuthPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    public var isLogined: Bool = false
    var repository: IdentityEntity = IdentityEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AuthViewControllerBuilder.rebuild(self)
        sessionChecking = false
        self.navigationMode = .noneBorder
        self.setFlatBackground()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        super.viewWillAppear(animated)
        self.setupNavigationBar()
        self.presenter?.bind()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.isLogined = CurrentUser.getCurrentUser() != nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter?.unbind()
    }
    
    func close(_ sender: UITapGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
    
    func login() {
        HUD.show(.progress)
        self.presenter?.login(repository: self.repository)
    }
}

extension AuthViewController: SimpleButtonDelegate {
    func simpleButton(onTap simpleButton: SimpleButton) {
        self.login()
    }
}

extension AuthViewController: UITextFieldDelegate {

    func textFieldDidChangeSelection(_ textField: UITextField) {
        if (textField == self.emailField) {
            self.repository.email = textField.text ?? ""
            guard self.repository.email != "", self.repository.password != "" else {
                self.loginButton.canceled = true
                return
            }
            self.loginButton.canceled = false
        } else if (textField == self.passwordField) {
            self.repository.password = textField.text ?? ""
            guard self.repository.password != "", self.repository.email != "" else {
                self.loginButton.canceled = true
                return
            }
            self.loginButton.canceled = false
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.emailField) {
            self.repository.email = textField.text ?? ""
            self.loginButton.canceled = !(self.repository.email != "" && self.repository.password != "")
            textField.resignFirstResponder()
            passwordField.becomeFirstResponder()
        } else if (textField == self.passwordField) {
            self.repository.password = textField.text ?? ""
            self.loginButton.canceled = !(self.repository.email != "" && self.repository.password != "")
            textField.resignFirstResponder()
            self.login()
        }
        return true
    }
}

extension AuthViewController: AuthPresenterOutput {
    func sync(didFetchRepository repository: IdentityEntity) {
        self.repository = repository
        DispatchQueue.main.async { [unowned self] in
            self.emailField.text = self.repository.email
            self.passwordField.text = self.repository.password
        }
    }

    func logined() {
        DispatchQueue.main.async { [unowned self] in
            HUD.hide()
            HUD.flash(.success)
            self.presenter?.toMain()
        }
    }

    func authPresenter(onError error: Error) {
        HUD.hide()
        let messageAlert = MessageView.viewFromNib(layout: .cardView)
        messageAlert.configureTheme(.error)
        messageAlert.configureDropShadow()
        messageAlert.configureContent(title: R.string.localizable.error(), body: error.getLocalizedReason())
        messageAlert.button?.isHidden = true
        var messageAlertConfig = SwiftMessages.defaultConfig
        messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
    }
}
