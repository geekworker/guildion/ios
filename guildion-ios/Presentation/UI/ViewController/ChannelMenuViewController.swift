//
//  ChannelMenuViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/28.
//

import Foundation
import UIKit
import PKHUD
import Nuke

class ChannelMenuViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.backgroundColor = Theme.themify(key: .background)
            scrollView.isScrollEnabled = false
        }
    }
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .background)
        }
    }
    @IBOutlet weak var headerView: UIView! {
        didSet {
            headerView.backgroundColor = Theme.themify(key: .backgroundThick)
            headerView.layer.cornerRadius = 8
            headerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.backgroundColor = Theme.themify(key: .border)
        }
    }
    @IBOutlet weak var collectionView: MenuCollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var presenter: ChannelMenuPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var permission: PermissionModel = PermissionModel()
    var repository: ChannelableProtocol = TextChannelEntity()
    var repositories: MemberEntities = MemberEntities()
    var participants: ParticipantEntities = ParticipantEntities()
    var menus: [MenuCollectionViewCell.MenuType] = [
        .setting, .notification, .invite, .editMembers, .leave
    ]
    
    private var loadingDecrementStatus: Int?
    private var loadingIncrementStatus: Int?
    private var lastWillDisplay: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        ChannelMenuViewControllerBuilder.rebuild(self)
        self.navigationMode = .none
        view.backgroundColor = UIColor.black.lighter(by: 10)
        scrollView.layer.cornerRadius = 8
        scrollView.clipsToBounds = true
        evalMenus()
        configureCollectionView()
        configureTableView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        updateLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func updateLayout() {
        switch repository.channelable_type {
        case .DMChannel, .IMChannel:
            guard let channel = repository as? DMChannelEntity else { return }
            self.titleLabel.text = channel.name
            self.descriptionLabel.text = channel.description == "" ? repository.channelable_type.string : channel.description
        case .TextChannel, .AnnouncementChannel:
            guard let channel = repository as? TextChannelEntity else { return }
            self.titleLabel.text = channel.name
            self.descriptionLabel.text = channel.description == "" ? repository.channelable_type.string : channel.description
        case .StreamChannel, .MusicChannel, .VoiceChannel, .PlayChannel, .StreamDMChannel:
            guard let channel = repository as? StreamChannelEntity else { return }
            self.titleLabel.text = channel.name
            self.descriptionLabel.text = channel.description == "" ? repository.channelable_type.string : channel.description
        default: break
        }
        UIView.animate(withDuration: 0, animations: {
            self.tableView.reloadData()
        })
    }
    
    func evalMenus() {
        self.menus = [
            .setting, .notification, .invite, .editMembers, .leave
        ]
        
        switch repository.channelable_type.superChannel {
        case .DMChannel:
            guard repository is DMChannelEntity else { return }
            if !((repository as? DMChannelEntity)?.configable ?? false) {
                self.menus = self.menus.filter({ $0 != .invite })
            }
            if !((repository as? DMChannelEntity)?.configable ?? false) {
                self.menus = self.menus.filter({ $0 != .editMembers })
            }
            if !((repository as? DMChannelEntity)?.configable ?? false) {
                self.menus = self.menus.filter({ $0 != .leave })
            }
        case .TextChannel:
            guard repository is TextChannelEntity else { return }
            if !permission.TextChannel.invitable {
                self.menus = self.menus.filter({ $0 != .invite })
            }
            if !permission.TextChannel.channel_members_managable {
                self.menus = self.menus.filter({ $0 != .editMembers })
            }
        case .StreamChannel:
            guard repository is StreamChannelEntity else { return }
            if ((repository as? StreamChannelEntity)?.is_dm ?? false) {
                if !((repository as? StreamChannelEntity)?.configable ?? false) {
                    self.menus = self.menus.filter({ $0 != .invite })
                }
            } else {
                if !permission.StreamChannel.invitable {
                    self.menus = self.menus.filter({ $0 != .invite })
                }
            }
            if ((repository as? StreamChannelEntity)?.is_dm ?? false) {
                if !((repository as? StreamChannelEntity)?.configable ?? false) {
                    self.menus = self.menus.filter({ $0 != .editMembers })
                }
            } else {
                if !permission.StreamChannel.channel_members_managable {
                    self.menus = self.menus.filter({ $0 != .editMembers })
                }
            }
            if !((repository as? StreamChannelEntity)?.configable ?? false) {
                self.menus = self.menus.filter({ $0 != .leave })
            }
        default: break
        }
    }
}

extension ChannelMenuViewController: UITableViewDelegate {
    fileprivate func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.prefetchDataSource = self
        self.tableView.register([
            UINib(resource: R.nib.memberTableViewCell): R.nib.memberTableViewCell.name,
        ])
        self.tableView.register(
            UINib(resource: R.nib.textTableViewHeaderFooterView),
            forHeaderFooterViewReuseIdentifier: R.nib.textTableViewHeaderFooterView.name
        )
        self.tableView.isScrollEnabled = true
        self.tableView.rowHeight = MemberTableViewCell.cellHeight
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if repositories.count > 0, repositories.count > indexPath.row, let repository = repositories[safe: indexPath.row], let last_id = repositories.last?.getCurrentChannelEntry(self.repository)?.id, repository.getCurrentChannelEntry(self.repository)?.id == last_id, last_id != loadingDecrementStatus {
            loadingDecrementStatus = last_id
            self.presenter?.decrementMembers(self.repository, first_id: last_id)
        }
        lastWillDisplay = indexPath.row
    }
}

extension ChannelMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.memberTableViewCell.name, for: indexPath) as? MemberTableViewCell else {
            fatalError("cell is not exists")
        }
        cell.backgroundColor = Theme.themify(key: .background)
        cell.contentView.backgroundColor = Theme.themify(key: .background)
        cell.setRepository(repositories[safe: indexPath.row] ?? MemberEntity())
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.nib.textTableViewHeaderFooterView.name) as? TextTableViewHeaderFooterView else {
            fatalError("This section is not exist")
        }
        view.titleLabel.text = R.string.localizable.members()
        view.titleLabel.textColor = Theme.themify(key: .stringSecondary)
        view.titleLabel.font = Font.FT(size: .S, family: .L)
        view.backgroundColor = Theme.themify(key: .background)
        view.contentView.backgroundColor = Theme.themify(key: .background)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return TextTableViewHeaderFooterView.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        guard let repository = repositories[safe: indexPath.row] else { return }
        self.presenter?.toMemberMenu(repository: repository)
    }
}

extension ChannelMenuViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        preheater.startPreheating(
            with: makeCacheRequests(indexPaths: indexPaths)
        )
    }
    
    private func makeCacheRequests(indexPaths: [IndexPath]) -> [ImageRequest] {
        var requests: [ImageRequest] = []
        _ = indexPaths.compactMap {
            if let repository = repositories[safe: $0.row], let url = URL(string: repository.picture_small) {
                requests.append(ImageRequest(url: url))
            }
        }
        return requests
    }
}

extension ChannelMenuViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        self.collectionView.configure()
        self.collectionView.setRepositories(menus)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionViewHeightConstraint.constant = self.collectionView.calcHeight()
    }
}

extension ChannelMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.collectionView.collectionView(cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.collectionView.collectionView(didSelectItemAt: indexPath) {
        case .invite: self.presenter?.toMemberSelect()
        case .editMembers: self.presenter?.toMembersEdit(repository: repository)
        case .leave:
            let alert: UIAlertController = UIAlertController(title: R.string.localizable.menuLeave(), message: R.string.localizable.leaveReally(), preferredStyle:  UIAlertController.Style.alert)
                        
            let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.menuLeave(), style: UIAlertAction.Style.default, handler:{
                [unowned self](action: UIAlertAction!) -> Void in
                HUD.show(.progress)
                self.presenter?.leave(repository: repository)
            })
            let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                (action: UIAlertAction!) -> Void in
            })
            alert.addAction(cancelAction)
            alert.addAction(defaultAction)
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        case .setting: self.presenter?.toChannelSetting()
        case .notification: self.presenter?.toChannelNotificationEdit()
        default: break
        }
        self.collectionView.deselectItem(at: indexPath, animated: true)
    }
}

extension ChannelMenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MenuCollectionViewCell.cellSize
    }
}

extension ChannelMenuViewController: MemberSelectViewControllerDelegate {
    func memberSelectViewController(shouldDismiss selecteds: MemberEntities, in memberSelectViewController: MemberSelectViewController) {
        guard selecteds.count > 0 else { return }
        HUD.show(.progress)
        self.presenter?.invite(repository: repository, repositories: selecteds)
    }
}

extension ChannelMenuViewController: ChannelMenuPresenterOutput {
    func channelMenuPresenter(didLeave repository: ChannelableProtocol, in channelMenuPresenter: ChannelMenuPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.dismissShouldAppear(animated: true, completion: {
            UIViewController.topMostController().navigationController?.popViewController(animated: true)
            UIViewController.topMostController().navigationController?.popViewController(animated: true)
        })
    }
    
    func channelMenuPresenter(didInvite repository: ChannelableProtocol, members: MemberEntities, in channelMenuPresenter: ChannelMenuPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.dismissShouldAppear(animated: true)
    }
    
    func channelMenuPresenter(onError error: Error, in channelMenuPresenter: ChannelMenuPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
    
    func sync(didFetchRepository repository: ChannelableProtocol) {
        self.repository = repository
        evalMenus()
    }
    
    func sync(didFetchRepository repositories: MemberEntities) {
        self.repositories = repositories
        self.updateLayout()
    }
    
    func sync(didFetchRepository repositories: ParticipantEntities) {
        self.participants = repositories
    }
    
    func sync(didDecrementRepository repositories: MemberEntities) {
        if repositories.count != 0, repositories.last?.getCurrentChannelEntry(self.repository)?.id != loadingDecrementStatus {
            loadingDecrementStatus = nil
            DispatchQueue.main.async {
                self.repositories = repositories
                self.tableView.reloadData()
            }
        }
    }
}
