//
//  UserGuildsViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class UserGuildsViewController: BaseViewController {
    var presenter: UserGuildsPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repositories: GuildEntities = GuildEntities()

    override func viewDidLoad() {
        super.viewDidLoad()
        UserGuildsViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
}

extension UserGuildsViewController: UserGuildsPresenterOutput {
    func sync(didFetchRepositories repositories: GuildEntities) {
        self.repositories = repositories
    }
}
