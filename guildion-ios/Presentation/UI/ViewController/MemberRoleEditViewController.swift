//
//  MemberRoleEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit
import Eureka
import PKHUD

class MemberRoleEditViewController: BaseFormViewController {
    var presenter: MemberRoleEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: MemberRoleEntity = MemberRoleEntity() {
        didSet {
            new_repository = repository.toNewMemory()
        }
    }
    var new_repository: MemberRoleEntity = MemberRoleEntity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MemberRoleEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    override func setupNavigationBar() {
        super.setupNavigationBar()
    }
    
    func handleValueChange() {
        if repository.priority == new_repository.priority &&
            repository.position == new_repository.position {
            self.resetRightNavigationBarButton()
        } else {
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section(R.string.localizable.roleName())
            <<< ButtonRow(R.string.localizable.name()) {
                $0.title = new_repository.Role?.name
            }.cellUpdate(buttonRowLeftAlignmentUpdate(_:_:))
        
            +++ Section(R.string.localizable.rolePriority())
            <<< SliderRow(R.string.localizable.rolePriority()) {
                $0.title = $0.tag
                $0.value = Float(new_repository.priority)
                $0.cell.slider.maximumValue = Float(DataConfig.Role.max_priority_limit)
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.priority = Int(value)
                $0.value = Float(Int(value))
                self.handleValueChange()
            }.cellUpdate {
                self.sliderRowCellUpdate($0, $1)
                $0.slider.isContinuous = false
                $0.slider.maximumValue = Float(DataConfig.Role.max_priority_limit)
            }
        
            +++ Section(R.string.localizable.roleDetailName())
            <<< TextRow(R.string.localizable.roleDetailName()) {
                var ruleSet = RuleSet<String>()
                ruleSet.add(rule: RuleRequired())
                $0.add(ruleSet: ruleSet)
                $0.value = new_repository.position
                $0.placeholder = $0.tag
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.position = value
                self.handleValueChange()
            }.cellUpdate(textRowCellUpdate(_:_:))
        
            +++ Section()
            <<< ButtonRow(R.string.localizable.guildMenuDelete()) {
                $0.title = $0.tag
                $0.onCellHighlightChanged({ _, _ in })
            }.cellUpdate { [unowned self] in
                self.buttonRowCellUpdate($0, $1)
                $0.textLabel?.textColor = Theme.themify(key: .danger)
                $0.textLabel?.font = Font.FT(size: .FM, family: .B)
            }.onCellSelection { [unowned self] cell, row in
                let alert: UIAlertController = UIAlertController(title: R.string.localizable.roleDelete(), message: R.string.localizable.deleteReally(), preferredStyle:  UIAlertController.Style.alert)
                            
                let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.roleDelete(), style: UIAlertAction.Style.default, handler:{
                    [unowned self](action: UIAlertAction!) -> Void in
                    HUD.show(.progress)
                     self.presenter?.delete(repository: self.repository)
                })
                let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                    (action: UIAlertAction!) -> Void in
                })
                alert.addAction(cancelAction)
                alert.addAction(defaultAction)
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                self.present(alert, animated: true, completion: nil)
            }
    }
}

extension MemberRoleEditViewController: MemberRoleEditPresenterOutput {
    func sync(didFetchRepository repository: MemberRoleEntity) {
        self.repository = repository
        self.reloadForm()
    }
    
    func memberRoleEditPresenter(didCreate repository: MemberRoleEntity, in memberRoleEditPresenter: MemberRoleEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func memberRoleEditPresenter(didUpdate repository: MemberRoleEntity, in memberRoleEditPresenter: MemberRoleEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func memberRoleEditPresenter(didDelete repository: MemberRoleEntity, in memberRoleEditPresenter: MemberRoleEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func memberRoleEditPresenter(onError error: Error, in memberRoleEditPresenter: MemberRoleEditPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
