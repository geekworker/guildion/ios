//
//  UserEditViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit
import Eureka
import PKHUD
import SwiftMessages

class UserEditViewController: BaseFormViewController {
    var presenter: UserEditPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var repository: UserEntity = UserEntity() {
        didSet {
            new_repository.nickname = repository.nickname
            new_repository.description = repository.description
            new_repository.picture_small = repository.picture_small
            new_repository.picture_large = repository.picture_large
            new_repository.id = repository.id
            new_repository.username = repository.username
        }
    }
    var new_repository: UserEntity = UserEntity()

    override func viewDidLoad() {
        super.viewDidLoad()
        UserEditViewControllerBuilder.rebuild(self)
    }
     
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.bind()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    fileprivate var shouldUpdate: Bool = false
    func handleValueChange() {
        if repository.nickname == new_repository.nickname && repository.description == new_repository.description && repository.picture_small == new_repository.picture_small && repository.picture_large == new_repository.picture_large {
            self.resetRightNavigationBarButton()
            self.shouldUpdate = false
        } else {
            self.shouldUpdate = true
            self.setupSaveNavigationBarButton()
        }
    }
    
    override func navigationBarSaveTapped(_ sender: Any) {
        HUD.show(.progress)
        self.presenter?.update(repository: new_repository)
    }
    
    override func configureForm() {
        super.configureForm()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        form +++
            Section()
            <<< ImageRow() {
                $0.title = R.string.localizable.profileImage()
                $0.value = UIImage(url: new_repository.picture_small)
            }.onChange { [unowned self] in
                guard let image = $0.value else {
                    self.new_repository.picture_small = DataConfig.Image.default_profile_image_url
                    self.reloadForm()
                    self.handleValueChange()
                    return
                }
                self.new_repository.picture_small = "\(ImagePickerHandler().generateImageURL(image))"
                self.handleValueChange()
            }.cellUpdate(imageRowCellUpdate(_:_:))
            <<< TextRow(R.string.localizable.nickname()) {
                var ruleSet = RuleSet<String>()
                ruleSet.add(rule: RuleRequired())
                $0.add(ruleSet: ruleSet)
                $0.value = new_repository.nickname
                $0.placeholder = $0.tag
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.nickname = value
                self.handleValueChange()
            }.cellUpdate(textRowCellUpdate(_:_:))
            <<< TextAreaRow(R.string.localizable.introduction()) {
                var ruleSet = RuleSet<String>()
                ruleSet.add(rule: RuleRequired())
                $0.add(ruleSet: ruleSet)
                $0.placeholder = R.string.localizable.introduction()
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
                $0.value = new_repository.description
            }.onChange { [unowned self] in
                guard let value = $0.value else { return }
                self.new_repository.description = value
                self.handleValueChange()
            }.cellUpdate(textAreaRowCellUpdate(_:_:))
            
            +++ Section()
            <<< ButtonRow(R.string.localizable.userUpdateAllMembers()) {
                $0.title = $0.tag
            }.cellUpdate { [unowned self] in
                self.buttonRowCellUpdate($0, $1)
                $0.textLabel?.textColor = Theme.themify(key: .warning)
                $0.textLabel?.font = Font.FT(size: .FM, family: .B)
            }.onCellSelection { [unowned self] cell, row in
                let alert: UIAlertController = UIAlertController(title: R.string.localizable.userUpdateAllMembersAlertTitle(), message: "", preferredStyle:  UIAlertController.Style.alert)
                            
                let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.next(), style: UIAlertAction.Style.default, handler:{ [unowned self] (action: UIAlertAction!) -> Void in
                    HUD.show(.progress)
                    self.presenter?.updates(repository: self.shouldUpdate ? self.new_repository : nil)
                })
                let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                    (action: UIAlertAction!) -> Void in
                })
                alert.addAction(cancelAction)
                alert.addAction(defaultAction)
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
                self.present(alert, animated: true, completion: nil)
            }
    }
}

extension UserEditViewController: UserEditPresenterOutput {
    func sync(didFetchRepository repository: UserEntity) {
        self.repository = repository
    }
    
    func userEditPresenter(afterUpdated repository: UserEntity, in userEditPresenter: UserEditPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.navigationController?.popViewController(animated: true)
        self.dismissShouldAppear(animated: true)
    }
    
    func userEditPresenter(onError error: Error, in userEditPresenter: UserEditPresenter) {
        HUD.hide()
        let messageAlert = MessageView.viewFromNib(layout: .cardView)
        messageAlert.configureTheme(.error)
        messageAlert.configureDropShadow()
        messageAlert.configureContent(title: R.string.localizable.error(), body: error.getLocalizedReason())
        messageAlert.button?.isHidden = true
        var messageAlertConfig = SwiftMessages.defaultConfig
        messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
    }
}
