//
//  HideNavigationCotroller.swift
//  nowy-ios
//
//  Created by Apple on 2020/04/21.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class HideNavigationController: UINavigationController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarHidden(true, animated: false)
    }
}
