//
//  GuildMenuViewController.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit
import PanModal
import Nuke
import PKHUD

class GuildMenuViewController: BaseViewController {
    public var viewContrllerParent: GuildViewController?
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseView: UIView! {
        didSet {
            baseView.backgroundColor = Theme.themify(key: .backgroundLight)
        }
    }
    @IBOutlet weak var headView: UIView! {
        didSet {
            headView.layer.cornerRadius = 8
            headView.clipsToBounds = true
        }
    }
    @IBOutlet weak var guildImageView: UIImageView! {
        didSet {
            guildImageView.layer.cornerRadius = 5
            guildImageView.clipsToBounds = true
            guildImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Theme.themify(key: .string)
        }
    }
    @IBOutlet weak var idLabel: UILabel! {
        didSet {
            idLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var onlineBadgeView: UIView! {
        didSet {
            onlineBadgeView.layer.cornerRadius = onlineBadgeView.frame.width / 2
            onlineBadgeView.clipsToBounds = true
            onlineBadgeView.backgroundColor = Theme.themify(key: .main)
        }
    }
    @IBOutlet weak var onlineCountLabel: UILabel! {
        didSet {
            onlineCountLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var memberBadgeView: UIView! {
        didSet {
            memberBadgeView.layer.cornerRadius = memberBadgeView.frame.width / 2
            memberBadgeView.clipsToBounds = true
            memberBadgeView.backgroundColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var memberCountLabel: UILabel! {
        didSet {
            memberCountLabel.textColor = Theme.themify(key: .stringSecondary)
        }
    }
    @IBOutlet weak var collectionView: MenuCollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    var presenter: GuildMenuPresenter? {
        didSet {
            self.presenter?.viewController = self
        }
    }
    var permission: PermissionModel = PermissionModel()
    var repository: GuildEntity = GuildEntity()
    var current_member: MemberEntity = MemberEntity()
    var menus: [MenuCollectionViewCell.MenuType] = [
        .addChannel, .addSection, .setting, .notification, .playlists, .invite, .leave
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        GuildMenuViewControllerBuilder.rebuild(self)
        evalMenus()
        configureCollectionView()
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.bind()
        updateLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.unbind()
    }
    
    func setRepository(repository: GuildEntity) {
        self.repository = repository
        if isViewLoaded {
            self.updateLayout()
        }
    }
    
    func updateLayout() {
        self.nameLabel.text = repository.name
        self.idLabel.text = "@" + repository.uid
        self.onlineCountLabel.text = "\(repository.active_count) \(R.string.localizable.online())"
        self.memberCountLabel.text = "\(repository.member_count) \(R.string.localizable.members())"
        
        if let url = URL(string: repository.picture_small) {
            Nuke.loadImage(with: url, into: self.guildImageView)
        }
    }
    
    func evalMenus() {
        self.menus = [
            .addChannel, .addSection, .setting, .notification, .playlists, .invite, .leave
        ]
        if !permission.Guild.channels_managable {
            self.menus = self.menus.filter({ $0 != .addChannel && $0 != .addSection })
        }
        
        if !permission.Guild.folders_viewable {
            self.menus = self.menus.filter({ $0 != .playlists })
        }
        
        if repository.OwnerId == CurrentUser.getCurrentUserEntity()?.id {
            self.menus = self.menus.filter({ $0 != .leave })
        }
    }
}

extension GuildMenuViewController: UICollectionViewDelegate {
    fileprivate func configureCollectionView() {
        self.collectionView.configure()
        self.collectionView.setRepositories(menus)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionViewHeightConstraint.constant = self.collectionView.calcHeight()
    }
}

extension GuildMenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionView.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.collectionView.collectionView(cellForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.collectionView.collectionView(didSelectItemAt: indexPath) {
        case .addChannel: self.presenter?.toChannelNew()
        case .addSection: self.presenter?.toSectionNew()
        case .leave:
            let alert: UIAlertController = UIAlertController(title: R.string.localizable.menuLeave(), message: R.string.localizable.leaveReally(), preferredStyle:  UIAlertController.Style.alert)
                        
            let defaultAction: UIAlertAction = UIAlertAction(title: R.string.localizable.menuLeave(), style: UIAlertAction.Style.default, handler:{
                [unowned self](action: UIAlertAction!) -> Void in
                HUD.show(.progress)
                self.presenter?.leave(repository)
            })
            let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
                (action: UIAlertAction!) -> Void in
            })
            alert.addAction(cancelAction)
            alert.addAction(defaultAction)
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            self.present(alert, animated: true, completion: nil)
        case .playlists:
            self.dismiss(animated: true, completion: nil)
            self.viewContrllerParent?.delegate?.guildViewController(didSelectPlaylists: self.viewContrllerParent ?? GuildViewController())
        case .setting: self.presenter?.toGuildSetting()
        case .notification: self.presenter?.toGuildNotificationSetting()
        case .invite: self.presenter?.toGuildShare()
        default: break
        }
        self.collectionView.deselectItem(at: indexPath, animated: true)
    }
}

extension GuildMenuViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MenuCollectionViewCell.cellSize
    }
}

extension GuildMenuViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return scrollView
    }
    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(300)
    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeight
    }

    var anchorModalToLongForm: Bool {
        return false
    }
    
    var allowsExtendedPanScrolling: Bool { return true }
}

extension GuildMenuViewController: GuildMenuPresenterOutput {
    func sync(didFetchRepository repository: GuildEntity) {
        self.repository = repository
        evalMenus()
    }
    
    func guildMenuPresenter(didLeave repository: GuildEntity, in guildMenuPresenter: GuildMenuPresenter) {
        HUD.hide()
        HUD.flash(.success)
        self.dismissShouldAppear(animated: true)
    }
    
    func guildMenuPresenter(onError error: Error, in guildMenuPresenter: GuildMenuPresenter) {
        HUD.hide()
        HUD.flash(.error)
    }
}
