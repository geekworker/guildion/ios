//
//  UniversalLinkHandler.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/15.
//

import Foundation
import UIKit

class UniversalLinkHandler: NSObject {
    
    static func execSegue(url: URL) -> Bool {
        AppConfig.should_open_universal_url = nil
        let linkType = UniversalLinkType(url: url)
        switch linkType {
        case .guildWelcome(let uid):
            BaseWireframe.toGuildWelcome(uid: uid)
            return true
        case .channel(let guild_uid, let channel_uid):
            GuildUseCase.shared.getGuildChannel(guild_uid: guild_uid, channel_uid: channel_uid).then({
                BaseWireframe.toGuildChannel(guild: $0.guild, member: $0.member, channel: $0.channel)
            })
            return true
        default: return false
        }
    }
}
