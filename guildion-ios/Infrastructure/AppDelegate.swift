//
//  AppDelegate.swift
//  guildion-ios
//
//  Created by Apple on 2020/09/28.
//

import UIKit
import CoreData
import ReSwift
import WebRTC
import OneSignal
import DropDown
import Hydra
import PKHUD

let sagaMiddleware: Middleware<State> = createSagasMiddleware()
var store = Store<State>(reducer: rootReducer, state: nil, middleware: [sagaMiddleware])
let appDelegate = UIApplication.shared.delegate as! AppDelegate

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    var isConfiguredOneSignal: Bool = false
    public var backgroundTaskID: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
    public var presenter: AppDelegatePresenter? {
        didSet {
            self.presenter?.delegate = self
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppDelegateBuilder.rebuild(self)
        self.launchOptions = launchOptions
        self.presenter?.bind()
        if let url = launchOptions?[.url] as? URL {
            AppConfig.should_open_universal_url = url
        }
        return true
    }
    
    func configureOneSignal() {
        guard !isConfiguredOneSignal, !Constant.LOCAL else { return }
        self.presenter?.configureOneSignal(launchOptions: launchOptions)
        self.isConfiguredOneSignal = true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        self.presenter?.exitBackground()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.presenter?.enterBackground()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.presenter?.unbind()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        guard (options[.sourceApplication] as? String) != nil else {
            return false
        }
        AppConfig.should_open_universal_url = url
        if UniversalLinkHandler.execSegue(url: url) {
            return true
        }
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            return UniversalLinkHandler.execSegue(url: userActivity.webpageURL!)
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

extension AppDelegate: AppDelegatePresenterOutput {
}
