//
//  AppDelegateBuilder.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit

class AppDelegateBuilder {
    static func rebuild(_ appDelegate: AppDelegate) {
        let appDelegatePresenter = AppDelegatePresenter(
            authUseCase: BaseViewControllerBuilder.build_AuthUseCase(),
            categoryUseCase: BaseViewControllerBuilder.build_CategoryUseCase(),
            folderUseCase: BaseViewControllerBuilder.build_FolderUseCase(),
            dmChannelUseCase: BaseViewControllerBuilder.build_DMChannelUseCase(),
            fileUseCase: BaseViewControllerBuilder.build_FileUseCase(),
            guildUseCase: BaseViewControllerBuilder.build_GuildUseCase(),
            searchUseCase: BaseViewControllerBuilder.build_SearchUseCase(),
            sessionUseCase: BaseViewControllerBuilder.build_SessionUseCase(),
            streamChannelUseCase: BaseViewControllerBuilder.build_StreamChannelUseCase(),
            streamUseCase: BaseViewControllerBuilder.build_StreamUseCase(),
            textChannelUseCase: BaseViewControllerBuilder.build_TextChannelUseCase(),
            tagUseCase: BaseViewControllerBuilder.build_TagUseCase(),
            notificationUseCase: BaseViewControllerBuilder.build_NotificationUseCase(),
            messageUseCase: BaseViewControllerBuilder.build_MessageUseCase(),
            memberUseCase: BaseViewControllerBuilder.build_MemberUseCase(),
            userUseCase: BaseViewControllerBuilder.build_UserUseCase()
        )
        appDelegate.presenter = appDelegatePresenter
    }
}
