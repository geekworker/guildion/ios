//
//  SceneDelegatePresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Hydra
import ReSwift
import Foundation
import WebRTC
import OneSignal

protocol SceneDelegatePresenterOutput: class {
}

class SceneDelegatePresenter: NSObject {
    var delegate: SceneDelegatePresenterOutput?
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase
    ) {
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
    }
    
    override init() {
    }
    
    func bind() {
        store.subscribe(self)
    }
    
    func unbind() {
        store.unsubscribe(self)
    }
    
    func enterBackground() {
        AppConfig.is_background = true
    }
    
    func exitBackground() {
        AppConfig.is_background = false
    }
}

extension SceneDelegatePresenter: OSSubscriptionObserver {
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        if let playerId = stateChanges.to.userId {
            print("Current playerId \(playerId)")
        }
    }
    
    func configureOneSignal(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
//            store.dispatch(self.notificationUseCase!.getUserNotifications())
        }
        
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
        OneSignal.initWithLaunchOptions(
            launchOptions,
            appId: ENV.ONESIGNAL.APP_ID,
            handleNotificationReceived: notificationReceivedBlock,
            handleNotificationAction: nil,
            settings: onesignalInitSettings
        )
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        OneSignal.add(self as OSSubscriptionObserver)
    }
}

extension SceneDelegatePresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    func newState(state: SceneDelegatePresenter.StoreSubscriberStateType) {
    }
}

