//
//  AppDelegatePresenter.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Hydra
import ReSwift
import Foundation
import WebRTC
import OneSignal
import DropDown
import Firebase
import Siren

protocol AppDelegatePresenterOutput: class {
}

class AppDelegatePresenter: NSObject {
    weak var delegate: AppDelegatePresenterOutput?
    private var joined = false
    private var fetchedNotifications = false
    
    var authUseCase: AuthUseCase?
    var categoryUseCase: CategoryUseCase?
    var folderUseCase: FolderUseCase?
    var dmChannelUseCase: DMChannelUseCase?
    var fileUseCase: FileUseCase?
    var guildUseCase: GuildUseCase?
    var searchUseCase: SearchUseCase?
    var sessionUseCase: SessionUseCase?
    var streamChannelUseCase: StreamChannelUseCase?
    var streamUseCase: StreamUseCase?
    var textChannelUseCase: TextChannelUseCase?
    var tagUseCase: TagUseCase?
    var notificationUseCase: NotificationUseCase?
    var messageUseCase: MessageUseCase?
    var memberUseCase: MemberUseCase?
    var userUseCase: UserUseCase?
    var roleUseCase: RoleUseCase?

    required init(
        authUseCase: AuthUseCase,
        categoryUseCase: CategoryUseCase,
        folderUseCase: FolderUseCase,
        dmChannelUseCase: DMChannelUseCase,
        fileUseCase: FileUseCase,
        guildUseCase: GuildUseCase,
        searchUseCase: SearchUseCase,
        sessionUseCase: SessionUseCase,
        streamChannelUseCase: StreamChannelUseCase,
        streamUseCase: StreamUseCase,
        textChannelUseCase: TextChannelUseCase,
        tagUseCase: TagUseCase,
        notificationUseCase: NotificationUseCase,
        messageUseCase: MessageUseCase,
        memberUseCase: MemberUseCase,
        userUseCase: UserUseCase
    ) {
        self.authUseCase = authUseCase
        self.categoryUseCase = categoryUseCase
        self.folderUseCase = folderUseCase
        self.dmChannelUseCase = dmChannelUseCase
        self.fileUseCase = fileUseCase
        self.guildUseCase = guildUseCase
        self.searchUseCase = searchUseCase
        self.sessionUseCase = sessionUseCase
        self.streamChannelUseCase = streamChannelUseCase
        self.streamUseCase = streamUseCase
        self.textChannelUseCase = textChannelUseCase
        self.tagUseCase = tagUseCase
        self.notificationUseCase = notificationUseCase
        self.messageUseCase = messageUseCase
        self.memberUseCase = memberUseCase
        self.userUseCase = userUseCase
    }
    
    override init() {
    }
    
    func bind() {
        store.subscribe(self)
        RTCInitializeSSL()
        DropDown.startListeningToKeyboard()
        FirebaseApp.configure()
        categoryUseCase!.getCategories().then({ _ in })
        if !ClientDevise.deviseRegisterd() {
            self.userUseCase!.registerDevise().then({
                self.authUseCase!.syncCurrentUser().then({ current_user in
                    AppConfig.session_out = current_user == nil
                    self.userUseCase!.active().then({})
                }).catch({ _ in
                    AppConfig.session_out = true
                })
            })
        } else if CurrentUser.getCurrentUserEntity() != nil {
            self.authUseCase!.syncCurrentUser().then({ current_user in
                AppConfig.session_out = current_user == nil
                self.userUseCase!.updateDevise().then({})
                self.userUseCase!.active().then({})
            }).catch({ _ in
                AppConfig.session_out = true
            })
        }
        self.forceUpdate()
    }
    
    func unbind() {
        RTCCleanupSSL()
        self.userUseCase!.logoutDevise().then({})
        self.userUseCase!.inActive().then({})
        if let vc = LiveConfig.playingStreamChannelViewController {
            vc.closeWebRTC()
            vc.presenter?.forceLeave()
        }
        sleep(1)
        store.unsubscribe(self)
    }
    
    func enterBackground() {
        AppConfig.is_background = true
    }
    
    func exitBackground() {
        AppConfig.is_background = false
    }
    
    func forceUpdate() {
        let siren = Siren.shared
        switch LocaleConfig.devise_default_locale {
        case .ja: siren.presentationManager = PresentationManager(forceLanguageLocalization: .japanese)
        case .en: siren.presentationManager = PresentationManager(forceLanguageLocalization: .english)
        default: break
        }
        siren.presentationManager = PresentationManager(forceLanguageLocalization: .japanese)
        siren.rulesManager = RulesManager(globalRules: .critical)
        siren.wail { results in
            switch results {
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension AppDelegatePresenter {
}

extension AppDelegatePresenter: OSSubscriptionObserver, CurrentUserProtocol, ClientDeviseProtocol {
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        if let playerId = stateChanges.to.userId {
            print("Current playerId \(playerId)")
            ClientDevise.getClientDevise().notification_id = playerId
            setClientDevise(entity: ClientDevise.getClientDevise())
            UserUseCase.shared.updateDevise().then({_ in})
        }
    }
    
    func configureOneSignal(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            if let action = notification?.payload, let url = URL(string: action.launchURL) {
                AppConfig.should_open_universal_url = url
            }
        }
        
        let notificationActionBlock: OSHandleNotificationActionBlock = { result in
            if let action = result?.notification.payload, let url = URL(string: action.launchURL) {
                AppConfig.should_open_universal_url = url
            }
        }
        
        let notificationClickBlock: OSHandleInAppMessageActionClickBlock = { action in
            if let unwrapped = action, let url = unwrapped.clickUrl {
                AppConfig.should_open_universal_url = url
            }
        }

        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
        OneSignal.initWithLaunchOptions(
            launchOptions,
            appId: ENV.ONESIGNAL.APP_ID,
            handleNotificationReceived: notificationReceivedBlock,
            handleNotificationAction: notificationActionBlock,
            settings: onesignalInitSettings
        )
        OneSignal.setInAppMessageClickHandler(notificationClickBlock)
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        OneSignal.add(self as OSSubscriptionObserver)
    }
}

extension AppDelegatePresenter: StoreSubscriber {
    typealias StoreSubscriberStateType = State
    func newState(state: AppDelegatePresenter.StoreSubscriberStateType) {
    }
}
