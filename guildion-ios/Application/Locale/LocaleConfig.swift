//
//  LocaleConfig.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation

struct LocaleConfig {
    enum Locales: String, CaseIterable {
        case ja = "ja"
        case en = "en"
        case other = ""
        
        init(lang: String) {
            switch lang {
            case "日本語": self = .ja
            case "English": self = .en
            default: self = .other
            }
        }
        
        init(locale: String) {
            switch locale {
            case Locales.ja.rawValue: self = .ja
            case Locales.en.rawValue: self = .en
            default: self = .other
            }
        }
        
        var languadge: String {
            switch self {
            case .ja: return "日本語"
            case .en: return "English"
            case .other: return Locales.en.languadge
            }
        }

        static var languadges: [String] = ["日本語", "English"]
    }
    
    static let default_locale: Locales = .en
    static let devise_default_locale: Locales = Locales.init(locale: NSLocale.current.languageCode ?? default_locale.rawValue)
    static let devise_default_country_code: String = Locale.current.regionCode ?? ""
}
