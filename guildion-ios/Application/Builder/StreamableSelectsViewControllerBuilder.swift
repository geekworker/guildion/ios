//
//  StreamableSelectsViewControllerBuilder.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit

class StreamableSelectsViewControllerBuilder: ViewControllerBuilder {
    typealias ViewControllerType = StreamableSelectsViewController
    
    static func build() -> ViewControllerType {
        let viewController = StreamableSelectsViewController()
        let wireframe = StreamableSelectsWireframe(viewController: viewController),
        presenter = StreamableSelectsViewControllerBuilder.build_Presenter(wireframe: wireframe)
        viewController.presenter = presenter
        return viewController
    }
    
    static func rebuild(_ viewController: ViewControllerType) {
        guard viewController.presenter == nil else { return }
        let wireframe = StreamableSelectsWireframe(viewController: viewController),
        presenter = StreamableSelectsViewControllerBuilder.build_Presenter(wireframe: wireframe)
        viewController.presenter = presenter
    }
    
    static func build_Presenter(
        wireframe: StreamableSelectsWireframe,
        authUseCase: AuthUseCase = build_AuthUseCase(),
        categoryUseCase: CategoryUseCase = build_CategoryUseCase(),
        folderUseCase: FolderUseCase = build_FolderUseCase(),
        dmChannelUseCase: DMChannelUseCase = build_DMChannelUseCase(),
        fileUseCase: FileUseCase = build_FileUseCase(),
        guildUseCase: GuildUseCase = build_GuildUseCase(),
        searchUseCase: SearchUseCase = build_SearchUseCase(),
        sessionUseCase: SessionUseCase = build_SessionUseCase(),
        streamChannelUseCase: StreamChannelUseCase = build_StreamChannelUseCase(),
        streamUseCase: StreamUseCase = build_StreamUseCase(),
        textChannelUseCase: TextChannelUseCase = build_TextChannelUseCase(),
        tagUseCase: TagUseCase = build_TagUseCase(),
        notificationUseCase: NotificationUseCase = build_NotificationUseCase(),
        messageUseCase: MessageUseCase = build_MessageUseCase(),
        memberUseCase: MemberUseCase = build_MemberUseCase(),
        userUseCase: UserUseCase = build_UserUseCase(),
        roleUseCase: RoleUseCase = build_RoleUseCase()
    ) -> StreamableSelectsPresenter {
        return StreamableSelectsPresenter(
            wireframe: wireframe,
            authUseCase: authUseCase,
            categoryUseCase: categoryUseCase,
            folderUseCase: folderUseCase,
            dmChannelUseCase: dmChannelUseCase,
            fileUseCase: fileUseCase,
            guildUseCase: guildUseCase,
            searchUseCase: searchUseCase,
            sessionUseCase: sessionUseCase,
            streamChannelUseCase: streamChannelUseCase,
            streamUseCase: streamUseCase,
            textChannelUseCase: textChannelUseCase,
            tagUseCase: tagUseCase,
            notificationUseCase: notificationUseCase,
            messageUseCase: messageUseCase,
            memberUseCase: memberUseCase,
            userUseCase: userUseCase,
            roleUseCase: roleUseCase
        )
    }
}


