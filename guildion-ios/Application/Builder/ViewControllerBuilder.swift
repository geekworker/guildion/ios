//
//  ViewControllerBuilder.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//


import UIKit
import Foundation

protocol ViewControllerBuilder: class {
    associatedtype ViewControllerType: UIViewController
    
    static func build() -> ViewControllerType
    
    static func rebuild(_ viewController: ViewControllerType)
}

extension ViewControllerBuilder {
    static func build_AuthDataStore() -> AuthDataStore {
        return AuthDataStore.shared
    }
    
    static func build_CategoryDataStore() -> CategoryDataStore {
        return CategoryDataStore.shared
    }
    
    static func build_FolderDataStore() -> FolderDataStore {
        return FolderDataStore.shared
    }
    
    static func build_DMChannelDataStore() -> DMChannelDataStore {
        return DMChannelDataStore.shared
    }
    
    static func build_FileDataStore() -> FileDataStore {
        return FileDataStore.shared
    }
    
    static func build_GuildDataStore() -> GuildDataStore {
        return GuildDataStore.shared
    }
    
    static func build_SearchDataStore() -> SearchDataStore {
        return SearchDataStore.shared
    }
    
    static func build_SessionDataStore() -> SessionDataStore {
        return SessionDataStore.shared
    }
    
    static func build_StreamChannelDataStore() -> StreamChannelDataStore {
        return StreamChannelDataStore.shared
    }
    
    static func build_StreamDataStore() -> StreamDataStore {
        return StreamDataStore.shared
    }
    
    static func build_TextChannelDataStore() -> TextChannelDataStore {
        return TextChannelDataStore.shared
    }
    
    static func build_TagDataStore() -> TagDataStore {
        return TagDataStore.shared
    }
    
    static func build_NotificationDataStore() -> NotificationDataStore {
        return NotificationDataStore.shared
    }
    
    static func build_MessageDataStore() -> MessageDataStore {
        return MessageDataStore.shared
    }
    
    static func build_MemberDataStore() -> MemberDataStore {
        return MemberDataStore.shared
    }
    
    static func build_UserDataStore() -> UserDataStore {
        return UserDataStore.shared
    }
    
    static func build_RoleDataStore() -> RoleDataStore {
        return RoleDataStore.shared
    }
    
    static func build_AuthRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> AuthRepository {
        return AuthRepository.shared
    }

    static func build_CategoryRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> CategoryRepository {
        return CategoryRepository.shared
    }

    static func build_FolderRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> FolderRepository {
        return FolderRepository.shared
    }

    static func build_DMChannelRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> DMChannelRepository {
        return DMChannelRepository.shared
    }

    static func build_FileRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> FileRepository {
        return FileRepository.shared
    }

    static func build_GuildRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> GuildRepository {
        return GuildRepository.shared
    }

    static func build_SearchRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> SearchRepository {
        return SearchRepository.shared
    }

    static func build_SessionRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> SessionRepository {
        return SessionRepository.shared
    }

    static func build_StreamChannelRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> StreamChannelRepository {
        return StreamChannelRepository.shared
    }

    static func build_StreamRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> StreamRepository {
        return StreamRepository.shared
    }

    static func build_TextChannelRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> TextChannelRepository {
        return TextChannelRepository.shared
    }

    static func build_TagRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> TagRepository {
        return TagRepository.shared
    }

    static func build_NotificationRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> NotificationRepository {
        return NotificationRepository.shared
    }

    static func build_MessageRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> MessageRepository {
        return MessageRepository.shared
    }

    static func build_MemberRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> MemberRepository {
        return MemberRepository.shared
    }

    static func build_UserRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> UserRepository {
        return UserRepository.shared
    }
    
    static func build_RoleRepository(
        authDataStore: AuthDataStore = build_AuthDataStore(),
        categoryDataStore: CategoryDataStore = build_CategoryDataStore(),
        folderDataStore: FolderDataStore = build_FolderDataStore(),
        dmChannelDataStore: DMChannelDataStore = build_DMChannelDataStore(),
        fileDataStore: FileDataStore = build_FileDataStore(),
        guildDataStore: GuildDataStore = build_GuildDataStore(),
        searchDataStore: SearchDataStore = build_SearchDataStore(),
        sessionDataStore: SessionDataStore = build_SessionDataStore(),
        streamChannelDataStore: StreamChannelDataStore = build_StreamChannelDataStore(),
        streamDataStore: StreamDataStore = build_StreamDataStore(),
        textChannelDataStore: TextChannelDataStore = build_TextChannelDataStore(),
        tagDataStore: TagDataStore = build_TagDataStore(),
        notificationDataStore: NotificationDataStore = build_NotificationDataStore(),
        messageDataStore: MessageDataStore = build_MessageDataStore(),
        memberDataStore: MemberDataStore = build_MemberDataStore(),
        userDataStore: UserDataStore = build_UserDataStore(),
        roleDataStore: RoleDataStore = build_RoleDataStore()
    ) -> RoleRepository {
        return RoleRepository.shared
    }
    
    static func build_AuthUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> AuthUseCase {
        return AuthUseCase.shared
    }

    static func build_CategoryUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> CategoryUseCase {
        return CategoryUseCase.shared
    }

    static func build_FolderUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> FolderUseCase {
        return FolderUseCase.shared
    }

    static func build_DMChannelUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> DMChannelUseCase {
        return DMChannelUseCase.shared
    }

    static func build_FileUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> FileUseCase {
        return FileUseCase.shared
    }

    static func build_GuildUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> GuildUseCase {
        return GuildUseCase.shared
    }

    static func build_SearchUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> SearchUseCase {
        return SearchUseCase.shared
    }

    static func build_SessionUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> SessionUseCase {
        return SessionUseCase.shared
    }

    static func build_StreamChannelUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> StreamChannelUseCase {
        return StreamChannelUseCase.shared
    }

    static func build_StreamUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> StreamUseCase {
        return StreamUseCase.shared
    }

    static func build_TextChannelUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> TextChannelUseCase {
        return TextChannelUseCase.shared
    }

    static func build_TagUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> TagUseCase {
        return TagUseCase.shared
    }

    static func build_NotificationUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> NotificationUseCase {
        return NotificationUseCase.shared
    }

    static func build_MessageUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> MessageUseCase {
        return MessageUseCase.shared
    }

    static func build_MemberUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> MemberUseCase {
        return MemberUseCase.shared
    }

    static func build_UserUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> UserUseCase {
        return UserUseCase.shared
    }
    
    static func build_RoleUseCase(
        authRepository: AuthRepository = build_AuthRepository(),
        categoryRepository: CategoryRepository = build_CategoryRepository(),
        folderRepository: FolderRepository = build_FolderRepository(),
        dmChannelRepository: DMChannelRepository = build_DMChannelRepository(),
        fileRepository: FileRepository = build_FileRepository(),
        guildRepository: GuildRepository = build_GuildRepository(),
        searchRepository: SearchRepository = build_SearchRepository(),
        sessionRepository: SessionRepository = build_SessionRepository(),
        streamChannelRepository: StreamChannelRepository = build_StreamChannelRepository(),
        streamRepository: StreamRepository = build_StreamRepository(),
        textChannelRepository: TextChannelRepository = build_TextChannelRepository(),
        tagRepository: TagRepository = build_TagRepository(),
        notificationRepository: NotificationRepository = build_NotificationRepository(),
        messageRepository: MessageRepository = build_MessageRepository(),
        memberRepository: MemberRepository = build_MemberRepository(),
        userRepository: UserRepository = build_UserRepository(),
        roleRepository: RoleRepository = build_RoleRepository()
    ) -> RoleUseCase {
        return RoleUseCase.shared
    }
}


class BaseViewControllerBuilder: ViewControllerBuilder {
    typealias ViewControllerType = UIViewController
    static func build() -> UIViewController {
        return UIViewController()
    }
    
    static func rebuild(_ viewController: UIViewController) {
    }
}
