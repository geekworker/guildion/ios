//
//  ChannelMembersEditViewControllerBuilder.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/02.
//

import Foundation
import UIKit

class ChannelMembersEditViewControllerBuilder: ViewControllerBuilder {
    typealias ViewControllerType = ChannelMembersEditViewController
    
    static func build() -> ViewControllerType {
        let viewController = ChannelMembersEditViewController()
        let wireframe = ChannelMembersEditWireframe(viewController: viewController),
        presenter = ChannelMembersEditViewControllerBuilder.build_Presenter(wireframe: wireframe)
        viewController.presenter = presenter
        return viewController
    }
    
    static func rebuild(_ viewController: ViewControllerType) {
        guard viewController.presenter == nil else { return }
        let wireframe = ChannelMembersEditWireframe(viewController: viewController),
        presenter = ChannelMembersEditViewControllerBuilder.build_Presenter(wireframe: wireframe)
        viewController.presenter = presenter
    }
    
    static func build_Presenter(
        wireframe: ChannelMembersEditWireframe,
        authUseCase: AuthUseCase = build_AuthUseCase(),
        categoryUseCase: CategoryUseCase = build_CategoryUseCase(),
        folderUseCase: FolderUseCase = build_FolderUseCase(),
        dmChannelUseCase: DMChannelUseCase = build_DMChannelUseCase(),
        fileUseCase: FileUseCase = build_FileUseCase(),
        guildUseCase: GuildUseCase = build_GuildUseCase(),
        searchUseCase: SearchUseCase = build_SearchUseCase(),
        sessionUseCase: SessionUseCase = build_SessionUseCase(),
        streamChannelUseCase: StreamChannelUseCase = build_StreamChannelUseCase(),
        streamUseCase: StreamUseCase = build_StreamUseCase(),
        textChannelUseCase: TextChannelUseCase = build_TextChannelUseCase(),
        tagUseCase: TagUseCase = build_TagUseCase(),
        notificationUseCase: NotificationUseCase = build_NotificationUseCase(),
        messageUseCase: MessageUseCase = build_MessageUseCase(),
        memberUseCase: MemberUseCase = build_MemberUseCase(),
        userUseCase: UserUseCase = build_UserUseCase(),
        roleUseCase: RoleUseCase = build_RoleUseCase()
    ) -> ChannelMembersEditPresenter {
        return ChannelMembersEditPresenter(
            wireframe: wireframe,
            authUseCase: authUseCase,
            categoryUseCase: categoryUseCase,
            folderUseCase: folderUseCase,
            dmChannelUseCase: dmChannelUseCase,
            fileUseCase: fileUseCase,
            guildUseCase: guildUseCase,
            searchUseCase: searchUseCase,
            sessionUseCase: sessionUseCase,
            streamChannelUseCase: streamChannelUseCase,
            streamUseCase: streamUseCase,
            textChannelUseCase: textChannelUseCase,
            tagUseCase: tagUseCase,
            notificationUseCase: notificationUseCase,
            messageUseCase: messageUseCase,
            memberUseCase: memberUseCase,
            userUseCase: userUseCase,
            roleUseCase: roleUseCase
        )
    }
}
