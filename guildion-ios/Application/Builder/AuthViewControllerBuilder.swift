//
//  AuthViewControllerBuilder.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit

class AuthViewControllerBuilder: ViewControllerBuilder {
    typealias ViewControllerType = AuthViewController
    
    static func build() -> ViewControllerType {
        let viewController = AuthViewController()
        let wireframe = AuthWireframe(viewController: viewController),
        authPresenter = AuthViewControllerBuilder.build_Presenter(wireframe: wireframe)
        viewController.presenter = authPresenter
        return viewController
    }
    
    static func rebuild(_ viewController: ViewControllerType) {
        guard viewController.presenter == nil else { return }
        let wireframe = AuthWireframe(viewController: viewController),
        authPresenter = AuthViewControllerBuilder.build_Presenter(wireframe: wireframe)
        viewController.presenter = authPresenter
    }
    
    static func build_Presenter(
        wireframe: AuthWireframe,
        authUseCase: AuthUseCase = build_AuthUseCase(),
        categoryUseCase: CategoryUseCase = build_CategoryUseCase(),
        folderUseCase: FolderUseCase = build_FolderUseCase(),
        dmChannelUseCase: DMChannelUseCase = build_DMChannelUseCase(),
        fileUseCase: FileUseCase = build_FileUseCase(),
        guildUseCase: GuildUseCase = build_GuildUseCase(),
        searchUseCase: SearchUseCase = build_SearchUseCase(),
        sessionUseCase: SessionUseCase = build_SessionUseCase(),
        streamChannelUseCase: StreamChannelUseCase = build_StreamChannelUseCase(),
        streamUseCase: StreamUseCase = build_StreamUseCase(),
        textChannelUseCase: TextChannelUseCase = build_TextChannelUseCase(),
        tagUseCase: TagUseCase = build_TagUseCase(),
        notificationUseCase: NotificationUseCase = build_NotificationUseCase(),
        messageUseCase: MessageUseCase = build_MessageUseCase(),
        memberUseCase: MemberUseCase = build_MemberUseCase(),
        userUseCase: UserUseCase = build_UserUseCase(),
        roleUseCase: RoleUseCase = build_RoleUseCase()
    ) -> AuthPresenter {
        return AuthPresenter(
            wireframe: wireframe,
            authUseCase: authUseCase,
            categoryUseCase: categoryUseCase,
            folderUseCase: folderUseCase,
            dmChannelUseCase: dmChannelUseCase,
            fileUseCase: fileUseCase,
            guildUseCase: guildUseCase,
            searchUseCase: searchUseCase,
            sessionUseCase: sessionUseCase,
            streamChannelUseCase: streamChannelUseCase,
            streamUseCase: streamUseCase,
            textChannelUseCase: textChannelUseCase,
            tagUseCase: tagUseCase,
            notificationUseCase: notificationUseCase,
            messageUseCase: messageUseCase,
            memberUseCase: memberUseCase,
            userUseCase: userUseCase,
            roleUseCase: roleUseCase
        )
    }
}
