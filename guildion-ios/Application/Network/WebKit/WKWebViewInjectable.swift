//
//  WKWebViewInjectable.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/24.
//

import Foundation
import UIKit
import WebKit

protocol WKWebViewInjectable: class {
    var webView: WKWebView { get }
    func injectCSS(_ css: String)
    func injectJS(_ js: String)
    func encodeStringTo64(fromString: String) -> String?
}

extension WKWebViewInjectable {
    func injectCSS(_ css: String) {
        let cssStyle = """
            javascript:(function() {
            var parent = document.getElementsByTagName('head').item(0);
            var style = document.createElement('style');
            style.type = 'text/css';
            style.innerHTML = window.atob('\(encodeStringTo64(fromString: css)!)');
            parent.appendChild(style)})()
        """
        let cssScript = WKUserScript(source: cssStyle, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        webView.configuration.userContentController.addUserScript(cssScript)
    }
    
    func injectJS(_ js: String) {
        let jsStyle = """
            javascript:(function() {
            var parent = document.getElementsByTagName('head').item(0);
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.innerHTML = window.atob('\(encodeStringTo64(fromString: js)!)');
            parent.appendChild(script)})()
        """
        let jsScript = WKUserScript(source: jsStyle, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        webView.configuration.userContentController.addUserScript(jsScript)
    }
    
    func encodeStringTo64(fromString: String) -> String? {
        let plainData = fromString.data(using: .utf8)
        return plainData?.base64EncodedString(options: [])
    }
}
