//
//  AssetsPicker.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/21.
//

import Foundation
import Alamofire
import RxSwift
import SwiftyJSON
import UIKit
import AWSCore
import AWSCognito
import AWSS3
import Photos
import Rswift
import Hydra
import AssetsPickerViewController
import PKHUD
import M13ProgressSuite

protocol AssetsPickerHandlerDelegate: class {
    var viewController: UIViewController { get }
    func updateAssets(_ assets: [PHAsset])
    func updateAssets(_ files: FileEntities)
    func cancelAssets()
}

extension AssetsPickerHandlerDelegate {
    func updateAssets(_ assets: [PHAsset]) {}
    func updateAssets(_ files: FileEntities) {}
}

extension AssetsPickerHandlerDelegate where Self: UIViewController {
}

class AssetsPickerHandler: NSObject {
    lazy var imageManager = {
        return PHCachingImageManager()
    }()
    public var assetsPickerVC: AssetsPickerViewController?
    public var assetsAlbumVC: AssetsAlbumViewController?
    public var progressAlertHandler: ProgressAlertHandler?
    public weak var delegate: AssetsPickerHandlerDelegate!
    public var allowImages: Bool = true
    
    func openImagePicker() {
        let config = AssetsPickerConfig()
        let pickOptions = PHFetchOptions();
        if !allowImages {
            pickOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue);
            config.assetFetchOptions = [
                .album: pickOptions,
                .smartAlbum: pickOptions,
            ]
            config.assetIsShowCameraButton = false
        }
        self.assetsPickerVC = AssetsPickerViewController()
        self.assetsAlbumVC = AssetsAlbumViewController(pickerConfig: config)
        self.assetsPickerVC?.pickerConfig = config
        self.assetsPickerVC?.pickerDelegate = self
        self.progressAlertHandler = ProgressAlertHandler()
        self.progressAlertHandler?.delegate = self.assetsPickerVC?.photoViewController
        UIViewController.topMostController().present(assetsPickerVC!, animated: true, completion: nil)
    }
    
    static func generateImageURL(_ uploadImage: UIImage) -> URL {
        let imageURL = URL(fileURLWithPath: NSTemporaryDirectory().appendingFormat(UUID().uuidString))
        if let jpegData = uploadImage.jpegData(compressionQuality: 80) {
            try! jpegData.write(to: imageURL, options: [.atomicWrite])
        }
        return imageURL
    }
    
    static func getThumnailImage(videoURL: URL) -> UIImage {
        let asset = AVURLAsset(url: videoURL, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let uiImage = UIImage(cgImage: cgImage)
            return uiImage
        } catch {
            return UIImage()
        }
    }
    
    fileprivate func fetchIntoFileEntity(selected assets: [PHAsset]) -> Promise<FileEntities> {
        return Promise<FileEntities> { resolve, reject, _ in
            let entities = FileEntities()
            for enumerater in assets.enumerated() {
                guard !(self.progressAlertHandler?.canceled ?? false) else { resolve(FileEntities()); return }
                let file = FileEntity()
                switch enumerater.element.mediaType {
                case .image:
                    do {
                        let image = try await(in: .background, self.requrestImage(asset: enumerater.element))
                        guard !(self.progressAlertHandler?.canceled ?? false) else { resolve(FileEntities()); return }
                        let url = AssetsPickerHandler.generateImageURL(image)
                        file.url = String(describing: url)
                        file.url_image = image
                        file.thumbnail = String(describing: url)
                        file.pixel_width = Double(enumerater.element.pixelWidth)
                        file.pixel_height = Double(enumerater.element.pixelHeight)
                        file.size_byte = Double(url.fileSize)
                        file.format = FileFormat.init(extention: enumerater.element.originalFilename.fileExtension) ?? .jpg
                        if file.format == .other {
                            file.format = .jpg
                        }
                        file.name = enumerater.element.originalFilename
                    } catch {
                        break
                    }
                case .video:
                    do {
                        let url = try await(in: .background, self.requrestVideoURL(asset: enumerater.element))
                        guard !(self.progressAlertHandler?.canceled ?? false) else { resolve(FileEntities()); return }
                        let thumbnail = AssetsPickerHandler.getThumnailImage(videoURL: url)
                        file.url = "\(url)"
                        file.thumbnail = "\(AssetsPickerHandler.generateImageURL(thumbnail))"
                        file.url_image = thumbnail
                        file.pixel_width = Double(enumerater.element.pixelWidth)
                        file.pixel_height = Double(enumerater.element.pixelHeight)
                        file.size_byte = Double(url.fileSize)
                        file.duration = (enumerater.element.duration) * 1000
                        file.format = FileFormat.init(extention: url.absoluteURL.pathExtension) ?? .mov
                        if file.format == .other {
                            file.format = .mov
                        }
                        file.name = enumerater.element.originalFilename
                    } catch {
                        break
                    }
                default: break
                }
                if let current_member = GuildConnector.shared.current_member {
                    file.Member = current_member
                    file.MemberId = current_member.id
                }
                entities.append(file)
                DispatchQueue.main.async {
                    self.progressAlertHandler?.setTaskProgress(completed: enumerater.offset + 1, of: assets.count)
                }
            }
            entities.items = entities.items.filter({ $0.format != .other })
            resolve(entities)
        }
    }
    
    func requrestImage(asset: PHAsset) -> Promise<UIImage> {
        return Promise<UIImage> { [unowned self] resolve, reject, _ in
            guard asset.mediaType == .image else { reject(AppError.unknown); return }
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isNetworkAccessAllowed = true
            options.isSynchronous = true
            imageManager.requestImage(for: asset, targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight), contentMode: .aspectFill, options: options) { (image, info) in
                guard let unwrapped = image else { reject(AppError.unknown); return }
                resolve(unwrapped)
            }
        }
    }
    
    func requrestVideoURL(asset: PHAsset) -> Promise<URL> {
        return Promise<URL> { resolve, reject, _ in
            guard asset.mediaType == .video else { reject(AppError.unknown); return }
            let options = PHVideoRequestOptions()
            options.version = .original
            options.deliveryMode = .highQualityFormat
            options.isNetworkAccessAllowed = true
            PHImageManager.default().requestAVAsset(forVideo: asset, options: options) { (asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) in
                if let unwrapped = asset as? AVURLAsset {
                    resolve(unwrapped.url)
                }
                DispatchQueue.main.async {
                    HUD.hide()
                }
            }
        }
    }
}

extension AssetsPickerHandler: AssetsPickerViewControllerDelegate {
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {
        self.delegate.cancelAssets()
        self.assetsPickerVC = nil
        self.assetsAlbumVC = nil
    }
    
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        self.delegate.updateAssets(assets)
        self.progressAlertHandler?.present(completion: {
            self.progressAlertHandler?.setTaskProgress(completed: 0, of: assets.count)
            let ph = self.progressAlertHandler
            self.fetchIntoFileEntity(selected: assets).then({ entities in
                guard self.delegate != nil, !(ph?.canceled ?? false) else { return }
                self.progressAlertHandler?.alertController?.dismiss(animated: true, completion: {
                    self.assetsPickerVC?.dismiss(animated: true, completion: {
                        self.progressAlertHandler = nil
                        self.assetsPickerVC = nil
                        self.assetsAlbumVC = nil
                        self.delegate.updateAssets(entities)
                    })
                })
            }).catch({ _ in
                self.progressAlertHandler?.alertController?.dismiss(animated: true, completion: nil)
                self.progressAlertHandler = nil
                self.assetsPickerVC = nil
                self.assetsAlbumVC = nil
                HUD.flash(.error)
            })
        })
    }
    
    func assetsPicker(controller: AssetsPickerViewController, didDismissByCancelling byCancel: Bool) {
        if byCancel {
            self.delegate.cancelAssets()
        }
        self.assetsPickerVC = nil
        self.assetsAlbumVC = nil
    }
}

extension AssetsPhotoViewController: ProgressAlertHandlerDelegate {
    func progressAlertHandler(onCancel progressAlertHandler: ProgressAlertHandler) {
    }
}
