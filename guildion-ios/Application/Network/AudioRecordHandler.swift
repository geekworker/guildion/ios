//
//  AudioRecordHandler.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import UIKit
import AVFoundation

class AudioRecordHandler: NSObject {
    var audioRecorder: AVAudioRecorder!
    var audioEngine: AVAudioEngine!
    var audioFile: AVAudioFile!
    var audioPlayerNode: AVAudioPlayerNode!
    static let shared: AudioRecordHandler = AudioRecordHandler()
    
    func setup() {
        do {
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 2,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ]

            audioRecorder = try AVAudioRecorder(url: getAudioFileUrl(), settings: settings)
            audioRecorder.delegate = self
            
        } catch let error {
            print(error)
        }
    }
    
    func getAudioFileUrl() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDirect = paths[0]
        let audioUrl = docsDirect.appendingPathComponent("recording.m4a")

        return audioUrl
    }
    
    func record() {
        if audioRecorder != nil, !audioRecorder.isRecording {
            audioRecorder.record()
        }
    }
    
    func recordForce() {
        guard audioRecorder != nil else { return }
        audioRecorder.stop()
        audioRecorder.record()
    }
    
    func stop() {
        if audioRecorder != nil, audioRecorder.isRecording {
            audioRecorder.stop()
        }
    }
    
    func stopForce() {
        guard audioRecorder != nil else { return }
        audioRecorder.stop()
    }
}

extension AudioRecordHandler: AVAudioRecorderDelegate {
}
