//
//  API.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import Foundation
import RxSwift
import Hydra

protocol BaseAPIProtocol {
    associatedtype ResponseType
    var method: HTTPMethod { get }
    var baseURL: URL { get }
    var path: String { get }
    var headers: HTTPHeaders? { get }
}

extension BaseAPIProtocol {
    var baseURL: URL {
        return try! Constant.APP_URL.asURL()
    }
    var headers: [String: String]? {
        return nil
    }
}

protocol BaseRequestProtocol: BaseAPIProtocol, URLRequestConvertible {
    var parameters: Parameters? { get }
    var encoding: URLEncoding { get }
}

extension BaseRequestProtocol {
    var encoding: URLEncoding {
        // MEMO: in default, get→quertString、post→httpBody
        return URLEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        for (key, value) in headers?.dictionary ?? [:] {
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        urlRequest.timeoutInterval = TimeInterval(30)
        if let params = parameters {
            urlRequest = try encoding.encode(urlRequest, with: params.merging(ENV.API.JSON) { $1 })
        }
        return urlRequest
    }
    
}

struct APIManager {
    
    private static let successRange = 200..<300
    private static let contentType = ["application/json"]
    
    static func call<T, V>(
        _ request: T,
        _ disposeBag: DisposeBag,
        onNext: @escaping (V) -> Void,
        onError: @escaping (Error) -> Void
    )
        where T : BaseRequestProtocol, V == T.ResponseType, T.ResponseType : Mappable {
            _ = observe(request)
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { onNext($0) },
                           onError: { onError($0) })
                .disposed(by: disposeBag)
    }
    
    static func observe<T, V>(_ request: T) -> Observable<V>
        where T: BaseRequestProtocol, V: Mappable, T.ResponseType == V {
            return Observable<V>.create { observer in
                let calling = callForJson(request) { response in
                    switch response {
                    case .success(let result): observer.onNext(result as! V)
                    case .failure(let error): observer.onError(error)
                    }
                    observer.onCompleted()
                }
                return Disposables.create() { calling.cancel() }
            }
    }
    
    static func callForJson<T, V>(
        _ request: T,
        completion: @escaping (APIResult) -> Void
    ) -> DataRequest
        where T: BaseRequestProtocol, V: Mappable, T.ResponseType == V {
            return customAlamofire(request).responseJSON { response in
                switch response.result {
                case .success(let result): completion(.success(mappingJson(request, result as! Parameters)))
                case .failure(let error):
                    if let unwrappd = response.data, let reason = String(data: unwrappd, encoding: .utf8) {
                        completion(.failure(AppError.custom(reason: reason)))
                    } else {
                        completion(.failure(error))
                    }
                }
            }
    }
    
    static func customAlamofire<T>(_ request: T) -> DataRequest
        where T: BaseRequestProtocol {
            
            return AF
                .request(request)
                .validate(statusCode: successRange)
                .validate(contentType: contentType)
    }
    
    static func mappingJson<T, V>(_ request: T, _ result: Parameters) -> V
        where T: BaseRequestProtocol, V: Mappable, T.ResponseType == V {
            return Mapper<V>().map(JSON: result)!
    }
    
}

enum APIResult {
    case success(Mappable)
    case failure(Error)
}

protocol BaseResponseProtocol: Mappable {
    init?(map: Map)
    mutating func mapping(map: Map)
    var result: Bool? { get }
    var success: Bool? { get }
    var data: Map? { get }
}

