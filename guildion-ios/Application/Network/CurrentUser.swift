//
//  CurrentUser.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit

class CurrentUser {

    let userDefaults = UserDefaults.standard

    static let prefix = "current_user:/"
    
    fileprivate static var shared: CurrentUser?

    public var id: Int?
    public var username: String = ""
    public var uid: String = ""
    public var nickname: String = ""
    public var description: String = ""
    public var picture_small: String = ""
    public var picture_large: String = ""
    public var locale: String = ""
    public var timezone: String = ""
    public var verified: Bool = false
    public var admin: Bool = false
    public var size_byte: Double = 0
    public var is_private: Bool = false
    public var permission: Bool = false
    
    init() {}

    init(
        id: Int?,
        username: String,
        uid: String,
        nickname: String,
        description: String,
        picture_small: String,
        picture_large: String,
        locale: String,
        timezone: String,
        verified: Bool,
        admin: Bool,
        size_byte: Double,
        is_private: Bool,
        permission: Bool
    ) {
        self.id = id
        self.username = username
        self.uid = uid
        self.nickname = nickname
        self.description = description
        self.picture_small = picture_small
        self.picture_large = picture_large
        self.locale = locale
        self.timezone = timezone
        self.verified = verified
        self.admin = admin
        self.size_byte = size_byte
        self.is_private = is_private
        self.permission = permission
    }

    static func getCurrentUser() -> CurrentUser? {
        let userDefaults = UserDefaults.standard
        if let unwrapped = shared {
            return unwrapped
        } else {
            guard let username = userDefaults.string(forKey: "\(CurrentUser.prefix)/username") else {
                return nil
            }
            shared = CurrentUser.init(
                id: userDefaults.integer(forKey: "\(CurrentUser.prefix)/id"),
                username: username,
                uid: userDefaults.string(forKey: "\(CurrentUser.prefix)/uid")!,
                nickname: userDefaults.string(forKey: "\(CurrentUser.prefix)/nickname")!,
                description: userDefaults.string(forKey: "\(CurrentUser.prefix)/description")!,
                picture_small: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_small")!,
                picture_large: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_large")!,
                locale: userDefaults.string(forKey: "\(CurrentUser.prefix)/locale")!,
                timezone: userDefaults.string(forKey: "\(CurrentUser.prefix)/timezone")!,
                verified: userDefaults.bool(forKey: "\(CurrentUser.prefix)/verified"),
                admin: userDefaults.bool(forKey: "\(CurrentUser.prefix)/admin"),
                size_byte: userDefaults.double(forKey: "\(CurrentUser.prefix)/size_byte"),
                is_private: userDefaults.bool(forKey: "\(CurrentUser.prefix)/is_private"),
                permission: userDefaults.bool(forKey: "\(CurrentUser.prefix)/permission")
            )
            return CurrentUser.init(
                id: userDefaults.integer(forKey: "\(CurrentUser.prefix)/id"),
                username: username,
                uid: userDefaults.string(forKey: "\(CurrentUser.prefix)/uid")!,
                nickname: userDefaults.string(forKey: "\(CurrentUser.prefix)/nickname")!,
                description: userDefaults.string(forKey: "\(CurrentUser.prefix)/description")!,
                picture_small: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_small")!,
                picture_large: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_large")!,
                locale: userDefaults.string(forKey: "\(CurrentUser.prefix)/locale")!,
                timezone: userDefaults.string(forKey: "\(CurrentUser.prefix)/timezone")!,
                verified: userDefaults.bool(forKey: "\(CurrentUser.prefix)/verified"),
                admin: userDefaults.bool(forKey: "\(CurrentUser.prefix)/admin"),
                size_byte: userDefaults.double(forKey: "\(CurrentUser.prefix)/size_byte"),
                is_private: userDefaults.bool(forKey: "\(CurrentUser.prefix)/is_private"),
                permission: userDefaults.bool(forKey: "\(CurrentUser.prefix)/permission")
            )
        }
    }

    static func getCurrentUserEntity() -> UserEntity? {
        let userDefaults = UserDefaults.standard
        if let unwrapped = shared {
            return unwrapped.toUserEntity()
        } else {
            guard let username = userDefaults.string(forKey: "\(CurrentUser.prefix)/username") else {
                return nil
            }
            shared = CurrentUser.init(
                id: userDefaults.integer(forKey: "\(CurrentUser.prefix)/id"),
                username: username,
                uid: userDefaults.string(forKey: "\(CurrentUser.prefix)/uid")!,
                nickname: userDefaults.string(forKey: "\(CurrentUser.prefix)/nickname")!,
                description: userDefaults.string(forKey: "\(CurrentUser.prefix)/description")!,
                picture_small: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_small")!,
                picture_large: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_large")!,
                locale: userDefaults.string(forKey: "\(CurrentUser.prefix)/locale")!,
                timezone: userDefaults.string(forKey: "\(CurrentUser.prefix)/timezone")!,
                verified: userDefaults.bool(forKey: "\(CurrentUser.prefix)/verified"),
                admin: userDefaults.bool(forKey: "\(CurrentUser.prefix)/admin"),
                size_byte: userDefaults.double(forKey: "\(CurrentUser.prefix)/size_byte"),
                is_private: userDefaults.bool(forKey: "\(CurrentUser.prefix)/is_private"),
                permission: userDefaults.bool(forKey: "\(CurrentUser.prefix)/permission")
            )
            return UserEntity.init(
                id: userDefaults.integer(forKey: "\(CurrentUser.prefix)/id"),
                username: username,
                uid: userDefaults.string(forKey: "\(CurrentUser.prefix)/uid")!,
                nickname: userDefaults.string(forKey: "\(CurrentUser.prefix)/nickname")!,
                description: userDefaults.string(forKey: "\(CurrentUser.prefix)/description")!,
                picture_small: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_small")!,
                picture_large: userDefaults.string(forKey: "\(CurrentUser.prefix)/picture_large")!,
                locale: userDefaults.string(forKey: "\(CurrentUser.prefix)/locale")!,
                timezone: userDefaults.string(forKey: "\(CurrentUser.prefix)/timezone")!,
                verified: userDefaults.bool(forKey: "\(CurrentUser.prefix)/verified"),
                admin: userDefaults.bool(forKey: "\(CurrentUser.prefix)/admin"),
                size_byte: userDefaults.double(forKey: "\(CurrentUser.prefix)/size_byte"),
                is_private: userDefaults.bool(forKey: "\(CurrentUser.prefix)/is_private"),
                permission: userDefaults.bool(forKey: "\(CurrentUser.prefix)/permission")
            )
        }
    }

    static func logout() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/id")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/username")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/uid")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/nickname")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/description")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/picture_small")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/picture_large")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/locale")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/timezone")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/verified")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/admin")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/size_byte")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/is_private")
        userDefaults.removeObject(forKey: "\(CurrentUser.prefix)/permission")
        shared = nil
        userDefaults.synchronize()
    }
    
    func toUserEntity() -> UserEntity? {
        return UserEntity.init(
            id: self.id ?? 0,
            username: self.username,
            uid: self.uid,
            nickname: self.nickname,
            description: self.description,
            picture_small: self.picture_small,
            picture_large: self.picture_large,
            locale: self.locale,
            timezone: self.timezone,
            verified: self.verified,
            admin: self.admin,
            size_byte: self.size_byte,
            is_private: self.is_private,
            permission: self.permission
        )
    }
}

extension CurrentUser: StringDefaultSettable {
    enum StringKey : String {
        case id
        case username
        case uid
        case nickname
        case description
        case picture_small
        case picture_large
        case locale
        case timezone
        case verified
        case admin
        case size_byte
        case is_private
        case permission

        case player_id
    }
}

protocol CurrentUserProtocol: class {
    func setCurrentUser(entity: UserEntity)
}

extension CurrentUserProtocol {
    func setCurrentUser(entity: UserEntity) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(entity.id, forKey: "\(CurrentUser.prefix)/id")
        userDefaults.set(entity.username, forKey: "\(CurrentUser.prefix)/username")
        userDefaults.set(entity.uid, forKey: "\(CurrentUser.prefix)/uid")
        userDefaults.set(entity.nickname, forKey: "\(CurrentUser.prefix)/nickname")
        userDefaults.set(entity.description, forKey: "\(CurrentUser.prefix)/description")
        userDefaults.set(entity.picture_small, forKey: "\(CurrentUser.prefix)/picture_small")
        userDefaults.set(entity.picture_large, forKey: "\(CurrentUser.prefix)/picture_large")
        userDefaults.set(entity.locale, forKey: "\(CurrentUser.prefix)/locale")
        userDefaults.set(entity.timezone, forKey: "\(CurrentUser.prefix)/timezone")
        userDefaults.set(entity.verified, forKey: "\(CurrentUser.prefix)/verified")
        userDefaults.set(entity.admin, forKey: "\(CurrentUser.prefix)/admin")
        userDefaults.set(entity.size_byte, forKey: "\(CurrentUser.prefix)/size_byte")
        userDefaults.set(entity.is_private, forKey: "\(CurrentUser.prefix)/is_private")
        userDefaults.set(entity.permission, forKey: "\(CurrentUser.prefix)/permission")
        
        if CurrentUser.shared == nil {
            CurrentUser.shared = CurrentUser()
        }
        CurrentUser.shared?.id = entity.id
        CurrentUser.shared?.username = entity.username
        CurrentUser.shared?.uid = entity.uid
        CurrentUser.shared?.nickname = entity.nickname
        CurrentUser.shared?.description = entity.description
        CurrentUser.shared?.picture_small = entity.picture_small
        CurrentUser.shared?.picture_large = entity.picture_large
        CurrentUser.shared?.locale = entity.locale
        CurrentUser.shared?.timezone = entity.timezone
        CurrentUser.shared?.verified = entity.verified
        CurrentUser.shared?.admin = entity.admin
        CurrentUser.shared?.size_byte = entity.size_byte
        CurrentUser.shared?.is_private = entity.is_private
        CurrentUser.shared?.permission = entity.permission
        
        userDefaults.synchronize()
    }
}
