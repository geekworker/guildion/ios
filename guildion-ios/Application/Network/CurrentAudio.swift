//
//  CurrentAudio.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/21.
//

import Foundation
import AVFoundation

class CurrentAudio {

    let userDefaults = UserDefaults.standard

    static let prefix = "current_audio:/"
    
    static var shared: CurrentAudio?

    public var stream_channel_output_volume: Float = 1
    public var stream_channel_input_volume: Float = 1
    public var stream_channel_participants_volume: Float = 1
    
    init() {}

    init(
        stream_channel_output_volume: Float,
        stream_channel_input_volume: Float,
        stream_channel_participants_volume: Float
    ) {
        self.stream_channel_output_volume = stream_channel_output_volume
        self.stream_channel_input_volume = stream_channel_input_volume
        self.stream_channel_participants_volume = stream_channel_participants_volume
    }

    static func getCurrentAudio() -> CurrentAudio? {
        let userDefaults = UserDefaults.standard
        if let unwrapped = shared {
            return unwrapped
        } else {
            shared = CurrentAudio(
                stream_channel_output_volume: userDefaults.float(forKey: "\(CurrentAudio.prefix)/stream_channel_output_volume"),
                stream_channel_input_volume: userDefaults.float(forKey: "\(CurrentAudio.prefix)/stream_channel_input_volume"),
                stream_channel_participants_volume: userDefaults.float(forKey: "\(CurrentAudio.prefix)/stream_channel_participants_volume")
            )
            return shared
        }
    }

    static func logout() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "\(CurrentAudio.prefix)/stream_channel_output_volume")
        userDefaults.removeObject(forKey: "\(CurrentAudio.prefix)/stream_channel_input_volume")
        userDefaults.removeObject(forKey: "\(CurrentAudio.prefix)/stream_channel_participants_volume")
        userDefaults.set(1, forKey: "\(CurrentAudio.prefix)/stream_channel_output_volume")
        userDefaults.set(1, forKey: "\(CurrentAudio.prefix)/stream_channel_input_volume")
        userDefaults.set(1, forKey: "\(CurrentAudio.prefix)/stream_channel_participants_volume")
        shared = nil
        userDefaults.synchronize()
    }
}

extension CurrentAudio: StringDefaultSettable {
    enum StringKey : String {
        case stream_channel_output_volume
        case stream_channel_input_volume
        case stream_channel_participants_volume
    }
}

protocol CurrentAudioProtocol: class {
    func setCurrentAudio(
        stream_channel_output_volume: Float,
        stream_channel_input_volume: Float,
        stream_channel_participants_volume: Float
    )
}

extension CurrentAudioProtocol {
    func setCurrentAudio(
        stream_channel_output_volume: Float,
        stream_channel_input_volume: Float,
        stream_channel_participants_volume: Float
    ) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(stream_channel_output_volume, forKey: "\(CurrentAudio.prefix)/stream_channel_output_volume")
        userDefaults.set(stream_channel_input_volume, forKey: "\(CurrentAudio.prefix)/stream_channel_input_volume")
        userDefaults.set(stream_channel_participants_volume, forKey: "\(CurrentAudio.prefix)/stream_channel_participants_volume")
        
        if CurrentAudio.shared == nil {
            CurrentAudio.shared = CurrentAudio()
        }
        CurrentAudio.shared?.stream_channel_output_volume = stream_channel_output_volume
        CurrentAudio.shared?.stream_channel_input_volume = stream_channel_input_volume
        CurrentAudio.shared?.stream_channel_participants_volume = stream_channel_participants_volume
        
        userDefaults.synchronize()
    }
}
