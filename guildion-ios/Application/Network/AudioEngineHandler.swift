//
//  AudioEngineHandler.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/19.
//

import Foundation
import AVKit
import AVFoundation

protocol AudioEngineHandlerDelegate: class {
    func audioEngineHandler(didInput meterLevel: Float, in audioEngineHandler: AudioEngineHandler)
}

class AudioEngineHandler {
    var delegates: [AudioEngineHandlerDelegate] = []
    static let shared: AudioEngineHandler = AudioEngineHandler()
    static let BUFFER_SIZE: AVAudioFrameCount = 4096
    static let MIN_DB: Float = -80
    var audioEngine: AVAudioEngine = AVAudioEngine()
    
    @inlinable public func inject(_ implementer: AudioEngineHandlerDelegate) {
        self.delegates.append(implementer)
    }
    
    @inlinable public func reject<T>(_ implementer: T) where T: Equatable {
        self.delegates = self.delegates.filter({
            if let unwrapped = $0 as? T, implementer == unwrapped {
                return false
            } else {
                return true
            }
        })
    }

    @inlinable public func invoke(_ delegater: (AudioEngineHandlerDelegate) -> ()) {
        for delegate in delegates {
            delegater(delegate)
        }
    }
    
    public func isRecording() -> Bool {
        return audioEngine.isRunning
    }
    
    func record(errorHandler: ((_ error: Error) -> ())? = nil) {
        do {
            self.stopRecording()
            self.audioEngine = AVAudioEngine()
            let node = audioEngine.inputNode
            let recordingFormat = node.outputFormat(forBus: 0)
            var installing_count = 0
            node.installTap(
                onBus: 0,
                bufferSize: AudioEngineHandler.BUFFER_SIZE,
                format: recordingFormat
            ) { [weak self] (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
                guard let strongSelf = self else { return }
                installing_count += 1
                guard installing_count >= 5 else { return }
                installing_count = 0
                guard let channelData = buffer.floatChannelData else { return }
                let channelDataValue = channelData.pointee
                let channelDataValueArray = stride(from: 0, to: Int(buffer.frameLength), by: buffer.stride).map{
                    channelDataValue[$0]
                }
                let rms = sqrt(channelDataValueArray.map{ $0 * $0 }.reduce(0, +) / Float(buffer.frameLength))
                let avgPower = 20 * log10(rms)
                let meterLevel = strongSelf.scaledPower(power: avgPower)
                DispatchQueue.main.async {
                    strongSelf.invoke({ $0.audioEngineHandler(didInput: meterLevel, in: strongSelf) })
                }
            }
            audioEngine.prepare()
            try audioEngine.start()
        } catch let err {
            errorHandler?(err)
            return
        }
    }
    
    func scaledPower(power: Float) -> Float {
        guard power.isFinite else { return 0.0 }
        if power < AudioEngineHandler.MIN_DB {
            return 0.0
        } else if power >= 1.0 {
            return 1.0
        } else {
            return (abs(AudioEngineHandler.MIN_DB) - abs(power)) / abs(AudioEngineHandler.MIN_DB)
        }
    }
    
    public func stopRecording() {
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
    }
    
//    func levelTimerCallback() {
//        audioEngine.updateMeters()
//
//        //print to the console if we are beyond a threshold value. Here I've used -7
//        if recorder.averagePowerForChannel(0) > -7 {
//            print("Dis be da level I'm hearin' you in dat mic ")
//            println(recorder.averagePowerForChannel(0))
//            println("Do the thing I want, mofo")
//        }
//    }
}
