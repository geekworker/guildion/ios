//
//  WebRTCEvent.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import UIKit
import AVFoundation
import WebRTC
import Starscream
import SwiftyJSON
import VideoToolbox

enum WebRTCEvent {
    case talkRequest(id: String, user: UserEntity)
    case offer(id: String, receiver_id: String, offer: RTCSessionDescription, user: UserEntity)
    case answer(answer: RTCSessionDescription, id: String, receiver_id: String, user: UserEntity)
    case candidate(candidate: RTCIceCandidate, id: String, receiver_id: String, user: UserEntity)
    case meterLevelChange(id: String, meter_level: Float, user: UserEntity)
    case close(id: String, user: UserEntity)
    case other
    
    init(
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String,
        sdp: String,
        ice: JSON
    ) {
        switch SDEventType.init(type) {
        case .talkRequest:
            if payload[WebRTCPayloadType.receiver_id.rawValue].string != nil {
                self = .other
            } else {
                self = .talkRequest(id: id, user: client_user)
            }
            break
            
        case .offer:
            let offer = RTCSessionDescription(
                type: RTCSessionDescription.type(for: type),
                sdp: sdp
            )
            self = .offer(id: id, receiver_id: payload[WebRTCPayloadType.receiver_id.rawValue].stringValue, offer: offer, user: client_user)
            break
        case .answer:
            let answer = RTCSessionDescription(
                type: RTCSessionDescription.type(for: type),
                sdp: sdp
            )
            self = .answer(answer: answer, id: id, receiver_id: payload[WebRTCPayloadType.receiver_id.rawValue].stringValue, user: client_user)
            break
        case .candidate:
            let candidate = RTCIceCandidate(
                sdp: ice["candidate"].stringValue,
                sdpMLineIndex: ice["sdpMLineIndex"].int32Value,
                sdpMid: ice["sdpMid"].stringValue
            )
            self = .candidate(candidate: candidate, id: id, receiver_id: payload[WebRTCPayloadType.receiver_id.rawValue].stringValue, user: client_user)
            break
        case .meterLevelChange:
            self = .meterLevelChange(id: id, meter_level: payload[WebRTCPayloadType.meterLevel.rawValue].floatValue, user: client_user)
            break
        case .close:
            self = .close(id: id, user: client_user)
            break
        default:
            self = .other
            break
        }
    }
}
