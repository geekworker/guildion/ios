//
//  WebRTCPayloadType.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation

enum WebRTCPayloadType: String {
    case receiver_id
    case meterLevel
}
