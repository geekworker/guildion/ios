//
//  SessionDescription.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import WebRTC
import UIKit
import SwiftyJSON

public enum SDEventType: String {
    case talkRequest = "talkRequest"
    case offer = "offer"
    case answer = "answer"
    case candidate = "candidate"
    case meterLevelChange = "meterLevelChange"
    case close = "close"
    case other
    
    init(_ string: String) {
        switch string {
            case SDEventType.talkRequest.rawValue: self = .talkRequest
            case SDEventType.offer.rawValue: self = .offer
            case SDEventType.answer.rawValue: self = .answer
            case SDEventType.candidate.rawValue: self = .candidate
            case SDEventType.meterLevelChange.rawValue: self = .meterLevelChange
            case SDEventType.close.rawValue: self = .close
            default: self = .other
        }
    }
}

public enum SDMessageDispatcher {
    case talkRequest(id: String)
    case sdp(id: String, receiver_id: String, desc: RTCSessionDescription)
    case meterLevelChange(id: String, meterLevel: Float)
    case sendIceCandidate(id: String, receiver_id: String, candidate: RTCIceCandidate)
    case close(id: String)
    
    var json: JSON {
        switch self {
        case .talkRequest(let id):
            return [
                WebSocketMessageType.type.rawValue: SDEventType.talkRequest.rawValue,
                WebSocketMessageType.id.rawValue: id
            ]
        case .sdp(let id, let receiver_id, let desc):
            return [
                WebSocketMessageType.id.rawValue: id,
                WebSocketMessageType.sdp.rawValue: desc.sdp,
                WebSocketMessageType.type.rawValue: RTCSessionDescription.string(for: desc.type),
                WebSocketMessageType.payload.rawValue: [
                    WebRTCPayloadType.receiver_id.rawValue: receiver_id,
                ]
            ]
        case .meterLevelChange(let id, let meterLevel):
            return [
                WebSocketMessageType.type.rawValue: SDEventType.meterLevelChange.rawValue,
                WebSocketMessageType.id.rawValue: id,
                WebSocketMessageType.payload.rawValue: [
                    WebRTCPayloadType.meterLevel.rawValue: meterLevel,
                ]
            ]
        case .close(let id):
            return [
                WebSocketMessageType.type.rawValue: SDEventType.close.rawValue,
                WebSocketMessageType.id.rawValue: id
            ]
        case .sendIceCandidate(let id, let receiver_id, let candidate):
            return [
                WebSocketMessageType.type.rawValue: SDEventType.candidate.rawValue,
                WebSocketMessageType.ice.rawValue: [
                    "candidate": candidate.sdp,
                    "sdpMLineIndex": candidate.sdpMLineIndex,
                    "sdpMid": candidate.sdpMid!
                ],
                WebSocketMessageType.id.rawValue: id,
                WebSocketMessageType.payload.rawValue: [
                    WebRTCPayloadType.receiver_id.rawValue: receiver_id,
                ]
            ]
        }
    }
    
    var jsonString: String {
        if let string = self.json.rawString() {
            return string
        } else {
            return ""
        }
    }
}
