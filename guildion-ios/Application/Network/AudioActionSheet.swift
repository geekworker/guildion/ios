//
//  AudioActionSheet.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import UIKit
import AVFoundation
import AVKit

protocol AudioActionSheetProtocol: class {
    func audioActionSheetShow()
    func audioActionSheetCancel()
    func setPreferredInput(to: AVAudioSessionPortDescription)
}

extension AudioActionSheetProtocol {
    func setPreferredInput(to: AVAudioSessionPortDescription) {
        do {
            try AVAudioSession.sharedInstance().setPreferredInput(to)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
    }
    
    func initializeInput() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            // try AVAudioSession.sharedInstance().setCategory(.playAndRecord, options: AVAudioSession.CategoryOptions.init(rawValue: 0))
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .voiceChat, options: [.defaultToSpeaker, .allowBluetooth, .mixWithOthers])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
        }
    }
    
    func initializePlayerInput() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            try AVAudioSession.sharedInstance().setCategory(.playback, options: [.defaultToSpeaker, .mixWithOthers])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
        }
    }
    
    func audioActionSheetShow() {}
    func audioActionSheetCancel() {}
    
    func audioImage(port: AVAudioSession.Port, resized: Bool = false) -> UIImage {
        switch port {
        case .builtInMic:
            return resized ? R.image.phone()!.withRenderingMode(.alwaysTemplate).resizeSizeFit(height: 30) : R.image.phone()!.withRenderingMode(.alwaysTemplate)
        case .builtInMic:
            return resized ? R.image.systemSpeaker()!.withRenderingMode(.alwaysTemplate).resizeSizeFit(height: 20) : R.image.systemSpeaker()!.withRenderingMode(.alwaysTemplate)
        case .headsetMic:
            return resized ? R.image.headphone()!.withRenderingMode(.alwaysTemplate).resizeSizeFit(height: 20) : R.image.headphone()!.withRenderingMode(.alwaysTemplate)
        case .headphones:
            return resized ? R.image.headphone()!.withRenderingMode(.alwaysTemplate).resizeSizeFit(height: 20) : R.image.headphone()!.withRenderingMode(.alwaysTemplate)
        case .bluetoothLE, .bluetoothHFP, .bluetoothA2DP:
            return resized ? R.image.bluetoothSpeaker()!.withRenderingMode(.alwaysTemplate).resizeSizeFit(height: 20) : R.image.bluetoothSpeaker()!.withRenderingMode(.alwaysTemplate)
        default: return UIImage()
        }
    }
}


extension AudioActionSheetProtocol where Self: UIViewController {
    func audioActionSheetShow() {
        let alertController = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: .actionSheet
        )
        guard let inputs = AVAudioSession.sharedInstance().availableInputs else { return }
        let current_route = AVAudioSession.sharedInstance().currentRoute
        for audioPort in inputs {
            var image: UIImage? = nil
            let action = UIAlertAction(title: audioPort.portName, style: .default) { (action) in
                self.setPreferredInput(to: audioPort)
            }
            
            if audioPort.portType == current_route.inputs.first?.portType && audioPort.portName == current_route.inputs.first?.portName {
                action.setValue(true, forKey: "checked")
            }
            
            image = self.audioImage(port: audioPort.portType, resized: true)
            
            guard let unwrappedImage = image else { return }
            action.setValue(unwrappedImage, forKey: "image")
            alertController.addAction(action)
        }
        let cancelAction = UIAlertAction(title: R.string.localizable.cancel(), style: .cancel) { (action) in
            self.audioActionSheetCancel()
        }
        
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
