//
//  ProgressAlertHandler.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/08.
//

import Foundation
import UIKit

protocol ProgressAlertHandlerDelegate: UIViewController {
    func progressAlertHandler(onCancel progressAlertHandler: ProgressAlertHandler)
}

extension ProgressAlertHandlerDelegate {
    func progressAlertHandler(onCancel progressAlertHandler: ProgressAlertHandler) {}
}

class ProgressAlertHandler {
    weak var delegate: ProgressAlertHandlerDelegate?
    var progressView: UIProgressView?
    var alertController: UIAlertController?
    var canceled: Bool = false
    
    func present(completion: (() -> Void)? = nil) {
        self.canceled = false
        self.alertController = UIAlertController(title: R.string.localizable.donwloadingFromIcloud(), message: "0 / 0", preferredStyle: .alert)
        self.progressView = UIProgressView()
        self.progressView?.progressTintColor = Theme.themify(key: .main)
        self.progressView?.backgroundColor = Theme.themify(key: .transparent)
        self.progressView?.progress = 0.0
        self.alertController?.view.addSubview(self.progressView!)
        let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: .cancel, handler: { [unowned self] action in
            self.canceled = true
            self.delegate?.progressAlertHandler(onCancel: self)
        })
        self.alertController?.addAction(cancelAction)
        self.delegate?.present(self.alertController!, animated: true, completion: {
            self.progressView?.frame = CGRect(x: 0, y: self.alertController!.view.frame.height - 46, width: self.alertController!.view.frame.width, height: 1)
            completion?()
        })
    }
    
    func setTaskProgress(completed: Int, of count: Int) {
        self.alertController?.message = "\(completed) / \(count)"
        self.setAlertProgress(Float(completed) / Float(count))
    }
    
    func setAlertProgress(_ progress: Float) {
        self.progressView?.setProgress(progress, animated: true)
    }
}
