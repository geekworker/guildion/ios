//
//  CurrentChannel.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit

class CurrentChannel {

    let userDefaults = UserDefaults.standard

    static let prefix = "current_channel:/"
    
    static var shared: CurrentChannel?

    public var id: Int?
    public var uid: String = ""
    public var messageable_type: MessageableType = .TextChannel
    
    init() {}

    init(
        id: Int?,
        uid: String,
        messageable_type: MessageableType
    ) {
        self.id = id
        self.uid = uid
        self.messageable_type = messageable_type
    }

    static func getCurrentChannel() -> CurrentChannel? {
        let userDefaults = UserDefaults.standard
        if let unwrapped = shared {
            return unwrapped
        } else {
            guard let uid = userDefaults.string(forKey: "\(CurrentChannel.prefix)/uid") else {
                return nil
            }
            shared = CurrentChannel(
                id: userDefaults.integer(forKey: "\(CurrentChannel.prefix)/id"),
                uid: uid,
                messageable_type: MessageableType.init(rawValue: userDefaults.string(forKey: "\(CurrentChannel.prefix)/messageable_type")!) ?? .TextChannel
            )
            return CurrentChannel(
                id: userDefaults.integer(forKey: "\(CurrentChannel.prefix)/id"),
                uid: uid,
                messageable_type: MessageableType.init(rawValue: userDefaults.string(forKey: "\(CurrentChannel.prefix)/messageable_type")!) ?? .TextChannel
            )
        }
    }

    static func logout() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "\(CurrentChannel.prefix)/id")
        userDefaults.removeObject(forKey: "\(CurrentChannel.prefix)/uid")
        userDefaults.removeObject(forKey: "\(CurrentChannel.prefix)/messageable_type")
        shared = nil
        userDefaults.synchronize()
    }
    
    static func setCurrentChannel(entity: MessageableEntity) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(entity.messageable_id, forKey: "\(CurrentUser.prefix)/id")
        userDefaults.set(entity.messageable_uid, forKey: "\(CurrentUser.prefix)/uid")
        userDefaults.set(entity.messageable_type?.rawValue, forKey: "\(CurrentUser.prefix)/messageable_type")
        
        if CurrentChannel.shared == nil {
            CurrentChannel.shared = CurrentChannel()
        }
        CurrentChannel.shared?.id = entity.messageable_id
        CurrentChannel.shared?.uid = entity.messageable_uid ?? ""
        CurrentChannel.shared?.messageable_type = entity.messageable_type ?? .TextChannel
        
        userDefaults.synchronize()
    }
}

extension CurrentChannel: StringDefaultSettable {
    enum StringKey : String {
        case id
        case uid
        case messageable_type
    }
}

protocol CurrentChannelProtocol: class {
    func setCurrentChannel(entity: MessageableEntity)
}

extension CurrentChannelProtocol {
    func setCurrentChannel(entity: MessageableEntity) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(entity.messageable_id, forKey: "\(CurrentChannel.prefix)/id")
        userDefaults.set(entity.messageable_uid, forKey: "\(CurrentChannel.prefix)/uid")
        userDefaults.set(entity.messageable_type?.rawValue, forKey: "\(CurrentChannel.prefix)/messageable_type")
        
        if CurrentChannel.shared == nil {
            CurrentChannel.shared = CurrentChannel()
        }
        CurrentChannel.shared?.id = entity.messageable_id
        CurrentChannel.shared?.uid = entity.messageable_uid ?? ""
        CurrentChannel.shared?.messageable_type = entity.messageable_type ?? .TextChannel
        
        userDefaults.synchronize()
    }
}

