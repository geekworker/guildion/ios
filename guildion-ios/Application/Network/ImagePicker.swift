//
//  ImagePicker.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/12.
//

import Foundation
import Alamofire
import RxSwift
import SwiftyJSON
import UIKit
import AWSCore
import AWSCognito
import AWSS3
import Photos
import Rswift

protocol ImagePickerHandlerDelegate: class {
    var viewController: UIViewController { get }
    func updateAssets(_ image: UIImage, info: [UIImagePickerController.InfoKey : Any])
    func cancelAssets()
}

class ImagePickerHandler: NSObject {
    public weak var delegate: ImagePickerHandlerDelegate!
    func openImagePicker(allowsEditing: Bool = true) {
        guard CurrentUser.getCurrentUserEntity() != nil else {
            return
        }

        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = allowsEditing

        delegate.viewController.present(picker, animated: true, completion: nil)
    }
    
    func openCamera(allowsEditing: Bool = true) {
        guard CurrentUser.getCurrentUserEntity() != nil else {
            return
        }

        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.delegate = self
        picker.allowsEditing = allowsEditing
        delegate.viewController.present(picker, animated: true, completion: nil)
    }
    
    func selectSource() {
        let alert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle:  UIAlertController.Style.actionSheet)
        
        alert.popoverPresentationController?.sourceView = self.delegate.viewController.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.delegate.viewController.view.bounds.size.width / 2.0, y: self.delegate.viewController.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        
        let cameraAction: UIAlertAction = UIAlertAction(title: R.string.localizable.camera(), style: UIAlertAction.Style.default, handler:{
            [unowned self] (action: UIAlertAction!) -> Void in
            self.openCamera(allowsEditing: false)
        })
        
        let photoAction: UIAlertAction = UIAlertAction(title: R.string.localizable.photo(), style: UIAlertAction.Style.default, handler:{
            [unowned self] (action: UIAlertAction!) -> Void in
            self.openImagePicker(allowsEditing: false)
        })
        
        let cancelAction: UIAlertAction = UIAlertAction(title: R.string.localizable.cancel(), style: UIAlertAction.Style.cancel, handler:{
            (action: UIAlertAction!) -> Void in
        })
        
        alert.addAction(cameraAction)
        alert.addAction(photoAction)
        alert.addAction(cancelAction)

        delegate.viewController.present(alert, animated: true, completion: nil)
    }
}

extension ImagePickerHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImagePicker: UIImage?

        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            selectedImagePicker = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage {
            selectedImagePicker = originalImage
        }

        delegate.viewController.dismiss(animated: true, completion: {
            if let selectedImage = selectedImagePicker {
                self.delegate.updateAssets(selectedImage, info: info)
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        delegate.cancelAssets()
        delegate.viewController.dismiss(animated: true, completion: nil)
    }
    
    func generateImageURL(_ uploadImage: UIImage) -> URL {
        let imageURL = URL(fileURLWithPath: NSTemporaryDirectory().appendingFormat(UUID().uuidString))
        if let jpegData = uploadImage.jpegData(compressionQuality: 80) {
            try! jpegData.write(to: imageURL, options: [.atomicWrite])
        }
        return imageURL
    }
}
