//
//  Saga.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import Foundation
import ReSwift

public struct Saga<State>: Action {
    let body: (_ dispatch: @escaping DispatchFunction, _ getState: @escaping () -> State?) -> Void
    public init(body: @escaping (
        _ dispatch: @escaping DispatchFunction,
        _ getState: @escaping () -> State?) -> Void) {
        self.body = body
    }
}

@available(*, deprecated, renamed: "Saga")
typealias SagaAction = Saga

public func createSagaMiddleware<State>() -> Middleware<State> {
    return { dispatch, getState in
        return { next in
            return { action in
                switch action {
                case let saga as Saga<State>:
                    saga.body(dispatch, getState)
                default:
                    next(action)
                }
            }
        }
    }
}

// swiftlint:disable identifier_name
@available(*, deprecated, renamed: "createSagaMiddleware")
func SagaMiddleware<State: StateType>() -> Middleware<State> {
    return createSagaMiddleware()
}
// swiftlint:enable identifier_name
//@available(*, deprecated, renamed: "createSagasMiddleware")
func createSagasMiddleware<State: StateType>() -> Middleware<State> {
    return createSagaMiddleware()
}

