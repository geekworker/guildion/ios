//
//  ActivityHandler.swift
//  nowy-ios
//
//  Created by Apple on 2020/07/19.
//  Copyright © 2020 Apple. All rights reserved.
//

import Alamofire
import RxSwift
import SwiftyJSON
import UIKit
import AWSCore
import AWSCognito
import AWSS3
import Photos
import Rswift

protocol ActivityHandlerDelegate: class {
    var activityParent: UIViewController { get }
    var activityItems: [Any] { get }
}

class ActivityHandler: NSObject {
    public weak var delegate: ActivityHandlerDelegate!
    
    func show() {
        let vc = UIActivityViewController(activityItems: delegate!.activityItems, applicationActivities: nil)
        vc.popoverPresentationController?.sourceView = self.delegate.activityParent.view
        vc.popoverPresentationController?.sourceRect = CGRect(x: self.delegate.activityParent.view.bounds.size.width / 2.0, y: self.delegate.activityParent.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        delegate?.activityParent.present(vc, animated: true, completion: {
        })
    }
}

extension ActivityHandler: UINavigationControllerDelegate, UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return URL(string: Constant.APP_URL)!
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return URL(string: Constant.APP_URL)!
    }
}
