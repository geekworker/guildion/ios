//
//  AWSHandler.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import AWSCore
import AWSS3
import Hydra

class AWSHandler {
    static let folderName = "uploads"
    static let unconvertedFolderName = "unconverted"
    static let convertedFolderName = "converted"
    
    init() {
        configureService()
    }
    
    func configureService() {
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: ENV.AWS.access_key, secretKey: ENV.AWS.secret_key)
        let serviceConfiguration = AWSServiceConfiguration(region: ENV.AWS.region, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = serviceConfiguration
    }
    
    public func uploadFile(_ url: URL, type: ContentType, onProgressUpdate: ((Float) -> ())? = nil) -> Promise<String> {
        let filename = "\(AWSHandler.folderName)/\(UUID().uuidString)." + type.file_extension
        return Promise<String> { resolve, reject, _ in
            let uploadImageCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock = { (task, error) -> Void in
                if let unwrappedError = error {
                    reject(unwrappedError)
                }
                resolve(AWSHandler.getUploadedURL(filename: filename))
            }
            let transferManager = AWSS3TransferUtility.default()
            let expression = AWSS3TransferUtilityUploadExpression()
            var uploadProgress = Float(0)
            expression.progressBlock = {(task, progress) in
                DispatchQueue.main.async {
                    if uploadProgress < Float(progress.fractionCompleted) {
                        uploadProgress = Float(progress.fractionCompleted)
                        onProgressUpdate?(uploadProgress)
                    }
                }
            }
            do {
                let data = try Data(contentsOf: url)
                transferManager.uploadData(
                    data,
                    bucket: ENV.AWS.bucket,
                    key: filename,
                    contentType: type.rawValue,
                    expression: expression,
                    completionHandler: uploadImageCompletionHandler
                ).continueWith { (task) -> AnyObject? in
                    if let error = task.error {
                        reject(error)
                    }
                    if let _ = task.result {
                    }
                    return nil
                }
            } catch {
                reject(AppError.unknown)
            }
        }
    }
    
    public func uploadUserFile(_ url: URL, type: ContentType, onProgressUpdate: ((Float) -> ())? = nil) -> Promise<String> {
        return Promise<String> { resolve, reject, _ in
            guard let current_user = CurrentUser.getCurrentUserEntity() else { return }
            let filename = "\(AWSHandler.folderName)/\(current_user.uid)/\(UUID().uuidString)." + type.file_extension
            
            let uploadImageCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock = { (task, error) -> Void in
                if let unwrappedError = error {
                    reject(unwrappedError)
                }
                resolve(AWSHandler.getUploadedURL(filename: filename))
            }
            let transferManager = AWSS3TransferUtility.default()
            let expression = AWSS3TransferUtilityUploadExpression()
            var uploadProgress = Float(0)
            expression.progressBlock = {(task, progress) in
                DispatchQueue.main.async {
                    if uploadProgress < Float(progress.fractionCompleted) {
                        uploadProgress = Float(progress.fractionCompleted)
                        onProgressUpdate?(uploadProgress)
                    }
                }
            }
            do {
                let data = try Data(contentsOf: url)
                transferManager.uploadData(
                    data,
                    bucket: ENV.AWS.bucket,
                    key: filename,
                    contentType: type.rawValue,
                    expression: expression,
                    completionHandler: uploadImageCompletionHandler
                ).continueWith { (task) -> AnyObject? in
                    if let error = task.error {
                        reject(error)
                    }
                    if let _ = task.result {
                    }
                    return nil
                }
            } catch {
                reject(AppError.unknown)
            }
        }
    }
    
    public func baseConvertMovie(_ url: URL, type: ContentType, onProgressUpdate: ((Float) -> ())? = nil) -> Promise<String> {
        return Promise<String> { resolve, reject, _ in
            guard let current_user = CurrentUser.getCurrentUserEntity(), type.is_movie else { return }
            let fileKey = UUID().uuidString + "." + type.file_extension
            let filename = "\(AWSHandler.unconvertedFolderName)/\(current_user.uid)/\(fileKey)"
            
            let uploadImageCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock = { (task, error) -> Void in
                if let unwrappedError = error {
                    reject(unwrappedError)
                }
                resolve(AWSHandler.getBaseConvertedURL(filename: filename, key: fileKey))
            }
            let expression = AWSS3TransferUtilityUploadExpression()
            var uploadProgress = Float(0)
            expression.progressBlock = {(task, progress) in
                DispatchQueue.main.async {
                    if uploadProgress < Float(progress.fractionCompleted) {
                        uploadProgress = Float(progress.fractionCompleted)
                        onProgressUpdate?(uploadProgress)
                    }
                }
            }
            do {
                let data = try Data(contentsOf: url)
                let transferManager = AWSS3TransferUtility.default()
                transferManager.uploadData(
                    data,
                    bucket: ENV.AWS.bucket,
                    key: filename,
                    contentType: type.rawValue,
                    expression: expression,
                    completionHandler: uploadImageCompletionHandler
                ).continueWith { (task) -> AnyObject? in
                    if let error = task.error {
                        reject(error)
                    }
                    if let _ = task.result {
                    }
                    return nil
                }
            } catch {
                reject(AppError.unknown)
            }
        }
    }
    
    public func uploadImage(_ url: URL) -> Promise<String> {
        return self.uploadFile(url, type: .jpg)
    }
    
    public func uploadUserImage(_ url: URL) -> Promise<String> {
        return self.uploadUserFile(url, type: .jpg)
    }
    
    public func deleteFile(_ url: URL) -> Promise<Bool> {
        return Promise<Bool> { resolve, reject, _ in
            let s3 = AWSS3.default()
            let deleteObjectRequest = AWSS3DeleteObjectRequest()
            deleteObjectRequest?.bucket = ENV.AWS.bucket
            deleteObjectRequest?.key = "\(AWSHandler.folderName)/\(url.lastPathComponent)"
            let completionHandler = { (task: AWSS3DeleteObjectOutput?, error: Error?) -> Void in
                if let unwrappedError = error {
                    reject(unwrappedError)
                }
                resolve(true)
            }
            s3.deleteObject(
                deleteObjectRequest!,
                completionHandler: completionHandler
            )
        }
    }
    
    static public func deletable(_ url: URL) -> Bool {
        return url.absoluteString.contains("https://s3-\(ENV.AWS.region_string).amazonaws.com/") || url.absoluteString.contains(ENV.AWS.CloudFront.URL)
    }
    
    static func getUploadedURL(filename: String) -> String {
        return "\(ENV.AWS.CloudFront.URL)/\(filename)"
    }
    
    static func getBaseConvertedURL(filename: String, key: String) -> String {
        let result = AWSHandler.convertedFolderName + "/" + key.excludeExtension + "SSD700/" + key.excludeExtension + "SD700.m3u8"
        return "\(ENV.AWS.CloudFront.URL)/\(result)"
//        let result = (filename.excludeExtension + "SD700.m3u8").replacingOccurrences(of: AWSHandler.unconvertedFolderName, with: AWSHandler.convertedFolderName).replacingOccurrences(of: key.excludeExtension, with: "\(key)/\(key.excludeExtension)")
//        return "\(ENV.AWS.CloudFront.URL)/\(result)"
    }
}
