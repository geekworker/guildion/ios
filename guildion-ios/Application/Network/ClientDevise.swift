//
//  ClientDevise.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import YMTGetDeviceName

class ClientDevise: NSObject {
    let userDefaults = UserDefaults.standard
    fileprivate static var shared: ClientDevise?
    static let prefix = "client_devise:/"
    
    public let model: String = YMTGetDeviceName.share.getDeviceName()
    public let os: String = "iOS"
    public let app_version: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    public var locale: LocaleConfig.Locales {
        get { LocaleConfig.devise_default_locale }
    }
    public let country_code: String? = LocaleConfig.devise_default_country_code
    #if TARGET_IPHONE_SIMULATOR
    public var udid: String = UUID().uuidString
    #else
    public var udid: String = UIDevice.current.identifierForVendor!.uuidString
    #endif
    public var uid: String = ""
    public var notification_id: String = ""
    public var permission: Bool = false
    public var accessToken: String = ""
    public var theme: Themes {
        get { Themes.current }
        set { Themes.current = newValue }
    }

    override init() {
    }
    
    init(
        uid: String,
        notification_id: String,
        permission: Bool,
        accessToken: String,
        theme: Themes = .dark
    ) {
        super.init()
        self.uid = uid
        self.notification_id = notification_id
        self.permission = permission
        self.accessToken = accessToken
        self.theme = theme
    }
    
    static func getClientDevise() -> ClientDevise {
        let userDefaults = UserDefaults.standard
        if let unwrapped = shared, unwrapped.uid != "" {
            return unwrapped
        } else {
            guard let uid = userDefaults.string(forKey: "\(ClientDevise.prefix)/uid"), uid != "" else {
                shared = ClientDevise.init(
                    uid: userDefaults.string(forKey: "\(ClientDevise.prefix)/uid") ?? "",
                    notification_id: userDefaults.string(forKey: "\(ClientDevise.prefix)/notification_id") ?? "",
                    permission: userDefaults.bool(forKey: "\(ClientDevise.prefix)/permission"),
                    accessToken: userDefaults.string(forKey: "\(ClientDevise.prefix)/accessToken") ?? ""
                )
                return shared!
            }
            shared = ClientDevise.init(
                uid: uid,
                notification_id: userDefaults.string(forKey: "\(ClientDevise.prefix)/notification_id") ?? "",
                permission: userDefaults.bool(forKey: "\(ClientDevise.prefix)/permission"),
                accessToken: userDefaults.string(forKey: "\(ClientDevise.prefix)/accessToken") ?? ""
            )
            return shared!
        }
    }
    
    static func deviseRegisterd() -> Bool {
        let userDefaults = UserDefaults.standard
        guard let uid = userDefaults.string(forKey: "\(ClientDevise.prefix)/uid"), uid != "" else {
            return false
        }
        return true
    }
    
    static func logout() {
        let userDefaults = UserDefaults.standard
        self.shared = nil
//        userDefaults.removeObject(forKey: "\(ClientDevise.prefix)/uid")
        userDefaults.removeObject(forKey: "\(ClientDevise.prefix)/permission")
//        userDefaults.removeObject(forKey: "\(ClientDevise.prefix)/notification_id")
        userDefaults.removeObject(forKey: "\(ClientDevise.prefix)/accessToken")
        userDefaults.removeObject(forKey: "\(ClientDevise.prefix)/theme")
        userDefaults.synchronize()
    }
    
}

extension ClientDevise: StringDefaultSettable {
    enum StringKey : String {
        case uid
        case udid
        case os
        case model
        case locale
        case country_code
        case notification_id
        case permission
        case accessToken
        case theme
        
        case player_id
    }
}

protocol ClientDeviseProtocol: class {
    func setClientDevise(entity: ClientDevise)
}

extension ClientDeviseProtocol {
    func setClientDevise(entity: ClientDevise) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(entity.uid, forKey: "\(ClientDevise.prefix)/uid")
        userDefaults.set(entity.notification_id, forKey: "\(ClientDevise.prefix)/notification_id")
        userDefaults.set(entity.permission, forKey: "\(ClientDevise.prefix)/permission")
        if entity.accessToken.count > 0 {
            userDefaults.set(entity.accessToken, forKey: "\(ClientDevise.prefix)/accessToken")
        }
        userDefaults.synchronize()
        if ClientDevise.shared == nil {
            ClientDevise.shared = ClientDevise()
        }
        ClientDevise.shared?.uid = entity.uid
        ClientDevise.shared?.notification_id = entity.notification_id
        ClientDevise.shared?.permission = entity.permission
        if entity.accessToken.count > 0 {
            ClientDevise.shared?.accessToken = entity.accessToken
        }
    }
}
