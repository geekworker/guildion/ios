//
//  CurrentGuild.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit

class CurrentGuild {

    let userDefaults = UserDefaults.standard

    static let prefix = "current_guild:/"
    
    static var shared: CurrentGuild?

    public var id: Int?
    public var uid: String = ""
    public var name: String = ""
    
    init() {}

    init(
        id: Int?,
        uid: String,
        name: String
    ) {
        self.id = id
        self.uid = uid
        self.name = name
    }

    static func getCurrentGuild() -> CurrentGuild? {
        let userDefaults = UserDefaults.standard
        if let unwrapped = shared {
            return unwrapped
        } else {
            guard let uid = userDefaults.string(forKey: "\(CurrentGuild.prefix)/uid") else {
                return nil
            }
            shared = CurrentGuild(
                id: userDefaults.integer(forKey: "\(CurrentGuild.prefix)/id"),
                uid: uid,
                name: userDefaults.string(forKey: "\(CurrentGuild.prefix)/name")!
            )
            return CurrentGuild(
                id: userDefaults.integer(forKey: "\(CurrentGuild.prefix)/id"),
                uid: uid,
                name: userDefaults.string(forKey: "\(CurrentGuild.prefix)/name")!
            )
        }
    }

    static func logout() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "\(CurrentGuild.prefix)/id")
        userDefaults.removeObject(forKey: "\(CurrentGuild.prefix)/uid")
        userDefaults.removeObject(forKey: "\(CurrentGuild.prefix)/name")
        shared = nil
        userDefaults.synchronize()
    }
    
    static func setCurrentGuild(entity: GuildEntity) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(entity.id, forKey: "\(CurrentUser.prefix)/id")
        userDefaults.set(entity.uid, forKey: "\(CurrentUser.prefix)/uid")
        userDefaults.set(entity.name, forKey: "\(CurrentUser.prefix)/name")
        
        if CurrentGuild.shared == nil {
            CurrentGuild.shared = CurrentGuild()
        }
        CurrentGuild.shared?.id = entity.id
        CurrentGuild.shared?.uid = entity.uid
        CurrentGuild.shared?.name = entity.name
        
        userDefaults.synchronize()
    }
}

extension CurrentGuild: StringDefaultSettable {
    enum StringKey : String {
        case id
        case uid
        case name
    }
}

protocol CurrentGuildProtocol: class {
    func setCurrentGuild(entity: GuildEntity)
}

extension CurrentGuildProtocol {
    func setCurrentGuild(entity: GuildEntity) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(entity.id, forKey: "\(CurrentGuild.prefix)/id")
        userDefaults.set(entity.uid, forKey: "\(CurrentGuild.prefix)/uid")
        userDefaults.set(entity.name, forKey: "\(CurrentGuild.prefix)/name")
        
        if CurrentGuild.shared == nil {
            CurrentGuild.shared = CurrentGuild()
        }
        CurrentGuild.shared?.id = entity.id
        CurrentGuild.shared?.uid = entity.uid
        CurrentGuild.shared?.name = entity.name
        
        userDefaults.synchronize()
    }
}

