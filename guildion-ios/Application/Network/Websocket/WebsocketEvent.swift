//
//  WebsocketEvent.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/29.
//

import Foundation
import Starscream
import SwiftyJSON

enum WebsocketEvent {
    case connected(client_user: UserEntity)
    case disconnected(client_user: UserEntity)
    case canceled(client_user: UserEntity)
    case join_room(roomname: String)
    case leave_room(roomname: String)
    case check_first(roomname: String)
    case other
    
    var string: String {
        switch self {
        case .connected: return "connected"
        case .disconnected: return "disconnected"
        case .canceled: return "canceled"
        case .join_room: return "Join"
        case .leave_room: return "Leave"
        case .check_first: return "CheckFirst"
        case .other: return "other"
        }
    }
    
    static func getString(_ event: WebsocketEvent) -> String {
        switch event {
        case .connected: return "connected"
        case .disconnected: return "disconnected"
        case .canceled: return "canceled"
        case .join_room: return "Join"
        case .leave_room: return "Leave"
        case .check_first: return "CheckFirst"
        case .other: return "other"
        }
    }
    
    init(json: JSON) {
        guard let type = json[WebSocketMessageType.type.rawValue].string,
              let _ = json[WebSocketMessageType.id.rawValue].string,
              let client_user = UserEntity.jsonable(json[WebSocketMessageType.client_user.rawValue])
        else {
            self = .other
            return
        }
        switch type {
        case "connected": self = .connected(client_user: client_user)
        case "disconnected": self = .disconnected(client_user: client_user)
        case "canceled": self = .canceled(client_user: client_user)
        case "CheckFirst": self = .check_first(roomname: json[WebSocketMessageType.roomname.rawValue].stringValue)
        default: self = .other
        }
    }
}
