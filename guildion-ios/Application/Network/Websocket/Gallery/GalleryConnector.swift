//
//  FolderConnector.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/26.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON
import Alamofire
import Hydra

protocol FolderConnectorDelegate: class {
    func folderConnector(event: WebsocketFolderEvent)
    func folderConnector(onError: Error)
}

extension FolderConnectorDelegate {
}

class FolderConnector {
    public static var shared: FolderConnector = {
        let connector = FolderConnector()
        WebSocketHandler.shared.inject(connector)
        return connector
    }()
    var delegates: [FolderConnectorDelegate] = []
    var current_folder: FolderEntity?
    
    @inlinable public func inject(_ implementer: FolderConnectorDelegate) {
        self.delegates.append(implementer)
    }
    
    @inlinable public func reject<T>(_ implementer: T) where T: Equatable {
        self.delegates = self.delegates.filter({
            if let unwrapped = $0 as? T, implementer == unwrapped {
                return false
            } else {
                return true
            }
        })
    }
    
    @inlinable public func invoke(_ delegater: (FolderConnectorDelegate) -> ()) {
        for delegate in delegates {
            delegater(delegate)
        }
    }
    
    func didJoin(current_folder: FolderEntity, current_guild: GuildEntity? = GuildConnector.shared.current_guild, current_member: MemberEntity? = GuildConnector.shared.current_member) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_guild = current_guild, let current_member = current_member else { resolve(()); return }
            if self.current_folder != nil && self.current_folder != current_folder {
                try await(self.didLeave())
            }
            
            if GuildConnector.shared.current_guild == nil {
                GuildConnector.shared.current_guild = current_guild
            }
            
            if GuildConnector.shared.current_member == nil {
                GuildConnector.shared.current_member = current_member
            }
            
            self.current_folder = current_folder
            try await(WebSocketHandler.shared.joinRoom(roomname: current_guild.uid + WebSocketHandler.separator + current_folder.uid))
            
            resolve(())
        }
    }
    
    func didLeave() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_guild = GuildConnector.shared.current_guild, let current_folder = self.current_folder else { resolve(()); return }
            try await(WebSocketHandler.shared.leaveRoom(roomname: current_guild.uid + WebSocketHandler.separator + current_folder.uid))
            try await(GuildConnector.shared.reJoin())
            self.current_folder = nil
            resolve(())
        }
    }
    
    func didCreateFile(file: FileEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_folder = self.current_folder else { resolve(()); return }
            let payload: JSON =  [
                WebsocketFolderPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketFolderPayloadType.file.rawValue: file.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketFolderEvent.getString(.didCreateFile(member: MemberEntity(), file: FileEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_folder.uid, to_parent: true))
            resolve(())
        }
    }
    
    func didDeleteFile(file: FileEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_folder = self.current_folder else { resolve(()); return }
            let payload: JSON =  [
                WebsocketFolderPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketFolderPayloadType.file.rawValue: file.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketFolderEvent.getString(.didDeleteFile(member: MemberEntity(), file: FileEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_folder.uid, to_parent: true))
            resolve(())
        }
    }
    
    func didCreateFolder(folder: FolderEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_folder = self.current_folder else { resolve(()); return }
            let payload: JSON =  [
                WebsocketFolderPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketFolderPayloadType.file.rawValue: folder.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketFolderEvent.getString(.didCreateFolder(member: MemberEntity(), folder: FolderEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_folder.uid, to_parent: true))
            resolve(())
        }
    }
    
    func didUpdateFolder(folder: FolderEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_folder = self.current_folder else { resolve(()); return }
            let payload: JSON =  [
                WebsocketFolderPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketFolderPayloadType.file.rawValue: folder.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketFolderEvent.getString(.didUpdateFolder(member: MemberEntity(), folder: FolderEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_folder.uid, to_parent: true))
            resolve(())
        }
    }
    
    func didDeleteFolder(folder: FolderEntity) -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_folder = self.current_folder else { resolve(()); return }
            let payload: JSON =  [
                WebsocketFolderPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketFolderPayloadType.file.rawValue: folder.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketFolderEvent.getString(.didDeleteFolder(member: MemberEntity(), folder: FolderEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_folder.uid, to_parent: true))
            resolve(())
        }
    }
}

extension FolderConnector: WebSocketHandlerDelegate {
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        let folder_event = WebsocketFolderEvent.init(
            type: type,
            id: id,
            payload: payload,
            client_user: client_user,
            client_room: client_room
        )
        self.invoke({ $0.folderConnector(event: folder_event) })
    }
    
    func webSocketHandler(event: WebsocketEvent) {}
    
    func webSocketHandler(onError: Error) {}
}

