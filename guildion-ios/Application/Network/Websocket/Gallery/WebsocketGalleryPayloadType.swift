//
//  WebsocketFolderPayloadType.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/26.
//

import Foundation

enum WebsocketFolderPayloadType: String {
    case client_member
    case file
    case folder
}
