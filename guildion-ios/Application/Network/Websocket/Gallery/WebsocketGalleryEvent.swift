//
//  WebsocketFolderEvent.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/26.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON

enum WebsocketFolderEvent {
    case didCreateFile(member: MemberEntity, file: FileEntity)
    case didDeleteFile(member: MemberEntity, file: FileEntity)
    case didCreateFolder(member: MemberEntity, folder: FolderEntity)
    case didUpdateFolder(member: MemberEntity, folder: FolderEntity)
    case didDeleteFolder(member: MemberEntity, folder: FolderEntity)
    case other
    
    var string: String {
        switch self {
        case .didCreateFile: return "didCreateFile"
        case .didDeleteFile: return "didDeleteFile"
        case .didCreateFolder: return "didCreateFolder"
        case .didUpdateFolder: return "didUpdateFolder"
        case .didDeleteFolder: return "didDeleteFolder"
        case .other: return "other"
        }
    }
    
    static func getString(_ event: WebsocketFolderEvent) -> String {
        switch event {
        case .didCreateFile: return "didCreateFile"
        case .didDeleteFile: return "didDeleteFile"
        case .didCreateFolder: return "didCreateFolder"
        case .didUpdateFolder: return "didUpdateFolder"
        case .didDeleteFolder: return "didDeleteFolder"
        case .other: return "other"
        }
    }
    
    init(
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        guard let client_member = MemberEntity.jsonable(payload[WebsocketFolderPayloadType.client_member.rawValue])
        else {
            self = .other
            return
        }
        switch type {
        case WebsocketFolderEvent.getString(.didCreateFile(member: MemberEntity(), file: FileEntity())):
            guard let file = FileEntity.jsonable(payload[WebsocketFolderPayloadType.file.rawValue]) else { self = .other; return }
            self = .didCreateFile(member: client_member, file: file)
        case WebsocketFolderEvent.getString(.didDeleteFile(member: MemberEntity(), file: FileEntity())):
            guard let file = FileEntity.jsonable(payload[WebsocketFolderPayloadType.file.rawValue]) else { self = .other; return }
            self = .didDeleteFile(member: client_member, file: file)
        case WebsocketFolderEvent.getString(.didCreateFolder(member: MemberEntity(), folder: FolderEntity())):
            guard let folder = FolderEntity.jsonable(payload[WebsocketFolderPayloadType.folder.rawValue]) else { self = .other; return }
            self = .didCreateFolder(member: client_member, folder: folder)
        case WebsocketFolderEvent.getString(.didUpdateFolder(member: MemberEntity(), folder: FolderEntity())):
            guard let folder = FolderEntity.jsonable(payload[WebsocketFolderPayloadType.folder.rawValue]) else { self = .other; return }
            self = .didUpdateFolder(member: client_member, folder: folder)
        case WebsocketFolderEvent.getString(.didDeleteFolder(member: MemberEntity(), folder: FolderEntity())):
            guard let folder = FolderEntity.jsonable(payload[WebsocketFolderPayloadType.folder.rawValue]) else { self = .other; return }
            self = .didDeleteFolder(member: client_member, folder: folder)
        default: self = .other
        }
    }
}
