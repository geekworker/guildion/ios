//
//  WebsocketMessageType.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/29.
//

import Foundation

enum WebSocketMessageType: String {
    case type
    case id
    case to_self
    case payload
    case client_user
    case roomname
    case parent
    case children
    case sdp
    case ice
}
