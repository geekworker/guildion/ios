//
//  WebsocketGuildEvent.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import UIKit
import Starscream
import SwiftyJSON

enum WebsocketGuildEvent {
    case fetchRequest
    case other
    
    var string: String {
        switch self {
        case .fetchRequest: return "fetchRequest"
        case .other: return "other"
        }
    }
    
    static func getString(_ event: WebsocketGuildEvent) -> String {
        switch event {
        case .fetchRequest: return "fetchRequest"
        case .other: return "other"
        }
    }
    
    init(
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        guard MemberEntity.jsonable(payload[WebsocketGuildPayloadType.client_member.rawValue]) != nil
        else {
            self = type == WebsocketGuildEvent.fetchRequest.string ? .fetchRequest : .other
            return
        }
        switch type {
        default: self = .other
        }
    }
}

