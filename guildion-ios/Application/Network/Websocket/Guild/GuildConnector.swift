//
//  GuildConnector.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON
import Alamofire
import Hydra

protocol GuildConnectorDelegate: class {
    func guildConnector(event: WebsocketGuildEvent)
    func guildConnector(onError: Error)
}

class GuildConnector {
    public static var shared: GuildConnector = {
        let connector = GuildConnector()
        WebSocketHandler.shared.inject(connector)
        return connector
    }()
    var delegates: [GuildConnectorDelegate] = []
    var current_guild: GuildEntity? {
        didSet {
            current_permission.Guild.current_guild = current_guild
        }
    }
    var current_member: MemberEntity? {
        didSet {
            current_permission.Guild.current_member_roles = current_member?.MemberRoles ?? MemberRoleEntities()
        }
    }
    var current_channels: Channelables {
        let entities = Channelables()
        entities.items = current_guild?.SectionChannels?.channels() ?? []
        return entities
    }
    var current_members: MemberEntities {
        return current_guild?.Members ?? MemberEntities()
    }
    var current_everyone_role: RoleEntity? {
        return current_guild?.Roles?.items.filter({ $0.is_everyone }).first
    }
    lazy var current_permission: PermissionModel = {
        return PermissionModel()
    }()
    
    @inlinable public func inject(_ implementer: GuildConnectorDelegate) {
        self.delegates.append(implementer)
    }
    
    @inlinable public func reject<T>(_ implementer: T) where T: Equatable {
        self.delegates = self.delegates.filter({
            if let unwrapped = $0 as? T, implementer == unwrapped {
                return false
            } else {
                return true
            }
        })
    }

    @inlinable public func invoke(_ delegater: (GuildConnectorDelegate) -> ()) {
        for delegate in delegates {
            delegater(delegate)
        }
    }
    
    func didJoin(current_guild: GuildEntity, current_member: MemberEntity) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard self.current_guild != current_guild && self.current_member != current_member else { resolve(()); return }
            if self.current_guild != nil && self.current_guild != current_guild && self.current_member != nil {
                try await(self.didLeave())
            }
            self.current_guild = current_guild
            self.current_member = current_member
            try await(WebSocketHandler.shared.joinRoom(roomname: current_guild.uid))
            
            resolve(())
        }
    }
    
    func didLeave() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_guild = self.current_guild else { resolve(()); return }
            try await(WebSocketHandler.shared.leaveRoom(roomname: current_guild.uid))
            self.current_guild = nil
            self.current_member = nil
            resolve(())
        }
    }
    
    func didJoinChildRoom() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_guild = self.current_guild else { resolve(()); return }
            try await(WebSocketHandler.shared.leaveRoom(roomname: current_guild.uid))
            resolve(())
        }
    }
    
    func reJoin() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_guild = self.current_guild else { resolve(()); return }
            try await(WebSocketHandler.shared.joinRoom(roomname: current_guild.uid))
            resolve(())
        }
    }
    
    func fetchRequest() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard let current_guild = self.current_guild else { resolve(()); return }
            let payload: JSON =  [:]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketGuildEvent.getString(.fetchRequest),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid, to_parent: true))
            resolve(())
        }
    }
}

extension GuildConnector: WebSocketHandlerDelegate {
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        let guild_event = WebsocketGuildEvent.init(
            type: type,
            id: id,
            payload: payload,
            client_user: client_user,
            client_room: client_room
        )
        self.invoke({ $0.guildConnector(event: guild_event) })
    }
    
    func webSocketHandler(event: WebsocketEvent) {}
    
    func webSocketHandler(onError: Error) {}
}
