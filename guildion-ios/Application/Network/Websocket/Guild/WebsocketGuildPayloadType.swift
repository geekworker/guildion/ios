//
//  WebsocketGuildPayloadType.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation

enum WebsocketGuildPayloadType: String {
    case client_member
    case guild
    case channel
}

