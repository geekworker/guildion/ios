//
//  WebsocketPayloadable.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/29.
//

import Foundation
import Starscream
import SwiftyJSON

protocol WebsocketPayloadable: class {
    associatedtype Element
    func toWebsocketPayload() -> JSON
    static func jsonable(_ json: JSON) -> Element?
}
