//
//  WebsocketChannelEvent.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import UIKit
import Starscream
import SwiftyJSON

enum WebsocketChannelEvent {
    case fetchRequest
    case didEditInput(member: MemberEntity)
    case endEditInput(member: MemberEntity)
    case didCreateMessage(member: MemberEntity, message: MessageEntity)
    case didUpdateMessage(member: MemberEntity, message: MessageEntity)
    case didDeleteMessage(member: MemberEntity, message: MessageEntity)
    case didReaction(member: MemberEntity, reaction: ReactionEntity)
    case other
    
    var string: String {
        switch self {
        case .fetchRequest: return "channel/fetchRequest"
        case .didEditInput: return "didEditInput"
        case .endEditInput: return "endEditInput"
        case .didCreateMessage: return "didCreateMessage"
        case .didUpdateMessage: return "didUpdateMessage"
        case .didDeleteMessage: return "didDeleteMessage"
        case .didReaction: return "didReaction"
        case .other: return "other"
        }
    }
    
    static func getString(_ event: WebsocketChannelEvent) -> String {
        switch event {
        case .fetchRequest: return "channel/fetchRequest"
        case .didEditInput: return "didEditInput"
        case .endEditInput: return "endEditInput"
        case .didCreateMessage: return "didCreateMessage"
        case .didUpdateMessage: return "didUpdateMessage"
        case .didDeleteMessage: return "didDeleteMessage"
        case .didReaction: return "didReaction"
        case .other: return "other"
        }
    }
    
    init(
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        guard let client_member = MemberEntity.jsonable(payload[WebsocketChannelPayloadType.client_member.rawValue])
        else {
            self = type == WebsocketChannelEvent.fetchRequest.string ? .fetchRequest : .other
            return
        }
        switch type {
        case WebsocketChannelEvent.getString(.didEditInput(member: MemberEntity())): self = .didEditInput(member: client_member)
        case WebsocketChannelEvent.getString(.endEditInput(member: MemberEntity())): self = .endEditInput(member: client_member)
        case WebsocketChannelEvent.getString(.didCreateMessage(member: MemberEntity(), message: MessageEntity())):
            guard let message = MessageEntity.jsonable(payload[WebsocketChannelPayloadType.message.rawValue]) else { self = .other; return }
            self = .didCreateMessage(member: client_member, message: message)
        case WebsocketChannelEvent.getString(.didUpdateMessage(member: MemberEntity(), message: MessageEntity())):
            guard let message = MessageEntity.jsonable(payload[WebsocketChannelPayloadType.message.rawValue]) else { self = .other; return }
            self = .didUpdateMessage(member: client_member, message: message)
        case WebsocketChannelEvent.getString(.didDeleteMessage(member: MemberEntity(), message: MessageEntity())):
            guard let message = MessageEntity.jsonable(payload[WebsocketChannelPayloadType.message.rawValue]) else { self = .other; return }
            self = .didDeleteMessage(member: client_member, message: message)
        case WebsocketChannelEvent.getString(.didReaction(member: MemberEntity(),  reaction: ReactionEntity())):
            guard let reaction = ReactionEntity.jsonable(payload[WebsocketChannelPayloadType.reaction.rawValue]) else { self = .other; return }
            self = .didReaction(member: client_member, reaction: reaction)
        default: self = .other
        }
    }
}
