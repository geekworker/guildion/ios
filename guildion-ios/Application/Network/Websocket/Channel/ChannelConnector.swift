//
//  ChannelConnector.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON
import Alamofire
import Hydra

protocol ChannelConnectorDelegate: class {
    func channelConnector(event: WebsocketChannelEvent)
    func channelConnector(onError: Error)
}

class ChannelConnector {
    public static var shared: ChannelConnector = {
        let connector = ChannelConnector()
        WebSocketHandler.shared.inject(connector)
        return connector
    }()
    var delegates: [ChannelConnectorDelegate] = []
    var current_channel: MessageableEntity? {
        didSet {
            switch current_channel?.messageable_type {
            case .TextChannel:
                guard let channel = current_channel as? TextChannelEntity, let roles = channel.Roles else { return }
                GuildConnector.shared.current_permission.TextChannel.current_text_channel_roles = roles
            case .DMChannel:
                guard let channel = current_channel as? DMChannelEntity, let roles = channel.Roles else { return }
                GuildConnector.shared.current_permission.DMChannel.current_dm_channel_roles = roles
            case .StreamChannel:
                guard let channel = current_channel as? StreamChannelEntity, let roles = channel.Roles else { return }
                GuildConnector.shared.current_permission.StreamChannel.current_stream_channel_roles = roles
            default: break
            }
        }
    }
    
    @inlinable public func inject(_ implementer: ChannelConnectorDelegate) {
        self.delegates.append(implementer)
    }
    
    @inlinable public func reject<T>(_ implementer: T) where T: Equatable {
        self.delegates = self.delegates.filter({
            if let unwrapped = $0 as? T, implementer == unwrapped {
                return false
            } else {
                return true
            }
        })
    }
    
    @inlinable public func invoke(_ delegater: (ChannelConnectorDelegate) -> ()) {
        for delegate in delegates {
            delegater(delegate)
        }
    }
    
    func didJoin(current_channel: MessageableEntity, current_guild: GuildEntity? = GuildConnector.shared.current_guild, current_member: MemberEntity? = GuildConnector.shared.current_member) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_guild = current_guild, let current_member = current_member else { resolve(()); return }
            if self.current_channel != nil && self.current_channel != current_channel {
                if LiveConfig.LiveStreamChannel != current_channel {
                    try await(self.didLeave())
                }
            }
            
            if GuildConnector.shared.current_guild == nil {
                GuildConnector.shared.current_guild = current_guild
            }
            
            if GuildConnector.shared.current_member == nil {
                GuildConnector.shared.current_member = current_member
            }
            
            self.current_channel = current_channel

            try await(WebSocketHandler.shared.joinRoom(roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            
            resolve(())
        }
    }
    
    func reJoin(current_channel: MessageableEntity, current_guild: GuildEntity? = GuildConnector.shared.current_guild, current_member: MemberEntity? = GuildConnector.shared.current_member) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard self.current_channel != current_channel else { resolve(()); return }
            self.current_channel = current_channel
            resolve(())
        }
    }
    
    func didLeave() -> Promise<Void> {
        return Promise<Void> (in: .main) { [unowned self] resolve, reject, _ in
            guard let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            try await(WebSocketHandler.shared.leaveRoom(roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            try await(GuildConnector.shared.reJoin())
            if let repository = LiveConfig.LiveStreamChannel {
                try await(StreamChannelConnector.shared.reJoin(current_channel: repository))
            } else {
                self.current_channel = nil
            }
            resolve(())
        }
    }
    
    func fetchRequest() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            guard let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON = [:]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketChannelEvent.getString(.fetchRequest),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func didEditInput() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketChannelEvent.getString(.didEditInput(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
    
    func endEditInput() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketChannelEvent.getString(.endEditInput(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
    
    func didCreateMessage(message: MessageEntity) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketChannelPayloadType.message.rawValue: message.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketChannelEvent.getString(.didCreateMessage(member: MemberEntity(), message: MessageEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func didUpdateMessage(message: MessageEntity) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketChannelPayloadType.message.rawValue: message.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketChannelEvent.getString(.didUpdateMessage(member: MemberEntity(), message: MessageEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func didDeleteMessage(message: MessageEntity) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketChannelPayloadType.message.rawValue: message.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketChannelEvent.getString(.didDeleteMessage(member: MemberEntity(), message: MessageEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func didReaction(reaction: ReactionEntity) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketChannelPayloadType.reaction.rawValue: reaction.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketChannelEvent.getString(.didReaction(member: MemberEntity(), reaction: ReactionEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
}

extension ChannelConnector: WebSocketHandlerDelegate {
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        let channel_event = WebsocketChannelEvent.init(
            type: type,
            id: id,
            payload: payload,
            client_user: client_user,
            client_room: client_room
        )
        self.invoke({ $0.channelConnector(event: channel_event) })
    }
    
    func webSocketHandler(event: WebsocketEvent) {}
    
    func webSocketHandler(onError: Error) {}
}

