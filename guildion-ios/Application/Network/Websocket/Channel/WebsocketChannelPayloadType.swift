//
//  WebsocketChannelPayloadType.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation

enum WebsocketChannelPayloadType: String {
    case client_member
    case channel
    case message
    case reaction
}

