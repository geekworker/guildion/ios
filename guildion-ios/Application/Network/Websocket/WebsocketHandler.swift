//
//  WebsocketHandler.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON
import Alamofire
import Hydra
import RxSwift
import ObjectMapper

protocol WebSocketHandlerDelegate: class {
    func webSocketHandler(event: WebsocketEvent)
    func didReceive(text: String)
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    )
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String,
        sdp: String,
        ice: JSON
    )
    func webSocketHandler(onError: Error)
    func webSocketHandlerDidConnect()
    func webSocketHandler(didRetry count: Int, error: Error)
    func webSocketHandlerDidDisconnect()
}

extension WebSocketHandlerDelegate {
    func didReceive(text: String) {}
    func webSocketHandler(event: WebsocketEvent) {}
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {}
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String,
        sdp: String,
        ice: JSON
    ) {}
    func webSocketHandler(onError: Error) {}
    func webSocketHandlerDidConnect() {}
    func webSocketHandler(didRetry count: Int, error: Error) {}
    func webSocketHandlerDidDisconnect() {}
}

class WebSocketHandler {
    public static let separator = "-"
    public static let shared = WebSocketHandler()
    var delegates: [WebSocketHandlerDelegate] = []
    var websocket: WebSocket!
    var wsURL: URL = URL(string: Constant.WS_URL)!
    var id: String {
        self._id
    }
    var connected: Bool = false
    var retrying: Bool = false
    var connect_promise: Promise<Void>?
    var last_promise_reject: ((_ error: Error) -> ())?
    private var pingPongTimer: Timer?
    private var disposeBag: DisposeBag = DisposeBag()
    
    public lazy var _id: String = {
        return UUID().uuidString
    }()
    
    init() {
    }
    
    init(wsURL: URL) {
        self.wsURL = wsURL
    }
    
    @inlinable public func inject(_ implementer: WebSocketHandlerDelegate) {
        self.delegates.append(implementer)
    }
    
    @inlinable public func reject<T>(_ implementer: T) where T: Equatable {
        self.delegates = self.delegates.filter({
            if let unwrapped = $0 as? T, implementer == unwrapped {
                return false
            } else {
                return true
            }
        })
    }

    @inlinable public func invoke(_ delegater: (WebSocketHandlerDelegate) -> ()) {
        for delegate in delegates {
            delegater(delegate)
        }
    }
    
    var connected_resolve: ((_ value: Void) -> ())?
    var connected_reject: ((_ error: Error) -> ())?
    func startWebSocket() -> Promise<Void> {
        guard connect_promise == nil else { return connect_promise! }
        self.connect_promise = Promise<Void> { resolve, reject, _ in
            guard !self.connected else {
                resolve(())
                return
            }
            self.wsURL = URL(string: Constant.WS_CONNECT_URL)!
            self.connected_resolve = resolve
            self.connected_reject = reject
            var request = URLRequest(url: self.wsURL)
            request.addValue(ENV.SIGNALING.KEY, forHTTPHeaderField: "API_KEY")
            request.addValue(ENV.SIGNALING.PASSWORD, forHTTPHeaderField: "API_SECRET")
            request.timeoutInterval = 5
            if self.websocket != nil {
                self.websocket.forceDisconnect()
            }
            self.websocket = WebSocket(request: request)
            self.websocket.delegate = self
            self.websocket.connect()
            DispatchQueue.main.asyncAfter(deadline: .now() + request.timeoutInterval, execute: {
                guard !self.connected else { return }
                reject(AppError.domainError)
            })
        }
        return self.connect_promise!
    }
    
    func startWebSocketWithAttempt() -> Promise<Void> {
        return Promise<Void> { resolve, reject, _ in
            let promise = self.connect_promise ?? self.startWebSocket()
            promise.retry(.max) { count, error in
                self.retrying = true
                self.invoke { $0.webSocketHandler(didRetry: count, error: error) }
                return true
            }.then({
                self.retrying = false
                resolve($0)
            }).catch({ _ in
                self.connect_promise = nil
                self.startWebSocketWithAttempt().then(resolve)
            })
        }
    }
    
    func closeWebSocket() {
        DispatchQueue.main.async {
            guard self.websocket != nil, self.connected else { return }
            self.websocket.disconnect()
            self.websocket = nil
            self.connect_promise = nil
            self.connected = false
            self.pingPongTimer?.invalidate()
            self.pingPongTimer = nil
            self.invoke { $0.webSocketHandlerDidDisconnect() }
        }
    }
    
    func websocketDidReceiveMessage(text: String) {
        self.invoke({ $0.didReceive(text: text) })
        let jsonMessage = JSON(parseJSON: text)
        let id = jsonMessage[WebSocketMessageType.id.rawValue].stringValue
        let to_self = jsonMessage[WebSocketMessageType.to_self.rawValue].boolValue
        let event = WebsocketEvent.init(json: jsonMessage)
        guard (!to_self && self.id != id) || (to_self && self.id == id) else { return }
        self.invoke({ $0.webSocketHandler(event: event) })
        self.invoke({
            $0.webSocketHandler(
                event: event,
                type: jsonMessage[WebSocketMessageType.type.rawValue].stringValue,
                id: id,
                payload: jsonMessage[WebSocketMessageType.payload.rawValue],
                client_user: UserEntity.jsonable(jsonMessage[WebSocketMessageType.client_user.rawValue]) ?? UserEntity(),
                client_room: jsonMessage[WebSocketMessageType.roomname.rawValue].stringValue
            )
        })
        self.invoke({
            $0.webSocketHandler(
                event: event,
                type: jsonMessage[WebSocketMessageType.type.rawValue].stringValue,
                id: id,
                payload: jsonMessage[WebSocketMessageType.payload.rawValue],
                client_user: UserEntity.jsonable(jsonMessage[WebSocketMessageType.client_user.rawValue]) ?? UserEntity(),
                client_room: jsonMessage[WebSocketMessageType.roomname.rawValue].stringValue,
                sdp: jsonMessage[WebSocketMessageType.sdp.rawValue].stringValue,
                ice: jsonMessage[WebSocketMessageType.ice.rawValue]
            )
        })
    }
    
    func joinRoom(roomname: String) -> Promise<Void> {
        let json: JSON =  [
            WebSocketMessageType.type.rawValue: WebsocketEvent.getString(.join_room(roomname: "")),
            WebSocketMessageType.id.rawValue: self.id,
            WebSocketMessageType.roomname.rawValue: roomname,
        ]
        return self.sendMessage(json: json, roomname: roomname)
    }
    
    func leaveRoom(roomname: String) -> Promise<Void> {
        let json: JSON =  [
            WebSocketMessageType.type.rawValue: WebsocketEvent.getString(.leave_room(roomname: "")),
            WebSocketMessageType.id.rawValue: self.id,
            WebSocketMessageType.roomname.rawValue: roomname,
        ]
        return self.sendMessage(json: json, roomname: roomname)
    }
    
    func checkFirst(roomname: String) -> Promise<Void> {
        let json: JSON =  [
            WebSocketMessageType.type.rawValue: WebsocketEvent.getString(.check_first(roomname: "")),
            WebSocketMessageType.id.rawValue: self.id,
            WebSocketMessageType.roomname.rawValue: roomname,
            WebSocketMessageType.to_self.rawValue: true
        ]
        return self.sendMessage(json: json, roomname: roomname)
    }
    
    func sendMessage(json: JSON, roomname: String, to_parent: Bool = false, to_children: Bool = false) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            var _json = json
            guard self.websocket != nil else { resolve(()); return }
            
            _json[WebSocketMessageType.id.rawValue] = JSON(self.id)
            _json[WebSocketMessageType.roomname.rawValue] = JSON(roomname)
            if let user = CurrentUser.getCurrentUserEntity() {
                _json[WebSocketMessageType.client_user.rawValue] = user.toWebsocketPayload()
            }
            if to_parent {
                _json[WebSocketMessageType.parent.rawValue] = "true"
            }
            if to_children {
                _json[WebSocketMessageType.children.rawValue] = "true"
            }
            guard let message = _json.rawString() else { resolve(()); return }
            self.last_promise_reject = reject
            self.websocket.write(
                string: message,
                completion: {
                    self.last_promise_reject = nil
                    resolve(())
                }
            )
            // Log.LOG("Sent text: \(message)")
        }
    }
    
    func _sendMessage(json: JSON, roomname: String, to_parent: Bool = false) {
        var _json = json
        guard self.websocket != nil else { return }
        
        _json[WebSocketMessageType.id.rawValue] = JSON(self.id)
        _json[WebSocketMessageType.roomname.rawValue] = JSON(roomname)
        if let user = CurrentUser.getCurrentUserEntity() {
            _json[WebSocketMessageType.client_user.rawValue] = user.toWebsocketPayload()
        }
        if to_parent {
            _json[WebSocketMessageType.parent.rawValue] = "true"
        }
        guard let message = _json.rawString() else { return }
        self.websocket.write(string: message)
    }
    
    func getRoomMembersCount(roomname: String) -> Promise<Int> {
        return Promise<Int> { resolve, reject, _ in
            let request = signalingRequest.getRoomMembersCount(roomname: roomname)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var count: Int = 0
                    count <- data["count"]
                    resolve(count)
                },
                onError: { err in reject(err) }
            )
        }
    }
}

extension WebSocketHandler: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            Log.LOG("Websocket is connected: \(headers)")
            self.connected = true
            self.retrying = false
            self.connected_resolve?(())
            self.connected_resolve = nil
            self.connected_reject = nil
            self.invoke { $0.webSocketHandlerDidConnect() }
            self.pingPongTimer = Timer.scheduledTimer(withTimeInterval: 20.0, repeats: true, block: { (timer) in
                if self.websocket != nil {
                    self.websocket.write(ping: Data())
                    return
                }
                timer.invalidate()
            })
            break
        case .disconnected(let reason, let code):
            Log.LOG("websocket is disconnected: \(reason) with code: \(code)")
             self.closeWebSocket()
             self.connected_reject?(AppError.unknown)
            break
        case .text(let string):
//            Log.LOG("Received text: \(string)")
            self.websocketDidReceiveMessage(text: string)
            break
        case .binary(let data):
            Log.LOG("Received data: \(data.count)")
            break
        case .ping(_):
            break
        case .pong(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            Log.LOG("cancelled")
            self.closeWebSocket()
            self.connected_reject?(AppError.unknown)
            break
        case .error(let error):
            guard let unwrapped = error else { return }
            Log.LOG("Recieved error: \(String(describing: unwrapped))")
            if let reject = last_promise_reject {
                reject(unwrapped)
            } else {
                self.invoke({ $0.webSocketHandler(onError: unwrapped) })
            }
            break
        case .viabilityChanged(_):
            break
        }
    }
}
