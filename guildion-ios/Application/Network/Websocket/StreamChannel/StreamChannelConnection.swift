//
//  StreamChannelConnection.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON
import Alamofire
import Hydra

protocol StreamChannelConnectorDelegate: class {
    func streamChannelConnector(event: WebsocketStreamChannelEvent)
    func streamChannelConnector(onError: Error)
}

class StreamChannelConnector {
    public static var shared: StreamChannelConnector = {
        let connector = StreamChannelConnector()
        WebSocketHandler.shared.inject(connector)
        return connector
    }()
    var delegates: [StreamChannelConnectorDelegate] = []
    var current_channel: StreamChannelEntity? {
        didSet {
            ChannelConnector.shared.current_channel = current_channel
        }
    }
    var current_participant: ParticipantEntity?
    var roomname: String? {
        guard let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { return nil }
        return current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!
    }
    
    @inlinable public func inject(_ implementer: StreamChannelConnectorDelegate) {
        self.delegates.append(implementer)
    }
    
    @inlinable public func reject<T>(_ implementer: T) where T: Equatable {
        self.delegates = self.delegates.filter({
            if let unwrapped = $0 as? T, implementer == unwrapped {
                return false
            } else {
                return true
            }
        })
    }

    @inlinable public func invoke(_ delegater: (StreamChannelConnectorDelegate) -> ()) {
        for delegate in delegates {
            delegater(delegate)
        }
    }
    
    func checkJoinable(current_channel: StreamChannelEntity, current_guild: GuildEntity) -> Promise<Bool> {
        return Promise<Bool> (in: .main) { resolve, reject, _ in
            WebSocketHandler.shared.getRoomMembersCount(
                roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!
            ).then({
                resolve($0 < current_channel.max_participant_count)
            }).catch({
                reject($0)
            })
        }
    }
    
    func checkBecomableHostMessage(current_channel: StreamChannelEntity, current_guild: GuildEntity) -> Promise<Void> {
        return Promise<Void> (in: .main) { resolve, reject, _ in
            WebSocketHandler.shared.checkFirst(
                roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!
            ).then({
                resolve(())
            }).catch({
                reject($0)
            })
        }
    }
    
    func didJoin(current_channel: StreamChannelEntity, current_guild: GuildEntity? = GuildConnector.shared.current_guild, current_member: MemberEntity? = GuildConnector.shared.current_member) -> Promise<Void> {
        return Promise<Void> (in: .main) { [unowned self] resolve, reject, _ in
            guard let current_guild = current_guild, let current_member = current_member else { resolve(()); return }
            if self.current_channel != nil && self.current_channel != current_channel  {
                if LiveConfig.LiveStreamChannel != current_channel {
                    try await(self.didLeave())
                }
            }
            self.current_channel = current_channel
            
            if GuildConnector.shared.current_guild == nil {
                GuildConnector.shared.current_guild = current_guild
            }
            
            if GuildConnector.shared.current_member == nil {
                GuildConnector.shared.current_member = current_member
            }
            
            try await(WebSocketHandler.shared.joinRoom(roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            
            resolve(())
        }
    }
    
    func reJoin(current_channel: StreamChannelEntity, current_guild: GuildEntity? = GuildConnector.shared.current_guild, current_member: MemberEntity? = GuildConnector.shared.current_member) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard self.current_channel == current_channel else { resolve(()); return }
            self.current_channel = current_channel
            resolve(())
        }
    }
    
    func didLeave() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            try await(WebSocketHandler.shared.leaveRoom(roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            try await(GuildConnector.shared.reJoin())
            ChannelConnector.shared.current_channel = nil
            self.current_channel = nil
            resolve(())
        }
    }
    
    func didParticipantJoin() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.didParticipantJoin(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func didParticipantLeave() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.didParticipantLeave(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func changePlayerStatus(syncPlayManager: SyncPlayManagerModel) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.changePlayerStatus(id: "", syncPlayManager: syncPlayManager, member: current_member)),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func changePlayerLoop(syncPlayManager: SyncPlayManagerModel) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.changePlayerLoop(id: "", syncPlayManager: syncPlayManager, member: current_member)),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func syncStream(syncPlayManager: SyncPlayManagerModel) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.syncStream(id: "", syncPlayManager: syncPlayManager, member: current_member)),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func transferHost(syncPlayManager: SyncPlayManagerModel, receiver_id: String) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON = [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.receiver_id.rawValue: receiver_id,
            ]
            
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.transferHost(id: "", receiver_id: receiver_id, syncPlayManager: syncPlayManager, member: current_member)),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func fetchRequest(syncPlayManager: SyncPlayManagerModel, receiver_id: String? = nil) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON = receiver_id == nil ? [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
            ] : [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.receiver_id.rawValue: receiver_id!,
            ]
            
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.fetchRequest(id: "", receiver_id: receiver_id, syncPlayManager: syncPlayManager, member: current_member)),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func fetchResponse(syncPlayManager: SyncPlayManagerModel, receiver_id: String) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON = [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.receiver_id.rawValue: receiver_id,
            ]
            
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.fetchResponse(id: "", receiver_id: receiver_id, syncPlayManager: syncPlayManager, member: current_member)),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!, to_parent: true))
            resolve(())
        }
    }
    
    func didStopPlayer() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.didStopPlayer(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
    
    func didPlayPlayer() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.didPlayPlayer(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
    
    func didSeekPlayer(syncPlayManager: SyncPlayManagerModel) -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.syncPlayManager.rawValue: syncPlayManager.toWebsocketPayload(),
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.didSeekPlayer(member: MemberEntity(), syncPlayManager: syncPlayManager)),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
    
    func didEditPlayer() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.didEditPlayer(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
    
    func endEditPlayer() -> Promise<Void> {
        return Promise<Void> { [unowned self] resolve, reject, _ in
            guard let current_member = GuildConnector.shared.current_member, let current_guild = GuildConnector.shared.current_guild, let current_channel = self.current_channel else { resolve(()); return }
            let payload: JSON =  [
                WebsocketStreamChannelPayloadType.client_member.rawValue: current_member.toWebsocketPayload()
            ]
            let json: JSON =  [
                WebSocketMessageType.payload.rawValue: payload,
                WebSocketMessageType.type.rawValue: WebsocketStreamChannelEvent.getString(.endEditPlayer(member: MemberEntity())),
            ]
            try await(WebSocketHandler.shared.sendMessage(json: json, roomname: current_guild.uid + WebSocketHandler.separator + current_channel.messageable_uid!))
            resolve(())
        }
    }
}

extension StreamChannelConnector: WebSocketHandlerDelegate {
    func webSocketHandler(
        event: WebsocketEvent,
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        let channel_event = WebsocketStreamChannelEvent.init(
            type: type,
            id: id,
            payload: payload,
            client_user: client_user,
            client_room: client_room
        )
        self.invoke({ $0.streamChannelConnector(event: channel_event) })
    }
    
    func webSocketHandler(event: WebsocketEvent) {}
    
    func webSocketHandler(onError: Error) {}
}


