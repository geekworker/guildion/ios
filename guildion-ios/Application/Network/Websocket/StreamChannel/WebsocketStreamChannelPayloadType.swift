//
//  WebsocketStreamChannelPayloadType.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation

enum WebsocketStreamChannelPayloadType: String {
    case client_member
    case client_participant
    case syncPlayManager
    case participant
    case receiver_id
}

