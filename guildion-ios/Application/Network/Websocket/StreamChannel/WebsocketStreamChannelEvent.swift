//
//  WebsocketStreamChannelEvent.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import Foundation
import UIKit
import Starscream
import SwiftyJSON

enum WebsocketStreamChannelEvent {
    case didParticipantJoin(member: MemberEntity)
    case didParticipantLeave(member: MemberEntity)
    
    case syncStream(id: String, syncPlayManager: SyncPlayManagerModel, member: MemberEntity)
    case changePlayerStatus(id: String, syncPlayManager: SyncPlayManagerModel, member: MemberEntity)
    case changePlayerLoop(id: String, syncPlayManager: SyncPlayManagerModel, member: MemberEntity)
    case transferHost(id: String, receiver_id: String, syncPlayManager: SyncPlayManagerModel, member: MemberEntity)
    case fetchRequest(id: String, receiver_id: String?, syncPlayManager: SyncPlayManagerModel, member: MemberEntity)
    case fetchResponse(id: String, receiver_id: String, syncPlayManager: SyncPlayManagerModel, member: MemberEntity)
    
    case didStopPlayer(member: MemberEntity)
    case didPlayPlayer(member: MemberEntity)
    case didSeekPlayer(member: MemberEntity, syncPlayManager: SyncPlayManagerModel)
    case didEditPlayer(member: MemberEntity)
    case endEditPlayer(member: MemberEntity)
    
    case other
    
    var string: String {
        switch self {
        case .didParticipantJoin: return "didParticipantJoin"
        case .didParticipantLeave: return "didParticipantLeave"
        case .syncStream: return "syncStream"
        case .changePlayerStatus: return "changePlayerStatus"
        case .changePlayerLoop: return "changePlayerLoop"
        case .transferHost: return "transferHost"
        case .fetchRequest: return "fetchRequest"
        case .fetchResponse: return "fetchResponse"
        case .didStopPlayer: return "didStopPlayer"
        case .didPlayPlayer: return "didPlayPlayer"
        case .didSeekPlayer: return "didSeekPlayer"
        case .didEditPlayer: return "didEditPlayer"
        case .endEditPlayer: return "endEditPlayer"
        
        case .other: return "other"
        }
    }
    
    static func getString(_ event: WebsocketStreamChannelEvent) -> String {
        switch event {
        case .didParticipantJoin: return "didParticipantJoin"
        case .didParticipantLeave: return "didParticipantLeave"
        case .syncStream: return "syncStream"
        case .changePlayerStatus: return "changePlayerStatus"
        case .changePlayerLoop: return "changePlayerLoop"
        case .transferHost: return "transferHost"
        case .fetchRequest: return "fetchRequest"
        case .fetchResponse: return "fetchResponse"
        case .didStopPlayer: return "didStopPlayer"
        case .didPlayPlayer: return "didPlayPlayer"
        case .didSeekPlayer: return "didSeekPlayer"
        case .didEditPlayer: return "didEditPlayer"
        case .endEditPlayer: return "endEditPlayer"
        
        case .other: return "other"
        }
    }
    
    init(
        type: String,
        id: String,
        payload: JSON,
        client_user: UserEntity,
        client_room: String
    ) {
        guard let client_member = MemberEntity.jsonable(payload[WebsocketStreamChannelPayloadType.client_member.rawValue])
        else {
            self = .other
            return
        }
        switch type {
        case WebsocketStreamChannelEvent.getString(.didParticipantJoin(member: MemberEntity())): self = .didParticipantJoin(member: client_member)
        case WebsocketStreamChannelEvent.getString(.didParticipantLeave(member: MemberEntity())): self = .didParticipantLeave(member: client_member)
        
        case WebsocketStreamChannelEvent.getString(.syncStream(id: id, syncPlayManager: SyncPlayManagerModel(), member: client_member)):
            guard let syncPlayManager = SyncPlayManagerModel.jsonable(payload[WebsocketStreamChannelPayloadType.syncPlayManager.rawValue]) else { self = .other; return }
            self = .syncStream(id: id, syncPlayManager: syncPlayManager, member: client_member)
        
        case WebsocketStreamChannelEvent.getString(.changePlayerStatus(id: id, syncPlayManager: SyncPlayManagerModel(), member: client_member)):
            guard let syncPlayManager = SyncPlayManagerModel.jsonable(payload[WebsocketStreamChannelPayloadType.syncPlayManager.rawValue]) else { self = .other; return }
            self = .changePlayerStatus(id: id, syncPlayManager: syncPlayManager, member: client_member)
        
        case WebsocketStreamChannelEvent.getString(.changePlayerLoop(id: id, syncPlayManager: SyncPlayManagerModel(), member: client_member)):
            guard let syncPlayManager = SyncPlayManagerModel.jsonable(payload[WebsocketStreamChannelPayloadType.syncPlayManager.rawValue]) else { self = .other; return }
            self = .changePlayerLoop(id: id, syncPlayManager: syncPlayManager, member: client_member)
            
        case WebsocketStreamChannelEvent.getString(.transferHost(id: id, receiver_id: "", syncPlayManager: SyncPlayManagerModel(), member: client_member)):
            guard let syncPlayManager = SyncPlayManagerModel.jsonable(payload[WebsocketStreamChannelPayloadType.syncPlayManager.rawValue]) else { self = .other; return }
            self = .transferHost(id: id, receiver_id: payload[WebsocketStreamChannelPayloadType.receiver_id.rawValue].stringValue, syncPlayManager: syncPlayManager, member: client_member)
            
        case WebsocketStreamChannelEvent.getString(.fetchRequest(id: id, receiver_id: "", syncPlayManager: SyncPlayManagerModel(), member: client_member)):
            guard let syncPlayManager = SyncPlayManagerModel.jsonable(payload[WebsocketStreamChannelPayloadType.syncPlayManager.rawValue]) else { self = .other; return }
            self = .fetchRequest(id: id, receiver_id: payload[WebsocketStreamChannelPayloadType.receiver_id.rawValue].string, syncPlayManager: syncPlayManager, member: client_member)
            
        case WebsocketStreamChannelEvent.getString(.fetchResponse(id: id, receiver_id: "", syncPlayManager: SyncPlayManagerModel(), member: client_member)):
            guard let syncPlayManager = SyncPlayManagerModel.jsonable(payload[WebsocketStreamChannelPayloadType.syncPlayManager.rawValue]) else { self = .other; return }
            self = .fetchResponse(id: id, receiver_id: payload[WebsocketStreamChannelPayloadType.receiver_id.rawValue].stringValue, syncPlayManager: syncPlayManager, member: client_member)

        case WebsocketStreamChannelEvent.getString(.didStopPlayer(member: MemberEntity())): self = .didStopPlayer(member: client_member)
        case WebsocketStreamChannelEvent.getString(.didPlayPlayer(member: MemberEntity())): self = .didPlayPlayer(member: client_member)
        case WebsocketStreamChannelEvent.getString(.didSeekPlayer(member: MemberEntity(), syncPlayManager: SyncPlayManagerModel())):
            guard let syncPlayManager = SyncPlayManagerModel.jsonable(payload[WebsocketStreamChannelPayloadType.syncPlayManager.rawValue]) else { self = .other; return }
            self = .didSeekPlayer(member: client_member, syncPlayManager: syncPlayManager)
        case WebsocketStreamChannelEvent.getString(.didEditPlayer(member: MemberEntity())): self = .didEditPlayer(member: client_member)
        case WebsocketStreamChannelEvent.getString(.endEditPlayer(member: MemberEntity())): self = .endEditPlayer(member: client_member)
        default: self = .other
        }
    }
}
