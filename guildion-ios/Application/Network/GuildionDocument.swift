//
//  GuildionDocument.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/23.
//

import Foundation
import UIKit

class GuildionDocument: UIDocument {
    public var text: String? = ""

    override func contents(forType typeName: String) throws -> Any {
        text?.data(using: .utf8) ?? Data()
    }

    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        guard let contents = contents as? Data else { return }
        text = String(data: contents, encoding: .utf8)
    }
}
