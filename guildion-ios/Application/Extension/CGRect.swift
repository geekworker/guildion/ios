//
//  CGRect.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/06.
//

import Foundation
import UIKit

extension CGRect {

    init(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) {

        self = CGRect(origin: CGPoint(x: x, y: y), size: CGSize(width: width, height: height))
    }

    static var one: CGRect {

        return CGRect(x: 0, y: 0, width: 1, height: 1)
    }
}
