//
//  Eureka.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/31.
//

import Foundation
import Eureka
import UIKit

extension ButtonRow: SelectableRowType {
    public var selectableValue: String? {
        get {
            self.tag
        }
        set(newValue) {
            self.tag = newValue
        }
    }
}
