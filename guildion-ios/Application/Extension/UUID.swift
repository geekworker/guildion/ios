//
//  UUID.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/13.
//

import Foundation

extension UUID {
    var uuidNoSymbol: String {
        return self.uuidString.replacingOccurrences(of: "-", with: "")
    }
}
