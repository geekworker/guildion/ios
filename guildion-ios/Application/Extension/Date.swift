//
//  Date.swift
//  selfinity
//
//  Created by Apple on 2018/10/17.
//  Copyright © 2018年 Apple. All rights reserved.
//

import UIKit

extension Date {
    static func UTCToLocal(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.sssZ" 
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dt ?? Date.CurrentDate()
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return dateFormatter.string(from: dt ?? Date.CurrentDate())
    }
    
    static func CurrentDate() -> Date {
        let format = DateFormatter()
        
        format.dateFormat =  "yyyy-MM-dd HH:mm:ss"
        format.timeZone   = TimeZone.current
        format.locale = Locale.current
        return format.string(from: Date()).getUTCDate()!
    }
    
    static func CurrentPrunedDate() -> Date {
        let format = DateFormatter()
        
        format.dateFormat =  "yyyy-MM-dd 00:00:00"
        format.timeZone   = TimeZone.current
        format.locale = Locale.current
        return format.string(from: Date()).getUTCDate()!
    }
    
    func getFormattedTime() -> String {
        let format = DateFormatter()
        format.dateFormat = "HH:mm"
        return format.string(from: self)
    }
    
    func convertLocalDate() -> Date {
        let format = DateFormatter()
        format.dateFormat =  "yyyy-MM-dd HH:mm:ss"
        format.timeZone   = TimeZone.current
        format.locale = Locale.current
        return format.string(from: self).getUTCDate()!
    }
    
    func convertJADate() -> Date {
        let format = DateFormatter()
        format.dateFormat =  "yyyy-MM-dd HH:mm:ss"
        format.timeZone   = TimeZone(identifier: "Asia/Tokyo")
        format.locale = Locale(identifier: "ja_JP")
        return format.string(from: self).getDate()!
    }
    
    static func getJADate() -> Date {
        let format = DateFormatter()
        format.dateFormat =  "yyyy-MM-dd HH:mm:ss"
        format.timeZone   = TimeZone(identifier: "Asia/Tokyo")
        return format.string(from: Date()).getDate()!
    }
    
    static func getJADateYMD() -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyy/MM/dd"
        format.timeZone   = TimeZone(identifier: "Asia/Tokyo")
        return format.string(from: Date())
    }
    
    static func getJADateH() -> String {
        let format = DateFormatter()
        format.dateFormat = "H"
        format.timeZone   = TimeZone(identifier: "Asia/Tokyo")
        return format.string(from: Date())
    }
    
    static func getJADatem() -> String {
        let format = DateFormatter()
        format.dateFormat = "m"
        format.timeZone   = TimeZone(identifier: "Asia/Tokyo")
        return format.string(from: Date())
    }
    
    static func getJADatemm() -> String {
        let format = DateFormatter()
        format.dateFormat = "mm"
        format.timeZone   = TimeZone(identifier: "Asia/Tokyo")
        return format.string(from: Date())
    }
    
    static func getJAYMD(_ date: Date) -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyy/MM/dd"
        format.timeZone   = TimeZone(identifier: "Asia/Tokyo")
        return format.string(from: date)
    }
}

extension Date {
    func fullDistance(from date: Date, resultIn component: Calendar.Component, calendar: Calendar = .current) -> Int? {
        calendar.dateComponents([component], from: self, to: date).value(for: component)
    }

    func distance(from date: Date, only component: Calendar.Component, calendar: Calendar = .current) -> Int {
        let days1 = calendar.component(component, from: self)
        let days2 = calendar.component(component, from: date)
        return days1 - days2
    }

    func hasSame(_ component: Calendar.Component, as date: Date) -> Bool {
        distance(from: date, only: component) == 0
    }
}

extension DateFormatter {
    enum Template: String {
        case date = "Md"      // 1/1
        case time = "Hm"     // 12:39:22
        case full = "yMdkHms" // 2017/1/1 12:39:22
        case onlyHour = "k"   // 17時
        case era = "GG"       // "西暦" (default) or "平成" (本体設定で和暦を指定している場合)
        case weekDay = "EEEE" // 日曜日
    }

    func setTemplate(_ template: Template) {
        dateFormat = DateFormatter.dateFormat(fromTemplate: template.rawValue, options: 0, locale: .current)
    }
}
