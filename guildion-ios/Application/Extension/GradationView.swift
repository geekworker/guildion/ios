//
//  GradationView.swift
//  selfinity
//
//  Created by Apple on 2018/10/07.
//  Copyright © 2018年 Apple. All rights reserved.
//

import UIKit

@IBDesignable
class GradationView: UIView {
    
    var gradientLayer: CAGradientLayer?
    
    @IBInspectable var topColor: UIColor = UIColor.white {
        didSet {
            setGradation()
        }
    }
    
    @IBInspectable var bottomColor: UIColor = UIColor.black {
        didSet {
            setGradation()
        }
    }
    
    @IBInspectable var startX: CGFloat = 0 {
        didSet {
            setGradation()
        }
    }
    
    @IBInspectable var startY: CGFloat = 0.5 {
        didSet {
            setGradation()
        }
    }
    
    @IBInspectable var endX: CGFloat = 1 {
        didSet {
            setGradation()
        }
    }
    
    @IBInspectable var endY: CGFloat = 0.5 {
        didSet {
            setGradation()
        }
    }
    
    func setGradation() {
        gradientLayer?.removeFromSuperlayer()
        gradientLayer = CAGradientLayer()
        gradientLayer!.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer?.startPoint = CGPoint(
            x: self.startX,
            y: self.startY
        )
        gradientLayer?.endPoint = CGPoint(
            x: endX,
            y: endY
        )
        gradientLayer!.frame.size = frame.size
        layer.addSublayer(gradientLayer!)
        layer.masksToBounds = true
    }
}
