//
//  UIViewController.swift
//  softbank_prototype
//
//  Created by Apple on 2018/08/30.
//  Copyright © 2018年 Apple. All rights reserved.
//


import UIKit
import RxSwift
import RxCocoa

extension UIViewController {
    enum Tags: Int {
        case indicator = 9999
    }
}

extension UIViewController {
    func isModal() -> Bool {
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.first != self {
                return false
            }
        }
        
        // Because I've segued many layers deep, this doesn't work
        //        if self.presentingViewController != nil {
        //            return true
        //        }
        
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController {
            return true
        }
        
        // Because I've segued many layers deep, this doesn't work
        //        if self.tabBarController?.presentingViewController is UITabBarController {
        //            return true
        //        }
        
        return false
    }
}

//extension UIViewController {
//    func dismissTabBarController() {
//        var viewController: UIViewController? = self
//        while viewController as? TabViewController == nil {
//            guard let presentingViewController = viewController?.presentingViewController else {
//                break
//            }
//            viewController = presentingViewController
//        }
//        (viewController as? TabViewController)?.updateMyPageTabIcon()
//        viewController?.dismiss(animated: true, completion: nil)
//    }
//}

extension UIViewController {
    static func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while topController.presentedViewController != nil {
            topController = topController.presentedViewController!
        }
        return topController
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIViewController {
    func setTabBarHidden(_ hidden: Bool, animated: Bool = true, duration: TimeInterval = 0.3) {
//        guard self.tabBarController?.tabBar.isHidden != hidden else { return }
        if animated {
            if let frame = self.tabBarController?.tabBar.frame {
                let factor: CGFloat = hidden ? 1 : -1
                let y = frame.origin.y + (frame.size.height * factor)
                UIView.animate(withDuration: duration, animations: {
                    self.tabBarController?.tabBar.frame = CGRect(x: frame.origin.x, y: y, width: frame.width, height: frame.height)
                })
                return
            }
        }
        self.tabBarController?.tabBar.isHidden = hidden
    }
}
