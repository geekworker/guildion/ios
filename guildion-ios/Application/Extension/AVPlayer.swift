//
//  AVPlayer.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/06.
//

import Foundation
import AVFoundation

extension AVPlayer {

    func isPlaying() -> Bool {

        return (self.rate != 0.0 && self.status == .readyToPlay)
    }
    
    func isPaused() -> Bool {

        return (self.rate == 0.0 && self.status == .readyToPlay)
    }
}
