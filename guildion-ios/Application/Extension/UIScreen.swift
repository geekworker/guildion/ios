//
//  UIScreen.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/06.
//

import Foundation
import UIKit

public extension UIScreen {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
