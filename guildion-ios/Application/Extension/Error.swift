//
//  Error.swift
//  guildion-ios
//
//  Created by Apple on 2019/06/17.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Alamofire

enum AppError: Error {
    case unknown
    case domainError
    case custom(reason: String)
}

extension AppError: LocalizedError {
    var errorDescription: String {
        switch self {
        case .unknown: return R.string.localizable.unknownError()
        case .domainError: return R.string.localizable.domainError()
        case .custom(let reason): return reason
        }
    }
}

extension Error {
    func getLocalizedReason() -> String {
        if let error = self as? AFError {
            switch error {
            case .invalidURL(_):
                return R.string.localizable.domainError()
//            case .parameterEncodingFailed(let reason):
//                return String(describing: reason)
//            case .multipartEncodingFailed(let reason):
//                return String(describing: reason)
//            case .responseValidationFailed(let reason):
//                switch reason {
//                case .dataFileNil, .dataFileReadFailed:
//                    return String(describing: reason)
//                case .missingContentType(let acceptableContentTypes):
//                    return String(describing: reason)
//                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
//                    return String(describing: reason)
//                case .unacceptableStatusCode(let code):
//                    return String(describing: reason)
//                case .customValidationFailed(let error):
//                    return String(describing: error)
//                }
//            case .responseSerializationFailed(let reason):
//                return String(describing: reason)
//            case .createUploadableFailed(error: let error):
//                return String(describing: error)
//            case .createURLRequestFailed(error: let error):
//                return String(describing: error)
//            case .downloadedFileMoveFailed(error: let error, source: let source, destination: let destination):
//                return String(describing: error)
//            case .explicitlyCancelled:
//                return String(describing: self)
//            case .parameterEncoderFailed(reason: let reason):
//                return String(describing: reason)
//            case .requestAdaptationFailed(error: let error):
//                return String(describing: error)
//            case .requestRetryFailed(retryError: _, originalError: _):
//                return R.string.localizable.domainError()
//            case .serverTrustEvaluationFailed(reason: let reason):
//                return String(describing: reason)
//            case .sessionDeinitialized:
//                return String(describing: self)
//            case .sessionInvalidated(error: let error):
//                return String(describing: error)
//            case .sessionTaskFailed(error: let error):
//                return String(describing: error)
            case .urlRequestValidationFailed(reason: _):
                return R.string.localizable.domainError()
            default: return R.string.localizable.domainError()
            }
        } else if self is URLError {
            return R.string.localizable.domainError()
        } else if let error = self as? AppError {
            return error.errorDescription
        } else {
            return String(describing: self)
        }
    }
    
    var by_internet_reason: Bool {
        if let error = self as? AFError {
            switch error {
            case .invalidURL(_):
                return true
            case .urlRequestValidationFailed(reason: _):
                return true
            default: return true
            }
        } else if self is URLError {
            return true
        } else if self is AppError {
            return false
        } else {
            return false
        }
    }
}
