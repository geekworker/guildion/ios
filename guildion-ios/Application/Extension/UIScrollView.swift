//
//  UIScrollView.swift
//  guildion-ios
//
//  Created by Apple on 2020/05/29.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa

enum ScrollDirection {
    case None
    case Up
    case Down
    case Left
    case Right
}

extension Reactive where Base: UIScrollView {
    private var contentOffsetDiff: Observable<(CGPoint, CGPoint)> {
        return Observable.zip(contentOffset, contentOffset.skip(1)) { ($0, $1) }
    }

    var verticalScrollDirection: Observable<ScrollDirection> {
        return contentOffsetDiff.flatMap { (old, new) in
            Observable<ScrollDirection>.create { observe in
                let direction = old.y < new.y ? ScrollDirection.Up : old.y > new.y ? .Down : .None
                observe.onNext(direction)
                observe.onCompleted()
                return Disposables.create()
            }
        }
    }

    var horizontalScrollDirection: Observable<ScrollDirection> {
        return contentOffsetDiff.flatMap { (old, new) in
            Observable<ScrollDirection>.create { observe in
                let direction = old.x < new.x ? ScrollDirection.Left : old.x > new.x ? .Right : .None
                observe.onNext(direction)
                observe.onCompleted()
                return Disposables.create()
            }
        }
    }
}

extension UIScrollView {
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
    }

    override open func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesMoved(touches, with: event)
    }

    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesEnded(touches, with: event)
    }
}
