//
//  TimeInterval.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/21.
//

import Foundation
import AVFoundation
import AVKit

extension TimeInterval {
    var minuteSecondMS: String {
        return String(format:"%d:%02d.%03d", minute, second, millisecond)
    }
    
    var hour: Int {
        return Int((self / 3600).truncatingRemainder(dividingBy: 60))
    }
    var minute: Int {
        return Int((self / 60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self * 1000).truncatingRemainder(dividingBy: 1000))
    }
    
    init(millisecond: Double) {
        self = TimeInterval(millisecond / 1000)
    }
    
    init(millisecond: Int) {
        self = TimeInterval(Double(millisecond) / 1000)
    }
}

extension CMTime {
    var float: Float {
        return Float(CMTimeGetSeconds(self))
    }
}

