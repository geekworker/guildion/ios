//
//  Log.swift
//  guildion-ios
//
//  Created by Apple on 2020/04/23.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class Log {
    
    static func LOG(_ body: String = "",
             function: String = #function,
             line: Int = #line)
    {
       print("[\(function) : \(line)] \(body)")
    }
}
