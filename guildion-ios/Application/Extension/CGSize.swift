//
//  CGSize.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/06.
//

import Foundation
import CoreGraphics

extension CGSize {
    func inverted() -> CGSize {
        return CGSize(width: self.height, height: self.width)
    }
}
