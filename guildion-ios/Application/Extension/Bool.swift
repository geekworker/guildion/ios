//
//  Bool.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/06.
//

import Foundation

extension Bool {

    mutating func flip() {

        self = !self
    }
}
