//
//  SyncPlayerConfig.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/18.
//

import Foundation
import UIKit

struct SyncPlayerConfig {
    static let min_diff_duration: Float = 2
    static func checkValidDiff(target_duration: Float, current_duration: Float) -> Bool {
        return abs(current_duration - target_duration) <= min_diff_duration
    }
    static let min_skip_duration_sc: Float = 10
    static let min_buffering_dispatch_interval_sc: TimeInterval = 1
    static var header_view_height: CGFloat {
        return UIScreen.main.hasNotch ? 240 : 200
    }
}
