//
//  Theme.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/07.
//

import Foundation
import UIKit

enum Themes: String, CaseIterable {
    case light = "light"
    case dark = "dark"
    
    static var current: Themes = Themes.dark
}

enum Theme: Int {
    case background
    case backgroundSecondary
    case backgroundThick
    case backgroundLight
    case backgroundContrast
    case transparent
    
    case string
    case stringSecondary
    case stringAccent
    case stringContrast
    case placeholder
    
    case shadow
    case border
    case borderSecondary
    case borderContrast
    
    case main
    case mainThick
    case mainLight
    
    case complementary
    case complementaryThick
    
    case cancel
    case alert
    case success
    case warning
    case danger
    case link
    case badge
    
    case searchBar
    case category
    case tag
    case playerTint
    case playerFilter
    case playerCancel
    
    static func themify(key: Theme) -> UIColor {
        switch Themes.current {
        case .dark:
            guard let theme = DarkTheme.init(rawValue: key.rawValue) else { return .white }
            return theme.color
        case .light:
            guard let theme = LightTheme.init(rawValue: key.rawValue) else { return .white }
            return theme.color
        }
    }
}

enum DarkTheme: Int {
    case background
    case backgroundSecondary
    case backgroundThick
    case backgroundLight
    case backgroundContrast
    case transparent
    
    case string
    case stringSecondary
    case stringAccent
    case stringContrast
    case placeholder
    
    case shadow
    case border
    case borderSecondary
    case borderContrast
    
    case main
    case mainThick
    case mainLight
    
    case complementary
    case complementaryThick
    
    case cancel
    case alert
    case success
    case warning
    case danger
    case link
    case badge
    
    case searchBar
    case category
    case tag
    case playerTint
    case playerFilter
    case playerCancel
    
    var color: UIColor {
        switch self {
        case .background: return Color.sub_black // .darker(by: 5)!
        case .backgroundSecondary: return Color.thick_black
        case .backgroundThick: return Color.black
        case .backgroundLight: return Color.light_black // .darker(by: 5)!
        case .backgroundContrast: return Color.white
        case .transparent: return Color.transparent
        case .string: return Color.white
        case .stringSecondary: return Color.green_gray
        case .stringAccent: return Color.emerald
        case .stringContrast: return Color.sub_black
        case .placeholder: return Color.sub_gray
        case .shadow: return Color.black.withAlphaComponent(0.8)
        case .border: return Color.thick_gray
        case .borderSecondary: return Color.green_gray
        case .borderContrast: return Color.white
        case .main: return Color.emerald
        case .mainThick: return Color.thick_emerald
        case .mainLight: return Color.light_emerald
        case .complementary: return Color.thick_orenge
        case .complementaryThick: return Color.dark_orenge
        case .cancel: return Color.thick_gray
        case .alert: return Color.red
        case .success: return Color.thick_green
        case .warning: return Color.orenge
        case .danger: return Color.red
        case .link: return Color.sky_blue
        case .badge: return Color.light_red
        case .searchBar: return Color.gray
        case .category: return Color.thick_emerald
        case .tag: return Color.green_gray
        case .playerTint: return Color.white
        case .playerFilter: return Color.fiter_black
        case .playerCancel: return Color.dark_white
        }
    }
}

enum LightTheme: Int {
    case background
    
    var color: UIColor {
        switch self {
        case .background: return Color.white
        }
    }
}
