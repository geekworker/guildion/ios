//
//  Constant.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation

struct Constant {
    static var LOCAL: Bool {
        get {
            #if DEVELOP
                return false
            #elseif STAGING
                return false
            #else
                return false
            #endif
        }
    }
    static var STAGING: Bool {
        get {
            #if DEVELOP
                return false
            #elseif STAGING
                return true
            #else
                return false
            #endif
        }
    }
    
    static let APP_NAME = "Guildion"
    static let APP_NAME_LATIN = "Guildion"
    static let APP_NAME_UPPERCASE = "GUILDION"
    static let HOST_NAME = "guildion"
    static let APP_DOMAIN = "guildion.us"
    static let APP_STAGING_IP = "http://3.209.221.175"
    static let APP_URL = LOCAL ?
    "http://0.0.0.0:8080" : (
        STAGING ?
        APP_STAGING_IP :
        "https://\(APP_DOMAIN)"
    )
    static let API_URL = LOCAL ?
    "http://0.0.0.0:8080/api/v1" : (
        STAGING ?
        "\(APP_STAGING_IP)/api/v1" :
        "https://\(APP_DOMAIN)/api/v1"
    )
    static let SIGNALING_APP_URL = /*LOCAL ? "http://0.0.0.0:1000/" :*/ "https://signaling.\(APP_DOMAIN)/"
    static let WS_URL = /*LOCAL ? "ws://localhost:1000/" :*/ "wss://signaling.guildion.us/"
    static let WS_CONNECT_URL = WS_URL + "signaling/v1/"
    static let APP_HOST = "\(APP_DOMAIN)"
    static let INC_NAME = "selfiinty"
    static let INC_FULL_NAME = "selfinity.inc"
    static let IMG_PROXY_URL = ENV.AWS.CloudFront.URL
    static let IMG_HOST_URL = "https://\(ENV.AWS.bucket).s3.\(ENV.AWS.region_string).amazonaws.com"
    static let IOS_URL_SCHEME = "guildion"
    static let IOS_URL_SCHEME_PROTOCOL = "guildion://"
}
