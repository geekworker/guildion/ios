//
//  Font.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation

struct Font {
    enum FontSizes: CGFloat {
        case XS = 8
        case FS = 10
        case S = 12
        case FM = 14
        case M = 16
        case FL = 18
        case L = 24
        case XL = 32
    }
    
    enum FontFamilies: String {
        case L = "Helvetica"
        case T = "Helvetica-Light"
        case B = "Helvetica-Bold"
        
//        Helvetica
//        Helvetica-Bold
//        Helvetica-BoldOblique
//        Helvetica-Light
//        Helvetica-LightOblique
//        Helvetica-Oblique
    }
    
    enum LignHeights: CGFloat {
        case M = 8
    }
    
    static func FS(key: FontSizes) -> CGFloat {
        return key.rawValue
    }
    
    static func FM(key: FontFamilies) -> String {
        return key.rawValue
    }
    
    static func LH(key: LignHeights) -> CGFloat {
        return key.rawValue
    }
    
    static func FT(size: FontSizes, family: FontFamilies) -> UIFont {
        return UIFont(
            name: Font.FM(
                key: family
            ),
            size: Font.FS(
                key: size
            )
        )!
    }
}

