//
//  ContentType.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/21.
//

import Foundation

enum ContentType: String {
    case other = "other"
    case txt = "text/plain"
    case csv = "text/csv"
    case html = "text/html"
    case css = "text/css"
    case javascript = "text/javascript"
    case exe = "application/octet-stream"
    case json = "application/json"
    case pdf = "application/pdf"
    case xls = "application/vnd.ms-excel"
    case xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    case ppt = "application/vnd.ms-powerpoint"
    case pptx = "application/vnd.openxmlformats-officedocument.presentationml.presentation"
    case doc = "application/msword"
    case docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    
    case bitmap = "image/bmp"
    case svg = "image/svg+xml"
    case zip = "application/zip"
    case lzh = "application/x-lzh"
    case tar = "application/x-tar"
    
    case ai = "application/postscript"
    case bmp, rle, dib = "image/x-bmp" // "image/x-ms-bmp"
    case cgm = "image/cgm"
    case dwf = "drawing/x-dwf"
    case epsf, eps, ps = "appilcation/postscript"
    case fif = "image/fif"
    case fpx = "image/fpx" // "image/x-fpx"
    case gif = "image/gif"
    case jpg, jpeg, jpe, jfif, jfi = "image/jpeg"
    case pcd = "image/pcd" // "image/x-photo-cd"
    case pict, pct = "image/pict"
    case png = "image/x-png" // "image/png"
    case tga, tpic, vda, vst = "image/x-targa"
    case tiff, tif = "image/tiff" // "image/x-tiff"
    case wrl = "model/vrml" // "x-world/x-vrml"
    case xbm = "image/x-bitmap"
    case xpm = "image/x-xpixmap"
    
    
    case aiff, aif = "audio/aiff" // "audio/x-aiff"
    case au, snd = "audio/basic"
    case kar, midi, mid, smf = "audio/midi" // "audio/x-midi"
    case m1a, m2a, mp2, mp3, mpa, mpega = "audio/mpeg" // "audio/x-mpeg"
    case rpm = "audio/x-pn-realaudio-plugin"
    // MARK: only swa is sound
    case swa, dcr, dir, dxr = "application/x-director"
    case vqf = "audio/x-twinvq"
    case wav = "audio/wav" // "audio/x-wav"
    
    
    case aab = "application/x-authorware-bin"
    case aam = "application/x-authorware-map"
    case aas = "application/x-authorware-seg"
    case asf = "video/x-ms-asf"
    case avi = "vide/x-msvideo"
    case flc, fli = "video/flc"
    case moov, mov, qt = "video/quicktime"
    case mp4 = "video/mp4"
    // MARK: _2s is equal 2s
    case mpeg, mpg, mpe, mpv, mng, m1s, m1v, _2s, m2v = "vide/mpeg" // "video/x-mpeg"
    
    case rm = "audio/x-pn-realaudio"
    case spl = "application/futuresplash"
    case swf = "application/x-shockwave-flash"
    case vdo = "video/vdo"
    case viv, vivo = "video/vnd.vivo"
    case xdm, xdma = "application/x-xdma"
    
    var file_extension: String {
        switch self {
        case .txt: return "txt"
        case .csv: return "csv"
        case .html: return "html"
        case .css: return "css"
        case .javascript: return "javascript"
        case .exe: return "exe"
        case .json: return "json"
        case .pdf: return "pdf"
        case .xls: return "xls"
        case .xlsx: return "xlsx"
        case .ppt: return "ppt"
        case .pptx: return "pptx"
        case .doc: return "doc"
        case .docx: return "docx"
        
        case .bitmap: return "bitmap"
        case .svg: return "svg"
        case .zip: return "zip"
        case .lzh: return "lzh"
        case .tar: return "tar"
        
        case .ai: return "ai"
        case .bmp: return "bmp"
        case .rle: return "rle"
        case .dib: return "dib"
        case .cgm: return "cgm"
        case .dwf: return "dwf"
        case .epsf: return "epsf"
        case .eps: return "eps"
        case .ps: return "ps"
        case .fif: return "fif"
        case .fpx: return "fpx"
        case .gif: return "gif"
        case .jpg: return "jpg"
        case .jpeg: return "jpeg"
        case .jpe: return "jpe"
        case .jfif: return "jfif"
        case .jfi: return "jfi"
        case .pcd: return "pcd"
        case .pict: return "pict"
        case .pct: return "pct"
        case .png: return "png"
        case .tga: return "tga"
        case .tpic: return "tpic"
        case .vda: return "vda"
        case .vst: return "vst"
        case .tiff: return "tiff"
        case .tif: return "tif"
        case .wrl: return "wrl"
        case .xbm: return "xbm"
        case .xpm: return "xpm"
        
        case .aiff: return "aiff"
        case .aif: return "aif"
        case .au: return "au"
        case .snd: return "snd"
        case .kar: return "kar"
        case .midi: return "midi"
        case .mid: return "mid"
        case .smf: return "smf"
        case .m1a: return "m1a"
        case .m2a: return "m2a"
        case .mp2: return "mp2"
        case .mp3: return "mp3"
        case .mpa: return "mpa"
        case .mpega: return "mpega"
        case .rpm: return "rpm"
        
        case .swa: return "swa"
        case .dcr: return "dcr"
        case .dir: return "dir"
        case .dxr: return "dxr"
        case .vqf: return "vqf"
        case .wav: return "wav"
        
        
        case .aab: return "aab"
        case .aam: return "aam"
        case .aas: return "aas"
        case .asf: return "asf"
        case .avi: return "avi"
        case .flc: return "flc"
        case .fli: return "fli"
        case .moov: return "moov"
        case .mov: return "mov"
        case .qt: return "qt"
        
        case .mpeg: return "mpeg"
        case .mpg: return "mpg"
        case .mpe: return "mpe"
        case .mpv: return "mpv"
        case .mng: return "mng"
        case .m1s: return "m1s"
        case .m1v: return "m1v"
        // MARK: _2s is equal 2s
        case ._2s: return "2s"
        case .m2v: return "m2v"
        case .mp4: return "mp4"
        
        case .rm: return "rm"
        case .spl: return "spl"
        case .swf: return "swf"
        case .vdo: return "vdo"
        case .viv: return "viv"
        case .vivo: return "vivo"
        case .xdm: return "xdm"
        case .xdma: return "xdma"
            
        default: return ""
        }
    }
    
    init(file_extension: String) {
        switch file_extension.replacingOccurrences(of: ".", with: "").lowercased() {
        case "txt": self = .txt
        case "csv": self = .csv
        case "html": self = .html
        case "css": self = .css
        case "javascript": self = .javascript
        case "exe": self = .exe
        case "json": self = .json
        case "pdf": self = .pdf
        case "xls": self = .xls
        case "xlsx": self = .xlsx
        case "ppt": self = .ppt
        case "pptx": self = .pptx
        case "doc": self = .doc
        case "docx": self = .docx
        
        case "bitmap": self = .bitmap
        case "svg": self = .svg
        case "zip": self = .zip
        case "lzh": self = .lzh
        case "tar": self = .tar

        case "ai": self = .ai
        case "bmp": self = .bmp
        case "rle": self = .rle
        case "dib": self = .dib
        case "cgm": self = .cgm
        case "dwf": self = .dwf
        case "epsf": self = .epsf
        case "eps": self = .eps
        case "ps": self = .ps
        case "fif": self = .fif
        case "fpx": self = .fpx
        case "gif": self = .gif
        case "jpg": self = .jpg
        case "jpeg": self = .jpeg
        case "jpe": self = .jpe
        case "jfif": self = .jfif
        case "jfi": self = .jfi
        case "pcd": self = .pcd
        case "pict": self = .pict
        case "pct": self = .pct
        case "png": self = .png
        case "tga": self = .tga
        case "tpic": self = .tpic
        case "vda": self = .vda
        case "vst": self = .vst
        case "tiff": self = .tiff
        case "tif": self = .tif
        case "wrl": self = .wrl
        case "xbm": self = .xbm
        case "xpm": self = .xpm
        
        case "aiff": self = .aiff
        case "aif": self = .aif
        case "au": self = .au
        case "snd": self = .snd
        case "kar": self = .kar
        case "midi": self = .midi
        case "mid": self = .mid
        case "smf": self = .smf
        case "m1a": self = .m1a
        case "m2a": self = .m2a
        case "mp2": self = .mp2
        case "mp3": self = .mp3
        case "mpa": self = .mpa
        case "mpega": self = .mpega
        case "rpm": self = .rpm
        
        case "swa": self = .swa
        case "dcr": self = .dcr
        case "dir": self = .dir
        case "dxr": self = .dxr
        case "vqf": self = .vqf
        case "wav": self = .wav
        
        
        case "aab": self = .aab
        case "aam": self = .aam
        case "aas": self = .aas
        case "asf": self = .asf
        case "avi": self = .avi
        case "flc": self = .flc
        case "fli": self = .fli
        case "moov": self = .moov
        case "mov": self = .mov
        case "qt": self = .qt
        
        case "mpeg": self = .mpeg
        case "mpg": self = .mpg
        case "mpe": self = .mpe
        case "mpv": self = .mpv
        case "mng": self = .mng
        case "m1s": self = .m1s
        case "m1v": self = .m1v
        // MARK: _2s is equal 2s
        case "2s": self = ._2s
        case "m2v": self = .m2v
        case "mp4": self = .mp4
        
        case "rm": self = .rm
        case "spl": self = .spl
        case "swf": self = .swf
        case "vdo": self = .vdo
        case "viv": self = .viv
        case "vivo": self = .vivo
        case "xdm": self = .xdm
        case "xdma": self = .xdma
            
        default: self = .other
        }
    }
    
    var is_movie: Bool {
        switch self {
        case .aab: return true
        case .aam: return true
        case .aas: return true
        case .asf: return true
        case .avi: return true
        case .flc: return true
        case .fli: return true
        case .moov: return true
        case .mov: return true
        case .qt: return true
        case .mpeg: return true
        case .mpg: return true
        case .mpe: return true
        case .mpv: return true
        case .mng: return true
        case .m1s: return true
        case .m1v: return true
        case ._2s: return true
        case .m2v: return true
        case .rm: return true
        case .spl: return true
        case .swf: return true
        case .vdo: return true
        case .viv: return true
        case .vivo: return true
        case .xdm: return true
        case .xdma: return true
        case .mp4: return true
        default: return false
        }
    }
    
    var is_sound: Bool {
        switch self {
        case .aiff: return true
        case .aif: return true
        case .au: return true
        case .snd: return true
        case .kar: return true
        case .midi: return true
        case .mid: return true
        case .smf: return true
        case .m1a: return true
        case .m2a: return true
        case .mp2: return true
        case .mp3: return true
        case .mpa: return true
        case .mpega: return true
        case .rpm: return true
        case .swa: return true
        case .dcr: return true
        case .dir: return true
        case .dxr: return true
        case .vqf: return true
        case .wav: return true
        default: return false
        }
    }
    
    var is_image: Bool {
        switch self {
        case .ai: return true
        case .bmp: return true
        case .rle: return true
        case .dib: return true
        case .cgm: return true
        case .dwf: return true
        case .epsf: return true
        case .eps: return true
        case .ps: return true
        case .fif: return true
        case .fpx: return true
        case .gif: return true
        case .jpg: return true
        case .jpeg: return true
        case .jpe: return true
        case .jfif: return true
        case .jfi: return true
        case .pcd: return true
        case .pict: return true
        case .pct: return true
        case .png: return true
        case .tga: return true
        case .tpic: return true
        case .vda: return true
        case .vst: return true
        case .tiff: return true
        case .tif: return true
        case .wrl: return true
        case .xbm: return true
        case .xpm: return true
        default: return false
        }
    }
}

