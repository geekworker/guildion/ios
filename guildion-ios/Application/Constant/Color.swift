//
//  Color.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/07.
//

import Foundation
import UIKit

struct Color {
    static let emerald = UIColor(hex: "22B4A5")
    static let thick_emerald = UIColor(hex: "0B8C75")
    static let light_emerald = UIColor(hex: "52F2D5")
    
    static let thick_green = UIColor(hex: "30FA30")
    static let orenge = UIColor(hex: "FFB652")
    static let thick_orenge = UIColor(hex: "B55122")
    static let dark_orenge = UIColor(hex: "692100")
    static let red = UIColor(hex: "ff555e")
    static let thick_red = UIColor(hex: "B21311")
    static let light_red = UIColor(hex: "FF555E")
    
    static let sky_blue = UIColor(hex: "3F7BE8")
    
    static let fiter_black = UIColor(red: 255, green: 255, blue: 255, alpha: 0.6)
    static let thick_black = UIColor(hex: "1F1F1F")
    static let black = UIColor(hex: "262626")
    static let sub_black = UIColor(hex: "30333B")
    static let light_black = UIColor(hex: "41444C")
    static let thick_gray = UIColor(hex: "676C7B")
    static let gray = UIColor(hex: "DBDBDB")
    static let sub_gray = UIColor(hex: "A2A2A2")
    static let green_gray = UIColor(hex: "8FA5B5")
    static let white = UIColor.white
    static let dark_white = UIColor(hex: "ADADAD")
    
    static let transparent = UIColor.clear
}

