//
//  PlanConfig.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import UIKit
import Foundation
import AVFoundation

struct PlanConfig {
    static public var byteCountFormatter: ByteCountFormatter {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = .useAll
        formatter.countStyle = .file
        formatter.includesUnit = true
        formatter.isAdaptive = true
        return formatter
    }
    static let free_plan_max_participant_count: Int = 30
    static let free_plan_max_size_byte: Double = 524288000
    static var free_plan_max_size: String {
        byteCountFormatter.string(fromByteCount: Int64(free_plan_max_size_byte))
    }


    enum SubscriptionSpan: Int, CaseIterable {
        case month1
        case month3
        case year
        case count

        var string: String {
            switch self {
            case .month1: return R.string.localizable.month()
            default: return R.string.localizable.planMonths()
            }
        }

        var count: Int {
            switch self {
            case .month1: return 1
            case .month3: return 3
            case .year: return 12
            default: return 0
            }
        }

        func price(plan: SubscriptionPlan) -> Int {
            switch plan {
            case .volumeMini:
                switch self {
                case .month1: return 300
                case .month3: return 750
                case .year: return 2800
                default: return 0
                }
            case .volumeStandardMini:
                switch self {
                case .month1: return 600
                case .month3: return 1600
                case .year: return 5600
                default: return 0
                }
            case .volumeStandard:
                switch self {
                case .month1: return 1500
                case .month3: return 4000
                case .year: return 12000
                default: return 0
                }
            case .volumeStandardBig:
                switch self {
                case .month1: return 2500
                case .month3: return 6000
                case .year: return 22500
                default: return 0
                }
            }
        }

        func priceString(plan: SubscriptionPlan) -> String {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            return "¥\(numberFormatter.string(from: NSNumber(value: self.price(plan: plan))) ?? "0")"
        }

        func monthPriceString(plan: SubscriptionPlan) -> String {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            return self != .month1 ? "¥\(numberFormatter.string(from: NSNumber(value: self.price(plan: plan) / self.count)) ?? "0")/\(R.string.localizable.monthPrefix())" : ""
        }

        func discount(plan: SubscriptionPlan) -> Int {
            switch plan {
            case .volumeMini:
                switch self {
                case .month1: return 0
                case .month3: return 17
                case .year: return 23
                default: return 0
                }
            case .volumeStandardMini:
                switch self {
                case .month1: return 0
                case .month3: return 12
                case .year: return 23
                default: return 0
                }
            case .volumeStandard:
                switch self {
                case .month1: return 0
                case .month3: return 12
                case .year: return 23
                default: return 0
                }
            case .volumeStandardBig:
                switch self {
                case .month1: return 0
                case .month3: return 20
                case .year: return 25
                default: return 0
                }
            }
        }

        func discountString(plan: SubscriptionPlan) -> String {
            switch plan {
            case .volumeMini:
                switch self {
                case .month1: return R.string.localizable.planSuperDiscount()
                default: return R.string.localizable.planDiscount("\(self.discount(plan: plan))%")
                }
            case .volumeStandardMini:
                switch self {
                case .month1: return R.string.localizable.planGoodDiscount()
                default: return R.string.localizable.planDiscount("\(self.discount(plan: plan))%")
                }
            case .volumeStandard:
                switch self {
                case .month1: return R.string.localizable.planNiceDiscount()
                default: return R.string.localizable.planDiscount("\(self.discount(plan: plan))%")
                }
            case .volumeStandardBig:
                switch self {
                case .month1: return R.string.localizable.planNiceDiscount()
                default: return R.string.localizable.planDiscount("\(self.discount(plan: plan))%")
                }
            }
        }
    }

    enum SubscriptionPlan: Int, CaseIterable {
        case volumeMini
        case volumeStandardMini
        case volumeStandard
        case volumeStandardBig

        var string: String {
            switch self {
            case .volumeMini: return R.string.localizable.planMini()
            case .volumeStandardMini: return R.string.localizable.planStandardMini()
            case .volumeStandard: return R.string.localizable.planStandard()
            case .volumeStandardBig: return R.string.localizable.planStandardBig()
            }
        }

        var storage_byte: Double {
            switch self {
            case .volumeMini: return 5368709120
            case .volumeStandardMini: return 16106127360
            case .volumeStandard: return 53687091200
            case .volumeStandardBig: return 107374182400
            }
        }

        var participant_max_limit: Int {
            switch self {
            case .volumeMini: return 10
            case .volumeStandardMini: return 20
            case .volumeStandard: return 50
            case .volumeStandardBig: return 100
            }
        }

        var enable_original_reaction: Bool {
            switch self {
            case .volumeMini: return true
            case .volumeStandardMini: return true
            case .volumeStandard: return true
            case .volumeStandardBig: return true
            }
        }

        func functionString(function: PlanFunction) -> String {
            switch self {
            case .volumeMini:
                switch function {
                case .volume: return byteCountFormatter.string(fromByteCount: Int64(self.storage_byte))
                case .participant: return "\(self.participant_max_limit)"
                case .reaction: return "○"
                default: return ""
                }
            case .volumeStandardMini:
                switch function {
                case .volume: return byteCountFormatter.string(fromByteCount: Int64(self.storage_byte))
                case .participant: return "\(self.participant_max_limit)"
                case .reaction: return "○"
                default: return ""
                }
            case .volumeStandard:
                switch function {
                case .volume: return byteCountFormatter.string(fromByteCount: Int64(self.storage_byte))
                case .participant: return "\(self.participant_max_limit)"
                case .reaction: return "○"
                default: return ""
                }
            case .volumeStandardBig:
                switch function {
                case .volume: return byteCountFormatter.string(fromByteCount: Int64(self.storage_byte))
                case .participant: return "\(self.participant_max_limit)"
                case .reaction: return "○"
                default: return ""
                }
            }
        }
    }

    enum PlanFunction: Int, CaseIterable {
        case volume
        case participant
        case reaction
        case count

        var string: String {
            switch self {
            case .volume: return R.string.localizable.planFunctionVolume()
            case .participant: return R.string.localizable.planFunctionParticipant()
            case .reaction: return R.string.localizable.planFunctionReaction()
            default: return ""
            }
        }
    }
}
