//
//  AppConfig.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/26.
//

import Foundation
import UIKit
import RxSwift

struct AppConfig {
    static var is_background: Bool = false {
        didSet {
            isBackgroundSubject.onNext(is_background)
        }
    }
    static private var isBackgroundSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    static var isBackgroundEvent: Observable<Bool> { return isBackgroundSubject }
    
    static var session_out: Bool = false {
        didSet {
            sessionOutSubject.onNext(session_out)
        }
    }
    static private var sessionOutSubject: PublishSubject<Bool>  = PublishSubject<Bool>()
    static var sessionOutEvent: Observable<Bool> { return sessionOutSubject }
    
    static let email_confirm_mode: Bool = false
    static var should_open_universal_url: URL? = nil
}
