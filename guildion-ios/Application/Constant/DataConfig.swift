//
//  DataConfig.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation

struct DataConfig {
    
    enum DataSize: Int {
        case XS
        case FS
        case S
        case FM
        case M
        case FL
        case L
        case XL
    }
    
    static func fetch_data_limit(_ size: DataSize) -> Int {
        switch (size) {
        case .XS:
            return 1
        case .FS:
            return 5
        case .S:
            return 10
        case .FM:
            return 20
        case .M:
            return 50
        case .FL:
            return 70
        case .L:
            return 100
        case .XL:
            return 500
        }
    }
    
    static func fetch_data_offset(_ size: DataSize) -> Int {
        switch (size) {
        case .XS:
            return 1
        case .FS:
            return 5
        case .S:
            return 10
        case .FM:
            return 20
        case .M:
            return 50
        case .FL:
            return 70
        case .L:
            return 100
        case .XL:
            return 500
        }
    }
    
    static func concurrency_size(_ size: DataSize) -> Int {
        switch (size) {
        case .XS:
            return 1
        case .FS:
            return 5
        case .S:
            return 10
        case .FM:
            return 20
        case .M:
            return 50
        case .FL:
            return 70
        case .L:
            return 100
        case .XL:
            return 500
        }
    }
    
    struct Image {
        static let default_guild_images = [
            ENV.AWS.CloudFront.URL + "/public/images/guilds/green.png",
            ENV.AWS.CloudFront.URL + "/public/images/guilds/blue.png",
            ENV.AWS.CloudFront.URL + "/public/images/guilds/pink.png",
            ENV.AWS.CloudFront.URL + "/public/images/guilds/purple.png",
            ENV.AWS.CloudFront.URL + "/public/images/guilds/yellow.png",
        ]
        static var default_guild_image_url: String {
            return default_guild_images[safe: Int.random(in: 0 ... default_guild_images.count)] ?? ""
        }
        static let default_profile_images = [
            ENV.AWS.CloudFront.URL + "/public/images/profiles/green.png",
            ENV.AWS.CloudFront.URL + "/public/images/profiles/blue.png",
            ENV.AWS.CloudFront.URL + "/public/images/profiles/pink.png",
            ENV.AWS.CloudFront.URL + "/public/images/profiles/purple.png",
            ENV.AWS.CloudFront.URL + "/public/images/profiles/yellow.png",
        ]
        static var default_profile_image_url: String {
            return default_profile_images[safe: Int.random(in: 0 ... default_profile_images.count)] ?? ""
        }
        static let faviconURL = "http://www.google.com/s2/favicons?sz=256&domain="
    }
    
    struct Session {
        static let email_code_digits = 6;
    }
    
    struct Stream {
        static let max_connection_count = 30;
    }
    
    struct Role {
        static let max_priority_limit = 100
    }
}
