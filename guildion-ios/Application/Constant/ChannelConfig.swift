//
//  ChannelConfig.swift
//  guildion-ios
//
//  Created by Apple on 2021/03/01.
//

import Foundation
import UIKit

struct ChannelConfig {
    static let headerless_max_interval_sc: TimeInterval = 300
}
