//
//  LiveConfig.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/31.
//

import UIKit
import Foundation
import AVFoundation

struct LiveConfig {
    static let defaultPosition: AVCaptureDevice.Position = .back
    static let defaultCameraMode: CameraLotateType = .back
    static var playingViewController: UINavigationController?
    static var playingStreamChannelViewController: StreamChannelViewController? {
        playingViewController?.viewControllers.first as? StreamChannelViewController
    }
    static var LiveStreamChannel: StreamChannelEntity? {
        playingStreamChannelViewController?.repository
    }
    static var LiveStream: StreamEntity? {
        playingStreamChannelViewController?.repository.LiveStream
    }
    static var min_meter_level: Float = 0.5
    static var max_start_offset: Double = 10
    static var min_start_offset: Double = 10
    static var fetch_request_timer_duration: Double = 1
}

