//
//  CameraRotateType.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/01.
//

import Foundation
import UIKit
import AVFoundation
import WebRTC
import Starscream
import SwiftyJSON
import VideoToolbox

public enum CameraLotateType: String {
    case front
    case back
    case none
    
    init(_ string: String) {
        switch string {
        case CameraLotateType.front.rawValue:
            self = .front
        case CameraLotateType.back.rawValue:
            self = .back
        case CameraLotateType.none.rawValue:
            self = .none
        default:
            self = .none
        }
    }
    
    var isVideo: Bool {
        return self != .none
    }
}

