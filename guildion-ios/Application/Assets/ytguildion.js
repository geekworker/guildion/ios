// MARK: @params
var defaultPlayerPosition = 48;
var cued = false;

// MARK: utils
function clickOrigin(e){
    var target = e.target;
    var tag = [];
    tag.tagType = target.tagName.toLowerCase();
    tag.tagClass = target.className.split(' ');
    tag.id = target.id;
    tag.parent = target.parentNode;
    return tag;
}

function getParams(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/**
 * Stop an iframe or HTML5 <video> from playing
 * @param  {Element} element The element that contains the video
 */
function stopVideo() {
    var iframe = document.querySelector('iframe');
    var video = document.querySelector('video');
    var container = document.getElementById("player-control-container");
    if (iframe) {
        var iframeSrc = iframe.src;
        iframe.src = iframeSrc;
        iframe.remove();
        cued = true;
    }
    if (video) {
        video.pause();
        video.remove();
        cued = true;
    }
    if (container) {
        container.remove();
        cued = true;
    }
};

function makeCueYT() {
    var elements = document.getElementsByClassName("ytp-cued-thumbnail-overlay");
    if (elements.length > 0) {
        elements[0].style.display = "block"
    }
}

function disableYTPlayerClick() {
    document.getElementById("movie_player").style["pointer-events"] = "none";
};

function dispatchRect() {
    var playerElement = document.getElementById("player");
    if (playerElement) {
        var width = playerElement.clientWidth,
            height = playerElement.clientHeight,
            rect = playerElement.getBoundingClientRect(),
            left = rect.left,
            top = rect.top;
        window.location.href = `ytguildion://onRectChange?height=${height}&width=${width}&x=${left}&y=${top}`;
    }
}

function addYTPlayerClickListener() {
    const player = document.getElementById("player");
    if (!!player) {
        document.getElementById("player").onclick = function(event) {
            window.location.href = `ytguildion://onYTPlayerClick`;
        }
    }
}

function addYTCommentClickListener() {
    const list = document.querySelectorAll('lazy-list')
    if (!!list && list.length > 1) {
        document.querySelectorAll('lazy-list')[1].onclick = function(e) {
            document.querySelector("ytm-engagement-panel").style.top = `${defaultPlayerPosition}px`;
            document.getElementsByClassName("engagement-panel-section-list-header")[0].onclick = function(e) {
            }
        }
    }
}

function detectYTDurationClick(e) {
    const element = clickOrigin(e);
    const target = e.target;
    if (element.tagType == "a" && !!target.href) {
        const _href = target.href;
        const href = _href.replace(window.location.hostname, "").replace("https://", "");
        if (!!href && !!getParams("v", href) && !!getParams("t", href)) {
            cued = false;
            window.location.href = `ytguildion://shouldDurationSkip?at=${getParams("t", href).replace("s", "")}`;
        }
    }
}

document.body.onclick = function(e){
    detectYTDurationClick(e);
};

window.onresize = function() {
    dispatchRect();
    disableYTPlayerClick()
}

window.onpopstate = function() {
    cued = false;
    stopVideo();
    makeCueYT();
    addYTPlayerClickListener();
    addYTCommentClickListener();
}

window.addEventListener('scroll', function(e) {
    var last_known_scroll_position = window.scrollY;
    var containerElement = document.getElementById("player-container-id");
    if (containerElement) {
        containerElement.style.top = String(
            Math.min(defaultPlayerPosition - last_known_scroll_position, defaultPlayerPosition)
        ) + "px";
    }
});

window.addEventListener('click', function(e) {
    disableYTPlayerClick()
    stopVideo();
    makeCueYT();
    addYTPlayerClickListener();
    addYTCommentClickListener();
});

document.addEventListener('transitionend', function(e) {
    disableYTPlayerClick()
    dispatchRect();
    stopVideo();
    makeCueYT();
    addYTPlayerClickListener();
    addYTCommentClickListener();
});

window.addEventListener('beforeunload', function(e) {
    cued = false;
    stopVideo();
    makeCueYT();
    disableYTPlayerClick();
    addYTPlayerClickListener();
    addYTCommentClickListener();
});

window.addEventListener('unload', function(e) {
    cued = false;
    stopVideo();
    makeCueYT();
    addYTPlayerClickListener();
    addYTCommentClickListener();
});

setInterval(function() {
    if (cued) { return }
    stopVideo();
    makeCueYT();
    addYTPlayerClickListener();
    addYTCommentClickListener();
}, 100);
