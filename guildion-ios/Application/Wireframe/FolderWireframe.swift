//
//  FolderWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit

class FolderWireframe: Wireframe {
    typealias ViewController = FolderViewController
    
    fileprivate weak var viewController: FolderViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toFileNew() {
        let vc = R.storyboard.fileNew().instantiateViewController(withIdentifier:
                                                                    R.storyboard.fileNew.fileNewViewController.identifier)
        vc.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(vc, animated: true)
    }
    
    func toFileSelect() {
        let navVC = R.storyboard.folders.fileSelectFromFolderViewController()!,
            vc = navVC.viewControllers.first as! FileSelectFromFolderViewController
        vc.delegate = viewController
        vc.current_guild = viewController!.current_guild
        navVC.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(navVC, animated: true)
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_member = GuildConnector.shared.current_member ?? MemberEntity()
            vc.current_guild = viewController!.current_guild
            vc.permission = GuildConnector.shared.current_permission
            viewController?.presentPanModal(vc)
        }
    }
}

