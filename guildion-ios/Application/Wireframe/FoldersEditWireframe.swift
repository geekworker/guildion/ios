//
//  FoldersEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/31.
//

import Foundation
import UIKit

class FoldersEditWireframe: Wireframe {
    typealias ViewController = FoldersEditViewController
    
    fileprivate weak var viewController: FoldersEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toFolderEdit() {
        viewController?.performSegue(withIdentifier: R.segue.foldersEditViewController.toFolderEdit.identifier, sender: nil)
    }
}
