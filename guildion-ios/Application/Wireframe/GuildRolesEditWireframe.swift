//
//  GuildRolesEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class GuildRolesEditWireframe: Wireframe {
    typealias ViewController = GuildRolesEditViewController
    
    fileprivate weak var viewController: GuildRolesEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toRoleEdit() {
        self.viewController?.performSegue(withIdentifier: R.segue.guildRolesEditViewController.toRoleEdit.identifier, sender: nil)
    }
}
