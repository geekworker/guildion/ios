//
//  GuildNewWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/12.
//

import Foundation
import UIKit

class GuildNewWireframe: Wireframe {
    typealias ViewController = GuildNewViewController
    
    fileprivate weak var viewController: GuildNewViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toPublicSetting() {
        viewController?.performSegue(withIdentifier: R.segue.guildNewViewController.toPublicSetting, sender: nil)
    }
}
