//
//  MailConfirmWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import UIKit

class MailConfirmWireframe: Wireframe {
    typealias ViewController = MailConfirmViewController
    
    fileprivate weak var viewController: MailConfirmViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toGuildNew() {
    }
    
    func toProfileSetting() {
        viewController?.performSegue(withIdentifier: R.segue.mailConfirmViewController.toProfileSetting.identifier, sender: nil)
    }
}
