//
//  GuildMemberRequestsWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class GuildMemberRequestsWireframe: Wireframe {
    typealias ViewController = GuildMemberRequestsViewController
    
    fileprivate weak var viewController: GuildMemberRequestsViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toUserMenu(repository: UserEntity) {
        if let vc = R.storyboard.guilds.userMenuViewController() {
            vc.repository = repository
            vc.current_guild = GuildConnector.shared.current_guild ?? GuildEntity()
            vc.current_member = GuildConnector.shared.current_member ?? MemberEntity()
            vc.permission = GuildConnector.shared.current_permission
            viewController?.presentPanModal(vc)
        }
    }
}
