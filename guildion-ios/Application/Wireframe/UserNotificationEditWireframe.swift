//
//  UserNotificationEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class UserNotificationEditWireframe: Wireframe {
    typealias ViewController = UserNotificationEditViewController
    
    fileprivate weak var viewController: UserNotificationEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
