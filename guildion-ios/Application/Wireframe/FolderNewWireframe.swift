//
//  FolderNewWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class FolderNewWireframe: Wireframe {
    typealias ViewController = FolderNewViewController
    
    fileprivate weak var viewController: FolderNewViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
