//
//  ChannelMenuWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/28.
//

import Foundation
import UIKit

class ChannelMenuWireframe: Wireframe {
    typealias ViewController = ChannelMenuViewController
    
    fileprivate weak var viewController: ChannelMenuViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toChannelSetting() {
        if let vc = R.storyboard.channelSetting.channelSettingViewControllerNavigation(), let channelSettingViewController = vc.viewControllers.first as? ChannelSettingViewController, let repository = viewController?.repository {
            // vc.modalPresentationStyle = .overFullScreen
            channelSettingViewController.repository = repository
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toChannelNotificationEdit() {
        if let vc = R.storyboard.channelSetting.channelNotificationEditViewControllerNavigation(), let channelNotificationEditViewController = vc.viewControllers.first as? ChannelNotificationEditViewController, let repository = viewController?.repository {
             channelNotificationEditViewController.channel = repository
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toMemberSelect() {
        if let vc = R.storyboard.memberSelect.memberSelectViewControllerNavigation(), let memberSelectViewController = vc.viewControllers.first as? MemberSelectViewController, let repository = viewController?.repository {
            memberSelectViewController.repository = repository
            memberSelectViewController.is_invite = true
            memberSelectViewController.delegate = viewController
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_guild = GuildConnector.shared.current_guild ?? GuildEntity()
            vc.current_member = GuildConnector.shared.current_member ?? MemberEntity()
            vc.permission = viewController!.permission
            viewController?.navigationController?.popViewController(animated: true)
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().presentPanModal(vc)
            })
        }
    }
    
    func toMembersEdit(repository: ChannelableProtocol) {
        if let vc = R.storyboard.channelSetting.channelMembersEditViewControllerNavigation(), let channelMembersEditViewController = vc.viewControllers.first as? ChannelMembersEditViewController, let repository = viewController?.repository {
            channelMembersEditViewController.repository = repository
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
}

