//
//  GuildShareWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class GuildShareWireframe: Wireframe {
    typealias ViewController = GuildShareViewController
    
    fileprivate weak var viewController: GuildShareViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
