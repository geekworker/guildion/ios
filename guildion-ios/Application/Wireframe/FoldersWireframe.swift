//
//  FoldersWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/23.
//

import Foundation
import UIKit

class FoldersWireframe: Wireframe {
    typealias ViewController = FoldersViewController
    
    fileprivate weak var viewController: FoldersViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toFileNew() {
        if let vc = R.storyboard.fileNew().instantiateViewController(
            withIdentifier: R.storyboard.fileNew.fileNewViewController.identifier
        ) as? UINavigationController, let fileNewViewController = vc.viewControllers.first as? FileNewViewController {
            vc.modalPresentationStyle = .overCurrentContext
            fileNewViewController.initRepository(current_guild: viewController!.current_guild, current_member: viewController!.current_member)
            fileNewViewController.delegate = viewController
            fileNewViewController.didCreateCallback = { self.viewController?.fileNewViewController(didCreate: $0) }
            fileNewViewController.assetsPickerHandler.allowImages = !(viewController?.is_playlist ?? false)
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toFolder() {
        viewController?.hidesBottomBarWhenPushed = true
        viewController?.performSegue(withIdentifier: R.segue.foldersViewController.toFolder, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toPlaylist() {
        viewController?.hidesBottomBarWhenPushed = true
        viewController?.performSegue(withIdentifier: R.segue.foldersViewController.toPlaylist, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toFile(_ repository: FileEntity) {
        if let vc = R.storyboard.folders.fileMenuViewController() {
            vc.repository = repository
            vc.delegate = self.viewController
            vc.initRepository(repository: repository, current_guild: viewController!.current_guild, current_member: viewController!.current_member, permission: viewController!.permission)
            viewController?.presentPanModal(vc)
        }
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_member = viewController!.current_member
            vc.current_guild = viewController!.current_guild
            vc.permission = viewController!.permission
            viewController?.presentPanModal(vc)
        }
    }
    
    func toFoldersEdit() {
        if let vc = R.storyboard.folders.foldersEditViewControllerNavigation(), let foldersEditViewController = vc.viewControllers.first as? FoldersEditViewController, let repositories = viewController?.folders, let current_guild = viewController?.current_guild {
            vc.modalPresentationStyle = .overCurrentContext
            foldersEditViewController.current_guild = current_guild
            foldersEditViewController.repositories = repositories
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
}
