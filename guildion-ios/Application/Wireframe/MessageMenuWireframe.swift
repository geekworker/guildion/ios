//
//  MessageMenuWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class MessageMenuWireframe: Wireframe {
    typealias ViewController = MessageMenuViewController
    
    fileprivate weak var viewController: MessageMenuViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
