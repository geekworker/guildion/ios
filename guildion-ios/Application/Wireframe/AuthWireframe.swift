//
//  AuthWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit

class AuthWireframe: Wireframe {
    typealias ViewController = AuthViewController
    
    fileprivate weak var viewController: AuthViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
