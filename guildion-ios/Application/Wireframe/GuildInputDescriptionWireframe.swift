//
//  GuildInputDescriptionWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit

class GuildInputDescriptionWireframe: Wireframe {
    typealias ViewController = GuildInputDescriptionViewController
    
    fileprivate weak var viewController: GuildInputDescriptionViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

