//
//  MessageSearchWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class MessageSearchWireframe: Wireframe {
    typealias ViewController = MessageSearchViewController
    
    fileprivate weak var viewController: MessageSearchViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
