//
//  GuildSettingWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit

class GuildSettingWireframe: Wireframe {
    typealias ViewController = GuildSettingViewController
    
    fileprivate weak var viewController: GuildSettingViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toGuildShare() {
        if let vc = R.storyboard.guilds.guildShareViewController(), let repository = viewController?.repository {
            vc.repository = repository
            viewController?.presentPanModal(vc)
        }
    }
    
    func toPlaylistsEdit() {
        if let vc = R.storyboard.folders.foldersEditViewController() {
            vc.is_playlist = true
            vc.current_guild = viewController!.repository
            self.viewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func toSectionChannelsEdit() {
        if let vc = R.storyboard.guildSetting.guildSectionChannelsEditViewController() {
            vc.repository = viewController!.repository
            self.viewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
