//
//  GuildChannelsEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class GuildChannelsEditWireframe: Wireframe {
    typealias ViewController = GuildChannelsEditViewController
    
    fileprivate weak var viewController: GuildChannelsEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toChannelSetting() {
        viewController?.performSegue(withIdentifier: R.segue.guildChannelsEditViewController.toChannelSetting.identifier, sender: nil)
    }
    
    func toSectionChannelsEdit() {
        viewController?.performSegue(withIdentifier: R.segue.guildChannelsEditViewController.toSectionChannelsEdit.identifier, sender: nil)
    }
}
