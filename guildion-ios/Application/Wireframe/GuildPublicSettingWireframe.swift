//
//  GuildPublicSettingWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/12.
//

import Foundation
import UIKit

class GuildPublicSettingWireframe: Wireframe {
    typealias ViewController = GuildPublicSettingViewController
    
    fileprivate weak var viewController: GuildPublicSettingViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toOpenSetting() {
        viewController?.performSegue(withIdentifier: R.segue.guildPublicSettingViewController.toOpenSetting, sender: nil)
    }
}
