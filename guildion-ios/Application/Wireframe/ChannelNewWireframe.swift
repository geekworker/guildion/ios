//
//  ChannelNewWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit

class ChannelNewWireframe: Wireframe {
    typealias ViewController = ChannelNewViewController
    
    fileprivate weak var viewController: ChannelNewViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberSelect() {
        if let vc = R.storyboard.memberSelect.memberSelectViewControllerNavigation(), let memberSelectViewController = vc.viewControllers.first as? MemberSelectViewController, let repositories = GuildConnector.shared.current_guild?.Members {
            vc.modalPresentationStyle = .overCurrentContext
            memberSelectViewController.is_invite = false
            memberSelectViewController.repositories = repositories
            memberSelectViewController.delegate = viewController
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
}
