//
//  UserEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class UserEditWireframe: Wireframe {
    typealias ViewController = UserEditViewController
    
    fileprivate weak var viewController: UserEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
