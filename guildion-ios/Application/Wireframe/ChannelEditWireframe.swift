//
//  ChannelEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class ChannelEditWireframe: Wireframe {
    typealias ViewController = ChannelEditViewController
    
    fileprivate weak var viewController: ChannelEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
