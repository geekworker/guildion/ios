//
//  ChannelRoleEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit

class ChannelRoleEditWireframe: Wireframe {
    typealias ViewController = ChannelRoleEditViewController
    
    fileprivate weak var viewController: ChannelRoleEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
