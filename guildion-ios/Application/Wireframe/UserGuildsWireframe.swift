//
//  UserGuildsWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class UserGuildsWireframe: Wireframe {
    typealias ViewController = UserGuildsViewController
    
    fileprivate weak var viewController: UserGuildsViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
