//
//  MembersEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class MembersEditWireframe: Wireframe {
    typealias ViewController = MembersEditViewController
    
    fileprivate weak var viewController: MembersEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberEdit() {
        viewController?.performSegue(withIdentifier: R.segue.membersEditViewController.toMemberEdit.identifier, sender: nil)
    }
}
