//
//  MembersWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/14.
//

import Foundation
import UIKit

class MembersWireframe: Wireframe {
    typealias ViewController = MembersViewController
    
    fileprivate weak var viewController: MembersViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
