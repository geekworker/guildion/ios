//
//  FileNewFromBrowserWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit

class FileNewFromBrowserWireframe: Wireframe {
    typealias ViewController = FileNewFromBrowserViewController
    
    fileprivate weak var viewController: FileNewFromBrowserViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

