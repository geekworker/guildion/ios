//
//  GuildOpenSettingWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/12.
//

import Foundation
import UIKit

class GuildOpenSettingWireframe: Wireframe {
    typealias ViewController = GuildOpenSettingViewController
    
    fileprivate weak var viewController: GuildOpenSettingViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toInputDescription() {
        viewController?.performSegue(withIdentifier: R.segue.guildOpenSettingViewController.toInputDescription, sender: nil)
    }
}
