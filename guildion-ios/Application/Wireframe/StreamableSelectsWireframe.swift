//
//  StreamableSelectsWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit

class StreamableSelectsWireframe: Wireframe {
    typealias ViewController = StreamableSelectsViewController
    
    fileprivate weak var viewController: StreamableSelectsViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toFileNewFromBrowser() {
        viewController?.performSegue(withIdentifier: R.segue.streamableSelectsViewController.toFileNewFromBrowser, sender: nil)
    }
    
    func toFileNewFromURL() {
        viewController?.performSegue(withIdentifier: R.segue.streamableSelectsViewController.toFileNewFromURL, sender: nil)
    }
    
    func toSelectablePlaylist(repository: FolderEntity) {
        if let vc = R.storyboard.folders.playlistViewController() {
            vc.selectable = true
            vc.selectDelegate = viewController
            vc.initRepository(repository, current_guild: self.viewController!.current_guild, current_member: self.viewController!.current_member, permission: GuildConnector.shared.current_permission)
            viewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func toSelectableAllFiles() {
        if let vc = R.storyboard.folders.playlistViewController() {
            vc.selectable = true
            vc.all_files = true
            vc.current_guild = viewController!.current_guild
            vc.current_member = viewController!.current_member
            vc.selectDelegate = viewController
            viewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func toWebBrowser() {
        guard let navVC = self.viewController?.navigationController as? PanContainerViewNavigationController else { return }
        let webkitViewController = WebKitViewController(nibName: R.nib.webKitViewController.name, bundle: nil)
        webkitViewController.setURL(SearchDomainType.youtube.url)
        webkitViewController.defaultMedia = true
        webkitViewController.delegate = viewController
        let containerView = PanContainerNavigationView()
        containerView.childViewController = webkitViewController
        if navVC.containerTransitionContext.containerView != nil {
            navVC.containerTransitionContext.forceDismiss(animated: true, completion: {
                self.toWebBrowser()
            })
            return
        }
        navVC.containerTransitionContext.sourceRect = CGRect(x: 0, y: -44, width: UIScreen.main.bounds.width, height: navVC.view.frame.height + 32)
        navVC.presentPanContainer(containerView, completion: nil)
    }
}

