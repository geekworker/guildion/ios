//
//  Wireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import SwiftMessages

protocol Wireframe: class {
    associatedtype ViewController: UIViewController

    init(viewController: ViewController)
}

extension Wireframe {
    static func toWelcome() {
        let vc = R.storyboard.welcome().instantiateViewController(withIdentifier: R.storyboard.welcome.welcomeViewController.identifier)
        vc.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(vc, animated: false)
    }
    
    static func toMain() {
        let vc = R.storyboard.main().instantiateViewController(withIdentifier: R.storyboard.main.baseTabBarController.identifier)
        vc.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(vc, animated: true)
    }
    
    static func toGuildNew() {
        let vc = R.storyboard.guildNew().instantiateViewController(withIdentifier:
                                                                    R.storyboard.guildNew.guildNewViewController.identifier)
        vc.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(vc, animated: true)
    }
    
    static func toGuildFromGlobal() {
        let vc = R.storyboard.main().instantiateViewController(withIdentifier: R.storyboard.main.baseTabBarController.identifier)
        vc.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(vc, animated: true)
    }
    
    static func toPlansWithAlert() {
        if let vc = R.storyboard.plans.plansViewControllerNavigation() {
            vc.modalPresentationStyle = .overCurrentContext
            UIViewController.topMostController().present(vc, animated: true, completion: {
                let messageAlert = MessageView.viewFromNib(layout: .cardView)
                messageAlert.configureTheme(.error)
                messageAlert.configureDropShadow()
                messageAlert.configureContent(title: R.string.localizable.error(), body: R.string.localizable.errorStorageFill())
                messageAlert.button?.isHidden = true
                var messageAlertConfig = SwiftMessages.defaultConfig
                messageAlertConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
                SwiftMessages.show(config: messageAlertConfig, view: messageAlert)
            })
        }
    }
    
    static func toGuildWelcome(uid: String, completion: (() -> Void)? = nil) {
        if let vc = R.storyboard.guildInfo.guildInfoViewController(), CurrentUser.getCurrentUserEntity() != nil {
            let repository = GuildEntity()
            repository.uid = uid
            vc.repository = repository
            vc.modalPresentationStyle = .overCurrentContext
            UIViewController.topMostController().dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
                completion?()
            })
        }
    }
    
    static func toGuildChannel(guild: GuildEntity, member: MemberEntity, channel: MessageableEntity, completion: (() -> Void)? = nil) {
        GuildConnector.shared.current_guild = guild
        GuildConnector.shared.current_member = member
        switch channel.messageable_type {
        case .DMChannel:
            BaseWireframe.toDMChannel(repository: channel as! DMChannelEntity, current_guild: guild, current_member: member, permission: GuildConnector.shared.current_permission)
        case .TextChannel:
            BaseWireframe.toTextChannel(repository: channel as! TextChannelEntity, current_guild: guild, current_member: member, permission: GuildConnector.shared.current_permission)
        case .StreamChannel:
            BaseWireframe.checkLiveAndToStreamChannel(repository: channel as! StreamChannelEntity, current_guild: guild, current_member: member, permission: GuildConnector.shared.current_permission)
        default: break
        }
    }
    
    static func toTextChannel(repository: TextChannelEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        let vc = TextChannelViewControllerBuilder.build()
        TextChannelViewControllerBuilder.rebuild(vc)
        vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: permission)
        UIViewController.topMostController().dismiss(animated: true, completion: {
            guard let tabVC = UIViewController.topMostController() as? BaseTabBarController, let navVC = tabVC.viewControllers?.first as? UINavigationController, let guildsVC = navVC.viewControllers.first as? GuildsViewController else { return }
            tabVC.tabBarController?.selectedIndex = 0
            navVC.hidesBottomBarWhenPushed = true
            guildsVC.setSelectedGuild(current_guild)
            GuildsWireframe.replaceViewControllers(sourceViewController: navVC, targetViewController: vc)
            navVC.navigationItem.title = repository.name
            navVC.hidesBottomBarWhenPushed = false
        })
    }
    
    static func toDMChannel(repository: DMChannelEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        let vc = DMChannelViewControllerBuilder.build()
        DMChannelViewControllerBuilder.rebuild(vc)
        vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: permission)
        UIViewController.topMostController().dismiss(animated: true, completion: {
            guard let tabVC = UIViewController.topMostController() as? BaseTabBarController, let navVC = tabVC.viewControllers?.first as? UINavigationController, let guildsVC = navVC.viewControllers.first as? GuildsViewController else { return }
            tabVC.tabBarController?.selectedIndex = 0
            navVC.hidesBottomBarWhenPushed = true
            guildsVC.setSelectedGuild(current_guild)
            GuildsWireframe.replaceViewControllers(sourceViewController: navVC, targetViewController: vc)
            navVC.navigationItem.title = repository.name
            navVC.hidesBottomBarWhenPushed = false
        })
    }
    
    static func checkLiveAndToStreamChannel(repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController, vc.repository == repository {
            guard vc.miniScreenTransitionContext.isMiniScreenViewPresented else { return }
            UIViewController.topMostController().dismiss(animated: true, completion: nil)
            navVC.modalPresentationStyle = .overCurrentContext
            MiniScreenView.shared?.dismiss(animated: true)
            UIViewController.topMostController().present(navVC, animated: true, completion: nil)
        } else if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController {
            vc.unbind()
            navVC.dismiss(animated: true, completion: {
                self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member, permission: permission)
            })
            MiniScreenView.shared?.dismiss(animated: true, completion: nil)
            LiveConfig.playingViewController = nil
            self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member, permission: permission)
        } else {
            self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member, permission: permission)
        }
    }
    
    static func toStreamChannel(repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        guard let navVC = R.storyboard.streamChannel.streamChannelViewControllerNavigation(), let vc = navVC.viewControllers.first as? StreamChannelViewController, let tabVC = UIViewController.topMostController() as? BaseTabBarController, let _navVC = tabVC.viewControllers?.first as? UINavigationController, let guildsVC = _navVC.viewControllers.first as? GuildsViewController else { return }
        StreamChannelViewControllerBuilder.rebuild(vc)
        tabVC.tabBarController?.selectedIndex = 0
        vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: permission)
        guildsVC.setSelectedGuild(current_guild)
        navVC.modalPresentationStyle = .overFullScreen
        UIViewController.topMostController().dismiss(animated: true, completion: {
            UIViewController.topMostController().present(navVC, animated: true)
        })
    }
}

class BaseWireframe: Wireframe {
    typealias ViewController = UIViewController
    required init(viewController: UIViewController) {
    }
}
