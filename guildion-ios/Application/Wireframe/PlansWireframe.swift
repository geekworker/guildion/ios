//
//  PlansWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit

class PlansWireframe: Wireframe {
    typealias ViewController = PlansViewController
    
    fileprivate weak var viewController: PlansViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
