//
//  ParticipantsWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/21.
//

import Foundation
import UIKit

class ParticipantsWireframe: Wireframe {
    typealias ViewController = ParticipantsViewController
    
    fileprivate weak var viewController: ViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
