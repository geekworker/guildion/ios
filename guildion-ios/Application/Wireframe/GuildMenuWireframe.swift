//
//  GuildMenuWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class GuildMenuWireframe: Wireframe {
    typealias ViewController = GuildMenuViewController
    
    fileprivate weak var viewController: GuildMenuViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toChannelNew(section: SectionChannelEntity) {
        guard !section.is_dm else { self.toDMNew(section: section); return }
        if let vc = R.storyboard.channelNew.channelNewViewControllerNavigation(), let channelNewViewController = vc.viewControllers.first as? ChannelNewViewController, let repository = viewController?.repository.toNewMemory() {
            vc.modalPresentationStyle = .overCurrentContext
            channelNewViewController.section = section
            channelNewViewController.guild = repository
            channelNewViewController.member = viewController!.current_member
            channelNewViewController.mode = .normal
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
            })
        }
    }
    
    func toSectionNew() {
        if let vc = R.storyboard.channelNew.channelNewViewControllerNavigation(), let channelNewViewController = vc.viewControllers.first as? ChannelNewViewController, let repository = viewController?.repository.toNewMemory() {
            vc.modalPresentationStyle = .overCurrentContext
            channelNewViewController.guild = repository
            channelNewViewController.mode = .section
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
            })
        }
    }
    
    func toDMNew(section: SectionChannelEntity) {
        if let vc = R.storyboard.channelNew.channelNewViewControllerNavigation(), let channelNewViewController = vc.viewControllers.first as? ChannelNewViewController, let repository = viewController?.repository.toNewMemory() {
            vc.modalPresentationStyle = .overCurrentContext
            channelNewViewController.section = section
            channelNewViewController.guild = repository
            channelNewViewController.mode = .dm
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
            })
        }
    }
    
    func toGuildSetting() {
        if let vc = R.storyboard.guildSetting.guildSettingViewControllerNavigation(), let guildSettingViewController = vc.viewControllers.first as? GuildSettingViewController, let repository = viewController?.repository.toNewMemory() {
            vc.modalPresentationStyle = .overFullScreen
            guildSettingViewController.repository = repository
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
            })
        }
    }
    
    func toGuildNotificationSetting() {
        if let vc = R.storyboard.guildSetting.guildNotificationEditViewControllerNavigation(), let guildSettingViewController = vc.viewControllers.first as? GuildNotificationEditViewController {
            vc.modalPresentationStyle = .overFullScreen
            guildSettingViewController.repository = viewController!.current_member
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
            })
        }
    }
    
    func toGuildShare() {
        if let vc = R.storyboard.guilds.guildShareViewController(), let repository = viewController?.repository {
            vc.repository = repository
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().presentPanModal(vc)
            })
        }
    }
}
