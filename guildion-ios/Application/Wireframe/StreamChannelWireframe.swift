//
//  StreamChannelWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import SideMenu

class StreamChannelWireframe: Wireframe {
    typealias ViewController = StreamChannelViewController
    
    public weak var viewController: StreamChannelViewController?
    lazy var webkitViewController: WebKitViewController = {
        let vc = WebKitViewController(nibName: R.nib.webKitViewController.name, bundle: nil)
        return vc
    }()
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toChannelMenu() {
        if let navVC = SideMenuManager.default.rightMenuNavigationController, let vc = SideMenuManager.default.rightMenuNavigationController?.viewControllers.first as? ChannelMenuViewController, let repository = viewController?.repository {
            vc.repository = repository
            vc.permission = viewController!.permission
            vc.repository.Guild = viewController?.current_guild.toNewMemory()
            viewController?.present(navVC, animated: true)
        }
    }
    
    func toStreamableSelects() {
        if let vc = R.storyboard.streamableSelects.streamableSelectsViewControllerRow() {
            let containerView = PanContainerNavigationView()
            containerView.childViewController = vc
            vc.current_guild = viewController!.current_guild
            vc.current_member = viewController!.current_member
            vc.delegate = viewController
            if viewController?.containerTransitionContext.containerView != nil {
                viewController?.containerTransitionContext.forceDismiss(animated: true, completion: {
                    self.toStreamableSelects()
                })
                return
            }
            viewController?.containerTransitionContext.sourceRect = CGRect(x: 0, y: viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height))
            viewController?.inputAccessoryView?.fadeOut(type: .Normal, completed: nil)
            viewController?.messageInputBar.inputTextView.resignFirstResponder()
            viewController?.presentPanContainer(containerView, completion: nil)
        }
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_member = viewController!.current_member
            vc.current_guild = viewController!.current_guild
            vc.permission = viewController!.permission
            viewController?.presentPanModal(vc)
        }
    }
    
    func toMessageMenu(repository: MessageEntity) {
        if let vc = R.storyboard.guilds.messageMenuViewController() {
            vc.initRepository(repository, current_guild: viewController!.current_guild, current_channel: viewController!.repository, current_member: viewController!.current_member, permission: viewController!.permission)
            vc.viewControllerParent = viewController
            viewController?.presentPanModal(vc)
        }
    }
    
    func toWebBrowser(_ url: URL, repositories: FileEntities? = nil) {
        let webkitViewController = WebKitViewController(nibName: R.nib.webKitViewController.name, bundle: nil)
        if let files = repositories {
            webkitViewController.setRepositories(files)
        }
        webkitViewController.setURL(url)
        let containerView = PanContainerNavigationView()
        containerView.childViewController = webkitViewController
        webkitViewController.delegate = viewController
        if viewController?.containerTransitionContext.containerView != nil {
            viewController?.containerTransitionContext.forceDismiss(animated: true, completion: {
                self.toWebBrowser(url, repositories: repositories)
            })
            return
        }
        viewController?.containerTransitionContext.sourceRect = CGRect(x: 0, y: viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height))
        viewController?.inputAccessoryView?.fadeOut(type: .Normal, completed: nil)
        viewController?.messageInputBar.inputTextView.resignFirstResponder()
        viewController?.presentPanContainer(containerView, completion: nil)
    }
    
    func toStreamChannelInfo() {
        if let vc = R.storyboard.streamChannel.streamChannelInfoViewController(), let repository = viewController?.repository {
            let containerView = PanContainerNavigationView()
            containerView.childViewController = vc
            vc.setRepository(repository)
            vc.folder = viewController?.folder ?? FolderEntity()
            vc.delegate = viewController
            vc.current_guild = viewController!.current_guild
            if viewController?.containerTransitionContext.containerView != nil {
                viewController?.containerTransitionContext.forceDismiss(animated: true, completion: {
                    self.toStreamChannelInfo()
                })
                return
            }
            viewController?.containerTransitionContext.sourceRect = CGRect(x: 0, y: viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height))
            viewController?.inputAccessoryView?.fadeOut(type: .Normal, completed: nil)
            viewController?.messageInputBar.inputTextView.resignFirstResponder()
            viewController?.presentPanContainer(containerView, completion: nil)
        }
    }
    
    func toParticipants() {
        if let vc = R.storyboard.streamChannel.participantsViewController() {
            let containerView = PanContainerNavigationView()
            containerView.childViewController = vc
            vc.current_channel = viewController!.repository
            vc.current_member = viewController!.current_member
            vc.current_guild = viewController!.current_guild
            vc.delegate = viewController
            viewController?.delegate = vc
            if viewController?.containerTransitionContext.containerView != nil {
                viewController?.containerTransitionContext.forceDismiss(animated: true, completion: {
                    self.toParticipants()
                })
                return
            }
            viewController?.containerTransitionContext.sourceRect = CGRect(x: 0, y: viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (viewController?.streamChannelHeaderViewHeightConstraint.constant ?? SyncPlayerConfig.header_view_height))
            viewController?.inputAccessoryView?.fadeOut(type: .Normal, completed: nil)
            viewController?.messageInputBar.inputTextView.resignFirstResponder()
            viewController?.presentPanContainer(containerView, completion: nil)
        }
    }
    
    func toImageViewer(repository: FileEntity) {
        guard let repositories = self.viewController?.repositories else { return }
        let vc = PanImageViewerViewController(startIndex: repositories.indexOfImageView(repository: repository), itemsDataSource: repositories, itemsDelegate: repositories, displacedViewsDataSource: repositories, configuration: [])
        let containerView = PanContainerNavigationView()
        containerView.childViewController = vc
        if viewController?.containerTransitionContext.containerView != nil {
            viewController?.containerTransitionContext.forceDismiss(animated: true, completion: {
                self.toImageViewer(repository: repository)
            })
            return
        }
        viewController?.containerTransitionContext.sourceRect = CGRect(x: 0, y: SyncPlayerConfig.header_view_height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - SyncPlayerConfig.header_view_height)
        viewController?.additionalBottomInset = viewController!.messagesCollectionView.frame.height - SyncPlayerConfig.header_view_height + 44
        viewController?.inputAccessoryView?.fadeOut(type: .Normal, completed: nil)
        viewController?.messageInputBar.inputTextView.resignFirstResponder()
        viewController?.presentPanContainer(containerView, completion: nil)
    }
}
