//
//  FileSelectFromFolderWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/22.
//

import Foundation
import UIKit

class FileSelectFromFolderWireframe: Wireframe {
    typealias ViewController = FileSelectFromFolderViewController
    
    fileprivate weak var viewController: FileSelectFromFolderViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
