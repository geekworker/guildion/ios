//
//  FileNewWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit

class FileNewWireframe: Wireframe {
    typealias ViewController = FileNewViewController
    
    fileprivate weak var viewController: FileNewViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toFileNewFromBrowser() {
        viewController?.performSegue(withIdentifier: R.segue.fileNewViewController.toFileNewFromBrowser, sender: nil)
    }
    
    func toFileNewFromURL() {
        viewController?.performSegue(withIdentifier: R.segue.fileNewViewController.toFileNewFromURL, sender: nil)
    }
    
    func toWebKit() {
        let webkitViewController = WebKitViewController(nibName: R.nib.webKitViewController.name, bundle: nil)
        let navVC = PanContainerViewNavigationController()
        navVC.viewControllers.append(webkitViewController)
        webkitViewController.defaultMedia = true
        webkitViewController.setURL(SearchDomainType.youtube.url)
        webkitViewController.delegate = viewController
        navVC.searchDelegate = webkitViewController
        navVC.modalPresentationStyle = .overCurrentContext
        viewController?.present(navVC, animated: true)
    }
}

