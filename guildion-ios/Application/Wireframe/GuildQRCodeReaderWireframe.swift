//
//  QRCodeReaderWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/15.
//

import Foundation
import UIKit

class GuildQRCodeReaderWireframe: Wireframe {
    typealias ViewController = GuildQRCodeReaderViewController
    
    fileprivate weak var viewController: ViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
