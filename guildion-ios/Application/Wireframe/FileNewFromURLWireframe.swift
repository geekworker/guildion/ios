//
//  FileNewFromURLWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/24.
//

import Foundation
import UIKit

class FileNewFromURLWireframe: Wireframe {
    typealias ViewController = FileNewFromURLViewController
    
    fileprivate weak var viewController: FileNewFromURLViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

