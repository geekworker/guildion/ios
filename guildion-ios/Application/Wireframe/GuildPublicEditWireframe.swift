//
//  GuildPublicEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class GuildPublicEditWireframe: Wireframe {
    typealias ViewController = GuildPublicEditViewController
    
    fileprivate weak var viewController: GuildPublicEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

