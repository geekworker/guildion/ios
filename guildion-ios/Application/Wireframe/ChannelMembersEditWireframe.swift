//
//  ChannelMembersEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/02.
//

import Foundation
import UIKit

class ChannelMembersEditWireframe: Wireframe {
    typealias ViewController = ChannelMembersEditViewController
    
    fileprivate weak var viewController: ChannelMembersEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberEdit() {
        viewController?.performSegue(withIdentifier: R.segue.channelMembersEditViewController.toMemberEdit.identifier, sender: nil)
    }
    
    func toMemberSelect() {
        if let vc = R.storyboard.memberSelect.memberSelectViewControllerNavigation(), let memberSelectViewController = vc.viewControllers.first as? MemberSelectViewController, let repository = viewController?.repository {
            memberSelectViewController.repository = repository
            memberSelectViewController.is_invite = true
            memberSelectViewController.delegate = viewController
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
}
