//
//  FileMenuWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/10.
//

import Foundation
import UIKit

class FileMenuWireframe: Wireframe {
    typealias ViewController = FileMenuViewController
    
    fileprivate weak var viewController: FileMenuViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func checkLiveAndToStreamChannel(repository: StreamChannelEntity) {
        if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController, vc.repository == repository {
            self.viewController?.dismiss(animated: true, completion: nil)
            navVC.modalPresentationStyle = .overCurrentContext
            MiniScreenView.shared?.dismiss(animated: true)
            UIViewController.topMostController().present(navVC, animated: true, completion: nil)
        } else if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController {
            MiniScreenView.shared?.dismiss(animated: true)
            vc.unbind()
            LiveConfig.playingViewController = nil
            self.toStreamChannel(repository: repository)
        } else {
            self.toStreamChannel(repository: repository)
        }
    }
    
    func toStreamChannel(repository: StreamChannelEntity) {
        guard let navVC = R.storyboard.streamChannel.streamChannelViewControllerNavigation(), let vc = navVC.viewControllers.first as? StreamChannelViewController, let current_guild = viewController?.current_guild, let current_member = viewController?.current_member else { return }
        StreamChannelViewControllerBuilder.rebuild(vc)
        vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: viewController!.permission)
        navVC.modalPresentationStyle = .overFullScreen
        viewController?.dismiss(animated: true, completion: {
            UIViewController.topMostController().present(navVC, animated: true)
        })
    }
    
    func toFileEdit() {
        if let vc = R.storyboard.fileEdit.fileEditViewControllerNavigation(), let fileEditViewController = vc.viewControllers.first as? FileEditViewController, let repository = viewController?.repository {
            fileEditViewController.repository = repository
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
            })
        }
    }
}
