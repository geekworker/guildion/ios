//
//  RoleEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/14.
//

import Foundation
import UIKit

class RoleEditWireframe: Wireframe {
    typealias ViewController = RoleEditViewController
    
    fileprivate weak var viewController: RoleEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
