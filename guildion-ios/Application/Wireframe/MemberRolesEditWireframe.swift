//
//  MemberRolesEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit

class MemberRolesEditWireframe: Wireframe {
    typealias ViewController = MemberRolesEditViewController
    
    fileprivate weak var viewController: MemberRolesEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberRoleEdit() {
        self.viewController?.performSegue(withIdentifier: R.segue.memberRolesEditViewController.toMemberRoleEdit, sender: nil)
    }
}
