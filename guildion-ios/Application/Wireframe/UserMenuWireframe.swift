//
//  UserMenuWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/25.
//

import Foundation
import UIKit

class UserMenuWireframe: Wireframe {
    typealias ViewController = UserMenuViewController
    
    fileprivate weak var viewController: UserMenuViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
