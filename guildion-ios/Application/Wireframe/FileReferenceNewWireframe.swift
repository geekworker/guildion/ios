//
//  FileReferenceNewWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/20.
//

import Foundation
import UIKit

class FileReferenceNewWireframe: Wireframe {
    typealias ViewController = FileReferenceNewViewController
    
    fileprivate weak var viewController: FileReferenceNewViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
