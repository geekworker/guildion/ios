//
//  MemberEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class MemberEditWireframe: Wireframe {
    typealias ViewController = MemberEditViewController
    
    fileprivate weak var viewController: MemberEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberRolesEdit() {
        self.viewController?.performSegue(withIdentifier: R.segue.memberEditViewController.toMemberRolesEdit, sender: nil)
    }
}
