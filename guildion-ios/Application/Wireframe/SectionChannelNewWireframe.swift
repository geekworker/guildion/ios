//
//  SectionChannelNewWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit

class SectionChannelNewWireframe: Wireframe {
    typealias ViewController = SectionChannelNewViewController
    
    fileprivate weak var viewController: SectionChannelNewViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

