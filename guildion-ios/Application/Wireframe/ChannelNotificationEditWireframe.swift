//
//  ChannelNotificationEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class ChannelNotificationEditWireframe: Wireframe {
    typealias ViewController = ChannelNotificationEditViewController
    
    fileprivate weak var viewController: ChannelNotificationEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
