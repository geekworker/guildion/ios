//
//  PlaylistWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/25.
//

import Foundation
import UIKit

class PlaylistWireframe: Wireframe {
    typealias ViewController = PlaylistViewController
    
    fileprivate weak var viewController: PlaylistViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toFileNew() {
        let vc = R.storyboard.fileNew().instantiateViewController(withIdentifier:
                                                                    R.storyboard.fileNew.fileNewViewController.identifier)
        vc.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(vc, animated: true)
    }
    
    func toFileSelect() {
        let navVC = R.storyboard.folders.fileSelectFromFolderViewController()!,
            vc = navVC.viewControllers.first as! FileSelectFromFolderViewController
        vc.delegate = viewController
        vc.current_guild = viewController!.current_guild
        vc.is_playlist = true
        navVC.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(navVC, animated: true)
    }
    
    func toFileMenu(repository: FileEntity, reference: FileReferenceEntity? = nil) {
        if let vc = R.storyboard.folders.fileMenuViewController() {
            vc.initRepository(repository: repository, current_guild: viewController!.current_guild, current_member: viewController!.current_member, permission: viewController!.permission)
            vc.reference = reference
            vc.delegate = self.viewController
            viewController?.presentPanModal(vc)
        }
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_member = viewController!.current_member
            vc.current_guild = viewController!.current_guild
            vc.permission = viewController!.permission
            viewController?.presentPanModal(vc)
        }
    }
    
    func toEdit() {
        viewController?.performSegue(withIdentifier: R.segue.playlistViewController.toEdit, sender: self)
    }
    
    func checkLiveAndToStreamChannel(repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity) {
        if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController, vc.repository == repository {
            self.viewController?.dismiss(animated: true, completion: nil)
            navVC.modalPresentationStyle = .overCurrentContext
            MiniScreenView.shared?.dismiss(animated: true)
            UIViewController.topMostController().present(navVC, animated: true, completion: nil)
        } else if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController {
            vc.unbind()
            MiniScreenView.shared?.dismiss(animated: true)
            LiveConfig.playingViewController = nil
            self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member)
        } else {
            self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member)
        }
    }
    
    func toStreamChannel(repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity) {
        guard let navVC = R.storyboard.streamChannel.streamChannelViewControllerNavigation(), let vc = navVC.viewControllers.first as? StreamChannelViewController else { return }
        StreamChannelViewControllerBuilder.rebuild(vc)
        vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: viewController!.permission)
        navVC.modalPresentationStyle = .overFullScreen
        UIViewController.topMostController().present(navVC, animated: true)
    }
}
