//
//  GuildInfoWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class GuildInfoWireframe: Wireframe {
    typealias ViewController = GuildInfoViewController
    
    fileprivate weak var viewController: GuildInfoViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
