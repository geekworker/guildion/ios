//
//  ProfileSettingWireframe.swift
//  nowy-ios
//
//  Created by Apple on 2020/07/10.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ProfileSettingWireframe: Wireframe {
    typealias ViewController = ProfileSettingViewController
    
    fileprivate weak var viewController: ViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
