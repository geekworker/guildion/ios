//
//  FileEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/03.
//

import Foundation
import UIKit

class FileEditWireframe: Wireframe {
    typealias ViewController = FileEditViewController
    
    fileprivate weak var viewController: FileEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
