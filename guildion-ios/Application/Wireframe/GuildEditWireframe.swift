//
//  GuildEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class GuildEditWireframe: Wireframe {
    typealias ViewController = GuildEditViewController
    
    fileprivate weak var viewController: GuildEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
