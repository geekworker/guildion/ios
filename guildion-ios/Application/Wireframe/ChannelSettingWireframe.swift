//
//  ChannelSettingWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class ChannelSettingWireframe: Wireframe {
    typealias ViewController = ChannelSettingViewController
    
    fileprivate weak var viewController: ChannelSettingViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
