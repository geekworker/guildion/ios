//
//  ChannelRolesEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class ChannelRolesEditWireframe: Wireframe {
    typealias ViewController = ChannelRolesEditViewController
    
    fileprivate weak var viewController: ChannelRolesEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toChannelRoleEdit() {
        self.viewController?.performSegue(withIdentifier: R.segue.channelRolesEditViewController.toChannelEdit.identifier, sender: nil)
    }
}
