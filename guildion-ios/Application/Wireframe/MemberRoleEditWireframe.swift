//
//  MemberRoleEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit

class MemberRoleEditWireframe: Wireframe {
    typealias ViewController = MemberRoleEditViewController
    
    fileprivate weak var viewController: MemberRoleEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
