//
//  SearchCompleteWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/11.
//

import Foundation
import UIKit

class SearchCompleteWireframe: Wireframe {
    typealias ViewController = SearchCompleteViewController
    
    fileprivate weak var viewController: SearchCompleteViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

