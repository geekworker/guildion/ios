//
//  ExploreWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit
import SPPermissions

class ExploreWireframe: Wireframe {
    typealias ViewController = ExploreViewController
    
    fileprivate weak var viewController: ExploreViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toGuildSearch() {
        viewController?.performSegue(withIdentifier: R.segue.exploreViewController.toGuildSearch.identifier, sender: nil)
    }
    
    func toGuildQRCodeReader() {
        if configurePhotoPermission() {
            guard let navVC = R.storyboard.guildQRCodeReader.guildQRCodeReaderViewControllerNavigation() else { return }
            navVC.modalPresentationStyle = .overFullScreen
            viewController?.present(navVC, animated: true)
        }
    }
}

extension ExploreWireframe: SPPermissionsDelegate, SPPermissionsDataSource {
    func configurePhotoPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self.viewController!)
        return false
    }
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        switch permission {
        case .camera:
            cell.permissionTitleLabel.text = R.string.localizable.permissionCamera()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionCameraText()
        case .microphone:
            cell.permissionTitleLabel.text = R.string.localizable.permissionVoice()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionVoiceText()
        case .photoLibrary:
            cell.permissionTitleLabel.text = R.string.localizable.permissionPhoto()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionPhotoText()
        default: break
        }
        return cell
    }
    
    func didHide(permissions ids: [Int]) {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard permissions.isEmpty else { return }
        self.toGuildQRCodeReader()
    }
}
