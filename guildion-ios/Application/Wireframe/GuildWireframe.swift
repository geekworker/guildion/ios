//
//  GuildWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import UIKit

class GuildWireframe: Wireframe {
    typealias ViewController = GuildViewController
    
    fileprivate weak var viewController: GuildViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toChannelNew(section: SectionChannelEntity) {
        guard !section.is_dm else { self.toDMNew(section: section); return }
        if let vc = R.storyboard.channelNew().instantiateViewController(withIdentifier: R.storyboard.channelNew.channelNewViewControllerNavigation.identifier) as? UINavigationController, let channelNewViewController = vc.viewControllers.first as? ChannelNewViewController, let repository = viewController?.repository.toNewMemory() {
            vc.modalPresentationStyle = .overCurrentContext
            channelNewViewController.section = section
            channelNewViewController.guild = repository
            channelNewViewController.member = viewController!.current_member
            channelNewViewController.mode = .normal
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toDMNew(section: SectionChannelEntity) {
        if let vc = R.storyboard.channelNew.channelNewViewControllerNavigation(), let channelNewViewController = vc.viewControllers.first as? ChannelNewViewController, let repository = viewController?.repository.toNewMemory() {
            vc.modalPresentationStyle = .overCurrentContext
            channelNewViewController.section = section
            channelNewViewController.guild = repository
            channelNewViewController.member = viewController!.current_member
            channelNewViewController.mode = .dm
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toGuildMenu() {
        if let vc = R.storyboard.guilds.guildMenuViewController(), let repository = viewController?.repository {
            vc.repository = repository
            vc.current_member = viewController!.current_member
            vc.permission = viewController!.permission
            vc.viewContrllerParent = viewController
            viewController?.presentPanModal(vc)
        }
    }
    
    func toGuildShare() {
        if let vc = R.storyboard.guilds.guildShareViewController(), let repository = viewController?.repository {
            vc.repository = repository
            viewController?.presentPanModal(vc)
        }
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_member = viewController!.current_member
            vc.current_guild = viewController!.repository
            vc.permission = viewController!.permission
            viewController?.presentPanModal(vc)
        }
    }
    
    func toFileMenu(repository: FileEntity) {
        if let vc = R.storyboard.folders.fileMenuViewController() {
            vc.initRepository(repository: repository, current_guild: viewController!.repository, current_member: viewController!.current_member, permission: viewController!.permission)
            viewController?.presentPanModal(vc)
        }
    }
    
    func toGuildOpenSetting() {
        if let navVC = R.storyboard.guildNew.guildOpenSettingViewControllerNavigation(), let vc = navVC.viewControllers.first as? GuildOpenSettingViewController, let repository = viewController?.repository {
            vc.repository = repository
            navVC.modalPresentationStyle = .overCurrentContext
            UIViewController.topMostController().present(navVC, animated: true, completion: nil)
        }
    }
    
    func toMemberRequest() {
//        if let navVC = R.storyboard.guildNew.guildOpenSettingViewControllerNavigation(), let vc = navVC.viewControllers.first as? GuildOpenSettingViewController, let repository = viewController?.repository {
//            vc.repository = repository
//            UIViewController.topMostController().present(vc, animated: true, completion: nil)
//        }
    }
}

