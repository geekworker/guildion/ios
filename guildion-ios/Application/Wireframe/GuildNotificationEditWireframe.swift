//
//  GuildNotificationEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class GuildNotificationEditWireframe: Wireframe {
    typealias ViewController = GuildNotificationEditViewController
    
    fileprivate weak var viewController: GuildNotificationEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
