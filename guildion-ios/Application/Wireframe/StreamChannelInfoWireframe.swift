//
//  StreamChannelInfoWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/14.
//

import Foundation
import UIKit

class StreamChannelInfoWireframe: Wireframe {
    typealias ViewController = StreamChannelInfoViewController
    
    fileprivate weak var viewController: StreamChannelInfoViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_guild = viewController!.current_guild
            vc.current_member = GuildConnector.shared.current_member ?? MemberEntity()
            vc.permission = GuildConnector.shared.current_permission
            viewController?.presentPanModal(vc)
        }
    }
}
