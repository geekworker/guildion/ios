//
//  FolderEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/19.
//

import Foundation
import UIKit

class FolderEditWireframe: Wireframe {
    typealias ViewController = FolderEditViewController
    
    fileprivate weak var viewController: FolderEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
