//
//  StreamChannelLiveSettingWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/02.
//

import Foundation
import UIKit

class StreamChannelLiveSettingWireframe: Wireframe {
    typealias ViewController = StreamChannelLiveSettingViewController
    
    fileprivate weak var viewController: StreamChannelLiveSettingViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
