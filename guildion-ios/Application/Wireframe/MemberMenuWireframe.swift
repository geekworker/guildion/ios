//
//  MemberMenuWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class MemberMenuWireframe: Wireframe {
    typealias ViewController = MemberMenuViewController
    
    fileprivate weak var viewController: MemberMenuViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func checkLiveAndToStreamChannel(repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity) {
        if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController, vc.repository == repository {
            self.viewController?.dismiss(animated: true, completion: nil)
            navVC.modalPresentationStyle = .overCurrentContext
            MiniScreenView.shared?.dismiss(animated: true)
            UIViewController.topMostController().present(navVC, animated: true, completion: nil)
        } else if let navVC = LiveConfig.playingViewController, let vc = navVC.viewControllers.first as? StreamChannelViewController {
            vc.unbind()
            MiniScreenView.shared?.dismiss(animated: true)
            LiveConfig.playingViewController = nil
            self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member)
        } else {
            self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member)
        }
    }
    
    func toMemberEdit() {
        if let vc = R.storyboard.memberEdit.memberEditViewControllerNavigatioin(), let memberEditViewController = vc.viewControllers.first as? MemberEditViewController, let repository = viewController?.repository.toNewMemory(), let guild = viewController?.current_guild {
            vc.modalPresentationStyle = .overCurrentContext
            memberEditViewController.repository = repository
            memberEditViewController.guild = guild
            viewController?.dismiss(animated: true, completion: {
                UIViewController.topMostController().present(vc, animated: true)
            })
        }
    }
    
    func toDMChannel(repository: DMChannelEntity, current_guild: GuildEntity, current_member: MemberEntity) {
        let vc = DMChannelViewControllerBuilder.build()
        DMChannelViewControllerBuilder.rebuild(vc)
        vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: viewController!.permission)
        viewController?.dismiss(animated: true, completion: {
            guard let tabVC = UIViewController.topMostController() as? BaseTabBarController, let navVC = tabVC.viewControllers?.first as? UINavigationController else { return }
            tabVC.tabBarController?.selectedIndex = 0
            navVC.hidesBottomBarWhenPushed = true
            GuildsWireframe.replaceViewControllers(sourceViewController: navVC, targetViewController: vc)
            navVC.navigationItem.title = repository.name
            navVC.hidesBottomBarWhenPushed = false
        })
    }
    
    func toStreamChannel(repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity) {
        guard let navVC = R.storyboard.streamChannel.streamChannelViewControllerNavigation(), let vc = navVC.viewControllers.first as? StreamChannelViewController else { return }
        StreamChannelViewControllerBuilder.rebuild(vc)
        vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: viewController!.permission)
        navVC.modalPresentationStyle = .overFullScreen
        viewController?.dismiss(animated: true, completion: {
            UIViewController.topMostController().present(navVC, animated: true)
        })
    }
}
