//
//  RoleMembersWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit

class RoleMembersWireframe: Wireframe {
    typealias ViewController = RoleMembersViewController
    
    fileprivate weak var viewController: RoleMembersViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberMenu(repository: MemberEntity) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_guild = GuildConnector.shared.current_guild ?? GuildEntity()
            vc.current_member = GuildConnector.shared.current_member ?? MemberEntity()
            vc.permission = GuildConnector.shared.current_permission
            viewController?.presentPanModal(vc)
        }
    }
}
