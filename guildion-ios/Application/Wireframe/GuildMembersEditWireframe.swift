//
//  GuildMembersEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class GuildMembersEditWireframe: Wireframe {
    typealias ViewController = GuildMembersEditViewController
    
    fileprivate weak var viewController: GuildMembersEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMemberEdit() {
        viewController?.performSegue(withIdentifier: R.segue.guildMembersEditViewController.toMemberEdit.identifier, sender: nil)
    }
}
