//
//  MemberSelectWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/13.
//

import Foundation
import UIKit

class MemberSelectWireframe: Wireframe {
    typealias ViewController = MemberSelectViewController
    
    fileprivate weak var viewController: MemberSelectViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
