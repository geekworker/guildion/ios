//
//  SessionWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/08.
//

import Foundation
import UIKit

class SessionWireframe: Wireframe {
    typealias ViewController = SessionViewController
    
    fileprivate weak var viewController: SessionViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toMailConfirm() {
        viewController?.performSegue(withIdentifier: R.segue.sessionViewController.toEmailConfirm, sender: nil)
    }
    
    func toProfileSetting() {
        viewController?.performSegue(withIdentifier: R.segue.sessionViewController.toProfileSetting, sender: nil)
    }
}
