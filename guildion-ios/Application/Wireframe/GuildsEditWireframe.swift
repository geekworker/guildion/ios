//
//  GuildsEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class GuildsEditWireframe: Wireframe {
    typealias ViewController = GuildsEditViewController
    
    fileprivate weak var viewController: GuildsEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toGuildEdit() {
        viewController?.performSegue(withIdentifier: R.segue.guildsEditViewController.toGuildSetting.identifier, sender: nil)
    }
}
