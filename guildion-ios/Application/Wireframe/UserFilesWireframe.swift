//
//  UserFilesWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/30.
//

import Foundation
import UIKit

class UserFilesWireframe: Wireframe {
    typealias ViewController = UserFilesViewController
    
    fileprivate weak var viewController: UserFilesViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toPlans() {
        if let vc = R.storyboard.plans.plansViewControllerNavigation() {
            vc.modalPresentationStyle = .overCurrentContext
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toFileMenu(repository: FileEntity, reference: FileReferenceEntity? = nil) {
        if let vc = R.storyboard.folders.fileMenuViewController() {
            vc.repository = repository
            vc.reference = reference
            vc.previewMode = true
            viewController?.presentPanModal(vc)
        }
    }
}
