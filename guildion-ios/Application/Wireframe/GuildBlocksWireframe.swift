//
//  GuildBlocksWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/12.
//

import Foundation
import UIKit

class GuildBlocksWireframe: Wireframe {
    typealias ViewController = GuildBlocksViewController
    
    fileprivate weak var viewController: GuildBlocksViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
