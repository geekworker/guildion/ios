//
//  GuildSectionChannelsEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/01.
//

import Foundation
import UIKit

class GuildSectionChannelsEditWireframe: Wireframe {
    typealias ViewController = GuildSectionChannelsEditViewController
    
    fileprivate weak var viewController: GuildSectionChannelsEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toSectionChannelEdit() {
        viewController?.performSegue(withIdentifier: R.segue.guildSectionChannelsEditViewController.toSectionChannelEdit.identifier, sender: nil)
    }
}
