//
//  MypageWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/13.
//

import Foundation
import UIKit

class MypageWireframe: Wireframe {
    typealias ViewController = MypageViewController
    
    fileprivate weak var viewController: MypageViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    func toPlans() {
        if let vc = R.storyboard.plans.plansViewControllerNavigation() {
            vc.modalPresentationStyle = .overCurrentContext
            UIViewController.topMostController().present(vc, animated: true)
        }
    }
    
    func toUserFiles() {
        if let vc = R.storyboard.mypage.userFilesViewController() {
            let navVC = UINavigationController(rootViewController: vc)
            navVC.modalPresentationStyle = .overCurrentContext
            UIViewController.topMostController().present(navVC, animated: true)
        }
    }
}
