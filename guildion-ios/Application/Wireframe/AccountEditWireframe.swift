//
//  AccountEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/05.
//

import Foundation
import UIKit

class AccountEditWireframe: Wireframe {
    typealias ViewController = AccountEditViewController
    
    fileprivate weak var viewController: AccountEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
