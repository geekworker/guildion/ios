//
//  FileReferencesNewWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/20.
//

import Foundation
import UIKit

class FileReferencesNewWireframe: Wireframe {
    typealias ViewController = FileReferencesNewViewController
    
    fileprivate weak var viewController: FileReferencesNewViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

