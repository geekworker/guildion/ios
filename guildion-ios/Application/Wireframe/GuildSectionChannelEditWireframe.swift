//
//  GuildSectionChannelEditWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2021/01/01.
//

import Foundation
import UIKit

class GuildSectionChannelEditWireframe: Wireframe {
    typealias ViewController = GuildSectionChannelEditViewController
    
    fileprivate weak var viewController: GuildSectionChannelEditViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}
