//
//  StreamChannelParticipantsWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/21.
//

import Foundation
import UIKit

class StreamChannelParticipantsWireframe: Wireframe {
    typealias ViewController = StreamChannelParticipantsViewController
    
    fileprivate weak var viewController: StreamChannelParticipantsViewController?
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
}

