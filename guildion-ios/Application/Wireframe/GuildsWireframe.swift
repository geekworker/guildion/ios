//
//  GuildsWireframe.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/09.
//

import Foundation
import UIKit
import SPPermissions
import SwiftMessages

class GuildsWireframe: Wireframe {
    typealias ViewController = GuildsViewController
    fileprivate weak var viewController: GuildsViewController?
    enum PermissionMode {
        case StreamChannel(
                repository: StreamChannelEntity,
                current_guild: GuildEntity,
                current_member: MemberEntity,
                permission: PermissionModel
             )
        case QRCodeReader
    }
    fileprivate var permission_mode: PermissionMode = .QRCodeReader
    
    required init(viewController: ViewController) {
        self.viewController = viewController
    }
    
    static func replaceViewControllers(sourceViewController: UIViewController, targetViewController: UIViewController) {
        guard let viewControllers = sourceViewController.navigationController?.viewControllers, let guildsVC = viewControllers.filter({ $0 as? GuildsViewController != nil }).first else { return }
        sourceViewController.navigationController?.popToViewController(guildsVC, animated: false)
        sourceViewController.navigationController?.pushViewController(targetViewController, animated: false)
    }
    
    static func replaceViewControllers(sourceViewController: UINavigationController, targetViewController: UIViewController) {
        let viewControllers = sourceViewController.viewControllers
        guard let guildsVC = viewControllers.filter({ $0 as? GuildsViewController != nil }).first else { return }
        sourceViewController.popToViewController(guildsVC, animated: false)
        sourceViewController.pushViewController(targetViewController, animated: false)
    }
    
    func toGuildNew() {
        let vc = R.storyboard.guildNew().instantiateViewController(withIdentifier:
                                                                    R.storyboard.guildNew.guildNewViewController.identifier)
        vc.modalPresentationStyle = .overCurrentContext
        UIViewController.topMostController().present(vc, animated: true)
    }
    
    func toFolders() {
        viewController?.hidesBottomBarWhenPushed = false
        viewController?.performSegue(withIdentifier: R.segue.guildsViewController.toFolders.identifier, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toPlaylists() {
        viewController?.hidesBottomBarWhenPushed = false
        viewController?.performSegue(withIdentifier: R.segue.guildsViewController.toPlaylists.identifier, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toTextChannel() {
        viewController?.hidesBottomBarWhenPushed = true
        viewController?.performSegue(withIdentifier: R.segue.guildsViewController.toTextChannel.identifier, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toDMChannel() {
        viewController?.hidesBottomBarWhenPushed = true
        viewController?.performSegue(withIdentifier: R.segue.guildsViewController.toDMChannel.identifier, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toStreamChannel(repository: StreamChannelEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        guard let navVC = R.storyboard.streamChannel.streamChannelViewControllerNavigation(), let vc = navVC.viewControllers.first as? StreamChannelViewController else { return }
        
        self.permission_mode = .StreamChannel(
            repository: repository,
            current_guild: current_guild,
            current_member: current_member,
            permission: permission
        )
        
        guard configureStreamPermission() else { return }
        
        StreamChannelConnector.shared.checkJoinable(
            current_channel: repository,
            current_guild: current_guild
        ).then({
            if $0 {
                StreamChannelViewControllerBuilder.rebuild(vc)
                vc.initalizeRepository(repository.toNewMemoryChannel(), current_guild: current_guild, current_member: current_member, permission: permission)
                navVC.modalPresentationStyle = .overFullScreen
                self.viewController?.present(navVC, animated: true)
            } else {
                let messageAlert = MessageView.viewFromNib(layout: .cardView)
                messageAlert.configureTheme(.error)
                messageAlert.configureDropShadow()
                messageAlert.configureContent(
                    title: R.string.localizable.error(),
                    body: AppError.custom(reason: R.string.localizable.streamChannelJoinMax()).getLocalizedReason()
                )
                messageAlert.button?.isHidden = true
                SwiftMessages.show(config: SwiftMessages.defaultConfig, view: messageAlert)
            }
        })
    }
    
    func toMemberRequests() {
        viewController?.hidesBottomBarWhenPushed = false
        viewController?.performSegue(withIdentifier: R.segue.guildsViewController.toMemberRequests.identifier, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toMembers() {
        viewController?.hidesBottomBarWhenPushed = false
        viewController?.performSegue(withIdentifier: R.segue.guildsViewController.toMembers.identifier, sender: nil)
        viewController?.hidesBottomBarWhenPushed = false
    }
    
    func toChannel(_ repository: ChannelableProtocol, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        switch repository.channelable_type.superChannel {
        case .TextChannel: self.toTextChannel()
        case .DMChannel: self.toDMChannel()
        case .StreamChannel: self.toStreamChannel(repository: repository as! StreamChannelEntity, current_guild: current_guild, current_member: current_member, permission: permission)
        default: break
        }
    }
    
    func toMemberMenu(repository: MemberEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        if let vc = R.storyboard.guilds.memberMenuViewController() {
            vc.repository = repository
            vc.current_member = current_member
            vc.current_guild = current_guild
            vc.permission = permission
            UIViewController.topMostController().presentPanModal(vc)
        }
    }
    
    func toFileMenu(repository: FileEntity, current_guild: GuildEntity, current_member: MemberEntity, permission: PermissionModel) {
        if let vc = R.storyboard.folders.fileMenuViewController() {
            vc.repository = repository
            vc.initRepository(repository: repository, current_guild: current_guild, current_member: current_member, permission: permission)
            viewController?.presentPanModal(vc)
        }
    }
    
    func toGuildsEdit() {
        guard let navVC = R.storyboard.mypage.guildsEditViewControllerNavigation() else { return }
        navVC.modalPresentationStyle = .overFullScreen
        viewController?.present(navVC, animated: true)
    }
    
    func toGuildQRCodeReader() {
        self.permission_mode = .QRCodeReader
        if configurePhotoPermission() {
            guard let navVC = R.storyboard.guildQRCodeReader.guildQRCodeReaderViewControllerNavigation() else { return }
            navVC.modalPresentationStyle = .overFullScreen
            viewController?.present(navVC, animated: true)
        }
    }
}

extension GuildsWireframe: SPPermissionsDelegate, SPPermissionsDataSource {
    func configurePhotoPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter({ !$0.isAuthorized })
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self.viewController!)
        return false
    }
    
    func configureStreamPermission() -> Bool {
        let permissions: [SPPermission] = [.photoLibrary, .camera, .microphone].filter({ !$0.isAuthorized })
        guard !permissions.isEmpty else { return true }
        let controller = SPPermissions.list(permissions)
        controller.titleText = R.string.localizable.permissionTitle()
        controller.headerText = R.string.localizable.permissionBody()
        controller.footerText = R.string.localizable.permissionFooter()
        controller.dataSource = self
        controller.delegate = self
        controller.present(on: self.viewController!)
        return false
    }
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        switch permission {
        case .camera:
            cell.permissionTitleLabel.text = R.string.localizable.permissionCamera()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionCameraText()
        case .microphone:
            cell.permissionTitleLabel.text = R.string.localizable.permissionVoice()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionVoiceText()
        case .photoLibrary:
            cell.permissionTitleLabel.text = R.string.localizable.permissionPhoto()
            cell.permissionDescriptionLabel.text = R.string.localizable.permissionPhotoText()
        default: break
        }
        return cell
    }
    
    func didHide(permissions ids: [Int]) {
        let permissions: [SPPermission] = [.photoLibrary, .camera].filter { !$0.isAuthorized }
        guard permissions.isEmpty else { return }
        switch permission_mode {
        case .QRCodeReader: self.toGuildQRCodeReader()
        case .StreamChannel(let repository, let current_guild, let current_member, let permission):
            self.toStreamChannel(repository: repository, current_guild: current_guild, current_member: current_member, permission: permission)
        }
    }
}
