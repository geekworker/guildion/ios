//
//  RoleEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

class RoleEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var GuildId: Int?
    public var uid: String = ""
    public var name: String = ""
    public var color: String = ""
    public var priority: Int = 0
    public var is_default: Bool = false
    public var is_everyone: Bool = false
    public var mentionable: Bool = false
    public var admin: Bool = false
    public var guild_managable: Bool = false
    public var roles_managable: Bool = false
    public var channels_managable: Bool = false
    public var channels_invitable: Bool = false
    public var kickable: Bool = false
    public var banable: Bool = false
    public var member_acceptable: Bool = false
    public var member_managable: Bool = false
    public var members_managable: Bool = false
    public var channels_viewable: Bool = false
    public var message_sendable: Bool = false
    public var messages_managable: Bool = false
    public var message_embeddable: Bool = false
    public var message_attachable: Bool = false
    public var messages_readable: Bool = false
    public var message_mentionable: Bool = false
    public var message_reactionable: Bool = false
    public var stream_connectable: Bool = false
    public var stream_speekable: Bool = false
    public var stream_livestreamable: Bool = false
    public var movie_selectable: Bool = false
    public var stream_mutable: Bool = false
    public var stream_deafenable: Bool = false
    public var stream_movable: Bool = false
    public var folders_viewable: Bool = true
    public var folder_creatable: Bool = true
    public var folders_managable: Bool = true
    public var files_viewable: Bool = true
    public var file_creatable: Bool = true
    public var files_managable: Bool = true
    public var files_downloadable: Bool = true
    public var permission: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Guild: GuildEntity?
    public var Members: MemberEntities?
    public var MemberRoles: MemberRoleEntities?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.color <- map["color"]
        self.priority <- map["priority"]
        self.is_default <- map["is_default"]
        self.is_everyone <- map["is_everyone"]
        self.mentionable <- map["mentionable"]
        self.admin <- map["admin"]
        self.guild_managable <- map["guild_managable"]
        self.roles_managable <- map["roles_managable"]
        self.channels_managable <- map["channels_managable"]
        self.channels_invitable <- map["channels_invitable"]
        self.kickable <- map["kickable"]
        self.banable <- map["banable"]
        self.member_acceptable <- map["member_acceptable"]
        self.member_managable <- map["member_managable"]
        self.members_managable <- map["members_managable"]
        self.channels_viewable <- map["channels_viewable"]
        self.message_sendable <- map["message_sendable"]
        self.messages_managable <- map["messages_managable"]
        self.message_embeddable <- map["message_embeddable"]
        self.message_attachable <- map["message_attachable"]
        self.messages_readable <- map["messages_readable"]
        self.message_mentionable <- map["message_mentionable"]
        self.message_reactionable <- map["message_reactionable"]
        self.stream_connectable <- map["stream_connectable"]
        self.stream_speekable <- map["stream_speekable"]
        self.stream_livestreamable <- map["stream_livestreamable"]
        self.movie_selectable <- map["movie_selectable"]
        self.stream_mutable <- map["stream_mutable"]
        self.stream_deafenable <- map["stream_deafenable"]
        self.stream_movable <- map["stream_movable"]
        self.folders_viewable <- map["folders_viewable"]
        self.folder_creatable <- map["folder_creatable"]
        self.folders_managable <- map["folders_managable"]
        self.files_viewable <- map["files_viewable"]
        self.file_creatable <- map["file_creatable"]
        self.files_managable <- map["files_managable"]
        self.files_downloadable <- map["files_downloadable"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.MemberRoles = MemberRoleEntities.init(map: map["MemberRoles"])
        self.Members = MemberEntities.init(map: map["Members"])
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.color <- map["color"]
        self.priority <- map["priority"]
        self.is_default <- map["is_default"]
        self.is_everyone <- map["is_everyone"]
        self.mentionable <- map["mentionable"]
        self.admin <- map["admin"]
        self.guild_managable <- map["guild_managable"]
        self.roles_managable <- map["roles_managable"]
        self.channels_managable <- map["channels_managable"]
        self.channels_invitable <- map["channels_invitable"]
        self.kickable <- map["kickable"]
        self.banable <- map["banable"]
        self.member_acceptable <- map["member_acceptable"]
        self.member_managable <- map["member_managable"]
        self.members_managable <- map["members_managable"]
        self.channels_viewable <- map["channels_viewable"]
        self.message_sendable <- map["message_sendable"]
        self.messages_managable <- map["messages_managable"]
        self.message_embeddable <- map["message_embeddable"]
        self.message_attachable <- map["message_attachable"]
        self.messages_readable <- map["messages_readable"]
        self.message_mentionable <- map["message_mentionable"]
        self.message_reactionable <- map["message_reactionable"]
        self.stream_connectable <- map["stream_connectable"]
        self.stream_speekable <- map["stream_speekable"]
        self.stream_livestreamable <- map["stream_livestreamable"]
        self.movie_selectable <- map["movie_selectable"]
        self.stream_mutable <- map["stream_mutable"]
        self.stream_deafenable <- map["stream_deafenable"]
        self.stream_movable <- map["stream_movable"]
        self.folders_viewable <- map["folders_viewable"]
        self.folder_creatable <- map["folder_creatable"]
        self.folders_managable <- map["folders_managable"]
        self.files_viewable <- map["files_viewable"]
        self.file_creatable <- map["file_creatable"]
        self.files_managable <- map["files_managable"]
        self.files_downloadable <- map["files_downloadable"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.MemberRoles = MemberRoleEntities.init(map: map["MemberRoles"])
        self.Members = MemberEntities.init(map: map["Members"])
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guild == nil { self.Guild = GuildEntity() }
        if self.Members == nil { self.Members = MemberEntities() }
        if self.MemberRoles == nil { self.MemberRoles = MemberRoleEntities() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.color <- map["color"]
                self.priority <- map["priority"]
                self.is_default <- map["is_default"]
                self.is_everyone <- map["is_everyone"]
                self.mentionable <- map["mentionable"]
                self.admin <- map["admin"]
                self.guild_managable <- map["guild_managable"]
                self.roles_managable <- map["roles_managable"]
                self.channels_managable <- map["channels_managable"]
                self.channels_invitable <- map["channels_invitable"]
                self.kickable <- map["kickable"]
                self.banable <- map["banable"]
                self.member_acceptable <- map["member_acceptable"]
                self.member_managable <- map["member_managable"]
                self.members_managable <- map["members_managable"]
                self.channels_viewable <- map["channels_viewable"]
                self.message_sendable <- map["message_sendable"]
                self.messages_managable <- map["messages_managable"]
                self.message_embeddable <- map["message_embeddable"]
                self.message_attachable <- map["message_attachable"]
                self.messages_readable <- map["messages_readable"]
                self.message_mentionable <- map["message_mentionable"]
                self.message_reactionable <- map["message_reactionable"]
                self.stream_connectable <- map["stream_connectable"]
                self.stream_speekable <- map["stream_speekable"]
                self.stream_livestreamable <- map["stream_livestreamable"]
                self.movie_selectable <- map["movie_selectable"]
                self.stream_mutable <- map["stream_mutable"]
                self.stream_deafenable <- map["stream_deafenable"]
                self.stream_movable <- map["stream_movable"]
                self.folders_viewable <- map["folders_viewable"]
                self.folder_creatable <- map["folder_creatable"]
                self.folders_managable <- map["folders_managable"]
                self.files_viewable <- map["files_viewable"]
                self.file_creatable <- map["file_creatable"]
                self.files_managable <- map["files_managable"]
                self.files_downloadable <- map["files_downloadable"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Guild!.asyncMapping(map: map["Guild"])
        promises += self.Members!.asyncMapping(map: map["Members"])
        promises += self.MemberRoles!.asyncMapping(map: map["MemberRoles"])
        return promises
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        
        let promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.color <- map["color"]
                self.priority <- map["priority"]
                self.is_default <- map["is_default"]
                self.is_everyone <- map["is_everyone"]
                self.mentionable <- map["mentionable"]
                self.admin <- map["admin"]
                self.guild_managable <- map["guild_managable"]
                self.roles_managable <- map["roles_managable"]
                self.channels_managable <- map["channels_managable"]
                self.channels_invitable <- map["channels_invitable"]
                self.kickable <- map["kickable"]
                self.banable <- map["banable"]
                self.member_acceptable <- map["member_acceptable"]
                self.member_managable <- map["member_managable"]
                self.members_managable <- map["members_managable"]
                self.channels_viewable <- map["channels_viewable"]
                self.message_sendable <- map["message_sendable"]
                self.messages_managable <- map["messages_managable"]
                self.message_embeddable <- map["message_embeddable"]
                self.message_attachable <- map["message_attachable"]
                self.messages_readable <- map["messages_readable"]
                self.message_mentionable <- map["message_mentionable"]
                self.message_reactionable <- map["message_reactionable"]
                self.stream_connectable <- map["stream_connectable"]
                self.stream_speekable <- map["stream_speekable"]
                self.stream_livestreamable <- map["stream_livestreamable"]
                self.movie_selectable <- map["movie_selectable"]
                self.stream_mutable <- map["stream_mutable"]
                self.stream_deafenable <- map["stream_deafenable"]
                self.stream_movable <- map["stream_movable"]
                self.folders_viewable <- map["folders_viewable"]
                self.folder_creatable <- map["folder_creatable"]
                self.folders_managable <- map["folders_managable"]
                self.files_viewable <- map["files_viewable"]
                self.file_creatable <- map["file_creatable"]
                self.files_managable <- map["files_managable"]
                self.files_downloadable <- map["files_downloadable"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "color": self.color,
            "priority": self.priority,
            "is_default": self.is_default,
            "is_everyone": self.is_everyone,
            "mentionable": self.mentionable,
            "admin": self.admin,
            "guild_managable": self.guild_managable,
            "roles_managable": self.roles_managable,
            "channels_managable": self.channels_managable,
            "channels_invitable": self.channels_invitable,
            "kickable": self.kickable,
            "banable": self.banable,
            "member_acceptable": self.member_acceptable,
            "member_managable": self.member_managable,
            "members_managable": self.members_managable,
            "channels_viewable": self.channels_viewable,
            "message_sendable": self.message_sendable,
            "messages_managable": self.messages_managable,
            "message_embeddable": self.message_embeddable,
            "message_attachable": self.message_attachable,
            "messages_readable": self.messages_readable,
            "message_mentionable": self.message_mentionable,
            "message_reactionable": self.message_reactionable,
            "stream_connectable": self.stream_connectable,
            "stream_speekable": self.stream_speekable,
            "stream_livestreamable": self.stream_livestreamable,
            "movie_selectable": self.movie_selectable,
            "stream_mutable": self.stream_mutable,
            "stream_deafenable": self.stream_deafenable,
            "stream_movable": self.stream_movable,
            "folders_viewable": self.folders_viewable,
            "folder_creatable": self.folder_creatable,
            "folders_managable": self.folders_managable,
            "files_viewable": self.files_viewable,
            "file_creatable": self.file_creatable,
            "files_managable": self.files_managable,
            "files_downloadable": self.files_downloadable,
            "permission": self.permission,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Guild": self.Guild?.toJSON() ?? [],
            "MemberRoles": self.MemberRoles?.toJSON() ?? [],
            "Members": self.Members?.toJSON() ?? [],
        ]
    }
    
    func toEveryOneBasedRole(everyone_role: RoleEntity) {
        self.Guild = everyone_role.Guild?.toNewMemory()
        self.GuildId = everyone_role.GuildId
        self.color = everyone_role.color
        self.priority = everyone_role.priority
        self.mentionable = everyone_role.mentionable
        self.admin = everyone_role.admin
        self.guild_managable = everyone_role.guild_managable
        self.roles_managable = everyone_role.roles_managable
        self.channels_managable = everyone_role.channels_managable
        self.channels_invitable = everyone_role.channels_invitable
        self.kickable = everyone_role.kickable
        self.banable = everyone_role.banable
        self.member_acceptable = everyone_role.member_acceptable
        self.member_managable = everyone_role.member_managable
        self.members_managable = everyone_role.members_managable
        self.channels_viewable = everyone_role.channels_viewable
        self.message_sendable = everyone_role.message_sendable
        self.messages_managable = everyone_role.messages_managable
        self.message_embeddable = everyone_role.message_embeddable
        self.message_attachable = everyone_role.message_attachable
        self.messages_readable = everyone_role.messages_readable
        self.message_mentionable = everyone_role.message_mentionable
        self.message_reactionable = everyone_role.message_reactionable
        self.stream_connectable = everyone_role.stream_connectable
        self.stream_speekable = everyone_role.stream_speekable
        self.stream_livestreamable = everyone_role.stream_livestreamable
        self.movie_selectable = everyone_role.movie_selectable
        self.stream_mutable = everyone_role.stream_mutable
        self.stream_deafenable = everyone_role.stream_deafenable
        self.stream_movable = everyone_role.stream_movable
        self.folders_viewable = everyone_role.folders_viewable
        self.folder_creatable = everyone_role.folder_creatable
        self.folders_managable = everyone_role.folders_managable
        self.files_viewable = everyone_role.files_viewable
        self.file_creatable = everyone_role.file_creatable
        self.files_managable = everyone_role.files_managable
        self.files_downloadable = everyone_role.files_downloadable
        self.permission = everyone_role.permission
    }
    
    func toNewMemory() -> RoleEntity {
        let new = RoleEntity()
        new.id = self.id
        new.GuildId = self.GuildId
        new.uid = self.uid
        new.name = self.name
        new.color = self.color
        new.priority = self.priority
        new.is_everyone = self.is_everyone
        new.is_default = self.is_default
        new.mentionable = self.mentionable
        new.admin = self.admin
        new.guild_managable = self.guild_managable
        new.roles_managable = self.roles_managable
        new.channels_managable = self.channels_managable
        new.channels_invitable = self.channels_invitable
        new.kickable = self.kickable
        new.banable = self.banable
        new.member_acceptable = self.member_acceptable
        new.member_managable = self.member_managable
        new.members_managable = self.members_managable
        new.channels_viewable = self.channels_viewable
        new.message_sendable = self.message_sendable
        new.messages_managable = self.messages_managable
        new.message_embeddable = self.message_embeddable
        new.message_attachable = self.message_attachable
        new.messages_readable = self.messages_readable
        new.message_mentionable = self.message_mentionable
        new.message_reactionable = self.message_reactionable
        new.stream_connectable = self.stream_connectable
        new.stream_speekable = self.stream_speekable
        new.stream_livestreamable = self.stream_livestreamable
        new.movie_selectable = self.movie_selectable
        new.stream_mutable = self.stream_mutable
        new.stream_deafenable = self.stream_deafenable
        new.stream_movable = self.stream_movable
        new.folders_viewable = self.folders_viewable
        new.folder_creatable = self.folder_creatable
        new.folders_managable = self.folders_managable
        new.files_viewable = self.files_viewable
        new.file_creatable = self.file_creatable
        new.files_managable = self.files_managable
        new.files_downloadable = self.files_downloadable
        new.permission = self.permission
        new.Guild = self.Guild?.toNewMemory()
        return new
    }
}

class RoleEntities: Entities, PropertyNames, Mappable {
    var items: [RoleEntity] = []
    var prioritest_role: RoleEntity? {
        return items.max { $0.priority < $1.priority }
    }
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncPureMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension RoleEntity: Equatable {
    static func == (lhs: RoleEntity, rhs: RoleEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.name == rhs.name && lhs.GuildId == rhs.GuildId }
        return lid == rid
    }
}

extension RoleEntities: Equatable {
    static func == (lhs: RoleEntities, rhs: RoleEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
