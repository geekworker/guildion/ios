//
//  StreamEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Hydra

class StreamEntity: ChannelogableEntity, Entity, PropertyNames, SectionChannelRowable {
    public var id: Int?
    public var StreamableId: Int?
    public var streamable_type: StreamableType = .File
    public var ChannelId: Int?
    public var delivery_start_at: Date = Date.CurrentDate()
    public var delivery_end_at: Date = Date.CurrentDate()
    public var duration: TimeInterval = TimeInterval.init(0)
    public var is_live: Bool = false
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var FileReference: FileReferenceEntity? {
        didSet {
            self.mode = Mode.init(repository: self)
        }
    }
    public var File: FileEntity? {
        didSet {
            self.mode = Mode.init(repository: self)
        }
    }
    public var Files: FileEntities? {
        didSet {
            self.mode = Mode.init(repository: self)
        }
    }
    public var Streamable: StreamableEntity?
    public var Channel: StreamChannelEntity?
    public var Participants: ParticipantEntities?
    
    override var channelogable_id: Int? {
        get { return self.id }
        set { self.id = newValue }
    }
    override var channelogable_type: ChannelogableType? {
        get { return .Stream }
        set { }
    }
    
    public var index: Int = 0
    public var sectionChannelRowType: SectionChannelRowableType = .Stream
    
    enum Mode: Int {
        case fileMovie
        case fileYouTube
        case fileReferenceMovie
        case fileReferenceYouTube
        case other
        
        init(repository: StreamEntity) {
            guard repository.StreamableId != 0 else { self = .other; return  }
            
            if let file = repository.File, file.provider == .youtube, file.Provider?.youtubeID != nil {
                self = .fileYouTube
            } else if let file = repository.File, file.format.content_type.is_movie {
                self = .fileMovie
            } else if let fileReference = repository.FileReference, let file = fileReference.File, file.provider == .youtube, file.Provider?.youtubeID != nil {
                self = .fileReferenceYouTube
            } else if let fileReference = repository.FileReference, let file = fileReference.File, file.format.content_type.is_movie {
                self = .fileReferenceMovie
            } else {
                self = .other
            }
        }
    }
    public var mode: Mode = .other
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
        self.id <- map["id"]
        self.StreamableId <- map["StreamableId"]
        self.streamable_type <- (map["streamable_type"], EnumTransform<StreamableType>())
        self.ChannelId <- map["ChannelId"]
        self.delivery_start_at <- map["delivery_start_at"]
        self.delivery_end_at <- map["delivery_end_at"]
        var ms: Double = 0
        ms <- map["duration"]
        self.duration = TimeInterval.init(millisecond: ms)
        self.is_live <- map["is_live"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Streamable = StreamableEntity.init(map: map)
        self.Channel <- map["Channel"]
        self.Participants = ParticipantEntities(map: map["Participants"])
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        self.id <- map["id"]
        self.StreamableId <- map["StreamableId"]
        self.streamable_type <- (map["streamable_type"], EnumTransform<StreamableType>())
        self.ChannelId <- map["ChannelId"]
        self.delivery_start_at <- map["delivery_start_at"]
        self.delivery_end_at <- map["delivery_end_at"]
        var ms: Double = 0
        ms <- map["duration"]
        self.duration = TimeInterval.init(millisecond: ms)
        self.is_live <- map["is_live"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Streamable = StreamableEntity.init(map: map)
        self.Channel <- map["Channel"]
        self.Participants = ParticipantEntities(map: map["Participants"])
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Channel == nil { self.Channel = StreamChannelEntity() }
        if self.File == nil { self.File = FileEntity() }
        if self.FileReference == nil { self.FileReference = FileReferenceEntity() }
        if self.Participants == nil { self.Participants = ParticipantEntities() }
        
        self.setStreamable(map: map)
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.StreamableId <- map["StreamableId"]
                self.streamable_type <- (map["streamable_type"], EnumTransform<StreamableType>())
                self.ChannelId <- map["ChannelId"]
                self.delivery_start_at <- map["delivery_start_at"]
                self.delivery_end_at <- map["delivery_end_at"]
                var ms: Double = 0
                ms <- map["duration"]
                self.duration = TimeInterval.init(millisecond: ms)
                self.is_live <- map["is_live"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Channel!.asyncMapping(map: map["Channel"])
        promises += self.Participants!.asyncMapping(map: map["Participants"])
        if let unwrapped_type = self.Streamable?.streamable_type, let unwrapped_map = self.Streamable?.streamable_map {
            switch(unwrapped_type) {
            case .File: promises += self.File?.asyncMapping(map: unwrapped_map["Streamable"]) ?? []
            case .FileReference: promises += self.FileReference?.asyncMapping(map: unwrapped_map["Streamable"]) ?? []
            }
        }
        return promises
    }
    
    func asyncMappingFast(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.File == nil { self.File = FileEntity() }
        if self.FileReference == nil { self.FileReference = FileReferenceEntity() }
        self.Streamable = StreamableEntity.init(map: map)
        self.streamable_type <- (map["streamable_type"], EnumTransform<StreamableType>())
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.StreamableId <- map["StreamableId"]
                self.ChannelId <- map["ChannelId"]
                self.delivery_start_at <- map["delivery_start_at"]
                self.delivery_end_at <- map["delivery_end_at"]
                var ms: Double = 0
                ms <- map["duration"]
                self.duration = TimeInterval.init(millisecond: ms)
                self.is_live <- map["is_live"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        switch(self.streamable_type) {
        case .File: promises += self.File?.asyncPureMapping(map: map["Streamable"]) ?? []
        case .FileReference: promises += self.FileReference?.asyncPureMapping(map: map["Streamable"]) ?? []
        }
        return promises
    }
    
    func setStreamable(map: Map) {
        self.Streamable = StreamableEntity.init(map: map)
        self.File = self.Streamable?.upcasting() as? FileEntity
        self.FileReference = self.Streamable?.upcasting() as? FileReferenceEntity
    }
    
    func toJSON() -> [String : Any] {
        var raw: [String : Any] = [
            "id": self.id ?? 0,
            "streamable_id": self.StreamableId ?? 0,
            "streamable_type": self.streamable_type.rawValue,
            "ChannelId": self.ChannelId ?? 0,
            "delivery_start_at": self.delivery_start_at,
            "delivery_end_at": self.delivery_end_at,
            "duration": self.duration * 1000,
            "is_live": self.is_live,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Files": self.Files?.toJSON() ?? [],
            // "Channel": self.Channel?.toJSON() ?? [],
            // "Participants": self.Participants?.toJSON() ?? [],
        ]
        if self.Files == nil, let unwapped = self.Streamable as? FileEntity {
            raw["Streamable"] = unwapped.toNewMemory().toJSON()
            raw["streamable_type"] = StreamableType.File.rawValue
        }
        if self.Files == nil, let unwapped = self.Streamable as? FileReferenceEntity {
            raw["Streamable"] = unwapped.toNewMemory().toJSON()
            raw["streamable_type"] = StreamableType.FileReference.rawValue
        }
        if self.Files != nil {
            raw["streamable_type"] = StreamableType.FileReference.rawValue
        }
        if self.Files == nil, self.Streamable == nil, self.StreamableId == nil {
            raw["streamable_type"] = nil
        }
        return raw
    }
}

class StreamEntities: Entities, PropertyNames, Mappable {
    var items: [StreamEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension StreamEntity /* : Equatable */ {
    static func == (lhs: StreamEntity, rhs: StreamEntity) -> Bool {
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension StreamEntities: Equatable {
    static func == (lhs: StreamEntities, rhs: StreamEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}

extension StreamEntity: WebsocketPayloadable {
    typealias Element = StreamEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "streamable_id": self.StreamableId ?? 0,
            "streamable_type": self.streamable_type.rawValue,
            "ChannelId": self.ChannelId ?? 0,
            "is_live": self.is_live,
            "permission": self.permission,
            "is_private": self.is_private,
            "File": self.File?.toWebsocketPayload() as Any,
            "FileReference": self.FileReference?.toWebsocketPayload() as Any,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> Element? {
        let element = StreamEntity()
        guard let id = json["id"].number, id != 0 else { return nil }
        element.id = Int(truncating: id)
        element.StreamableId = json["streamable_id"].intValue
        element.streamable_type = StreamableType(rawValue: json["streamable_type"].stringValue) ?? .File
        element.ChannelId = json["ChannelId"].intValue
        element.is_live = json["is_live"].boolValue
        element.permission = json["permission"].boolValue
        element.is_private = json["is_private"].boolValue
        element.Streamable = StreamableEntity()
        element.Streamable?.streamable_type = element.streamable_type
        element.Streamable?.streamable_id = element.StreamableId
        element.File = FileEntity.jsonable(json["File"])
        element.FileReference = FileReferenceEntity.jsonable(json["FileReference"])
        element.mode = Mode.init(repository: element)
        return element
    }
}
