//
//  ParticipantEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra

protocol ParticipantEntityDelegate: class {
    func participantEntity(didChange meterLevel: Float, in fileEntity: ParticipantEntity)
}

class ParticipantEntity: Entity, PropertyNames, Mappable, SectionChannelRowable {
    public var id: Int?
    public var MemberId: Int?
    public var StreamId: Int?
    public var delivery_start_at: Date = Date.CurrentDate()
    public var delivery_end_at: Date = Date.CurrentDate()
    public var index: Int = 0
    public var camera_enable: Bool = false
    public var mic_enable: Bool = false
    public var is_live: Bool = false
    public var meterLevel: Float = 0 {
        didSet {
            self.delegate?.participantEntity(didChange: meterLevel, in: self)
        }
    }
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Member: MemberEntity?
    public var Stream: StreamEntity?
    var sectionChannelRowType: SectionChannelRowableType = .Participant
    weak var delegate: ParticipantEntityDelegate?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.StreamId <- map["StreamId"]
        self.delivery_start_at <- map["delivery_start_at"]
        self.delivery_end_at <- map["delivery_end_at"]
        self.camera_enable <- map["camera_enable"]
        self.mic_enable <- map["mic_enable"]
        self.is_live <- map["is_live"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Stream <- map["Stream"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.StreamId <- map["StreamId"]
        self.delivery_start_at <- map["delivery_start_at"]
        self.delivery_end_at <- map["delivery_end_at"]
        self.camera_enable <- map["camera_enable"]
        self.mic_enable <- map["mic_enable"]
        self.is_live <- map["is_live"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Stream <- map["Stream"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Member == nil { self.Member = MemberEntity() }
        if self.Stream == nil { self.Stream = StreamEntity() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.StreamId <- map["StreamId"]
                self.delivery_start_at <- map["delivery_start_at"]
                self.delivery_end_at <- map["delivery_end_at"]
                self.camera_enable <- map["camera_enable"]
                self.mic_enable <- map["mic_enable"]
                self.is_live <- map["is_live"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Member!.asyncMapping(map: map["Member"])
        promises += self.Stream!.asyncMapping(map: map["Stream"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "MemberId": self.MemberId ?? 0,
            "StreamId": self.StreamId ?? 0,
            "delivery_start_at": self.delivery_start_at,
            "delivery_end_at": self.delivery_end_at,
            "camera_enable": self.camera_enable,
            "mic_enable": self.mic_enable,
            "is_live": self.is_live,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Member": self.Member?.toJSON() ?? [],
            "Stream": self.Stream?.toJSON() ?? [],
        ]
    }
}

class ParticipantEntities: Entities, PropertyNames, Mappable {
    var items: [ParticipantEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension ParticipantEntity: Equatable {
    static func == (lhs: ParticipantEntity, rhs: ParticipantEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension ParticipantEntities: Equatable {
    static func == (lhs: ParticipantEntities, rhs: ParticipantEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
