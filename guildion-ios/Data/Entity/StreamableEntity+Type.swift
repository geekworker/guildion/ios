//
//  StreamableEntity+Type.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/30.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

enum StreamableType: String {
    case File = "File"
    case FileReference = "FileReference"
}

class StreamableEntity: Mappable {
    var streamable_type: StreamableType?
    var streamable_id: Int?
    var streamable_uid: String?
    var streamable_map: Map?

    init() {}
    
    required init?(map: Map) {
        streamable_type <- (map["streamable_type"], EnumTransform<StreamableType>())
        streamable_map = map
    }

    func mapping(map: Map) {
        streamable_type <- (map["streamable_type"], EnumTransform<StreamableType>())
        streamable_map = map
    }

    static func objectForMapping(map: Map) -> StreamableEntity? {
        let typeString: String? = map["streamable_type"].value()
        if let typeString = typeString {
            let streamable_type: StreamableType? = StreamableType(rawValue: typeString)
            if let streamable_type = streamable_type {
                switch(streamable_type) {
                case .File:
                    var entity = FileEntity()
                    entity <- map["Streamable"]
                    return entity
                case .FileReference:
                    var entity = FileReferenceEntity()
                    entity <- map["Streamable"]
                    return entity
                }
            }
        }
        return nil
    }
    
    func upcasting() -> StreamableEntity? {
        guard let unwrapped_type = streamable_type, let unwrapped_map = streamable_map else { return nil }
        switch(unwrapped_type) {
        case .File:
            var entity = FileEntity()
            entity <- unwrapped_map["Streamable"]
            return entity
        case .FileReference:
            var entity = FileReferenceEntity()
            entity <- unwrapped_map["Streamable"]
            return entity
        }
    }
}

protocol StreamableEntities: class {
    associatedtype Element
    var items: [Element] { get set }
}
