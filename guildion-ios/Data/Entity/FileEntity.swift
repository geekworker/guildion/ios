//
//  FileEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Hydra

protocol FileEntityDelegate: class {
    func fileEntity(didChange progress: Float, in fileEntity: FileEntity)
}

class FileEntity: StreamableEntity, PropertyNames {
    public var id: Int?
    public var MemberId: Int?
    public var uid: String = UUID().uuidString
    public var name: String = ""
    public var description: String = ""
    public var thumbnail: String = ""
    public var url: String = ""
    public var thumbnail_source: String = ""
    public var url_source: String = ""
    public var url_image: UIImage?
    public var format: FileFormat = .other
    public var size_byte: Double = 0
    public var duration: TimeInterval = TimeInterval.init(0)
    public var pixel_width: Double = 0
    public var pixel_height: Double = 0
    public var is_deleted: Bool = true
    public var provider: ProviderType = .other
    public var view_count: Int = 0
    public var downloadable: Bool = false
    public var is_uploaded: Bool = false
    public var is_upload_failed: Bool = false
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Streams: StreamEntities?
    public var Folders: FolderEntities?
    public var StreamChannels: StreamChannelEntities?
    public var Messages: MessageEntities?
    public var Member: MemberEntity?
    public var FileReference: FileReferenceEntity?
    public var Provider: ProviderEntity?
    public var progress: Float?
    
    override var streamable_id: Int? {
        get { return self.id }
        set { self.id = newValue }
    }
    override var streamable_uid: String? {
        get { return self.uid }
        set { self.uid = newValue ?? "" }
    }
    override var streamable_type: StreamableType? {
        get { return .File }
        set { }
    }
    
    public lazy var byteCountFormatter: ByteCountFormatter = {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = .useAll
        formatter.countStyle = .file
        formatter.includesUnit = true
        formatter.isAdaptive = true
        return formatter
    }()
    public var size: String {
        return self.size_byte == 0 ? "0 byte" : byteCountFormatter.string(fromByteCount: Int64(self.size_byte))
    }
    public var formattedDuration: String {
        let duration = self.id == nil ? self.duration / 1000 : self.duration
        return duration.hour > 0 ? String(format:"%0d:%02d:%02d", duration.hour, duration.minute, duration.second) : String(format:"%0d:%02d", duration.minute, duration.second)
    }
    
    static func getFormattedDuration(duration: TimeInterval) -> String {
        return duration.hour > 0 ? String(format:"%0d:%02d:%02d", duration.hour, duration.minute, duration.second) : String(format:"%0d:%02d", duration.minute, duration.second)
    }
    public weak var delegate: FileEntityDelegate?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.thumbnail <- map["thumbnail"]
        self.url <- map["url"]
        self.format <- (map["format"], EnumTransform<FileFormat>())
        self.size_byte <- map["size_byte"]
        var ms: Double = 0
        ms <- map["duration"]
        self.duration = TimeInterval.init(millisecond: ms)
        self.pixel_width <- map["pixel_width"]
        self.pixel_height <- map["pixel_height"]
        self.is_deleted <- map["is_deleted"]
        self.provider <- (map["provider"], EnumTransform<ProviderType>())
        self.view_count <- map["view_count"]
        self.downloadable <- map["downloadable"]
        self.is_uploaded <- map["is_uploaded"]
        self.is_upload_failed <- map["is_upload_failed"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Streams = StreamEntities(map: map["Streams"])
        self.Folders = FolderEntities(map: map["Folders"])
        self.StreamChannels = StreamChannelEntities(map: map["StreamChannels"])
        self.Messages = MessageEntities(map: map["Messages"])
        self.Member <- map["Member"]
        self.FileReference <- map["FileReference"]
        self.Provider = ProviderEntity.init(url: self.url)
    }
    
    override func mapping(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.thumbnail <- map["thumbnail"]
        self.url <- map["url"]
        self.format <- (map["format"], EnumTransform<FileFormat>())
        var ms: Double = 0
        ms <- map["duration"]
        self.duration = TimeInterval.init(millisecond: ms)
        self.pixel_width <- map["pixel_width"]
        self.pixel_height <- map["pixel_height"]
        self.size_byte <- map["size_byte"]
        self.is_deleted <- map["is_deleted"]
        self.provider <- (map["provider"], EnumTransform<ProviderType>())
        self.view_count <- map["view_count"]
        self.downloadable <- map["downloadable"]
        self.permission <- map["permission"]
        self.is_uploaded <- map["is_uploaded"]
        self.is_upload_failed <- map["is_upload_failed"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Streams = StreamEntities(map: map["Streams"])
        self.Folders = FolderEntities(map: map["Folders"])
        self.StreamChannels = StreamChannelEntities(map: map["StreamChannels"])
        self.Messages = MessageEntities(map: map["Messages"])
        self.Member <- map["Member"]
        self.FileReference <- map["FileReference"]
        self.Provider = ProviderEntity.init(url: self.url)
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Streams == nil { self.Streams = StreamEntities() }
        if self.Folders == nil { self.Folders = FolderEntities() }
        if self.StreamChannels == nil { self.StreamChannels = StreamChannelEntities() }
        if self.Messages == nil { self.Messages = MessageEntities() }
        if self.Member == nil {  Member = MemberEntity() }
        if self.FileReference == nil { FileReference = FileReferenceEntity() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.thumbnail <- map["thumbnail"]
                self.url <- map["url"]
                self.format <- (map["format"], EnumTransform<FileFormat>())
                var ms: Double = 0
                ms <- map["duration"]
                self.duration = TimeInterval.init(millisecond: ms)
                self.pixel_width <- map["pixel_width"]
                self.pixel_height <- map["pixel_height"]
                self.size_byte <- map["size_byte"]
                self.is_deleted <- map["is_deleted"]
                self.provider <- (map["provider"], EnumTransform<ProviderType>())
                self.view_count <- map["view_count"]
                self.permission <- map["permission"]
                self.downloadable <- map["downloadable"]
                self.is_uploaded <- map["is_uploaded"]
                self.is_upload_failed <- map["is_upload_failed"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                self.Provider = ProviderEntity.init(url: self.url)
                resolve(())
            },
        ]
        promises += self.Streams!.asyncMapping(map: map["Streams"])
        promises += self.Folders!.asyncMapping(map: map["Folders"])
        promises += self.StreamChannels!.asyncMapping(map: map["StreamChannels"])
        promises += self.Messages!.asyncMapping(map: map["Messages"])
        promises += self.Member!.asyncMapping(map: map["Member"])
        promises += self.FileReference!.asyncMapping(map: map["FileReference"])
        return promises
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        let promises = [
            Promise<Void> (in: .background) { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.thumbnail <- map["thumbnail"]
                self.url <- map["url"]
                self.format <- (map["format"], EnumTransform<FileFormat>())
                var ms: Double = 0
                ms <- map["duration"]
                self.duration = TimeInterval.init(millisecond: ms)
                self.pixel_width <- map["pixel_width"]
                self.pixel_height <- map["pixel_height"]
                self.size_byte <- map["size_byte"]
                self.is_deleted <- map["is_deleted"]
                self.provider <- (map["provider"], EnumTransform<ProviderType>())
                self.view_count <- map["view_count"]
                self.permission <- map["permission"]
                self.downloadable <- map["downloadable"]
                self.is_uploaded <- map["is_uploaded"]
                self.is_upload_failed <- map["is_upload_failed"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                self.Provider = ProviderEntity.init(url: self.url)
                resolve(())
            },
        ]
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "MemberId": self.MemberId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "thumbnail": self.thumbnail,
            "url": self.url,
            "format": self.format.rawValue,
            "duration": self.duration,
            "pixel_width": self.pixel_width,
            "pixel_height": self.pixel_height,
            "size_byte": self.size_byte,
            "is_deleted": self.is_deleted,
            "provider": self.provider.rawValue,
            "view_count": self.view_count,
            "downloadable": self.downloadable,
            "is_uploaded": self.is_uploaded,
            "is_upload_failed": self.is_upload_failed,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
//            "Streams": self.Streams?.toJSON() ?? [],
//            "Folders": self.Folders?.toJSON() ?? [],
//            "StreamChannels": self.StreamChannels?.toJSON() ?? [],
//            "Messages": self.Messages?.toJSON() ?? [],
//            "Member": self.Member?.toNewMemory().toJSON() ?? [],
//            "FileReference": self.FileReference?.toJSON() ?? []
        ]
    }
    
    func setProgress(_ progress: Float) {
        self.progress = progress
        self.delegate?.fileEntity(didChange: progress, in: self)
    }
    
    func toNewMemory() -> FileEntity {
        let new = FileEntity()
        new.id = self.id
        new.MemberId = self.MemberId
        new.uid = self.uid
        new.name = self.name
        new.description = self.description
        new.thumbnail = self.thumbnail
        new.url = self.url
        new.format = self.format
        new.duration = self.duration
        new.pixel_width = self.pixel_width
        new.pixel_height = self.pixel_height
        new.size_byte = self.size_byte
        new.is_deleted = self.is_deleted
        new.provider = self.provider
        new.view_count = self.view_count
        new.downloadable = self.downloadable
        new.is_uploaded = self.is_uploaded
        new.is_upload_failed = self.is_upload_failed
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

class FileEntities: Entities, StreamableEntities, PropertyNames, Mappable {
    var items: [FileEntity] = []
    var uploadings: [FileEntity] {
        self.items.filter({ $0.progress != nil && $0.progress! < 1 })
    }
    var storage_count: Int {
        return items.filter({ $0.format != .other && $0.format != .youtube  }).count
    }
    var available_storage: Bool {
        guard let current_user = CurrentUser.getCurrentUserEntity() else { return false }
        return PlanConfig.free_plan_max_size_byte >= current_user.size_byte + items.reduce(Double(0), { $0 + $1.size_byte })
    }
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
    
    func unified() {
        for item in items.filter({ $0.id == 0 || $0.id == nil }) {
            item.id = items
                .filter({ $0.id != 0 && $0.id != nil })
                .filter { $0.MemberId == $0.MemberId && $0.description == $0.description && $0.name == $0.name }.first?.id
        }
        self.items = items.unique
    }
}

extension FileEntity: Equatable {
    static func == (lhs: FileEntity, rhs: FileEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.uid == rhs.uid && (lhs.description == rhs.description && lhs.name == rhs.name && lhs.MemberId == rhs.MemberId) }
        return lid == rid && lhs.uid == rhs.uid
    }
    
    static func === (lhs: FileEntity, rhs: FileEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.uid == rhs.uid }
        return lid == rid && lhs.uid == rhs.uid
    }
}

extension FileEntities: Equatable {
    static func == (lhs: FileEntities, rhs: FileEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}

extension FileEntity: WebsocketPayloadable {
    typealias Element = FileEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "uid": self.uid,
            "MemberId": self.MemberId ?? 0,
            "name": self.name,
            "description": self.description,
            "thumbnail": self.thumbnail,
            "url": self.url,
            "format": self.format.rawValue,
            "size_byte": self.size_byte,
            "duration": self.duration,
            "pixel_width": self.pixel_width,
            "pixel_height": self.pixel_height,
            "is_deleted": self.is_deleted,
            "provider": self.provider.rawValue,
            "view_count": self.view_count,
            "is_uploaded": self.is_uploaded,
            "is_upload_failed": self.is_upload_failed,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> FileEntity? {
        let entity = FileEntity()
        guard let id = json["id"].number, id != 0, let MemberId = json["MemberId"].number, MemberId != 0 else { return nil }
        entity.id = Int(truncating: id)
        entity.uid = json["uid"].stringValue
        entity.MemberId = Int(truncating: MemberId)
        entity.name = json["name"].stringValue
        entity.description = json["description"].stringValue
        entity.thumbnail = json["thumbnail"].stringValue
        entity.url = json["url"].stringValue
        entity.format = FileFormat.init(rawValue: json["format"].stringValue) ?? .other
        entity.size_byte = json["size_byte"].doubleValue
        entity.duration = TimeInterval.init(millisecond: TimeInterval(json["duration"].doubleValue)) * 1000
        entity.pixel_width = json["pixel_width"].doubleValue
        entity.pixel_height = json["pixel_height"].doubleValue
        entity.is_deleted = json["is_deleted"].boolValue
        entity.provider = ProviderType.init(rawValue: json["provider"].stringValue) ?? .other
        entity.view_count = json["view_count"].intValue
        entity.is_uploaded = json["is_uploaded"].boolValue
        entity.is_upload_failed = json["is_upload_failed"].boolValue
        entity.Provider = ProviderEntity.init(url: entity.url)
        return entity
    }
}


extension FileEntities: WebsocketPayloadable {
    static func jsonable(_ json: JSON) -> FileEntity? {
        return nil
    }
    
    func toWebsocketPayload() -> JSON {
        let json: JSON = [
            "Files": self.compactMap({ $0.toWebsocketPayload })
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> FileEntities? {
        let entities = FileEntities()
        entities.items = json["Files"].array?.compactMap({ FileEntity.jsonable($0) }) ?? []
        return entities
    }
}
