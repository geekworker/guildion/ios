//
//  ChannelogEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/21.
//

import Foundation
import UIKit
import ObjectMapper
import SwiftyJSON
import Hydra

class ChannelogEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var MessageId: Int?
    public var ChannelogableId: Int?
    public var channelogable_type: ChannelogableType = .Stream
    public var thumbnail: String = ""
    public var template: String = ""
    public var action: String = ""
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Message: MessageEntity?
    public var Channelogable: ChannelogableEntity?
    public var Stream: StreamEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.MessageId <- map["MessageId"]
        self.ChannelogableId <- map["ChannelogableId"]
        self.channelogable_type <- (map["channelogable_type"], EnumTransform<ChannelogableType>())
        self.thumbnail <- map["thumbnail"]
        self.template <- map["template"]
        self.action <- map["action"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Message <- map["Message"]
        self.Channelogable = ChannelogableEntity.init(map: map)
        self.Stream = Channelogable?.upcasting() as? StreamEntity
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.MessageId <- map["MessageId"]
        self.ChannelogableId <- map["ChannelogableId"]
        self.channelogable_type <- (map["channelogable_type"], EnumTransform<ChannelogableType>())
        self.thumbnail <- map["thumbnail"]
        self.template <- map["template"]
        self.action <- map["action"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Message <- map["Message"]
        self.Channelogable = ChannelogableEntity.init(map: map)
        self.Stream = Channelogable?.upcasting() as? StreamEntity
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Message == nil { self.Message = MessageEntity() }
        if self.Stream == nil { self.Stream = StreamEntity() }
        self.Channelogable = ChannelogableEntity.init(map: map)
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MessageId <- map["MessageId"]
                self.ChannelogableId <- map["ChannelogableId"]
                self.channelogable_type <- (map["channelogable_type"], EnumTransform<ChannelogableType>())
                self.thumbnail <- map["thumbnail"]
                self.template <- map["template"]
                self.action <- map["action"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Message!.asyncMapping(map: map["Message"])
        
        if let unwrapped = self.Channelogable?.channelogable_type {
            switch unwrapped {
            case .Stream: promises += self.Stream!.asyncMapping(map: map["Channelogable"])
            }
        }
        
        return promises
    }
    
    func asyncMappingFast(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Stream == nil { self.Stream = StreamEntity() }
        self.Channelogable = ChannelogableEntity.init(map: map)
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MessageId <- map["MessageId"]
                self.ChannelogableId <- map["ChannelogableId"]
                self.channelogable_type <- (map["channelogable_type"], EnumTransform<ChannelogableType>())
                self.thumbnail <- map["thumbnail"]
                self.template <- map["template"]
                self.action <- map["action"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        
        if let unwrapped = self.Channelogable?.channelogable_type {
            switch unwrapped {
            case .Stream: promises += self.Stream!.asyncMappingFast(map: map["Channelogable"])
            }
        }
        
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "MessageId": self.MessageId ?? 0,
            "ChannelogableId": self.ChannelogableId ?? 0,
            "channelogable_type": self.channelogable_type.rawValue,
            "thumbnail": self.thumbnail,
            "template": self.template,
            "action": self.action,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
             "Message": self.Message?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> ChannelogEntity {
        let new = ChannelogEntity()
        new.id = self.id
        new.MessageId = self.MessageId
        new.ChannelogableId = self.ChannelogableId
        new.channelogable_type = self.channelogable_type
        new.thumbnail = self.thumbnail
        new.template = self.template
        new.action = self.action
        new.permission = self.permission
        new.is_private = self.is_private
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

class ChannelogEntities: Entities, PropertyNames, Mappable {
    var items: [ChannelogEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return self.items.compactMap { $0.toJSON() }
    }
}

extension ChannelogEntity: Equatable {
    static func == (lhs: ChannelogEntity, rhs: ChannelogEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension ChannelogEntities: Equatable {
    static func == (lhs: ChannelogEntities, rhs: ChannelogEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
