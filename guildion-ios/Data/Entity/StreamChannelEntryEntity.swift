//
//  StreamChannelEntryEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

class StreamChannelEntryEntity: Entity, PropertyNames, Mappable, ChannelEntryableProtocol {
    public var id: Int?
    public var MemberId: Int?
    public var ChannelId: Int?
    public var mute: Bool = true
    public var messages_mute: Bool = true
    public var mentions_mute: Bool = true
    public var everyone_mentions_mute: Bool = true
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Member: MemberEntity?
    public var Channel: StreamChannelEntity?
    
    public var channelable_type: ChannelableType = .StreamChannel
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.ChannelId <- map["ChannelId"]
        self.mute <- map["mute"]
        self.messages_mute <- map["messages_mute"]
        self.mentions_mute <- map["mentions_mute"]
        self.everyone_mentions_mute <- map["everyone_mentions_mute"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Channel <- map["Channel"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.ChannelId <- map["ChannelId"]
        self.mute <- map["mute"]
        self.messages_mute <- map["messages_mute"]
        self.mentions_mute <- map["mentions_mute"]
        self.everyone_mentions_mute <- map["everyone_mentions_mute"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Channel <- map["Channel"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Member == nil { self.Member = MemberEntity() }
        if self.Channel == nil { self.Channel = StreamChannelEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.ChannelId <- map["ChannelId"]
                self.mute <- map["mute"]
                self.messages_mute <- map["messages_mute"]
                self.mentions_mute <- map["mentions_mute"]
                self.everyone_mentions_mute <- map["everyone_mentions_mute"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ] + self.Member!.asyncMapping(map: map["Member"]) + self.Channel!.asyncMapping(map: map["Channel"])
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.ChannelId <- map["ChannelId"]
                self.mute <- map["mute"]
                self.messages_mute <- map["messages_mute"]
                self.mentions_mute <- map["mentions_mute"]
                self.everyone_mentions_mute <- map["everyone_mentions_mute"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "MemberId": self.MemberId ?? 0,
            "ChannelId": self.ChannelId ?? 0,
            "mute": self.mute,
            "messages_mute": self.messages_mute,
            "mentions_mute": self.mentions_mute,
            "everyone_mentions_mute": self.everyone_mentions_mute,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Member": self.Member?.toJSON() ?? [],
            "Channel": self.Channel?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> ChannelEntryableProtocol {
        let new = StreamChannelEntryEntity()
        new.id = self.id
        new.ChannelId = self.ChannelId
        new.MemberId = self.MemberId
        new.mute = self.mute
        new.messages_mute = self.messages_mute
        new.mentions_mute = self.mentions_mute
        new.everyone_mentions_mute = self.everyone_mentions_mute
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

class StreamChannelEntryEntities: Entities, PropertyNames, Mappable {
    var items: [StreamChannelEntryEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}
