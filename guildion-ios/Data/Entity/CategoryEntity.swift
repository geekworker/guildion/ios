//
//  CategoryEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//


import UIKit
import Foundation
import ObjectMapper
import Hydra

class CategoryEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var uid: String = ""
    public var ja_name: String = ""
    public var ja_groupname: String = ""
    public var en_name: String = ""
    public var en_groupname: String = ""
    public var guild_count: Int = 0
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Guilds: GuildEntities?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.ja_name <- map["ja_name"]
        self.ja_groupname <- map["ja_groupname"]
        self.en_name <- map["en_name"]
        self.en_groupname <- map["en_groupname"]
        self.guild_count <- map["guild_count"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guilds = GuildEntities(map: map["Guilds"])
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.ja_name <- map["ja_name"]
        self.ja_groupname <- map["ja_groupname"]
        self.en_name <- map["en_name"]
        self.en_groupname <- map["en_groupname"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guilds = GuildEntities(map: map["Guilds"])
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guilds == nil { self.Guilds = GuildEntities() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.uid <- map["uid"]
                self.ja_name <- map["ja_name"]
                self.ja_groupname <- map["ja_groupname"]
                self.en_name <- map["en_name"]
                self.en_groupname <- map["en_groupname"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ] + self.Guilds!.asyncMapping(map: map["Guilds"])
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.uid <- map["uid"]
                self.ja_name <- map["ja_name"]
                self.ja_groupname <- map["ja_groupname"]
                self.en_name <- map["en_name"]
                self.en_groupname <- map["en_groupname"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "uid": self.uid,
            "ja_name": self.ja_name,
            "ja_groupname": self.ja_groupname,
            "en_name": self.en_name,
            "en_groupname": self.en_groupname,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Guilds": self.Guilds?.toJSON() ?? [],
        ]
    }
    
    func getLocalableName(locale: LocaleConfig.Locales) -> String {
        switch locale {
        case .ja: return self.ja_name
        case .en: return self.en_name
        default: return ""
        }
    }
}

class CategoryEntities: Entities, PropertyNames, Mappable {
    var items: [CategoryEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
}

extension CategoryEntity: Equatable {
    static func == (lhs: CategoryEntity, rhs: CategoryEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension CategoryEntities: Equatable {
    static func == (lhs: CategoryEntities, rhs: CategoryEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
