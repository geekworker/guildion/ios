//
//  SectionChannelRowable.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit

enum SectionChannelRowableType: String {
    case TextChannel = "TextChannel"
    case StreamChannel = "StreamChannel"
    case DMChannel = "DMChannel"
    case Stream = "Stream"
    case Participant = "Participant"
}

protocol SectionChannelRowable: class {
    var index: Int { get set }
    var sectionChannelRowType: SectionChannelRowableType { get }
}

extension SectionChannelRowable {}
