//
//  FileFormat.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/21.
//

import Foundation
import UIKit

enum FileFormat: String {
    case mp4 // = ContentType.mp4.file_extension
    case mov // = ContentType.mov.file_extension
    
    case mp3 // = ContentType.mp3.file_extension
    
    case jpg // = ContentType.jpg.file_extension
    case jpeg // = ContentType.jpeg.file_extension
    case png // = ContentType.png.file_extension
    
    case youtube
    case other = ""
    
    var content_type: ContentType {
        return ContentType.init(file_extension: self.rawValue)
    }
    
    init?(extention: String) {
        switch extention.replacingOccurrences(of: ".", with: "") {
        case "mp4": self = .mp4
        case "MP4": self = .mp4
        case "mov": self = .mov
        case "MOV": self = .mov
        case "mp3": self = .mp3
        case "MP3": self = .mp3
        case "jpg": self = .jpg
        case "JPG": self = .jpg
        case "jpeg": self = .jpeg
        case "JPEG": self = .jpeg
        case "png": self = .png
        case "PNG": self = .png
        default: self = .other
        }
    }
    
    var iconImage: UIImage {
        if self.content_type.is_movie {
            return R.image.videoPlay()!.withRenderingMode(.alwaysTemplate)
        } else if self.content_type.is_image {
            return R.image.insertPicture()!.withRenderingMode(.alwaysTemplate)
        } else if self.content_type.is_sound {
            return R.image.speaker()!.withRenderingMode(.alwaysTemplate)
        } else if self == .youtube {
            return R.image.yt_icon_rgb()!.withRenderingMode(.alwaysOriginal)
        } else {
            return UIImage()
        }
    }
    
    var uploadable: Bool {
        switch self {
        case .youtube, .other: return false
        default: return true
        }
    }
}

