//
//  ReactionEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Hydra

class ReactionEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var MemberId: Int?
    public var MessageId: Int?
    public var data: String = ""
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Member: MemberEntity?
    public var Message: MessageEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.MessageId <- map["MessageId"]
        self.data <- map["data"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Message <- map["Message"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.MessageId <- map["MessageId"]
        self.data <- map["data"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Message <- map["Message"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Member == nil { self.Member = MemberEntity() }
        if self.Message == nil { self.Message = MessageEntity() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.MessageId <- map["MessageId"]
                self.data <- map["data"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Member!.asyncMapping(map: map["Member"])
        promises += self.Message!.asyncMapping(map: map["Message"])
        return promises
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        
        let promises = [
            Promise<Void> (in: .background) { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.MessageId <- map["MessageId"]
                self.data <- map["data"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "MemberId": self.MemberId ?? 0,
            "MessageId": self.MessageId ?? 0,
            "data": self.data,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Member": self.Member?.toJSON() ?? [],
            "Message": self.Message?.toJSON() ?? [],
        ]
    }
}

extension ReactionEntity: WebsocketPayloadable {
    typealias Element = ReactionEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "MemberId": self.MemberId ?? 0,
            "MessageId": self.MessageId ?? 0,
            "data": self.data,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> ReactionEntity? {
        let reaction = ReactionEntity()
        guard let id = json["id"].number, id != 0, let MemberId = json["MemberId"].number, MemberId != 0, let MessageId = json["MessageId"].number, MessageId != 0 else { return nil }
        reaction.id = Int(truncating: id)
        reaction.MemberId = Int(truncating: MemberId)
        reaction.MessageId = Int(truncating: MessageId)
        reaction.data = json["data"].stringValue
        return reaction
    }
}

class ReactionEntities: Entities, PropertyNames, Mappable {
    var items: [ReactionEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
    
    func toNestedArray() -> [ReactionEntities] {
        var dic: [String: ReactionEntities] = [:]
        _ = self.items.map({
            if dic[$0.data] == nil {
                let entities = ReactionEntities()
                entities.items.append($0)
                dic[$0.data] = entities
            } else {
                dic[$0.data]?.items.append($0)
            }
        })
        var results: [ReactionEntities] = []
        for (_, value) in dic {
            results.append(value)
        }
        return results.sorted(by: { l, r -> Bool in
            return l.items.count > r.items.count
        })
    }
}

extension ReactionEntity: Equatable {
    static func == (lhs: ReactionEntity, rhs: ReactionEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.data == rhs.data && lhs.MessageId == rhs.MessageId && lhs.MemberId == rhs.MemberId
        }
        return lid == rid
    }
}

extension ReactionEntities: Equatable {
    static func == (lhs: ReactionEntities, rhs: ReactionEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
