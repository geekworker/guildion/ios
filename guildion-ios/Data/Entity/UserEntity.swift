//
//  UserEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Hydra

class UserEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var username: String = ""
    public var uid: String = ""
    public var nickname: String = ""
    public var description: String = ""
    public var picture_small: String = ""
    public var picture_large: String = ""
    public var locale: String = ""
    public var timezone: String = ""
    public var size_byte: Double = 0
    public var verified: Bool = false
    public var admin: Bool = false
    public var active: Bool = false
    public var actived_at: Date = Date.CurrentDate()
    public var is_private: Bool = false
    public var permission: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Identity: IdentityEntity?
    public var Members: MemberEntities?
    public var Guilds: GuildEntities?
    public var Blocks: UserEntities?
    public var Mutes: UserEntities?
    public var Blockers: UserEntities?
    public var Muters: UserEntities?
    public var GuildBlocks: GuildEntities?
    public var Subscriptions: SubscriptionEntities?
    public var CurrentSubscription: SubscriptionEntity? {
        return Subscriptions?.sorted(by: { (a, b) -> Bool in return a.started_at > b.started_at }).first
    }
    
    public lazy var byteCountFormatter: ByteCountFormatter = {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = .useAll
        formatter.countStyle = .file
        formatter.includesUnit = true
        formatter.isAdaptive = true
        return formatter
    }()
    public var size: String {
        return self.size_byte == 0 ? "0 byte" : byteCountFormatter.string(fromByteCount: Int64(self.size_byte))
    }

    required init?(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.username <- map["username"]
        self.nickname <- map["nickname"]
        self.description <- map["description"]
        self.picture_small <- map["picture_small"]
        self.picture_large <- map["picture_large"]
        self.locale <- map["locale"]
        self.timezone <- map["timezone"]
        self.size_byte <- map["size_byte"]
        self.verified <- map["verified"]
        self.admin <- map["admin"]
        self.active <- map["active"]
        self.actived_at <- map["actived_at"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Identity <- map["Identity"]
        self.Members = MemberEntities(map: map["Members"])
        self.Guilds = GuildEntities(map: map["Guilds"])
        self.Blocks = UserEntities(map: map["Blocks"])
        self.Mutes = UserEntities(map: map["Mutes"])
        self.Blockers = UserEntities(map: map["Blockers"])
        self.Muters = UserEntities(map: map["Muters"])
        self.GuildBlocks = GuildEntities(map: map["GuildBlockers"])
        self.Subscriptions = SubscriptionEntities(map: map["Subscriptions"])
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.username <- map["username"]
        self.nickname <- map["nickname"]
        self.description <- map["description"]
        self.picture_small <- map["picture_small"]
        self.picture_large <- map["picture_large"]
        self.locale <- map["locale"]
        self.timezone <- map["timezone"]
        self.size_byte <- map["size_byte"]
        self.verified <- map["verified"]
        self.admin <- map["admin"]
        self.active <- map["active"]
        self.actived_at <- map["actived_at"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Identity <- map["Identity"]
        self.Members = MemberEntities(map: map["Members"])
        self.Guilds = GuildEntities(map: map["Guilds"])
        self.Blocks = UserEntities(map: map["Blocks"])
        self.Mutes = UserEntities(map: map["Mutes"])
        self.Blockers = UserEntities(map: map["Blockers"])
        self.Muters = UserEntities(map: map["Muters"])
        self.GuildBlocks = GuildEntities(map: map["GuildBlockers"])
        self.Subscriptions = SubscriptionEntities(map: map["Subscriptions"])
    }

    init() {
    }

    init(
        id: Int,
        username: String,
        uid: String,
        nickname: String,
        description: String,
        picture_small: String,
        picture_large: String,
        locale: String,
        timezone: String,
        verified: Bool,
        admin: Bool,
        size_byte: Double,
        is_private: Bool,
        permission: Bool
    ) {
        self.id = id
        self.username = username
        self.uid = uid
        self.nickname = nickname
        self.description = description
        self.picture_small = picture_small
        self.picture_large = picture_large
        self.locale = locale
        self.timezone = timezone
        self.verified = verified
        self.admin = admin
        self.size_byte = size_byte
        self.is_private = is_private
        self.permission = permission
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Identity == nil { self.Identity = IdentityEntity() }
        if self.Members == nil { self.Members = MemberEntities() }
        if self.Guilds == nil { self.Guilds = GuildEntities() }
        if self.Blocks == nil { self.Blocks = UserEntities() }
        if self.Mutes == nil { self.Mutes = UserEntities() }
        if self.Blockers == nil { self.Blockers = UserEntities() }
        if self.Muters == nil { self.Muters = UserEntities() }
        if self.GuildBlocks == nil { self.GuildBlocks = GuildEntities() }
        if self.Subscriptions == nil { self.Subscriptions = SubscriptionEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.uid <- map["uid"]
                self.username <- map["username"]
                self.nickname <- map["nickname"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.locale <- map["locale"]
                self.timezone <- map["timezone"]
                self.size_byte <- map["size_byte"]
                self.verified <- map["verified"]
                self.admin <- map["admin"]
                self.active <- map["active"]
                self.actived_at <- map["actived_at"]
                self.is_private <- map["is_private"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Identity!.asyncMapping(map: map["Identity"])
        promises += self.Members!.asyncMapping(map: map["Members"])
        promises += self.Guilds!.asyncMapping(map: map["Guilds"])
        promises += self.Blocks!.asyncMapping(map: map["Blocks"])
        promises += self.Mutes!.asyncMapping(map: map["Mutes"])
        promises += self.Blockers!.asyncMapping(map: map["Blockers"])
        promises += self.Muters!.asyncMapping(map: map["Muters"])
        promises += self.GuildBlocks!.asyncMapping(map: map["GuildBlockers"])
        promises += self.Subscriptions!.asyncMapping(map: map["Subscriptions"])
        return promises
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        let promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.uid <- map["uid"]
                self.username <- map["username"]
                self.nickname <- map["nickname"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.locale <- map["locale"]
                self.timezone <- map["timezone"]
                self.size_byte <- map["size_byte"]
                self.verified <- map["verified"]
                self.admin <- map["admin"]
                self.active <- map["active"]
                self.actived_at <- map["actived_at"]
                self.is_private <- map["is_private"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }

    func toJSON() -> [String: Any] {
        return [
            "id": self.id ?? 0,
            "username": self.username,
            "uid": self.uid,
            "nickname": self.nickname,
            "description": self.description,
            "picture_small": self.picture_small,
            "picture_large": self.picture_large,
            "locale": self.locale,
            "timezone": self.timezone,
            "size_byte": self.size_byte,
            "verified": self.verified,
            "admin": self.admin,
            "active": self.active,
            "actived_at": self.actived_at,
            "is_private": self.is_private,
            "permission": self.permission,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Identity": self.Identity?.toJSON() ?? [],
            "Members": self.Members?.toJSON() ?? [],
            "Guilds": self.Guilds?.toJSON() ?? [],
            "Subscriptions": self.Subscriptions?.toJSON() ?? [],
        ]
    }
    
    func pureUser() -> UserEntity {
        let u = UserEntity()
        u.id = self.id
        u.username = self.username
        u.uid = self.uid
        u.nickname = self.nickname
        u.description = self.description
        u.picture_small = self.picture_small
        u.picture_large = self.picture_large
        u.locale = self.locale
        u.timezone = self.timezone
        u.verified = self.verified
        u.admin = self.admin
        u.size_byte = self.size_byte
        u.active = self.active
        u.actived_at = self.actived_at
        u.is_private = self.is_private
        u.permission = self.permission
        u.created_at = self.created_at
        u.updated_at = self.updated_at
        u.Members = self.Members
        return u
    }
}

class UserEntities: Entities, PropertyNames, Mappable {
    var items: [UserEntity] = []

    init() {
    }

    required init?(map: Map) {
        self.items <- map
    }

    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }

    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension UserEntity: WebsocketPayloadable {
    typealias Element = UserEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "username": self.username,
            "nickname": self.nickname,
            "description": self.description,
            "picture_small": self.picture_small,
            "picture_large": self.picture_large,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> UserEntity? {
        let user = UserEntity()
        guard let id = json["id"].number, id != 0 else { return nil }
        user.id = Int(truncating: id)
        user.username = json["username"].stringValue
        user.nickname = json["nickname"].stringValue
        user.description = json["description"].stringValue
        user.picture_small = json["picture_small"].stringValue
        user.picture_large = json["picture_large"].stringValue
        return user
    }
}

extension UserEntity: Equatable {
    static func == (lhs: UserEntity, rhs: UserEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.username == rhs.username && lhs.username != "" && rhs.username != "" }
        return lid == rid
    }
}

extension UserEntities: Equatable {
    static func == (lhs: UserEntities, rhs: UserEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
