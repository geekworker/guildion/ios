//
//  AccessTokenEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra

class AccessTokenEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var IdentityId: Int?
    public var token: String = ""
    public var expired_at: Date = Date.CurrentDate()
    public var is_one_time: Bool = false
    public var is_private: Bool = false
    public var permission: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Identity: IdentityEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.IdentityId <- map["IdentityId"]
        self.token <- map["token"]
        self.expired_at <- map["expired_at"]
        self.is_one_time <- map["is_one_time"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Identity <- map["Identity"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.IdentityId <- map["IdentityId"]
        self.token <- map["token"]
        self.expired_at <- map["expired_at"]
        self.is_one_time <- map["is_one_time"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Identity <- map["Identity"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Identity == nil { self.Identity = IdentityEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.IdentityId <- map["IdentityId"]
                self.token <- map["token"]
                self.expired_at <- map["expired_at"]
                self.is_one_time <- map["is_one_time"]
                self.is_private <- map["is_private"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ] + self.Identity!.asyncMapping(map: map["Identity"])
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "IdentityId": self.IdentityId ?? 0,
            "token": self.token,
            "expired_at": self.expired_at,
            "is_one_time": self.is_one_time,
            "is_private": self.is_private,
            "permission": self.permission,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Identity": self.Identity?.toJSON() ?? [],
        ]
    }
}

class AccessTokenEntities: Entities, PropertyNames, Mappable {
    var items: [AccessTokenEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}
