//
//  MessageEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Hydra

class MessageEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var SenderId: Int?
    public var MessageableId: Int?
    public var messageable_type: MessageableType = .TextChannel
    public var uid: String = UUID().uuidString
    public var text: String = ""
    public var permission: Bool = false
    public var is_static: Bool = false
    public var is_log: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Sender: MemberEntity?
    public var Files: FileEntities?
    public var Reads: ReadEntities?
    public var Reactions: ReactionEntities? = ReactionEntities()
    public var Messageable: MessageableEntity?
    public var TextChannel: TextChannelEntity?
    public var DMChannel: DMChannelEntity?
    public var StreamChannel: StreamChannelEntity?
    public var Channelog: ChannelogEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.SenderId <- map["SenderId"]
        self.MessageableId <- map["MessageableId"]
        self.messageable_type <- (map["messageable_type"], EnumTransform<MessageableType>())
        self.uid <- map["uid"]
        self.text <- map["text"]
        self.is_log <- map["is_log"]
        self.is_static <- map["is_static"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Sender <- map["Sender"]
        self.Files = FileEntities(map: map["Files"])
        self.Reads = ReadEntities(map: map["Reads"])
        self.Reactions = ReactionEntities(map: map["Reactions"])
        self.Messageable = MessageableEntity.init(map: map)
        self.TextChannel = Messageable?.upcasting() as? TextChannelEntity
        self.DMChannel = Messageable?.upcasting() as? DMChannelEntity
        self.StreamChannel = Messageable?.upcasting() as? StreamChannelEntity
        self.Channelog <- map["Channelog"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.SenderId <- map["SenderId"]
        self.MessageableId <- map["MessageableId"]
        self.messageable_type <- (map["messageable_type"], EnumTransform<MessageableType>())
        self.uid <- map["uid"]
        self.text <- map["text"]
        self.is_log <- map["is_log"]
        self.is_static <- map["is_static"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Sender <- map["Sender"]
        self.Files = FileEntities(map: map["Files"])
        self.Reads = ReadEntities(map: map["Reads"])
        self.Reactions = ReactionEntities(map: map["Reactions"])
        self.Messageable = MessageableEntity.init(map: map)
        self.TextChannel = Messageable?.upcasting() as? TextChannelEntity
        self.DMChannel = Messageable?.upcasting() as? DMChannelEntity
        self.StreamChannel = Messageable?.upcasting() as? StreamChannelEntity
        self.Channelog <- map["Channelog"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Sender == nil { self.Sender = MemberEntity() }
        if self.Files == nil { self.Files = FileEntities() }
        if self.Reads == nil { self.Reads = ReadEntities() }
        if self.Reactions == nil { self.Reactions = ReactionEntities() }
        if self.TextChannel == nil { self.TextChannel = TextChannelEntity() }
        if self.DMChannel == nil { self.DMChannel = DMChannelEntity() }
        if self.StreamChannel == nil { self.StreamChannel = StreamChannelEntity() }
        if self.Channelog == nil { self.Channelog = ChannelogEntity() }
        self.Messageable = MessageableEntity.init(map: map)
        
        var promises = [
            Promise<Void> { [weak self] resolve, _, _ in
                if let strongSelf = self {
                    strongSelf.id <- map["id"]
                }
                if let strongSelf = self {
                    strongSelf.SenderId <- map["SenderId"]
                }
                if let strongSelf = self {
                    strongSelf.MessageableId <- map["MessageableId"]
                }
                if let strongSelf = self {
                    strongSelf.messageable_type <- (map["messageable_type"], EnumTransform<MessageableType>())
                }
                if let strongSelf = self {
                    strongSelf.uid <- map["uid"]
                }
                if let strongSelf = self {
                    strongSelf.text <- map["text"]
                }
                if let strongSelf = self {
                    strongSelf.is_log <- map["is_log"]
                }
                if let strongSelf = self {
                    strongSelf.is_static <- map["is_static"]
                }
                if let strongSelf = self {
                    strongSelf.permission <- map["permission"]
                }
                if let strongSelf = self {
                    strongSelf.is_private <- map["is_private"]
                }
                if let strongSelf = self, let dateString = map["created_at"].currentValue as? String {
                    strongSelf.created_at = Date.UTCToLocal(date: dateString)
                }
                if let strongSelf = self, let dateString = map["updated_at"].currentValue as? String {
                    strongSelf.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Sender!.asyncMapping(map: map["Sender"])
        promises += self.Channelog!.asyncMapping(map: map["Channelog"])
        promises += self.Files!.asyncMapping(map: map["Files"])
        promises += self.Reads!.asyncMapping(map: map["Reads"])
        promises += self.Reactions!.asyncMapping(map: map["Reactions"])
        
        if let unwrapped = self.Messageable?.messageable_type {
            switch unwrapped {
            case .TextChannel: promises += self.TextChannel!.asyncMapping(map: map["Messageable"])
            case .DMChannel: promises += self.DMChannel!.asyncMapping(map: map["Messageable"])
            case .StreamChannel: promises += self.StreamChannel!.asyncMapping(map: map["Messageable"])
            }
        }
        return promises
    }
    
    func asyncMappingFast(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Sender == nil { self.Sender = MemberEntity() }
        if self.Files == nil { self.Files = FileEntities() }
        if self.Reactions == nil { self.Reactions = ReactionEntities() }
        if self.Channelog == nil { self.Channelog = ChannelogEntity() }
        self.Messageable = MessageableEntity.init(map: map)
        
        var promises = [
            Promise<Void> (in: .main) { [weak self] resolve, _, _ in
                if let strongSelf = self {
                    strongSelf.id <- map["id"]
                }
                if let strongSelf = self {
                    strongSelf.SenderId <- map["SenderId"]
                }
                if let strongSelf = self {
                    strongSelf.MessageableId <- map["MessageableId"]
                }
                if let strongSelf = self {
                    strongSelf.messageable_type <- (map["messageable_type"], EnumTransform<MessageableType>())
                }
                if let strongSelf = self {
                    strongSelf.uid <- map["uid"]
                }
                if let strongSelf = self {
                    strongSelf.text <- map["text"]
                }
                if let strongSelf = self {
                    strongSelf.is_log <- map["is_log"]
                }
                if let strongSelf = self {
                    strongSelf.is_static <- map["is_static"]
                }
                if let strongSelf = self {
                    strongSelf.permission <- map["permission"]
                }
                if let strongSelf = self {
                    strongSelf.is_private <- map["is_private"]
                }
                if let strongSelf = self, let dateString = map["created_at"].currentValue as? String {
                    strongSelf.created_at = Date.UTCToLocal(date: dateString)
                }
                if let strongSelf = self, let dateString = map["updated_at"].currentValue as? String {
                    strongSelf.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Sender!.asyncPureMapping(map: map["Sender"])
        promises += self.Channelog!.asyncMappingFast(map: map["Channelog"])
        promises += self.Files!.asyncPureMapping(map: map["Files"])
        promises += self.Reactions!.asyncPureMapping(map: map["Reactions"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "SenderId": self.SenderId ?? 0,
            "MessageableId": self.MessageableId ?? 0,
            "messageable_type": self.messageable_type.rawValue,
            "uid": self.uid,
            "text": self.text,
            "is_log": self.is_log,
            "is_static": self.is_static,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Sender": self.Sender?.toJSON() ?? [],
            "Files": self.Files?.toJSON() ?? [],
            "Reads": self.Reads?.toJSON() ?? [],
            // "Reactions": self.Reactions?.toJSON() ?? [],
            "TextChannel": self.TextChannel?.toJSON() ?? [],
            "DMChannel": self.DMChannel?.toJSON() ?? [],
            "StreamChannel": self.StreamChannel?.toJSON() ?? [],
        ]
    }
    
    func isRead(_ current_member: MemberEntity) -> Bool {
        return self.Reads?.items.filter { $0.MemberId == current_member.id ?? 0 } .count ?? 0 > 0
    }
    
    func fetchOGP() {
    }
}

class MessageEntities: Entities, PropertyNames, Mappable {
    var items: [MessageEntity] = []
    var imageViewers: [ImageViewerModel] {
        let nested: [[ImageViewerModel]] = self.filter({ $0.Files?.count ?? 0 > 0 }).compactMap({ message in message.Files?.compactMap({ FileEntity_ImageViewerModelTranslator.translate($0) }) ?? [] })
        return nested.reduce([]) { $0 + $1 }
    }
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncMappingFast(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMappingFast(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return self.items.compactMap { $0.toJSON() }
    }
    
    func unified() {
        for item in items.filter({ $0.id == 0 || $0.id == nil }) {
            item.id = items
                .filter({ $0.id != 0 && $0.id != nil })
                .filter { $0.text == item.text && $0.Files?.count == $0.Files?.count && $0.SenderId == $0.SenderId }.first?.id
            item.uid = items
                .filter({ $0.id != 0 && $0.id != nil })
                .filter { $0.text == item.text && $0.Files?.count == $0.Files?.count && $0.SenderId == $0.SenderId }.first?.uid ?? ""
        }
        self.items = items.unique
    }
    
    func getUnReadCount() -> Promise<Int> {
        return Promise<Int> (in: .background) { resolve, reject, _ in
            let unread_count = self.filter { $0.Reads?.filter({ !$0.complete }).count ?? 0 > 0 } .count 
            resolve(unread_count)
        }
    }
}

extension MessageEntity: WebsocketPayloadable {
    typealias Element = MessageEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "uid": self.uid,
            "SenderId": self.SenderId ?? 0,
            "Sender": (self.Sender ?? MemberEntity()).toWebsocketPayload(),
            "Files": self.Files?.compactMap({ $0.toWebsocketPayload() }) ?? [],
            "MessageableId": self.MessageableId ?? 0,
            "messageable_type": self.messageable_type.rawValue,
            "text": self.text,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> MessageEntity? {
        let entity = MessageEntity()
        guard let id = json["id"].number, id != 0, let SenderId = json["SenderId"].number, SenderId != 0, let MessageableId = json["MessageableId"].number, MessageableId != 0 else { return nil }
        entity.id = Int(truncating: id)
        entity.uid = json["uid"].stringValue
        entity.SenderId = Int(truncating: SenderId)
        entity.Sender = MemberEntity.jsonable(json["Sender"])
        entity.Files = FileEntities.jsonable(json)
        entity.MessageableId = Int(truncating: MessageableId)
        entity.messageable_type =  MessageableType.init(rawValue: json["messageable_type"].stringValue) ?? .TextChannel
        entity.text = json["text"].stringValue
        return entity
    }
}

extension MessageEntity: Equatable {
    static func == (lhs: MessageEntity, rhs: MessageEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.uid == rhs.uid || (lhs.text == rhs.text && lhs.Files == rhs.Files && lhs.SenderId
            == rhs.SenderId)
        }
        return lid == rid
    }
}

extension MessageEntities: Equatable {
    static func == (lhs: MessageEntities, rhs: MessageEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}

public extension String {
    func extractURLExcludeYouTube() -> [URL] {
        return self.extractURL().filter({ ProviderEntity(url: $0.absoluteString).youtubeID == nil })
    }
}

extension MessageEntities: ImageViewerDisplacedViewsDataSource {
    func provideDisplacementItem(atIndex index: Int) -> DisplaceableView? {
        return nil
    }
}

extension MessageEntities: ImageViewerItemsDataSource {
    func indexOfImageView(repository: FileEntity) -> Int {
        var i = 0
        for message in self.items {
            for enumerater in (message.Files?.items ?? []).filter({ $0.format.content_type.is_image && URL(string: $0.url) != nil }).enumerated() {
                if enumerater.element == repository {
                    return i
                } else {
                    i += 1
                }
            }
        }
        return 0
    }
    
    func itemCount() -> Int {
        return self.imageViewers.count
    }
    
    func provideImageViewerItem(_ index: Int) -> ImageViewerModel {
        return self.imageViewers[safe: index] ?? self.imageViewers.last!
    }
}

extension MessageEntities: ImageViewerItemsDelegate {
    func removeImageViewerItem(at index: Int) {
    }
}
