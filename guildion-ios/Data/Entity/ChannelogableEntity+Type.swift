//
//  ChannelogableEntity+Type.swift
//  guildion-ios
//
//  Created by Apple on 2020/12/23.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

enum ChannelogableType: String {
    case Stream = "Stream"
}

class ChannelogableEntity: Mappable {
    var channelogable_type: ChannelogableType?
    var channelogable_id: Int?
    var channelogable_map: Map?

    init() {}
    
    required init?(map: Map) {
        channelogable_type <- (map["channelogable_type"], EnumTransform<ChannelogableType>())
        channelogable_map = map
    }

    func mapping(map: Map) {
        channelogable_type <- (map["channelogable_type"], EnumTransform<ChannelogableType>())
        channelogable_map = map
    }

    static func objectForMapping(map: Map) -> ChannelogableEntity? {
        let typeString: String? = map["channelogable_type"].value()
        if let typeString = typeString {
            let channelogable_type: ChannelogableType? = ChannelogableType(rawValue: typeString)
            if let channelogable_type = channelogable_type {
                switch(channelogable_type) {
                case .Stream:
                    var entity = StreamEntity()
                    entity <- map["Channelogable"]
                    return entity
                }
            }
        }
        return nil
    }
    
    func upcasting() -> ChannelogableEntity? {
        guard let unwrapped_type = channelogable_type, let unwrapped_map = channelogable_map else { return nil }
        switch(unwrapped_type) {
        case .Stream:
            var entity = StreamEntity()
            entity <- unwrapped_map["Channelogable"]
            return entity
        }
    }
}

class ChannelogableEntities: PropertyNames, Mappable {
    var items: [ChannelogableEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension ChannelogableEntity: Equatable {
    static func == (lhs: ChannelogableEntity, rhs: ChannelogableEntity) -> Bool {
        guard let lid = lhs.channelogable_id, let rid = rhs.channelogable_id else { return false }
        return lid == rid && lhs.channelogable_type == rhs.channelogable_type
    }
}
