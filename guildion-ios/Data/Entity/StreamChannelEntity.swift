//
//  StreamChannelEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra
import SwiftyJSON

class StreamChannelEntity: MessageableEntity, Entity, ChannelableProtocol, SectionChannelRowable {
    public var id: Int?
    public var GuildId: Int?
    public var SectionId: Int?
    public var uid: String = ""
    public var name: String = ""
    public var description: String = ""
    public var index: Int = 0 {
        didSet {
            if let unwrapped = _index {
                index_updated = index != unwrapped
            }
            _index = index
        }
    }
    public var max_participant_count: Int = 0
    public var message_count: Int = 0
    public var unread_count: Int = 0
    public var member_count: Int = 0
    public var temporary: Bool = false
    public var is_nsfw: Bool = false
    public var is_default: Bool = false
    public var is_static: Bool = false
    public var is_im: Bool = false
    public var is_dm: Bool = false
    public var is_system: Bool = false
    public var is_loop: Bool = false
    public var permission: Bool = false
    public var is_private: Bool = false
    public var read_only: Bool = false
    public var voice_only: Bool = false
    public var movie_only: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Guild: GuildEntity?
    public var Section: SectionChannelEntity?
    public var Messages: MessageEntities?
    public var Members: MemberEntities?
    public var Streams: StreamEntities?
    public var Entries: StreamChannelEntryEntities?
    public var Roles: StreamChannelRoleEntities?
    public var LiveStream: StreamEntity? {
        get {
            guard Streams?.count != 0 else { return nil }
            return Streams?.items.filter({ $0.is_live }).max(by: { $1.id ?? 0 > $0.id ?? 0 })
        }
        set {
            if let newStream = newValue {
                guard Streams?.count != 0 else { return }
                for stream in Streams!.items.enumerated() {
                    if stream.element == newStream {
                        Streams?.items[stream.offset] = newStream
                    }
                }
            } else {
                guard Streams?.count != 0 else { return }
                for stream in Streams!.items.enumerated() {
                    if stream.element == LiveStream {
                        Streams?.items.remove(at: stream.offset)
                    }
                }
            }
        }
    }
    
    private var _index: Int? = nil
    public var index_updated: Bool = false
    
    var channelable_type: ChannelableType {
        if self.voice_only { return .VoiceChannel }
        if self.movie_only { return .MusicChannel }
        if self.is_im { return .PlayChannel }
        if self.is_dm { return .StreamDMChannel }
        return .StreamChannel
    }
    var sectionChannelRowType: SectionChannelRowableType = .StreamChannel
    override var messageable_id: Int? {
        get { return self.id }
        set { self.id = newValue }
    }
    override var messageable_uid: String? {
        get { return self.uid }
        set { self.uid = newValue ?? "" }
    }
    override var messageable_type: MessageableType? {
        get { return .StreamChannel }
        set { }
    }
    public var configable: Bool {
        return !is_system && !is_static && !is_im
    }
    public var deletable: Bool {
        return !is_static && !is_im
    }
    
    override init() {
        super.init()
    }
    
    init(channelable_type: ChannelableType) {
        super.init()
        switch channelable_type {
        case .VoiceChannel: self.voice_only = true
        case .MusicChannel: self.movie_only = true
        case .PlayChannel: self.is_im = true; self.is_dm = true
        case .StreamDMChannel: self.is_dm = true
        case .StreamChannel: self.is_dm = false
        default: break
        }
    }
    
    required init?(map: Map) {
        super.init()
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.SectionId <- map["SectionId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.index <- map["index"]
        self.max_participant_count <- map["max_participant_count"]
        self.message_count <- map["message_count"]
        self.unread_count <- map["unread_count"]
        self.member_count <- map["member_count"]
        self.temporary <- map["temporary"]
        self.is_nsfw <- map["is_nsfw"]
        self.is_default <- map["is_default"]
        self.is_static <- map["is_static"]
        self.is_im <- map["is_im"]
        self.is_dm <- map["is_dm"]
        self.is_system <- map["is_system"]
        self.is_loop <- map["is_loop"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        self.read_only <- map["read_only"]
        self.voice_only <- map["voice_only"]
        self.movie_only <- map["movie_only"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.Section <- map["Section"]
        self.Messages = MessageEntities(map: map["Messages"])
        self.Members = MemberEntities(map: map["Members"])
        self.Streams = StreamEntities(map: map["Streams"])
        self.Entries = StreamChannelEntryEntities(map: map["StreamChannelEntries"])
        self.Roles = StreamChannelRoleEntities(map: map["StreamChannelEntries"])
    }
    
    override func mapping(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.SectionId <- map["SectionId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.index <- map["index"]
        self.max_participant_count <- map["max_participant_count"]
        self.message_count <- map["message_count"]
        self.unread_count <- map["unread_count"]
        self.member_count <- map["member_count"]
        self.temporary <- map["temporary"]
        self.is_nsfw <- map["is_nsfw"]
        self.is_default <- map["is_default"]
        self.is_static <- map["is_static"]
        self.is_im <- map["is_im"]
        self.is_dm <- map["is_dm"]
        self.is_system <- map["is_system"]
        self.is_loop <- map["is_loop"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        self.read_only <- map["read_only"]
        self.voice_only <- map["voice_only"]
        self.movie_only <- map["movie_only"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.Section <- map["Section"]
        self.Messages = MessageEntities(map: map["Messages"])
        self.Members = MemberEntities(map: map["Members"])
        self.Streams = StreamEntities(map: map["Streams"])
        self.Entries = StreamChannelEntryEntities(map: map["StreamChannelEntries"])
        self.Roles = StreamChannelRoleEntities(map: map["StreamChannelEntries"])
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guild == nil { self.Guild = GuildEntity() }
        if self.Section == nil { self.Section = SectionChannelEntity() }
        if self.Messages == nil { self.Messages = MessageEntities() }
        if self.Members == nil { self.Members = MemberEntities() }
        if self.Streams == nil { self.Streams = StreamEntities() }
        if self.Entries == nil { self.Entries = StreamChannelEntryEntities() }
        if self.Roles == nil { self.Roles = StreamChannelRoleEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.SectionId <- map["SectionId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.index <- map["index"]
                self.max_participant_count <- map["max_participant_count"]
                self.message_count <- map["message_count"]
                self.unread_count <- map["unread_count"]
                self.member_count <- map["member_count"]
                self.temporary <- map["temporary"]
                self.is_nsfw <- map["is_nsfw"]
                self.is_default <- map["is_default"]
                self.is_static <- map["is_static"]
                self.is_im <- map["is_im"]
                self.is_dm <- map["is_dm"]
                self.is_system <- map["is_system"]
                self.is_loop <- map["is_loop"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.read_only <- map["read_only"]
                self.voice_only <- map["voice_only"]
                self.movie_only <- map["movie_only"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Guild!.asyncMapping(map: map["Guilds"])
        promises += self.Section!.asyncMapping(map: map["Section"])
        promises += self.Streams!.asyncMapping(map: map["Streams"])
        promises += self.Messages!.asyncMapping(map: map["Messages"])
        promises += self.Members!.asyncMapping(map: map["Members"])
        promises += self.Entries!.asyncMapping(map: map["StreamChannelEntries"])
        promises += self.Roles!.asyncMapping(map: map["StreamChannelRoles"])
        return promises
    }
    
    func asyncFastMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Streams == nil { self.Streams = StreamEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.SectionId <- map["SectionId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.index <- map["index"]
                self.max_participant_count <- map["max_participant_count"]
                self.message_count <- map["message_count"]
                self.unread_count <- map["unread_count"]
                self.member_count <- map["member_count"]
                self.temporary <- map["temporary"]
                self.is_nsfw <- map["is_nsfw"]
                self.is_default <- map["is_default"]
                self.is_static <- map["is_static"]
                self.is_im <- map["is_im"]
                self.is_dm <- map["is_dm"]
                self.is_system <- map["is_system"]
                self.is_loop <- map["is_loop"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.read_only <- map["read_only"]
                self.voice_only <- map["voice_only"]
                self.movie_only <- map["movie_only"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Streams!.asyncMapping(map: map["Streams"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "SectionId": self.SectionId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "index": self.index,
            "max_participant_count": self.max_participant_count,
            "message_count": self.message_count,
            "member_count": self.member_count,
            "temporary": self.temporary,
            "is_nsfw": self.is_nsfw,
            "is_static": self.is_static,
            "is_im": self.is_im,
            "is_default": self.is_default,
            "is_dm": self.is_dm,
            "is_system": self.is_system,
            "is_loop": self.is_loop,
            "permission": self.permission,
            "is_private": self.is_private,
            "read_only": self.read_only,
            "voice_only": self.voice_only,
            "movie_only": self.movie_only,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Guild": self.Guild?.toJSON() ?? [],
            "Section": self.Section?.toJSON() ?? [],
            "StreamChannelEntries": self.Entries?.toJSON() ?? [],
            "StreamChannelRoles": self.Roles?.toJSON() ?? [],
//            "Messages": self.Messages?.toJSON() ?? [],
            "Members": self.Members?.toNewMemoryJSON() ?? [],
//            "Streams": self.Streams?.toJSON() ?? [],
//            "LiveStream": self.LiveStream?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> ChannelableProtocol {
        let new = StreamChannelEntity()
        new.Guild = self.Guild?.toNewMemory()
        new.id = self.id
        new.GuildId = self.GuildId
        new.SectionId = self.SectionId
        new.uid = self.uid
        new.name = self.name
        new.description = self.description
        new.index = self.index
        new.max_participant_count = self.max_participant_count
        new.message_count = self.message_count
        new.member_count = self.member_count
        new.temporary = self.temporary
        new.is_static = self.is_static
        new.is_im = self.is_im
        new.is_nsfw = self.is_nsfw
        new.is_dm = self.is_dm
        new.is_default = self.is_default
        new.permission = self.permission
        new.is_private = self.is_private
        new.read_only = self.read_only
        new.voice_only = self.voice_only
        new.movie_only = self.movie_only
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
    
    func toNewMemoryChannel() -> StreamChannelEntity{
        let new = StreamChannelEntity()
        new.Guild = self.Guild?.toNewMemory()
        new.id = self.id
        new.GuildId = self.GuildId
        new.SectionId = self.SectionId
        new.uid = self.uid
        new.name = self.name
        new.description = self.description
        new.index = self.index
        new.max_participant_count = self.max_participant_count
        new.message_count = self.message_count
        new.member_count = self.member_count
        new.temporary = self.temporary
        new.is_static = self.is_static
        new.is_im = self.is_im
        new.is_nsfw = self.is_nsfw
        new.is_dm = self.is_dm
        new.is_system = self.is_system
        new.is_loop = self.is_loop
        new.is_default = self.is_default
        new.permission = self.permission
        new.is_private = self.is_private
        new.read_only = self.read_only
        new.voice_only = self.voice_only
        new.movie_only = self.movie_only
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

extension StreamChannelEntity: WebsocketPayloadable {
    typealias Element = StreamChannelEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "SectionId": self.SectionId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "index": self.index,
            "max_participant_count": self.max_participant_count,
            "message_count": self.message_count,
            "member_count": self.member_count,
            "temporary": self.temporary,
            "is_nsfw": self.is_nsfw,
            "is_default": self.is_default,
            "is_dm": self.is_dm,
            "permission": self.permission,
            "is_private": self.is_private,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> StreamChannelEntity? {
        let new = StreamChannelEntity()
        guard let id = json["id"].int, id != 0 else { return nil }
        new.id = id
        new.GuildId = json["GuildId"].intValue
        new.SectionId = json["SectionId"].intValue
        new.uid = json["uid"].stringValue
        new.name = json["name"].stringValue
        new.description = json["description"].stringValue
        new.max_participant_count = json["max_participant_count"].intValue
        new.member_count = json["member_count"].intValue
        new.message_count = json["message_count"].intValue
        new.index = json["index"].intValue
        new.temporary = json["temporary"].boolValue
        new.is_nsfw = json["is_nsfw"].boolValue
        new.is_default = json["is_default"].boolValue
        new.is_dm = json["is_dm"].boolValue
        new.permission = json["permission"].boolValue
        new.is_private = json["is_private"].boolValue
        return new
    }
}

class StreamChannelEntities: Entities, PropertyNames, Mappable {
    var items: [StreamChannelEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
}




