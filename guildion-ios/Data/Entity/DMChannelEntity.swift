//
//  DMChannelEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra
import SwiftyJSON

class DMChannelEntity: MessageableEntity, Entity, ChannelableProtocol, SectionChannelRowable {
    public var id: Int?
    public var GuildId: Int?
    public var SectionId: Int?
    public var uid: String = ""
    public var name: String = ""
    public var description: String = ""
    public var index: Int = 0 {
        didSet {
            if let unwrapped = _index {
                index_updated = index != unwrapped
            }
            _index = index
        }
    }
    public var message_count: Int = 0
    public var unread_count: Int = 0
    public var member_count: Int = 0
    public var permission: Bool = false
    public var is_static: Bool = false
    public var is_im: Bool = false
    public var is_system: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Guild: GuildEntity?
    public var Section: SectionChannelEntity?
    public var Messages: MessageEntities?
    public var Members: MemberEntities?
    public var Entries: DMChannelEntryEntities?
    public var Roles: DMChannelRoleEntities?
    
    private var _index: Int? = nil
    public var index_updated: Bool = false
    
    var channelable_type: ChannelableType {
        return self.is_im ? .IMChannel : .DMChannel
    }
    var sectionChannelRowType: SectionChannelRowableType = .DMChannel
    override var messageable_id: Int? {
        get { return self.id }
        set { self.id = newValue }
    }
    override var messageable_uid: String? {
        get { return self.uid }
        set { self.uid = newValue ?? "" }
    }
    override var messageable_type: MessageableType? {
        get { return .DMChannel }
        set { }
    }
    public var configable: Bool {
        return !is_system && !is_static && !is_im
    }
    public var deletable: Bool {
        return !is_static && !is_im
    }
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.SectionId <- map["SectionId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.index <- map["index"]
        self.message_count <- map["message_count"]
        self.unread_count <- map["unread_count"]
        self.member_count <- map["member_count"]
        self.is_static <- map["is_static"]
        self.is_im <- map["is_im"]
        self.is_system <- map["is_system"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.Section <- map["Section"]
        self.Messages = MessageEntities(map: map["Messages"])
        self.Members = MemberEntities(map: map["Members"])
        self.Entries = DMChannelEntryEntities(map: map["DMChannelEntries"])
        self.Roles = DMChannelRoleEntities(map: map["DMChannelRoles"])
    }
    
    override func mapping(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.SectionId <- map["SectionId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.index <- map["index"]
        self.message_count <- map["message_count"]
        self.unread_count <- map["unread_count"]
        self.member_count <- map["member_count"]
        self.is_static <- map["is_static"]
        self.is_im <- map["is_im"]
        self.is_system <- map["is_system"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.Section <- map["Section"]
        self.Messages = MessageEntities(map: map["Messages"])
        self.Members = MemberEntities(map: map["Members"])
        self.Entries = DMChannelEntryEntities(map: map["DMChannelEntries"])
        self.Roles = DMChannelRoleEntities(map: map["DMChannelRoles"])
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guild == nil { self.Guild = GuildEntity() }
        if self.Section == nil { self.Section = SectionChannelEntity() }
        if self.Messages == nil { self.Messages = MessageEntities() }
        if self.Members == nil { self.Members = MemberEntities() }
        if self.Entries == nil { self.Entries = DMChannelEntryEntities() }
        if self.Roles == nil { self.Roles = DMChannelRoleEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.SectionId <- map["SectionId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.index <- map["index"]
                self.message_count <- map["message_count"]
                self.unread_count <- map["unread_count"]
                self.member_count <- map["member_count"]
                self.is_static <- map["is_static"]
                self.is_im <- map["is_im"]
                self.is_system <- map["is_system"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Guild!.asyncMapping(map: map["Guilds"])
        promises += self.Section!.asyncMapping(map: map["Section"])
        promises += self.Messages!.asyncMapping(map: map["Messages"])
        promises += self.Members!.asyncMapping(map: map["Members"])
        promises += self.Entries!.asyncMapping(map: map["DMChannelEntries"])
        promises += self.Roles!.asyncMapping(map: map["DMChannelRoles"])
        return promises
    }
    
    func asyncFastMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        let promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.SectionId <- map["SectionId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.index <- map["index"]
                self.message_count <- map["message_count"]
                self.unread_count <- map["unread_count"]
                self.member_count <- map["member_count"]
                self.is_static <- map["is_static"]
                self.is_im <- map["is_im"]
                self.is_system <- map["is_system"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "SectionId": self.SectionId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "index": self.index,
            "message_count": self.message_count,
            "member_count": self.member_count,
            "permission": self.permission,
            "is_static": self.is_static,
            "is_im": self.is_im,
            "is_system": self.is_system,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Guild": self.Guild?.toJSON() ?? [],
            "Section": self.Section?.toJSON() ?? [],
            "Messages": self.Messages?.toJSON() ?? [],
            "Members": self.Members?.toJSON() ?? [],
            "DMChannelEntries": self.Entries?.toJSON() ?? [],
            "DMChannelRoles": self.Roles?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> ChannelableProtocol {
        let new = DMChannelEntity()
        new.Guild = self.Guild?.toNewMemory()
        new.id = self.id
        new.GuildId = self.GuildId
        new.SectionId = self.SectionId
        new.uid = self.uid
        new.name = self.name
        new.description = self.description
        new.index = self.index
        new.message_count = self.message_count
        new.member_count = self.member_count
        new.is_static = self.is_static
        new.is_im = self.is_im
        new.is_system = self.is_system
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
    
    func toNewMemoryChannel() -> DMChannelEntity {
        let new = DMChannelEntity()
        new.Guild = self.Guild?.toNewMemory()
        new.id = self.id
        new.GuildId = self.GuildId
        new.SectionId = self.SectionId
        new.uid = self.uid
        new.name = self.name
        new.description = self.description
        new.index = self.index
        new.message_count = self.message_count
        new.member_count = self.member_count
        new.is_static = self.is_static
        new.is_im = self.is_im
        new.is_system = self.is_system
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

extension DMChannelEntity: WebsocketPayloadable {
    typealias Element = DMChannelEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "SectionId": self.SectionId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "index": self.index,
            "message_count": self.message_count,
            "member_count": self.member_count,
            "permission": self.permission,
            "is_private": self.is_private,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> DMChannelEntity? {
        let new = DMChannelEntity()
        guard let id = json["id"].int, id != 0 else { return nil }
        new.id = id
        new.GuildId = json["GuildId"].intValue
        new.SectionId = json["SectionId"].intValue
        new.uid = json["uid"].stringValue
        new.name = json["name"].stringValue
        new.description = json["description"].stringValue
        new.member_count = json["member_count"].intValue
        new.message_count = json["message_count"].intValue
        new.index = json["index"].intValue
        new.permission = json["permission"].boolValue
        new.is_private = json["is_private"].boolValue
        return new
    }
}

class DMChannelEntities: Entities, PropertyNames, Mappable {
    var items: [DMChannelEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
}
