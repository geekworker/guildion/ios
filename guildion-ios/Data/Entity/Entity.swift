//
//  Entity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Hydra
import ObjectMapper

protocol Entity: class {
    var id: Int? { get set }
    func toJSON() -> [String: Any]
    func asyncMapping(map _map: Map, shouldConvert: Bool) -> [Promise<Void>]
}

extension Entity {
    func asyncMapping(map _map: Map, shouldConvert: Bool) -> [Promise<Void>] { return [] }
}

protocol Entities: class {
    associatedtype Element
    var items: [Element] { get set }
    var count: Int { get }
    func toJSON() -> [[String: Any]]
    func asyncMapping(map: Map) -> [Promise<Void>]
}

extension Entities {
    var count: Int {
        get { return items.count }
    }
    
    subscript(safe index: Int) -> Element? {
        return items[safe: index]
    }
}


extension Entities where Element: Equatable {
    
    @inlinable var capacity: Int { self.items.capacity }

    @inlinable static func + (lhs: Self, rhs: Self) -> Self {
        lhs.items = lhs.items + rhs.items
        return lhs
    }

    @inlinable static func += (lhs: inout Self, rhs: Self) {
        lhs.items += rhs.items
    }
    
    func unified() {
        self.items = items.unique
    }
    
    func contains(_ element: Element) -> Bool {
        return items.contains(element)
    }
    
    func remove(_ element: Element) {
        items.remove(element: element)
    }
    
    func append(_ newElement: Element) {
        items.append(newElement)
    }
    
    func insert(_ newElement: Element, at: Int) {
        items.insert(newElement, at: at)
    }
    
    func indexOf(_ element: Element) -> Int? {
        for item in items.enumerated() {
            if item.element == element {
                return item.offset
            }
        }
        return nil
    }
    
    func elementsEqual<OtherSequence>(_ other: OtherSequence) -> Bool where OtherSequence : Sequence, Self.Element == OtherSequence.Element {
        return self.items.elementsEqual(other)
    }
    
    func popLast() -> Element? {
        return self.items.popLast()
    }
    
    func removeLast() -> Element? {
        return self.items.removeLast()
    }
    
    func removeLast(_ k: Int) {
        self.items.removeLast(k)
    }
    
    func dropLast(_ k: Int) -> ArraySlice<Element> {
        return self.items.dropLast(k)
    }
    
    func suffix(_ maxLength: Int) -> ArraySlice<Element> {
        return self.items.suffix(maxLength)
    }
    
//    func popFirst() -> Self.Element? {
//        return self.items.popFirst()
//    }
    
    @inlinable func map<T>(_ transform: (Element) throws -> T) rethrows -> [T] {
        try! self.items.map(transform)
    }
    
    @inlinable func dropFirst(_ k: Int = 1) -> ArraySlice<Element> {
        return self.items.dropFirst(k)
    }
    
    @inlinable func drop(while predicate: (Element) throws -> Bool) rethrows -> ArraySlice<Element> {
        try! self.items.drop(while: predicate)
    }
    
    @inlinable func prefix(_ maxLength: Int) -> ArraySlice<Element> {
        self.items.prefix(maxLength)
    }
    
    @inlinable func prefix(while predicate: (Element) throws -> Bool) rethrows -> ArraySlice<Element> {
        try! self.items.prefix(while: predicate)
    }
    
    @inlinable func prefix(upTo end: Int) -> ArraySlice<Element> {
        self.items.prefix(upTo: end)
    }
    
    @inlinable func suffix(from start: Int) -> ArraySlice<Element> {
        self.items.suffix(from: start)
    }
    
    @inlinable func prefix(through position: Int) -> ArraySlice<Element> {
        self.items.prefix(through: position)
    }
    
    @inlinable func split(maxSplits: Int = Int.max, omittingEmptySubsequences: Bool = true, whereSeparator isSeparator: (Element) throws -> Bool) rethrows -> [ArraySlice<Element>] {
        try! self.items.split(maxSplits: maxSplits, omittingEmptySubsequences: omittingEmptySubsequences, whereSeparator: isSeparator)
    }
    
    @inlinable func removeFirst() -> Element {
        self.items.removeFirst()
    }
    
    @inlinable func removeFirst(_ k: Int) {
        self.items.removeFirst(k)
    }
    
    @inlinable var first: Element? {
        self.items.first
    }
    
    @inlinable var last: Element? {
        self.items.last
    }
    
    @inlinable func firstIndex(where predicate: (Element) throws -> Bool) rethrows -> Int? {
        try! self.items.firstIndex(where: predicate)
    }
    
    @inlinable func last(where predicate: (Element) throws -> Bool) rethrows -> Element? {
        try! self.items.last(where: predicate)
    }
    
    @inlinable func lastIndex(where predicate: (Element) throws -> Bool) rethrows -> Int? {
        return try! self.items.lastIndex(where: predicate)
    }
    
    
    
    @inlinable func reduce<Result>(_ initialResult: Result, _ nextPartialResult: (Result, Element) throws -> Result) rethrows -> Result {
        return try! self.items.reduce(initialResult, nextPartialResult)
    }
    
    @inlinable func reduce<Result>(into initialResult: Result, _ updateAccumulatingResult: (inout Result, Element) throws -> ()) rethrows -> Result {
        return try! self.items.reduce(into: initialResult, updateAccumulatingResult)
    }
    
    @inlinable func flatMap<SegmentOfResult>(_ transform: (Element) throws -> SegmentOfResult) rethrows -> [SegmentOfResult.Element] where SegmentOfResult : Sequence {
        try! self.items.flatMap(transform)
    }
    
    @inlinable func compactMap<ElementOfResult>(_ transform: (Element) throws -> ElementOfResult?) rethrows -> [ElementOfResult] {
        try! self.items.compactMap(transform)
    }
    
    @inlinable func sorted(by areInIncreasingOrder: (Element, Element) throws -> Bool) rethrows -> [Element] {
        return try! self.items.sorted(by: areInIncreasingOrder)
    }
    
    @inlinable func sort(by areInIncreasingOrder: (Element, Element) throws -> Bool) rethrows {
        try! self.items.sort(by: areInIncreasingOrder)
    }
    
    @inlinable func filter(_ isIncluded: (Self.Element) throws -> Bool) rethrows -> [Self.Element] {
        try! self.items.filter(isIncluded)
    }
    
    @inlinable func enumerated() -> EnumeratedSequence<[Self.Element]> {
        return self.items.enumerated()
    }
}
