//
//  MessageableEntity+Type.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/10.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

enum MessageableType: String {
    case TextChannel = "TextChannel"
    case StreamChannel = "StreamChannel"
    case DMChannel = "DMChannel"
}

class MessageableEntity: Mappable {
    var messageable_type: MessageableType?
    var messageable_id: Int?
    var messageable_uid: String?
    var messageable_map: Map?

    init() {}
    
    required init?(map: Map) {
        messageable_type <- (map["messageable_type"], EnumTransform<MessageableType>())
        messageable_map = map
    }

    func mapping(map: Map) {
        messageable_type <- (map["messageable_type"], EnumTransform<MessageableType>())
        messageable_map = map
    }

    static func objectForMapping(map: Map) -> MessageableEntity? {
        let typeString: String? = map["messageable_type"].value()
        if let typeString = typeString {
            let messageable_type: MessageableType? = MessageableType(rawValue: typeString)
            if let messageable_type = messageable_type {
                switch(messageable_type) {
                case .TextChannel:
                    var entity = TextChannelEntity()
                    entity <- map["Messageable"]
                    return entity
                case .StreamChannel:
                    var entity = StreamChannelEntity()
                    entity <- map["Messageable"]
                    return entity
                case .DMChannel:
                    var entity = DMChannelEntity()
                    entity <- map["Messageable"]
                    return entity
                }
            }
        }
        return nil
    }
    
    func upcasting() -> MessageableEntity? {
        guard let unwrapped_type = messageable_type, let unwrapped_map = messageable_map else { return nil }
        switch(unwrapped_type) {
        case .TextChannel:
            var entity = TextChannelEntity()
            entity <- unwrapped_map["Messageable"]
            return entity
        case .StreamChannel:
            var entity = StreamChannelEntity()
            entity <- unwrapped_map["Messageable"]
            return entity
        case .DMChannel:
            var entity = DMChannelEntity()
            entity <- unwrapped_map["Messageable"]
            return entity
        }
    }
}

class MessageableEntities: PropertyNames, Mappable {
    var items: [MessageableEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension MessageableEntity: Equatable {
    static func == (lhs: MessageableEntity, rhs: MessageableEntity) -> Bool {
        guard let lid = lhs.messageable_id, let rid = rhs.messageable_id else { return false }
        return lid == rid && lhs.messageable_type == rhs.messageable_type
    }
}
