//
//  Devise.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/21.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra
import SwiftyJSON

class DeviseEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var uid: String = ""
    public var udid: String = ""
    public var os: String = ""
    public var model: String = ""
    public var locale: String = ""
    public var country_code: String = ""
    public var notification_id: String = ""
    public var app_version: String = ""
    public var permission: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.udid <- map["udid"]
        self.os <- map["os"]
        self.model <- map["model"]
        self.locale <- map["locale"]
        self.country_code <- map["country_code"]
        self.notification_id <- map["notification_id"]
        self.app_version <- map["app_version"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.udid <- map["udid"]
        self.os <- map["os"]
        self.model <- map["model"]
        self.locale <- map["locale"]
        self.country_code <- map["country_code"]
        self.notification_id <- map["notification_id"]
        self.app_version <- map["app_version"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        let promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.uid <- map["uid"]
                self.udid <- map["udid"]
                self.os <- map["os"]
                self.model <- map["model"]
                self.locale <- map["locale"]
                self.country_code <- map["country_code"]
                self.notification_id <- map["notification_id"]
                self.app_version <- map["app_version"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "uid": self.uid,
            "udid": self.udid,
            "os": self.os,
            "model": self.model,
            "locale": self.locale,
            "country_code": self.country_code,
            "notification_id": self.notification_id,
            "app_version": self.app_version,
            "permission": self.permission,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        ]
    }
    
    func toNewMemory() -> DeviseEntity {
        let new = DeviseEntity()
        new.id = self.id
        new.uid = self.uid
        new.udid = self.udid
        new.os = self.os
        new.locale = self.locale
        new.country_code = self.country_code
        new.notification_id = self.notification_id
        new.app_version = self.app_version
        new.permission = self.permission
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

class DeviseEntities: Entities, PropertyNames, Mappable {
    var items: [DeviseEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
}

extension DeviseEntity: Equatable {
    static func == (lhs: DeviseEntity, rhs: DeviseEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.uid == rhs.uid }
        return lid == rid && lhs.uid == rhs.uid
    }
}

extension DeviseEntities: Equatable {
    static func == (lhs: DeviseEntities, rhs: DeviseEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
