//
//  ChannelRoleable.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation
import UIKit

protocol ChannelRoleableProtocol: class {
    var RoleId: Int? { get set }
    var channelable_type: ChannelableType { get }
    func toNewMemory() -> ChannelRoleableProtocol
}
