//
//  IdentityEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra

class IdentityEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var UserId: Int?
    public var username: String = ""
    public var mail_notification_token: String = ""
    public var enable_mail_notification: Bool = true
    public var email_is_verified: Bool = false
    public var last_attempt_verify_email: Date = Date.CurrentDate()
    public var verify_email_attempts: Int = 0
    public var email: String = ""
    public var token: String = ""
    public var password: String = ""
    public var delete_password_token: String = ""
    public var verified: Bool = false
    public var is_deleted: Bool = false
    public var permission: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var User: UserEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.UserId <- map["UserId"]
        self.username <- map["username"]
        self.mail_notification_token <- map["mail_notification_token"]
        self.enable_mail_notification <- map["enable_mail_notification"]
        self.email_is_verified <- map["email_is_verified"]
        self.last_attempt_verify_email <- map["last_attempt_verify_email"]
        self.verify_email_attempts <- map["verify_email_attempts"]
        self.email <- map["email"]
        self.token <- map["token"]
        self.password <- map["password"]
        self.delete_password_token <- map["delete_password_token"]
        self.verified <- map["verified"]
        self.is_deleted <- map["is_deleted"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.User <- map["User"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.UserId <- map["UserId"]
        self.username <- map["username"]
        self.mail_notification_token <- map["mail_notification_token"]
        self.enable_mail_notification <- map["enable_mail_notification"]
        self.email_is_verified <- map["email_is_verified"]
        self.last_attempt_verify_email <- map["last_attempt_verify_email"]
        self.verify_email_attempts <- map["verify_email_attempts"]
        self.email <- map["email"]
        self.token <- map["token"]
        self.password <- map["password"]
        self.delete_password_token <- map["delete_password_token"]
        self.verified <- map["verified"]
        self.is_deleted <- map["is_deleted"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.User <- map["User"]
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "UserId": self.UserId ?? 0,
            "username": self.username,
            "mail_notification_token": self.mail_notification_token,
            "enable_mail_notification": self.enable_mail_notification,
            "email_is_verified": self.email_is_verified,
            "last_attempt_verify_email": self.last_attempt_verify_email,
            "verify_email_attempts": self.verify_email_attempts,
            "email": self.email,
            "token": self.token,
            "password": self.password,
            "delete_password_token": self.delete_password_token,
            "verified": self.verified,
            "is_deleted": self.is_deleted,
            "permission": self.permission,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "User": self.User?.toJSON() ?? [],
        ]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.User == nil { self.User = UserEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.UserId <- map["UserId"]
                self.username <- map["username"]
                self.mail_notification_token <- map["mail_notification_token"]
                self.enable_mail_notification <- map["enable_mail_notification"]
                self.email_is_verified <- map["email_is_verified"]
                self.last_attempt_verify_email <- map["last_attempt_verify_email"]
                self.verify_email_attempts <- map["verify_email_attempts"]
                self.email <- map["email"]
                self.token <- map["token"]
                self.password <- map["password"]
                self.delete_password_token <- map["delete_password_token"]
                self.verified <- map["verified"]
                self.is_deleted <- map["is_deleted"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        + self.User!.asyncMapping(map: map["User"])
    }
}

class IdentityEntities: Entities, PropertyNames, Mappable {
    var items: [IdentityEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}
