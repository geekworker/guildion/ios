//
//  FolderEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Hydra

class FolderEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var GuildId: Int?
    public var MemberId: Int?
    public var uid: String = ""
    public var name: String = ""
    public var description: String = ""
    public var thumbnail: String = ""
    public var index: Int = 0 {
        didSet {
            if let unwrapped = _index {
                index_updated = index != unwrapped
            }
            _index = index
        }
    }
    public var size_byte: Double = 0
    public var file_count: Int = 0
    public var movie_count: Int = 0
    public var read_only: Bool = true
    public var is_nsfw: Bool = true
    public var is_playlist: Bool = false
    public var temporary: Bool = false
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Guild: GuildEntity?
    public var Member: MemberEntity?
    public var Files: FileEntities?
    public var FileReferences: FileReferenceEntities?
    
    private var _index: Int? = nil
    public var index_updated: Bool = false
    
    public lazy var byteCountFormatter: ByteCountFormatter = {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = .useAll
        formatter.countStyle = .file
        formatter.includesUnit = true
        formatter.isAdaptive = true
        return formatter
    }()
    public var size: String {
        byteCountFormatter.string(fromByteCount: Int64(self.size_byte))
    }
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.MemberId <- map["MemberId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.thumbnail <- map["thumbnail"]
        self.index <- map["index"]
        self.size_byte <- map["size_byte"]
        self.file_count <- map["file_count"]
        self.movie_count <- map["movie_count"]
        self.read_only <- map["read_only"]
        self.is_playlist <- map["is_playlist"]
        self.temporary <- map["temporary"]
        self.is_nsfw <- map["is_nsfw"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.Member <- map["Member"]
        self.Files = FileEntities(map: map["Files"])
        self.FileReferences = FileReferenceEntities(map: map["FileReferences"])
        self.FileReferences?.items = self.FileReferences?.sorted() ?? []
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.MemberId <- map["MemberId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.thumbnail <- map["thumbnail"]
        self.index <- map["index"]
        self.size_byte <- map["size_byte"]
        self.file_count <- map["file_count"]
        self.movie_count <- map["movie_count"]
        self.read_only <- map["read_only"]
        self.is_playlist <- map["is_playlist"]
        self.temporary <- map["temporary"]
        self.is_nsfw <- map["is_nsfw"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.Member <- map["Member"]
        self.Files = FileEntities(map: map["Files"])
        self.FileReferences = FileReferenceEntities(map: map["FileReferences"])
        self.FileReferences?.items = self.FileReferences?.sorted() ?? []
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guild == nil { self.Guild = GuildEntity() }
        if self.Member == nil { self.Member = MemberEntity() }
        if self.Files == nil { self.Files = FileEntities() }
        if self.FileReferences == nil { self.FileReferences = FileReferenceEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.MemberId <- map["MemberId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.thumbnail <- map["thumbnail"]
                self.index <- map["index"]
                self.size_byte <- map["size_byte"]
                self.file_count <- map["file_count"]
                self.movie_count <- map["movie_count"]
                self.read_only <- map["read_only"]
                self.is_playlist <- map["is_playlist"]
                self.temporary <- map["temporary"]
                self.is_nsfw <- map["is_nsfw"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Guild!.asyncMapping(map: map["Guild"])
        promises += self.Member!.asyncMapping(map: map["Member"])
        promises += self.Files!.asyncMapping(map: map["Files"])
        promises += self.FileReferences!.asyncMapping(map: map["FileReferences"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "MemberId": self.MemberId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "thumbnail": self.thumbnail,
            "index": self.index,
            "size_byte": self.size_byte,
            "file_count": self.file_count,
            "movie_count": self.movie_count,
            "read_only": self.read_only,
            "is_playlist": self.is_playlist,
            "temporary": self.temporary,
            "is_nsfw": self.is_nsfw,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            // "Guild": self.Guild?.toJSON() ?? [],
            // "Member": self.Member?.toJSON() ?? [],
            // "Files": self.Files?.toJSON() ?? [],
//            "FileReferences": self.FileReferences?.toJSON() ?? []
        ]
    }
    
    func increment(reference: FileReferenceEntity) -> FileReferenceEntity? {
        var offset: Int?
        return FileReferences?.items.enumerated().compactMap({
            if $0.element == reference {
                offset = $0.offset + 1
            }
            if $0.offset == offset {
                return $0.element
            }
            return nil
        }).first
    }
    
    func decrement(reference: FileReferenceEntity) -> FileReferenceEntity? {
        var offset: Int?
        return FileReferences?.items.reversed().enumerated().compactMap({
            if $0.element == reference {
                offset = $0.offset + 1
            }
            if $0.offset == offset {
                return $0.element
            }
            return nil
        }).first
    }
    
    func incrementFromFile(file: FileEntity) -> FileReferenceEntity? {
        var offset: Int?
        return FileReferences?.items.enumerated().compactMap({
            if $0.element.File == file {
                offset = $0.offset + 1
            }
            if $0.offset == offset {
                return $0.element
            }
            return nil
        }).first
    }
    
    func decrementFromFile(file: FileEntity) -> FileReferenceEntity? {
        var offset: Int?
        return FileReferences?.items.reversed().enumerated().compactMap({
            if $0.element.File == file {
                offset = $0.offset + 1
            }
            if $0.offset == offset {
                return $0.element
            }
            return nil
        }).first
    }
    
    func toNewMemory() -> FolderEntity {
        let new = FolderEntity()
        new.id = self.id
        new.GuildId = self.GuildId
        new.MemberId = self.MemberId
        new.uid = self.uid
        new.name = self.name
        new.description = self.description
        new.thumbnail = self.thumbnail
        new.index = self.index
        new.size_byte = self.size_byte
        new.file_count = self.file_count
        new.movie_count = self.movie_count
        new.read_only = self.read_only
        new.is_nsfw = self.is_nsfw
        new.is_playlist = self.is_playlist
        new.temporary = self.temporary
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

class FolderEntities: Entities, PropertyNames, Mappable {
    var items: [FolderEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return self.items.compactMap { $0.toJSON() }
    }
    
    func unified() {
        for item in items.filter({ $0.id == 0 || $0.id == nil }) {
            item.id = items
                .filter({ $0.id != 0 || $0.id == nil })
                .filter { $0.MemberId == $0.MemberId && $0.GuildId == $0.GuildId && $0.name == $0.name }.first?.id
        }
        self.items = items.unique
    }
    
    func sorted() -> [FolderEntity] {
        return items.sorted(by: { lChannel, rChannel -> Bool in
            return lChannel.index == rChannel.index ? lChannel.created_at > rChannel.created_at : lChannel.index > rChannel.index
        })
    }
}

extension FolderEntity: Equatable {
    static func == (lhs: FolderEntity, rhs: FolderEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.uid == rhs.uid }
        return lid == rid
    }
}

extension FolderEntities: Equatable {
    static func == (lhs: FolderEntities, rhs: FolderEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}

extension FolderEntity: WebsocketPayloadable {
    typealias Element = FolderEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "uid": self.uid,
            "GuildId": self.GuildId ?? 0,
            "MemberId": self.MemberId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "thumbnail": self.thumbnail,
            "index": self.index,
            "size_byte": self.size_byte,
            "file_count": self.file_count,
            "movie_count": self.movie_count,
            "read_only": self.read_only,
            "is_nsfw": self.is_nsfw,
            "is_playlist": self.is_playlist,
            "temporary": self.temporary,
            "permission": self.permission,
            "is_private": self.is_private,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> FolderEntity? {
        let entity = FolderEntity()
        guard let id = json["id"].int, id != 0, let MemberId = json["MemberId"].int, MemberId != 0, let GuildId = json["GuildId"].int, GuildId != 0 else { return nil }
        entity.id = id
        entity.GuildId = GuildId
        entity.MemberId = MemberId
        entity.uid = json["uid"].stringValue
        entity.name = json["name"].stringValue
        entity.description = json["description"].stringValue
        entity.thumbnail = json["thumbnail"].stringValue
        entity.index = json["index"].intValue
        entity.size_byte = json["size_byte"].doubleValue
        entity.file_count = json["file_count"].intValue
        entity.movie_count = json["movie_count"].intValue
        entity.read_only = json["read_only"].boolValue
        entity.is_nsfw = json["is_nsfw"].boolValue
        entity.is_playlist = json["is_playlist"].boolValue
        entity.temporary = json["temporary"].boolValue
        entity.permission = json["permission"].boolValue
        entity.is_private = json["is_private"].boolValue
        return entity
    }
}
