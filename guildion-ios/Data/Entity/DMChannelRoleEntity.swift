//
//  DMChannelRoleEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

class DMChannelRoleEntity: Entity, PropertyNames, Mappable, ChannelRoleableProtocol {
    public var id: Int?
    public var RoleId: Int?
    public var ChannelId: Int?
    public var roles_managable: Bool?
    public var channel_managable: Bool?
    public var channel_members_managable: Bool?
    public var kickable: Bool?
    public var invitable: Bool?
    public var channel_viewable: Bool?
    public var message_sendable: Bool?
    public var messages_managable: Bool?
    public var message_embeddable: Bool?
    public var message_attachable: Bool?
    public var messages_readable: Bool?
    public var message_mentionable: Bool?
    public var message_reactionable: Bool?
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Role: RoleEntity?
    public var Channel: DMChannelEntity?
    
    public var channelable_type: ChannelableType = .DMChannel
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.RoleId <- map["RoleId"]
        self.ChannelId <- map["ChannelId"]
        self.roles_managable <- map["roles_managable"]
        self.channel_managable <- map["channel_managable"]
        self.channel_members_managable <- map["channel_members_managable"]
        self.kickable <- map["kickable"]
        self.invitable <- map["invitable"]
        self.channel_viewable <- map["channel_viewable"]
        self.message_sendable <- map["message_sendable"]
        self.messages_managable <- map["messages_managable"]
        self.message_embeddable <- map["message_embeddable"]
        self.message_attachable <- map["message_attachable"]
        self.messages_readable <- map["messages_readable"]
        self.message_mentionable <- map["message_mentionable"]
        self.message_reactionable <- map["message_reactionable"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Role <- map["Role"]
        self.Channel <- map["Channel"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.RoleId <- map["RoleId"]
        self.ChannelId <- map["ChannelId"]
        self.roles_managable <- map["roles_managable"]
        self.channel_managable <- map["channel_managable"]
        self.channel_members_managable <- map["channel_members_managable"]
        self.kickable <- map["kickable"]
        self.invitable <- map["invitable"]
        self.channel_viewable <- map["channel_viewable"]
        self.message_sendable <- map["message_sendable"]
        self.messages_managable <- map["messages_managable"]
        self.message_embeddable <- map["message_embeddable"]
        self.message_attachable <- map["message_attachable"]
        self.messages_readable <- map["messages_readable"]
        self.message_mentionable <- map["message_mentionable"]
        self.message_reactionable <- map["message_reactionable"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Role <- map["Role"]
        self.Channel <- map["Channel"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Role == nil { self.Role = RoleEntity() }
        if self.Channel == nil { self.Channel = DMChannelEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.RoleId <- map["RoleId"]
                self.ChannelId <- map["ChannelId"]
                self.roles_managable <- map["roles_managable"]
                self.channel_managable <- map["channel_managable"]
                self.channel_members_managable <- map["channel_members_managable"]
                self.kickable <- map["kickable"]
                self.invitable <- map["invitable"]
                self.channel_viewable <- map["channel_viewable"]
                self.message_sendable <- map["message_sendable"]
                self.messages_managable <- map["messages_managable"]
                self.message_embeddable <- map["message_embeddable"]
                self.message_attachable <- map["message_attachable"]
                self.messages_readable <- map["messages_readable"]
                self.message_mentionable <- map["message_mentionable"]
                self.message_reactionable <- map["message_reactionable"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ] + self.Role!.asyncMapping(map: map["Role"]) + self.Channel!.asyncMapping(map: map["Channel"])
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "RoleId": self.RoleId ?? 0,
            "ChannelId": self.ChannelId ?? 0,
            "roles_managable": self.roles_managable as Any,
            "channel_managable": self.channel_managable as Any,
            "channel_members_managable": self.channel_members_managable as Any,
            "kickable": self.kickable as Any,
            "invitable": self.invitable as Any,
            "channel_viewable": self.channel_viewable as Any,
            "message_sendable": self.message_sendable as Any,
            "messages_managable": self.messages_managable as Any,
            "message_embeddable": self.message_embeddable as Any,
            "message_attachable": self.message_attachable as Any,
            "messages_readable": self.messages_readable as Any,
            "message_mentionable": self.message_mentionable as Any,
            "message_reactionable": self.message_reactionable as Any,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Role": self.Role?.toJSON() ?? [],
            "Channel": self.Channel?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> ChannelRoleableProtocol {
        let new = DMChannelRoleEntity()
        new.id = self.id
        new.RoleId = self.RoleId
        new.ChannelId = self.ChannelId
        new.roles_managable = self.roles_managable
        new.channel_managable = self.channel_managable
        new.channel_members_managable = self.channel_members_managable
        new.kickable = self.kickable
        new.invitable = self.invitable
        new.channel_viewable = self.channel_viewable
        new.message_sendable = self.message_sendable
        new.messages_managable = self.messages_managable
        new.message_embeddable = self.message_embeddable
        new.message_attachable = self.message_attachable
        new.messages_readable = self.messages_readable
        new.message_mentionable = self.message_mentionable
        new.message_reactionable = self.message_reactionable
        new.Role = self.Role
        new.Channel = self.Channel
        return new
    }
}

class DMChannelRoleEntities: Entities, PropertyNames, Mappable {
    var items: [DMChannelRoleEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension DMChannelRoleEntity: Equatable {
    static func == (lhs: DMChannelRoleEntity, rhs: DMChannelRoleEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension DMChannelRoleEntities: Equatable {
    static func == (lhs: DMChannelRoleEntities, rhs: DMChannelRoleEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
