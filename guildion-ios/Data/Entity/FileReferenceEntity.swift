//
//  FileReferenceEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra
import SwiftyJSON

class FileReferenceEntity: StreamableEntity, PropertyNames {
    public var id: Int?
    public var FileId: Int?
    public var FolderId: Int?
    public var index: Int = 0 {
        didSet {
            if let unwrapped = _index {
                index_updated = index != unwrapped
            }
            _index = index
        }
    }
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var File: FileEntity?
    public var Folder: FolderEntity?
    
    private var _index: Int? = nil
    public var index_updated: Bool = false
    
    override var streamable_id: Int? {
        get { return self.id }
        set { self.id = newValue }
    }
    override var streamable_uid: String? {
        get { return "\(self.id ?? 0)" }
        set { }
    }
    override var streamable_type: StreamableType? {
        get { return .FileReference }
        set { }
    }
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.id <- map["id"]
        self.FileId <- map["FileId"]
        self.FolderId <- map["FolderId"]
        self.index <- map["index"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.File <- map["File"]
        self.Folder <- map["Folder"]
    }
    
    override func mapping(map: Map) {
        self.id <- map["id"]
        self.FileId <- map["FileId"]
        self.FolderId <- map["FolderId"]
        self.index <- map["index"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.File <- map["File"]
        self.Folder <- map["Folder"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.File == nil { self.File = FileEntity() }
        if self.Folder == nil { self.Folder = FolderEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.FileId <- map["FileId"]
                self.FolderId <- map["FolderId"]
                self.index <- map["index"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ] + self.File!.asyncMapping(map: map["File"]) + self.Folder!.asyncMapping(map: map["Folder"])
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.File == nil { self.File = FileEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.FileId <- map["FileId"]
                self.FolderId <- map["FolderId"]
                self.index <- map["index"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ] + self.File!.asyncPureMapping(map: map["File"])
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "FileId": self.FileId ?? 0,
            "FolderId": self.FolderId ?? 0,
            "index": self.index,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "File": self.File?.toJSON() ?? [],
            "Folder": self.Folder?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> FileReferenceEntity {
        let new = FileReferenceEntity()
        new.id = self.id
        new.FileId = self.FileId
        new.FolderId = self.FolderId
        new.index = self.index
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        new.File = self.File?.toNewMemory()
        return new
    }
}

class FileReferenceEntities: Entities, StreamableEntities, PropertyNames, Mappable {
    var items: [FileReferenceEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
    
    func sorted() -> [FileReferenceEntity] {
        return items.sorted(by: { lhs, rhs -> Bool in
            return lhs.index == rhs.index ? lhs.created_at < rhs.created_at : lhs.index < rhs.index
        })
    }
    
    func unified() {
        for item in items.filter({ $0.id == 0 || $0.id == nil }) {
            item.id = items
                .filter({ $0.id != 0 && $0.id != nil })
                .filter { $0.FileId == $0.FileId && $0.FolderId == $0.FolderId }.first?.id
        }
        self.items = items.unique
    }
}

extension FileReferenceEntity: WebsocketPayloadable {
    typealias Element = FileReferenceEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "FileId": self.FileId ?? 0,
            "FolderId": self.FolderId ?? 0,
            "index": self.index,
            "File": self.File?.toWebsocketPayload() as Any
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> FileReferenceEntity? {
        let entity = FileReferenceEntity()
        guard let id = json["id"].number, id != 0, let FileId = json["FileId"].number, FileId != 0 else { return nil }
        entity.id = Int(truncating: id)
        entity.FolderId = Int(truncating: json["FolderId"].numberValue)
        entity.FileId = Int(truncating: FileId)
        entity.index = Int(truncating: json["index"].numberValue)
        entity.File = FileEntity.jsonable(json["File"])
        return entity
    }
}

extension FileReferenceEntity: Equatable {
    static func == (lhs: FileReferenceEntity, rhs: FileReferenceEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.FileId == rhs.FileId && lhs.FolderId == rhs.FolderId  }
        return lid == rid
    }
}

extension FileReferenceEntities: Equatable {
    static func == (lhs: FileReferenceEntities, rhs: FileReferenceEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
