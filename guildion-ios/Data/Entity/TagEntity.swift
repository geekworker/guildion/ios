//
//  TagEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra

class TagEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var uid: String = ""
    public var name: String = ""
    public var locale: String = ""
    public var country_code: String = ""
    public var guild_count: Int = 0
    public var is_nsfw: Bool = true
    public var permission: Bool = false
    public var is_private: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Guilds: GuildEntities?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.locale <- map["locale"]
        self.country_code <- map["country_code"]
        self.guild_count <- map["guild_count"]
        self.is_nsfw <- map["is_nsfw"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guilds = GuildEntities(map: map["Guilds"])
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.locale <- map["locale"]
        self.country_code <- map["country_code"]
        self.guild_count <- map["guild_count"]
        self.is_nsfw <- map["is_nsfw"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guilds = GuildEntities(map: map["Guilds"])
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guilds == nil { self.Guilds = GuildEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.locale <- map["locale"]
                self.country_code <- map["country_code"]
                self.guild_count <- map["guild_count"]
                self.is_nsfw <- map["is_nsfw"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Guilds!.asyncMapping(map: map["Guilds"])
        return promises
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        let promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.locale <- map["locale"]
                self.country_code <- map["country_code"]
                self.guild_count <- map["guild_count"]
                self.is_nsfw <- map["is_nsfw"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "uid": self.uid,
            "name": self.name,
            "locale": self.locale,
            "country_code": self.country_code,
            "guild_count": self.guild_count,
            "is_nsfw": self.is_nsfw,
            "permission": self.permission,
            "is_private": self.is_private,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Guilds": self.Guilds?.toJSON() ?? [],
        ]
    }
}

class TagEntities: Entities, PropertyNames, Mappable {
    var items: [TagEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncPureMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension TagEntity: Equatable {
    static func == (lhs: TagEntity, rhs: TagEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.name == rhs.name && lhs.name != "" && rhs.name != "" }
        return lid == rid
    }
}

extension TagEntities: Equatable {
    static func == (lhs: TagEntities, rhs: TagEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
