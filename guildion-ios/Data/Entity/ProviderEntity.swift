//
//  ProviderEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/21.
//

import Foundation
import UIKit
import URLEmbeddedView
import Hydra
import YoutubePlayer_in_WKWebView

enum ProviderType: String {
    case youtube
    case other = ""
}

class ProviderEntity {
    var type: ProviderType
    var url: String
    
    public var isExpired: Bool?
    public var imageURL: URL?
    public var pageDescription: String?
    public var pageTitle: String?
    public var pageType: String?
    public var siteName: String?
    public var sourceURL: URL?
    
    public var pixel_width: Double?
    public var pixel_height: Double?
    
    public var duration: TimeInterval?
    
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"

        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: url.count)

        guard let result = regex?.firstMatch(in: url, range: range) else {
            return nil
        }

        return (url as NSString).substring(with: result.range)
    }
    
    static func getYouTubeID(urlString: String) -> String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"

        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: urlString.count)

        guard let result = regex?.firstMatch(in: urlString, range: range) else {
            return nil
        }

        return (urlString as NSString).substring(with: result.range)
    }
    
    init(url: String) {
        self.url = url
        self.type = .other
        if let unwrappped = youtubeID {
            self.type = .youtube
            self.url = "https://www.youtube.com/watch?v=\(unwrappped)"
        }
    }
    
    func convertFileEntity() -> FileEntity {
        guard self.type == .youtube else { return FileEntity() }
        let file = FileEntity()
        file.Provider = self
        file.url = self.url
        file.format = .youtube
        if let url = self.imageURL {
            file.thumbnail = String(describing: url)
        } else {
            file.thumbnail = ""
        }
        file.name = self.pageTitle ?? ""
        file.description = self.pageDescription ?? ""
        file.pixel_width = self.pixel_width ?? 0
        file.pixel_height = self.pixel_height ?? 0
        file.duration = self.duration ?? TimeInterval(0)
        return file
    }
    
    func fetchYouTubeData() -> Promise<ProviderEntity> {
        return Promise<ProviderEntity> (in: .background) { resolve, reject, _ in
            guard self.type == .youtube else { return }
            let promises: [Promise<ProviderEntity>] = [
                self.fetchOGData(),
//                self.fetchYouTubeViewData(),
            ]
            all(promises).then(in: .main, { results in
                resolve(self)
            }).catch(in: .background, { error in
                reject(error)
            })
        }
    }
    
    func fetchOGData() -> Promise<ProviderEntity> {
        return Promise<ProviderEntity> (in: .background) { resolve, reject, _ in
            guard self.type == .youtube else { return }
            OGDataProvider.shared.cacheManager = OGDataNoCacheManager()
            OpenGraphDataDownloader.shared.fetchOGData(urlString: self.url) { result in
                switch result {
                case let .success(data, isExpired):
                    self.imageURL = data.imageUrl
                    self.pageDescription = data.pageDescription
                    self.pageTitle = data.pageTitle
                    self.pageType = data.pageType
                    self.siteName = data.siteName
                    self.sourceURL = data.sourceUrl
                    self.isExpired = isExpired
                    self.setSizeData()
                    resolve(self)
                case let .failure(error, isExpired):
                    print(error)
                    self.isExpired = isExpired
                    reject(error)
                }
            }
        }
    }
    
    func fetchYouTubeViewData() -> Promise<ProviderEntity> {
        return Promise<ProviderEntity> (in: .background) { resolve, reject, _ in
            guard self.type == .youtube, let youtubeID = self.youtubeID else { return }
            let wkytPlayerView = WKYTPlayerView()
            wkytPlayerView.load(withVideoId: youtubeID)
            wkytPlayerView.getDuration({ time, error in
                if let unwrapped_error = error {
                    reject(unwrapped_error)
                } else {
                    self.duration = time
                }
            })
        }
    }
    func fetchYouTubeViewData(wkytPlayerView: WKYTPlayerView) -> Promise<ProviderEntity> {
        return Promise<ProviderEntity> (in: .background) { resolve, reject, _ in
            guard self.type == .youtube, self.youtubeID != nil else { return }
            wkytPlayerView.getDuration({ time, error in
                if let unwrapped_error = error {
                    reject(unwrapped_error)
                } else {
                    self.duration = time
                }
            })
        }
    }
    
    func setSizeData() {
        guard let url = self.imageURL else { return }
        let image = UIImage(url: String(describing: url))
        self.pixel_width = Double(image.size.width)
        self.pixel_height = Double(image.size.height)
    }
}
