//
//  SubscriptionEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/06.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra

public enum SubscriptionStatus: String {
    case free
    case trial
    case intro
    case promo
    case regular
    case grace
    case refunded
    case expired
}

class SubscriptionEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var UserId: Int?
    public var product_id: Int?
    public var status: SubscriptionStatus = .free
    public var started_at: Date = Date.CurrentDate()
    public var canceled_at: Date = Date.CurrentDate()
    public var expired_at: Date = Date.CurrentDate()
    public var is_in_retry_billing: Bool = false
    public var is_autorenew_enabled: Bool = false
    public var is_private: Bool = false
    public var permission: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var User: UserEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.UserId <- map["UserId"]
        self.product_id <- map["product_id"]
        self.status <- (map["status"], EnumTransform<SubscriptionStatus>())
        if let dateString = map["started_at"].currentValue as? String {
            self.started_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["canceled_at"].currentValue as? String {
            self.canceled_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["expired_at"].currentValue as? String {
            self.expired_at = Date.UTCToLocal(date: dateString)
        }
        self.is_in_retry_billing <- map["is_in_retry_billing"]
        self.is_autorenew_enabled <- map["is_autorenew_enabled"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.User <- map["User"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.UserId <- map["UserId"]
        self.product_id <- map["product_id"]
        self.status <- (map["status"], EnumTransform<SubscriptionStatus>())
        if let dateString = map["started_at"].currentValue as? String {
            self.started_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["canceled_at"].currentValue as? String {
            self.canceled_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["expired_at"].currentValue as? String {
            self.expired_at = Date.UTCToLocal(date: dateString)
        }
        self.is_in_retry_billing <- map["is_in_retry_billing"]
        self.is_autorenew_enabled <- map["is_autorenew_enabled"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.User <- map["User"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.User == nil { self.User = UserEntity() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.UserId <- map["UserId"]
                self.product_id <- map["product_id"]
                self.status <- (map["status"], EnumTransform<SubscriptionStatus>())
                if let dateString = map["started_at"].currentValue as? String {
                    self.started_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["canceled_at"].currentValue as? String {
                    self.canceled_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["expired_at"].currentValue as? String {
                    self.expired_at = Date.UTCToLocal(date: dateString)
                }
                self.is_in_retry_billing <- map["is_in_retry_billing"]
                self.is_autorenew_enabled <- map["is_autorenew_enabled"]
                self.is_private <- map["is_private"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.User!.asyncMapping(map: map["User"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "UserId": self.UserId ?? 0,
            "product_id": self.product_id ?? 0,
            "status": self.status.rawValue,
            "started_at": self.started_at,
            "canceled_at": self.canceled_at,
            "expired_at": self.expired_at,
            "is_in_retry_billing": self.is_in_retry_billing,
            "is_autorenew_enabled ": self.is_autorenew_enabled ,
            "is_private": self.is_private,
            "permission": self.permission,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "User": self.User?.toJSON() ?? [],
        ]
    }
}

class SubscriptionEntities: Entities, PropertyNames, Mappable {
    var items: [SubscriptionEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension SubscriptionEntity: Equatable {
    static func == (lhs: SubscriptionEntity, rhs: SubscriptionEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.UserId == rhs.UserId && lhs.product_id == rhs.product_id }
        return lid == rid
    }
}

extension SubscriptionEntities: Equatable {
    static func == (lhs: SubscriptionEntities, rhs: SubscriptionEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
