//
//  ChannelEntryable.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/15.
//

import Foundation

protocol ChannelEntryableProtocol: class {
    var id: Int? { get set }
    var channelable_type: ChannelableType { get }
    var mute: Bool { get set }
    var messages_mute: Bool { get set }
    var mentions_mute: Bool { get set }
    var everyone_mentions_mute: Bool { get set }
    func toNewMemory() -> ChannelEntryableProtocol
}
