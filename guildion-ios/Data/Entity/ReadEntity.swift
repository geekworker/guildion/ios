//
//  ReadEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra

class ReadEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var MemberId: Int?
    public var MessageId: Int?
    public var complete: Bool = true
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Member: MemberEntity?
    public var Message: MessageEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.MessageId <- map["MessageId"]
        self.complete <- map["complete"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Message <- map["Message"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.MessageId <- map["MessageId"]
        self.complete <- map["complete"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Message <- map["Message"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Member == nil { self.Member = MemberEntity() }
        if self.Message == nil { self.Message = MessageEntity() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.MessageId <- map["MessageId"]
                self.complete <- map["complete"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Member!.asyncMapping(map: map["Member"])
        promises += self.Message!.asyncMapping(map: map["Message"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "MemberId": self.MemberId ?? 0,
            "MessageId": self.MessageId ?? 0,
            "complete": self.complete,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Member": self.Member?.toJSON() ?? [],
            "Message": self.Message?.toJSON() ?? [],
        ]
    }
}

class ReadEntities: Entities, PropertyNames, Mappable {
    var items: [ReadEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
}

extension ReadEntity: Equatable {
    static func == (lhs: ReadEntity, rhs: ReadEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.MemberId == rhs.MemberId && lhs.MessageId == rhs.MessageId
        }
        return lid == rid
    }
}

extension ReadEntities: Equatable {
    static func == (lhs: ReadEntities, rhs: ReadEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
