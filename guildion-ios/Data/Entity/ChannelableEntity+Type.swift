//
//  ChannelableEntity+Type.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/11.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

enum ChannelableType: String {
    case TextChannel = "TextChannel"
    case StreamChannel = "StreamChannel"
    case DMChannel = "DMChannel"
    case SectionChannel = "SectionChannel"
    case IMChannel = "IMChannel"
    case PlayChannel = "PlayChannel"
    case AnnouncementChannel = "AnnouncementChannel"
    case VoiceChannel = "VoiceChannel"
    case MusicChannel = "MusicChannel"
    case StreamDMChannel = "StreamDMChannel"
    
    var superChannel: ChannelableType {
        switch self {
        case .TextChannel, .AnnouncementChannel: return .TextChannel
        case .SectionChannel: return .SectionChannel
        case .DMChannel, .IMChannel: return .DMChannel
        case .StreamChannel, .PlayChannel, .StreamDMChannel, .MusicChannel, .VoiceChannel: return .StreamChannel
        }
    }
    
    var image: UIImage {
        switch self {
        case .TextChannel: return R.image.hash_tag()!.withRenderingMode(.alwaysTemplate)
        case .StreamChannel, .StreamDMChannel: return R.image.watchParty()!.withRenderingMode(.alwaysTemplate)
        case .PlayChannel: return R.image.miniPlaying()!.withRenderingMode(.alwaysTemplate)
        case .AnnouncementChannel: return R.image.announcement()!.withRenderingMode(.alwaysTemplate)
        case .VoiceChannel: return R.image.speaker()!.withRenderingMode(.alwaysTemplate)
        case .MusicChannel: return R.image.miniPlaying()!.withRenderingMode(.alwaysTemplate)//R.image.music()!.withRenderingMode(.alwaysTemplate)
        default: return UIImage()
        }
    }
    
    var string: String {
        switch self {
        case .TextChannel: return R.string.localizable.textChannel()
        case .StreamChannel, .StreamDMChannel: return R.string.localizable.streamChannel()
        case .DMChannel: return R.string.localizable.dmChannel()
        case .SectionChannel: return R.string.localizable.sectionChannel()
        case .PlayChannel: return R.string.localizable.playChannel()
        case .AnnouncementChannel: return R.string.localizable.announcementChannel()
        case .VoiceChannel: return R.string.localizable.voiceChannel()
        case .MusicChannel: return R.string.localizable.musicChannel()
        default: return ""
        }
    }
}

protocol ChannelableProtocol: class {
    var name: String { get set }
    var created_at: Date { get set }
    var index: Int { get set }
    var uid: String { get set }
    var id: Int? { get set }
    var GuildId: Int? { get set }
    var Guild: GuildEntity? { get set }
    var channelable_type: ChannelableType { get }
    func toNewMemory() -> ChannelableProtocol
}

extension ChannelableProtocol {
    func toTextChannel() -> TextChannelEntity? {
        guard channelable_type.superChannel == .TextChannel else { return nil }
        return self as? TextChannelEntity
    }
    
    func toDMChannel() -> DMChannelEntity? {
        guard channelable_type.superChannel == .DMChannel else { return nil }
        return self as? DMChannelEntity
    }
    
    func toSectionChannel() -> SectionChannelEntity? {
        guard channelable_type.superChannel == .SectionChannel else { return nil }
        return self as? SectionChannelEntity
    }
    
    func toStreamChannel() -> StreamChannelEntity? {
        guard channelable_type.superChannel == .StreamChannel else { return nil }
        return self as? StreamChannelEntity
    }
}

class Channelables: Entities, PropertyNames, Mappable {
    var items: [ChannelableProtocol] = []

    init() {
    }

    required init?(map: Map) {
        self.items <- map
    }

    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        return []
    }
    
    func sorted() -> [ChannelableProtocol] {
        return items.sorted(by: { lChannel, rChannel -> Bool in
            return lChannel.index == rChannel.index ? lChannel.created_at < rChannel.created_at : lChannel.index < rChannel.index
        })
    }
    
    func dm_sorted() -> [ChannelableProtocol] {
        return items.sorted(by: { lChannel, rChannel -> Bool in
            return lChannel.created_at < rChannel.created_at
        })
    }
    
    func toJSON() -> [[String : Any]] {
        // FIXME
        return [[:]]
    }
}
