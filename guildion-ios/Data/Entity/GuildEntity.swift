//
//  GuildEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra
import SwiftyJSON

class GuildEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var CategoryId: Int?
    public var OwnerId: Int?
    public var uid: String = ""
    public var name: String = ""
    public var description: String = ""
    public var picture_small: String = ""
    public var picture_large: String = ""
    public var active_count: Int = 0
    public var member_count: Int = 0
    public var member_request_count: Int = 0
    public var channel_count: Int = 0
    public var file_count: Int = 0
    public var movie_count: Int = 0
    public var stream_count: Int = 0
    public var folder_count: Int = 0
    public var playlist_count: Int = 0
    public var role_count: Int = 0
    public var size_byte: Int = 0
    public var locale: String = ""
    public var country_code: String = ""
    public var is_nsfw: Bool = true
    public var permission: Bool = false
    public var is_public: Bool = false
    public var is_private: Bool = false
    public var bumped_at: Date = Date.CurrentDate()
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Category: CategoryEntity?
    public var Owner: UserEntity?
    public var Tags: TagEntities?
    public var Members: MemberEntities?
    public var Folders: FolderEntities?
    public var StreamChannels: StreamChannelEntities?
    public var TextChannels: TextChannelEntities?
    public var DMChannels: DMChannelEntities?
    public var SectionChannels: SectionChannelEntities?
    public var MemberRequests: MemberRequestEntities?
    public var Channels: Channelables?
    public var Roles: RoleEntities?
    
    public var index: Int = 0 {
        didSet {
            if let unwrapped = _index {
                index_updated = index != unwrapped
            }
            _index = index
        }
    }
    private var _index: Int? = nil
    public var index_updated: Bool = false
    
    public var picture_small_image: UIImage {
        if URL(string: self.picture_small) != nil {
            return UIImage(url: self.picture_small)
        } else {
            return UIImage(url: DataConfig.Image.default_guild_image_url)
        }
    }
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.CategoryId <- map["CategoryId"]
        self.OwnerId <- map["OwnerId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.picture_small <- map["picture_small"]
        self.picture_large <- map["picture_large"]
        self.index <- map["index"]
        self.active_count <- map["active_count"]
        self.member_count <- map["member_count"]
        self.member_request_count <- map["member_request_count"]
        self.channel_count <- map["channel_count"]
        self.file_count <- map["file_count"]
        self.movie_count <- map["movie_count"]
        self.stream_count <- map["stream_count"]
        self.folder_count <- map["folder_count"]
        self.playlist_count <- map["playlist_count"]
        self.role_count <- map["role_count"]
        self.locale <- map["locale"]
        self.country_code <- map["country_code"]
        self.is_nsfw <- map["is_nsfw"]
        self.permission <- map["permission"]
        self.is_public <- map["is_public"]
        self.is_private <- map["is_private"]
        self.bumped_at <- map["bumped_at"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Category <- map["Category"]
        self.Owner <- map["Owner"]
        self.Tags = TagEntities(map: map["Tags"])
        self.Members = MemberEntities(map: map["Members"])
        self.MemberRequests = MemberRequestEntities(map: map["MemberRequests"])
        self.Folders = FolderEntities(map: map["Folders"])
        self.StreamChannels = StreamChannelEntities(map: map["StreamChannels"])
        self.TextChannels = TextChannelEntities(map: map["TextChannels"])
        self.DMChannels = DMChannelEntities(map: map["DMChannels"])
        self.SectionChannels = SectionChannelEntities(map: map["SectionChannels"])
        self.Channels = Channelables()
        self.Channels?.items = SectionChannels!.items + StreamChannels!.items + TextChannels!.items + DMChannels!.items
        self.Channels?.items = self.Channels!.sorted()
        self.Roles = RoleEntities(map: map["RoleChannels"])
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.CategoryId <- map["CategoryId"]
        self.OwnerId <- map["OwnerId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.picture_small <- map["picture_small"]
        self.picture_large <- map["picture_large"]
        self.index <- map["index"]
        self.active_count <- map["active_count"]
        self.member_count <- map["member_count"]
        self.member_request_count <- map["member_request_count"]
        self.channel_count <- map["channel_count"]
        self.file_count <- map["file_count"]
        self.movie_count <- map["movie_count"]
        self.stream_count <- map["stream_count"]
        self.folder_count <- map["folder_count"]
        self.playlist_count <- map["playlist_count"]
        self.role_count <- map["role_count"]
        self.locale <- map["locale"]
        self.country_code <- map["country_code"]
        self.is_nsfw <- map["is_nsfw"]
        self.permission <- map["permission"]
        self.is_public <- map["is_public"]
        self.is_private <- map["is_private"]
        self.bumped_at <- map["bumped_at"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Category <- map["Category"]
        self.Owner <- map["Owner"]
        self.Tags = TagEntities(map: map["Tags"])
        self.Members = MemberEntities(map: map["Members"])
        self.MemberRequests = MemberRequestEntities(map: map["MemberRequests"])
        self.Folders = FolderEntities(map: map["Folders"])
        self.StreamChannels = StreamChannelEntities(map: map["StreamChannels"])
        self.TextChannels = TextChannelEntities(map: map["TextChannels"])
        self.DMChannels = DMChannelEntities(map: map["DMChannels"])
        self.SectionChannels = SectionChannelEntities(map: map["SectionChannels"])
        self.Roles = RoleEntities(map: map["RoleChannels"])
        self.updateChannels()
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Category == nil { self.Category = CategoryEntity() }
        if self.Owner == nil { self.Owner = UserEntity() }
        if self.Tags == nil { self.Tags = TagEntities() }
        if self.Members == nil { self.Members = MemberEntities() }
        if self.MemberRequests == nil { self.MemberRequests = MemberRequestEntities() }
        if self.Folders == nil { self.Folders = FolderEntities() }
        if self.StreamChannels == nil { self.StreamChannels = StreamChannelEntities() }
        if self.TextChannels == nil { self.TextChannels = TextChannelEntities() }
        if self.DMChannels == nil { self.DMChannels = DMChannelEntities() }
        if self.SectionChannels == nil { self.SectionChannels = SectionChannelEntities() }
        if self.Roles == nil { self.Roles = RoleEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.CategoryId <- map["CategoryId"]
                self.OwnerId <- map["OwnerId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.active_count <- map["active_count"]
                self.member_count <- map["member_count"]
                self.member_request_count <- map["member_request_count"]
                self.channel_count <- map["channel_count"]
                self.file_count <- map["file_count"]
                self.movie_count <- map["movie_count"]
                self.stream_count <- map["stream_count"]
                self.folder_count <- map["folder_count"]
                self.playlist_count <- map["playlist_count"]
                self.role_count <- map["role_count"]
                self.locale <- map["locale"]
                self.country_code <- map["country_code"]
                self.is_nsfw <- map["is_nsfw"]
                self.permission <- map["permission"]
                self.is_public <- map["is_public"]
                self.is_private <- map["is_private"]
                self.bumped_at <- map["bumped_at"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Category!.asyncMapping(map: map["Category"])
        promises += self.Owner!.asyncMapping(map: map["Owner"])
        promises += self.Tags!.asyncMapping(map: map["Tags"])
        promises += self.Members!.asyncMapping(map: map["Members"])
        promises += self.MemberRequests!.asyncMapping(map: map["MemberRequests"])
        promises += self.Folders!.asyncMapping(map: map["Folders"])
        promises += self.StreamChannels!.asyncMapping(map: map["StreamChannels"])
        promises += self.TextChannels!.asyncMapping(map: map["TextChannels"])
        promises += self.DMChannels!.asyncMapping(map: map["DMChannels"])
        promises += self.SectionChannels!.asyncMapping(map: map["SectionChannels"])
        promises += self.Roles!.asyncMapping(map: map["Roles"])
        return promises
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        let promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.CategoryId <- map["CategoryId"]
                self.OwnerId <- map["OwnerId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.active_count <- map["active_count"]
                self.member_count <- map["member_count"]
                self.member_request_count <- map["member_request_count"]
                self.channel_count <- map["channel_count"]
                self.file_count <- map["file_count"]
                self.movie_count <- map["movie_count"]
                self.stream_count <- map["stream_count"]
                self.folder_count <- map["folder_count"]
                self.playlist_count <- map["playlist_count"]
                self.role_count <- map["role_count"]
                self.locale <- map["locale"]
                self.country_code <- map["country_code"]
                self.is_nsfw <- map["is_nsfw"]
                self.permission <- map["permission"]
                self.is_public <- map["is_public"]
                self.is_private <- map["is_private"]
                self.bumped_at <- map["bumped_at"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }
    
    func asyncFastMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Owner == nil { self.Owner = UserEntity() }
        if self.Members == nil { self.Members = MemberEntities() }
        if self.MemberRequests == nil { self.MemberRequests = MemberRequestEntities() }
        if self.SectionChannels == nil { self.SectionChannels = SectionChannelEntities() }
        if self.Roles == nil { self.Roles = RoleEntities() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.CategoryId <- map["CategoryId"]
                self.OwnerId <- map["OwnerId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.active_count <- map["active_count"]
                self.member_count <- map["member_count"]
                self.member_request_count <- map["member_request_count"]
                self.channel_count <- map["channel_count"]
                self.file_count <- map["file_count"]
                self.movie_count <- map["movie_count"]
                self.stream_count <- map["stream_count"]
                self.folder_count <- map["folder_count"]
                self.playlist_count <- map["playlist_count"]
                self.role_count <- map["role_count"]
                self.locale <- map["locale"]
                self.country_code <- map["country_code"]
                self.is_nsfw <- map["is_nsfw"]
                self.permission <- map["permission"]
                self.is_public <- map["is_public"]
                self.is_private <- map["is_private"]
                self.bumped_at <- map["bumped_at"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Owner!.asyncPureMapping(map: map["Owner"])
        promises += self.Members!.asyncFastMapping(map: map["Members"])
        promises += self.SectionChannels!.asyncFastMapping(map: map["SectionChannels"])
        promises += self.Roles!.asyncPureMapping(map: map["Roles"])
        return promises
    }
    
    func asyncFastSearchMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Owner == nil { self.Owner = UserEntity() }
        if self.Members == nil { self.Members = MemberEntities() }
        if self.MemberRequests == nil { self.MemberRequests = MemberRequestEntities() }
        if self.SectionChannels == nil { self.SectionChannels = SectionChannelEntities() }
        if self.Tags == nil { self.Tags = TagEntities() }
        if self.Category == nil { self.Category = CategoryEntity() }
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.CategoryId <- map["CategoryId"]
                self.OwnerId <- map["OwnerId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.active_count <- map["active_count"]
                self.member_count <- map["member_count"]
                self.member_request_count <- map["member_request_count"]
                self.channel_count <- map["channel_count"]
                self.file_count <- map["file_count"]
                self.movie_count <- map["movie_count"]
                self.stream_count <- map["stream_count"]
                self.folder_count <- map["folder_count"]
                self.playlist_count <- map["playlist_count"]
                self.role_count <- map["role_count"]
                self.locale <- map["locale"]
                self.country_code <- map["country_code"]
                self.is_nsfw <- map["is_nsfw"]
                self.permission <- map["permission"]
                self.is_public <- map["is_public"]
                self.is_private <- map["is_private"]
                self.bumped_at <- map["bumped_at"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Owner!.asyncPureMapping(map: map["Owner"])
        promises += self.Members!.asyncFastMapping(map: map["Members"])
        promises += self.MemberRequests!.asyncMapping(map: map["MemberRequests"])
        promises += self.Tags!.asyncPureMapping(map: map["Tags"])
        promises += self.Category!.asyncPureMapping(map: map["Category"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "CategoryId": self.CategoryId ?? 0,
            "OwnerId": self.OwnerId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "picture_small": self.picture_small,
            "picture_large": self.picture_large,
            "index": self.index,
            "active_count": self.active_count,
            "member_count": self.member_count,
            "channel_count": self.channel_count,
            "file_count": self.file_count,
            "movie_count": self.movie_count,
            "stream_count": self.stream_count,
            "folder_count": self.folder_count,
            "playlist_count": self.playlist_count,
            "role_count": self.role_count,
            "locale": self.locale,
            "country_code": self.country_code,
            "is_nsfw": self.is_nsfw,
            "permission": self.permission,
            "is_public": self.is_public,
            "is_private": self.is_private,
            "bumped_at": self.bumped_at,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Category": self.Category?.toJSON() ?? [],
            "Owner": self.Owner?.toJSON() ?? [],
            "Tags": self.Tags?.toJSON() ?? [],
            "Members": self.Members?.toJSON() ?? [],
            "Folders": self.Folders?.toJSON() ?? [],
            "StreamChannels": self.StreamChannels?.toJSON() ?? [],
            "TextChannels": self.TextChannels?.toJSON() ?? [],
            "DMChannels": self.DMChannels?.toJSON() ?? [],
            "SectionChannels": self.SectionChannels?.toJSON() ?? [],
            "MemberRequests": self.MemberRequests?.toJSON() ?? [],
            "Roles": self.Roles?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> GuildEntity {
        let newGuild = GuildEntity()
        newGuild.Tags = self.Tags
        newGuild.Category = self.Category
        newGuild.Owner = self.Owner
        newGuild.id = self.id
        newGuild.CategoryId = self.CategoryId
        newGuild.OwnerId = self.OwnerId
        newGuild.uid = self.uid
        newGuild.name = self.name
        newGuild.description = self.description
        newGuild.picture_small = self.picture_small
        newGuild.picture_large = self.picture_large
        newGuild.index = self.index
        newGuild.active_count = self.active_count
        newGuild.member_count = self.member_count
        newGuild.channel_count = self.channel_count
        newGuild.file_count = self.file_count
        newGuild.movie_count = self.movie_count
        newGuild.stream_count = self.stream_count
        newGuild.folder_count = self.folder_count
        newGuild.playlist_count = self.playlist_count
        newGuild.role_count = self.role_count
        newGuild.locale = self.locale
        newGuild.country_code = self.country_code
        newGuild.is_nsfw = self.is_nsfw
        newGuild.permission = self.permission
        newGuild.is_public = self.is_public
        newGuild.is_private = self.is_private
        newGuild.bumped_at = self.bumped_at
        newGuild.created_at = self.created_at
        newGuild.updated_at = self.updated_at
        return newGuild
    }
    
    func updateChannels() {
        self.Channels = Channelables()
        self.SectionChannels?.items = self.SectionChannels?.sorted() ?? []
        for section in SectionChannels?.items ?? [] { section.updateRows() }
        self.Channels?.items += SectionChannels?.items ?? []
        self.Channels?.items += StreamChannels?.items ?? []
        self.Channels?.items += TextChannels?.items ?? []
        self.Channels?.items += DMChannels?.items ?? []
        self.Channels?.items = self.Channels!.sorted()
    }
}

extension GuildEntity: WebsocketPayloadable {
    typealias Element = GuildEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "CategoryId": self.CategoryId ?? 0,
            "OwnerId": self.OwnerId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "picture_small": self.picture_small,
            "picture_large": self.picture_large,
            "active_count": self.active_count,
            "member_count": self.member_count,
            "channel_count": self.channel_count,
            "file_count": self.file_count,
            "movie_count": self.movie_count,
            "stream_count": self.stream_count,
            "folder_count": self.folder_count,
            "playlist_count": self.playlist_count,
            "role_count": self.role_count,
            "locale": self.locale,
            "country_code": self.country_code,
            "is_nsfw": self.is_nsfw,
            "permission": self.permission,
            "is_public": self.is_public,
            "is_private": self.is_private,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> GuildEntity? {
        let newGuild = GuildEntity()
        guard let id = json["id"].int, id != 0, let OwnerId = json["OwnerId"].int, OwnerId != 0 else { return nil }
        newGuild.id = id
        newGuild.CategoryId = json["CategoryId"].intValue
        newGuild.OwnerId = OwnerId
        newGuild.uid = json["uid"].stringValue
        newGuild.name = json["name"].stringValue
        newGuild.description = json["description"].stringValue
        newGuild.picture_small = json["picture_small"].stringValue
        newGuild.picture_large = json["picture_large"].stringValue
        newGuild.active_count = json["active_count"].intValue
        newGuild.member_count = json["member_count"].intValue
        newGuild.channel_count = json["channel_count"].intValue
        newGuild.file_count = json["file_count"].intValue
        newGuild.movie_count = json["movie_count"].intValue
        newGuild.stream_count = json["stream_count"].intValue
        newGuild.folder_count = json["folder_count"].intValue
        newGuild.playlist_count = json["playlist_count"].intValue
        newGuild.role_count = json["role_count"].intValue
        newGuild.locale = json["locale"].stringValue
        newGuild.country_code = json["country_code"].stringValue
        newGuild.is_nsfw = json["is_nsfw"].boolValue
        newGuild.permission = json["permission"].boolValue
        newGuild.is_public = json["is_public"].boolValue
        newGuild.is_private = json["is_private"].boolValue
        return newGuild
    }
}

class GuildEntities: Entities, PropertyNames, Mappable {
    var items: [GuildEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncPureMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastSearchMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastSearchMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
    
    func sorted() -> [GuildEntity] {
        return items.sorted(by: { lhs, rhs -> Bool in
            return lhs.index == rhs.index ? lhs.created_at < rhs.created_at : lhs.index < rhs.index
        })
    }
}

extension GuildEntity: Equatable {
    static func == (lhs: GuildEntity, rhs: GuildEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension GuildEntities: Equatable {
    static func == (lhs: GuildEntities, rhs: GuildEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}

