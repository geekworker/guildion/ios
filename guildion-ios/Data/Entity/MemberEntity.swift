//
//  MemberEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import SwiftyJSON
import Hydra

class MemberEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var UserId: Int?
    public var GuildId: Int?
    public var uid: String = ""
    public var nickname: String = ""
    public var description: String = ""
    public var picture_small: String = ""
    public var picture_large: String = ""
    public var index: Int = 0
    public var permission: Bool = false
    public var is_private: Bool = false
    public var mute: Bool = false
    public var messages_mute: Bool = false
    public var mentions_mute: Bool = false
    public var everyone_mentions_mute: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var User: UserEntity?
    public var Guild: GuildEntity?
    public var Messages: MessageEntities?
    public var Folders: FolderEntities?
    public var ReadMessages: MessageEntities?
    public var ReactionMessages: MessageEntities?
    public var StreamChannel: StreamChannelEntity?
    public var TextChannel: TextChannelEntity?
    public var DMChannel: DMChannelEntity?
    public var StreamChannelEntry: StreamChannelEntryEntity?
    public var TextChannelEntry: TextChannelEntryEntity?
    public var DMChannelEntry: DMChannelEntryEntity?
    public var Roles: RoleEntities?
    public var MemberRoles: MemberRoleEntities?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.UserId <- map["UserId"]
        self.GuildId <- map["GuildId"]
        self.uid <- map["uid"]
        self.nickname <- map["nickname"]
        self.description <- map["description"]
        self.picture_small <- map["picture_small"]
        self.picture_large <- map["picture_large"]
        self.index <- map["index"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        self.mute <- map["mute"]
        self.messages_mute <- map["messages_mute"]
        self.mentions_mute <- map["mentions_mute"]
        self.everyone_mentions_mute <- map["everyone_mentions_mute"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.User <- map["User"]
        self.Guild <- map["Guild"]
        self.Messages = MessageEntities(map: map["Messages"])
        self.Folders = FolderEntities(map: map["Folders"])
        self.ReadMessages = MessageEntities(map: map["ReadMessages"])
        self.ReactionMessages = MessageEntities(map: map["ReactionMessages"])
        self.StreamChannel = StreamChannelEntity(map: map["StreamChannel"])
        self.TextChannel = TextChannelEntity(map: map["TextChannel"])
        self.DMChannel = DMChannelEntity(map: map["DMChannel"])
        self.StreamChannelEntry = StreamChannelEntryEntity(map: map["StreamChannelEntry"])
        self.TextChannelEntry = TextChannelEntryEntity(map: map["TextChannelEntry"])
        self.DMChannelEntry = DMChannelEntryEntity(map: map["DMChannelEntry"])
        self.MemberRoles = MemberRoleEntities.init(map: map["MemberRoles"])
        self.Roles = RoleEntities.init(map: map["Roles"])
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.UserId <- map["UserId"]
        self.GuildId <- map["GuildId"]
        self.uid <- map["uid"]
        self.nickname <- map["nickname"]
        self.description <- map["description"]
        self.picture_small <- map["picture_small"]
        self.picture_large <- map["picture_large"]
        self.index <- map["index"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        self.mute <- map["mute"]
        self.messages_mute <- map["messages_mute"]
        self.mentions_mute <- map["mentions_mute"]
        self.everyone_mentions_mute <- map["everyone_mentions_mute"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.User <- map["User"]
        self.Guild <- map["Guild"]
        self.Messages = MessageEntities(map: map["Messages"])
        self.Folders = FolderEntities(map: map["Folders"])
        self.ReadMessages = MessageEntities(map: map["ReadMessages"])
        self.ReactionMessages = MessageEntities(map: map["ReactionMessages"])
        self.StreamChannel = StreamChannelEntity(map: map["StreamChannel"])
        self.TextChannel = TextChannelEntity(map: map["TextChannel"])
        self.DMChannel = DMChannelEntity(map: map["DMChannel"])
        self.StreamChannelEntry = StreamChannelEntryEntity(map: map["StreamChannelEntry"])
        self.TextChannelEntry = TextChannelEntryEntity(map: map["TextChannelEntry"])
        self.DMChannelEntry = DMChannelEntryEntity(map: map["DMChannelEntry"])
        self.MemberRoles = MemberRoleEntities.init(map: map["MemberRoles"])
        self.Roles = RoleEntities.init(map: map["Roles"])
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.User == nil { self.User = UserEntity() }
        if self.Guild == nil { self.Guild = GuildEntity() }
        if self.Messages == nil { self.Messages = MessageEntities() }
        if self.Folders == nil { self.Folders = FolderEntities() }
        if self.ReadMessages == nil { self.ReadMessages = MessageEntities() }
        if self.ReactionMessages == nil { self.ReactionMessages = MessageEntities() }
        if self.MemberRoles == nil { self.MemberRoles = MemberRoleEntities() }
        if self.Roles == nil { self.Roles = RoleEntities() }
        if self.StreamChannel == nil { self.StreamChannel = StreamChannelEntity() }
        if self.TextChannel == nil { self.TextChannel = TextChannelEntity() }
        if self.DMChannel == nil { self.DMChannel = DMChannelEntity() }
        if self.StreamChannelEntry == nil { self.StreamChannelEntry = StreamChannelEntryEntity() }
        if self.TextChannelEntry == nil { self.TextChannelEntry = TextChannelEntryEntity() }
        if self.DMChannelEntry == nil { self.DMChannelEntry = DMChannelEntryEntity() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.UserId <- map["UserId"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.nickname <- map["nickname"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.mute <- map["mute"]
                self.messages_mute <- map["messages_mute"]
                self.mentions_mute <- map["mentions_mute"]
                self.everyone_mentions_mute <- map["everyone_mentions_mute"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.User!.asyncMapping(map: map["User"])
        promises += self.Guild!.asyncMapping(map: map["Guild"])
        promises += self.Messages!.asyncMapping(map: map["Messages"])
        promises += self.Folders!.asyncMapping(map: map["Folders"])
        promises += self.ReadMessages!.asyncMapping(map: map["ReadMessages"])
        promises += self.ReactionMessages!.asyncMapping(map: map["ReactionMessages"])
        promises += self.MemberRoles!.asyncMapping(map: map["MemberRoles"])
        promises += self.Roles!.asyncMapping(map: map["Roles"])
        promises += self.TextChannel!.asyncMapping(map: map["TextChannel"])
        promises += self.DMChannel!.asyncMapping(map: map["DMChannel"])
        promises += self.StreamChannel!.asyncMapping(map: map["StreamChannel"])
        promises += self.TextChannelEntry!.asyncMapping(map: map["TextChannelEntry"])
        promises += self.DMChannelEntry!.asyncMapping(map: map["DMChannelEntry"])
        promises += self.StreamChannelEntry!.asyncMapping(map: map["StreamChannelEntry"])
        
        return promises
    }
    
    func asyncFastMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.MemberRoles == nil { self.MemberRoles = MemberRoleEntities() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.UserId <- map["UserId"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.nickname <- map["nickname"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.mute <- map["mute"]
                self.messages_mute <- map["messages_mute"]
                self.mentions_mute <- map["mentions_mute"]
                self.everyone_mentions_mute <- map["everyone_mentions_mute"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.MemberRoles!.asyncFastMapping(map: map["MemberRoles"])
        return promises
    }
    
    func asyncGuildMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guild == nil { self.Guild = GuildEntity() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.UserId <- map["UserId"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.nickname <- map["nickname"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.mute <- map["mute"]
                self.messages_mute <- map["messages_mute"]
                self.mentions_mute <- map["mentions_mute"]
                self.everyone_mentions_mute <- map["everyone_mentions_mute"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Guild!.asyncPureMapping(map: map["Guild"])
        return promises
    }
    
    func asyncChannelMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.TextChannelEntry == nil { self.TextChannelEntry = TextChannelEntryEntity() }
        if self.StreamChannelEntry == nil { self.StreamChannelEntry = StreamChannelEntryEntity() }
        if self.DMChannelEntry == nil { self.DMChannelEntry = DMChannelEntryEntity() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.UserId <- map["UserId"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.nickname <- map["nickname"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.mute <- map["mute"]
                self.messages_mute <- map["messages_mute"]
                self.mentions_mute <- map["mentions_mute"]
                self.everyone_mentions_mute <- map["everyone_mentions_mute"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.TextChannelEntry!.asyncPureMapping(map: map["TextChannelEntry"])
        promises += self.StreamChannelEntry!.asyncPureMapping(map: map["StreamChannelEntry"])
        promises += self.DMChannelEntry!.asyncPureMapping(map: map["DMChannelEntry"])
        return promises
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        
        let promises = [
            Promise<Void> (in: .background) { resolve, _, _ in
                self.id <- map["id"]
                self.UserId <- map["UserId"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.nickname <- map["nickname"]
                self.description <- map["description"]
                self.picture_small <- map["picture_small"]
                self.picture_large <- map["picture_large"]
                self.index <- map["index"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.mute <- map["mute"]
                self.messages_mute <- map["messages_mute"]
                self.mentions_mute <- map["mentions_mute"]
                self.everyone_mentions_mute <- map["everyone_mentions_mute"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "UserId": self.UserId ?? 0,
            "GuildId": self.GuildId ?? 0,
            "uid": self.uid,
            "nickname": self.nickname,
            "description": self.description,
            "picture_small": self.picture_small,
            "picture_large": self.picture_large,
            "index": self.index,
            "permission": self.permission,
            "is_private": self.is_private,
            "mute": self.mute,
            "messages_mute": self.messages_mute,
            "mentions_mute": self.mentions_mute,
            "everyone_mentions_mute": self.everyone_mentions_mute,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "User": self.User?.toJSON() ?? [],
            "Guild": self.Guild?.toJSON() ?? [],
            "Messages": self.Messages?.toJSON() ?? [],
            "Folders": self.Folders?.toJSON() ?? [],
            "ReadMessages": self.ReadMessages?.toJSON() ?? [],
            "ReactionMessages": self.ReactionMessages?.toJSON() ?? [],
            "StreamChannel": self.StreamChannel?.toJSON() ?? [],
            "TextChannel": self.TextChannel?.toJSON() ?? [],
            "DMChannel": self.DMChannel?.toJSON() ?? [],
            "MemberRoles": self.MemberRoles?.toJSON() ?? [],
            "Roles": self.Roles?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> MemberEntity {
        let new = MemberEntity()
        new.id = self.id
        new.GuildId = self.GuildId
        new.Guild = self.Guild?.toNewMemory()
        new.UserId = self.UserId
        new.uid = self.uid
        new.nickname = self.nickname
        new.mute = self.mute
        new.messages_mute = self.messages_mute
        new.mentions_mute = self.mentions_mute
        new.everyone_mentions_mute = self.everyone_mentions_mute
        new.description = self.description
        new.picture_small = self.picture_small
        new.picture_large = self.picture_large
        new.index = self.index
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
    
    func getCurrentChannelEntry(_ channel: ChannelableProtocol) -> ChannelEntryableProtocol? {
        switch channel.channelable_type.superChannel {
        case .StreamChannel: return self.StreamChannelEntry
        case .TextChannel: return self.TextChannelEntry
        case .DMChannel: return self.DMChannelEntry
        default: return nil
        }
    }
}

class MemberEntities: Entities, PropertyNames, Mappable {
    var items: [MemberEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncPureMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncGuildMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncGuildMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncChannelMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncChannelMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return self.items.compactMap { $0.toJSON() }
    }
    
    func toNewMemoryJSON() -> [[String: Any]] {
        return self.items.compactMap { $0.toNewMemory().toJSON() }
    }
    
    func toNewMemory() -> MemberEntities {
        let entities = MemberEntities()
        entities.items = self.items.compactMap { $0.toNewMemory() }
        return entities
    }
}

extension MemberEntity: WebsocketPayloadable {
    typealias Element = MemberEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "UserId": self.UserId ?? 0,
            "GuildId": self.GuildId ?? 0,
            "uid": self.uid,
            "nickname": self.nickname,
            "description": self.description,
            "picture_small": self.picture_small,
            "picture_large": self.picture_large,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> MemberEntity? {
        let member = MemberEntity()
        guard let id = json["id"].number, id != 0, let UserId = json["UserId"].number, UserId != 0, let GuildId = json["GuildId"].number, GuildId != 0 else { return nil }
        member.id = Int(truncating: id)
        member.UserId = Int(truncating: UserId)
        member.GuildId = Int(truncating: GuildId)
        member.uid = json["uid"].stringValue
        member.nickname = json["nickname"].stringValue
        member.description = json["description"].stringValue
        member.picture_small = json["picture_small"].stringValue
        member.picture_large = json["picture_large"].stringValue
        return member
    }
}

extension MemberEntity: Equatable {
    static func == (lhs: MemberEntity, rhs: MemberEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension MemberEntities: Equatable {
    static func == (lhs: MemberEntities, rhs: MemberEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
