//
//  SectionChannelEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra
import SwiftyJSON

class SectionChannelEntity: Entity, PropertyNames, Mappable, ChannelableProtocol, Accordionable {
    public var id: Int?
    public var GuildId: Int?
    public var uid: String = ""
    public var name: String = ""
    public var description: String = ""
    public var index: Int = 0 {
        didSet {
            if let unwrapped = _index {
                index_updated = index != unwrapped
            }
            _index = index
        }
    }
    public var channel_count: Int = 0
    public var is_empty: Bool = false
    public var permission: Bool = false
    public var is_private: Bool = false
    public var is_dm: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Guild: GuildEntity?
    public var Channels: Channelables?
    public var TextChannels: TextChannelEntities?
    public var StreamChannels: StreamChannelEntities?
    public var DMChannels: DMChannelEntities?
    public var rows: [SectionChannelRowable] = []
    var channelable_type: ChannelableType = .SectionChannel
    var collapsed: Bool = false
    
    private var _index: Int? = nil
    public var index_updated: Bool = false
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.index <- map["index"]
        self.channel_count <- map["channel_count"]
        self.is_empty <- map["is_empty"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        self.is_dm <- map["is_dm"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.StreamChannels = StreamChannelEntities(map: map["StreamChannels"])
        self.TextChannels = TextChannelEntities(map: map["TextChannels"])
        self.DMChannels = DMChannelEntities(map: map["DMChannels"])
        self.Channels = Channelables()
        self.Channels?.items = StreamChannels!.items + TextChannels!.items + DMChannels!.items
        self.Channels?.items = is_dm ? self.Channels!.dm_sorted() : self.Channels!.sorted()
        self.updateRows()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.GuildId <- map["GuildId"]
        self.uid <- map["uid"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.index <- map["index"]
        self.channel_count <- map["channel_count"]
        self.is_empty <- map["is_empty"]
        self.permission <- map["permission"]
        self.is_private <- map["is_private"]
        self.is_dm <- map["is_dm"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Guild <- map["Guild"]
        self.StreamChannels = StreamChannelEntities(map: map["StreamChannels"])
        self.TextChannels = TextChannelEntities(map: map["TextChannels"])
        self.DMChannels = DMChannelEntities(map: map["DMChannels"])
        self.Channels = Channelables()
        self.Channels?.items = StreamChannels!.items + TextChannels!.items + DMChannels!.items
        self.Channels?.items = self.Channels!.sorted()
        self.updateRows()
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Guild == nil { self.Guild = GuildEntity() }
        if self.StreamChannels == nil { self.StreamChannels = StreamChannelEntities() }
        if self.TextChannels == nil { self.TextChannels = TextChannelEntities() }
        if self.DMChannels == nil { self.DMChannels = DMChannelEntities() }
        if self.Channels == nil { self.Channels = Channelables() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.index <- map["index"]
                self.channel_count <- map["channel_count"]
                self.is_empty <- map["is_empty"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.is_dm <- map["is_dm"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.Guild!.asyncMapping(map: map["Guild"])
        promises += self.StreamChannels!.asyncMapping(map: map["StreamChannels"])
        promises += self.TextChannels!.asyncMapping(map: map["TextChannels"])
        promises += self.DMChannels!.asyncMapping(map: map["DMChannels"])
        return promises
    }
    
    func asyncFastMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.StreamChannels == nil { self.StreamChannels = StreamChannelEntities() }
        if self.TextChannels == nil { self.TextChannels = TextChannelEntities() }
        if self.DMChannels == nil { self.DMChannels = DMChannelEntities() }
        if self.Channels == nil { self.Channels = Channelables() }
        
        var promises = [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.GuildId <- map["GuildId"]
                self.uid <- map["uid"]
                self.name <- map["name"]
                self.description <- map["description"]
                self.index <- map["index"]
                self.channel_count <- map["channel_count"]
                self.is_empty <- map["is_empty"]
                self.permission <- map["permission"]
                self.is_private <- map["is_private"]
                self.is_dm <- map["is_dm"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        promises += self.StreamChannels!.asyncFastMapping(map: map["StreamChannels"])
        promises += self.TextChannels!.asyncFastMapping(map: map["TextChannels"])
        promises += self.DMChannels!.asyncFastMapping(map: map["DMChannels"])
        return promises
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "index": self.index,
            "channel_count": self.channel_count,
            "is_empty": self.is_empty,
            "permission": self.permission,
            "is_private": self.is_private,
            "is_dm": self.is_dm,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Guild": self.Guild?.toJSON() ?? [],
            "TextChannels": self.TextChannels?.toJSON() ?? [],
            "StreamChannels": self.StreamChannels?.toJSON() ?? [],
            "DMChannels": self.DMChannels?.toJSON() ?? [],
        ]
    }
    
    func updateRows() {
        self.rows = []
        self.Channels = Channelables()
        self.Channels?.items = StreamChannels!.items + TextChannels!.items + DMChannels!.items
        self.Channels?.items = self.Channels!.sorted()
        guard let channels = Channels else { return }
        for channel in channels.sorted().enumerated() {
            channel.element.index = channel.offset
            switch channel.element.channelable_type.superChannel {
            case .StreamChannel:
                guard let streamChannel = channel.element as? StreamChannelEntity else { break }
                self.rows.append(streamChannel)
                if let stream = streamChannel.LiveStream, let participants = stream.Participants {
                    if stream.mode != .other {
                        self.rows.append(stream)
                    }
                    self.rows += participants.items.enumerated().compactMap({ participant in
                        participant.element.index = streamChannel.index + rows.filter({ $0.sectionChannelRowType == .Participant || $0.sectionChannelRowType == .Stream }).count
                        return participant.element
                    })
                }
            case .DMChannel:
                guard let dmChannel = channel.element as? DMChannelEntity else { break }
                dmChannel.index += rows.filter({ $0.sectionChannelRowType == .Participant || $0.sectionChannelRowType == .Stream }).count
                self.rows.append(dmChannel)
            case .TextChannel:
                guard let textChannel = channel.element as? TextChannelEntity else { break }
                textChannel.index += rows.filter({ $0.sectionChannelRowType == .Participant || $0.sectionChannelRowType == .Stream }).count
                self.rows.append(textChannel)
                
            default: break
            }
        }
    }
    
    func toNewMemory() -> ChannelableProtocol {
        let new = SectionChannelEntity()
        new.id = self.id
        new.GuildId = self.GuildId
        new.uid = self.uid
        new.name = self.name
        new.index = self.index
        new.channel_count = self.channel_count
        new.is_empty = self.is_empty
        new.permission = self.permission
        new.is_private = self.is_private
        new.is_dm = self.is_dm
        new.description = self.description
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }

    func toNewMemorySection() -> SectionChannelEntity {
        let new = SectionChannelEntity()
        new.id = self.id
        new.GuildId = self.GuildId
        new.uid = self.uid
        new.name = self.name
        new.index = self.index
        new.channel_count = self.channel_count
        new.is_empty = self.is_empty
        new.permission = self.permission
        new.is_private = self.is_private
        new.is_dm = self.is_dm
        new.description = self.description
        new.permission = self.permission
        new.is_private = self.is_private
        new.created_at = self.created_at
        new.updated_at = self.updated_at
        return new
    }
}

extension SectionChannelEntity: WebsocketPayloadable {
    typealias Element = SectionChannelEntity
    func toWebsocketPayload() -> JSON {
        let json: JSON =  [
            "id": self.id ?? 0,
            "GuildId": self.GuildId ?? 0,
            "uid": self.uid,
            "name": self.name,
            "description": self.description,
            "index": self.index,
            "channel_count": self.channel_count,
            "is_empty": self.is_empty,
            "permission": self.permission,
            "is_private": self.is_private,
            "is_dm": self.is_dm,
        ]
        return json
    }
    
    static func jsonable(_ json: JSON) -> SectionChannelEntity? {
        let new = SectionChannelEntity()
        guard let id = json["id"].int, id != 0 else { return nil }
        new.id = id
        new.GuildId = json["GuildId"].intValue
        new.uid = json["uid"].stringValue
        new.name = json["name"].stringValue
        new.description = json["description"].stringValue
        new.channel_count = json["channel_count"].intValue
        new.index = json["index"].intValue
        new.is_empty = json["is_empty"].boolValue
        new.is_dm = json["is_dm"].boolValue
        new.permission = json["permission"].boolValue
        new.is_private = json["is_private"].boolValue
        return new
    }
}

class SectionChannelEntities: Entities, PropertyNames, Mappable {
    var sort_mode: Bool = true
    var items: [SectionChannelEntity] = [] {
        didSet {
            guard sort_mode else { return }
            self.items = self.sorted()
        }
    }
    var visibleItems: [SectionChannelEntity] {
        return items.filter { !$0.collapsed }
    }
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
        self.items = self.sorted()
    }
    
    func mapping(map: Map) {
        self.items <- map
        self.items = self.sorted()
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
    
    func sorted() -> [SectionChannelEntity] {
        return items.sorted(by: { lChannel, rChannel -> Bool in
            return lChannel.index == rChannel.index ? lChannel.created_at < rChannel.created_at : lChannel.index < rChannel.index
        })
    }
    
    func channels() -> [ChannelableProtocol] {
        return Array(self.compactMap({ $0.Channels?.items }).joined())
    }
}

extension SectionChannelEntity: Equatable {
    static func == (lhs: SectionChannelEntity, rhs: SectionChannelEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return false }
        return lid == rid
    }
}

extension SectionChannelEntities: Equatable {
    static func == (lhs: SectionChannelEntities, rhs: SectionChannelEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
