//
//  MemberRequestEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Foundation
import ObjectMapper
import Hydra

class MemberRequestEntity: NSObject, Entity, PropertyNames, Mappable {
    public var id: Int?
    public var VoterId: Int?
    public var GuildId: Int?
    public var is_accepted: Bool = false
    public var is_private: Bool = false
    public var permission: Bool = false
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Voter: UserEntity?
    public var Guild: GuildEntity?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.VoterId <- map["VoterId"]
        self.GuildId <- map["GuildId"]
        self.is_accepted <- map["is_accepted"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Voter <- map["Voter"]
        self.Guild <- map["Guild"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.VoterId <- map["VoterId"]
        self.GuildId <- map["GuildId"]
        self.is_accepted <- map["is_accepted"]
        self.is_private <- map["is_private"]
        self.permission <- map["permission"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Voter <- map["Voter"]
        self.Guild <- map["Guild"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Voter == nil { self.Voter = UserEntity() }
        if self.Guild == nil { self.Guild = GuildEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.VoterId <- map["VoterId"]
                self.GuildId <- map["GuildId"]
                self.is_accepted <- map["is_accepted"]
                self.is_private <- map["is_private"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        + self.Voter!.asyncMapping(map: map["Voter"])
        + self.Guild!.asyncMapping(map: map["Guild"])
    }
    
    func asyncFastMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Voter == nil { self.Voter = UserEntity() }
        if self.Guild == nil { self.Guild = GuildEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.VoterId <- map["VoterId"]
                self.GuildId <- map["GuildId"]
                self.is_accepted <- map["is_accepted"]
                self.is_private <- map["is_private"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        + self.Voter!.asyncPureMapping(map: map["Voter"])
        + self.Guild!.asyncPureMapping(map: map["Guild"])
    }
    
    func asyncPureMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.VoterId <- map["VoterId"]
                self.GuildId <- map["GuildId"]
                self.is_accepted <- map["is_accepted"]
                self.is_private <- map["is_private"]
                self.permission <- map["permission"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "VoterId": self.VoterId ?? 0,
            "GuildId": self.GuildId ?? 0,
            "is_accepted": self.is_accepted,
            "is_private": self.is_private,
            "permission": self.permission,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Voter": self.Voter?.toJSON() ?? [],
            "Guild": self.Guild?.toJSON() ?? []
        ]
    }
}

class MemberRequestEntities: NSObject, Entities, PropertyNames, Mappable {
    var items: [MemberRequestEntity] = []
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncPureMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncPureMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return compactMap { $0.toJSON() }
    }
}

