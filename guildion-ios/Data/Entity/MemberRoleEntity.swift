//
//  MemberRoleEntity.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import ObjectMapper
import Hydra

class MemberRoleEntity: Entity, PropertyNames, Mappable {
    public var id: Int?
    public var MemberId: Int?
    public var RoleId: Int?
    public var position: String = ""
    public var priority: Int = 0
    public var created_at: Date = Date.CurrentDate()
    public var updated_at: Date = Date.CurrentDate()
    public var Member: MemberEntity?
    public var Role: RoleEntity?
    
    init() {
    }
    
    required init?(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.RoleId <- map["RoleId"]
        self.position <- map["position"]
        self.priority <- map["priority"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Role <- map["Role"]
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.MemberId <- map["MemberId"]
        self.RoleId <- map["RoleId"]
        self.position <- map["position"]
        self.priority <- map["priority"]
        if let dateString = map["created_at"].currentValue as? String {
            self.created_at = Date.UTCToLocal(date: dateString)
        }
        if let dateString = map["updated_at"].currentValue as? String {
            self.updated_at = Date.UTCToLocal(date: dateString)
        }
        self.Member <- map["Member"]
        self.Role <- map["Role"]
    }
    
    func asyncMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Member == nil { self.Member = MemberEntity() }
        if self.Role == nil { self.Role = RoleEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.RoleId <- map["RoleId"]
                self.position <- map["position"]
                self.priority <- map["priority"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        + self.Member!.asyncMapping(map: map["Member"])
        + self.Role!.asyncMapping(map: map["Role"])
    }
    
    func asyncFastMapping(map _map: Map, shouldConvert: Bool = true) -> [Promise<Void>] {
        guard _map.isKeyPresent else { return [] }
        var map = _map
        if shouldConvert, let json = map.JSON[map.currentKey ?? ""] as? [String: Any] {
            map = Map.init(mappingType: .fromJSON, JSON: json)
        }
        if self.Role == nil { self.Role = RoleEntity() }
        return [
            Promise<Void> { resolve, _, _ in
                self.id <- map["id"]
                self.MemberId <- map["MemberId"]
                self.RoleId <- map["RoleId"]
                self.position <- map["position"]
                self.priority <- map["priority"]
                if let dateString = map["created_at"].currentValue as? String {
                    self.created_at = Date.UTCToLocal(date: dateString)
                }
                if let dateString = map["updated_at"].currentValue as? String {
                    self.updated_at = Date.UTCToLocal(date: dateString)
                }
                resolve(())
            },
        ]
        + self.Role!.asyncPureMapping(map: map["Role"])
    }
    
    func toJSON() -> [String : Any] {
        return [
            "id": self.id ?? 0,
            "MemberId": self.MemberId ?? 0,
            "RoleId": self.RoleId ?? 0,
            "position": self.position,
            "priority": self.priority,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "Member": self.Member?.toJSON() ?? [],
            "Role": self.Role?.toJSON() ?? [],
        ]
    }
    
    func toNewMemory() -> MemberRoleEntity {
        let entity = MemberRoleEntity()
        entity.id = self.id
        entity.Member = self.Member?.toNewMemory()
        entity.MemberId = self.MemberId
        entity.Role = self.Role?.toNewMemory()
        entity.RoleId = self.RoleId
        entity.position = self.position
        entity.priority = self.priority
        return entity
    }
}

class MemberRoleEntities: Entities, PropertyNames, Mappable {
    var items: [MemberRoleEntity] = []
    
    init() {
    }
    
    required init?(map: Map) {
        self.items <- map
    }
    
    func mapping(map: Map) {
        self.items <- map
    }
    
    func sorted() -> [MemberRoleEntity] {
        return items.sorted(by: { lhs, rhs -> Bool in
            return lhs.priority > rhs.priority
        })
    }
    
    func asyncMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func asyncFastMapping(map: Map) -> [Promise<Void>] {
        guard map.isKeyPresent else { return [] }
        guard let json = map.JSON[map.currentKey ?? ""] as? NSArray else { return [] }
        self.items = []
        return json.compactMap({
            let entity = Element()
            self.items.append(entity)
            let map = Map.init(mappingType: .fromJSON, JSON: ($0 as? [String: Any] ?? [:]))
            _ = map["id"]
            return entity.asyncFastMapping(map: map, shouldConvert: false)
        }).reduce([] as [Promise<Void>], +)
    }
    
    func toJSON() -> [[String: Any]] {
        return items.compactMap { $0.toJSON() }
    }
    
    func toRoles() -> RoleEntities {
        let entities = RoleEntities()
        entities.items = items.compactMap({ $0.Role })
        return entities
    }
}

extension MemberRoleEntity: Equatable {
    static func == (lhs: MemberRoleEntity, rhs: MemberRoleEntity) -> Bool{
        guard let lid = lhs.id, let rid = rhs.id else { return lhs.MemberId == rhs.MemberId && lhs.RoleId == rhs.RoleId }
        return lid == rid
    }
}

extension MemberRoleEntities: Equatable {
    static func == (lhs: MemberRoleEntities, rhs: MemberRoleEntities) -> Bool {
        guard lhs.count == rhs.count else { return false }
        let results: [Bool] = lhs.items.enumerated().compactMap {
            return $0.element == rhs[safe: $0.offset]
        }
        return results.filter { $0 }.count == lhs.count
    }
}
