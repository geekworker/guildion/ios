//
//  MessageRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra
import RxSwift

class MessageRepository: RepositoryImpl {
    static var shared: MessageRepository = MessageRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?
    
    var awsHandler = AWSHandler()

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func getMessage(uid: String) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            self.messageDataStore?.getMessage(
                uid: uid
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentMessages(channel: MessageableEntity, limit: Int, is_static: Bool) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            guard let messageable_id = channel.messageable_id, let messageable_type = channel.messageable_type?.rawValue else { return }
            self.messageDataStore?.getCurrentMessages(
                messageable_type: messageable_type,
                messageable_id: messageable_id,
                limit: limit,
                is_static: is_static
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementMessages(channel: MessageableEntity, limit: Int, last_id: Int, is_static: Bool) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            guard let messageable_id = channel.messageable_id, let messageable_type = channel.messageable_type?.rawValue else { return }
            self.messageDataStore?.getIncrementMessages(
                messageable_type: messageable_type,
                messageable_id: messageable_id,
                limit: limit,
                last_id: last_id,
                is_static: is_static
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementMessages(channel: MessageableEntity, limit: Int, first_id: Int, is_static: Bool) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            guard let messageable_id = channel.messageable_id, let messageable_type = channel.messageable_type?.rawValue else { return }
            self.messageDataStore?.getDecrementMessages(
                messageable_type: messageable_type,
                messageable_id: messageable_id,
                limit: limit,
                first_id: first_id,
                is_static: is_static
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func create(message: MessageEntity, onNextProgress: @escaping ((_ progress: CGFloat) -> Void)) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            
            // MARK: progress breakdown
            // 1. start
            // 2. upload file url
            // 3. upload file thumbnail
            // 4. send
            // 5. finish
            let progressCount = CGFloat(5)
            let progressOffset = CGFloat(1) / progressCount
            
            onNextProgress(progressOffset * 1)
                
            var promises: [Promise<String>] = []
            var t_promises: [Promise<String>] = []
            if let files = message.Files {
                for file in files.items {
                    if let url = URL(string: file.url), !url.uploaded, file.format != .youtube && !file.format.content_type.is_movie {
                        promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type, onProgressUpdate: {
                            file.setProgress($0)
                        }))
                    } else if let url = URL(string: file.url), !url.uploaded, file.format != .youtube && file.format.content_type.is_movie {
                        promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type, onProgressUpdate: {
                            file.setProgress($0)
                        })) /* baseConvertMovie */
                    } else {
                        promises.append(Promise<String> { resolve, reject, _ in resolve(file.url) })
                    }
                    
                    if let url = URL(string: file.thumbnail), !url.uploaded, file.format.content_type.is_movie {
                        t_promises.append(self.awsHandler.uploadUserImage(url))
                    } else {
                        t_promises.append(Promise<String> { resolve, reject, _ in resolve(file.thumbnail) })
                    }
                }
            }
            
            all(promises).then(in: .background, { urls in
                onNextProgress(progressOffset * 2)
                
                for url in urls.enumerated() {
                    guard let file = message.Files?[safe: url.offset] else { break }
                    file.url = url.element
                }
                all(t_promises).then(in: .background, { t_urls in
                    
                    onNextProgress(progressOffset * 3)
                    for t_url in t_urls.enumerated() {
                        guard let file = message.Files?[safe: t_url.offset] else { break }
                        
                        if file.format.content_type.is_movie {
                            file.thumbnail = t_url.element
                        } else if file.format != .youtube {
                            file.thumbnail = file.url
                        }
                    }
                    onNextProgress(progressOffset * 4)
                    self.messageDataStore?.create(
                        message: message
                    ).then(in: .background, { result in
                        resolve(result)
                        onNextProgress(progressOffset * 5)
                    }).catch(in: .background, { err in
                        reject(err)
                    })
                }).catch(in: .background, { err in
                    reject(err)
                })
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func update(message: MessageEntity, onNextProgress: @escaping ((_ progress: CGFloat) -> Void)) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            
            // MARK: progress breakdown
            // 1. start
            // 2. upload file url
            // 3. upload file thumbnail
            // 4. send
            // 5. finish
            let progressCount = CGFloat(5)
            let progressOffset = CGFloat(1) / progressCount
            
            onNextProgress(progressOffset * 1)
                
            var promises: [Promise<String>] = []
            var t_promises: [Promise<String>] = []
            if let files = message.Files {
                for file in files.items {
                    if let url = URL(string: file.url), !url.uploaded, file.format != .youtube, file.id != nil, file.id != 0 {
                        promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type))
                    } else {
                        promises.append(Promise<String> { resolve, reject, _ in resolve(file.url) })
                    }
                    
                    if let url = URL(string: file.thumbnail), !url.uploaded, file.format.content_type.is_movie, file.id != nil, file.id != 0 {
                        t_promises.append(self.awsHandler.uploadUserImage(url))
                    } else {
                        t_promises.append(Promise<String> { resolve, reject, _ in resolve(file.thumbnail) })
                    }
                }
            }
            
            all(promises).then(in: .background, { urls in
                onNextProgress(progressOffset * 2)
                
                for url in urls.enumerated() {
                    guard let file = message.Files?[safe: url.offset] else { break }
                    file.url = url.element
                }
                all(t_promises).then(in: .background, {t_urls in
                    
                    onNextProgress(progressOffset * 3)
                    for t_url in t_urls.enumerated() {
                        guard let file = message.Files?[safe: t_url.offset] else { break }
                        
                        if file.format.content_type.is_movie {
                            file.thumbnail = t_url.element
                        } else if file.format != .youtube {
                            file.thumbnail = file.url
                        }
                    }
                    onNextProgress(progressOffset * 4)
                    self.messageDataStore?.update(
                        message: message
                    ).then(in: .background, { result in
                        resolve(result)
                        onNextProgress(progressOffset * 5)
                    }).catch(in: .background, { err in
                        reject(err)
                    })
                })
            })
        }
    }
    
    func update(message: MessageEntity, is_static: Bool) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            self.messageDataStore?.update(
                message: message,
                is_static: is_static
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(message: MessageEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.messageDataStore?.delete(
                message: message
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(reaction: ReactionEntity) -> Promise<ReactionEntity> {
        return Promise<ReactionEntity> (in: .background) { resolve, reject, _ in
            self.messageDataStore?.create(
                reaction: reaction
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func reads(messages: MessageEntities) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            self.messageDataStore?.reads(
                messages: messages
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func unreads(messages: MessageEntities) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            self.messageDataStore?.unreads(
                messages: messages
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
}
