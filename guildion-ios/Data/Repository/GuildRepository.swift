//
//  GuildRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class GuildRepository: RepositoryImpl {
    static var shared: GuildRepository = GuildRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?
    
    var awsHandler = AWSHandler()

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func getUserGuilds(limit: Int, offset: Int) -> Promise<GuildEntities> {
        return Promise<GuildEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getUserGuilds(
                offset: offset,
                limit: limit
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getGuild(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getGuild(
                uid: uid
            ).then(in: .background, { result in
                result.updateChannels()
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getGuildWelcome(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getGuildWelcome(
                uid: uid
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getGuildChannel(guild_uid: String, channel_uid: String) -> Promise<(guild: GuildEntity, channel: MessageableEntity, member: MemberEntity)> {
        return Promise<(guild: GuildEntity, channel: MessageableEntity, member: MemberEntity)> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getGuildChannel(
                guild_uid: guild_uid,
                channel_uid: channel_uid
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getGuildChannels(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getGuildChannels(
                uid: uid
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getGuildRoles(uid: String) -> Promise<RoleEntities> {
        return Promise<RoleEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getGuildRoles(
                uid: uid
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getGuildMembers(guild: GuildEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getGuildMembers(
                guild: guild
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentGuildMembers(guild: GuildEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getCurrentGuildMembers(
                guild: guild,
                offset: offset,
                limit: limit
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementGuildMembers(guild: GuildEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getIncrementGuildMembers(
                guild: guild,
                offset: offset,
                limit: limit,
                last_id: last_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementGuildMembers(guild: GuildEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getDecrementGuildMembers(
                guild: guild,
                offset: offset,
                limit: limit,
                first_id: first_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentMemberRequests(guild: GuildEntity, offset: Int, limit: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getCurrentMemberRequests(
                guild: guild,
                offset: offset,
                limit: limit
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementMemberRequests(guild: GuildEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getIncrementMemberRequests(
                guild: guild,
                offset: offset,
                limit: limit,
                last_id: last_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementMemberRequests(guild: GuildEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getDecrementMemberRequests(
                guild: guild,
                offset: offset,
                limit: limit,
                first_id: first_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func create(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            var promises: [Promise<String>] = []
            if let url = URL(string: guild.picture_small), !url.uploaded {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(guild.picture_small) })
            }
            
            if let url = URL(string: guild.picture_large), !url.uploaded {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(guild.picture_large) })
            }
            
            all(promises).then(in: .background, { urls in
                guild.picture_small = urls[0]
                guild.picture_large = urls[1]
                self.guildDataStore?.create(
                    guild: guild
                ).then(in: .background, { result in
                    resolve(result)
                }).catch(in: .background, { err in
                    reject(err)
                })
            })
        }
    }
    
    func update(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            var promises: [Promise<String>] = []
            if let url = URL(string: guild.picture_small), !url.uploaded, guild.picture_small != GuildConnector.shared.current_guild?.picture_small {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(guild.picture_small) })
            }
            
            if let url = URL(string: guild.picture_large), !url.uploaded, guild.picture_large != GuildConnector.shared.current_guild?.picture_large {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(guild.picture_large) })
            }
            
            all(promises).then(in: .background, { urls in
                guild.picture_small = urls[0]
                guild.picture_large = urls[1]
                self.guildDataStore?.update(
                    guild: guild
                ).then(in: .background, { result in
                    resolve(result)
                }).catch(in: .background, { err in
                    reject(err)
                })
            })
        }
    }
    
    func delete(guild: GuildEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.delete(
                guild: guild
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.create(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.update(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func sectionIndexUpdate(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.sectionIndexUpdate(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func sectionIndexesUpdate(channels: SectionChannelEntities) -> Promise<SectionChannelEntities> {
        return Promise<SectionChannelEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.sectionIndexesUpdate(
                channels: channels
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(channel: SectionChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.delete(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func join(guild: GuildEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.join(
                guild: guild
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func leave(guild: GuildEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.leave(
                guild: guild
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func bump(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.bump(
                guild: guild
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func block(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.block(
                guild: guild,
                target: target
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func unblock(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.unblock(
                guild: guild,
                target: target
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func kick(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.kick(
                guild: guild,
                target: target
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func report(guild: GuildEntity, description: String) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.report(
                guild: guild,
                description: description
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getBlocks(guild: GuildEntity, offset: Int, limit: Int) -> Promise<UserEntities> {
        return Promise<UserEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.getBlocks(
                guild: guild,
                offset: offset,
                limit: limit
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func createMemberRequest(guild: GuildEntity) -> Promise<MemberRequestEntity> {
        return Promise<MemberRequestEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.createMemberRequest(
                guild: guild
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateMemberRequest(guild: GuildEntity, target: UserEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.updateMemberRequest(
                guild: guild,
                target: target
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func deleteMemberRequest(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.deleteMemberRequest(
                guild: guild,
                target: target
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func updateIndexes(guilds: GuildEntities) -> Promise<GuildEntities> {
        return Promise<GuildEntities> (in: .background) { resolve, reject, _ in
            self.guildDataStore?.updateIndexes(
                guilds: guilds
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
}
