//
//  NotificationRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class NotificationRepository: RepositoryImpl {
    static var shared: NotificationRepository = NotificationRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
//    func getUserNotifications(user: UserEntity, limit: Int, offset: Int) -> Promise<NotificationEntities> {
//        return Promise<NotificationEntities> (in: .background) { resolve, reject, _ in
//            self.notificationDataStore?.getUserNotifications(
//                user: user,
//                limit: limit,
//                offset: offset
//            ).then(in: .background, { result in
//                resolve(result)
//            }).catch(in: .background, { err in
//                reject(err)
//            })
//        }
//    }
//    
//    func getUserFriendRequestNotifications(user: UserEntity, limit: Int, offset: Int) -> Promise<NotificationEntities> {
//        return Promise<NotificationEntities> (in: .background) { resolve, reject, _ in
//            self.notificationDataStore?.getUserFriendRequestNotifications(
//                user: user,
//                limit: limit,
//                offset: offset
//            ).then(in: .background, { result in
//                resolve(result)
//            }).catch(in: .background, { err in
//                reject(err)
//            })
//        }
//    }
//    
//    func getUserFriendRequestNotificationsCount(user: UserEntity) -> Promise<Int> {
//        return Promise<Int> (in: .background) { resolve, reject, _ in
//            self.notificationDataStore?.getUserFriendRequestNotificationsCount(
//                user: user
//            ).then(in: .background, { result in
//                resolve(result)
//            }).catch(in: .background, { err in
//                reject(err)
//            })
//        }
//    }
//    
//    func check(notification: NotificationEntity) -> Promise<NotificationEntity> {
//        return Promise<NotificationEntity> (in: .background) { resolve, reject, _ in
//            self.notificationDataStore?.check(
//                notification: notification
//            ).then(in: .background, { result in
//                resolve(result)
//            }).catch(in: .background, { err in
//                reject(err)
//            })
//        }
//    }
//    
//    func checks(notifications: NotificationEntities) -> Promise<NotificationEntities> {
//        return Promise<NotificationEntities> (in: .background) { resolve, reject, _ in
//            self.notificationDataStore?.checks(
//                notifications: notifications
//            ).then(in: .background, { result in
//                resolve(result)
//            }).catch(in: .background, { err in
//                reject(err)
//            })
//        }
//    }
//    
//    func getNotification(notification: NotificationEntity) -> Promise<NotificationEntity> {
//        return Promise<NotificationEntity> (in: .background) { resolve, reject, _ in
//            self.notificationDataStore?.getNotification(
//                notification: notification
//            ).then(in: .background, { result in
//                resolve(result)
//            }).catch(in: .background, { err in
//                reject(err)
//            })
//        }
//    }
}


