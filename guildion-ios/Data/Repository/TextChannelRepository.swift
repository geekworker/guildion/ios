//
//  TextChannelRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class TextChannelRepository: RepositoryImpl {
    static var shared: TextChannelRepository = TextChannelRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func get(uid: String) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.getTextChannel(
                uid: uid
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getRoles(uid: String) -> Promise<TextChannelRoleEntities> {
        return Promise<TextChannelRoleEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.getTextChannelRoles(
                uid: uid
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getUserGuildTextChannels(guild: GuildEntity, offset: Int, limit: Int) -> Promise<TextChannelEntities> {
        return Promise<TextChannelEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.getUserGuildTextChannels(
                guild: guild,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getCurrentTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.getCurrentTextChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getIncrementTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.getIncrementTextChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit,
                last_id: last_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getDecrementTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.getDecrementTextChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit,
                first_id: first_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getTextChannelInvitableMembers(channel: TextChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.getTextChannelInvitableMembers(
                channel: channel,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.create(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.update(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func sectionIndexUpdate(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.sectionIndexUpdate(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func sectionIndexesUpdate(channels: TextChannelEntities) -> Promise<TextChannelEntities> {
        return Promise<TextChannelEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.sectionIndexesUpdate(
                channels: channels
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: TextChannelEntity, permission: Bool) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.update(
                channel: channel,
                permission: permission
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(entry: TextChannelEntryEntity) -> Promise<TextChannelEntryEntity> {
        return Promise<TextChannelEntryEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.update(
                entry: entry
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(channel: TextChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.delete(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func invite(channel: TextChannelEntity, targets: MemberEntities) -> Promise<TextChannelEntryEntities> {
        return Promise<TextChannelEntryEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.invite(
                channel: channel,
                targets: targets
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func kick(channel: TextChannelEntity, targets: MemberEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.kick(
                channel: channel,
                targets: targets
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(channel: TextChannelEntity, role: RoleEntity) -> Promise<TextChannelRoleEntity> {
        return Promise<TextChannelRoleEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.create(
                channel: channel,
                role: role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel_role: TextChannelRoleEntity) -> Promise<TextChannelRoleEntity> {
        return Promise<TextChannelRoleEntity> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.update(
                channel_role: channel_role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(channel: TextChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.delete(
                channel: channel,
                role: role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func leave(channel: TextChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.leave(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func reads(channel: TextChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> (in: .background) { resolve, reject, _ in
            self.textChannelDataStore?.reads(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
}


