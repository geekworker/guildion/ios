//
//  FolderRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class FolderRepository: RepositoryImpl {
    static var shared: FolderRepository = FolderRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func getFolder(id: Int) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getFolder(
                id: id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getFolders(guild_id: Int, limit: Int, offset: Int, is_playlist: Bool) -> Promise<FolderEntities> {
        return Promise<FolderEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getFolders(
                guild_id: guild_id,
                limit: limit,
                offset: offset,
                is_playlist: is_playlist
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentFolderReference(folder_id: Int, limit: Int) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getCurrentFolderReferences(
                folder_id: folder_id,
                limit: limit
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementFolderReference(folder_id: Int, limit: Int, last_id: Int) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getIncrementFolderReferences(
                folder_id: folder_id,
                limit: limit,
                last_id: last_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementFolderReference(folder_id: Int, limit: Int, first_id: Int) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getDecrementFolderReferences(
                folder_id: folder_id,
                limit: limit,
                first_id: first_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentGuildFiles(guild_id: Int, limit: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getCurrentGuildFiles(
                guild_id: guild_id,
                limit: limit,
                is_playlist: is_playlist
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementGuildFiles(guild_id: Int, limit: Int, last_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getIncrementGuildFiles(
                guild_id: guild_id,
                limit: limit,
                last_id: last_id,
                is_playlist: is_playlist
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementGuildFiles(guild_id: Int, limit: Int, first_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getDecrementGuildFiles(
                guild_id: guild_id,
                limit: limit,
                first_id: first_id,
                is_playlist: is_playlist
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentMemberFiles(guild_id: Int, limit: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getCurrentMemberFiles(
                guild_id: guild_id,
                limit: limit,
                is_playlist: is_playlist
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementMemberFiles(guild_id: Int, limit: Int, last_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getIncrementMemberFiles(
                guild_id: guild_id,
                limit: limit,
                last_id: last_id,
                is_playlist: is_playlist
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementMemberFiles(guild_id: Int, limit: Int, first_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.getDecrementMemberFiles(
                guild_id: guild_id,
                limit: limit,
                first_id: first_id,
                is_playlist: is_playlist
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func create(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.create(
                folder: folder
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func update(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.update(
                folder: folder
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func delete(folder: FolderEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.delete(
                folder: folder
            ).then(in: .background, { result in
                resolve(())
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateIndex(reference: FileReferenceEntity) -> Promise<FileReferenceEntity> {
        return Promise<FileReferenceEntity> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.updateIndex(
                reference: reference
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateIndexes(references: FileReferenceEntities) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.updateIndexes(
                references: references
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateIndex(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.updateIndex(
                folder: folder
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateIndexes(folders: FolderEntities) -> Promise<FolderEntities> {
        return Promise<FolderEntities> (in: .background) { resolve, reject, _ in
            self.folderDataStore?.updateIndexes(
                folders: folders
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
}
