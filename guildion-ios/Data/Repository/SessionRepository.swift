//
//  SessionRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

class SessionRepository: RepositoryImpl, ClientDeviseProtocol, CurrentUserProtocol {
    static var shared: SessionRepository = SessionRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func findOneIdentityRequest() -> Promise<IdentityEntity> {
        return Promise<IdentityEntity> (in: .background) { resolve, reject, _ in
            self.sessionDataStore?.findOneIdentityRequest(
                accessToken: ClientDevise.getClientDevise().accessToken
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func generateAccessToken(accessToken: String, is_one_time: Bool) -> Promise<String> {
        return Promise<String> (in: .background) { resolve, reject, _ in
            self.sessionDataStore?.generateAccessToken(
                accessToken: accessToken,
                is_one_time: is_one_time
            ).then(in: .background, { result in
                let devise = ClientDevise.getClientDevise()
                devise.accessToken = accessToken
                self.setClientDevise(entity: devise)
                AppConfig.session_out = false
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func rersendConfirmEmail(email: String) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.sessionDataStore?.resendConfirmEmail(
                email: email
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func confirmEmail(email: String, code: String) -> Promise<(identity: IdentityEntity, accessToken: String)> {
        return Promise<(identity: IdentityEntity, accessToken: String)> (in: .background) { resolve, reject, _ in
            self.sessionDataStore?.confrimEmail(
                email: email, code: code
            ).then(in: .background, { result in
                guard let user = result.identity.User else { return }
                self.setCurrentUser(entity: user)
                let devise = ClientDevise.getClientDevise()
                devise.accessToken = result.accessToken
                self.setClientDevise(entity: devise)
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func register(
        nickname: String,
        email: String,
        password: String,
        locale: String,
        country_code: String,
        timezone: String,
        enable_mail_notification: Bool
    ) -> Promise<IdentityEntity> {
        return Promise<IdentityEntity> (in: .background) { resolve, reject, _ in
            self.sessionDataStore?.register(
                nickname: nickname, email: email, password: password, locale: locale, country_code: country_code, timezone: timezone, enable_mail_notification: enable_mail_notification
            ).then(in: .background, { result in
                guard let user = result.identity.User else { return }
                self.setCurrentUser(entity: user)
                let devise = ClientDevise.getClientDevise()
                devise.accessToken = result.accessToken
                self.setClientDevise(entity: devise)
                AppConfig.session_out = false
                resolve(result.identity)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
}
