//
//  StreamRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class StreamRepository: RepositoryImpl {
    static var shared: StreamRepository = StreamRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?
    
    var awsHandler = AWSHandler()
    
    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func startLive(stream: StreamEntity, loggable: Bool) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> (in: .background) { resolve, reject, _ in
            
            var promises: [Promise<String>] = []
            var t_promises: [Promise<String>] = []
            
            if let files = stream.Files {
                for file in files.items {
                    if let url = URL(string: file.url), !url.uploaded, file.format != .youtube && !file.format.content_type.is_movie {
                        promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type))
                    } else if let url = URL(string: file.url), !url.uploaded, file.format != .youtube && file.format.content_type.is_movie {
                        promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type)) /* baseConvertMovie */
                    } else {
                        promises.append(Promise<String> { resolve, reject, _ in resolve(file.url) })
                    }
                    
                    if let url = URL(string: file.thumbnail), !url.uploaded, file.format.content_type.is_movie {
                        t_promises.append(self.awsHandler.uploadUserImage(url))
                    } else {
                        t_promises.append(Promise<String> { resolve, reject, _ in resolve(file.thumbnail) })
                    }
                }
            }
            
            all(promises).then(in: .background, { urls in
                
                for url in urls.enumerated() {
                    guard let file = stream.Files?[safe: url.offset] else { break }
                    file.url = url.element
                }
                all(t_promises).then(in: .background, { t_urls in
                    
                    for t_url in t_urls.enumerated() {
                        guard let file = stream.Files?[safe: t_url.offset] else { break }
                        if file.format.content_type.is_movie {
                            file.thumbnail = t_url.element
                        } else if file.format != .youtube {
                            file.thumbnail = file.url
                        }
                    }
                    
                    self.streamDataStore?.startLive(
                        stream: stream,
                        loggable: loggable
                    ).then(in: .background, { result in
                        resolve(result)
                    }).catch(in: .background, { err in
                        reject(err)
                    })
                })
            })
        }
    }
    
    func endLive(stream: StreamEntity) -> Promise<StreamEntity> {
        return Promise<StreamEntity> (in: .background) { resolve, reject, _ in
            self.streamDataStore?.endLive(
                stream: stream
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func switchLive(stream: StreamEntity, current_stream: StreamEntity) -> Promise<StreamEntity> {
        return Promise<StreamEntity> (in: .background) { resolve, reject, _ in
            var promises: [Promise<String>] = []
            var t_promises: [Promise<String>] = []
            
            if let files = stream.Files {
                for file in files.items {
                    if let url = URL(string: file.url), !url.uploaded, file.format != .youtube && !file.format.content_type.is_movie {
                        promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type))
                    } else if let url = URL(string: file.url), !url.uploaded, file.format != .youtube && file.format.content_type.is_movie {
                        promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type)) /* baseConvertMovie */
                    } else {
                        promises.append(Promise<String> { resolve, reject, _ in resolve(file.url) })
                    }
                    
                    if let url = URL(string: file.thumbnail), !url.uploaded, file.format.content_type.is_movie {
                        t_promises.append(self.awsHandler.uploadUserImage(url))
                    } else {
                        t_promises.append(Promise<String> { resolve, reject, _ in resolve(file.thumbnail) })
                    }
                }
            }
            
            all(promises).then(in: .background, { urls in
                
                for url in urls.enumerated() {
                    guard let file = stream.Files?[safe: url.offset] else { break }
                    file.url = url.element
                }
                all(t_promises).then(in: .background, {t_urls in
                    
                    for t_url in t_urls.enumerated() {
                        guard let file = stream.Files?[safe: t_url.offset] else { break }
                        if file.format.content_type.is_movie {
                            file.thumbnail = t_url.element
                        } else if file.format != .youtube {
                            file.thumbnail = file.url
                        }
                    }
                    
                    self.streamDataStore?.switchLive(
                        stream: stream,
                        current_stream: current_stream
                    ).then(in: .background, { result in
                        resolve(result)
                    }).catch(in: .background, { err in
                        reject(err)
                    })
                })
            })
        }
    }
    
    func join(channel: StreamChannelEntity) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> (in: .background) { resolve, reject, _ in
            self.streamDataStore?.join(
                channel: channel
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func leave(channel: StreamChannelEntity, duration: Float) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> (in: .background) { resolve, reject, _ in
            self.streamDataStore?.leave(
                channel: channel,
                duration: duration
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateParticipantState(participant: ParticipantEntity) -> Promise<ParticipantEntity> {
        return Promise<ParticipantEntity> (in: .background) { resolve, reject, _ in
            self.streamDataStore?.updateParticipantState(
                participant: participant
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
}


