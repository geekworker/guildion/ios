//
//  MemberRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class MemberRepository: RepositoryImpl {
    static var shared: MemberRepository = MemberRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?
    
    var awsHandler = AWSHandler()

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func get(id: Int) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.getMember(
                id: id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getRoles(member: MemberEntity) -> Promise<MemberRoleEntities> {
        return Promise<MemberRoleEntities> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.getRoles(
                member: member
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getCurrentRoleMembers(role: RoleEntity, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.getCurrentRoleMembers(
                role: role,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getIncrementRoleMembers(role: RoleEntity, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.getIncrementRoleMembers(
                role: role,
                limit: limit,
                last_id: last_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getDecrementRoleMembers(role: RoleEntity, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.getDecrementRoleMembers(
                role: role,
                limit: limit,
                first_id: first_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            var promises: [Promise<String>] = []
            if let url = URL(string: member.picture_small), !url.uploaded, member.picture_small != GuildConnector.shared.current_member?.picture_small {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(member.picture_small) })
            }
            if let url = URL(string: member.picture_large), !url.uploaded, member.picture_large != GuildConnector.shared.current_member?.picture_large {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(member.picture_large) })
            }
            all(promises).then(in: .background, { urls in
                member.picture_small = urls[0]
                member.picture_large = urls[1]
                self.memberDataStore?.update(
                    member: member
                ).then({
                    resolve($0)
                }).catch(in: .background, {
                    reject($0)
                })
            })
        }
    }
    
    func updates() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.updates(
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func updateNotification(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.updateNotification(
                member: member
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func transferOwner(guild: GuildEntity, target: MemberEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.transferOwner(
                guild: guild,
                target: target
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(member_role: MemberRoleEntity) -> Promise<MemberRoleEntity> {
        return Promise<MemberRoleEntity> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.create(
                member_role: member_role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(member_role: MemberRoleEntity) -> Promise<MemberRoleEntity> {
        return Promise<MemberRoleEntity> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.update(
                member_role: member_role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(member_role: MemberRoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.delete(
                member_role: member_role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func updateIndex(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.updateIndex(
                member: member
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateIndexes(members: MemberEntities) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.memberDataStore?.updateIndexes(
                members: members
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
}
