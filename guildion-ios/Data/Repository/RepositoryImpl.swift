//
//  RepositoryImpl.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/04.
//

import UIKit
import Foundation

protocol RepositoryImpl: class {
    var authDataStore: AuthDataStore? { get set }
    var categoryDataStore: CategoryDataStore? { get set }
    var folderDataStore: FolderDataStore? { get set }
    var dmChannelDataStore: DMChannelDataStore? { get set }
    var fileDataStore: FileDataStore? { get set }
    var guildDataStore: GuildDataStore? { get set }
    var searchDataStore: SearchDataStore? { get set }
    var sessionDataStore: SessionDataStore? { get set }
    var streamChannelDataStore: StreamChannelDataStore? { get set }
    var streamDataStore: StreamDataStore? { get set }
    var textChannelDataStore: TextChannelDataStore? { get set }
    var tagDataStore: TagDataStore? { get set }
    var notificationDataStore: NotificationDataStore? { get set }
    var messageDataStore: MessageDataStore? { get set }
    var memberDataStore: MemberDataStore? { get set }
    var userDataStore: UserDataStore? { get set }
    var roleDataStore: RoleDataStore? { get set }
}

