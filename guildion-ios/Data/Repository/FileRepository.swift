//
//  FileRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class FileRepository: RepositoryImpl {
    static var shared: FileRepository = FileRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?
    
    var awsHandler = AWSHandler()

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func get(id: Int) -> Promise<FileEntity> {
        return Promise<FileEntity> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.getFile(
                id: id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentUserFiles(limit: Int) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.getCurrentUserFiles(
                limit: limit
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementUserFiles(limit: Int, last_id: Int) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.getIncrementUserFiles(
                limit: limit,
                last_id: last_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementUserFiles(limit: Int, first_id: Int) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.getDecrementUserFiles(
                limit: limit,
                first_id: first_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func creates(files: FileEntities, onNextProgress: @escaping ((_ progress: CGFloat) -> Void), didUpload: @escaping ((_ results: FileEntities) -> Void) = { _ in }) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            // MARK: progress breakdown
            // 1. start
            // 2. upload file url
            // 3. upload file thumbnail
            // 4. send
            // 5. finish
            let progressCount = CGFloat(5)
            let progressOffset = CGFloat(1) / progressCount
            onNextProgress(progressOffset * 1)
            
            var promises: [Promise<String>] = []
            var t_promises: [Promise<String>] = []
            
            for file in files.items {
                if let url = URL(string: file.url), file.format != .youtube && !file.format.content_type.is_movie {
                    promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type, onProgressUpdate: {
                        file.setProgress($0)
                    }))
                } else if let url = URL(string: file.url), file.format != .youtube && file.format.content_type.is_movie {
                    promises.append(self.awsHandler.uploadUserFile(url, type: file.format.content_type, onProgressUpdate: {
                        file.setProgress($0)
                    })) /* baseConvertMovie */
                } else {
                    promises.append(Promise<String> { resolve, reject, _ in resolve(file.url) })
                }
                
                if let url = URL(string: file.thumbnail), file.format.content_type.is_movie {
                    t_promises.append(self.awsHandler.uploadUserImage(url))
                } else {
                    t_promises.append(Promise<String> { resolve, reject, _ in resolve(file.thumbnail) })
                }
            }
            
            all(promises).then(in: .background, { urls in
                onNextProgress(progressOffset * 2)
                
                for url in urls.enumerated() {
                    guard let file = files[safe: url.offset] else { break }
                    file.url = url.element
                }
                all(t_promises).then(in: .background, {t_urls in
                    
                    onNextProgress(progressOffset * 3)
                    for t_url in t_urls.enumerated() {
                        guard let file = files[safe: t_url.offset] else { break }
                        if file.format.content_type.is_movie {
                            file.thumbnail = t_url.element
                        } else if file.format != .youtube {
                            file.thumbnail = file.url
                        }
                    }
                    onNextProgress(progressOffset * 4)
                    self.fileDataStore?.creates(
                        files: files
                    ).then(in: .background, { result in
                        resolve(result)
                        onNextProgress(progressOffset * 5)
                        didUpload(result)
                    }).catch(in: .background, { err in
                        reject(err)
                    })
                }).catch(in: .background, { err in
                    reject(err)
                })
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func update(file: FileEntity) -> Promise<FileEntity> {
        return Promise<FileEntity> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.update(
                file: file
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func delete(file: FileEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.delete(
                file: file
            ).then(in: .background, { result in
                resolve(())
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func deletes(files: FileEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.deletes(
                files: files
            ).then(in: .background, { result in
                resolve(())
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func createReference(reference: FileReferenceEntity) -> Promise<FileReferenceEntity> {
        return Promise<FileReferenceEntity> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.createReference(
                reference: reference
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func createReferences(references: FileReferenceEntities) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.createReferences(
                references: references
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateReference(reference: FileReferenceEntity) -> Promise<FileReferenceEntity> {
        return Promise<FileReferenceEntity> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.updateReference(
                reference: reference
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func deleteReference(reference: FileReferenceEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.deleteReference(
                reference: reference
            ).then(in: .background, { result in
                resolve(())
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func deleteReferences(references: FileReferenceEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.fileDataStore?.deleteReferences(
                references: references
            ).then(in: .background, { result in
                resolve(())
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
}

