//
//  StreamChannelRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class StreamChannelRepository: RepositoryImpl {
    static var shared: StreamChannelRepository = StreamChannelRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func getStreamChannel(uid: String) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getStreamChannelFromUID(
                uid: uid
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getRoles(uid: String) -> Promise<StreamChannelRoleEntities> {
        return Promise<StreamChannelRoleEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getStreamChannelRoles(
                uid: uid
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getParticipants(uid: String) -> Promise<ParticipantEntities> {
        return Promise<ParticipantEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getStreamChannelParticipants(
                uid: uid
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getUserGuildStreamChannels(guild: GuildEntity, offset: Int, limit: Int) -> Promise<StreamChannelEntities> {
        return Promise<StreamChannelEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getUserGuildStreamChannels(
                guild: guild,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getCurrentStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getCurrentStreamChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getIncrementStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getIncrementStreamChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit,
                last_id: last_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getDecrementStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getDecrementStreamChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit,
                first_id: first_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getStreamChannelInvitableMembers(channel: StreamChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.getStreamChannelInvitableMembers(
                channel: channel,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.create(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.update(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func sectionIndexUpdate(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.sectionIndexUpdate(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func sectionIndexesUpdate(channels: StreamChannelEntities) -> Promise<StreamChannelEntities> {
        return Promise<StreamChannelEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.sectionIndexesUpdate(
                channels: channels
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: StreamChannelEntity, permission: Bool) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.update(
                channel: channel,
                permission: permission
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: StreamChannelEntity, is_loop: Bool) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.update(
                channel: channel,
                is_loop: is_loop
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(entry: StreamChannelEntryEntity) -> Promise<StreamChannelEntryEntity> {
        return Promise<StreamChannelEntryEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.update(
                entry: entry
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(channel: StreamChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.delete(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func invite(channel: StreamChannelEntity, targets: MemberEntities) -> Promise<StreamChannelEntryEntities> {
        return Promise<StreamChannelEntryEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.invite(
                channel: channel,
                targets: targets
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func kick(channel: StreamChannelEntity, targets: MemberEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.kick(
                channel: channel,
                targets: targets
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func start(guild: GuildEntity, target: MemberEntity?, stream: StreamEntity?, voice_only: Bool, movie_only: Bool) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.start(
                guild: guild,
                target: target,
                stream: stream,
                voice_only: voice_only,
                movie_only: movie_only
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(channel: StreamChannelEntity, role: RoleEntity) -> Promise<StreamChannelRoleEntity> {
        return Promise<StreamChannelRoleEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.create(
                channel: channel,
                role: role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel_role: StreamChannelRoleEntity) -> Promise<StreamChannelRoleEntity> {
        return Promise<StreamChannelRoleEntity> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.update(
                channel_role: channel_role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(channel: StreamChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.delete(
                channel: channel,
                role: role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func leave(channel: StreamChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.leave(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func reads(channel: StreamChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> (in: .background) { resolve, reject, _ in
            self.streamChannelDataStore?.reads(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
}


