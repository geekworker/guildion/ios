//
//  DMChannelRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class DMChannelRepository: RepositoryImpl {
    static var shared: DMChannelRepository = DMChannelRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func get(uid: String) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.getDMChannel(
                uid: uid
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getRoles(uid: String) -> Promise<DMChannelRoleEntities> {
        return Promise<DMChannelRoleEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.getDMChannelRoles(
                uid: uid
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getUserGuildDMChannels(guild: GuildEntity, offset: Int, limit: Int) -> Promise<DMChannelEntities> {
        return Promise<DMChannelEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.getUserGuildDMChannels(
                guild: guild,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getCurrentDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.getCurrentDMChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getIncrementDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.getIncrementDMChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit,
                last_id: last_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getDecrementDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.getDecrementDMChannelMembers(
                channel: channel,
                offset: offset,
                limit: limit,
                first_id: first_id
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func getDMChannelInvitableMembers(channel: DMChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.getDMChannelInvitableMembers(
                channel: channel,
                offset: offset,
                limit: limit
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func start(guild: GuildEntity, target: MemberEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.start(
                guild: guild,
                target: target
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.create(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.update(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func sectionIndexUpdate(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.sectionIndexUpdate(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel: DMChannelEntity, permission: Bool) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.update(
                channel: channel,
                permission: permission
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(entry: DMChannelEntryEntity) -> Promise<DMChannelEntryEntity> {
        return Promise<DMChannelEntryEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.update(
                entry: entry
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(channel: DMChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.delete(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func invite(channel: DMChannelEntity, targets: MemberEntities) -> Promise<DMChannelEntryEntities> {
        return Promise<DMChannelEntryEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.invite(
                channel: channel,
                targets: targets
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func kick(channel: DMChannelEntity, targets: MemberEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.kick(
                channel: channel,
                targets: targets
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func create(channel: DMChannelEntity, role: RoleEntity) -> Promise<DMChannelRoleEntity> {
        return Promise<DMChannelRoleEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.create(
                channel: channel,
                role: role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func update(channel_role: DMChannelRoleEntity) -> Promise<DMChannelRoleEntity> {
        return Promise<DMChannelRoleEntity> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.update(
                channel_role: channel_role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func delete(channel: DMChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.delete(
                channel: channel,
                role: role
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func leave(channel: DMChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.leave(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
    
    func reads(channel: DMChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> (in: .background) { resolve, reject, _ in
            self.dmChannelDataStore?.reads(
                channel: channel
            ).then({
                resolve($0)
            }).catch(in: .background, {
                reject($0)
            })
        }
    }
}

