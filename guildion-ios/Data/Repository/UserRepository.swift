//
//  UserRepository.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Hydra

class UserRepository: RepositoryImpl, ClientDeviseProtocol {
    static var shared: UserRepository = UserRepository(
        authDataStore: AuthDataStore.shared,
        categoryDataStore: CategoryDataStore.shared,
        folderDataStore: FolderDataStore.shared,
        dmChannelDataStore: DMChannelDataStore.shared,
        fileDataStore: FileDataStore.shared,
        guildDataStore: GuildDataStore.shared,
        searchDataStore: SearchDataStore.shared,
        sessionDataStore: SessionDataStore.shared,
        streamChannelDataStore: StreamChannelDataStore.shared,
        streamDataStore: StreamDataStore.shared,
        textChannelDataStore: TextChannelDataStore.shared,
        tagDataStore: TagDataStore.shared,
        notificationDataStore: NotificationDataStore.shared,
        messageDataStore: MessageDataStore.shared,
        memberDataStore: MemberDataStore.shared,
        userDataStore: UserDataStore.shared,
        roleDataStore: RoleDataStore.shared
    )
    var authDataStore: AuthDataStore?
    var categoryDataStore: CategoryDataStore?
    var folderDataStore: FolderDataStore?
    var dmChannelDataStore: DMChannelDataStore?
    var fileDataStore: FileDataStore?
    var guildDataStore: GuildDataStore?
    var searchDataStore: SearchDataStore?
    var sessionDataStore: SessionDataStore?
    var streamChannelDataStore: StreamChannelDataStore?
    var streamDataStore: StreamDataStore?
    var textChannelDataStore: TextChannelDataStore?
    var tagDataStore: TagDataStore?
    var notificationDataStore: NotificationDataStore?
    var messageDataStore: MessageDataStore?
    var memberDataStore: MemberDataStore?
    var userDataStore: UserDataStore?
    var roleDataStore: RoleDataStore?
    
    var awsHandler = AWSHandler()

    required init(
        authDataStore: AuthDataStore,
        categoryDataStore: CategoryDataStore,
        folderDataStore: FolderDataStore,
        dmChannelDataStore: DMChannelDataStore,
        fileDataStore: FileDataStore,
        guildDataStore: GuildDataStore,
        searchDataStore: SearchDataStore,
        sessionDataStore: SessionDataStore,
        streamChannelDataStore: StreamChannelDataStore,
        streamDataStore: StreamDataStore,
        textChannelDataStore: TextChannelDataStore,
        tagDataStore: TagDataStore,
        notificationDataStore: NotificationDataStore,
        messageDataStore: MessageDataStore,
        memberDataStore: MemberDataStore,
        userDataStore: UserDataStore,
        roleDataStore: RoleDataStore
    ) {
        self.authDataStore = authDataStore
        self.categoryDataStore = categoryDataStore
        self.folderDataStore = folderDataStore
        self.dmChannelDataStore = dmChannelDataStore
        self.fileDataStore = fileDataStore
        self.guildDataStore = guildDataStore
        self.searchDataStore = searchDataStore
        self.sessionDataStore = sessionDataStore
        self.streamChannelDataStore = streamChannelDataStore
        self.streamDataStore = streamDataStore
        self.textChannelDataStore = textChannelDataStore
        self.tagDataStore = tagDataStore
        self.notificationDataStore = notificationDataStore
        self.messageDataStore = messageDataStore
        self.memberDataStore = memberDataStore
        self.userDataStore = userDataStore
        self.roleDataStore = roleDataStore
    }
    
    func getUser(id: Int) -> Promise<UserEntity> {
        return Promise<UserEntity> (in: .background) { resolve, reject, _ in
            self.userDataStore?.getUser(
                id: id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateUser(user: UserEntity) -> Promise<UserEntity> {
        return Promise<UserEntity> (in: .background) { resolve, reject, _ in
            var promises: [Promise<String>] = []
            if user.picture_small != CurrentUser.getCurrentUserEntity()?.picture_small, let url = URL(string: user.picture_small), !url.uploaded {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(user.picture_small) })
            }
            if user.picture_large != CurrentUser.getCurrentUserEntity()?.picture_large, let url = URL(string: user.picture_large), !url.uploaded {
                promises.append(self.awsHandler.uploadUserImage(url))
            } else {
                promises.append(Promise<String> { resolve, reject, _ in resolve(user.picture_large) })
            }
            all(promises).then(in: .background, { urls in
                user.picture_small = urls[0]
                user.picture_large = urls[1]
                self.userDataStore?.updateUser(
                    user: user
                ).then(in: .background, { result in
                    resolve(result)
                }).catch(in: .background, { err in
                    reject(err)
                })
            })
        }
    }
    
    func updateUserEmailNotification(user: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.userDataStore?.updateUserEmailNotification(
                user: user
            ).then(in: .background, { result in
                resolve(())
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func deleteUser() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.userDataStore?.deleteUser(
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func registerDevise() -> Promise<DeviseEntity> {
        return Promise<DeviseEntity> (in: .background) { resolve, reject, _ in
            guard !ClientDevise.deviseRegisterd() else { return }
            self.userDataStore?.registerDevise(
            ).then(in: .background, { result in
                let devise = DeviseEntity_ClientDeviseTranslator.translate(result)
                self.setClientDevise(entity: devise)
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func updateDevise() -> Promise<DeviseEntity> {
        return Promise<DeviseEntity> (in: .background) { resolve, reject, _ in
            if !ClientDevise.deviseRegisterd() {
                do {
                    try await(self.registerDevise())
                } catch let error {
                    reject(error)
                }
            }
            self.userDataStore?.updateDevise(
            ).then(in: .background, { result in
                let devise = DeviseEntity_ClientDeviseTranslator.translate(result)
                self.setClientDevise(entity: devise)
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func logoutDevise() -> Promise<DeviseEntity> {
        return Promise<DeviseEntity> (in: .background) { resolve, reject, _ in
            if !ClientDevise.deviseRegisterd() {
                do {
                    try await(self.registerDevise())
                } catch let error {
                    reject(error)
                }
            }
            self.userDataStore?.logoutDevise(
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func active() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            guard CurrentUser.getCurrentUserEntity() != nil else { return }
            self.userDataStore?.active(
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func inActive() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            guard CurrentUser.getCurrentUserEntity() != nil else { return }
            self.userDataStore?.inActive(
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func block(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.userDataStore?.block(
                target: target
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func unblock(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.userDataStore?.unblock(
                target: target
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func mute(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.userDataStore?.mute(
                target: target
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func unmute(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.userDataStore?.unmute(
                target: target
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func report(target: UserEntity, detail: String) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            self.userDataStore?.report(
                target: target,
                detail: detail
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getMembers() -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.userDataStore?.getMembers(
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getCurrentMembers(offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.userDataStore?.getCurrentMembers(
                limit: limit,
                offset: offset
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getIncrementMembers(offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.userDataStore?.getIncrementMembers(
                limit: limit,
                offset: offset,
                last_id: last_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getDecrementMembers(offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            self.userDataStore?.getDecrementMembers(
                limit: limit,
                offset: offset,
                first_id: first_id
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
    
    func getRelationStats() -> Promise<UserEntity> {
        return Promise<UserEntity> (in: .background) { resolve, reject, _ in
            self.userDataStore?.getRelationStats(
            ).then(in: .background, { result in
                resolve(result)
            }).catch(in: .background, { err in
                reject(err)
            })
        }
    }
}
