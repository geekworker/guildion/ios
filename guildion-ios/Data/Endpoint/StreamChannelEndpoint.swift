//
//  StreamChannelEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct streamChannelResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum streamChannelRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = streamChannelResponse
    
    case getStreamChannel(id: Int)
    case getStreamChannelFromUID(uid: String)
    case getStreamChannelRoles(uid: String)
    case getStreamChannelParticipants(uid: String)
    case getUserGuildStreamChannels(guild: GuildEntity, offset: Int, limit: Int)
    case getCurrentStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int)
    case getIncrementStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int, last_id: Int)
    case getDecrementStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int, first_id: Int)
    case getStreamChannelInvitableMembers(channel: StreamChannelEntity, offset: Int, limit: Int)
    case start(guild: GuildEntity, target: MemberEntity?, stream: StreamEntity?, voice_only: Bool, movie_only: Bool)
    case create(channel: StreamChannelEntity)
    case update(channel: StreamChannelEntity)
    case updatePermission(channel: StreamChannelEntity, permission: Bool)
    case updateLoop(channel: StreamChannelEntity, is_loop: Bool)
    case updateEntry(entry: StreamChannelEntryEntity)
    case sectionIndexUpdate(channel: StreamChannelEntity)
    case sectionIndexesUpdate(channels: StreamChannelEntities)
    case delete(channel: StreamChannelEntity)
    case reads(channel: StreamChannelEntity)
    case join(channel: StreamChannelEntity)
    case leave(channel: StreamChannelEntity)
    case addRole(channel: StreamChannelEntity, role: RoleEntity)
    case updateRole(channel_role: StreamChannelRoleEntity)
    case removeRole(channel: StreamChannelEntity, role: RoleEntity)
    case invite(channel: StreamChannelEntity, targets: MemberEntities)
    case kick(channel: StreamChannelEntity, targets: MemberEntities)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getStreamChannel, .getStreamChannelFromUID: return "/api/v1/channel/stream"
        case .getStreamChannelRoles: return "/api/v1/channel/stream/roles"
        case .getStreamChannelParticipants: return "/api/v1/channel/stream/participants"
        case .getUserGuildStreamChannels: return "/api/v1/channels/streams"
        case .getCurrentStreamChannelMembers: return "/api/v1/channel/stream/members/current"
        case .getIncrementStreamChannelMembers: return "/api/v1/channel/stream/members/increment"
        case .getDecrementStreamChannelMembers: return "/api/v1/channel/stream/members/decrement"
        case .getStreamChannelInvitableMembers: return "/api/v1/channel/stream/invitable/members"
        case .start: return "/api/v1/channel/stream/start"
        case .create: return "/api/v1/channel/stream/create"
        case .update: return "/api/v1/channel/stream/update"
        case .sectionIndexUpdate: return "/api/v1/channel/stream/section/update"
        case .sectionIndexesUpdate: return "/api/v1/channel/streams/section/update"
        case .updateEntry: return "/api/v1/channel/stream/entry/update"
        case .updatePermission: return "/api/v1/channel/stream/update/permission"
        case .updateLoop: return "/api/v1/channel/stream/update/loop"
        case .delete: return "/api/v1/channel/stream/delete"
        case .reads: return "/api/v1/channel/stream/messages/read"
        case .join: return "/api/v1/channel/stream/join"
        case .leave: return "/api/v1/channel/stream/leave"
        case .addRole: return "/api/v1/channel/stream/role/create"
        case .updateRole: return "/api/v1/channel/stream/role/update"
        case .removeRole: return "/api/v1/channel/stream/role/delete"
        case .invite: return "/api/v1/channel/stream/invites"
        case .kick: return "/api/v1/channel/stream/kicks"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getStreamChannelFromUID(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getStreamChannelRoles(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getStreamChannelParticipants(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getStreamChannel(let id):
            return [
                "id": id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getUserGuildStreamChannels(let guild, let offset, let limit):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getStreamChannelInvitableMembers(let channel, let offset, let limit):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentStreamChannelMembers(let channel, let offset, let limit):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementStreamChannelMembers(let channel, let offset, let limit, let last_id):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementStreamChannelMembers(let channel, let offset, let limit, let first_id):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .reads(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .start(let guild, let target, let stream, let voice_only, let movie_only):
            var json: [String : Any] = [
                "guild": guild.toJSON(),
                "voice_only": voice_only,
                "movie_only": movie_only,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            if let unwrapped = target { json["target"] = unwrapped.toJSON() }
            if let unwrapped = stream { json["stream"] = unwrapped.toJSON() }
            return json
        case .create(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .sectionIndexUpdate(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .sectionIndexesUpdate(let channels):
            return [
                "channels": channels.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .invite(let channel, let targets):
            return [
                "channel": channel.toJSON(),
                "members": targets.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .kick(let channel, let targets):
            return [
                "channel": channel.toJSON(),
                "members": targets.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updatePermission(channel: let channel, permission: let permission):
            return [
                "channel": channel.toJSON(),
                "permission": permission,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateLoop(let channel, let is_loop):
            return [
                "channel": channel.toJSON(),
                "is_loop": is_loop,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateEntry(entry: let entry):
            return [
                "entry": entry.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .join(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .leave(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .addRole(channel: let channel, role: let role):
            return [
                "channel": channel.toJSON(),
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateRole(channel_role: let channel_role):
            return [
                "channel_role": channel_role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .removeRole(channel: let channel, role: let role):
            return [
                "channel": channel.toJSON(),
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        }
    }
}
