//
//  TextChannelEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct textChannelResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum textChannelRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = textChannelResponse
    
    case getTextChannel(uid: String)
    case getTextChannelRoles(uid: String)
    case getUserGuildTextChannels(guild: GuildEntity, offset: Int, limit: Int)
    case getCurrentTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int)
    case getIncrementTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int, last_id: Int)
    case getDecrementTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int, first_id: Int)
    case getTextChannelInvitableMembers(channel: TextChannelEntity, offset: Int, limit: Int)
    case create(channel: TextChannelEntity)
    case update(channel: TextChannelEntity)
    case updatePermission(channel: TextChannelEntity, permission: Bool)
    case updateEntry(entry: TextChannelEntryEntity)
    case delete(channel: TextChannelEntity)
    case sectionIndexUpdate(channel: TextChannelEntity)
    case sectionIndexesUpdate(channels: TextChannelEntities)
    case reads(channel: TextChannelEntity)
    case join(channel: TextChannelEntity)
    case leave(channel: TextChannelEntity)
    case addRole(channel: TextChannelEntity, role: RoleEntity)
    case updateRole(channel_role: TextChannelRoleEntity)
    case removeRole(channel: TextChannelEntity, role: RoleEntity)
    case invite(channel: TextChannelEntity, targets: MemberEntities)
    case kick(channel: TextChannelEntity, targets: MemberEntities)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getTextChannel: return "/api/v1/channel/text"
        case .getTextChannelRoles: return "/api/v1/channel/text/roles"
        case .getUserGuildTextChannels: return "/api/v1/channels/texts"
        case .getCurrentTextChannelMembers: return "/api/v1/channel/text/members/current"
        case .getIncrementTextChannelMembers: return "/api/v1/channel/text/members/increment"
        case .getDecrementTextChannelMembers: return "/api/v1/channel/text/members/decrement"
        case .getTextChannelInvitableMembers: return "/api/v1/channel/text/invitable/members"
        case .create: return "/api/v1/channel/text/create"
        case .update: return "/api/v1/channel/text/update"
        case .sectionIndexUpdate: return "/api/v1/channel/text/section/update"
        case .sectionIndexesUpdate: return "/api/v1/channel/texts/section/update"
        case .updateEntry: return "/api/v1/channel/text/entry/update"
        case .updatePermission: return "/api/v1/channel/text/update/permission"
        case .delete: return "/api/v1/channel/text/delete"
        case .reads: return "/api/v1/channel/text/messages/read"
        case .join: return "/api/v1/channel/text/join"
        case .leave: return "/api/v1/channel/text/leave"
        case .addRole: return "/api/v1/channel/text/role/create"
        case .updateRole: return "/api/v1/channel/text/role/update"
        case .removeRole: return "/api/v1/channel/text/role/delete"
        case .invite: return "/api/v1/channel/text/invites"
        case .kick: return "/api/v1/channel/text/kicks"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getTextChannel(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getTextChannelRoles(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getUserGuildTextChannels(let guild, let offset, let limit):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentTextChannelMembers(let channel, let offset, let limit):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementTextChannelMembers(let channel, let offset, let limit, let last_id):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementTextChannelMembers(let channel, let offset, let limit, let first_id):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getTextChannelInvitableMembers(let channel, let offset, let limit):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .create(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .sectionIndexUpdate(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .sectionIndexesUpdate(let channels):
            return [
                "channels": channels.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .invite(let channel, let targets):
            return [
                "channel": channel.toJSON(),
                "members": targets.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .kick(let channel, let targets):
            return [
                "channel": channel.toJSON(),
                "members": targets.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updatePermission(channel: let channel, permission: let permission):
            return [
                "channel": channel.toJSON(),
                "permission": permission,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateEntry(entry: let entry):
            return [
                "entry": entry.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .reads(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .join(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .leave(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .addRole(channel: let channel, role: let role):
            return [
                "channel": channel.toJSON(),
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateRole(channel_role: let channel_role):
            return [
                "channel_role": channel_role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .removeRole(channel: let channel, role: let role):
            return [
                "channel": channel.toJSON(),
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        }
    }
}



