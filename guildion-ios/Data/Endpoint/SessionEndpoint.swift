//
//  SessionEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct sessionResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum sessionRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = sessionResponse
    
    case findOneIdentityRequest(accessToken: String)
    case generateAccessToken(accessToken: String, is_one_time: Bool)
    case resendConfirmEmail(email: String)
    case confirmEmail(email: String, code: String)
    case register(
        nickname: String,
        email: String,
        password: String,
        locale: String,
        country_code: String,
        timezone: String,
        enable_mail_notification: Bool
    )
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .findOneIdentityRequest: return "/api/v1/identity"
        case .generateAccessToken: return "/api/v1/access_token/create"
        case .resendConfirmEmail: return "/api/v1/session/confirmation/resend/email"
        case .confirmEmail: return "/api/v1/confirm_email"
        case .register: return "/api/v1/session/register"
        }
    }
    
    var parameters: Parameters? {
        switch self {

        case .findOneIdentityRequest(
            let accessToken
            ):
            return [
                "accessToken": "\(accessToken)",
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            
        case .generateAccessToken(let accessToken, let is_one_time):
            return [
                "accessToken": "\(accessToken)",
                "is_one_time": "\(is_one_time)",
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            
        case .resendConfirmEmail(let email):
            return [
                "email": "\(email)",
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            
        case .register(
            let nickname,
            let email,
            let password,
            let locale,
            let country_code,
            let timezone,
            let enable_mail_notification
        ):
            return [
                "nickname": "\(nickname)",
                "email": "\(email)",
                "password": "\(password)",
                "locale": "\(locale)",
                "country_code": "\(country_code)",
                "timezone": "\(timezone)",
                "enable_mail_notification": enable_mail_notification,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        
        case .confirmEmail(let email, let code):
            return [
                "email": "\(email)",
                "code": "\(code)",
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken
            ]
        }
    }
}
