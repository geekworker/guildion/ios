//
//  DMChannelEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct dmChannelResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum dmChannelRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = dmChannelResponse
    
    case getDMChannel(uid: String)
    case getDMChannelRoles(uid: String)
    case getUserGuildDMChannels(guild: GuildEntity, offset: Int, limit: Int)
    case getCurrentDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int)
    case getIncrementDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int, last_id: Int)
    case getDecrementDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int, first_id: Int)
    case getDMChannelInvitableMembers(channel: DMChannelEntity, offset: Int, limit: Int)
    case start(guild: GuildEntity, target: MemberEntity)
    case create(channel: DMChannelEntity)
    case update(channel: DMChannelEntity)
    case updatePermission(channel: DMChannelEntity, permission: Bool)
    case updateEntry(entry: DMChannelEntryEntity)
    case delete(channel: DMChannelEntity)
    case sectionIndexUpdate(channel: DMChannelEntity)
    case reads(channel: DMChannelEntity)
    case join(channel: DMChannelEntity)
    case leave(channel: DMChannelEntity)
    case addRole(channel: DMChannelEntity, role: RoleEntity)
    case updateRole(channel_role: DMChannelRoleEntity)
    case removeRole(channel: DMChannelEntity, role: RoleEntity)
    case invite(channel: DMChannelEntity, targets: MemberEntities)
    case kick(channel: DMChannelEntity, targets: MemberEntities)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getDMChannel: return "/api/v1/channel/dm"
        case .getDMChannelRoles: return "/api/v1/channel/dm/roles"
        case .getUserGuildDMChannels: return "/api/v1/channels/dms"
        case .getCurrentDMChannelMembers: return "/api/v1/channel/dm/members/current"
        case .getIncrementDMChannelMembers: return "/api/v1/channel/dm/members/increment"
        case .getDecrementDMChannelMembers: return "/api/v1/channel/dm/members/decrement"
        case .getDMChannelInvitableMembers: return "/api/v1/channel/dm/invitable/members"
        case .start: return "/api/v1/channel/dm/start"
        case .create: return "/api/v1/channel/dm/create"
        case .update: return "/api/v1/channel/dm/update"
        case .sectionIndexUpdate: return "/api/v1/channel/dm/section/update"
        case .updateEntry: return "/api/v1/channel/dm/entry/update"
        case .updatePermission: return "/api/v1/channel/dm/update/permission"
        case .delete: return "/api/v1/channel/dm/delete"
        case .reads: return "/api/v1/channel/dm/messages/read"
        case .join: return "/api/v1/channel/dm/join"
        case .leave: return "/api/v1/channel/dm/leave"
        case .addRole: return "/api/v1/channel/dm/role/create"
        case .updateRole: return "/api/v1/channel/dm/role/update"
        case .removeRole: return "/api/v1/channel/dm/role/delete"
        case .invite: return "/api/v1/channel/dm/invites"
        case .kick: return "/api/v1/channel/dm/kicks"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getDMChannel(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDMChannelRoles(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getUserGuildDMChannels(let guild, let offset, let limit):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDMChannelInvitableMembers(let channel, let offset, let limit):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentDMChannelMembers(let channel, let offset, let limit):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementDMChannelMembers(let channel, let offset, let limit, let last_id):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementDMChannelMembers(let channel, let offset, let limit, let first_id):
            return [
                "channel": channel.toJSON(),
                "offset": offset,
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .start(let guild, let target):
            return [
                "guild": guild.toJSON(),
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .create(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .sectionIndexUpdate(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .invite(let channel, let targets):
            return [
                "channel": channel.toJSON(),
                "members": targets.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .kick(let channel, let targets):
            return [
                "channel": channel.toJSON(),
                "members": targets.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updatePermission(channel: let channel, permission: let permission):
            return [
                "channel": channel.toJSON(),
                "permission": permission,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateEntry(entry: let entry):
            return [
                "entry": entry.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .reads(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .join(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .leave(channel: let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .addRole(channel: let channel, role: let role):
            return [
                "channel": channel.toJSON(),
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateRole(channel_role: let channel_role):
            return [
                "channel_role": channel_role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .removeRole(channel: let channel, role: let role):
            return [
                "channel": channel.toJSON(),
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        }
    }
}



