//
//  FolderEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct folderResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum folderRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = folderResponse
    
    case getFolder(id :Int)
    case getFolders(guild_id: Int, limit: Int, offset: Int, is_playlist: Bool)
    case getCurrentFolderReferences(folder_id: Int, limit: Int)
    case getIncrementFolderReferences(folder_id: Int, limit: Int, last_id: Int)
    case getDecrementFolderReferences(folder_id: Int, limit: Int, first_id: Int)
    case getCurrentGuildFiles(guild_id: Int, limit: Int, is_playlist: Bool)
    case getIncrementGuildFiles(guild_id: Int, limit: Int, last_id: Int, is_playlist: Bool)
    case getDecrementGuildFiles(guild_id: Int, limit: Int, first_id: Int, is_playlist: Bool)
    case getCurrentMemberFiles(guild_id: Int, limit: Int, is_playlist: Bool)
    case getIncrementMemberFiles(guild_id: Int, limit: Int, last_id: Int, is_playlist: Bool)
    case getDecrementMemberFiles(guild_id: Int, limit: Int, first_id: Int, is_playlist: Bool)
    case create(folder: FolderEntity)
    case update(folder: FolderEntity)
    case delete(folder: FolderEntity)
    case updateIndex(reference: FileReferenceEntity)
    case updateIndexes(references: FileReferenceEntities)
    case updateFolderIndex(folder: FolderEntity)
    case updateFolderIndexes(folders: FolderEntities)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getFolder: return "/api/v1/folder"
        case .getFolders: return "/api/v1/guild/folders"
        case .getCurrentFolderReferences: return "/api/v1/folder/references/current"
        case .getIncrementFolderReferences: return "/api/v1/folder/references/increment"
        case .getDecrementFolderReferences: return "/api/v1/folder/references/decrement"
        case .getCurrentGuildFiles: return "/api/v1/guild/files/current"
        case .getIncrementGuildFiles: return "/api/v1/guild/files/increment"
        case .getDecrementGuildFiles: return "/api/v1/guild/files/decrement"
        case .getCurrentMemberFiles: return "/api/v1/member/files/current"
        case .getIncrementMemberFiles: return "/api/v1/member/files/increment"
        case .getDecrementMemberFiles: return "/api/v1/member/files/decrement"
        case .create: return "/api/v1/folder/create"
        case .update: return "/api/v1/folder/update"
        case .delete: return "/api/v1/folder/delete"
        case .updateIndex: return "/api/v1/folder/file/index/update"
        case .updateIndexes: return "/api/v1/folder/files/index/update"
        case .updateFolderIndex: return "/api/v1/folder/index/update"
        case .updateFolderIndexes: return "/api/v1/folders/index/update"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getFolder(let id):
            return [
                "id": id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getFolders(let guild_id, let limit, let offset, let is_playlist):
            return [
                "guild_id": guild_id,
                "limit": limit,
                "offset": offset,
                "is_playlist": is_playlist,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentFolderReferences(let folder_id, let limit):
            return [
                "folder_id": folder_id,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementFolderReferences(let folder_id, let limit, let last_id):
            return [
                "folder_id": folder_id,
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementFolderReferences(let folder_id, let limit, let first_id):
            return [
                "folder_id": folder_id,
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentGuildFiles(let guild_id, let limit, let is_playlist):
            return [
                "guild_id": guild_id,
                "limit": limit,
                "is_playlist": is_playlist,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementGuildFiles(let guild_id, let limit, let last_id, let is_playlist):
            return [
                "guild_id": guild_id,
                "limit": limit,
                "last_id": last_id,
                "is_playlist": is_playlist,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementGuildFiles(let guild_id, let limit, let first_id, let is_playlist):
            return [
                "guild_id": guild_id,
                "limit": limit,
                "first_id": first_id,
                "is_playlist": is_playlist,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentMemberFiles(let guild_id, let limit, let is_playlist):
            return [
                "guild_id": guild_id,
                "limit": limit,
                "is_playlist": is_playlist,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementMemberFiles(let guild_id, let limit, let last_id, let is_playlist):
            return [
                "guild_id": guild_id,
                "limit": limit,
                "last_id": last_id,
                "is_playlist": is_playlist,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementMemberFiles(let guild_id, let limit, let first_id, let is_playlist):
            return [
                "guild_id": guild_id,
                "limit": limit,
                "first_id": first_id,
                "is_playlist": is_playlist,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .create(let folder):
            return [
                "folder": folder.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let folder):
            return [
                "folder": folder.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let folder):
            return [
                "folder": folder.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateIndex(let reference):
            return [
                "reference": reference.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateIndexes(let references):
            return [
                "references": references.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateFolderIndex(let folder):
            return [
                "folder": folder.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateFolderIndexes(let folders):
            return [
                "folders": folders.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}

