//
//  MemberEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct memberResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum memberRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = memberResponse
    
    case getMember(id :Int)
    case getRoles(member: MemberEntity)
    case getCurrentRoleMembers(role: RoleEntity, limit: Int)
    case getIncrementRoleMembers(role: RoleEntity, limit: Int, last_id: Int)
    case getDecrementRoleMembers(role: RoleEntity, limit: Int, first_id: Int)
    case update(member: MemberEntity)
    case updates
    case updateNotification(member: MemberEntity)
    case transferOwner(guild: GuildEntity, target: MemberEntity)
    case addRole(member_role: MemberRoleEntity)
    case updateRole(member_role: MemberRoleEntity)
    case removeRole(member_role: MemberRoleEntity)
    case updateIndex(member: MemberEntity)
    case updateIndexes(members: MemberEntities)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getMember: return "/api/v1/member"
        case .getRoles: return "/api/v1/member/roles"
        case .getCurrentRoleMembers: return "/api/v1/role/members/current"
        case .getIncrementRoleMembers: return "/api/v1/role/members/increment"
        case .getDecrementRoleMembers: return "/api/v1/role/members/decrement"
        case .update: return "/api/v1/member/update"
        case .updates: return "/api/v1/members/update"
        case .updateNotification: return "/api/v1/member/update/notification"
        case .transferOwner: return "/api/v1/guild/transfer"
        case .addRole: return "/api/v1/member/role/create"
        case .updateRole: return "/api/v1/member/role/update"
        case .removeRole: return "/api/v1/member/role/delete"
        case .updateIndex: return "/api/v1/member/index/update"
        case .updateIndexes: return "/api/v1/members/index/update"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getMember(let id):
            return [
                "id": id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentRoleMembers(let role, let limit):
            return [
                "role": role.toJSON(),
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementRoleMembers(let role, let limit, let last_id):
            return [
                "role": role.toJSON(),
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementRoleMembers(let role, let limit, let first_id):
            return [
                "role": role.toJSON(),
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getRoles(let member):
            return [
                "member": member.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let member):
            return [
                "member": member.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updates:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateNotification(let member):
            return [
                "member": member.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .transferOwner(let guild, let target):
            return [
                "guild": guild.toJSON(),
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .addRole(let member_role):
            return [
                "member_role": member_role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateRole(let member_role):
            return [
                "member_role": member_role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .removeRole(let member_role):
            return [
                "member_role": member_role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON()
            ]
        case .updateIndex(let member):
            return [
                "member": member.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateIndexes(let members):
            return [
                "members": members.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}

