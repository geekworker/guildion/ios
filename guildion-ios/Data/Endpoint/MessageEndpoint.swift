//
//  MessageEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct messageResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum messageRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = messageResponse
    
    case getMessage(uid: String)
    case reads(messages: MessageEntities)
    case unreads(messages: MessageEntities)
    case getCurrentMessages(messageable_type: String, messageable_id: Int, limit: Int, is_static: Bool)
    case getIncrementMessages(messageable_type: String, messageable_id: Int, limit: Int, last_id: Int, is_static: Bool)
    case getDecrementMessages(messageable_type: String, messageable_id: Int, limit: Int, first_id: Int, is_static: Bool)
    case create(message: MessageEntity)
    case update(message: MessageEntity)
    case updateStatic(message: MessageEntity, is_static: Bool)
    case delete(message: MessageEntity)
    case createReaction(reaction: ReactionEntity)
    case deleteReaction(reaction: ReactionEntity)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getMessage: return "/api/v1/channel/message"
        case .reads: return "/api/v1/channel/messages/read"
        case .unreads: return "/api/v1/channel/messages/unread"
        case .getCurrentMessages: return "/api/v1/channel/messages/current"
        case .getIncrementMessages: return "/api/v1/channel/messages/increment"
        case .getDecrementMessages: return "/api/v1/channel/messages/decrement"
        case .create: return "/api/v1/channel/message/create"
        case .update: return "/api/v1/channel/message/update"
        case .updateStatic: return "/api/v1/channel/message/update/static"
        case .delete: return "/api/v1/channel/message/delete"
        case .createReaction: return "/api/v1/reaction/create"
        case .deleteReaction: return "/api/v1/reaction/delete"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getMessage(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .reads(let messages):
            return [
                "messages": messages.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .unreads(let messages):
            return [
                "messages": messages.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentMessages(let messageable_type, let messageable_id, let limit, let is_static):
            return [
                "is_static": is_static,
                "messageable_type": messageable_type,
                "messageable_id": messageable_id,
                "limit": "\(limit)",
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementMessages(let messageable_type, let messageable_id, let limit, let last_id, let is_static):
            return [
                "is_static": is_static,
                "messageable_type": messageable_type,
                "messageable_id": messageable_id,
                "limit": "\(limit)",
                "last_id": "\(last_id)",
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementMessages(let messageable_type, let messageable_id, let limit, let first_id, let is_static):
            return [
                "is_static": is_static,
                "messageable_type": messageable_type,
                "messageable_id": messageable_id,
                "limit": "\(limit)",
                "first_id": "\(first_id)",
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .create(let message):
            return [
                "message": message.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let message):
            return [
                "message": message.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateStatic(let message, let is_static):
            return [
                "is_static": is_static,
                "message": message.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let message):
            return [
                "message": message.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .createReaction(let reaction):
            return [
                "reaction": reaction.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .deleteReaction(let reaction):
            return [
                "reaction": reaction.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}
