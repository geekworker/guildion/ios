//
//  GuildEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct guildResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum guildRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = guildResponse
    
    case getGuild(uid: String)
    case getGuildWelcome(uid: String)
    case getGuildChannel(guild_uid: String, channel_uid: String)
    case getGuildChannels(uid: String)
    case getGuildRoles(uid: String)
    case getGuildRolesFrom(id: Int)
    case getUserGuilds(offset: Int, limit: Int)
    case getGuildMembers(guild: GuildEntity)
    case getCurrentGuildMembers(guild: GuildEntity, offset: Int, limit: Int)
    case getIncrementGuildMembers(guild: GuildEntity, offset: Int, limit: Int, last_id: Int)
    case getDecrementGuildMembers(guild: GuildEntity, offset: Int, limit: Int, first_id: Int)
    case getCurrentMemberRequests(guild: GuildEntity, offset: Int, limit: Int)
    case getIncrementMemberRequests(guild: GuildEntity, offset: Int, limit: Int, last_id: Int)
    case getDecrementMemberRequests(guild: GuildEntity, offset: Int, limit: Int, first_id: Int)
    case create(guild: GuildEntity)
    case update(guild: GuildEntity)
    case delete(guild: GuildEntity)
    case createSection(channel: SectionChannelEntity)
    case updateSection(channel: SectionChannelEntity)
    case sectionIndexUpdate(channel: SectionChannelEntity)
    case sectionIndexesUpdate(channels: SectionChannelEntities)
    case deleteSection(channel: SectionChannelEntity)
    case bump(guild: GuildEntity)
    case join(guild: GuildEntity)
    case leave(guild: GuildEntity)
    case block(guild: GuildEntity, target: UserEntity)
    case unblock(guild: GuildEntity, target: UserEntity)
    case kick(guild: GuildEntity, target: UserEntity)
    case report(guild: GuildEntity, description: String)
    case getBlocks(guild: GuildEntity, offset: Int, limit: Int)
    case createMemberRequest(guild: GuildEntity)
    case updateMemberRequest(guild: GuildEntity, target: UserEntity)
    case deleteMemberRequest(guild: GuildEntity, target: UserEntity)
    case updateIndex(guild: GuildEntity)
    case updateIndexes(guilds: GuildEntities)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getGuild: return "/api/v1/guild"
        case .getGuildWelcome: return "/api/v1/guild/welcome"
        case .getGuildChannel: return "/api/v1/guild/channel"
        case .getGuildChannels: return "/api/v1/guild/channels"
        case .getGuildRoles, .getGuildRolesFrom: return "/api/v1/guild/roles"
        case .getUserGuilds: return "/api/v1/user/guilds"
        case .getGuildMembers: return "/api/v1/guild/members"
        case .getCurrentGuildMembers: return "/api/v1/guild/members/current"
        case .getIncrementGuildMembers: return "/api/v1/guild/members/increment"
        case .getDecrementGuildMembers: return "/api/v1/guild/members/decrement"
        case .getCurrentMemberRequests: return "/api/v1/guild/member/requests/current"
        case .getIncrementMemberRequests: return "/api/v1/guild/member/requests/increment"
        case .getDecrementMemberRequests: return "/api/v1/guild/member/requests/decrement"
        case .create: return "/api/v1/guild/create"
        case .update: return "/api/v1/guild/update"
        case .delete: return "/api/v1/guild/delete"
        case .createSection: return "/api/v1/channel/section/create"
        case .updateSection: return "/api/v1/channel/section/update"
        case .sectionIndexUpdate: return "/api/v1/channel/section/index/update"
        case .sectionIndexesUpdate: return "/api/v1/channel/sections/index/update"
        case .deleteSection: return "/api/v1/channel/section/delete"
        case .bump: return "/api/v1/guild/bump"
        case .join: return "/api/v1/guild/join"
        case .leave: return "/api/v1/guild/leave"
        case .block: return "/api/v1/guild/block"
        case .unblock: return "/api/v1/guild/unblock"
        case .kick: return "/api/v1/guild/kick"
        case .report: return "/api/v1/guild/report"
        case .getBlocks: return "/api/v1/guild/blocks"
        case .createMemberRequest: return "/api/v1/guild/member/request/create"
        case .updateMemberRequest: return "/api/v1/guild/member/request/update"
        case .deleteMemberRequest: return "/api/v1/guild/member/request/delete"
        case .updateIndex: return "/api/v1/guild/index/update"
        case .updateIndexes: return "/api/v1/guilds/index/update"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getGuild(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getGuildWelcome(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getGuildRoles(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getGuildRolesFrom(let id):
            return [
                "id": id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getGuildChannel(let guild_uid, let channel_uid):
            return [
                "guild_uid": guild_uid,
                "channel_uid": channel_uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getGuildChannels(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getUserGuilds(let offset, let limit):
            return [
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getGuildMembers(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentGuildMembers(let guild, let offset, let limit):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementGuildMembers(let guild, let offset, let limit, let last_id):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementGuildMembers(let guild, let offset, let limit, let first_id):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentMemberRequests(let guild, let offset, let limit):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementMemberRequests(let guild, let offset, let limit, let last_id):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementMemberRequests(let guild, let offset, let limit, let first_id):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .create(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .createSection(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateSection(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .sectionIndexUpdate(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .sectionIndexesUpdate(let channels):
            return [
                "channels": channels.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .deleteSection(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .bump(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .join(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .leave(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .block(guild: let guild, target: let target):
            return [
                "guild": guild.toJSON(),
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .unblock(guild: let guild, target: let target):
            return [
                "guild": guild.toJSON(),
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .kick(guild: let guild, target: let target):
            return [
                "guild": guild.toJSON(),
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .report(let guild, let description):
            return [
                "guild": guild.toJSON(),
                "description": description,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getBlocks(guild: let guild, offset: let offset, limit: let limit):
            return [
                "guild": guild.toJSON(),
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .createMemberRequest(guild: let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateMemberRequest(guild: let guild, target: let target):
            return [
                "guild": guild.toJSON(),
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .deleteMemberRequest(guild: let guild, target: let target):
            return [
                "guild": guild.toJSON(),
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateIndex(let guild):
            return [
                "guild": guild.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateIndexes(let guilds):
            return [
                "guilds": guilds.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}


