//
//  RoleEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/07.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct roleResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum roleRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = roleResponse
    
    case getRole(uid: String)
    case create(role: RoleEntity)
    case update(role: RoleEntity)
    case delete(role: RoleEntity)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getRole: return "/api/v1/role"
        case .create: return "/api/v1/role/create"
        case .update: return "/api/v1/role/update"
        case .delete: return "/api/v1/role/delete"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getRole(let uid):
            return [
                "uid": uid,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .create(let role):
            return [
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let role):
            return [
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let role):
            return [
                "role": role.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}
