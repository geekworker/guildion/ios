//
//  AuthEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//


import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct authResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum authRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = authResponse
    
    case login(email: String, password: String)
    case syncUser
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .login: return "/api/v1/authenticate"
        case .syncUser: return "/api/v1/user/sync"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .login(
            let email,
            let password
        ):
            return [
                "email": "\(email)",
                "password": "\(password)",
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken
            ]
        
        case .syncUser:
        return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}

