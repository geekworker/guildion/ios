//
//  StreamEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct streamResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum streamRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = streamResponse
    
    case getStream(id :Int)
    case startLive(stream: StreamEntity, loggable: Bool)
    case switchLive(stream: StreamEntity, current_stream: StreamEntity)
    case updateParticipantState(participant: ParticipantEntity)
    case endLive(stream: StreamEntity)
    case join(channel: StreamChannelEntity)
    case leave(channel: StreamChannelEntity, duration: Float)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getStream: return "/api/v1/stream"
        case .startLive: return "/api/v1/stream/start"
        case .switchLive: return "/api/v1/stream/switch"
        case .updateParticipantState: return "/api/v1/stream/participant/update"
        case .endLive: return "/api/v1/stream/end"
        case .join: return "/api/v1/stream/join"
        case .leave: return "/api/v1/stream/leave"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getStream(let id):
            return [
                "id": id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .startLive(let stream, let loggable):
            return [
                "stream": stream.toJSON(),
                "loggable": loggable,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .switchLive(let stream, let current_stream):
            return [
                "stream": stream.toJSON(),
                "current_stream": current_stream.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateParticipantState(let participant):
            return [
                "participant": participant.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .endLive(let stream):
            return [
                "stream": stream.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .join(let channel):
            return [
                "channel": channel.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .leave(let channel, let duration):
            return [
                "channel": channel.toJSON(),
                "duration": "\(duration * 1000)",
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}
