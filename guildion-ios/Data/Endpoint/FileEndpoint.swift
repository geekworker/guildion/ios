//
//  FileEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct fileResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum fileRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = fileResponse
    
    case getFile(id :Int)
    case getCurrentUserFiles(limit: Int)
    case getIncrementUserFiles(limit: Int, last_id: Int)
    case getDecrementUserFiles(limit: Int, first_id: Int)
    case createReference(reference: FileReferenceEntity)
    case createReferences(references: FileReferenceEntities)
    case updateReference(reference: FileReferenceEntity)
    case deleteReference(reference: FileReferenceEntity)
    case deleteReferences(references: FileReferenceEntities)
    case creates(files: FileEntities)
    case create(file: FileEntity)
    case update(file: FileEntity)
    case delete(file: FileEntity)
    case deletes(files: FileEntities)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getFile: return "/api/v1/file"
        case .getCurrentUserFiles: return "/api/v1/user/files/current"
        case .getIncrementUserFiles: return "/api/v1/user/files/increment"
        case .getDecrementUserFiles: return "/api/v1/user/files/decrement"
        case .createReference: return "/api/v1/folder/file/create"
        case .createReferences: return "/api/v1/folder/files/create"
        case .updateReference: return "/api/v1/folder/file/update"
        case .deleteReference: return "/api/v1/folder/file/delete"
        case .deleteReferences: return "/api/v1/folder/files/delete"
        case .creates: return "/api/v1/files/create"
        case .create: return "/api/v1/file/create"
        case .update: return "/api/v1/file/update"
        case .delete: return "/api/v1/file/delete"
        case .deletes: return "/api/v1/files/delete"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getFile(let id):
            return [
                "id": id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentUserFiles(let limit):
            return [
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementUserFiles(let limit, let last_id):
            return [
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementUserFiles(let limit, let first_id):
            return [
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .createReference(let reference):
            return [
                "reference": reference.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .createReferences(let references):
            return [
                "references": references.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateReference(let reference):
            return [
                "reference": reference.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .deleteReference(let reference):
            return [
                "reference": reference.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .deleteReferences(let references):
            return [
                "references": references.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .creates(let files):
            return [
                "files": files.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .create(let file):
            return [
                "file": file.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .update(let file):
            return [
                "file": file.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .delete(let file):
            return [
                "file": file.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .deletes(let files):
            return [
                "files": files.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}

