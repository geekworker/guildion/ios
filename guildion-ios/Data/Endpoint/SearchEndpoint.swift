//
//  SearchEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct searchResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum searchRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = searchResponse
    
    case searchGuilds(keyword: String, limit: Int, offset: Int)
    case searchMessages(keyword: String, messageable_type: String, messageable_id: Int, limit: Int, offset: Int)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .searchGuilds: return "/api/v1/guilds/search"
        case .searchMessages: return "/api/v1/messages/search"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .searchGuilds(let keyword, let limit, let offset):
            return [
                "keyword": "\(keyword)",
                "limit": limit,
                "offset": offset,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .searchMessages(let keyword, let messageable_type, let messageable_id, let limit, let offset):
            return [
                "keyword": "\(keyword)",
                "messageable_id": messageable_id,
                "messageable_type": messageable_type,
                "limit": limit,
                "offset": offset,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}
