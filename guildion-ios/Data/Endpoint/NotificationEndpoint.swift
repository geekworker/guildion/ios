//
//  NotificationEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

//import UIKit
//import Alamofire
//import ObjectMapper
//import SwiftyJSON
//import RxCocoa
//import RxSwift
//import Hydra
//
//struct notificationResponse: BaseResponseProtocol {
//    var success: Bool?
//    var data: Map?
//    var error: Error = AppError.unknown
//    var result: Bool?
//    
//    init?(map: Map) { }
//    
//    mutating func mapping(map: Map) {
//        data = map
//        error = MappableError(map: map) ?? AppError.unknown
//        result <- map["result"]
//        success <- map["success"]
//    }
//    
//}
//
//enum notificationRequest: BaseRequestProtocol {
//    var headers: HTTPHeaders? {
//        switch self {
//        default: return HTTPHeaders()
//        }
//    }
//    typealias ResponseType = streamResponse
//    
//    case getUserNotifications(limit: Int, offset: Int)
//    case getNotification(notification: NotificationEntity)
//    case check(notification: NotificationEntity)
//    case checks(notifications: NotificationEntities)
//    
//    var method: HTTPMethod {
//        switch self {
//        default: return .post
//        }
//    }
//    
//    var path: String {
//        switch self {
//        case .getUserNotifications: return "/api/v1/notifications"
//        case .getNotification: return "/api/v1/notification"
//        case .check: return "/api/v1/notification/check"
//        case .checks: return "/api/v1/notifications/check"
//        }
//    }
//    
//    var parameters: Parameters? {
//        switch self {
//        case .getUserNotifications(let limit, let offset):
//            return [
//                "limit": "\(limit)",
//                "offset": "\(offset)"
//            ]
//        case .getNotification(let notification):
//            return [
//                "notification": notification.toJSON()
//            ]
//        case .check(let notification):
//            return [
//                "notification": notification.toJSON()
//            ]
//        case .checks(let notifications):
//            return [
//                "notifications": notifications.toUnCheckedJSON()
//            ]
//        }
//    }
//}
