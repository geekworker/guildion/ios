//
//  UserEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/05.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct userResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum userRequest: BaseRequestProtocol {
    var headers: HTTPHeaders? {
        switch self {
        default: return HTTPHeaders()
        }
    }
    typealias ResponseType = userResponse
    
    case getUser(id :Int)
    case updateUser(user: UserEntity)
    case updateUserEmailNotification(user: UserEntity)
    case deleteUser
    case registerUserDevise
    case updateUserDevise
    case logoutUserDevise
    case active
    case inActive
    case block(target: UserEntity)
    case unblock(target: UserEntity)
    case mute(target: UserEntity)
    case unmute(target: UserEntity)
    case getRelationStats
    case report(target: UserEntity, detail: String)
    case getBlocks(id: Int, offset: Int, limit: Int)
    case getMutes(id: Int, offset: Int, limit: Int)
    case getMembers
    case getCurrentMembers(offset: Int, limit: Int)
    case getIncrementMembers(offset: Int, limit: Int, last_id: Int)
    case getDecrementMembers(offset: Int, limit: Int, first_id: Int)
    
    var method: HTTPMethod {
        switch self {
        default: return .post
        }
    }
    
    var path: String {
        switch self {
        case .getUser: return "/api/v1/user"
        case .updateUser: return "/api/v1/user/update"
        case .updateUserEmailNotification: return "/api/v1/user/notification/email/update"
        case .deleteUser: return "/api/v1/user/delete"
        case .registerUserDevise: return "/api/v1/user/devise/create"
        case .updateUserDevise: return "/api/v1/user/devise/update"
        case .logoutUserDevise: return "/api/v1/user/devise/logout"
        case .active: return "/api/v1/user/active"
        case .inActive: return "/api/v1/user/inactive"
        case .block: return "/api/v1/user/block"
        case .unblock: return "/api/v1/user/unblock"
        case .mute: return "/api/v1/user/mute"
        case .unmute: return "/api/v1/user/unmute"
        case .report: return "/api/v1/user/report"
        case .getBlocks: return "/api/v1/user/blocks"
        case .getMutes: return "/api/v1/user/mutes"
        case .getMembers: return "/api/v1/user/members"
        case .getCurrentMembers: return "/api/v1/user/members/current"
        case .getIncrementMembers: return "/api/v1/user/members/increment"
        case .getDecrementMembers: return "/api/v1/user/members/decrement"
        case .getRelationStats: return "/api/v1/user/relation/stats"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getUser(let id):
            return [
                "id": id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateUser(let user):
            return [
                "user": user.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateUserEmailNotification(let user):
            return [
                "user": user.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .deleteUser:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .registerUserDevise:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .updateUserDevise:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .logoutUserDevise:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .active:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .inActive:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .block(let target):
            return [
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            
        case .unblock(let target):
            return [
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        
        case .mute(let target):
            return [
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            
        case .unmute(let target):
            return [
                "target": target.toJSON(),
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        
        case .report(let target, let detail):
            return [
                "target": target.toJSON(),
                "description": detail,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            
        case .getBlocks(let id, let offset, let limit):
            return [
                "id": id,
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
            
        case .getMutes(let id, let offset, let limit):
            return [
                "id": id,
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getMembers:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getCurrentMembers(let offset, let limit):
            return [
                "offset": offset,
                "limit": limit,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getIncrementMembers(let offset, let limit, let last_id):
            return [
                "offset": offset,
                "limit": limit,
                "last_id": last_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getDecrementMembers(let offset, let limit, let first_id):
            return [
                "offset": offset,
                "limit": limit,
                "first_id": first_id,
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        case .getRelationStats:
            return [
                "accessToken": ClientDevise.getClientDevise().accessToken,
                "devise": ClientDevise_DeviseEntityTranslator.translate(ClientDevise.getClientDevise()).toJSON(),
            ]
        }
    }
}
