//
//  SignalingEndpoint.swift
//  guildion-ios
//
//  Created by Apple on 2021/02/21.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

struct signalingResponse: BaseResponseProtocol {
    var success: Bool?
    var data: Map?
    var result: Bool?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data = map
        result <- map["result"]
        success <- map["success"]
    }
    
}

enum signalingRequest: BaseRequestProtocol {
    var baseURL: URL {
        return URL(string: Constant.SIGNALING_APP_URL)!
    }
    var headers: HTTPHeaders? {
        var header = HTTPHeaders()
        header.add(name: "API_KEY", value: ENV.SIGNALING.KEY)
        header.add(name: "API_SECRET", value: ENV.SIGNALING.PASSWORD)
        return header
    }
    typealias ResponseType = sessionResponse
    
    case getRoomMembersCount(roomname: String)
    
    var method: HTTPMethod {
        switch self {
        default: return .get
        }
    }
    
    var path: String {
        switch self {
        case .getRoomMembersCount(let roomname): return "/api/v1/room/\(roomname)/members/count"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getRoomMembersCount(_):
            return [:]
        }
    }
}
