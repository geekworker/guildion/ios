//
//  TextChannelDataStore.swift
//  textee-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class TextChannelDataStore: DataStoreImpl {
    static var shared: TextChannelDataStore = TextChannelDataStore()
    private let disposeBag = DisposeBag()
    
    func getTextChannel(uid: String) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.getTextChannel(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entity = TextChannelEntity()
                    all(entity.asyncMapping(map: data["channel"])).then(in: .main, { _ in resolve(entity) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getTextChannelRoles(uid: String) -> Promise<TextChannelRoleEntities> {
        return Promise<TextChannelRoleEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.getTextChannelRoles(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let entities = TextChannelRoleEntities(map: data["roles"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(entities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getUserGuildTextChannels(guild: GuildEntity, offset: Int, limit: Int) -> Promise<TextChannelEntities> {
        return Promise<TextChannelEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.getUserGuildTextChannels(guild: guild, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let textChannelEntities = TextChannelEntities(map: data["channels"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(textChannelEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.getCurrentTextChannelMembers(channel: channel, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.getIncrementTextChannelMembers(channel: channel, offset: offset, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementTextChannelMembers(channel: TextChannelEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.getDecrementTextChannelMembers(channel: channel, offset: offset, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getTextChannelInvitableMembers(channel: TextChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.getTextChannelInvitableMembers(channel: channel, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let memberEntities = MemberEntities(map: data["members"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(memberEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.create(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: TextChannelEntity?
                    channel <- data["channel"]
                    guard let textChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(textChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.update(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: TextChannelEntity?
                    channel <- data["channel"]
                    guard let textChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(textChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: TextChannelEntity, permission: Bool) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.updatePermission(channel: channel, permission: permission)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: TextChannelEntity?
                    channel <- data["channel"]
                    guard let textChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(textChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(entry: TextChannelEntryEntity) -> Promise<TextChannelEntryEntity> {
        return Promise<TextChannelEntryEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.updateEntry(entry: entry)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: TextChannelEntryEntity?
                    entity <- data["entry"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func sectionIndexUpdate(channel: TextChannelEntity) -> Promise<TextChannelEntity> {
        return Promise<TextChannelEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.sectionIndexUpdate(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: TextChannelEntity?
                    channel <- data["channel"]
                    guard let textChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(textChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func sectionIndexesUpdate(channels: TextChannelEntities) -> Promise<TextChannelEntities> {
        return Promise<TextChannelEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.sectionIndexesUpdate(channels: channels)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = TextChannelEntities()
                    all(entities.asyncMapping(map: data["channels"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(channel: TextChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.delete(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func invite(channel: TextChannelEntity, targets: MemberEntities) -> Promise<TextChannelEntryEntities> {
        return Promise<TextChannelEntryEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.invite(channel: channel, targets: targets)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwraeppdEntities = TextChannelEntryEntities.init(map: data["entries"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwraeppdEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func kick(channel: TextChannelEntity, targets: MemberEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.kick(channel: channel, targets: targets)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(channel: TextChannelEntity, role: RoleEntity) -> Promise<TextChannelRoleEntity> {
        return Promise<TextChannelRoleEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.addRole(channel: channel, role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: TextChannelRoleEntity?
                    entity <- data["channel_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel_role: TextChannelRoleEntity) -> Promise<TextChannelRoleEntity> {
        return Promise<TextChannelRoleEntity> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.updateRole(channel_role: channel_role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: TextChannelRoleEntity?
                    entity <- data["channel_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(channel: TextChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.removeRole(channel: channel, role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func leave(channel: TextChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.leave(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func reads(channel: TextChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> (in: .background) { resolve, reject, _ in
            let request = textChannelRequest.reads(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwraeppdEntities = ReadEntities.init(map: data["reads"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwraeppdEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
}
