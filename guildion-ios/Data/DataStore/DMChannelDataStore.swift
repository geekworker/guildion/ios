//
//  DMChannelDataStore.swift
//  dmee-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class DMChannelDataStore: DataStoreImpl {
    static var shared: DMChannelDataStore = DMChannelDataStore()
    private let disposeBag = DisposeBag()
    
    func getDMChannel(uid: String) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.getDMChannel(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entity = DMChannelEntity()
                    all(entity.asyncMapping(map: data["channel"])).then(in: .main, { _ in resolve(entity) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDMChannelRoles(uid: String) -> Promise<DMChannelRoleEntities> {
        return Promise<DMChannelRoleEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.getDMChannelRoles(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let entities = DMChannelRoleEntities(map: data["roles"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(entities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getUserGuildDMChannels(guild: GuildEntity, offset: Int, limit: Int) -> Promise<DMChannelEntities> {
        return Promise<DMChannelEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.getUserGuildDMChannels(guild: guild, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let dmChannelEntities = DMChannelEntities(map: data["channels"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(dmChannelEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.getCurrentDMChannelMembers(channel: channel, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.getIncrementDMChannelMembers(channel: channel, offset: offset, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementDMChannelMembers(channel: DMChannelEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.getDecrementDMChannelMembers(channel: channel, offset: offset, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDMChannelInvitableMembers(channel: DMChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.getDMChannelInvitableMembers(channel: channel, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let memberEntities = MemberEntities(map: data["members"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(memberEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func start(guild: GuildEntity, target: MemberEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.start(guild: guild, target: target)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: DMChannelEntity?
                    channel <- data["channel"]
                    guard let dmChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(dmChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.create(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: DMChannelEntity?
                    channel <- data["channel"]
                    guard let dmChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(dmChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.update(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: DMChannelEntity?
                    channel <- data["channel"]
                    guard let dmChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(dmChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: DMChannelEntity, permission: Bool) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.updatePermission(channel: channel, permission: permission)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: DMChannelEntity?
                    channel <- data["channel"]
                    guard let dmChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(dmChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(entry: DMChannelEntryEntity) -> Promise<DMChannelEntryEntity> {
        return Promise<DMChannelEntryEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.updateEntry(entry: entry)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: DMChannelEntryEntity?
                    entity <- data["entry"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func sectionIndexUpdate(channel: DMChannelEntity) -> Promise<DMChannelEntity> {
        return Promise<DMChannelEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.sectionIndexUpdate(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: DMChannelEntity?
                    channel <- data["channel"]
                    guard let dmChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(dmChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(channel: DMChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.delete(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func invite(channel: DMChannelEntity, targets: MemberEntities) -> Promise<DMChannelEntryEntities> {
        return Promise<DMChannelEntryEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.invite(channel: channel, targets: targets)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwraeppdEntities = DMChannelEntryEntities.init(map: data["entries"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwraeppdEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func kick(channel: DMChannelEntity, targets: MemberEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.kick(channel: channel, targets: targets)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(channel: DMChannelEntity, role: RoleEntity) -> Promise<DMChannelRoleEntity> {
        return Promise<DMChannelRoleEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.addRole(channel: channel, role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: DMChannelRoleEntity?
                    entity <- data["channel_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel_role: DMChannelRoleEntity) -> Promise<DMChannelRoleEntity> {
        return Promise<DMChannelRoleEntity> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.updateRole(channel_role: channel_role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: DMChannelRoleEntity?
                    entity <- data["channel_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(channel: DMChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.removeRole(channel: channel, role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func leave(channel: DMChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.leave(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func reads(channel: DMChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> (in: .background) { resolve, reject, _ in
            let request = dmChannelRequest.reads(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwraeppdEntities = ReadEntities.init(map: data["reads"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwraeppdEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
}
