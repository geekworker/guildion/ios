//
//  StreamChannelDataStore.swift
//  streamee-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class StreamChannelDataStore: DataStoreImpl {
    static var shared: StreamChannelDataStore = StreamChannelDataStore()
    private let disposeBag = DisposeBag()
    
    func getStreamChannelFromUID(uid: String) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getStreamChannelFromUID(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entity = StreamChannelEntity()
                    all(entity.asyncMapping(map: data["channel"])).then(in: .main, { _ in resolve(entity) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getStreamChannelRoles(uid: String) -> Promise<StreamChannelRoleEntities> {
        return Promise<StreamChannelRoleEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getStreamChannelRoles(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let entities = StreamChannelRoleEntities(map: data["roles"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(entities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getStreamChannelParticipants(uid: String) -> Promise<ParticipantEntities> {
        return Promise<ParticipantEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getStreamChannelParticipants(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let entities = ParticipantEntities(map: data["participants"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(entities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getStreamChannel(id: Int) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getStreamChannel(id :id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entity = StreamChannelEntity()
                    all(entity.asyncMapping(map: data["channel"])).then(in: .main, { _ in resolve(entity) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getUserGuildStreamChannels(guild: GuildEntity, offset: Int, limit: Int) -> Promise<StreamChannelEntities> {
        return Promise<StreamChannelEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getUserGuildStreamChannels(guild: guild, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let streamChannelEntities = StreamChannelEntities(map: data["channels"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(streamChannelEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getCurrentStreamChannelMembers(channel: channel, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getIncrementStreamChannelMembers(channel: channel, offset: offset, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementStreamChannelMembers(channel: StreamChannelEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getDecrementStreamChannelMembers(channel: channel, offset: offset, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncChannelMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getStreamChannelInvitableMembers(channel: StreamChannelEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.getStreamChannelInvitableMembers(channel: channel, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let memberEntities = MemberEntities(map: data["members"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(memberEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func start(guild: GuildEntity, target: MemberEntity?, stream: StreamEntity?, voice_only: Bool, movie_only: Bool) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.start(guild: guild, target: target, stream: stream, voice_only: voice_only, movie_only: movie_only)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: StreamChannelEntity?
                    channel <- data["channel"]
                    guard let streamChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(streamChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.create(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: StreamChannelEntity?
                    channel <- data["channel"]
                    guard let streamChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(streamChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.update(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: StreamChannelEntity?
                    channel <- data["channel"]
                    guard let streamChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(streamChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: StreamChannelEntity, permission: Bool) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.updatePermission(channel: channel, permission: permission)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: StreamChannelEntity?
                    channel <- data["channel"]
                    guard let streamChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(streamChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: StreamChannelEntity, is_loop: Bool) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.updateLoop(channel: channel, is_loop: is_loop)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: StreamChannelEntity?
                    channel <- data["channel"]
                    guard let streamChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(streamChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(entry: StreamChannelEntryEntity) -> Promise<StreamChannelEntryEntity> {
        return Promise<StreamChannelEntryEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.updateEntry(entry: entry)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: StreamChannelEntryEntity?
                    entity <- data["entry"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func sectionIndexUpdate(channel: StreamChannelEntity) -> Promise<StreamChannelEntity> {
        return Promise<StreamChannelEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.sectionIndexUpdate(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: StreamChannelEntity?
                    channel <- data["channel"]
                    guard let streamChannelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(streamChannelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func sectionIndexesUpdate(channels: StreamChannelEntities) -> Promise<StreamChannelEntities> {
        return Promise<StreamChannelEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.sectionIndexesUpdate(channels: channels)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = StreamChannelEntities()
                    all(entities.asyncMapping(map: data["channels"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(channel: StreamChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.delete(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func invite(channel: StreamChannelEntity, targets: MemberEntities) -> Promise<StreamChannelEntryEntities> {
        return Promise<StreamChannelEntryEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.invite(channel: channel, targets: targets)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwraeppdEntities = StreamChannelEntryEntities.init(map: data["entries"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwraeppdEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func kick(channel: StreamChannelEntity, targets: MemberEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.kick(channel: channel, targets: targets)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(channel: StreamChannelEntity, role: RoleEntity) -> Promise<StreamChannelRoleEntity> {
        return Promise<StreamChannelRoleEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.addRole(channel: channel, role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: StreamChannelRoleEntity?
                    entity <- data["channel_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel_role: StreamChannelRoleEntity) -> Promise<StreamChannelRoleEntity> {
        return Promise<StreamChannelRoleEntity> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.updateRole(channel_role: channel_role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: StreamChannelRoleEntity?
                    entity <- data["channel_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(channel: StreamChannelEntity, role: RoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.removeRole(channel: channel, role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func leave(channel: StreamChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.leave(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func reads(channel: StreamChannelEntity) -> Promise<ReadEntities> {
        return Promise<ReadEntities> (in: .background) { resolve, reject, _ in
            let request = streamChannelRequest.reads(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwraeppdEntities = ReadEntities.init(map: data["reads"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwraeppdEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
}


