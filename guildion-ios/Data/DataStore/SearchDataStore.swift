//
//  SearchDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class SearchDataStore: DataStoreImpl {
    static var shared: SearchDataStore = SearchDataStore()
    private let disposeBag = DisposeBag()
    
    func searchGuilds(keyword: String, limit: Int, offset: Int) -> Promise<GuildEntities> {
        return Promise<GuildEntities> (in: .background) { resolve, reject, _ in
            let request = searchRequest.searchGuilds(keyword: keyword, limit: limit, offset: offset)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let guilds = GuildEntities()
                    all(guilds.asyncFastSearchMapping(map: data["guilds"])).then(in: .main, { _ in resolve(guilds) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func searchMessages(keyword: String, messageable_type: String, messageable_id: Int, limit: Int, offset: Int) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            let request = searchRequest.searchMessages(keyword: keyword, messageable_type: messageable_type, messageable_id: messageable_id, limit: limit, offset: offset)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let messageEntities = MessageEntities(map: data["messages"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(messageEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
}

