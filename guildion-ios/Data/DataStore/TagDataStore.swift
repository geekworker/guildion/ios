//
//  TagDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class TagDataStore: DataStoreImpl {
    static var shared: TagDataStore = TagDataStore()
    private let disposeBag = DisposeBag()
    
    func getTags(limit: Int, offset: Int) -> Promise<TagEntities> {
        return Promise<TagEntities> (in: .background) { resolve, reject, _ in
            let request = tagRequest.getTags(limit: limit, offset: offset)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = TagEntities()
                    all(entities.asyncPureMapping(map: data["tags"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
}


