//
//  GuildDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class GuildDataStore: DataStoreImpl {
    static var shared: GuildDataStore = GuildDataStore()
    private let disposeBag = DisposeBag()
    
    func getGuild(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getGuild(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let guild = GuildEntity()
                    all(guild.asyncFastMapping(map: data["guild"])).then(in: .main, { _ in resolve(guild) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getGuildWelcome(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getGuildWelcome(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let guild = GuildEntity()
                    all(guild.asyncFastSearchMapping(map: data["guild"])).then(in: .main, { _ in resolve(guild) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getGuildChannel(guild_uid: String, channel_uid: String) -> Promise<(guild: GuildEntity, channel: MessageableEntity, member: MemberEntity)> {
        return Promise<(guild: GuildEntity, channel: MessageableEntity, member: MemberEntity)> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getGuildChannel(guild_uid: guild_uid, channel_uid: channel_uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let guild = GuildEntity()
                    let member = MemberEntity()
                    let typeString: String? = data["channel_type"].value()
                    guard let unwrapped = typeString, let messageable_type = MessageableType(rawValue: unwrapped) else { reject(AppError.unknown); return }
                    var promises = guild.asyncFastMapping(map: data["guild"]) + member.asyncFastMapping(map: data["member"])
                    switch messageable_type {
                    case .TextChannel:
                        let channel = TextChannelEntity()
                        promises += channel.asyncFastMapping(map: data["channel"])
                        all(promises).then(in: .main, { _ in resolve((guild: guild, channel: channel, member: member)) }).catch(in: .background, { reject($0) })
                    case .StreamChannel:
                        let channel = StreamChannelEntity()
                        promises += channel.asyncFastMapping(map: data["channel"])
                        all(promises).then(in: .main, { _ in resolve((guild: guild, channel: channel, member: member)) }).catch(in: .background, { reject($0) })
                    case .DMChannel:
                        let channel = DMChannelEntity()
                        promises += channel.asyncFastMapping(map: data["channel"])
                        all(promises).then(in: .main, { _ in resolve((guild: guild, channel: channel, member: member)) }).catch(in: .background, { reject($0) })
                    }
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getGuildChannels(uid: String) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getGuildChannels(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var guild: GuildEntity?
                    guild <- data["guild"]
                    guard let guildEntity = guild else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(guildEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getGuildRoles(uid: String) -> Promise<RoleEntities> {
        return Promise<RoleEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getGuildRoles(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = RoleEntities()
                    all(entities.asyncPureMapping(map: data["roles"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getUserGuilds(offset: Int, limit: Int) -> Promise<GuildEntities> {
        return Promise<GuildEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getUserGuilds(offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let guilds = GuildEntities()
                    all(guilds.asyncPureMapping(map: data["guilds"])).then(in: .main, { _ in resolve(guilds) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getGuildMembers(guild: GuildEntity) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getGuildMembers(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncPureMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentGuildMembers(guild: GuildEntity, offset: Int, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getCurrentGuildMembers(guild: guild, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncPureMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementGuildMembers(guild: GuildEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getIncrementGuildMembers(guild: guild, offset: offset, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncPureMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementGuildMembers(guild: GuildEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getDecrementGuildMembers(guild: guild, offset: offset, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncPureMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentMemberRequests(guild: GuildEntity, offset: Int, limit: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getCurrentMemberRequests(guild: guild, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = MemberRequestEntities()
                    all(entities.asyncFastMapping(map: data["requests"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementMemberRequests(guild: GuildEntity, offset: Int, limit: Int, last_id: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getIncrementMemberRequests(guild: guild, offset: offset, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = MemberRequestEntities()
                    all(entities.asyncFastMapping(map: data["requests"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementMemberRequests(guild: GuildEntity, offset: Int, limit: Int, first_id: Int) -> Promise<MemberRequestEntities> {
        return Promise<MemberRequestEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getDecrementMemberRequests(guild: guild, offset: offset, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = MemberRequestEntities()
                    all(entities.asyncFastMapping(map: data["requests"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.create(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var guild: GuildEntity?
                    guild <- data["guild"]
                    guard let guildEntity = guild else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(guildEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.update(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var guild: GuildEntity?
                    guild <- data["guild"]
                    guard let guildEntity = guild else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(guildEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(guild: GuildEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.delete(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func join(guild: GuildEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.join(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var member: MemberEntity?
                    member <- data["member"]
                    guard let memberEntity = member else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(memberEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func leave(guild: GuildEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.leave(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.createSection(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: SectionChannelEntity?
                    channel <- data["channel"]
                    guard let channelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(channelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.updateSection(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: SectionChannelEntity?
                    channel <- data["channel"]
                    guard let channelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(channelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func sectionIndexUpdate(channel: SectionChannelEntity) -> Promise<SectionChannelEntity> {
        return Promise<SectionChannelEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.sectionIndexUpdate(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var channel: SectionChannelEntity?
                    channel <- data["channel"]
                    guard let channelEntity = channel else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(channelEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func sectionIndexesUpdate(channels: SectionChannelEntities) -> Promise<SectionChannelEntities> {
        return Promise<SectionChannelEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.sectionIndexesUpdate(channels: channels)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = SectionChannelEntities()
                    all(entities.asyncMapping(map: data["channels"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                    resolve(entities)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(channel: SectionChannelEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.deleteSection(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func bump(guild: GuildEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.bump(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: GuildEntity?
                    entity <- data["guild"]
                    guard let guildEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(guildEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func block(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.block(guild: guild, target: target)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func unblock(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.unblock(guild: guild, target: target)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func kick(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.kick(guild: guild, target: target)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func report(guild: GuildEntity, description: String) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.report(guild: guild, description: description)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getBlocks(guild: GuildEntity, offset: Int, limit: Int) -> Promise<UserEntities> {
        return Promise<UserEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.getBlocks(guild: guild, offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwrappedEntities = UserEntities(map: data["users"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func createMemberRequest(guild: GuildEntity) -> Promise<MemberRequestEntity> {
        return Promise<MemberRequestEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.createMemberRequest(guild: guild)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: MemberRequestEntity?
                    entity <- data["request"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateMemberRequest(guild: GuildEntity, target: UserEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            let request = guildRequest.updateMemberRequest(guild: guild, target: target)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: MemberEntity?
                    entity <- data["member"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func deleteMemberRequest(guild: GuildEntity, target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = guildRequest.deleteMemberRequest(guild: guild, target: target)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateIndexes(guilds: GuildEntities) -> Promise<GuildEntities> {
        return Promise<GuildEntities> (in: .background) { resolve, reject, _ in
            let request = guildRequest.updateIndexes(guilds: guilds)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let guilds = GuildEntities()
                    all(guilds.asyncPureMapping(map: data["guilds"])).then(in: .main, { _ in resolve(guilds) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
}
