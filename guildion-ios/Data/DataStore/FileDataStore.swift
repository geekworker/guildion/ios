//
//  FileDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class FileDataStore: DataStoreImpl {
    static var shared: FileDataStore = FileDataStore()
    private let disposeBag = DisposeBag()
    
    func getFile(id: Int) -> Promise<FileEntity> {
        return Promise<FileEntity> (in: .background) { resolve, reject, _ in
            let request = fileRequest.getFile(id :id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var file: FileEntity?
                    file <- data["file"]
                    guard let fileEntity = file else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentUserFiles(limit: Int) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = fileRequest.getCurrentUserFiles(limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let files = FileEntities()
                    all(files.asyncMapping(map: data["files"])).then(in: .main, { _ in resolve(files) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementUserFiles(limit: Int, last_id: Int) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = fileRequest.getIncrementUserFiles(limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let files = FileEntities()
                    all(files.asyncMapping(map: data["files"])).then(in: .main, { _ in resolve(files) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementUserFiles(limit: Int, first_id: Int) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = fileRequest.getDecrementUserFiles(limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let files = FileEntities()
                    all(files.asyncMapping(map: data["files"])).then(in: .main, { _ in resolve(files) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func createReference(reference: FileReferenceEntity) -> Promise<FileReferenceEntity> {
        return Promise<FileReferenceEntity> (in: .background) { resolve, reject, _ in
            let request = fileRequest.createReference(reference: reference)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var file: FileReferenceEntity?
                    file <- data["reference"]
                    guard let fileEntity = file else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func createReferences(references: FileReferenceEntities) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            let request = fileRequest.createReferences(references: references)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let fileReferenceEntities = FileReferenceEntities(map: data["references"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileReferenceEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateReference(reference: FileReferenceEntity) -> Promise<FileReferenceEntity> {
        return Promise<FileReferenceEntity> (in: .background) { resolve, reject, _ in
            let request = fileRequest.updateReference(reference: reference)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var file: FileReferenceEntity?
                    file <- data["reference"]
                    guard let fileEntity = file else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func deleteReference(reference: FileReferenceEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = fileRequest.deleteReference(reference: reference)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func deleteReferences(references: FileReferenceEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = fileRequest.deleteReferences(references: references)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func creates(files: FileEntities) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = fileRequest.creates(files: files)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let fileEntities = FileEntities(map: data["files"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntities)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(file: FileEntity) -> Promise<FileEntity> {
        return Promise<FileEntity> (in: .background) { resolve, reject, _ in
            let request = fileRequest.create(file: file)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var file: FileEntity?
                    file <- data["file"]
                    guard let fileEntity = file else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(file: FileEntity) -> Promise<FileEntity> {
        return Promise<FileEntity> (in: .background) { resolve, reject, _ in
            let request = fileRequest.update(file: file)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var file: FileEntity?
                    file <- data["file"]
                    guard let fileEntity = file else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(file: FileEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = fileRequest.delete(file: file)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func deletes(files: FileEntities) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = fileRequest.deletes(files: files)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
}

