//
//  FolderDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class FolderDataStore: DataStoreImpl {
    static var shared: FolderDataStore = FolderDataStore()
    private let disposeBag = DisposeBag()
    
    func getFolder(id: Int) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getFolder(id: id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var folder: FolderEntity?
                    folder <- data["folder"]
                    guard let folderEntity = folder else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(folderEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getFolders(guild_id: Int, limit: Int, offset: Int, is_playlist: Bool) -> Promise<FolderEntities> {
        return Promise<FolderEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getFolders(guild_id: guild_id, limit: limit, offset: offset, is_playlist: is_playlist)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let folders = FolderEntities()
                    all(folders.asyncMapping(map: data["folders"])).then(in: .main, { _ in resolve(folders) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentFolderReferences(folder_id: Int, limit: Int) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getCurrentFolderReferences(folder_id: folder_id, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = FileReferenceEntities()
                    all(entities.asyncMapping(map: data["references"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementFolderReferences(folder_id: Int, limit: Int, last_id: Int) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getIncrementFolderReferences(folder_id: folder_id, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = FileReferenceEntities()
                    all(entities.asyncMapping(map: data["references"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementFolderReferences(folder_id: Int, limit: Int, first_id: Int) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getDecrementFolderReferences(folder_id: folder_id, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = FileReferenceEntities()
                    all(entities.asyncMapping(map: data["references"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentGuildFiles(guild_id: Int, limit: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getCurrentGuildFiles(guild_id: guild_id, limit: limit, is_playlist: is_playlist)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = FileEntities()
                    all(entities.asyncMapping(map: data["files"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementGuildFiles(guild_id: Int, limit: Int, last_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getIncrementGuildFiles(guild_id: guild_id, limit: limit, last_id: last_id, is_playlist: is_playlist)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = FileEntities()
                    all(entities.asyncMapping(map: data["files"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementGuildFiles(guild_id: Int, limit: Int, first_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getDecrementGuildFiles(guild_id: guild_id, limit: limit, first_id: first_id, is_playlist: is_playlist)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = FileEntities()
                    all(entities.asyncMapping(map: data["files"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentMemberFiles(guild_id: Int, limit: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getCurrentMemberFiles(guild_id: guild_id, limit: limit, is_playlist: is_playlist)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let fileEntities = FileEntities(map: data["files"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementMemberFiles(guild_id: Int, limit: Int, last_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getIncrementMemberFiles(guild_id: guild_id, limit: limit, last_id: last_id, is_playlist: is_playlist)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let fileEntities = FileEntities(map: data["files"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementMemberFiles(guild_id: Int, limit: Int, first_id: Int, is_playlist: Bool) -> Promise<FileEntities> {
        return Promise<FileEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.getDecrementMemberFiles(guild_id: guild_id, limit: limit, first_id: first_id, is_playlist: is_playlist)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let fileEntities = FileEntities(map: data["files"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(fileEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            let request = folderRequest.create(folder: folder)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var folder: FolderEntity?
                    folder <- data["folder"]
                    guard let folderEntity = folder else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(folderEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            let request = folderRequest.update(folder: folder)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var folder: FolderEntity?
                    folder <- data["folder"]
                    guard let folderEntity = folder else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(folderEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(folder: FolderEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = folderRequest.delete(folder: folder)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateIndex(reference: FileReferenceEntity) -> Promise<FileReferenceEntity> {
        return Promise<FileReferenceEntity> (in: .background) { resolve, reject, _ in
            let request = folderRequest.updateIndex(reference: reference)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: FileReferenceEntity?
                    entity <- data["reference"]
                    guard let unwrapped = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateIndexes(references: FileReferenceEntities) -> Promise<FileReferenceEntities> {
        return Promise<FileReferenceEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.updateIndexes(references: references)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwrapped = FileReferenceEntities.init(map: data["references"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateIndex(folder: FolderEntity) -> Promise<FolderEntity> {
        return Promise<FolderEntity> (in: .background) { resolve, reject, _ in
            let request = folderRequest.updateFolderIndex(folder: folder)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: FolderEntity?
                    entity <- data["folder"]
                    guard let unwrapped = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateIndexes(folders: FolderEntities) -> Promise<FolderEntities> {
        return Promise<FolderEntities> (in: .background) { resolve, reject, _ in
            let request = folderRequest.updateFolderIndexes(folders: folders)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwrapped = FolderEntities.init(map: data["folders"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
}

