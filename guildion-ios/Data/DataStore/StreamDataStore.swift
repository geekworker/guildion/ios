//
//  StreamDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class StreamDataStore: DataStoreImpl {
    static var shared: StreamDataStore = StreamDataStore()
    private let disposeBag = DisposeBag()
    
    func getStream(id :Int) -> Promise<StreamEntity> {
        return Promise<StreamEntity> (in: .background) { resolve, reject, _ in
            let request = streamRequest.getStream(
                id: id
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entity = StreamEntity()
                    all(entity.asyncMapping(map: data["stream"])).then(in: .main, { _ in resolve(entity) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }

    func startLive(stream: StreamEntity, loggable: Bool) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> (in: .background) { resolve, reject, _ in
            let request = streamRequest.startLive(
                stream: stream,
                loggable: loggable
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let stream = StreamEntity()
                    let participant = ParticipantEntity()
                    all(
                        stream.asyncMapping(
                            map: data["stream"]) + participant.asyncMapping(map: data["participant"]
                        )
                    ).then(in: .main, { _ in resolve((stream: stream, participant: participant)) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func endLive(stream: StreamEntity) -> Promise<StreamEntity> {
        return Promise<StreamEntity> (in: .background) { resolve, reject, _ in
            let request = streamRequest.endLive(
                stream: stream
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let stream = StreamEntity()
                    all(stream.asyncMapping(map: data["stream"])).then(in: .main, { _ in resolve(stream) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func switchLive(stream: StreamEntity, current_stream: StreamEntity) -> Promise<StreamEntity> {
        return Promise<StreamEntity> (in: .background) { resolve, reject, _ in
            let request = streamRequest.switchLive(stream: stream, current_stream: current_stream)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let stream = StreamEntity()
                    all(stream.asyncMapping(map: data["stream"])).then(in: .main, { _ in resolve(stream) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateParticipantState(participant: ParticipantEntity) -> Promise<ParticipantEntity> {
        return Promise<ParticipantEntity> (in: .background) { resolve, reject, _ in
            let request = streamRequest.updateParticipantState(participant: participant)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let participant = ParticipantEntity()
                    all(participant.asyncMapping(map: data["participant"])).then(in: .main, { _ in resolve(participant) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func join(channel: StreamChannelEntity) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> (in: .background) { resolve, reject, _ in
            let request = streamRequest.join(channel: channel)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let stream = StreamEntity()
                    let participant = ParticipantEntity()
                    all(
                        stream.asyncMapping(
                            map: data["stream"]) + participant.asyncMapping(map: data["participant"]
                        )
                    ).then(in: .main, { _ in resolve((stream: stream, participant: participant)) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func leave(channel: StreamChannelEntity, duration: Float) -> Promise<(stream: StreamEntity, participant: ParticipantEntity)> {
        return Promise<(stream: StreamEntity, participant: ParticipantEntity)> (in: .background) { resolve, reject, _ in
            let request = streamRequest.leave(channel: channel, duration: duration)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let stream = StreamEntity()
                    let participant = ParticipantEntity()
                    all(
                        stream.asyncMapping(
                            map: data["stream"]) + participant.asyncMapping(map: data["participant"]
                        )
                    ).then(in: .main, { _ in resolve((stream: stream, participant: participant)) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
}
