//
//  SessionDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class SessionDataStore: DataStoreImpl {
    static var shared: SessionDataStore = SessionDataStore()
    private let disposeBag = DisposeBag()
    
    func findOneIdentityRequest(accessToken: String) -> Promise<IdentityEntity> {
        return Promise<IdentityEntity> (in: .background) { resolve, reject, _ in
            let request = sessionRequest.findOneIdentityRequest(
                accessToken: accessToken
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var identity: IdentityEntity?
                    identity <- data["identity"]
                    guard let identityEntity = identity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(identityEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func generateAccessToken(accessToken: String, is_one_time: Bool) -> Promise<String> {
        return Promise<String> (in: .background) { resolve, reject, _ in
            let request = sessionRequest.generateAccessToken(
                accessToken: accessToken,
                is_one_time: is_one_time
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var accessToken: String?
                    accessToken <- data["accessToken"]
                    guard let unwrappedAccessToken = accessToken else {
                        return
                    }
                    resolve(unwrappedAccessToken)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func resendConfirmEmail(email: String) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = sessionRequest.resendConfirmEmail(
                email: email
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func confrimEmail(email: String, code: String) -> Promise<(identity: IdentityEntity, accessToken: String)> {
        return Promise<(identity: IdentityEntity, accessToken: String)> (in: .background) { resolve, reject, _ in
            let request = sessionRequest.confirmEmail(email: email, code: code)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var identity: IdentityEntity?
                    var accessToken: String?
                    identity <- data["identity"]
                    accessToken <- data["accessToken"]
                    guard let identityEntity = identity, let unwrappedAccessToken = accessToken else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve((identityEntity, unwrappedAccessToken))
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func register(
        nickname: String,
        email: String,
        password: String,
        locale: String,
        country_code: String,
        timezone: String,
        enable_mail_notification: Bool
    ) -> Promise<(identity: IdentityEntity, accessToken: String)> {
        return Promise<(identity: IdentityEntity, accessToken: String)> (in: .background) { resolve, reject, _ in
            let request = sessionRequest.register(nickname: nickname, email: email, password: password, locale: locale, country_code: country_code, timezone: timezone, enable_mail_notification: enable_mail_notification)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var identity: IdentityEntity?
                    var user: UserEntity?
                    var accessToken: String?
                    identity <- data["identity"]
                    user <- data["user"]
                    accessToken <- data["accessToken"]
                    guard let identityEntity = identity, let unwrappedAccessToken = accessToken else {
                        reject(AppError.unknown)
                        return
                    }
                    identityEntity.User = user
                    resolve(
                        (identity: identityEntity, accessToken: unwrappedAccessToken)
                    )
            },
                onError: { err in reject(err) }
            )
        }
    }
}

