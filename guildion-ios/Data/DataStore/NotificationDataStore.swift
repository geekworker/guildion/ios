//
//  NotificationDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class NotificationDataStore: DataStoreImpl {
    static var shared: NotificationDataStore = NotificationDataStore()
    private let disposeBag = DisposeBag()
    
//    func getUserNotifications(user: UserEntity, limit: Int, offset: Int) -> Promise<NotificationEntities> {
//        return Promise<NotificationEntities> (in: .background) { resolve, reject, _ in
//            let request = notificationRequest.getUserNotifications(
//                user: user,
//                limit: limit,
//                offset: offset
//            )
//            APIManager.call(
//                request,
//                self.disposeBag,
//                onNext: { result in
//                    guard let data = result.data else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    guard let notificationEntities = NotificationEntities(map: data["notifications"]) else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    resolve(notificationEntities)
//            },
//                onError: { err in reject(err) }
//            )
//        }
//    }
//    
//    func getUserFriendRequestNotifications(user: UserEntity, limit: Int, offset: Int) -> Promise<NotificationEntities> {
//        return Promise<NotificationEntities> (in: .background) { resolve, reject, _ in
//            let request = notificationRequest.getUserFriendRequestNotifications(
//                user: user,
//                limit: limit,
//                offset: offset
//            )
//            APIManager.call(
//                request,
//                self.disposeBag,
//                onNext: { result in
//                    guard let data = result.data else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    guard let notificationEntities = NotificationEntities(map: data["notifications"]) else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    resolve(notificationEntities)
//            },
//                onError: { err in reject(err) }
//            )
//        }
//    }
//    
//    func getUserFriendRequestNotificationsCount(user: UserEntity) -> Promise<Int> {
//        return Promise<Int> (in: .background) { resolve, reject, _ in
//            let request = notificationRequest.getUserFriendRequestNotificationsCount(
//                user: user
//            )
//            APIManager.call(
//                request,
//                self.disposeBag,
//                onNext: { result in
//                    guard let data = result.data else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    var count: Int?
//                    count <- data["count"]
//                    guard let unwrapped_count = count else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    resolve(unwrapped_count)
//            },
//                onError: { err in reject(err) }
//            )
//        }
//    }
//    
//    func check(notification: NotificationEntity) -> Promise<NotificationEntity> {
//        return Promise<NotificationEntity> (in: .background) { resolve, reject, _ in
//            let request = notificationRequest.check(
//                notification: notification
//            )
//            APIManager.call(
//                request,
//                self.disposeBag,
//                onNext: { result in
//                    guard let data = result.data else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    var notificationEntity: NotificationEntity?
//                    notificationEntity <- data["notification"]
//                    guard let unwrapped = notificationEntity else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    resolve(unwrapped)
//            },
//                onError: { err in reject(err) }
//            )
//        }
//    }
//    
//    func checks(notifications: NotificationEntities) -> Promise<NotificationEntities> {
//        return Promise<NotificationEntities> (in: .background) { resolve, reject, _ in
//            print(notifications.toUnCheckedJSON())
//            let request = notificationRequest.checks(
//                notifications: notifications
//            )
//            APIManager.call(
//                request,
//                self.disposeBag,
//                onNext: { result in
//                    guard let data = result.data else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    guard let notificationEntities = NotificationEntities(map: data["notifications"]) else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    resolve(notificationEntities)
//            },
//                onError: { err in reject(err) }
//            )
//        }
//    }
//    
//    func getNotification(notification: NotificationEntity) -> Promise<NotificationEntity> {
//        return Promise<NotificationEntity> (in: .background) { resolve, reject, _ in
//            let request = notificationRequest.getNotification(
//                notification: notification
//            )
//            APIManager.call(
//                request,
//                self.disposeBag,
//                onNext: { result in
//                    guard let data = result.data else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    guard let notificationEntity = NotificationEntity(map: data["notification"]) else {
//                        reject(AppError.unknown)
//                        return
//                    }
//                    resolve(notificationEntity)
//            },
//                onError: { err in reject(err) }
//            )
//        }
//    }
}
