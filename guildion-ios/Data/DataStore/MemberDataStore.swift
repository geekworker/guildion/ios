//
//  MemberDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class MemberDataStore: DataStoreImpl {
    static var shared: MemberDataStore = MemberDataStore()
    private let disposeBag = DisposeBag()
    
    func getMember(id: Int) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            let request = memberRequest.getMember(id :id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var member: MemberEntity?
                    member <- data["member"]
                    guard let memberEntity = member else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(memberEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentRoleMembers(role: RoleEntity, limit: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = memberRequest.getCurrentRoleMembers(role: role, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncFastMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementRoleMembers(role: RoleEntity, limit: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = memberRequest.getIncrementRoleMembers(role: role, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncFastMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementRoleMembers(role: RoleEntity, limit: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = memberRequest.getDecrementRoleMembers(role: role, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncFastMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getRoles(member: MemberEntity) -> Promise<MemberRoleEntities> {
        return Promise<MemberRoleEntities> (in: .background) { resolve, reject, _ in
            let request = memberRequest.getRoles(member: member)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let entities = MemberRoleEntities(map: data["member_roles"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(entities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            let request = memberRequest.update(member: member)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: MemberEntity?
                    entity <- data["member"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updates() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = memberRequest.updates
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateNotification(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            let request = memberRequest.updateNotification(member: member)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: MemberEntity?
                    entity <- data["member"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func transferOwner(guild: GuildEntity, target: MemberEntity) -> Promise<GuildEntity> {
        return Promise<GuildEntity> (in: .background) { resolve, reject, _ in
            let request = memberRequest.transferOwner(guild: guild, target: target)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: GuildEntity?
                    entity <- data["guild"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(member_role: MemberRoleEntity) -> Promise<MemberRoleEntity> {
        return Promise<MemberRoleEntity> (in: .background) { resolve, reject, _ in
            let request = memberRequest.addRole(member_role: member_role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: MemberRoleEntity?
                    entity <- data["member_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(member_role: MemberRoleEntity) -> Promise<MemberRoleEntity> {
        return Promise<MemberRoleEntity> (in: .background) { resolve, reject, _ in
            let request = memberRequest.updateRole(member_role: member_role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: MemberRoleEntity?
                    entity <- data["member_role"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(member_role: MemberRoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = memberRequest.removeRole(member_role: member_role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateIndex(member: MemberEntity) -> Promise<MemberEntity> {
        return Promise<MemberEntity> (in: .background) { resolve, reject, _ in
            let request = memberRequest.updateIndex(member: member)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: MemberEntity?
                    entity <- data["member"]
                    guard let unwrapped = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateIndexes(members: MemberEntities) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = memberRequest.updateIndexes(members: members)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let unwrapped = MemberEntities.init(map: data["members"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }

}
