//
//  RoleDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/11/08.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class RoleDataStore: DataStoreImpl {
    static var shared: RoleDataStore = RoleDataStore()
    private let disposeBag = DisposeBag()
    
    func create(role: RoleEntity) -> Promise<RoleEntity> {
        return Promise<RoleEntity> (in: .background) { resolve, reject, _ in
            let request = roleRequest.create(role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var role: RoleEntity?
                    role <- data["role"]
                    guard let roleEntity = role else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(roleEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(role: RoleEntity) -> Promise<RoleEntity> {
        return Promise<RoleEntity> (in: .background) { resolve, reject, _ in
            let request = roleRequest.update(role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var role: RoleEntity?
                    role <- data["role"]
                    guard let roleEntity = role else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(roleEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(role: RoleEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = roleRequest.delete(role: role)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard result.data != nil else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
}
