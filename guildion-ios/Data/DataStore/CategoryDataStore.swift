//
//  CategoryDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class CategoryDataStore: DataStoreImpl {
    static var shared: CategoryDataStore = CategoryDataStore()
    private let disposeBag = DisposeBag()
    
    func getCategories(limit: Int, offset: Int) -> Promise<CategoryEntities> {
        return Promise<CategoryEntities> (in: .background) { resolve, reject, _ in
            let request = categoryRequest.getCategories(limit: limit, offset: offset)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = CategoryEntities()
                    all(entities.asyncPureMapping(map: data["categories"])).then(in: .main, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
}

