//
//  MessageDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class MessageDataStore: DataStoreImpl {
    static var shared: MessageDataStore = MessageDataStore()
    private let disposeBag = DisposeBag()
    
    func getMessage(uid: String) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            let request = messageRequest.getMessage(uid: uid)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var message: MessageEntity?
                    message <- data["message"]
                    guard let messageEntity = message else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(messageEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func reads(messages: MessageEntities) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            let request = messageRequest.reads(messages: messages)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let messageEntities = MessageEntities(map: data["messages"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(messageEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func unreads(messages: MessageEntities) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            let request = messageRequest.unreads(messages: messages)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    guard let messageEntities = MessageEntities(map: data["messages"]) else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(messageEntities)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentMessages(messageable_type: String, messageable_id: Int, limit: Int, is_static: Bool) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            let request = messageRequest.getCurrentMessages(messageable_type: messageable_type, messageable_id: messageable_id, limit: limit, is_static: is_static)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = MessageEntities()
                    all(entities.asyncMappingFast(map: data["messages"])).then(in: .background, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementMessages(messageable_type: String, messageable_id: Int, limit: Int, last_id: Int, is_static: Bool) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            let request = messageRequest.getIncrementMessages(messageable_type: messageable_type, messageable_id: messageable_id, limit: limit, last_id: last_id, is_static: is_static)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = MessageEntities()
                    all(entities.asyncMappingFast(map: data["messages"])).then(in: .background, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementMessages(messageable_type: String, messageable_id: Int, limit: Int, first_id: Int, is_static: Bool) -> Promise<MessageEntities> {
        return Promise<MessageEntities> (in: .background) { resolve, reject, _ in
            let request = messageRequest.getDecrementMessages(messageable_type: messageable_type, messageable_id: messageable_id, limit: limit, first_id: first_id, is_static: is_static)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let entities = MessageEntities()
                    all(entities.asyncMappingFast(map: data["messages"])).then(in: .background, { _ in resolve(entities) }).catch(in: .background, { reject($0) })
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(message: MessageEntity) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            let request = messageRequest.create(message: message)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var message: MessageEntity?
                    message <- data["message"]
                    guard let messageEntity = message else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(messageEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(message: MessageEntity) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            let request = messageRequest.update(message: message)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var message: MessageEntity?
                    message <- data["message"]
                    guard let messageEntity = message else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(messageEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func update(message: MessageEntity, is_static: Bool) -> Promise<MessageEntity> {
        return Promise<MessageEntity> (in: .background) { resolve, reject, _ in
            let request = messageRequest.updateStatic(message: message, is_static: is_static)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var message: MessageEntity?
                    message <- data["message"]
                    guard let messageEntity = message else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(messageEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(message: MessageEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = messageRequest.delete(message: message)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func create(reaction: ReactionEntity) -> Promise<ReactionEntity> {
        return Promise<ReactionEntity> (in: .background) { resolve, reject, _ in
            let request = messageRequest.createReaction(reaction: reaction)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: ReactionEntity?
                    entity <- data["reaction"]
                    guard let unwrappedEntity = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrappedEntity)
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func delete(reaction: ReactionEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = messageRequest.deleteReaction(reaction: reaction)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
}

