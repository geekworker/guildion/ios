//
//  UserDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class UserDataStore: DataStoreImpl {
    static var shared: UserDataStore = UserDataStore()
    private let disposeBag = DisposeBag()
    
    func getUser(id :Int) -> Promise<UserEntity> {
        return Promise<UserEntity> (in: .background) { resolve, reject, _ in
            let request = userRequest.getUser(
                id: id
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var user: UserEntity?
                    user <- data["user"]
                    guard let userEntity = user else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(userEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateUser(user: UserEntity) -> Promise<UserEntity> {
        return Promise<UserEntity> (in: .background) { resolve, reject, _ in
            let request = userRequest.updateUser(
                user: user
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var user: UserEntity?
                    user <- data["user"]
                    guard let userEntity = user else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(userEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateUserEmailNotification(user: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.updateUserEmailNotification(
                user: user
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func deleteUser() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.deleteUser
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }

    func registerDevise() -> Promise<DeviseEntity> {
        return Promise<DeviseEntity> (in: .background) { resolve, reject, _ in
            let request = userRequest.registerUserDevise
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: DeviseEntity?
                    entity <- data["devise"]
                    guard let unwrapped = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func updateDevise() -> Promise<DeviseEntity> {
        return Promise<DeviseEntity> (in: .background) { resolve, reject, _ in
            let request = userRequest.updateUserDevise
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: DeviseEntity?
                    entity <- data["devise"]
                    guard let unwrapped = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func logoutDevise() -> Promise<DeviseEntity> {
        return Promise<DeviseEntity> (in: .background) { resolve, reject, _ in
            let request = userRequest.logoutUserDevise
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var entity: DeviseEntity?
                    entity <- data["devise"]
                    guard let unwrapped = entity else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(unwrapped)
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func active() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.active
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func inActive() -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.inActive
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func block(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.block(
                target: target
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func unblock(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.unblock(
                target: target
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func mute(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.mute(
                target: target
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func unmute(target: UserEntity) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.unmute(
                target: target
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func report(target: UserEntity, detail: String) -> Promise<Void> {
        return Promise<Void> (in: .background) { resolve, reject, _ in
            let request = userRequest.report(
                target: target,
                detail: detail
            )
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    resolve(())
            },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getMembers() -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = userRequest.getMembers
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncGuildMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getCurrentMembers(limit: Int, offset: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = userRequest.getCurrentMembers(offset: offset, limit: limit)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncGuildMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getIncrementMembers(limit: Int, offset: Int, last_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = userRequest.getIncrementMembers(offset: offset, limit: limit, last_id: last_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncGuildMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getDecrementMembers(limit: Int, offset: Int, first_id: Int) -> Promise<MemberEntities> {
        return Promise<MemberEntities> (in: .background) { resolve, reject, _ in
            let request = userRequest.getDecrementMembers(offset: offset, limit: limit, first_id: first_id)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    let members = MemberEntities()
                    all(members.asyncGuildMapping(map: data["members"])).then(in: .main, { _ in resolve(members) }).catch(in: .background, { reject($0) })
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func getRelationStats() -> Promise<UserEntity> {
        return Promise<UserEntity> (in: .background) { resolve, reject, _ in
            let request = userRequest.getRelationStats
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var user: UserEntity?
                    user <- data["user"]
                    guard let userEntity = user else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve(userEntity)
            },
                onError: { err in reject(err) }
            )
        }
    }
}
