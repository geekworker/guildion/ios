//
//  AuthDataStore.swift
//  guildion-ios
//
//  Created by Apple on 2020/10/06.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import RxCocoa
import RxSwift
import Hydra

final class AuthDataStore: DataStoreImpl {
    static var shared: AuthDataStore = AuthDataStore()
    private let disposeBag = DisposeBag()
    
    func login(email: String, password: String) -> Promise<(user: UserEntity, accessToken: String)> {
        return Promise<(user: UserEntity, accessToken: String)> (in: .background) { resolve, reject, _ in
            let request = authRequest.login(email: email, password: password)
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var user: UserEntity?,
                    accessToken: String?
                    user <- data["user"]
                    accessToken <- data["accessToken"]
                    guard let userEntity = user, let unwrappedAccessToken = accessToken else {
                        reject(AppError.unknown)
                        return
                    }
                    resolve((user: userEntity, accessToken: unwrappedAccessToken))
                },
                onError: { err in reject(err) }
            )
        }
    }
    
    func syncUser() -> Promise<UserEntity?> {
        return Promise<UserEntity?> (in: .background) { resolve, reject, _ in
            let request = authRequest.syncUser
            APIManager.call(
                request,
                self.disposeBag,
                onNext: { result in
                    guard let data = result.data else {
                        reject(AppError.unknown)
                        return
                    }
                    var user: UserEntity?
                    user <- data["user"]
                    resolve(user)
                },
                onError: { err in reject(err) }
            )
        }
    }
}
